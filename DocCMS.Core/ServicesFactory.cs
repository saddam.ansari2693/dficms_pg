﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocCMS.Core.Interface;

namespace DocCMS.Core
{
    public class ServicesFactory
    {
        public static IDocCMSServices DocCMSServices
        {
            get
            {
                return DocCMS.Core.Implementation.DocCMSServices.Current();
            }
        }
    }//==== Class Ends Here ===//
}
