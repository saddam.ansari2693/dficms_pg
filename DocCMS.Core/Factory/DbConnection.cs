﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DocCMS.Core;
using Npgsql;

namespace DocCMS.Core.Factory
{
    public class DbConnection
    {
        public NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString));

        public System.Data.DataSet SelectCommand(string strQ)
        {
            var ds = new DataSet();
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        using (NpgsqlDataAdapter sqlAdpt = new NpgsqlDataAdapter { SelectCommand = sqlComd })
                        {
                            ds = new DataSet();
                            sqlAdpt.Fill(ds);
                            SqlCon.Close();
                        }
                    }
                }
                return ds;  
            }
            catch (Exception)
            {
                ds = null;
                return ds;
            }
        }

        public System.Data.DataTable SelectCommand_DataTable(string strQ)
        {
            var dt = new DataTable();
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        using (NpgsqlDataAdapter sqlAdpt = new NpgsqlDataAdapter { SelectCommand = sqlComd })
                        {
                            dt = new DataTable();
                            sqlAdpt.Fill(dt);
                            SqlCon.Close();
                        }
                    }
                }
                return dt;
            }
            catch (Exception)
            {
                dt = null;
                return dt;
            }
        }

        public string SelectString(string strQ)
        {

            string retval = "";
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = Convert.ToString(sqlComd.ExecuteScalar());
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public decimal SelectTotal(string strQ)
        {
            decimal retval = 0;
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = Convert.ToDecimal(sqlComd.ExecuteScalar());
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public int SelectCount(string strQ)
        {

            int retval = 0;
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = Convert.ToInt32(sqlComd.ExecuteScalar());
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public string SelectCountString(string strQ)
        {
            string retval = "";
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = Convert.ToString(sqlComd.ExecuteScalar());
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public Int32 InsertCommand(string strQ)
        {

            Int32 retval = 0;
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = sqlComd.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public Int32 InsertCommandReturnidentity(string strQ)
        {
            
            Int32 retval = 0;
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = sqlComd.ExecuteNonQuery();
                        if (retval == 1)
                        {
                            using (NpgsqlCommand cmdid = new NpgsqlCommand("Select @@idENTITY as value", SqlCon))
                            {
                                var objidentity = cmdid.ExecuteScalar();
                                retval = Convert.ToInt32(objidentity);
                            }
                        }
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public Int32 UpdateCommand(string strQ)
        {
            Int32 retval = 0;
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = sqlComd.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public Int32 DeleteCommand(string strQ)
        {
            Int32 retval = 0;
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                        SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        retval = sqlComd.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                }
                return retval;
            }
            catch (Exception)
            {
                return retval;
            }
        }

        public void ExecuteCommand(string strQ)
        {
            try
            {
                using (NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString)))
                {
                    if (SqlCon.State == ConnectionState.Closed)
                    {
                         SqlCon.Open();
                    }
                    using (NpgsqlCommand sqlComd = new NpgsqlCommand(strQ.ToString(), SqlCon))
                    {
                        sqlComd.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

        public string Execute_Multiple_Query(List<string> lstquery)
        {
            NpgsqlTransaction transExecute = null;
            NpgsqlCommand cmdExecute = new NpgsqlCommand();
            NpgsqlConnection SqlCon = new NpgsqlConnection(ServicesFactory.DocCMSServices.Decrypt(ConfigurationManager.ConnectionStrings["DocCMSConnectionString"].ConnectionString));
            try
            {

                if (SqlCon.State == ConnectionState.Closed)
                {
                    SqlCon.Open();
                }
                transExecute = SqlCon.BeginTransaction();
                for (int intLoopVariable = 0; intLoopVariable < lstquery.Count; intLoopVariable++)
                {
                    cmdExecute = new NpgsqlCommand(lstquery[intLoopVariable], SqlCon);
                    cmdExecute.Transaction = transExecute;
                    cmdExecute.ExecuteNonQuery();
                }
                transExecute.Commit();
            }
            catch (SqlException sqlex)
            {
                if (transExecute != null)
                {
                    transExecute.Rollback();
                    return sqlex.Message.Replace("'", "");
                }
            }
            catch (Exception ex)
            {
                if (transExecute != null)
                {
                    transExecute.Rollback();
                    return ex.Message.Replace("'", "");
                }
            }
            finally
            {
                cmdExecute.Dispose();
                SqlCon.Close();
            }
            return "";
        }

    }// Class Ends here
}

    