﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.Factory
{
    public static class Service
    {
        #region Private

        private static DbConnection _dbFunctions;

        #endregion

        #region Public properties

        public static DbConnection DbFunctions
        {
            get { return _dbFunctions ?? (_dbFunctions = new DbConnection()); }
        }

        #endregion
    }
}
