﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocCMS.Core.Interface;
using DocCMS.Core.Factory;
using System.Data;
using DocCMS.Core.DataTypes;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;
using System.Net;

namespace DocCMS.Core.Implementation
{
    public partial class DocCMSServices : IDocCMSServices
    {

        #region Singletone Method
        private static DocCMSServices _current;

        public static DocCMSServices Current()
        {
            if (_current == null)
            {
                _current = new DocCMSServices();
            }
            return _current;
        }

        public DocCMSServices()
        {
        }

        #endregion

        #region Common Function
        public string Get_Single_Value_String(string Colname, string tablename, string conCol, string conval, string paramtype)
        {
            string retval = "";
            try
            {
                StringBuilder strData = new StringBuilder();
                strData.Append(" Select " + Colname + " from " + tablename + " where " + conCol + "=");
                if (paramtype == "Number")
                {
                    strData.Append(conval);
                }
                else
                {
                    strData.Append(" '");
                    strData.Append(conval);
                    strData.Append(" '");
                }
                object objData = Service.DbFunctions.SelectCountString(strData.ToString());
                if (objData != null)
                {
                    retval = Convert.ToString(objData);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Cryptography Functions
        private const String DEFAULTKEY = "OIWPLTBZD";
        private String mstrErrorString = String.Empty;
        private String mstrOutputString = String.Empty;

        static Byte[] GetMD5Hash(String strKey)
        {
            MD5CryptoServiceProvider objHashMD5 = null;
            Byte[] objPwdhash;
            try
            {
                objHashMD5 = new MD5CryptoServiceProvider();
                objPwdhash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strKey));

            }
            catch (Exception ex)
            {
                throw new Exception(" MD5_ERROR Crypto.Vb::GetMD5Hash() Unable to generate MD5 hash.");
            }
            finally
            {
                if (objHashMD5 != null)
                {
                    objHashMD5.Clear();
                }
            }
            return objPwdhash;
        }
        public String Encrypt(String strDecryptedString)
        {
            return EncryptWithKey(strDecryptedString, DEFAULTKEY).Replace("+", "^^").Replace("=", "~~");
        }
        public String Decrypt(String strEncryptedString)
        {
            return DecryptWithKey(strEncryptedString.Replace("^^", "+").Replace("~~", "="), DEFAULTKEY);
        }
        public String EncryptWithKey(String strDecryptedString, String strKey)
        {
            String strEncrypted = String.Empty;
            TripleDESCryptoServiceProvider objDES = null;
            Byte[] objBuff;
            try
            {
                objDES = new TripleDESCryptoServiceProvider();
                if (strKey.Length > 0)
                {
                    objDES.Key = GetMD5Hash(strKey);
                }
                objDES.Mode = CipherMode.ECB;
                objBuff = ASCIIEncoding.ASCII.GetBytes(strDecryptedString);
                strEncrypted = Convert.ToBase64String(objDES.CreateEncryptor().TransformFinalBlock(objBuff, 0, objBuff.Length));
            }
            catch (Exception ex)
            {
                throw new Exception(" CRYPTO_ERROR Crypto.cs::EncryptWithKey() Error encrypting string.");
            }
            finally
            {
                if (objDES != null)
                {
                    objDES.Clear();
                }
            }
            return strEncrypted;
        }


        public String DecryptWithKey(String strEncryptedString, String strKey)
        {
            String strDecrypted = String.Empty;
            TripleDESCryptoServiceProvider objDES = null;
            Byte[] objBuff;
            try
            {
                if (strKey.Length == 0)
                {
                    throw new Exception(" Invalid key supplied. Unable to decrypt.");
                }
                objDES = new TripleDESCryptoServiceProvider();
                if (strKey.Length > 0)
                {
                    objDES.Key = GetMD5Hash(strKey);
                }
                objDES.Mode = CipherMode.ECB;
                objBuff = Convert.FromBase64String(strEncryptedString);
                strDecrypted = ASCIIEncoding.ASCII.GetString(objDES.CreateDecryptor().TransformFinalBlock(objBuff, 0, objBuff.Length));
            }
            catch (Exception ex)
            {
                throw new Exception(" CRYPTO_ERROR Crypto.cs::DecryptWithKey() Error decrypting string.");
            }
            finally
            {
                if (objDES != null)
                {
                    objDES.Clear();
                }
            }
            return strDecrypted;
        }

        #endregion encryption password

        #region Functions for User Profiles
        // TO FETCH ALL USER DETAILS TO BIND REPEATER ON USER DASHBOARD
        public DataTable Fetch_All_Users()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUsers = new StringBuilder();
                strUsers.Append(" Select userprofiles.userid,userprofiles.fname || ' ' || userprofiles.lname as name ");
                strUsers.Append("  ,userprofiles.emailid,userprofiles.address,userprofiles.city,userprofiles.state,userprofiles.phonenumber");
                strUsers.Append("  ,userprofiles.active,to_char(userprofiles.creationdate, 'DD/MM/YYYY') creationdate ");
                strUsers.Append("  ,userroles.rolename,userprofiles.roleid,userroles.parentid from userprofiles");
                strUsers.Append("  INNER JOIN userroles ");
                strUsers.Append("  On userprofiles.roleid=userroles.roleid ");
                strUsers.Append("  where userid!='47A0E9E2-5115-4BC0-8422-4F94C647799D' and userroles.active=True");
                strUsers.Append("  Order by name");
                DataSet ds = Service.DbFunctions.SelectCommand(strUsers.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TO FETCH PARTICULAR USER DETAILS BY GIVING emailid AND password
        public DataTable Fetch_User_Profile_Details(string emailid, string password)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from userprofiles where emailid='" + emailid + "' and password='" + Encrypt(password) + "' and active=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }

        //FETCH USER BY emailid
        public Cls_UserProfiles Fetch_User_Details_Byid(string emailid)
        {
            Cls_UserProfiles objUserProfile = null;
            try
            {
                StringBuilder strUsers = new StringBuilder();
                strUsers.Append(" SELECT * from userprofiles WHERE emailid = '" + emailid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strUsers.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objUserProfile = new Cls_UserProfiles();
                        objUserProfile.emailid = Convert.ToString(dt.Rows[0]["emailid"]);
                        objUserProfile.fname = Convert.ToString(dt.Rows[0]["fname"]);
                        objUserProfile.lname = Convert.ToString(dt.Rows[0]["lname"]);
                        objUserProfile.address = Convert.ToString(dt.Rows[0]["address"]);
                        objUserProfile.city = Convert.ToString(dt.Rows[0]["city"]);
                        objUserProfile.state = Convert.ToString(dt.Rows[0]["state"]);
                        objUserProfile.zipcode = Convert.ToString(dt.Rows[0]["zipcode"]);
                        objUserProfile.phonenumber = Convert.ToString(dt.Rows[0]["phonenumber"]);
                        objUserProfile.imagename = Convert.ToString(dt.Rows[0]["imagename"]);
                        objUserProfile.password = Convert.ToString(dt.Rows[0]["password"]);
                        string activeStatus = Convert.ToString(dt.Rows[0]["active"]);
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objUserProfile.active = true;
                            }
                            else
                            {
                                objUserProfile.active = false;
                            }
                        }
                        else
                        {
                            objUserProfile.active = false;
                        }
                    }
                }
                return objUserProfile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //FETCH USER rolename
        public DataTable Fetch_Selected_Roles(Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRoles = new StringBuilder();
                strRoles.Append(" Select roleid,rolename,displayorder from userroles where active=True and roleid=" + roleid);
                DataSet ds = Service.DbFunctions.SelectCommand(strRoles.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    dt = SortTable(dtCMS, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable Fetch_Roles(Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                string SuperAdminname = ServicesFactory.DocCMSServices.Fetch_Roles_name(roleid);
                StringBuilder strRoles = new StringBuilder();
                if (SuperAdminname.ToUpper() != "SuperAdmin".ToUpper())
                {
                    strRoles.Append(" Select roleid,rolename,displayorder,active from userroles where active=True and (roleid=" + roleid+" or parentid="+roleid+")");
                }
                else
                {
                    strRoles.Append(" Select roleid,rolename,displayorder,active from userroles");
                }
                DataSet ds = Service.DbFunctions.SelectCommand(strRoles.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    dt = SortTable(dtCMS, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Fetch_Roles_name(Int32 roleid)
        {
            string Rolesname = "";
            try
            {
                StringBuilder strRoles = new StringBuilder();
                strRoles.Append(" Select rolename from userroles where roleid=" + roleid);
                Rolesname = Service.DbFunctions.SelectString(strRoles.ToString());
                if (Rolesname != null && Rolesname != "")
                {
                    return Rolesname;
                }
                return Rolesname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //FETCH SubModule for login redirect
        public DataTable Get_SubModule_From_Login(string navigationurl)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strGet = new StringBuilder();
                strGet.Append(" select * from submodules where navigationurl like '%" + navigationurl + "%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strGet.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //CHECK FOR USER 

        public bool User_Available(string emailid)
        {
            bool UserExist = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From userprofiles where emailid='" + emailid + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    UserExist = true;
                }
                return UserExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //CHECK emailid FOR change password
        public bool Check_User_By_emailid(string emailid)
        {
            bool UserExist = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From userprofiles where emailid='" + emailid + "' and active=True");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    UserExist = true;
                }
                return UserExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Check_User_Login_Details(string emailid, string password)
        {
            bool CheckUser = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From userprofiles where emailid='" + emailid + "' and password='" + Encrypt(password) + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    CheckUser = true;
                }
                return CheckUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Check_For_Active_UserRole(string emailid, string password)
        {
            bool CheckUser = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From userprofiles");
                strUser.Append("  inner join  userroles On userprofiles.roleid=userroles.roleid");
                strUser.Append("  where emailid='" + emailid + "' and password='" + Encrypt(password) + "'");
                strUser.Append("  and  userroles.active=True and userprofiles.active=True ");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    CheckUser = true;
                }
                return CheckUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TO CHANGE USER password
        public Int32 Change_User_password_ById(Guid userid, string password)
        {
            try
            {
                StringBuilder strChangePswd = new StringBuilder();
                strChangePswd.Append(" Update userprofiles ");
                strChangePswd.Append(" Set ");
                strChangePswd.Append(" password=");
                strChangePswd.Append(" '");
                strChangePswd.Append(Encrypt(password));
                strChangePswd.Append(" '");
                strChangePswd.Append(" Where ");
                strChangePswd.Append(" userid=");
                strChangePswd.Append("'");
                strChangePswd.Append(userid);
                strChangePswd.Append("'");
                return Service.DbFunctions.UpdateCommand(strChangePswd.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // TO INSERT,UPDATE DELETE USER PROFILE
        public Int32 Insert_User_Profile(Cls_UserProfiles userprofile)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strInsertUser = new StringBuilder();
                strInsertUser.Append(" Insert into userprofiles(userid,password,emailid,fname,lname,roleid,address,state,city,zipcode,phonenumber,active,createdby,creationdate,imagename) ");
                strInsertUser.Append(" Values(" );
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.userid);
                strInsertUser.Append("'");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(Encrypt(userprofile.password).Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.emailid.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.fname.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.lname.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(userprofile.roleid);
                strInsertUser.Append(",");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.address.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.state.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.city.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.zipcode);
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.phonenumber);
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.active);
                strInsertUser.Append(" '");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.createdby);
                strInsertUser.Append("'");
                strInsertUser.Append(",");
                strInsertUser.Append(" LOCALTIMESTAMP");
                strInsertUser.Append(" ,");
                strInsertUser.Append(" '");
                strInsertUser.Append(userprofile.imagename.Replace(" '", "''"));
                strInsertUser.Append(" '");
                strInsertUser.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strInsertUser.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // Compare old password with new password

        public bool Compare_password_with_Old_password(string emailid, string password)
        {
            bool CheckUser = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From userprofiles where emailid='" + emailid + "' and password='" + Encrypt(password) + "' ");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    CheckUser = true;
                }
                return CheckUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Int32 Update_User_Profile(Cls_UserProfiles userprofile)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdateUser = new StringBuilder();
                strUpdateUser.Append(" Update userprofiles ");
                strUpdateUser.Append(" Set ");
                strUpdateUser.Append(" fname=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.fname.Replace(" '", "''"));
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" lname=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.lname.Replace(" '", "''"));
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" roleid=");
                strUpdateUser.Append(userprofile.roleid);
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" address=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.address.Replace(" '", "''"));
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                if (!string.IsNullOrEmpty(userprofile.password))
                {
                    strUpdateUser.Append(" password=");
                    strUpdateUser.Append(" '");
                    strUpdateUser.Append(Encrypt(userprofile.password));
                    strUpdateUser.Append(" '");
                    strUpdateUser.Append(" ,");
                }
                strUpdateUser.Append(" state=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.state.Replace(" '", "''"));
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" city=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.city.Replace(" '", "''"));
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" zipcode=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.zipcode);
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" phonenumber=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.phonenumber);
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" active=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.active);
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" modifiedby=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.modifiedby);
                strUpdateUser.Append("'");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" modifieddate=");
                strUpdateUser.Append(" LOCALTIMESTAMP ");
                strUpdateUser.Append(" ,");
                strUpdateUser.Append(" imagename=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.imagename);
                strUpdateUser.Append(" '");
                strUpdateUser.Append(" Where ");
                strUpdateUser.Append(" emailid=");
                strUpdateUser.Append(" '");
                strUpdateUser.Append(userprofile.emailid);
                strUpdateUser.Append(" '");
                retVal = Service.DbFunctions.UpdateCommand(strUpdateUser.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        public void Delete_User_Profile(string emailid)
        {
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from userprofiles where emailid='" + emailid + "'");
                Service.DbFunctions.DeleteCommand(strDelete.ToString());
            }
            catch (Exception ex)
            { throw ex; }
        }
        // Fetch Top 1 Sub Module Navigation URL by role id to redirect from login page

        public DataTable Fetch_SubModule_URL_By_Role_Id(Int32 roleid)
        {
            try
            {
                DataTable dtURL = null;
                StringBuilder strfetch = new StringBuilder();
                strfetch.Append(" Select  submodules.navigationurl,submodules.submoduleid from submodules inner join  roleprivileges ");
                strfetch.Append("  on submodules.submoduleid=roleprivileges.submoduleid where roleprivileges.roleid=" + roleid);
                strfetch.Append("  and  (isview=True OR isedit=True OR isdelete=True OR isadd=True)");
                strfetch.Append("   Order by roleprivileges.id asc limit 1");
                DataSet ds = Service.DbFunctions.SelectCommand(strfetch.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtURL = ds.Tables[0];

                }
                return dtURL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region User Roles Functions

        // Fetch The User Roles and Privilege
        public DataTable Fetch_RolePrivilege(Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select COUNT(*) From roleprivileges where roleid="+roleid+"");
                Int32 PrivCount = Convert.ToInt32(Service.DbFunctions.SelectCount(strRolePrivilege.ToString()));
                strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select modules.moduleid,submoduleid,modulename,COALESCE(isview,0) [View],COALESCE(isadd,0) [Add], COALESCE(isedit,0) [Edit],COALESCE(isdelete,0) [Delete]  from modules  ");
                strRolePrivilege.Append("  INNER Join submodules  ON modules.moduleid=submodules.moduleid ");
                strRolePrivilege.Append("  LEFT OUTER Join roleprivileges  ON roleprivileges.moduleid=modules.moduleid ");
                if (PrivCount > 0)
                {
                    strRolePrivilege.Append("  where roleprivileges.roleid="+roleid+" ");
                }
                strRolePrivilege.Append("  order by moduleid,submoduleid");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        // Fetch The User Roles
        public DataTable Fetch_User_Roles(Int32 parentid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUserRole = new StringBuilder();
                string SuperAdminname = ServicesFactory.DocCMSServices.Fetch_Roles_name(parentid);
                if (SuperAdminname.ToUpper() == "SuperAdmin".ToUpper())
                {
                    strUserRole.Append(" Select * from userroles");
                }
                else
                {
                    strUserRole.Append(" Select * from userroles where (parentid="+parentid+" or roleid="+parentid+")");
                }
                DataSet ds = Service.DbFunctions.SelectCommand(strUserRole.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    dt = SortTable(dtCMS, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //FETCH SUBMODULES
        public DataTable Fetch_SubModules(Int32 moduleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select moduleid innermoduleid,submoduleid,submodulename,displayorder from submodules  where moduleid=" + moduleid);
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    dt = SortTable(dtCMS, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch The User Roles by id
        public Cls_UserRoles Fetch_UserRole_ById(Int32 roleid)
        {
            Cls_UserRoles ObjUserRole = null;
            try
            {
                StringBuilder strUserRole = new StringBuilder();
                strUserRole.Append(" Select rolename,description,displayorder,active from userroles where roleid='"+roleid+"'");
                DataSet ds = Service.DbFunctions.SelectCommand(strUserRole.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ObjUserRole = new Cls_UserRoles();
                        ObjUserRole.rolename = Convert.ToString(dt.Rows[0]["rolename"]);
                        ObjUserRole.description = Convert.ToString(dt.Rows[0]["description"]);
                        ObjUserRole.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(dt.Rows[0]["active"]);
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE" || activeStatus.ToUpper() == "0")
                            {
                                ObjUserRole.active = true;
                            }
                            else
                            {
                                ObjUserRole.active = false;
                            }
                        }
                        else
                        {
                            ObjUserRole.active = false;
                        }
                    }
                }
                return ObjUserRole;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Insert User Roles
        public Int32 Insert_User_Roles(Cls_UserRoles userRole)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserRole = new StringBuilder();
                strUserRole.Append(" Insert into userroles(rolename,description,displayorder,active,parentid,createby,creationdate) ");
                strUserRole.Append(" Values(" );
                strUserRole.Append(" '");
                strUserRole.Append(userRole.rolename.Replace(" '", "''"));
                strUserRole.Append(" '");
                strUserRole.Append(" ,");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.description.Replace(" '", "''"));
                strUserRole.Append(" '");
                strUserRole.Append(" ,");
                strUserRole.Append(userRole.displayorder);
                strUserRole.Append(" ,");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.active);
                strUserRole.Append(" '");
                strUserRole.Append(" ,");
                strUserRole.Append(userRole.parentid);
                strUserRole.Append(",");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.createby);
                strUserRole.Append("'");
                strUserRole.Append(" ,");
                strUserRole.Append(" LOCALTIMESTAMP");
                strUserRole.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strUserRole.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // TO FETCH ALL USER DETAILS TO BIND REPEATER ON USER DASHBOARD
        public DataTable Fetch_All_Users_Except_SuperAdmin()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUsers = new StringBuilder();
                strUsers.Append(" Select userprofiles.userid,userprofiles.fname|| ' ' ||userprofiles.lname as name ");
                strUsers.Append("  ,userprofiles.emailid,userprofiles.address,userprofiles.city,userprofiles.state,userprofiles.phonenumber");
                strUsers.Append("  ,userprofiles.active,to_char(userprofiles.creationdate, 'DD/MM/YYYY') creationdate  ");
                strUsers.Append("  ,userroles.rolename,userprofiles.roleid,userroles.parentid from userprofiles");
                strUsers.Append("  INNER JOIN userroles ");
                strUsers.Append("  On userprofiles.roleid=userroles.roleid ");
                strUsers.Append("  where userid!='47A0E9E2-5115-4BC0-8422-4F94C647799D' and userroles.active=True and rolename<>'SuperAdmin'");
                strUsers.Append("  Order by name");
                DataSet ds = Service.DbFunctions.SelectCommand(strUsers.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Update User Roles
        public Int32 Update_UserRoles(Cls_UserRoles userRole)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserRole = new StringBuilder();
                strUserRole.Append(" Update userroles ");
                strUserRole.Append(" Set ");
                strUserRole.Append(" rolename=");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.rolename.Replace(" '", "''"));
                strUserRole.Append(" '");
                strUserRole.Append(" ,");
                strUserRole.Append(" description=");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.description.Replace(" '", "''"));
                strUserRole.Append(" '");
                strUserRole.Append(" ,");
                strUserRole.Append(" displayorder=");
                strUserRole.Append(userRole.displayorder);
                strUserRole.Append(" ,");
                strUserRole.Append(" active=");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.active);
                strUserRole.Append(" '");
                strUserRole.Append(" ,");
                strUserRole.Append(" parentid=");
                strUserRole.Append(userRole.parentid);
                strUserRole.Append(",");
                strUserRole.Append(" lastmodifiedby=");
                strUserRole.Append(" '");
                strUserRole.Append(userRole.lastmodifiedby).Replace(" ", "");
                strUserRole.Append("'");
                strUserRole.Append(" ,");
                strUserRole.Append(" lastmodificationdate=");
                strUserRole.Append(" LOCALTIMESTAMP ");
                strUserRole.Append(" Where ");
                strUserRole.Append(" roleid=");
                strUserRole.Append(userRole.roleid);
                retVal = Service.DbFunctions.UpdateCommand(strUserRole.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Update User Roles Display Order
        public Int32 Update_Role_Dorder(List<Cls_UserRoles> objRole)
        {
            Int32 retVal = 0;
            try
            {
                if (objRole != null && objRole.Count > 0)
                {
                    for (Int32 i = 0; i < objRole.Count; i++)
                    {

                        StringBuilder strRole = new StringBuilder();
                        strRole.Append(" Update userroles ");
                        strRole.Append(" Set ");
                        strRole.Append(" displayorder=");
                        strRole.Append(objRole[i].displayorder);
                        strRole.Append(" ,");
                        strRole.Append(" lastmodifiedby=");
                        strRole.Append(" '");
                        strRole.Append(objRole[i].lastmodifiedby);
                        strRole.Append("'");
                        strRole.Append(" ,");
                        strRole.Append(" lastmodificationdate=");
                        strRole.Append(" LOCALTIMESTAMP ");
                        strRole.Append(" Where ");
                        strRole.Append(" roleid=");
                        strRole.Append(objRole[i].roleid);
                        retVal = Service.DbFunctions.UpdateCommand(strRole.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Delete User Roles By roleid
        public Int32 Delete_userroles(Int32 roleid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserRole = new StringBuilder();
                strUserRole.Append(" Delete from userroles ");
                strUserRole.Append(" Where ");
                strUserRole.Append(" roleid=");
                strUserRole.Append(roleid);
                retVal = Service.DbFunctions.DeleteCommand(strUserRole.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //Check for Existing Role name
        public Int32 Check_Existing_Role(string rolename)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*)  from userroles where rolename='" + rolename + "' ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Reordering functions block for UserRole Display Order
        public void Reorder_UserRoles(Int32 roleid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_User_Role_Dorder();
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "Select  roleid from userroles where displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["roleid"].ToString());
                        query = string.Format(" Update userroles SET displayorder=displayorder-1 where roleid={0}", roleid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userroles SET displayorder=displayorder+1 where roleid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "Select roleid from userroles where displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["roleid"].ToString());
                        query = string.Format(" Update userroles SET displayorder=displayorder+1 where roleid={0}", roleid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userroles SET displayorder=displayorder-1 where roleid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Get Maximum Display Order for userroles
        public Int32 Get_Max_User_Role_Dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from userroles");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        // To get Minimum Display Order for userroles
        public Int32 Get_Min_User_Role_Dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(Min(displayorder),0) as Mindorder from userroles");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Adjust User Role Display Order
        public void Adjust_User_Role_Dorder()
        {
            try
            {
                DataTable Dtcontent = null;
                string query = "select * from userroles";
                DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    Dtcontent = SortTable(dtCMS, "displayorder");
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int index;
                    for (index = 0; index < Dtcontent.Rows.Count; index++)
                    {
                        Int32 id = Convert.ToInt32(Dtcontent.Rows[index]["roleid"]);
                        Int32 displayorder = Convert.ToInt32(Dtcontent.Rows[index]["displayorder"]);
                        if (displayorder != (index + 1))
                        {
                            query = string.Format(" Update userroles SET displayorder=" + (index + 1) + " where roleid={0}", id);
                            Service.DbFunctions.UpdateCommand(query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region modules Functions

        // Fetch modules
        public DataTable Fetch_Modules()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select * from modules Order By displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        // Fetch modules By Module id
        public Clsmodules Fetch_Modules_ById(Int32 moduleid)
        {
            Clsmodules objModule = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select moduleid,modulename,description,navigationurl,displayorder,active from modules where moduleid='" + moduleid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objModule = new Clsmodules();
                        objModule.moduleid = Convert.ToInt32(dt.Rows[0]["moduleid"]);
                        objModule.modulename = Convert.ToString(dt.Rows[0]["modulename"]);
                        objModule.description = Convert.ToString(dt.Rows[0]["description"]);
                        objModule.navigationurl = Convert.ToString(dt.Rows[0]["navigationurl"]);
                        objModule.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["active"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objModule.active = true;
                            }
                            else
                            {
                                objModule.active = false;
                            }
                        }
                        else
                        {
                            objModule.active = false;
                        }
                    }
                }
                return objModule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Check for Existing Module name
        public Int32 Check_Existing_Module(string modulename)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strmodulename = new StringBuilder();
                strmodulename.Append(" Select COUNT(*)  from modules where modulename='" + modulename + "' ");
                object objCount = Service.DbFunctions.SelectCount(strmodulename.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert New Module
        public Int32 Insert_Modules(Clsmodules modules)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Insert into modules(modulename,description,navigationurl,displayorder,active,createdby,creationdate) ");
                strmodules.Append(" Values(" );
                strmodules.Append(" '");
                strmodules.Append(modules.modulename.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(modules.description.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(modules.navigationurl.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(modules.displayorder);
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(modules.active);
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(modules.createdby);
                strmodules.Append("'");
                strmodules.Append(",");
                strmodules.Append(" LOCALTIMESTAMP");
                strmodules.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //To Update Existing Module
        public Int32 Update_Modules(Clsmodules modules)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Update modules ");
                strmodules.Append(" Set ");
                strmodules.Append(" modulename=");
                strmodules.Append(" '");
                strmodules.Append(modules.modulename.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" description=");
                strmodules.Append(" '");
                strmodules.Append(modules.description.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" navigationurl=");
                strmodules.Append(" '");
                strmodules.Append(modules.navigationurl.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" displayorder=");
                strmodules.Append(modules.displayorder);
                strmodules.Append(" ,");
                strmodules.Append(" active=");
                strmodules.Append(" '");
                strmodules.Append(modules.active);
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" lastmodifiedby=");
                strmodules.Append(" '");
                strmodules.Append(modules.lastmodifiedby);
                strmodules.Append("'");
                strmodules.Append(" ,");
                strmodules.Append(" lastmodificationdate=");
                strmodules.Append(" LOCALTIMESTAMP ");
                strmodules.Append(" Where ");
                strmodules.Append(" moduleid=");
                strmodules.Append(modules.moduleid);
                retVal = Service.DbFunctions.UpdateCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        //To Update Existing Module Display order
        public Int32 Update_Module_Dorder(List<Clsmodules> objmodules)
        {
            Int32 retVal = 0;
            try
            {
                if (objmodules != null && objmodules.Count > 0)
                {
                    for (Int32 i = 0; i < objmodules.Count; i++)
                    {

                        StringBuilder strmodules = new StringBuilder();
                        strmodules.Append(" Update modules ");
                        strmodules.Append(" Set ");
                        strmodules.Append(" displayorder=");
                        strmodules.Append(objmodules[i].displayorder);
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodifiedby=");
                        strmodules.Append(" '");
                        strmodules.Append(objmodules[i].lastmodifiedby);
                        strmodules.Append("'");
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodificationdate=");
                        strmodules.Append(" LOCALTIMESTAMP ");
                        strmodules.Append(" Where ");
                        strmodules.Append(" moduleid=");
                        strmodules.Append(objmodules[i].moduleid);
                        retVal = Service.DbFunctions.UpdateCommand(strmodules.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Delete Module
        public Int32 Delete_Modules(Int32 moduleid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Delete from modules ");
                strmodules.Append(" Where ");
                strmodules.Append(" moduleid=");
                strmodules.Append(moduleid);
                retVal = Service.DbFunctions.DeleteCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // Reordering functions block for modules Display Order
        public void Reorder_Modules(Int32 moduleid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_Modules_Dorder();
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "select moduleid from modules where displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["moduleid"].ToString());
                        query = string.Format(" Update modules SET displayorder=displayorder-1 where moduleid={0}", moduleid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update modules SET displayorder=displayorder+1 where moduleid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "select  moduleid from modules where displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["moduleid"].ToString());
                        query = string.Format(" Update modules SET displayorder=displayorder+1 where moduleid={0}", moduleid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update modules SET displayorder=displayorder-1 where moduleid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Maximum Display Order for Module
        public Int32 Get_Max_Modules_Dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from modules");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        // To Get Minimum Display Order for Module
        public Int32 Get_Min_Modules_Dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(Min(displayorder),0) as Mindorder from modules");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Adjust Module Display Order 
        public void Adjust_Modules_Dorder()
        {
            try
            {
                DataTable Dtcontent = null;
                string query = "select * from modules Order by displayorder";
                DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int index;
                    for (index = 0; index < Dtcontent.Rows.Count; index++)
                    {
                        Int32 id = Convert.ToInt32(Dtcontent.Rows[index]["moduleid"]);
                        Int32 displayorder = Convert.ToInt32(Dtcontent.Rows[index]["displayorder"]);
                        if (displayorder != (index + 1))
                        {
                            query = string.Format(" Update modules SET displayorder=" + (index + 1) + " where moduleid={0}", id);
                            Service.DbFunctions.UpdateCommand(query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Sub modules Functions

        // Fetch All submodules
        public DataTable Fetch_SubModules()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Select submodules.moduleid,modules.modulename,submodules.submoduleid,submodules.submodulename ");
                strsubmodules.Append(" ,submodules.navigationurl,submodules.displayorder,submodules.active  ");
                strsubmodules.Append(" from submodules  ");
                strsubmodules.Append(" inner join modules On SUBmodules.moduleid=modules.moduleid  ");
                strsubmodules.Append(" Order By submodules.moduleid,submodules.displayorder ");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Fetch submodules by Module id
        public DataTable Fetch_SubModules_ByModuleId(Int32 moduleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Select submoduleid,submodulename,displayorder,active from submodules  where moduleid=" + moduleid + " Order By displayorder Asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Fetch SubModule By SubModule id
        public Clssubmodules Fetch_SubModules_ById(Int32 submoduleid)
        {
            Clssubmodules objSubModule = null;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Select submoduleid,submodulename,description,navigationurl,displayorder,active from submodules where submoduleid='" + submoduleid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objSubModule = new Clssubmodules();
                        objSubModule.submoduleid = Convert.ToInt32(dt.Rows[0]["submoduleid"]);
                        objSubModule.submodulename = Convert.ToString(dt.Rows[0]["submodulename"]);
                        objSubModule.description = Convert.ToString(dt.Rows[0]["description"]);
                        objSubModule.navigationurl = Convert.ToString(dt.Rows[0]["navigationurl"]);
                        objSubModule.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["active"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objSubModule.active = true;
                            }
                            else
                            {
                                objSubModule.active = false;
                            }
                        }
                        else
                        {
                            objSubModule.active = false;
                        }
                    }
                }
                return objSubModule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch All moduleid and modulename For DropDown od Module name
        public DataTable Fetch_ModulesName()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select moduleid,modulename,displayorder from modules where active=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtModule = ds.Tables[0];
                    dt = SortTable(dtModule, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Check for Existing Sub Module name
        public Int32 Check_Existing_SubModule(string submodulename)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*)  from submodules where submodulename='" + submodulename + "' ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Insert New submodules
        public Int32 Insert_SubModules(Clssubmodules submodules)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Insert into submodules(submodulename,moduleid,description,navigationurl,displayorder,active,createdby,creationdate) ");
                strsubmodules.Append(" Values(" );
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.submodulename.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(submodules.moduleid);
                strsubmodules.Append(" ,");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.description.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.navigationurl.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(submodules.displayorder);
                strsubmodules.Append(" ,");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.active);
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.createdby);
                strsubmodules.Append("'");
                strsubmodules.Append(",");
                strsubmodules.Append(" LOCALTIMESTAMP");
                strsubmodules.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strsubmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // To Update Existing SubModule
        public Int32 Update_SubModules(Clssubmodules submodules)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Update submodules ");
                strsubmodules.Append(" Set ");
                strsubmodules.Append(" submodulename=");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.submodulename.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" moduleid=");
                strsubmodules.Append(submodules.moduleid);
                strsubmodules.Append(" ,");
                strsubmodules.Append(" description=");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.description.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" navigationurl=");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.navigationurl.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" displayorder=");
                strsubmodules.Append(submodules.displayorder);
                strsubmodules.Append(" ,");
                strsubmodules.Append(" active=");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.active);
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" lastmodifiedby=");
                strsubmodules.Append(" '");
                strsubmodules.Append(submodules.lastmodifiedby);
                strsubmodules.Append("'");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" lastmodificationdate=");
                strsubmodules.Append(" LOCALTIMESTAMP ");
                strsubmodules.Append(" Where ");
                strsubmodules.Append(" submoduleid=");
                strsubmodules.Append(submodules.submoduleid);
                retVal = Service.DbFunctions.UpdateCommand(strsubmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // To Update Existing SubModule Display order
        public Int32 Update_SubModule_Dorder(List<Clssubmodules> objsubmodules)
        {
            Int32 retVal = 0;
            try
            {
                if (objsubmodules != null && objsubmodules.Count > 0)
                {
                    for (Int32 i = 0; i < objsubmodules.Count; i++)
                    {

                        StringBuilder strsubmodules = new StringBuilder();
                        strsubmodules.Append(" Update submodules ");
                        strsubmodules.Append(" Set ");
                        strsubmodules.Append(" displayorder=");
                        strsubmodules.Append(objsubmodules[i].displayorder);
                        strsubmodules.Append(" ,");
                        strsubmodules.Append(" lastmodifiedby=");
                        strsubmodules.Append(" '");
                        strsubmodules.Append(objsubmodules[i].lastmodifiedby);
                        strsubmodules.Append("'");
                        strsubmodules.Append(" ,");
                        strsubmodules.Append(" lastmodificationdate=");
                        strsubmodules.Append(" LOCALTIMESTAMP ");
                        strsubmodules.Append(" Where ");
                        strsubmodules.Append(" submoduleid=");
                        strsubmodules.Append(objsubmodules[i].submoduleid);
                        retVal = Service.DbFunctions.UpdateCommand(strsubmodules.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // To Delete SubModule By submoduleid
        public Int32 Delete_SubModules(Int32 submoduleid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Delete from submodules ");
                strmodules.Append(" Where ");
                strmodules.Append(" submoduleid=");
                strmodules.Append(submoduleid);
                retVal = Service.DbFunctions.DeleteCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //Reordering functions block for submodules Display Order
        public void Reorder_SubModules(Int32 submoduleid, Int32 moduleid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_SubModules_Dorder(moduleid);
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "select  submoduleid from submodules where moduleid=" + moduleid + " and displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["submoduleid"].ToString());
                        query = string.Format(" Update submodules SET displayorder=displayorder-1 where submoduleid={0}", submoduleid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update submodules SET displayorder=displayorder+1 where submoduleid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                    else
                    {
                        Adjust_Submodules_Dorder(moduleid);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "select  submoduleid from submodules where moduleid=" + moduleid + " and displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["submoduleid"].ToString());
                        query = string.Format(" Update submodules SET displayorder=displayorder+1 where submoduleid={0}", submoduleid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update submodules SET displayorder=displayorder-1 where submoduleid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                    else
                    {
                        Adjust_Submodules_Dorder(moduleid);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // To Get Max Sub Module By moduleid for Display Order
        public Int32 Get_Max_SubModules_Dorder(Int32 moduleid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from submodules where moduleid=" + moduleid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // To Get Minimum Sub Module  By moduleid  for Display Order
        public Int32 Get_Min_SubModules_Dorder(Int32 moduleid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(Min(displayorder),0) as Mindorder from submodules where moduleid=" + moduleid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Adjust SubModule  By moduleid  Display order
        public void Adjust_Submodules_Dorder(Int32 moduleid)
        {
            try
            {
                DataTable Dtcontent = null;
                string query = "select * from submodules where moduleid=" + moduleid + " Order by displayorder";
                DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int index;
                    for (index = 0; index < Dtcontent.Rows.Count; index++)
                    {
                        Int32 id = Convert.ToInt32(Dtcontent.Rows[index]["submoduleid"]);
                        Int32 displayorder = Convert.ToInt32(Dtcontent.Rows[index]["displayorder"]);

                        if (displayorder != (index + 1))
                        {
                            query = string.Format(" Update submodules SET displayorder=" + (index + 1) + " where submoduleid={0}", id);
                            Service.DbFunctions.UpdateCommand(query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region privileges Functions

        // Fetch  All privileges
        public DataTable Fetch_privileges()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strprivileges = new StringBuilder();
                strprivileges.Append(" Select * from privileges Order By displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strprivileges.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Fetch privileges By privilgeid
        public Clsprivileges Fetch_privileges_Byid(Int32 privilgeid)
        {
            Clsprivileges objPrivilege = null;
            try
            {
                StringBuilder strprivileges = new StringBuilder();
                strprivileges.Append(" Select privilgeid,privilegename,displayorder,active from privileges where privilgeid='" + privilgeid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strprivileges.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objPrivilege = new Clsprivileges();
                        objPrivilege.privilgeid = Convert.ToInt32(dt.Rows[0]["privilgeid"]);
                        objPrivilege.privilegename = Convert.ToString(dt.Rows[0]["privilegename"]);
                        objPrivilege.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["active"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objPrivilege.active = true;
                            }
                            else
                            {
                                objPrivilege.active = false;
                            }
                        }
                        else
                        {
                            objPrivilege.active = false;
                        }
                    }
                }
                return objPrivilege;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Insert new Privilege
        public Int32 Insert_privileges(Clsprivileges privileges)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strprivileges = new StringBuilder();
                strprivileges.Append(" Insert into privileges(privilegename,displayorder,active,createdby,creationdate) ");
                strprivileges.Append(" Values(" );
                strprivileges.Append(" '");
                strprivileges.Append(privileges.privilegename.Replace(" '", "''"));
                strprivileges.Append(" '");
                strprivileges.Append(" ,");
                strprivileges.Append(privileges.displayorder);
                strprivileges.Append(" ,");
                strprivileges.Append(" '");
                strprivileges.Append(privileges.active);
                strprivileges.Append(" '");
                strprivileges.Append(" ,");
                strprivileges.Append(" '");
                strprivileges.Append(privileges.createdby);
                strprivileges.Append("'");
                strprivileges.Append(",");
                strprivileges.Append(" LOCALTIMESTAMP");
                strprivileges.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strprivileges.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // To Update Existing Privilege
        public Int32 Update_privileges(Clsprivileges privileges)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strprivileges = new StringBuilder();
                strprivileges.Append(" Update privileges ");
                strprivileges.Append(" Set ");
                strprivileges.Append(" privilegename=");
                strprivileges.Append(" '");
                strprivileges.Append(privileges.privilegename.Replace(" '", "''"));
                strprivileges.Append(" '");
                strprivileges.Append(" ,");
                strprivileges.Append(" displayorder=");
                strprivileges.Append(privileges.displayorder);
                strprivileges.Append(" ,");
                strprivileges.Append(" active=");
                strprivileges.Append(" '");
                strprivileges.Append(privileges.active);
                strprivileges.Append(" '");
                strprivileges.Append(" ,");
                strprivileges.Append(" lastmodifiedby=");
                strprivileges.Append(" '");
                strprivileges.Append(privileges.lastmodifiedby);
                strprivileges.Append("'");
                strprivileges.Append(" ,");
                strprivileges.Append(" lastmodificationdate=");
                strprivileges.Append(" LOCALTIMESTAMP ");
                strprivileges.Append(" Where ");
                strprivileges.Append(" privilgeid=");
                strprivileges.Append(privileges.privilgeid);
                retVal = Service.DbFunctions.UpdateCommand(strprivileges.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // to delete Privilege by Privilege id
        public Int32 Delete_privileges(Int32 privilgeid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strprivileges = new StringBuilder();
                strprivileges.Append(" Delete from privileges ");
                strprivileges.Append(" Where ");
                strprivileges.Append(" privilgeid=");
                strprivileges.Append(privilgeid);
                retVal = Service.DbFunctions.DeleteCommand(strprivileges.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // Reorder the Privilege
        public void Reorder_privileges(Int32 privilgeid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_privileges_dorder();
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "Select  privilgeid from privileges where displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["privilgeid"].ToString());
                        query = string.Format(" Update privileges SET displayorder=displayorder-1 where privilgeid={0}", privilgeid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update privileges SET displayorder=displayorder+1 where privilgeid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "Select  privilgeid from privileges where displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["privilgeid"].ToString());
                        query = string.Format(" Update privileges SET displayorder=displayorder+1 where privilgeid={0}", privilgeid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update privileges SET displayorder=displayorder-1 where privilgeid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Max Privilege For Display order
        public Int32 Get_Max_privileges_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from privileges");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //Get Minimum Privilege For Display order
        public Int32 Get_Min_privileges_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(Min(displayorder),0) as Mindorder from privileges");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Adjust Privilege Display order
        public void Adjust_privileges_dorder()
        {
            try
            {
                DataTable Dtcontent = null;
                string query = "select * from privileges Order by displayorder";
                DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int index;
                    for (index = 0; index < Dtcontent.Rows.Count; index++)
                    {
                        Int32 id = Convert.ToInt32(Dtcontent.Rows[index]["privilgeid"]);
                        Int32 displayorder = Convert.ToInt32(Dtcontent.Rows[index]["displayorder"]);
                        if (displayorder != (index + 1))
                        {
                            query = string.Format(" Update privileges SET displayorder=" + (index + 1) + " where privilgeid={0}", id);
                            Service.DbFunctions.UpdateCommand(query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Assigned privileges Functions

        // Fetch privileges By roleid and moduleid
        public DataTable Fetch_privileges_By_roleid(Int32 roleid, Int32 moduleid)
        {
            //DataTable dt = null;
            //try
            //{
            //    StringBuilder strsubmodules = new StringBuilder();
            //    strsubmodules.Append(" Select DISTINCT roleprivileges.moduleid innermoduleid,roleprivileges.submoduleid,submodulename,submodules.displayorder ");
            //    strsubmodules.Append(" , Case when isview=1 THEN 'Checked' else '' END isview  ");
            //    strsubmodules.Append(" , Case when isadd=1 THEN 'Checked' else '' END isadd  ");
            //    strsubmodules.Append(" , Case when isedit=1 THEN 'Checked' else '' END isedit  ");
            //    strsubmodules.Append(" , Case when isdelete=1 THEN 'Checked' else '' END isdelete  ");
            //    strsubmodules.Append(" from roleprivileges ");
            //    strsubmodules.Append(" INNER JOIN submodules  ON submodules.submoduleid=roleprivileges.submoduleid ");
            //    strsubmodules.Append(" where roleid="+roleid+" and roleprivileges.submoduleid<>0 and roleprivileges.moduleid=" + moduleid + " ");
            //    strsubmodules.Append(" UNION   ");
            //    strsubmodules.Append(" Select DISTINCT moduleid innermoduleid,submodules.submoduleid,submodulename,submodules.displayorder,'' isview,'' isadd,'' isedit,'' isdelete from submodules  ");
            //    strsubmodules.Append(" Where moduleid=" + moduleid + " and submoduleid NOT IN  ");
            //    strsubmodules.Append(" (Select DISTINCT submoduleid from roleprivileges where roleid="+roleid+" and moduleid=" + moduleid + " and submoduleid<>0)");

            //    DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
            //    if (ds != null && ds.Tables.Count > 0)
            //    {
            //        DataTable dtModule = ds.Tables[0];
            //        dt = SortTable(dtModule, "displayorder");
            //    }
            //    return dt;
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}

            DataTable dt = null;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Select DISTINCT roleprivileges.moduleid innermoduleid,roleprivileges.submoduleid,submodulename,submodules.displayorder ");
                strsubmodules.Append(" , Case when isview=True THEN 'Checked' else '' END isview  ");
                strsubmodules.Append(" , Case when isadd=True THEN 'Checked' else '' END isadd  ");
                strsubmodules.Append(" , Case when isedit=True THEN 'Checked' else '' END isedit  ");
                strsubmodules.Append(" , Case when isdelete=True THEN 'Checked' else '' END isdelete  ");
                strsubmodules.Append(" from roleprivileges ");
                strsubmodules.Append(" INNER JOIN submodules  ON submodules.submoduleid=roleprivileges.submoduleid ");
                strsubmodules.Append(" where roleid="+roleid+" and roleprivileges.submoduleid<>0 and roleprivileges.moduleid=" + moduleid + " ");
                strsubmodules.Append(" UNION   ");
                strsubmodules.Append(" Select DISTINCT moduleid innermoduleid,submodules.submoduleid,submodulename,submodules.displayorder,'' isview,'' isadd,'' isedit,'' isdelete from submodules  ");
                strsubmodules.Append(" Where moduleid=" + moduleid + " and submoduleid NOT IN  ");
                strsubmodules.Append(" (Select DISTINCT submoduleid from roleprivileges where roleid="+roleid+" and moduleid=" + moduleid + " and submoduleid<>0)");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtModule = ds.Tables[0];
                    dt = SortTable(dtModule, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public DataTable Fetch_privileges_By_RoleId_For_Other_Admin(Int32 roleid, Int32 moduleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" select * from (" );
                strsubmodules.Append(" Select DISTINCT roleprivileges.moduleid innermoduleid,roleprivileges.submoduleid,submodulename,submodules.displayorder ");
                strsubmodules.Append(" , Case when isview=True THEN 'Checked' else '' END isview  ");
                strsubmodules.Append(" , Case when isadd=True THEN 'Checked' else '' END isadd  ");
                strsubmodules.Append(" , Case when isedit=True THEN 'Checked' else '' END isedit  ");
                strsubmodules.Append(" , Case when isdelete=True THEN 'Checked' else '' END isdelete  ");
                strsubmodules.Append(" from roleprivileges ");
                strsubmodules.Append(" INNER JOIN submodules  ON submodules.submoduleid=roleprivileges.submoduleid ");
                strsubmodules.Append(" where roleid="+roleid+" and roleprivileges.submoduleid<>0 and roleprivileges.moduleid=" + moduleid + " ");
                strsubmodules.Append(" UNION   ");
                strsubmodules.Append(" Select DISTINCT moduleid innermoduleid,submodules.submoduleid,submodulename,submodules.displayorder,'' isview,'' isadd,'' isedit,'' isdelete from submodules  ");
                strsubmodules.Append(" Where moduleid=" + moduleid + " and submoduleid NOT IN  ");
                strsubmodules.Append(" (Select DISTINCT submoduleid from roleprivileges where roleid="+roleid+" and moduleid=" + moduleid + " and submoduleid<>0)");
                strsubmodules.Append(" ) roleprivileges  where isadd='Checked' or isview='Checked' or isedit='Checked' or isdelete='Checked'");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtModule = ds.Tables[0];
                    dt = SortTable(dtModule, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // To Fetch the Module List and privileges for the privileges Grid
        public DataTable Fetch_Modules_For_Grid(Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select DISTINCT roleprivileges.moduleid, modules.modulename,modules.displayorder, Case when isview=True THEN 'Checked' else '' END isview ");
                strmodules.Append(" , Case when isadd=True THEN 'Checked' else '' END isadd ");
                strmodules.Append(" , Case when isedit=True THEN 'Checked' else '' END isedit ");
                strmodules.Append(" , Case when isdelete=True THEN 'Checked' else '' END isdelete ");
                strmodules.Append("  from roleprivileges ");
                strmodules.Append("  INNER JOIN modules ON modules.moduleid=roleprivileges.moduleid ");
                strmodules.Append("  where roleid="+roleid+" and submoduleid=0 ");
                strmodules.Append("  UNION ");
                strmodules.Append("  Select DISTINCT modules.moduleid,modulename,modules.displayorder,'' isview,'' isadd,'' isedit,'' isdelete from modules Where modules.moduleid NOT IN( ");
                strmodules.Append("  Select DISTINCT moduleid from roleprivileges where roleid="+roleid+")");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtModule = ds.Tables[0];
                    dt = SortTable(dtModule, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch active Module By roleid
        public Int32 Fetch_Checked_modules_By_roleid(Int32 roleid)
        {
            Int32 RetVal = 0;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select Distinct roleid,roleprivileges.moduleid,modules.modulename from roleprivileges  ");
                strRolePrivilege.Append(" Inner Join modules ON modules.moduleid= roleprivileges.moduleid  and roleid="+roleid+" ");
                strRolePrivilege.Append("  where roleprivileges.isview=True or roleprivileges.isadd=True or roleprivileges.isdelete=True or roleprivileges.isedit=True");
                object ObjCount = Service.DbFunctions.SelectCount(strRolePrivilege.ToString());
                if (ObjCount != null)
                {
                    RetVal = Convert.ToInt32(ObjCount);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Count If All modules isactive, isview,isadd,isdelete Checked or Not
        public Int32 Fetch_CheckedAll_modules_By_roleid(Int32 roleid)
        {
            Int32 retval = 0;
            object ObjCount = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append("  Select COUNT(*) from roleprivileges where roleid="+roleid+"");
                ObjCount = Service.DbFunctions.SelectCount(strRolePrivilege.ToString());
                if (ObjCount != null)
                {
                    retval = Convert.ToInt32(ObjCount);
                    if (retval > 0)
                    {
                        strRolePrivilege = new StringBuilder();
                        strRolePrivilege.Append("   Select COUNT(*) from roleprivileges where roleid="+roleid+" and (isview=False or isedit=False or isadd=False or isdelete=False)");
                        ObjCount = Service.DbFunctions.SelectCount(strRolePrivilege.ToString());
                        if (ObjCount != null)
                        {
                            retval = Convert.ToInt32(ObjCount);
                        }
                    }
                    else
                    {
                        retval = 1;
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TO Insert Roles & privileges
        public string Insert_Roles_privileges(string roleid, string PrivList)
        {
            List<string> lstQuery = new List<string>();
            StringBuilder strQuery = new StringBuilder();
            try
            {
                string[] PrivArray = PrivList.Split(',');
                if (PrivArray.Length > 1)
                {
                    strQuery.Append(" Delete from roleprivileges where roleid ="+roleid+"");
                    lstQuery.Add(strQuery.ToString());
                    for (Int32 i = 0; i < PrivArray.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(PrivArray[i]))
                        {
                            string[] queryArray = Convert.ToString(PrivArray[i]).Split('~');
                            if (queryArray.Length > 1)
                            {
                                if (!string.IsNullOrEmpty(roleid) && Convert.ToInt32(roleid) == 1)
                                {
                                    strQuery = new StringBuilder();
                                    strQuery.Append(" Insert into roleprivileges(roleid,moduleid,submoduleid,isview,isadd,isedit,isdelete) ");
                                    strQuery.Append(" Values(" );
                                    strQuery.Append(queryArray[0]);
                                    strQuery.Append(" ,");
                                    strQuery.Append(queryArray[1]);
                                    strQuery.Append(" ,");
                                    strQuery.Append(queryArray[2]);
                                    strQuery.Append(" ,");
                                    strQuery.Append(" '");
                                    strQuery.Append(queryArray[3]);
                                    strQuery.Append(" '");
                                    strQuery.Append(" ,");
                                    strQuery.Append(" '");
                                    strQuery.Append(queryArray[4]);
                                    strQuery.Append(" '");
                                    strQuery.Append(" ,");
                                    strQuery.Append(" '");
                                    strQuery.Append(queryArray[5]);
                                    strQuery.Append(" '");
                                    strQuery.Append(" ,");
                                    strQuery.Append(" '");
                                    strQuery.Append(queryArray[6]);
                                    strQuery.Append(" '");
                                    strQuery.Append(" )");
                                    lstQuery.Add(strQuery.ToString());
                                }
                                else
                                {
                                    DataTable dtMasterPriv = GetParent_privileges(roleid, queryArray[1], queryArray[2]);
                                    if (dtMasterPriv != null && dtMasterPriv.Rows.Count > 0)
                                    {
                                        string Curisview = "0";
                                        string Curisadd = "0";
                                        string Curisedit = "0";
                                        string Curisdelete = "0";

                                        string Childisview = queryArray[3];
                                        string Childisadd = queryArray[4];
                                        string Childisedit = queryArray[5];
                                        string Childisdelete = queryArray[6];

                                        string Parentisview = Convert.ToString(dtMasterPriv.Rows[0]["isview"]);
                                        string Parentisadd = Convert.ToString(dtMasterPriv.Rows[0]["isadd"]);
                                        string Parentisedit = Convert.ToString(dtMasterPriv.Rows[0]["isedit"]);
                                        string Parentisdelete = Convert.ToString(dtMasterPriv.Rows[0]["isdelete"]);
                                        if (!string.IsNullOrEmpty(Parentisview) && (Parentisview.Equals(" 1") || Parentisview.Equals(" True")))
                                            Curisview = Childisview;
                                        else
                                            Curisview = "0";

                                        if (!string.IsNullOrEmpty(Parentisadd) && (Parentisadd.Equals(" 1") || Parentisadd.Equals(" True")))
                                            Curisadd = Childisadd;
                                        else
                                            Curisadd = "0";

                                        if (!string.IsNullOrEmpty(Parentisedit) && (Parentisedit.Equals(" 1") || Parentisedit.Equals(" True")))
                                            Curisedit = Childisedit;
                                        else
                                            Curisedit = "0";

                                        if (!string.IsNullOrEmpty(Parentisdelete) && (Parentisdelete.Equals(" 1") || Parentisdelete.Equals(" True")))
                                            Curisdelete = Childisdelete;
                                        else
                                            Curisedit = "0";

                                        strQuery = new StringBuilder();
                                        strQuery.Append(" Insert into roleprivileges(roleid,moduleid,submoduleid,isview,isadd,isedit,isdelete) ");
                                        strQuery.Append(" Values(" );
                                        strQuery.Append(queryArray[0]);
                                        strQuery.Append(" ,");
                                        strQuery.Append(queryArray[1]);
                                        strQuery.Append(" ,");
                                        strQuery.Append(queryArray[2]);
                                        strQuery.Append(" ,");
                                        strQuery.Append(" '");
                                        strQuery.Append(Curisview);
                                        strQuery.Append(" '");
                                        strQuery.Append(" ,");
                                        strQuery.Append(" '");
                                        strQuery.Append(Curisadd);
                                        strQuery.Append(" '");
                                        strQuery.Append(" ,");
                                        strQuery.Append(" '");
                                        strQuery.Append(Curisedit);
                                        strQuery.Append(" '");
                                        strQuery.Append(" ,");
                                        strQuery.Append(" '");
                                        strQuery.Append(Curisdelete);
                                        strQuery.Append(" '");
                                        strQuery.Append(" )");
                                        lstQuery.Add(strQuery.ToString());
                                    }
                                }
                            }

                        }
                    }
                    if (lstQuery.Count > 1)
                    {
                        Service.DbFunctions.Execute_Multiple_Query(lstQuery);
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable GetParent_privileges(string roleid, string moduleid, string submoduleid)
        {
            if (submoduleid == "79")
            {
            }
            DataTable dt = null;
            try
            {
                StringBuilder strMasterRoles = new StringBuilder();
                strMasterRoles.Append(" Select * from roleprivileges where moduleid=" + moduleid + " and submoduleid=" + submoduleid + " and roleid=(Select parentid from userroles where roleid="+roleid+")");
                DataSet ds = Service.DbFunctions.SelectCommand(strMasterRoles.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Content Pages Functions

        // Fetch cmsid for active content page



        //Fetch Content Page Details
        public DataTable Fetch_ContentPage()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select pageid,pagename,pagedescription,to_char(expirationdate, 'DD/MM/YYYY') expirationdate,isactive, to_char(creationdate, 'DD/MM/YYYY') creationdate  from content_pages order by pageid desc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }
        //Fetch for FrontEndContentPage Details
        public DataTable Fetch_Content_For_FrontEndContentPage()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                //strFetchData.Append(" Select pageid,pagename,pagedescription,Convert(varchar,content_pages.expirationdate,101)expirationdate,isactive, Convert(varchar,content_pages.creationdate,101) creationdate from content_pages order by pageid desc");
                strFetchData.Append(" Select pageid,pagename,pagedescription,isactive activeStatus,to_char(content_pages.expirationdate, 'DD/MM/YYYY') expirationdate");
                strFetchData.Append(" ,Case When isactive=True then 'checked' else '' END isactive, to_char(content_pages.creationdate, 'DD/MM/YYYY')  creationdate from content_pages order by pageid desc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }
        // Fetch  Page name whose Status is active
        public DataTable Fetch_Page_name()
        {

            DataTable RetVal = null;
            try
            {
                StringBuilder strpagename = new StringBuilder();
                strpagename.Append(" Select pageid,pagename from content_pages where isactive=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strpagename.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtModule = ds.Tables[0];
                    RetVal = SortTable(dtModule, "pageid");
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //isactive Update
        public Int32 Update_isactive_ContentPage_By_pageid(Int32 pageid, bool isactive)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update content_pages set isactive='" + isactive + "' where pageid=" + pageid + "");
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Fetch Content Page Master Detail By Page id
        public DataTable Fetch_Page_Master_Data_By_pageid(string pageid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetch = new StringBuilder();
                strFetch.Append(" SELECT pagename,pagedescription,to_char(expirationdate, 'DD/MM/YYYY')expirationdate,isactive,ismultipage from content_pages WHERE pageid = '" + pageid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetch.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Fetch Content Page Detail Detail By Page id
        public DataTable Fetch_Page_Detail_Data_By_pageid(string pageid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUsers = new StringBuilder();
                strUsers.Append(" SELECT * from  content_pages_detail WHERE pageid = '" + pageid + "'  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strUsers.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtContent = ds.Tables[0];
                    dt = SortTable(dtContent, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Insert new  Content Page Master
        public Int32 Insert_ContentPage(Cls_ContentPages contentPage)
        {
            Int32 retVal = 0;

            try
            {
                //Int32 retval = Delete_Content_Page_Byid(contentPage.pageid);

                StringBuilder strcontentPage = new StringBuilder();
                if (Convert.ToInt32(contentPage.pageid) > 0)
                {
                    strcontentPage.Append(" Update content_pages Set ");
                    strcontentPage.Append(" pagedescription=");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.pagedescription.Replace(" '", "''"));
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" expirationdate=");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.expirationdate);
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" isactive=");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.isactive);
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" ismultipage=");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.ismultipage);
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" lastmodifiedby=");
                    strcontentPage.Append("'");
                    strcontentPage.Append(contentPage.createdby);
                    strcontentPage.Append("'");
                    strcontentPage.Append(",");
                    strcontentPage.Append(" lastmodificationdate=");
                    strcontentPage.Append(" LOCALTIMESTAMP ");
                    strcontentPage.Append("  where pageid=" + contentPage.pageid + "");
                    retVal = Service.DbFunctions.UpdateCommand(strcontentPage.ToString());
                    if (retVal > 0)
                    {
                        retVal = Convert.ToInt32(contentPage.pageid);
                    }
                }
                else
                {
                    strcontentPage.Append(" Insert into content_pages(pagename,pagedescription,expirationdate,isactive,ismultipage,createdby,creationdate) ");
                    strcontentPage.Append(" Values(" );
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.pagename.Replace(" '", "''"));
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.pagedescription.Replace(" '", "''"));
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.expirationdate);
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.isactive);
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.ismultipage);
                    strcontentPage.Append(" '");
                    strcontentPage.Append(" ,");
                    strcontentPage.Append(" '");
                    strcontentPage.Append(contentPage.createdby);
                    strcontentPage.Append("'");
                    strcontentPage.Append(",");
                    strcontentPage.Append(" LOCALTIMESTAMP");
                    strcontentPage.Append(" )");
                    retVal = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // To Insert Content Detail Page
        public Int32 Insert_ContentPage_Detail(Cls_ContentPage_Detail contentPage)
        {
            Int32 retVal = 0;
            try
            {

                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into content_pages_detail");
                strcontentPage.Append(" (" );
                strcontentPage.Append(" pageid,filedtype,dorder,textfieldheading1,textfieldheadingvalue1,textfieldheading2,textfieldheadingvalue2");
                strcontentPage.Append(" ,textfieldheading3,textfieldheadingvalue3,textfieldheading4,textfieldheadingvalue4,textfieldheading5,textfieldheadingvalue5,textfieldheading6");
                strcontentPage.Append(" ,textfieldheadingvalue6,textfieldheading7,textfieldheadingvalue7,textfieldheading8,textfieldheadingvalue8,textfieldheading9,textfieldheadingvalue9");
                strcontentPage.Append(" ,textfieldheading10,textfieldheadingvalue10,textfieldheading11,textfieldheadingvalue11,textfieldheading12,textfieldheadingvalue12,textfieldheading13");
                strcontentPage.Append(" ,textfieldheadingvalue13,textfieldheading14,textfieldheadingvalue14,textfieldheading15,textfieldheadingvalue15,textfieldheading16,textfieldheadingvalue16");
                strcontentPage.Append(" ,textfieldheading17,textfieldheadingvalue17,textfieldheading18,textfieldheadingvalue18,textfieldheading19,textfieldheadingvalue19,textfieldheading20");
                strcontentPage.Append(" ,textfieldheadingvalue20,textfieldheading21,textfieldheadingvalue21,textfieldheading22,textfieldheadingvalue22,textfieldheading23,textfieldheadingvalue23");
                strcontentPage.Append(" ,textfieldheading24,textfieldheadingvalue24,textfieldheading25,textfieldheadingvalue25,textfieldheading26,textfieldheadingvalue26,textfieldheading27");
                strcontentPage.Append(" ,textfieldheadingvalue27,textfieldheading28,textfieldheadingvalue28,textfieldheading29,textfieldheadingvalue29,textfieldheading30,textfieldheadingvalue30");
                strcontentPage.Append(" ) ");
                strcontentPage.Append(" Values");
                strcontentPage.Append(" (" );
                strcontentPage.Append(contentPage.pageid);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.filedtype.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(contentPage.dorder);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading3.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue3.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading4.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue4.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading5.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue5.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading6.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue6.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading7.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue7.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading8.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue8.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading9.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue9.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading10.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue10.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading11.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue11.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading12.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue12.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading13.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue13.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading14.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue14.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading15.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue15.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading16.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue16.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading17.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue17.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading18.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue18.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading19.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue19.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading20.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue20.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading21.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue21.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading22.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue22.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading23.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue23.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading24.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue24.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading25.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue25.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading26.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue26.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading27.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue27.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading28.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue28.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading29.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue29.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheading30.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(contentPage.textfieldheadingvalue30.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strcontentPage.ToString());
                return retVal;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Update Existing Content Page By pageid
        public Int32 Update_Content_Page_Byid(string pageid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from content_pages_detail WHERE pageid = '" + pageid + "'");
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // Delete Content Page By pageid
        public Int32 Delete_Content_Page_Byid(string pageid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" EXEC [spCheckDependencyBeforeDelete] 'pageid','content_pages,content_pages_detail,','" + pageid + "'");
                object objCount = Service.DbFunctions.SelectCount(strDelete.ToString());
                if (objCount != null)
                {
                    retVal = Convert.ToInt32(objCount);
                }
                if (retVal == 0)
                {
                    strDelete = new StringBuilder();
                    strDelete.Append(" Delete from content_pages_detail WHERE pageid = '" + pageid + "'");
                    retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                    strDelete = new StringBuilder();
                    strDelete.Append(" Delete from content_pages WHERE pageid = '" + pageid + "'");
                    retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                }
                else
                {
                    retVal = 9999;
                }
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //Check for Existing Page
        public Int32 Check_Existing_pagename(string pagename)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strpagename = new StringBuilder();
                strpagename.Append(" Select COUNT(*)  from content_pages where pagename='" + pagename + "' ");
                object objCount = Service.DbFunctions.SelectCount(strpagename.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Content management Page Function
        // Get the pageid by pagename
        public string Fetch_Page_id_By_name(string pagename)
        {
            string pageid = "";
            try
            {
                StringBuilder strpageid = new StringBuilder();
                strpageid.Append(" Select pageid from content_pages where pagename='" + pagename.Trim().Replace(" '", "''") + "'");
                object objpageid = Service.DbFunctions.SelectCount(strpageid.ToString());
                if (objpageid != null)
                {
                    pageid = Convert.ToString(objpageid);
                }
                return pageid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch Page name By pageid
        public string Fetch_pagename_By_pageid(Int32 pageid)
        {
            string pagename = "";
            try
            {
                StringBuilder strpageid = new StringBuilder();
                strpageid.Append(" Select pagename from content_pages where pageid='" + pageid + "'");
                DataSet Ds = Service.DbFunctions.SelectCommand(strpageid.ToString());
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    DataTable dt = Ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        pagename = Convert.ToString(dt.Rows[0][0]);
                    }
                }
                return pagename;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch Page name By pageid
        public DataTable Get_cmsid_by_Projectcategoryname(Int32 pageid, string Projectcategory)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and  fieldtype='Dropdown' and fieldvalue='" + Projectcategory + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check if the page is drafted or not
        public bool Is_Content_Page_Drafted(Int32 pageid, Int32 cmsid)
        {
            bool isDraft = false;
            try
            {
                if (Service.DbFunctions.SelectCount(" Select COALESCE(COUNT(*),0) from content_management_master_draft  where cmsid=" + cmsid + " and pageid=" + pageid + "") > 0)
                {
                    isDraft = true;
                }
                return isDraft;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fill Data into the Content management Page
        public string Fetch_ContentValue_By_cmsid(Int32 cmsid, Int32 dorder)
        {
            string retval = "";
            try
            {

                StringBuilder strdata = new StringBuilder();
                if (Service.DbFunctions.SelectCount(" Select COUNT(*) from content_management_detail_draft where cmsid=" + cmsid + "") > 0)
                {
                    strdata.Append(" Select COALESCE(fieldvalue,'') fieldvalue from content_management_detail_draft where cmsid=" + cmsid + " and dorder=" + dorder + "");
                }
                else
                {
                    strdata.Append(" Select COALESCE(fieldvalue,'') fieldvalue from content_management_detail where cmsid=" + cmsid + " and dorder=" + dorder + "");
                }
                DataSet ds = Service.DbFunctions.SelectCommand(strdata.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        retval = Convert.ToString(dt.Rows[0][0]);
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fill Data into the Content management Page for checked value
        public DataTable Fetch_ContentValue_Checked_By_cmsid(Int32 cmsid, Int32 dorder)
        {
            DataTable retval = null;
            try
            {
                bool isDrafted = false;
                StringBuilder strdata = new StringBuilder();
                if (Service.DbFunctions.SelectCount(" Select COUNT(*) from content_management_detail_draft where cmsid=" + cmsid + "") > 0)
                {
                    isDrafted = true;
                    strdata.Append(" Select COALESCE(fieldvalue,'') fieldvalue,DraftDetailid,fieldtext from content_management_detail_draft where cmsid=" + cmsid + " and dorder=" + dorder + "");
                }
                else
                {
                    strdata.Append(" Select COALESCE(fieldvalue,'') fieldvalue,cmsdetailid,fieldtext from content_management_detail where cmsid=" + cmsid + " and dorder=" + dorder + "");
                }
                DataTable dt = Service.DbFunctions.SelectCommand_DataTable(strdata.ToString());
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (isDrafted)
                    {
                        retval = SortTable(dt, "DraftDetailid");
                    }
                    else
                    {
                        retval = SortTable(dt, "cmsdetailid");
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TO Fetch the Content Title
        public string Fetch_Content_Title(Int32 cmsid)
        {
            string retval = "";
            try
            {
                StringBuilder strTitle = new StringBuilder();
                if (Service.DbFunctions.SelectCount(" Select COUNT(*) from content_management_detail_draft where cmsid=" + cmsid + "") > 0)
                {
                    strTitle.Append(" Select fieldvalue from content_management_detail_draft where DraftDetailid=(Select MIN(DraftDetailid) from content_management_detail_draft where cmsid=" + cmsid + ")");
                }
                else
                {
                    strTitle.Append(" Select fieldvalue from content_management_detail where cmsdetailid=(Select MIN(cmsdetailid) from content_management_detail where cmsid=" + cmsid + ")");
                }
                retval = Convert.ToString(Service.DbFunctions.SelectCountString(strTitle.ToString()));
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TO Fetch the Content Title Special Page
        public string Fetch_Content_Title_SpecialPage(Int32 cmsid)
        {
            string retval = "";
            try
            {
                StringBuilder strTitle = new StringBuilder();
                if (Service.DbFunctions.SelectCount(" Select COUNT(*) from content_management_detail_draft where cmsid=" + cmsid + "") > 0)
                {
                    strTitle.Append(" Select fieldvalue from content_management_detail_draft where fieldtext='Button Description' and cmsid=" + cmsid + "");
                }
                else
                {
                    strTitle.Append(" Select fieldvalue from content_management_detail where fieldtext='Button Description' and cmsid=" + cmsid + "");
                }
                retval = Convert.ToString(Service.DbFunctions.SelectCountString(strTitle.ToString()));
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //FETCH Detail Page by CSMid
        public DataTable Fetch_Page_Detail_ByContentid(Int32 CSMid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strCSMid = new StringBuilder();
                strCSMid.Append(" SELECT * from  content_management_detail WHERE cmsid = " + CSMid);
                DataSet ds = Service.DbFunctions.SelectCommand(strCSMid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    dt = SortTable(dtCMS, "cmsdetailid");
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // To get the details for a control
        public DataTable Fetch_Page_Data_By_fieldtype(string pageid, string fieldtype, string dorder)
        {
            try
            {
                DataTable dt = null;
                StringBuilder strData = new StringBuilder();
                strData.Append(" Select * from content_pages_detail where pageid=" + pageid + " and filedtype='" + fieldtype.Trim().Replace(" '", "''") + "' and dorder=" + dorder + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch Dropdown itemdata from Table
        public DataTable Fetch_ItemValue_ForDropDown(string pagename)
        {
            DataTable dtRetVal = new DataTable();
            dtRetVal.Columns.Add(" fieldvalue", typeof(string));
            DataTable dtCMS = null;
            try
            {
                List<string> objQueryList = new List<string>();
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" SELECT q2.cmsid,cast(q2.fieldvalue as int) dorder FROM( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pagename='" + pagename + "' and fieldtype='Checkbox' and fieldvalue='True'");
                strCMS.Append("   )  q1 LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pagename='" + pagename + "' and fieldtext like '%Display Order%') q2");
                strCMS.Append("   ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                for (Int32 i = 0; i < dtCMS.Rows.Count; i++)
                {
                    strCMS = new StringBuilder();
                    strCMS.Append(" Select fieldvalue from content_management_detail where cmsdetailid =(Select MIN(cmsdetailid) from content_management_detail where cmsid=" + dtCMS.Rows[i]["cmsid"] + ")");
                    DataSet dsmenu = Service.DbFunctions.SelectCommand(strCMS.ToString());
                    if (dsmenu != null && dsmenu.Tables.Count > 0)
                    {
                        dtRetVal.Rows.Add(Convert.ToString(dsmenu.Tables[0].Rows[0][0]));
                    }
                }
                return dtRetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch Dropdown itemdata from Table for Catalouge
        public DataTable Fetch_ItemValue_ForDropDown_by_fieldtext(string pagename,string fieldtext)
        {
            DataTable dtRetVal = new DataTable();
            dtRetVal.Columns.Add(" fieldvalue", typeof(string));
            DataTable dtCMS = null;
            try
            {
                List<string> objQueryList = new List<string>();
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" SELECT distinct q2.cmsid,cast(q2.fieldvalue as int) dorder FROM( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pagename='" + pagename + "' and fieldtype='Checkbox' and fieldvalue='True'");
                strCMS.Append("   )  q1 LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pagename='" + pagename + "' and fieldtext like '%Display Order%') q2");
                strCMS.Append("   ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                for (Int32 i = 0; i < dtCMS.Rows.Count; i++)
                {
                    strCMS = new StringBuilder();
                    strCMS.Append(" Select fieldvalue from content_management_detail where cmsdetailid =(Select MIN(cmsdetailid) from content_management_detail where cmsid=" + dtCMS.Rows[i]["cmsid"] + " and fieldtext='"+fieldtext+"')");
                    DataSet dsmenu = Service.DbFunctions.SelectCommand(strCMS.ToString());
                    if (dsmenu != null && dsmenu.Tables.Count > 0)
                    {
                        dtRetVal.Rows.Add(Convert.ToString(dsmenu.Tables[0].Rows[0][0]));
                    }
                }
                return dtRetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch Dropdown itemdata from Table for special page
        public DataTable Fetch_ItemValue_ForDropDown_forSpecialPage(string pagename)
        {
            DataTable dtRetVal = new DataTable();
            dtRetVal.Columns.Add(" fieldvalue", typeof(string));
            DataTable dtCMS = null;
            try
            {
                List<string> objQueryList = new List<string>();
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" SELECT q2.cmsid,cast(q2.fieldvalue as int) dorder FROM( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pagename='" + pagename + "' and fieldtype='Checkbox' and fieldvalue='True'");
                strCMS.Append("   )  q1 LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pagename='" + pagename + "' and fieldtext like '%Display Order%') q2");
                strCMS.Append("   ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                for (Int32 i = 0; i < dtCMS.Rows.Count; i++)
                {
                    strCMS = new StringBuilder();
                    strCMS.Append(" Select fieldvalue from content_management_detail where fieldtext='Button Description' and cmsid=" + dtCMS.Rows[i]["cmsid"] + "");
                    DataSet dsmenu = Service.DbFunctions.SelectCommand(strCMS.ToString());
                    if (dsmenu != null && dsmenu.Tables.Count > 0)
                    {
                        dtRetVal.Rows.Add(Convert.ToString(dsmenu.Tables[0].Rows[0][0]));
                    }
                }
                return dtRetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Get max  dorder of menu level 2 by menu name 
        public Int32 Get_Max_Content_dorder_ByMenuname_Level2(Int32 pageid, string MainMenuname, string SubMenuname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append("  Select COALESCE(MAX(cast(q1.fieldvalue as int)),0) as MaxOrder from ( ");
                strMax.Append("  Select fieldvalue,content_management_detail.cmsid from content_management_detail    ");
                strMax.Append("  inner join content_management_master on content_management_master.cmsid=content_management_detail.cmsid where pageid=" + pageid + " and (fieldtext Like '%Display%') ) q1 ");
                strMax.Append("  left join (Select content_management_detail.cmsid,fieldvalue,fieldtype from content_management_detail    ");
                strMax.Append("  where cmsid in( Select content_management_detail.cmsid from content_management_detail    ");
                strMax.Append("  inner join content_management_master on content_management_master.cmsid=content_management_detail.cmsid where pageid=" + pageid + "   ");
                strMax.Append("  and fieldtext ='Sub Menu(Level-1)' and fieldvalue='" + SubMenuname + "') ) q2  on q1.cmsid=q2.cmsid  where q2.fieldvalue='" + MainMenuname + "' and q2.fieldtype like 'DropDown' ");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Bind Content management Dashboard
        public DataTable Fetch_CMS_Dashboard(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select content_management_master.cmsid,content_management_master.pageid,content_management_master.pagename,'' displayorder  ");
                strCMS.Append("  ,to_char(content_management_master.creationdate, 'DD/MM/YYYY') creationdate,content_pages.isactive,content_pages.ismultipage ");
                strCMS.Append("  ,Case WHEN RIGHT(to_char(content_pages.expirationdate, 'DD/MM/YYYY'),4)='1900' THEN '' ELSE to_char(content_pages.expirationdate, 'DD/MM/YYYY') END expirationdate from content_management_master ");
                strCMS.Append("  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  where content_management_master.pageid=" + pageid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch Catelouge CMS Detail
        public DataTable Fetch_Catalouge_Dashboard(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q1.cmsid ,q1.pageid,q1.pagename,q5.category,q4.isactive,q1.ismultipage,q1.fieldtext,cast(q1.displayorder as int) displayorder,q2.fieldtext,q2.imagename,q3.description,q6.internaldescription,q7.productnumber,q8.Vendorproductnumber,q9.Vendorname,q10.DetailDeascription,q11.productname,q1.creationdate FROM  ");
                strCMS.Append("  (Select content_management_master.cmsid,content_management_master.pageid,content_management_master.pagename,");
                strCMS.Append("    content_management_detail.fieldtext,content_management_detail.fieldvalue as displayorder  ,  ");
                strCMS.Append(" to_char(content_management_master.creationdate, 'DD/MM/YYYY') creationdate,content_pages.isactive,content_pages.ismultipage");
                strCMS.Append("   from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("    INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid ");
                strCMS.Append("  where content_management_master.pageid=" + pageid + " and  fieldtext like '%Display%' )  q1 ");
                strCMS.Append("    LEFT JOIN ");
                strCMS.Append("    (Select content_management_master.cmsid,content_management_detail.fieldtext,content_management_detail.fieldvalue as imagename ");
                strCMS.Append("   from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("   INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid ");
                strCMS.Append("   where content_management_master.pageid=" + pageid + "  and  fieldtext like '%Upload%') q2 ");
                strCMS.Append("   ON q1.cmsid = q2.cmsid ");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_master.cmsid,");
                strCMS.Append("  content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) as description ");
                strCMS.Append("  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where content_management_master.pageid=" + pageid + "  and  fieldtext = 'Description') q3 ");
                strCMS.Append("  ON q3.cmsid = q2.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_master.cmsid,");
                strCMS.Append("  content_management_detail.fieldtext,content_management_detail.fieldvalue as isactive ");
                strCMS.Append("  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where content_management_master.pageid=" + pageid + "  and  fieldtext like '%Is Active%' and fieldtype='Checkbox' ) q4 ");
                strCMS.Append("  ON q4.cmsid = q3.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_master.cmsid,");
                strCMS.Append("  content_management_detail.fieldtext,content_management_detail.fieldvalue as category ");
                strCMS.Append("  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where content_management_master.pageid=" + pageid + "  and  fieldtype like '%Dropdown%'  and fieldvalue not in('Public','Internal') ) q5 ");
                strCMS.Append("  ON q4.cmsid = q5.cmsid");
                strCMS.Append("  LEFT JOIN (Select content_management_master.cmsid, content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) ");
                strCMS.Append("  as internaldescription  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid  ");
                strCMS.Append("   INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where content_management_master.pageid=66  ");
                strCMS.Append("  and  fieldtext = 'Internal Description') q6  ON q5.cmsid = q6.cmsid ");
                strCMS.Append("  LEFT JOIN ");
                strCMS.Append("  (Select content_management_master.cmsid, content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) ");
                strCMS.Append("  as productnumber  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where ");
                strCMS.Append("  content_management_master.pageid=66   and  fieldtext = 'Product Number') q7  ON q6.cmsid = q7.cmsid ");
                strCMS.Append("  LEFT JOIN ");
                strCMS.Append("   (Select content_management_master.cmsid, content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) ");
                strCMS.Append("   as Vendorproductnumber  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where ");
                strCMS.Append("   content_management_master.pageid=66   and  fieldtext = 'Vendor (Cross Reference) Product Number') q8  ON q7.cmsid = q8.cmsid ");
                strCMS.Append("  LEFT JOIN ");
                strCMS.Append("  (Select content_management_master.cmsid, content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) ");
                strCMS.Append("  as Vendorname  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where ");
                strCMS.Append("  content_management_master.pageid=66   and  fieldtext = 'Vendor Name') q9  ON q8.cmsid = q9.cmsid ");
                strCMS.Append("  LEFT JOIN ");
                strCMS.Append("  (Select content_management_master.cmsid, content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) ");
                strCMS.Append("  as DetailDeascription  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where ");
                strCMS.Append("  content_management_master.pageid=66   and  fieldtext = 'Detail Description') q10  ON q9.cmsid = q10.cmsid ");
                strCMS.Append("  LEFT JOIN ");
                strCMS.Append("  (Select content_management_master.cmsid, content_management_detail.fieldtext,left(content_management_detail.fieldvalue,100) ");
                strCMS.Append("  as productname  from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where ");
                strCMS.Append("  content_management_master.pageid=66   and  fieldtext = 'Product Name') q11  ON q10.cmsid = q11.cmsid ");
                strCMS.Append(" ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "displayorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // get Content Modified by roleid
        public string Get_Content_modifiedby_pageidAndcmsid(Int32 pageid, Int32 cmsid)
        {
            string retval = "";
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append(" Select lastmodifiedby from content_management_master_draft where pageid=" + pageid + " and cmsid=" + cmsid + "");
                string roleid = Service.DbFunctions.SelectString(str.ToString());
                if (roleid != null)
                {
                    retval = roleid;
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Bind Content management Dashboard
        public DataTable Fetch_CMS_Dashboard_Fordisplayorder(Int32 pageid, string Fieldname)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                if (Fieldname.ToUpper() == "DISPLAY")
                {
                    strCMS.Append(" Select content_management_master.cmsid,content_management_master.pageid,content_management_master.pagename,content_management_detail.fieldtext ");
                    strCMS.Append("  ,cast(content_management_detail.fieldvalue as int) displayorder,content_pages.isactive,content_pages.ismultipage  ");
                    strCMS.Append("  ,Case WHEN RIGHT(to_char(content_pages.expirationdate, 'DD/MM/YYYY'),4)='1900' THEN '' ELSE to_char(content_pages.expirationdate, 'DD/MM/YYYY') END expirationdate  ");
                    strCMS.Append("  from content_management_master   ");
                    strCMS.Append("  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid   ");
                    strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid  ");
                    strCMS.Append("  where content_management_master.pageid=" + pageid + " and  fieldtext like '%" + Fieldname + "%' ");
                    strCMS.Append("  UNION  ");
                    strCMS.Append("  Select content_management_master_draft.cmsid,content_management_master_draft.pageid,content_management_master_draft.pagename,content_management_detail_draft.fieldtext ");
                    strCMS.Append("  ,cast(content_management_detail_draft.fieldvalue as int) displayorder   ");
                    strCMS.Append("  ,content_pages.isactive,content_pages.ismultipage  ");
                    strCMS.Append("  ,Case WHEN RIGHT(to_char(content_pages.expirationdate, 'DD/MM/YYYY'),4)='1900' THEN '' ELSE to_char(content_pages.expirationdate, 'DD/MM/YYYY') END expirationdate  ");
                    strCMS.Append("  from content_management_master_draft ");
                    strCMS.Append("  INNER JOIN content_pages ON content_pages.pageid=content_management_master_draft.pageid  ");
                    strCMS.Append("  INNER JOIN content_management_detail_draft on content_management_master_draft.cmsid=content_management_detail_draft.cmsid  ");
                    strCMS.Append("  where content_management_master_draft.pageid=" + pageid + " and  fieldtext like '%" + Fieldname + "%' ");
                }
                else
                {
                    strCMS.Append(" Select q1.cmsid,q1.pageid,q1.isactive,q1.expirationdate,q1.ismultipage,q1.creationdate,q1.fieldvalue pagename,cast(q2.fieldvalue as int) displayorder from ");
                    strCMS.Append("  (Select content_management_master.cmsid,content_management_master.pageid,content_management_master.pagename,content_management_detail.fieldvalue ,content_management_detail.fieldtext  , ");
                    strCMS.Append("  content_pages.isactive,content_pages.ismultipage  ,");
                    strCMS.Append("  Case WHEN RIGHT(to_char(content_pages.expirationdate, 'DD/MM/YYYY'),4)='1900' THEN '' ELSE to_char(content_pages.expirationdate, 'DD/MM/YYYY') END expirationdate from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid   INNER JOIN content_management_detail on  ");
                    strCMS.Append("  content_management_master.cmsid=content_management_detail.cmsid where content_management_master.pageid=" + pageid + " and  fieldtype like '%Dropdown%' )q1 ");
                    strCMS.Append("   left join(" );
                    strCMS.Append("  Select content_management_master.cmsid,");
                    strCMS.Append("  content_management_detail.fieldvalue from content_management_master  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                    strCMS.Append("  INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid where content_management_master.pageid=" + pageid + " and  fieldtext like '%Display%' )q2");
                    strCMS.Append("  on q1.cmsid=q2.cmsid");
                    strCMS.Append("  UNION ");
                    strCMS.Append(" Select q1.cmsid,q1.pageid,q1.isactive,q1.expirationdate,q1.ismultipage,q1.creationdate,q1.fieldvalue pagename,cast(q2.fieldvalue as int) displayorder from ");
                    strCMS.Append("  (Select content_management_master_draft.cmsid,content_management_master_draft.pageid,content_management_master_draft.pagename,content_management_detail_draft.fieldvalue ,content_management_detail_draft.fieldtext  , ");
                    strCMS.Append("  content_pages.isactive,content_pages.ismultipage  ,");
                    strCMS.Append("  Case WHEN RIGHT(to_char(content_pages.expirationdate, 'DD/MM/YYYY'),4)='1900' THEN '' ELSE to_char(content_pages.expirationdate, 'DD/MM/YYYY')  END expirationdate from content_management_master_draft  INNER JOIN content_pages ON content_pages.pageid=content_management_master_draft.pageid   INNER JOIN content_management_detail_draft on  ");
                    strCMS.Append("  content_management_master_draft.cmsid=content_management_detail_draft.cmsid where content_management_master_draft.pageid=" + pageid + " and  fieldtype like '%Dropdown%' )q1 ");
                    strCMS.Append("   left join(" );
                    strCMS.Append("  Select content_management_master_draft.cmsid,");
                    strCMS.Append("  content_management_detail_draft.fieldvalue from content_management_master_draft  INNER JOIN content_pages ON content_pages.pageid=content_management_master_draft.pageid ");
                    strCMS.Append("  INNER JOIN content_management_detail_draft on content_management_master_draft.cmsid=content_management_detail_draft.cmsid where content_management_master_draft.pageid=" + pageid + " and  fieldtext like '%Display%' )q2");
                    strCMS.Append("  on q1.cmsid=q2.cmsid");
                }
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "displayorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // To Bind Content management Dashboard
        public DataTable Fetch_CMS_Dashboard_Fordisplayorder(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select content_management_master.cmsid,content_management_master.pageid,content_management_master.pagename,content_management_detail.fieldtext,cast(content_management_detail.fieldvalue as int) displayorder ");
                strCMS.Append("  ,to_char(content_management_master.creationdate, 'DD/MM/YYYY') creationdate,content_pages.isactive,content_pages.ismultipage ");
                strCMS.Append("  ,Case WHEN RIGHT(to_char(content_pages.expirationdate, 'DD/MM/YYYY'),4)='1900' THEN '' ELSE to_char(content_pages.expirationdate, 'DD/MM/YYYY') END expirationdate from content_management_master ");
                strCMS.Append("  INNER JOIN content_pages ON content_pages.pageid=content_management_master.pageid ");
                strCMS.Append("   INNER JOIN content_management_detail on content_management_master.cmsid=content_management_detail.cmsid");
                strCMS.Append("  where content_management_master.pageid=" + pageid + "");
                strCMS.Append("  and  fieldtext like '%Display%' ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "displayorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Count Max Display order for CMS
        public Int32 Get_Max_Content_dorder(Int32 pageid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(cast(fieldvalue as int)),0) as MaxOrder from content_management_detail ");
                strMax.Append("  inner join content_management_master on content_management_master.cmsid=content_management_detail.cmsid where pageid=" + pageid + " and (fieldtext Like '%Display%') ");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Get max content dorder by menu name
        public Int32 Get_Max_Content_dorder_ByMenuname(Int32 pageid, string Menuname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(cast(q1.fieldvalue as int)),0) as MaxOrder from");
                strMax.Append("  (Select fieldvalue,content_management_detail.cmsid from content_management_detail ");
                strMax.Append("   inner join content_management_master on content_management_master.cmsid=content_management_detail.cmsid where pageid=" + pageid + " and (fieldtext Like '%Display%') ) q1");
                strMax.Append("  left join (Select content_management_detail.cmsid,fieldvalue,fieldtype from content_management_detail  ");
                strMax.Append("  inner join content_management_master on content_management_master.cmsid=content_management_detail.cmsid where pageid=" + pageid + " ) q2 ");
                strMax.Append("  on q1.cmsid=q2.cmsid ");
                strMax.Append("  where q2.fieldvalue='" + Menuname + "' and q2.fieldtype like 'DropDown' ");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        // Check duplicate Sub menu level 1 during save
        public bool Duplicate_Sub_Menu_Level_1_Check(List<Cls_ContentManagement> lstCMS, string fieldtext, string fieldvalue)
        {
            bool retval = false;
            try
            {
                StringBuilder strQuery = new StringBuilder();
                Int32 pageid = lstCMS[0].pageid;
                strQuery.Append("  Select COUNT(*) from content_management_detail where cmsid IN ");
                strQuery.Append("  (Select cmsid from content_management_detail where fieldtext='Sub Menu(Level-1) Name' and fieldvalue='" + fieldvalue + "')");
                strQuery.Append("  and fieldtext='Select Main Menu' and fieldvalue='" + lstCMS[1].fieldvalue + "' ");
                object objCount = Service.DbFunctions.SelectCount(strQuery.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToBoolean(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Check duplicate Sub menu level 1 during update
        public bool Duplicate_Sub_Menu_Level_1_Check_Update(List<Cls_ContentManagement> lstCMS, string fieldtext, string fieldvalue)
        {
            bool retval = false;
            try
            {
                StringBuilder strQuery = new StringBuilder();
                Int32 pageid = lstCMS[0].pageid;
                strQuery.Append("  Select COUNT(*) from content_management_detail where cmsid IN ");
                strQuery.Append("  (Select cmsid from content_management_detail where fieldtext='Sub Menu(Level-1) Name' and fieldvalue='" + fieldvalue + "')");
                strQuery.Append("  and fieldtext='Select Main Menu' and fieldvalue='" + lstCMS[1].fieldvalue + "' ");
                object objCount = Service.DbFunctions.SelectCount(strQuery.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToBoolean(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 
        private bool CheckSubMenuLevel1Duplicate(List<Cls_ContentManagement> objCMS)
        {
            bool IsValid = false;
            try
            {
                IsValid = ServicesFactory.DocCMSServices.Duplicate_Sub_Menu_Level_1_Check(objCMS, objCMS[2].fieldtext, objCMS[2].fieldvalue);
                return IsValid;
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        private bool CheckSubMenuLevel1DuplicateUpdate(List<Cls_ContentManagement> objCMS)
        {
            bool IsValid = false;
            try
            {
                IsValid = ServicesFactory.DocCMSServices.Duplicate_Sub_Menu_Level_1_Check_Update(objCMS, objCMS[2].fieldtext, objCMS[2].fieldvalue);
                return IsValid;
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        private string CheckSubMenuLevel2Duplicate(List<Cls_ContentManagement> objCMS)
        {
            return "";
        }

        // Insert CMS Data
        public string Insert_CMS_Data(List<Cls_ContentManagement> objCMS, string userid)
        {
            try
            {
                string RetVal = "";
                if (objCMS != null && objCMS.Count > 0)
                {
                    if (objCMS[0].pagename.ToUpper().IndexOf(" LEVEL-1") > -1)
                    {
                        bool CheckValueExist = CheckSubMenuLevel1Duplicate(objCMS);
                        if (CheckValueExist)
                        {
                            return "Already Exists";
                        }
                        else
                        {
                            // Do Nothing
                        }
                    }
                    else if (objCMS[0].pagename.ToUpper().IndexOf(" LEVEL-2") > -1)
                    {
                        CheckSubMenuLevel2Duplicate(objCMS);
                    }
                    List<string> objQueryList = new List<string>();
                    Cls_ContentManagement objMaster = objCMS[0];
                    StringBuilder Query = new StringBuilder();
                    Query.Append(" Insert Into content_management_master (pageid,pagename,createdby,creationdate,lastmodifiedby,lastmodificationdate)");
                    Query.Append("  VALUES(" );
                    Query.Append(objMaster.pageid);
                    Query.Append(" ,'");
                    Query.Append(objMaster.pagename);
                    Query.Append(" ','");
                    Query.Append(userid);
                    Query.Append("',");
                    Query.Append(" LOCALTIMESTAMP");
                    Query.Append(" ,'");
                    Query.Append(userid);
                    Query.Append("',");
                    Query.Append(" LOCALTIMESTAMP");
                    Query.Append(" ");
                    Query.Append(" )");
                    objQueryList.Add(Query.ToString());
                    for (Int32 i = 1; i < objCMS.Count; i++)
                    {
                        Query = new StringBuilder();
                        Query.Append(" Insert Into content_management_detail (cmsid,fieldtext,dorder,fieldtype,fieldvalue)");
                        Query.Append("  Select COALESCE(MAX(cmsid),0) as cmsid ");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldtext);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(objCMS[i].dorder);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldtype);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldvalue);
                        Query.Append(" '");
                        Query.Append("  from content_management_master where pagename='" + objCMS[0].pagename + "'");
                        objQueryList.Add(Query.ToString());
                    }
                    if (objQueryList != null && objQueryList.Count > 0)
                    {
                        RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                    }
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Int32 Get_Max_Draft_CMS_id()
        {
            return Service.DbFunctions.SelectCount(" Select COALESCE(MAX(maxdraftcmsid),0)  maxdraftcmsid from dfi_max_draft_cms_id");
        }

        // Insert CMS Data draft
        public string Insert_CMS_Data_Draft(List<Cls_ContentManagement> objCMS, string userid)
        {
            try
            {
                string RetVal = "";
                StringBuilder strDraftExist = new StringBuilder();
                Int32 Maxcmsid = 0;
                if (objCMS[0].cmsid == 0)
                {
                    if (Service.DbFunctions.SelectCount(" Select COALESCE(Count(*),0) from content_management_master_draft  where cmsid>=99999999") > 0)
                    {
                        Maxcmsid = Service.DbFunctions.SelectCount(" Select COALESCE(MAX(cmsid),0)+1 from content_management_master_draft where cmsid>=99999999");
                    }
                    else
                    {
                        Maxcmsid = Get_Max_Draft_CMS_id();
                    }
                }
                else
                {
                    Maxcmsid = objCMS[0].cmsid;
                }
                List<string> objQueryList = new List<string>();
                objQueryList.Add(" Delete from content_management_detail_draft where cmsid=" + Maxcmsid + "");
                objQueryList.Add(" Delete from content_management_master_draft where cmsid=" + Maxcmsid + "");
                if (objCMS != null && objCMS.Count > 0)
                {
                    Cls_ContentManagement objMaster = objCMS[0];
                    StringBuilder Query = new StringBuilder();
                    Query.Append(" Insert Into content_management_master_draft (cmsid,pageid,pagename,createdby,creationdate,lastmodifiedby,lastmodificationdate)");
                    Query.Append("  VALUES(" );
                    Query.Append(Maxcmsid);
                    Query.Append(" ,");
                    Query.Append(objMaster.pageid);
                    Query.Append(" ,'");
                    Query.Append(objMaster.pagename);
                    Query.Append(" ','");
                    Query.Append(userid);
                    Query.Append("',");
                    Query.Append(" LOCALTIMESTAMP");
                    Query.Append(" ,'");
                    Query.Append(userid);
                    Query.Append("',");
                    Query.Append(" LOCALTIMESTAMP");
                    Query.Append(" ");
                    Query.Append(" )");
                    objQueryList.Add(Query.ToString());
                    for (Int32 i = 1; i < objCMS.Count; i++)
                    {
                        Query = new StringBuilder();
                        Query.Append(" Insert Into content_management_detail_draft (draftid,cmsid,fieldtext,dorder,fieldtype,fieldvalue)");
                        Query.Append("  Select COALESCE(MAX(draftid),0) as draftid ");
                        Query.Append(" ,");
                        Query.Append(Maxcmsid);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldtext);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(objCMS[i].dorder);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldtype);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldvalue);
                        Query.Append(" '");
                        Query.Append("  from content_management_master_draft where pageid='" + objCMS[0].pageid + "'");
                        objQueryList.Add(Query.ToString());
                    }
                    if (objQueryList != null && objQueryList.Count > 0)
                    {
                        RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                        if (RetVal == "")
                        {
                            RetVal = Convert.ToString(Maxcmsid);
                        }
                    }
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Remove Draft Content Data
        public string Remove_CMS_Data_Draft(Int32 pageid, Int32 cmsid)
        {
            try
            {
                string RetVal = "";
                List<string> objQueryList = new List<string>();
                objQueryList.Add(" Delete from content_management_detail_draft where cmsid=" + cmsid + "");
                objQueryList.Add(" Delete from content_management_master_draft where cmsid=" + cmsid + "");
                if (objQueryList != null && objQueryList.Count > 0)
                {
                    RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Update Page Content
        public string Update_CMS_Data(List<Cls_ContentManagement> objCMS, string userid, Int32 cmsid)
        {
            try
            {
                string RetVal = "";
                if (objCMS != null && objCMS.Count > 0)
                {
                    List<string> objQueryList = new List<string>();
                    Cls_ContentManagement objMaster = objCMS[0];
                    StringBuilder Query = new StringBuilder();
                    Query.Append(" Update content_management_master");
                    Query.Append("  Set ");
                    Query.Append("  lastmodifiedby= ");
                    Query.Append(" '");
                    Query.Append(userid);
                    Query.Append("'");
                    Query.Append(" ,");
                    Query.Append("  lastmodificationdate= ");
                    Query.Append(" LOCALTIMESTAMP ");
                    Query.Append(" Where ");
                    Query.Append(" cmsid=");
                    Query.Append(cmsid);
                    objQueryList.Add(Query.ToString());
                    Query = new StringBuilder();
                    Query.Append(" Delete From content_management_detail  where cmsid= " + cmsid + "  ");
                    objQueryList.Add(Query.ToString());
                    for (Int32 i = 1; i < objCMS.Count; i++)
                    {
                        Query = new StringBuilder();
                        Query.Append(" Insert Into content_management_detail (cmsid,fieldtext,dorder,fieldtype,fieldvalue)");
                        Query.Append("  VALUES (" );
                        Query.Append(Convert.ToString(cmsid));
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldtext);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(objCMS[i].dorder);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldtype);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objCMS[i].fieldvalue);
                        Query.Append(" ')");
                        objQueryList.Add(Query.ToString());
                    }
                    if (objQueryList != null && objQueryList.Count > 0)
                    {
                        RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                    }
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Update Content Management Page Display Order 
        public Int32 Update_Content_displayorder(List<Cls_ContentManagement> objContent)
        {
            Int32 retVal = 0;
            try
            {
                if (objContent != null && objContent.Count > 0)
                {

                    for (Int32 i = 0; i < objContent.Count; i++)
                    {

                        StringBuilder strContent = new StringBuilder();
                        strContent.Append(" Update content_management_detail ");
                        strContent.Append(" Set");
                        strContent.Append("  fieldvalue=");
                        strContent.Append(" '");
                        strContent.Append(objContent[i].displayorder);
                        strContent.Append(" '");
                        strContent.Append("  Where ");
                        strContent.Append(" cmsid=");
                        strContent.Append(objContent[i].cmsid);
                        strContent.Append("  and fieldtext like '%Display%'");
                        retVal = Service.DbFunctions.UpdateCommand(strContent.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Delete Page Content By cmsid
        public Int32 Delete_CMS_Data(Int32 cmsid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Delete from content_management_detail ");
                strCMS.Append(" Where ");
                strCMS.Append(" cmsid=");
                strCMS.Append(cmsid);
                retVal = Service.DbFunctions.DeleteCommand(strCMS.ToString());
                strCMS = new StringBuilder();
                strCMS.Append(" Delete from content_management_master WHERE cmsid= '" + cmsid + "'");
                retVal = Service.DbFunctions.DeleteCommand(strCMS.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //TO CHANGE Set as default 
        public Int32 Update_Default_Check(Int32 pageid)
        {
            try
            {
                StringBuilder strUpdateCheck = new StringBuilder();
                strUpdateCheck.Append(" Update content_management_detail ");
                strUpdateCheck.Append(" Set ");
                strUpdateCheck.Append(" fieldvalue=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" False");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append("  Where ");
                strUpdateCheck.Append("  cmsid");
                strUpdateCheck.Append("  in (Select content_management_detail.cmsid from content_management_detail");
                strUpdateCheck.Append("  inner join content_management_master on content_management_master.cmsid=content_management_detail.cmsid where pageid=" + pageid + " ");
                strUpdateCheck.Append("  and fieldtype='Checkbox') and fieldtype='Checkbox'");
                return Service.DbFunctions.UpdateCommand(strUpdateCheck.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //check existing menu name for content page in case of update
        public bool Check_Existing_Menu_For_Update_Bycmsid(string Menuname, Int32 cmsid, Int32 pageid)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strcheck = new StringBuilder();
                strcheck.Append(" Select Count(*) menucount from content_management_detail inner join content_management_master ");
                strcheck.Append("  on content_management_detail.cmsid=content_management_master.cmsid");
                strcheck.Append("  where pageid=" + pageid + " and fieldtext like '%Name%' and fieldvalue='" + Menuname.Trim().Replace(" '", "''") + "' and content_management_detail.cmsid not in ("  + cmsid + ")");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strcheck.ToString());
                if (retVal > 0)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Check existing Prduct Detail category by category name
        public bool Check_Existing_ProductcategoryDetail_Update_Bycmsid(string categoryname, Int32 cmsid, Int32 pageid)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strcheck = new StringBuilder();
                strcheck.Append(" Select Count(*) menucount from content_management_detail inner join content_management_master ");
                strcheck.Append("  on content_management_detail.cmsid=content_management_master.cmsid");
                strcheck.Append("  where pageid=" + pageid + " and fieldtext like '%Select Category%' and fieldvalue='" + categoryname.Trim().Replace(" '", "''") + "' and content_management_detail.cmsid not in ("  + cmsid + ")");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strcheck.ToString());
                if (retVal > 0)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //check existing existing Prduct Detail category by category name in case of save
        public bool Check_Existing_ProductcategoryDetail_For_Save(string categoryname, Int32 pageid)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strcheck = new StringBuilder();
                strcheck.Append(" Select Count(*) menucount from content_management_detail inner join content_management_master ");
                strcheck.Append("  on content_management_detail.cmsid=content_management_master.cmsid");
                strcheck.Append("  where pageid=" + pageid + " and fieldtext like '%Select Category%' and fieldvalue='" + categoryname.Trim().Replace(" '", "''") + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strcheck.ToString());
                if (retVal > 0)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //check existing menu name for content page in case of save
        public bool Check_Existing_Menu_For_Save(string Menuname, Int32 pageid)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strcheck = new StringBuilder();
                strcheck.Append(" Select Count(*) menucount from content_management_detail inner join content_management_master ");
                strcheck.Append("  on content_management_detail.cmsid=content_management_master.cmsid");
                strcheck.Append("  where pageid=" + pageid + " and fieldtext like '%Name%' and fieldvalue='" + Menuname.Trim().Replace(" '", "''") + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strcheck.ToString());
                if (retVal > 0)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_ItemValue_ForDropDown_SubMenu_Leve1(string pagename, string MainMenuname)
        {
            DataTable dtRetVal = new DataTable();
            dtRetVal.Columns.Add(" fieldvalue", typeof(string));
            DataTable dtCMS = null;
            try
            {
                List<string> objQueryList = new List<string>();
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" SELECT q2.cmsid,cast(q2.fieldvalue as int) dorder FROM( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pagename='" + pagename + "' and fieldtype='Checkbox' and fieldvalue='True'");
                strCMS.Append("   )  q1 LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pagename='" + pagename + "' and fieldtext like '%Display Order%') q2");
                strCMS.Append("   ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                if (pagename.ToUpper() == "SUB MENU(LEVEL-1) name")
                {
                    strCMS = new StringBuilder();
                    strCMS.Append(" Select DISTINCT fieldvalue from content_management_detail where fieldtext like '%Sub Menu Name%'");
                    strCMS.Append(" And cmsid IN(Select cmsid from content_management_detail where fieldtext like '%Active%' and fieldvalue='True')");
                    strCMS.Append(" And cmsid IN(Select cmsid from content_management_detail where fieldtext like '%Select Main Menu%' and fieldvalue='" + MainMenuname + "' )");
                    DataSet dsmenu = Service.DbFunctions.SelectCommand(strCMS.ToString());
                    if (dsmenu != null && dsmenu.Tables.Count > 0)
                    {
                        DataTable dtSubMenu = dsmenu.Tables[0];
                        for (Int32 i = 0; i < dtSubMenu.Rows.Count; i++)
                        {
                            dtRetVal.Rows.Add(Convert.ToString(dsmenu.Tables[0].Rows[i][0]));
                        }
                    }
                }
                else
                {
                    for (Int32 i = 0; i < dtCMS.Rows.Count; i++)
                    {
                        strCMS = new StringBuilder();
                        strCMS.Append(" Select fieldvalue from content_management_detail where cmsdetailid =(Select MIN(cmsdetailid) from content_management_detail where cmsid=" + dtCMS.Rows[i]["cmsid"] + ")");
                        DataSet dsmenu = Service.DbFunctions.SelectCommand(strCMS.ToString());
                        if (dsmenu != null && dsmenu.Tables.Count > 0)
                        {
                            dtRetVal.Rows.Add(Convert.ToString(dsmenu.Tables[0].Rows[0][0]));
                        }
                    }
                }
                return dtRetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetch menu name by page name
        public DataTable Fetch_Main_Menu_Dashboard_Bypagename_Menuname(Int32 pageid, string Menuname)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("   Select Distinct q1.cmsid,cast(q2.fieldvalue as int) dorder from ");
                strCMS.Append("  (Select content_management_detail.cmsid,dorder from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pageid=" + pageid + "");
                strCMS.Append("   and fieldvalue='" + Menuname + "')q1");
                strCMS.Append("  left join ");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pageid=" + pageid + "");
                strCMS.Append("  and fieldtext like '%Display%') q2");
                strCMS.Append("  on q1.cmsid=q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region TinyMCE Function
        // To Fetch All images for image Upload 
        public DataTable Fetch_All_images()
        {
            DataTable dtimages = null;
            try
            {
                StringBuilder strimages = new StringBuilder();
                strimages.Append(" select id,filename from fileupload where filecategoryid=True Union select -1 as id,' None' as Fimename order by filename");
                DataSet ds = Service.DbFunctions.SelectCommand(strimages.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtimages = ds.Tables[0];
                }
                return dtimages;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Media management functions
        //========FETCH MEDIA FILES
        public DataTable Fetch_All_MediaFiles()
        {
            try
            {
                StringBuilder strq = new StringBuilder();
                strq.Append(" SELECT [id] ,[filecategoryid] ,C.blogcategoryname as Filecategory , '..' + right([filename],15) as [filename],[filepath]+ 'icon/'+ [filename] as FileIconPath ");
                strq.Append(" , [filepath]+ [filename] as [filepath] ,[creationdate] ,[modificationdate],videolink  FROM fileupload U  ");
                strq.Append(" INNER JOIN fileuploadcategory C on C.categoryid = U.filecategoryid  ORDER BY U.id ");
                DataTable dscontent = Service.DbFunctions.SelectCommand(strq.ToString()).Tables[0];
                return dscontent;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        // Fetch Media File by id
        public DataTable Fetch_Media_Files_By_id(string id)
        {
            try
            {
                StringBuilder strq = new StringBuilder();
                strq.Append(" SELECT * FROM fileupload WHERE id=" + id);
                DataTable dscontent = Service.DbFunctions.SelectCommand(strq.ToString()).Tables[0];
                return dscontent;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        //fetch Media File By names
        public DataTable Fetch_Media_Files_By_name(string fname, Int32 id)
        {
            try
            {
                StringBuilder strq = new StringBuilder();
                strq.Append(" SELECT * FROM fileupload WHERE filename='" + fname + "' and filecategoryid=" + id + "");
                DataTable dscontent = Service.DbFunctions.SelectCommand(strq.ToString()).Tables[0];
                return dscontent;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        // IN CASE OF EDIT FILE
        public DataTable Fetch_Media_Files_By_name_and_id(string fname, Int32 id, Int32 filecategoryid)
        {
            try
            {
                StringBuilder strq = new StringBuilder();
                strq.Append(" SELECT * FROM fileupload WHERE filename='" + fname + "' and id <>" + id + " and filecategoryid=" + filecategoryid + "");
                DataTable dscontent = Service.DbFunctions.SelectCommand(strq.ToString()).Tables[0];
                return dscontent;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        // To Verify the Deletion of the file
        public bool Verify_Deletion(int id, string type)
        {
            bool result = true;
            StringBuilder query = new StringBuilder();
            DataTable Dtcontent;
            if (type == "content")
            {
                query.Append(" select MAX(s.total) as 'TotalRecord' from(" );
                query.Append(" select COUNT(*) as 'Total' from AssociatedSites where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from FooterLinks where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from HomePageSlider where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from ManagedLinks where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from modules_DirectorDetails where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from NavigationLinks where ContentPage=CONVERT(nvarchar," + id + ")");
                query.Append(" union select COUNT(*) as 'Total' from NavigationSubLinks where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from NavigationThirdLinks where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SideBarSubLinks where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SideBarThirdLevel where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SlideShowMain where ContentPageLink=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from UtilityBarLink where ContentPage=CONVERT(nvarchar," + id + ") ");
                query.Append(" ) s");
                Dtcontent = Service.DbFunctions.SelectCommand(query.ToString()).Tables[0];
                if (Convert.ToInt32(Dtcontent.Rows[0]["TotalRecord"]) > 0)
                {
                    result = false;
                }
            }
            else if (type == "document")
            {
                query.Append(" select MAX(s.total) as 'TotalRecord' from(" );
                query.Append(" select COUNT(*) as 'Total' from AssociatedSites where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from FooterLinks where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from HomePageSlider where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from ManagedLinks where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from modules_DirectorDetails where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from NavigationLinks where Documentid=CONVERT(nvarchar," + id + ")");
                query.Append(" union select COUNT(*) as 'Total' from NavigationSubLinks where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from NavigationThirdLinks where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SlideShowItems where Fileid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SideBarSubLinks where Fileid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SideBarThirdLevel where Fileid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from UtilityBarLink where Documentid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from PolicyMemorandumCatalog where Documentid=" + id + "");
                query.Append(" union select COUNT(*) as 'Total' from PolicyMemorandum_Exhibits where Documentid=" + id + "");
                query.Append(" ) s");
                Dtcontent = Service.DbFunctions.SelectCommand(query.ToString()).Tables[0];
                if (Convert.ToInt32(Dtcontent.Rows[0]["TotalRecord"]) > 0)
                {
                    result = false;
                }
            }
            if (type == "policy")
            {
                query.Append(" select MAX(s.total) as 'TotalRecord' from(" );
                query.Append(" select COUNT(*) as 'Total' from AssociatedSites where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from FooterLinks where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from HomePageSlider where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from ManagedLinks where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from modules_DirectorDetails where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from NavigationLinks where Policyid=CONVERT(nvarchar," + id + ")");
                query.Append(" union select COUNT(*) as 'Total' from NavigationSubLinks where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from UtilityBarLink where Policyid=CONVERT(nvarchar," + id + ") ");
                query.Append(" ) s");
                Dtcontent = Service.DbFunctions.SelectCommand(query.ToString()).Tables[0];
                if (Convert.ToInt32(Dtcontent.Rows[0]["TotalRecord"]) > 0)
                {
                    result = false;
                }
            }
            if (type == "slide")
            {
                query.Append(" select MAX(s.total) as 'TotalRecord' from(" );
                query.Append(" select COUNT(*) as 'Total' from SlideShowimages where Slideid=CONVERT(nvarchar," + id + ") ");
                query.Append(" union select COUNT(*) as 'Total' from SlideShowItems where Slideid=CONVERT(nvarchar," + id + ") ");
                query.Append(" ) s");
                Dtcontent = Service.DbFunctions.SelectCommand(query.ToString()).Tables[0];
                if (Convert.ToInt32(Dtcontent.Rows[0]["TotalRecord"]) > 0)
                {
                    result = false;
                }
            }
            else if (type == "sidebar")
            {
                query.Append(" select MAX(s.total) as 'TotalRecord' from(" );
                query.Append(" select COUNT(*) as 'Total' from CalohiiContent where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_About where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_contactus where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_DataMovement where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_EhealthPartners where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_GenericSecondary where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_Homepage where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_Patients where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_Privacy where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_Providers where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_ProvidersSecondary where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_statedepartment where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_TermConditions where SibeBarid=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from SideBarSubLinks where idSideBar=" + id + " ");
                query.Append(" union select COUNT(*) as 'Total' from Module_TermConditions where SibeBarid=" + id + " ");
                query.Append(" ) s");
                Dtcontent = Service.DbFunctions.SelectCommand(query.ToString()).Tables[0];
                if (Convert.ToInt32(Dtcontent.Rows[0]["TotalRecord"]) > 0)
                {
                    result = false;
                }
            }
            return result;
        }

        //====== FOR THE PURPOSE OF FETCH DATA FROM FILEcategory TABLE
        public DataTable Fetch_File_category_All()
        {
            StringBuilder strq = new StringBuilder();
            strq.Append(" SELECT categoryid,categoryname  FROM fileuploadcategory ORDER BY categoryid");
            DataTable Dtcontent = Service.DbFunctions.SelectCommand(strq.ToString()).Tables[0];
            return Dtcontent;
        }

        //====== FOR THE PURPOSE OF FETCH DATA FROM AUDIO AND VidEO TABLE
        public DataTable Fetch_Audio_Video_Filecategory_All()
        {
            StringBuilder strq = new StringBuilder();
            strq.Append(" SELECT categoryid,categoryname  FROM fileuploadcategory where categoryid = 2 OR categoryid = 3 ORDER BY categoryid");
            DataTable Dtcontent = Service.DbFunctions.SelectCommand(strq.ToString()).Tables[0];
            return Dtcontent;
        }

        //BIND DATA IN MEDIA DASHBOARD
        public DataTable Bind_Media_Dashboard(string query)
        {
            DataTable dt = null;
            try
            {
                DataSet ds = Service.DbFunctions.SelectCommand(query);
                if (ds != null & ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //========INSERT MEDIA FILES
        public Int32 Insert_MediaFiles(Clsfileuploader fileUploader)
        {
            Int32 retVal = 0;
            try
            {
                if (string.IsNullOrEmpty(fileUploader.videolink))
                {
                    fileUploader.videolink = " ";
                }
                StringBuilder insertQuery = new StringBuilder();
                insertQuery.Append(" INSERT INTO fileupload (filecategoryid,filename,filepath,creationdate,alternatetext,description,uploadedby,videolink) ");
                insertQuery.Append(" Values(" );
                insertQuery.Append(Convert.ToInt32(fileUploader.filecategoryid));
                insertQuery.Append(" ,");
                insertQuery.Append(" '");
                insertQuery.Append(fileUploader.filename.Replace(" '", "''"));
                insertQuery.Append(" '");
                insertQuery.Append(" ,");
                insertQuery.Append(" '");
                insertQuery.Append(fileUploader.filepath.Replace(" '", "''"));
                insertQuery.Append(" '");
                insertQuery.Append(" ,");
                insertQuery.Append("LOCALTIMESTAMP");
                insertQuery.Append(",");
                insertQuery.Append(" '");
                insertQuery.Append(fileUploader.alternatetext.Replace(" '", "''"));
                insertQuery.Append(" '");
                insertQuery.Append(" ,");
                insertQuery.Append(" '");
                insertQuery.Append(fileUploader.description.Replace(" '", "''"));
                insertQuery.Append(" '");
                insertQuery.Append(" ,");
                insertQuery.Append(" '");
                insertQuery.Append(fileUploader.uploadedby);
                insertQuery.Append("'");
                insertQuery.Append(" ,");
                insertQuery.Append(" '");
                insertQuery.Append(fileUploader.videolink.Replace(" '", "''"));
                insertQuery.Append(" '");
                insertQuery.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(insertQuery.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // to Update Media Files
        public Int32 Update_MediaFiles(Clsfileuploader fileUploader)
        {
            Int32 retVal = 0;
            try
            {

                if (string.IsNullOrEmpty(fileUploader.videolink))
                {
                    fileUploader.videolink = " ";
                }
                StringBuilder updatequery = new StringBuilder();
                updatequery.Append(" UPDATE fileupload SET ");
                updatequery.Append("  filename=");
                updatequery.Append(" '");
                updatequery.Append(fileUploader.filename.Replace(" '", "''"));
                updatequery.Append(" '");
                updatequery.Append(" ,");
                updatequery.Append(" filepath=");
                updatequery.Append(" '");
                updatequery.Append(fileUploader.filepath.Replace(" '", "''"));
                updatequery.Append(" '");
                updatequery.Append(" ,");
                updatequery.Append(" alternatetext=");
                updatequery.Append(" '");
                updatequery.Append(fileUploader.alternatetext);
                updatequery.Append(" '");
                updatequery.Append(" ,");
                updatequery.Append(" description=");
                updatequery.Append(" '");
                updatequery.Append(fileUploader.description.Replace(" '", "''"));
                updatequery.Append(" '");
                updatequery.Append(" ,");
                updatequery.Append(" modificationdate=");
                updatequery.Append(" LOCALTIMESTAMP ");
                updatequery.Append(" ,");
                updatequery.Append(" uploadedby=");
                updatequery.Append(" '");
                updatequery.Append(fileUploader.uploadedby);
                updatequery.Append("',");
                updatequery.Append(" videolink=");
                updatequery.Append(" '");
                updatequery.Append(fileUploader.videolink.Replace(" '", "''"));
                updatequery.Append(" '");
                updatequery.Append(" Where ");
                updatequery.Append(" id=");
                updatequery.Append(fileUploader.id);
                retVal = Service.DbFunctions.UpdateCommand(updatequery.ToString());
                return retVal;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        // To delete media Files
        public void Delete_MediaFile(Clsfileuploader fileUploader)
        {
            try
            {
                StringBuilder strq = new StringBuilder();
                strq.Append(" delete from fileupload where id=" + fileUploader.id);
                Service.DbFunctions.DeleteCommand(strq.ToString());
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        #endregion

        #region Left Side Bar functions
        //======FOR THE PURPOSE TO GET MODULES DATA BASED ON USER roleid
        public Int32 Fetch_roleid_By_userid(string userid)
        {
            try
            {
                Int32 RetVal = 0;
                StringBuilder strRole = new StringBuilder();
                strRole.Append(" Select roleid from userprofiles where userid='"+userid+"'");
                object objroleid = Service.DbFunctions.SelectCount(strRole.ToString());
                if (objroleid != null)
                {
                    RetVal = Convert.ToInt32(objroleid);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======FOR THE PURPOSE TO GET SUBMODULES DATA BASED ON USER roleid AND moduleid
        public DataTable Fetch_LeftSide_modules_By_roleid(Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select Distinct roleid,roleprivileges.moduleid,modules.displayorder,modules.modulename from roleprivileges  ");
                strRolePrivilege.Append("  Inner Join modules ON modules.moduleid= roleprivileges.moduleid and roleprivileges.isview=True and roleid="+roleid+"");
                strRolePrivilege.Append("  Order By modules.displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======FOR THE PURPOSE TO GET USER ROLE id BASED ON USER id
        public DataTable Fetch_LeftSide_submodules_By_roleid(Int32 roleid, Int32 moduleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select DISTINCT roleprivileges.submoduleid,submodules.submodulename,submodules.displayorder from roleprivileges    ");
                strRolePrivilege.Append("  Inner Join submodules ON submodules.submoduleid= roleprivileges.submoduleid ");
                strRolePrivilege.Append("  and roleprivileges.isview=True and roleprivileges.moduleid=" + moduleid + " and roleid="+roleid+" ");
                strRolePrivilege.Append("  order by submodules.displayorder  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======FOR THE PURPOSE TO GET MODULE id BASED ON SUBMODULE id
        public DataTable Fetch_moduleId_By_SubModuleId(Int32 submoduleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select moduleid from submodules where submoduleid=" + submoduleid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======FOR THE PURPOSE TO GET USER ROLE id BASED ON USER id
        public DataTable Get_Roles_Privileges_Based_on_SubModuleId(Int32 submoduleid, Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select DISTINCT roleprivileges.submoduleid,submodules.submodulename,submodules.displayorder, roleprivileges.isadd, roleprivileges.isedit, roleprivileges.isdelete from roleprivileges    ");
                strRolePrivilege.Append("  Inner Join submodules ON submodules.submoduleid= roleprivileges.submoduleid ");
                strRolePrivilege.Append("  and roleprivileges.isview=True and roleprivileges.submoduleid=" + submoduleid + " and roleid="+roleid+" ");
                strRolePrivilege.Append("  order by submodules.displayorder  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region company Functions

        // Fetch All company Detail
        public DataTable Fetch_company()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Select * from company Order By companyid");
                DataSet ds = Service.DbFunctions.SelectCommand(strcompany.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch Compnay detail By company id
        public Cls_company Fetch_company_Byid(Int32 companyid)
        {
            Cls_company objcompany = null;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Select companyid,companyname,description,address,city,state,isactive,companylogo from company where companyid='" + companyid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strcompany.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objcompany = new Cls_company();
                        objcompany.companyid = Convert.ToInt32(dt.Rows[0]["companyid"]);
                        objcompany.companyname = Convert.ToString(dt.Rows[0]["companyname"]);
                        objcompany.description = Convert.ToString(dt.Rows[0]["description"]);
                        objcompany.address = Convert.ToString(dt.Rows[0]["address"]);
                        objcompany.city = Convert.ToString(dt.Rows[0]["city"]);
                        objcompany.state = Convert.ToString(dt.Rows[0]["state"]);
                        objcompany.companylogo = Convert.ToString(dt.Rows[0]["companylogo"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objcompany.isactive = true;
                            }
                            else
                            {
                                objcompany.isactive = false;
                            }
                        }
                        else
                        {
                            objcompany.isactive = false;
                        }
                    }
                }
                return objcompany;
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }

        //Check for Existing company name
        public Int32 Check_Existing_company(string companyname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*)  from company where companyname='" + companyname + "' ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert company
        public Int32 Insert_company(Cls_company company)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Insert into company(companyname,description,address,city,state,companylogo,isactive,createdby,creationdate) ");
                strcompany.Append(" Values(" );
                strcompany.Append(" '");
                strcompany.Append(company.companyname.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.description.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.address.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.city.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.state.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.companylogo.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.isactive);
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" '");
                strcompany.Append(company.createdby);
                strcompany.Append("'");
                strcompany.Append(",");
                strcompany.Append(" LOCALTIMESTAMP");
                strcompany.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strcompany.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Update Existing company by company id
        public Int32 Update_company(Cls_company company)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Update company ");
                strcompany.Append(" Set ");
                strcompany.Append(" companyname=");
                strcompany.Append(" '");
                strcompany.Append(company.companyname.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" description=");
                strcompany.Append(" '");
                strcompany.Append(company.description.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" address=");
                strcompany.Append(" '");
                strcompany.Append(company.address.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" city=");
                strcompany.Append(" '");
                strcompany.Append(company.city.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" state=");
                strcompany.Append(" '");
                strcompany.Append(company.state.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" companylogo=");
                strcompany.Append(" '");
                strcompany.Append(company.companylogo.Replace(" '", "''"));
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" isactive=");
                strcompany.Append(" '");
                strcompany.Append(company.isactive);
                strcompany.Append(" '");
                strcompany.Append(" ,");
                strcompany.Append(" lastmodifiedby=");
                strcompany.Append(" '");
                strcompany.Append(company.lastmodifiedby);
                strcompany.Append("'");
                strcompany.Append(" ,");
                strcompany.Append(" lastmodificationdate=");
                strcompany.Append(" LOCALTIMESTAMP ");
                strcompany.Append(" Where ");
                strcompany.Append(" companyid=");
                strcompany.Append(company.companyid);
                retVal = Service.DbFunctions.UpdateCommand(strcompany.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To delete company By company id
        public Int32 Delete_company(Int32 companyid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Delete from company ");
                strcompany.Append(" Where ");
                strcompany.Append(" companyid=");
                strcompany.Append(companyid);
                retVal = Service.DbFunctions.DeleteCommand(strcompany.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        #endregion

        #region SMTP Functions
        //Fetch SMTP detial by department name
        public Cls_SMTP Fetch_SMTP_Detail_By_department(string department)
        {
            Cls_SMTP objSMTP = null;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Select * from smtpsettings inner join smtpemails on smtpsettings.smtpid=smtpemails.smtpid where smtpemails.department='" + department + "' and isactive='1' ");
                DataSet ds = Service.DbFunctions.SelectCommand(strcompany.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objSMTP = new Cls_SMTP();
                        objSMTP.smtpid = Convert.ToInt32(dt.Rows[0]["smtpid"]);
                        objSMTP.smtpport = Convert.ToString(dt.Rows[0]["smtpport"]);
                        objSMTP.smtphostserver = Convert.ToString(dt.Rows[0]["smtpserverhost"]);
                        objSMTP.senderemailid = Convert.ToString(dt.Rows[0]["senderemailid"]);
                        objSMTP.senderpassword = Convert.ToString(dt.Rows[0]["senderpassword"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["smtpsslserver"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objSMTP.smtpsslserver = true;
                            }
                            else
                            {
                                objSMTP.smtpsslserver = false;
                            }
                        }
                        else
                        {
                            objSMTP.smtpsslserver = false;
                        }
                        objSMTP.ccemail = Convert.ToString(dt.Rows[0]["ccemail"]);
                        objSMTP.bccemail = Convert.ToString(dt.Rows[0]["bccemail"]);
                        objSMTP.department = Convert.ToString(dt.Rows[0]["department"]);
                        objSMTP.toemail = Convert.ToString(dt.Rows[0]["toemail"]);
                        activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objSMTP.isactive = true;
                            }
                            else
                            {
                                objSMTP.isactive = false;
                            }
                        }
                        else
                        {
                            objSMTP.isactive = false;
                        }
                    }
                }
                return objSMTP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch SMTP Detail
        public Cls_SMTP Fetch_SMTP()
        {
            Cls_SMTP objSMTP = null;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Select * from smtpsettings Order By smtpid");
                DataSet ds = Service.DbFunctions.SelectCommand(strcompany.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objSMTP = new Cls_SMTP();
                        objSMTP.smtpid = Convert.ToInt32(dt.Rows[0]["smtpid"]);
                        objSMTP.smtpport = Convert.ToString(dt.Rows[0]["smtpport"]);
                        objSMTP.smtphostserver = Convert.ToString(dt.Rows[0]["smtpserverhost"]);
                        objSMTP.senderemailid = Convert.ToString(dt.Rows[0]["senderemailid"]);
                        objSMTP.senderpassword = Decrypt(Convert.ToString(dt.Rows[0]["senderpassword"]));
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["smtpsslserver"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objSMTP.smtpsslserver = true;
                            }
                            else
                            {
                                objSMTP.smtpsslserver = false;
                            }
                        }
                        else
                        {
                            objSMTP.smtpsslserver = false;
                        }
                    }
                }
                return objSMTP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //fetch smtp email detail by id
        public Cls_SMTP Fetch_SMTP_email_Detail_By_id(Int32 id)
        {
            Cls_SMTP objSMTP = null;
            try
            {
                StringBuilder strcompany = new StringBuilder();
                strcompany.Append(" Select * from smtpemails where id=" + id + " Order By id");
                DataSet ds = Service.DbFunctions.SelectCommand(strcompany.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objSMTP = new Cls_SMTP();
                        objSMTP.id = Convert.ToInt32(dt.Rows[0]["id"]);
                        objSMTP.smtpid = Convert.ToInt32(dt.Rows[0]["smtpid"]);
                        objSMTP.ccemail = Convert.ToString(dt.Rows[0]["ccemail"]);
                        objSMTP.bccemail = Convert.ToString(dt.Rows[0]["bccemail"]);
                        objSMTP.department = Convert.ToString(dt.Rows[0]["department"]);
                        objSMTP.toemail = Convert.ToString(dt.Rows[0]["toemail"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objSMTP.isactive = true;
                            }
                            else
                            {
                                objSMTP.isactive = false;
                            }
                        }
                        else
                        {
                            objSMTP.isactive = false;
                        }
                    }
                }
                return objSMTP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //FEtch SMTP email Detail
        public DataTable Fetch_SMTP_email_Detail(Int32 smtpid)
        {
            DataTable dtSMTP = null;
            try
            {
                StringBuilder strFetch = new StringBuilder();
                strFetch.Append(" Select * from smtpemails where smtpid=" + smtpid + " Order By id");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetch.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtSMTP = ds.Tables[0];
                    if (dtSMTP != null && dtSMTP.Rows.Count > 0)
                    {
                        return dtSMTP;
                    }
                }
                return dtSMTP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //To Insert SMTP
        public Int32 Insert_SMTP(Cls_SMTP smtp)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSmtp = new StringBuilder();
                strSmtp.Append(" Insert into smtpsettings(smtpserverhost,smtpport,senderemailid,senderpassword,smtpsslserver) ");
                strSmtp.Append(" Values(" );
                strSmtp.Append(" '");
                strSmtp.Append(smtp.smtphostserver.Replace(" '", "''"));
                strSmtp.Append(" '");
                strSmtp.Append(" ,'");
                strSmtp.Append(smtp.smtpport);
                strSmtp.Append(" '");
                strSmtp.Append(" ,'");
                strSmtp.Append(smtp.senderemailid);
                strSmtp.Append(" '");
                strSmtp.Append(" ,");
                strSmtp.Append(" '");
                strSmtp.Append(Encrypt(smtp.senderpassword));
                strSmtp.Append(" '");
                strSmtp.Append(" ,");
                strSmtp.Append(" '");
                strSmtp.Append(smtp.smtpsslserver);
                strSmtp.Append(" '");
                strSmtp.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strSmtp.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Delete SMPTP before Inserting any new smptp
        public Int32 Delete_SMTP(Int32 smtpid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSmtp = new StringBuilder();
                strSmtp.Append(" Delete from smtpemails ");
                strSmtp.Append(" Where ");
                strSmtp.Append(" id=");
                strSmtp.Append(smtpid);
                retVal = Service.DbFunctions.DeleteCommand(strSmtp.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Update SMTP
        public Int32 Update_SMTP(Cls_SMTP clsSMTP)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSMTP = new StringBuilder();
                strSMTP.Append(" Update smtpsettings ");
                strSMTP.Append(" Set ");
                strSMTP.Append(" smtpserverhost=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.smtphostserver.Replace(" '", "''"));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" smtpport=");
                strSMTP.Append(clsSMTP.smtpport);
                strSMTP.Append(" ,");
                strSMTP.Append(" senderemailid=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.senderemailid.Replace(" '", "''"));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" senderpassword=");
                strSMTP.Append(" '");
                strSMTP.Append(Encrypt(clsSMTP.senderpassword));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" smtpsslserver=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.smtpsslserver);
                strSMTP.Append(" '");
                strSMTP.Append("  Where ");
                strSMTP.Append(" smtpid=");
                strSMTP.Append(clsSMTP.smtpid);
                retVal = Service.DbFunctions.UpdateCommand(strSMTP.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert SMTP email detail
        public Int32 Insert_SMTP_email_Detail(Cls_SMTP smtp)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSmtp = new StringBuilder();
                strSmtp.Append(" Insert into smtpemails(smtpid,department,toemail,ccemail,bccemail,isactive) ");
                strSmtp.Append(" Values(" );
                strSmtp.Append(smtp.smtpid);
                strSmtp.Append(" ,");
                strSmtp.Append(" '");
                strSmtp.Append(smtp.department.Replace(" '", "''"));
                strSmtp.Append(" '");
                strSmtp.Append(" ,'");
                strSmtp.Append(smtp.toemail.Replace(" '", "''"));
                strSmtp.Append(" '");
                strSmtp.Append(" ,'");
                strSmtp.Append(smtp.ccemail.Replace(" '", "''"));
                strSmtp.Append(" '");
                strSmtp.Append(" ,");
                strSmtp.Append(" '");
                strSmtp.Append(smtp.bccemail.Replace(" '", "''"));
                strSmtp.Append(" '");
                strSmtp.Append(" ,");
                strSmtp.Append(" '");
                strSmtp.Append(smtp.isactive);
                strSmtp.Append(" '");
                strSmtp.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strSmtp.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // get amx display order for SMTP email
        public Int32 Get_Max_SMTP_email_dorder(Int32 smtpid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(Count(id),0) as Maxdorder from smtpemails where smtpid=" + smtpid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Check for SMTP department
        public Int32 Check_Existing_department(string departmentname, Int32 smtpid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*) CountNo from smtpemails where smtpid=" + smtpid + " and department='" + departmentname + "'  ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Check for SMTP department for Update 
        public Int32 Check_Existing_department_For_Update(string departmentname, Int32 smtpid, Int32 id)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*) CountNo from smtpemails where smtpid=" + smtpid + " and department='" + departmentname + "' and id not in ("  + id + ") ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Update SMTP detail by id
        public Int32 Update_SMTP_email_Detail(Cls_SMTP clsSMTP)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSMTP = new StringBuilder();
                strSMTP.Append(" Update smtpemails ");
                strSMTP.Append(" Set ");
                strSMTP.Append(" department=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.department.Replace(" '", "''"));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" toemail=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.toemail.Replace(" '", "''"));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" ccemail=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.ccemail.Replace(" '", "''"));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" bccemail=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.bccemail.Replace(" '", "''"));
                strSMTP.Append(" '");
                strSMTP.Append(" ,");
                strSMTP.Append(" isactive=");
                strSMTP.Append(" '");
                strSMTP.Append(clsSMTP.isactive);
                strSMTP.Append(" '");
                strSMTP.Append("  Where ");
                strSMTP.Append(" id=");
                strSMTP.Append(clsSMTP.id);
                retVal = Service.DbFunctions.UpdateCommand(strSMTP.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Access Page function

        // TO FETCH PARTICULAR USER DETAILS BY GIVING emailid AND password
        public DataTable Validate_Application_User(string username, string password)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from applicationusers where username='" + username + "' or email='" + username + "' and password='" + Encrypt(password) + "' and isactive=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion

        #region Plan Pages Functions

        //Fetch Plan Page Details
        public DataTable Fetch_Plan()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from plan_master where isactive=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtplan = ds.Tables[0];
                    dt = SortTable(dtplan, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch Plan detail for Display order
        public DataTable Fetch_Plan_For_displayorder()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from plan_master ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtplan = ds.Tables[0];
                    dt = SortTable(dtplan, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch  Plan name whose Status is active
        public DataTable Fetch_Plan_name()
        {

            DataTable RetVal = null;
            try
            {
                StringBuilder strpagename = new StringBuilder();
                strpagename.Append(" Select planid,planname from plan_master where isactive=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strpagename.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    RetVal = SortTable(dt, "planid");
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch  Plan Master Detail By Plan id
        public Cls_Plans Fetch_Plan_Master_Byid(Int32 planid)
        {
            Cls_Plans objPlans = null;
            try
            {
                StringBuilder strPlans = new StringBuilder();
                strPlans.Append(" Select * from plan_master where planid=" + planid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strPlans.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objPlans = new Cls_Plans();
                        objPlans.planname = Convert.ToString(dt.Rows[0]["planname"]);
                        objPlans.plandescription = Convert.ToString(dt.Rows[0]["plandescription"]);
                        objPlans.price = Convert.ToString(dt.Rows[0]["price"]);
                        objPlans.dorder = Convert.ToInt32(dt.Rows[0]["dorder"]);
                        objPlans.imagename = Convert.ToString(dt.Rows[0]["imagename"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objPlans.active = true;
                            }
                            else
                            {
                                objPlans.active = false;
                            }
                        }
                        else
                        {
                            objPlans.active = false;
                        }
                    }
                }
                return objPlans;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch  Plan Master Detail By Plan id
        public DataTable Fetch_Plan_Master_Data_By_planid(Int32 planid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetch = new StringBuilder();
                strFetch.Append(" SELECT planname,plandescription,price,imagename,dorder,isactive from plan_master WHERE planid = '" + planid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetch.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch Plan Detail Detail By Plan id
        public DataTable Fetch_Plan_Detail_Data_By_planid(Int32 planid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetch = new StringBuilder();
                strFetch.Append(" SELECT * from  plan_detail WHERE planid = " + planid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetch.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtCMS = ds.Tables[0];
                    dt = SortTable(dtCMS, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Insert Plan function
        public string Insert_Plan_Detail(List<Cls_Plans> objPlanList)
        {
            string RetVal = "";
            try
            {
                List<string> objQueryList = new List<string>();
                Cls_Plans objMaster = objPlanList[0];
                StringBuilder Query = new StringBuilder();
                if (objPlanList != null && objPlanList.Count > 0)
                {
                    Query.Append(" Insert into plan_master(planname,plandescription,imagename,price,dorder,isactive,createdby,creationdate) ");
                    Query.Append(" Values(" );
                    Query.Append(" '");
                    Query.Append(objMaster.planname.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append(" '");
                    Query.Append(objMaster.plandescription.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append(" '");
                    Query.Append(objMaster.imagename.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append(" '");
                    Query.Append(objMaster.price.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append(" '");
                    Query.Append(objMaster.dorder);
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append(" '");
                    Query.Append(objMaster.isactive);
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append(" '");
                    Query.Append(objMaster.createdby);
                    Query.Append("'");
                    Query.Append(",");
                    Query.Append(" LOCALTIMESTAMP");
                    Query.Append(" )");
                    objQueryList.Add(Query.ToString());
                    for (Int32 i = 1; i < objPlanList.Count; i++)
                    {
                        Query = new StringBuilder();
                        Query.Append(" Insert into plan_detail");
                        Query.Append(" (" );
                        Query.Append(" planid,featurename,displayorder,active");
                        Query.Append(" )");
                        Query.Append("  Select COALESCE(MAX(planid),0) as planid");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objPlanList[i].featurename.Replace(" '", "''"));
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(objPlanList[i].displayorder);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objPlanList[i].active);
                        Query.Append(" '");
                        Query.Append("  from plan_master");
                        objQueryList.Add(Query.ToString());
                    }
                    if (objQueryList != null && objQueryList.Count > 0)
                    {
                        RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                    }
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Update Existing Plan Page By planid
        public string Update_Plan_Page_Byid(List<Cls_Plans> objPlanList)
        {
            string RetVal = "";
            try
            {
                List<string> objQueryList = new List<string>();
                Cls_Plans objMaster = objPlanList[0];
                StringBuilder Query = new StringBuilder();
                if (objPlanList != null && objPlanList.Count > 0)
                {
                    Query.Append(" Update plan_master ");
                    Query.Append("  Set");
                    Query.Append("  plandescription=");
                    Query.Append(" '");
                    Query.Append(objMaster.plandescription.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append("  imagename=");
                    Query.Append(" '");
                    Query.Append(objMaster.imagename.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append("  price=");
                    Query.Append(" '");
                    Query.Append(objMaster.price.Replace(" '", "''"));
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append("  dorder=");
                    Query.Append(" '");
                    Query.Append(objMaster.dorder);
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append("  isactive=");
                    Query.Append(" '");
                    Query.Append(objMaster.isactive);
                    Query.Append(" '");
                    Query.Append(" ,");
                    Query.Append("  lastmodifiedby=");
                    Query.Append(" '");
                    Query.Append(objMaster.lastmodifiedby);
                    Query.Append("'");
                    Query.Append(" ,");
                    Query.Append("  lastmodificationdate=");
                    Query.Append(" LOCALTIMESTAMP");
                    Query.Append("  where planid=" + objMaster.planid + "");
                    objQueryList.Add(Query.ToString());
                    StringBuilder strDelete = new StringBuilder();
                    strDelete.Append(" Delete from plan_detail where planid='" + objMaster.planid + "'");
                    Service.DbFunctions.DeleteCommand(strDelete.ToString());
                    for (Int32 i = 1; i < objPlanList.Count; i++)
                    {
                        Query = new StringBuilder();
                        Query.Append(" Insert into plan_detail");
                        Query.Append(" (" );
                        Query.Append(" planid,featurename,displayorder,active");
                        Query.Append(" )");
                        Query.Append("  Values(" );
                        Query.Append(" " + objMaster.planid + "");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objPlanList[i].featurename.Replace(" '", "''"));
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(objPlanList[i].displayorder);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objPlanList[i].active);
                        Query.Append(" '");
                        Query.Append("  )");
                        objQueryList.Add(Query.ToString());
                    }
                    if (objQueryList != null && objQueryList.Count > 0)
                    {
                        RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                    }
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Update Existing Plan Display order
        public Int32 Update_Plans_dorder(List<Cls_Plans> objPlanList)
        {
            Int32 retVal = 0;
            try
            {
                if (objPlanList != null && objPlanList.Count > 0)
                {
                    for (Int32 i = 0; i < objPlanList.Count; i++)
                    {
                        StringBuilder strPlans = new StringBuilder();
                        strPlans.Append(" Update plan_master ");
                        strPlans.Append(" Set ");
                        strPlans.Append(" dorder=");
                        strPlans.Append(objPlanList[i].dorder);
                        strPlans.Append(" ,");
                        strPlans.Append(" lastmodifiedby=");
                        strPlans.Append(" '");
                        strPlans.Append(objPlanList[i].lastmodifiedby);
                        strPlans.Append("'");
                        strPlans.Append(" ,");
                        strPlans.Append(" lastmodificationdate=");
                        strPlans.Append(" LOCALTIMESTAMP ");
                        strPlans.Append(" Where ");
                        strPlans.Append(" planid=");
                        strPlans.Append(objPlanList[i].planid);
                        retVal = Service.DbFunctions.UpdateCommand(strPlans.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Check for Existing Plan Page By Plan name
        public Int32 Check_Existing_planname(string planname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strplanname = new StringBuilder();
                strplanname.Append(" Select COUNT(*)  from plan_master where planname='" + planname + "' ");
                object objCount = Service.DbFunctions.SelectCount(strplanname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Maximum Display Order for Plans
        public Int32 Get_Max_Plans_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from plan_master");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Delete Plans 
        public Int32 Delete_Plans(Int32 planid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from plan_detail ");
                strDelete.Append(" Where ");
                strDelete.Append(" planid=");
                strDelete.Append(planid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                strDelete = new StringBuilder();
                strDelete.Append(" Delete from plan_master ");
                strDelete.Append(" Where ");
                strDelete.Append(" planid=");
                strDelete.Append(planid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Get Maximum Display Order for Plans detail
        public Int32 Get_FeatureCount(Int32 planid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from plan_detail where  planid=" + planid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Blogs Details Form
        // Ngo Title EXISTS OR NOT 

        public bool ChkDuplicate_BlogTitle(string blogname)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From Blog where blogname='" + blogname + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch All Blog category id and blogcategoryname For DropDown order by name
        public DataTable Fetch_Blog_category_name()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strcategory = new StringBuilder();
                strcategory.Append(" Select blogcategoryid,blogcategoryname,navigationurl from blog_category where isactive=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strcategory.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "blogcategoryname");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch All Blog category id and blogcategoryname For DropDown order by dorder
        public DataTable Fetch_Blog_category_name_By_dorder()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strcategory = new StringBuilder();
                strcategory.Append(" Select blogcategoryid,blogcategoryname,navigationurl,dorder from blog_category where isactive=True");
                DataSet ds = Service.DbFunctions.SelectCommand(strcategory.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Search for string based on category name 
        public DataTable Search_Blog_Text(string SearchText)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strcategory = new StringBuilder();
                strcategory.Append(" Select blog.blogid,blog_category.blogcategoryname,blog.navigationurl,blog.blogname,blog.imagename,blog.description,blog.blogpostedby,blog.dorder,to_char(blog.creationdate, 'DD/MM/YYYY') creationdate , Case when blog.isactive=True then 'Checked' else '' end isactive from blog ");
                strcategory.Append("  inner join  blog_category on blog.blogcategoryid=blog_category.blogcategoryid");
                strcategory.Append("  where blog.isactive=True and (blog_category.blogcategoryname like'%" + SearchText + "%' OR blog.blogname like'%" + SearchText + "%'");
                strcategory.Append("   OR blog.blogpostedby like'%" + SearchText + "%' OR blog.description like '%" + SearchText + "%')");
                strcategory.Append("  and blog_category.isactive=True and blog.isactive=True ");
                DataSet ds = Service.DbFunctions.SelectCommand(strcategory.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch Blog order by display order
        public DataTable Fetch_Blog_Order_By_dorder()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select * from blog");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }

        //Get Converted comment Video
        public DataTable Fetch_All_Converted_VideosList(string memberid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strVideos = new StringBuilder();
                if (memberid != null && memberid != "")
                {
                    strVideos.Append(" select videoid,sessionid,memberid,videoname,coverimage from tempcommentvideoconverted where memberid=" + memberid + " order by videoid desc ");
                }
                else
                {
                    strVideos.Append(" select videoid,sessionid,memberid,videoname,coverimage from tempcommentvideoconverted order by videoid desc ");
                }
                DataSet ds = Service.DbFunctions.SelectCommand(strVideos.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======Inser Blogs Details================================//
        public Int32 Insert_BlogDetails(Cls_Blog ObjBlog)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into Blog (blogcategoryid,accesstype,blogname,blogpostedby,navigationurl,imagename,description,dorder,isactive,createdby,creationdate,allowposting) VALUES(" );
                strInsert.Append(ObjBlog.blogcategoryid);
                strInsert.Append(" ,");
                strInsert.Append(ObjBlog.accesstype.Trim().Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(ObjBlog.blogname.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(ObjBlog.blogpostedby.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(ObjBlog.navigationurl.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(ObjBlog.imagename.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(ObjBlog.description.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(ObjBlog.dorder);
                strInsert.Append(" ,'");
                strInsert.Append(ObjBlog.isactive);
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(ObjBlog.createdby);
                strInsert.Append("'");
                strInsert.Append(",");
                strInsert.Append(" LOCALTIMESTAMP");
                strInsert.Append(" ,'");
                strInsert.Append(ObjBlog.allowposting);
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommandReturnidentity(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //======Adjust Blogs Details================================//
        public void Adjust_Blogs_dorder()
        {
            DataTable Dtcontent = null;
            string query = "select * from blog";
            DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dtBlog = ds.Tables[0];
                Dtcontent = SortTable(dtBlog, "dorder");
            }
            if (Dtcontent != null && Dtcontent.Rows.Count > 0)
            {
                int index;
                for (index = 0; index < Dtcontent.Rows.Count; index++)
                {
                    Int64 id = Convert.ToInt64(Dtcontent.Rows[index]["blogid"]);
                    Int64 displayorder = Convert.ToInt64(Dtcontent.Rows[index]["dorder"]);

                    if (displayorder != (index + 1))
                    {
                        query = string.Format(" Update blog SET dorder=" + (index + 1) + " where blogid={0}", id);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
        }

        // TO Fetch Blog id By Blog name
        public Int32 Fetch_blogid_Byname(string blogname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strBlog = new StringBuilder();
                strBlog.Append(" Select Distinct blogid from blog where blogname='" + blogname + "'");
                DataSet Ds = Service.DbFunctions.SelectCommand(strBlog.ToString());
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    DataTable dt = Ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        retval = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //--Fetch Blogs--BY id--------//
        public Cls_Blog Fetch_Blog_By_id(Int32 blogid)
        {
            Cls_Blog objNGO = null;
            try
            {
                StringBuilder strFetchDataByid = new StringBuilder();
                strFetchDataByid.Append(" Select * from blog where blogid=" + blogid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchDataByid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objNGO = new Cls_Blog();
                        objNGO.blogtype = Convert.ToInt32(dt.Rows[0]["blogtype"]);
                        objNGO.dorder = Convert.ToInt32(dt.Rows[0]["dorder"]);
                        objNGO.blogcategoryid = Convert.ToInt32(dt.Rows[0]["blogcategoryid"]);
                        objNGO.blogname = Convert.ToString(dt.Rows[0]["blogname"]);
                        objNGO.accesstype = Convert.ToString(dt.Rows[0]["accesstype"]);
                        objNGO.blogpostedby = Convert.ToString(dt.Rows[0]["blogpostedby"]);
                        objNGO.navigationurl = Convert.ToString(dt.Rows[0]["navigationurl"]);
                        objNGO.description = Convert.ToString(dt.Rows[0]["description"]);
                        objNGO.imagename = Convert.ToString(dt.Rows[0]["imagename"]);
                        objNGO.creationdate = Convert.ToString(dt.Rows[0]["creationdate"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        string allowpostingStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["allowposting"]));
                        if (!string.IsNullOrEmpty(allowpostingStatus))
                        {
                            if (allowpostingStatus.ToUpper() == "TRUE")
                            {
                                objNGO.allowposting = true;
                            }
                            else
                            {
                                objNGO.allowposting = false;
                            }
                        }
                        else
                        {
                            objNGO.allowposting = false;
                        }

                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objNGO.isactive = true;
                            }
                            else
                            {
                                objNGO.isactive = false;
                            }
                        }
                        else
                        {
                            objNGO.isactive = false;
                        }

                    }
                }
                return objNGO;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
        // Blogs EXISTS OR NOT 

        public bool ChkDuplicate_BlogTitleUpdate(string blogname, Int32 blogid)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From Blog where blogname='" + blogname + "' and blogid Not In("  + blogid + ")");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal >= 1)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Uodate Blogs Status Details --
        public Int32 Update_Blog_Status(Int32 blogid, bool Status)
        {
            try
            {
                StringBuilder strUpdatestatus = new StringBuilder();
                strUpdatestatus.Append(" Update blog Set isactive='" + Status + "' where blogid=" + blogid + "");
                return Service.DbFunctions.UpdateCommand(strUpdatestatus.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        // Uodate Blogs Details --
        public Int32 Update_Blog_By_id(Cls_Blog objBlog)
        {

            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update blog SET ");
                strUpdate.Append(" blogcategoryid=");
                strUpdate.Append(objBlog.blogcategoryid);
                strUpdate.Append(" ,");
                strUpdate.Append(" accesstype=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.accesstype.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" blogname=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.blogname.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" navigationurl=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.navigationurl.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                if (!string.IsNullOrEmpty(objBlog.imagename))
                {
                    strUpdate.Append(" imagename=");
                    strUpdate.Append(" '");
                    strUpdate.Append(objBlog.imagename.Trim().Replace(" '", "''"));
                    strUpdate.Append(" ',");
                }
                strUpdate.Append(" description=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.description.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" blogpostedby=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.blogpostedby.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" dorder=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.dorder);
                strUpdate.Append(" ',");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.isactive);
                strUpdate.Append(" '");
                strUpdate.Append(" ,");
                strUpdate.Append(" lastmodifiedby=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.lastmodifiedby);
                strUpdate.Append("',");
                strUpdate.Append(" allowposting=");
                strUpdate.Append(" '");
                strUpdate.Append(objBlog.allowposting);
                strUpdate.Append(" '");
                strUpdate.Append(" ,");
                strUpdate.Append(" blogtype=");
                strUpdate.Append(objBlog.blogtype);
                strUpdate.Append(" ,");
                strUpdate.Append("  lastmodificationdate=");
                strUpdate.Append(" LOCALTIMESTAMP ");
                strUpdate.Append("  Where ");
                strUpdate.Append("  blogid = ");
                strUpdate.Append(objBlog.blogid);
                return Service.DbFunctions.UpdateCommand(strUpdate.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        //======Fetch Blogs Details================================//isactive activeStatus
        public DataTable Fetch_Blog_Details()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select blog.blogid,blog.blogtype,blog_category.isactive activeStatus,blog_category.blogcategoryname,blog.blogname,blog.imagename,blog.blogpostedby,blog.dorder,to_char(blog.creationdate, 'DD/MM/YYYY') creationdate , Case when blog.allowposting=True then 'Checked' else '' end isactive , Case when blog.allowposting=True then 'Checked' else '' end allowposting from blog ");
                strFetchData.Append("  inner join  blog_category on blog.blogcategoryid=blog_category.blogcategoryid");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        //======Fetch Blogs Details=for Front End===============================//
        public DataTable Fetch_Blog_Details_FrontEnd(string accesstype)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select blog.blogid,blog.blogtype,blog_category.blogcategoryname,blog.blogname,blog.navigationurl,blog.imagename,blog.blogpostedby,blog.dorder,to_char(blog.creationdate, 'DD/MM/YYYY') creationdate,substring(blog.description,1,100)as description  from blog");
                strFetchData.Append("  inner join  blog_category on blog.blogcategoryid=blog_category.blogcategoryid ");
                strFetchData.Append("  where blog_category.isactive=True and blog.isactive=True ");
                if (accesstype.ToString().ToUpper() == "PUBLIC")
                {
                    strFetchData.Append("  and blog.accesstype='" + accesstype + "'");
                }
                strFetchData.Append("  Order by blog.creationdate desc ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
        //===== // Select Top 3 Bog Detail for footer===============================//
        public DataTable Fetch_Blog_Details_onFooter()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select blog.blogid,blog_category.blogcategoryname,blog.blogname,blog.imagename,blog.navigationurl,blog.blogpostedby,blog.dorder,to_char(blog.creationdate, 'DD/MM/YYYY') creationdate,substring(blog.description,1,100)as description  from blog");
                strFetchData.Append("  inner join  blog_category on blog.blogcategoryid=blog_category.blogcategoryid");
                strFetchData.Append("  where blog_category.isactive=True and blog.isactive=True ");
                strFetchData.Append("   Order by blog.creationdate desc limit 3");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        //======Fetch Blogs Details=for Front End=BY category id==============================//
        public DataTable Fetch_Blog_Details_FrontEnd_BY_Blog_categoryid(Int32 blogcategoryid, string accesstype)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select blog.blogid,blog.blogtype,blog_category.blogcategoryname,blog.blogname,blog.imagename,blog.blogpostedby,blog.navigationurl,blog.dorder,to_char(blog.creationdate, 'DD/MM/YYYY') creationdate,blog.description from blog");
                strFetchData.Append("  inner join  blog_category on blog.blogcategoryid=blog_category.blogcategoryid");
                strFetchData.Append("   Where blog.blogcategoryid='" + blogcategoryid + "'");
                strFetchData.Append("  and blog.isactive=True and blog_category.isactive=True ");
                if (accesstype.ToString().ToUpper() == "PUBLIC")
                {
                    strFetchData.Append("  and blog.accesstype='" + accesstype + "'");
                }
                strFetchData.Append("  Order by blog.creationdate desc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { 
                throw ex; 
            }
        }
        //======Fetch Blogs Details=for Front End=BY id==============================//
        public DataTable Fetch_Blog_Details_FrontEnd_BYid(Int32 blogid, string accesstype)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select blog.blogid,blog.blogtype,blog_category.blogcategoryname,blog.navigationurl,blog.blogname,blog.imagename,blog.blogpostedby,blog.dorder,to_char(blog.creationdate, 'DD/MM/YYYY')  creationdate,blog.description,blog.allowposting from blog");
                strFetchData.Append("  inner join  blog_category on blog.blogcategoryid=blog_category.blogcategoryid");
                strFetchData.Append("   Where blogid='" + blogid + "' ");
                if (accesstype.ToString().ToUpper() == "PUBLIC")
                {
                    strFetchData.Append("  and blog.accesstype='" + accesstype + "'");
                }
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        //Count comment
        public Int32 Count_comments(Int32 blogid)
        {
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From blogcomment where blogid=" + blogid + "");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Fetch converted Video
        public DataTable GetConvertedVideo()
        {
            try
            {
                StringBuilder strYear = new StringBuilder();
                strYear.Append(" select videoid,sessionid,memberid,videoname,coverimage from tempcommentvideoconverted");
                return Service.DbFunctions.SelectCommand(strYear.ToString()).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Insert comments 
        public Int32 Insert_comments(Cls_comment objcomment)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into blogcomment (blogid,memberid,comment,posteddate,accesstype) VALUES(" );
                strInsert.Append(" ");
                strInsert.Append(objcomment.blogid);
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objcomment.memberid);
                strInsert.Append(" '");
                strInsert.Append(" ,'");
                strInsert.Append(objcomment.comment.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(" LOCALTIMESTAMP");
                strInsert.Append(" ,'");
                strInsert.Append(objcomment.accesstype.Trim().Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        public DataTable Fetch_comment_Based_On_blogid(Int32 blogid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  Select commentid,blogid,blogcomment.memberid,COALESCE(comment,'')comment,CONVERT(VARCHAR(20),posteddate,106)posteddate, ");
                strCMS.Append("  (memberaccounts.fname || ' ' || memberaccounts.lname) as username,COALESCE(imageurl,'')imageurl,COALESCE(imagethumbnailurl,'')");
                strCMS.Append("  imagethumbnailurl,COALESCE(videourl,'')videourl	,COALESCE(videocoverimageurl,'')videocoverimageurl  from Blogcomment inner join memberaccounts ");
                strCMS.Append("  on blogcomment.memberid=memberaccounts.memberid ");
                strCMS.Append("   where blogcomment.blogid=" + blogid + " ");
                if (string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["memberid"])) || Convert.ToInt32(HttpContext.Current.Session["memberid"]) <= 0)
                {
                    strCMS.Append("  and accesstype ='Public' ");
                }
                strCMS.Append("  order by commentid asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Upload comment image
        public Int32 Insert_comments_image(Cls_comment objcomment)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into blogcomment (blogid,memberid,imageurl,imagethumbnailurl,posteddate,accesstype) VALUES(" );
                strInsert.Append(" ");
                strInsert.Append(objcomment.blogid);
                strInsert.Append(" ,");
                strInsert.Append(objcomment.memberid);
                strInsert.Append(" ,'");
                strInsert.Append(objcomment.imageurl.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objcomment.imagethumbnailurl.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(" LOCALTIMESTAMP");
                strInsert.Append(" ,'");
                strInsert.Append(objcomment.accesstype.Trim().Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        // Upload comment Video 
        public Int32 Insert_comments_Video(Cls_comment objcomment)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into blogcomment (blogid,memberid,videourl,videocoverimageurl,posteddate,accesstype) VALUES(" );
                strInsert.Append(" ");
                strInsert.Append(objcomment.blogid);
                strInsert.Append(" ,");
                strInsert.Append(objcomment.memberid);
                strInsert.Append(" ,'");
                strInsert.Append(objcomment.videourl.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objcomment.videocoverimageurl.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(" LOCALTIMESTAMP");
                strInsert.Append(" ,'");
                strInsert.Append(objcomment.accesstype.Trim().Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Truncate Tempvideo Table
        public Int32 Truncate_TempVideo_ConvertedVideo()
        {
            try
            {
                object Objretval = null;
                Int32 retval = 0;
                List<string> lstQuery = new List<string>();
                StringBuilder strTempVideo = new StringBuilder();
                StringBuilder strConvertedVideo = new StringBuilder();
                strTempVideo.Append(" Truncate table tempcommentvideo");
                lstQuery.Add(strTempVideo.ToString());
                strConvertedVideo.Append(" Truncate table tempcommentvideoconverted");
                lstQuery.Add(strConvertedVideo.ToString());
                Objretval = Service.DbFunctions.Execute_Multiple_Query(lstQuery);
                if (Objretval != null && Objretval != "")
                {
                    retval = Convert.ToInt32(Objretval);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Delete image
        public void Deleteimage(string imagename)
        {
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from tblimage where imagename='" + imagename + "'");
                Service.DbFunctions.DeleteCommand(strDelete.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Delete Temp Video
        public Int32 Delete_Temp_Video(string videoname, string memberid)
        {
            try
            {
                Int32 retval = 0;
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from tempcommentvideo where memberid='" + memberid + "' and videoname='" + videoname + "' ");
                retval = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Temp comment Video
        public DataTable Fetch_All_Temp_VideosList(string memberid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strVideos = new StringBuilder();
                strVideos.Append(" select videoid,memberid,videoname from tempcommentvideo where memberid=" + memberid + " order by videoid desc ");
                DataSet ds = Service.DbFunctions.SelectCommand(strVideos.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Temp comment Video Converted comment Video
        public Int32 Insert_Temp_comment_Video_Converted(Cls_TempcommentVideoConverted objTempcommentVideoConverted)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into tempcommentvideoconverted (sessionid,memberid,videoname,coverimage) VALUES(" );
                strInsert.Append(" '");
                strInsert.Append(objTempcommentVideoConverted.sessionid.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objTempcommentVideoConverted.memberid.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objTempcommentVideoConverted.videoname.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objTempcommentVideoConverted.coverimage.Trim().Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        //======Delete Blogs Details================================//
        public Int32 Delete_Blog_By_id(Int32 blogid)
        {

            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from blog where blogid=" + blogid + "");

                return Service.DbFunctions.DeleteCommand(strDelete.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update dorder Blog Details --
        public void Reorder_Blog_Type(Int32 id, Int32 CurrentOrder, string OpType)
        {
            string query = "";
            DataTable Dtcontent = null;
            DataSet ds = null;
            Int64 MaxOrder = Get_Max_Blog_Type_dorder();
            if (OpType == "UP" && CurrentOrder != 1)
            {
                query = "select  blogid from blog where dorder=" + (CurrentOrder - 1) + " limit 1";
                ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["blogid"].ToString());
                    query = string.Format(" Update blog SET dorder=dorder-1 where blogid={0}", id);
                    Service.DbFunctions.UpdateCommand(query);
                    query = string.Format(" Update blog SET dorder=dorder+1 where blogid={0}", Previousid);
                    Service.DbFunctions.UpdateCommand(query);
                }
            }
            if (OpType == "DOWN" && CurrentOrder != MaxOrder)
            {
                query = "select  blogid from blog where dorder=" + (CurrentOrder + 1) + " limit 1";
                ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["blogid"].ToString());
                    query = string.Format(" Update blog SET dorder=dorder+1 where blogid={0}", id);
                    Service.DbFunctions.UpdateCommand(query);
                    query = string.Format(" Update blog SET dorder=dorder-1 where blogid={0} ", Nextid);
                    Service.DbFunctions.UpdateCommand(query);
                }
            }
        }

        // Method to get blog type Order 
        public Int32 Get_Max_Blog_Type_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from blog");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //---------Get_Max_Banner_dorder-----------''
        public Int32 Get_Max_Blogs_Type_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from blog");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }


        //---------Get_Min_Blog_Type_dorder-----------''
        public Int32 Get_Min_Blog_Type_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(Min(dorder),0) as Maxdorder from blog");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update Display order of Blog
        public Int32 Update_Blog_dorder(List<Cls_Blog> objBlog)
        {
            Int32 retVal = 0;
            try
            {
                if (objBlog != null && objBlog.Count > 0)
                {
                    for (Int32 i = 0; i < objBlog.Count; i++)
                    {

                        StringBuilder strBlog = new StringBuilder();
                        strBlog.Append(" Update blog ");
                        strBlog.Append(" Set ");
                        strBlog.Append(" dorder=");
                        strBlog.Append(objBlog[i].dorder);
                        strBlog.Append(" ,");
                        strBlog.Append(" lastmodifiedby=");
                        strBlog.Append(" '");
                        strBlog.Append(objBlog[i].lastmodifiedby);
                        strBlog.Append("'");
                        strBlog.Append(" ,");
                        strBlog.Append(" lastmodificationdate=");
                        strBlog.Append(" LOCALTIMESTAMP ");
                        strBlog.Append(" Where ");
                        strBlog.Append(" blogid=");
                        strBlog.Append(objBlog[i].blogid);
                        retVal = Service.DbFunctions.UpdateCommand(strBlog.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TO Fetch Blog name By blogid
        public string Fetch_blogname_Byid(string blogid)
        {
            string retval = "";
            try
            {
                StringBuilder strBlog = new StringBuilder();
                strBlog.Append(" Select Distinct blogname from blog where blogid='" + blogid + "'");
                DataSet Ds = Service.DbFunctions.SelectCommand(strBlog.ToString());
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    DataTable dt = Ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        retval = Convert.ToString(dt.Rows[0][0]);
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Blogs category Details Form
        // Ngo Title EXISTS OR NOT 

        public bool ChkDuplicate_Blogcategory(string blogcategoryname)
        {
            bool BannersExist = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From blog_category where blogcategoryname='" + blogcategoryname + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal > 0)
                {
                    BannersExist = true;
                }
                return BannersExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TO Fetch Blog categoryid By Blog category name
        public Int32 Fetch_categoryid_Bycategoryname(string blogcategoryname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strBlog = new StringBuilder();
                strBlog.Append(" Select Distinct blogcategoryid from blog_category where blogcategoryname='" + blogcategoryname + "'");
                DataSet Ds = Service.DbFunctions.SelectCommand(strBlog.ToString());
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    DataTable dt = Ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        retval = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //======Inser Blogs Details================================//
        public Int32 Insert_Blog_category(Cls_Blog_category ObjCat)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into blog_category (blogcategoryname,description,navigationurl,dorder,isactive,createdby,creationdate) VALUES(" );
                strInsert.Append(" '");
                strInsert.Append(ObjCat.blogcategoryname.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(ObjCat.description.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(ObjCat.navigationurl.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(ObjCat.dorder);
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(ObjCat.isactive);
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(ObjCat.createdby);
                strInsert.Append("'");
                strInsert.Append(",");
                strInsert.Append(" LOCALTIMESTAMP");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommandReturnidentity(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======Adjust Blogs Details================================//
        public void Adjust_category_dorder()
        {
            DataTable Dtcontent = null;
            string query = "select * from blog_category";
            DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dtBlog = ds.Tables[0];
                Dtcontent = SortTable(dtBlog, "dorder");
            }
            if (Dtcontent != null && Dtcontent.Rows.Count > 0)
            {
                int index;
                for (index = 0; index < Dtcontent.Rows.Count; index++)
                {
                    Int64 id = Convert.ToInt64(Dtcontent.Rows[index]["blogcategoryid"]);
                    Int64 displayorder = Convert.ToInt64(Dtcontent.Rows[index]["dorder"]);

                    if (displayorder != (index + 1))
                    {
                        query = string.Format(" Update blog_category SET dorder=" + (index + 1) + " where blogcategoryid={0}", id);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
        }

        // Blog category During Update EXISTS OR NOT 
        public bool ChkDuplicate_BlogcategoryUpdate(string blogcategoryname, Int32 blogcategoryid)
        {
            bool SocialMediaHostExist = false;
            try
            {
                StringBuilder strUser = new StringBuilder();
                strUser.Append(" Select Count(*) From blog_category where blogcategoryname='" + blogcategoryname + "' and blogcategoryid Not In("  + blogcategoryid + ")");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strUser.ToString());
                if (retVal >= 1)
                {
                    SocialMediaHostExist = true;
                }
                return SocialMediaHostExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //--Fetch category--BY id--------//
        public Cls_Blog_category Fetch_category_By_id(Int32 blogcategoryid)
        {
            Cls_Blog_category objCat = null;
            try
            {
                StringBuilder strFetchDataByid = new StringBuilder();
                strFetchDataByid.Append(" Select * from blog_category where blogcategoryid=" + blogcategoryid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchDataByid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objCat = new Cls_Blog_category();
                        objCat.dorder = Convert.ToInt32(dt.Rows[0]["dorder"]);
                        objCat.blogcategoryname = Convert.ToString(dt.Rows[0]["blogcategoryname"]);
                        objCat.description = Convert.ToString(dt.Rows[0]["description"]);
                        objCat.navigationurl = Convert.ToString(dt.Rows[0]["navigationurl"]);
                        objCat.creationdate = Convert.ToString(dt.Rows[0]["creationdate"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objCat.isactive = true;
                            }
                            else
                            {
                                objCat.isactive = false;
                            }
                        }
                        else
                        {
                            objCat.isactive = false;
                        }
                    }
                }
                return objCat;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        // Uodate Blogs Status Details --
        public Int32 Update_category_Status(Int32 blogcategoryid, bool Status)
        {
            try
            {
                StringBuilder strUpdatestatus = new StringBuilder();
                strUpdatestatus.Append(" Update blog_category Set isactive='" + Status + "' where blogcategoryid=" + blogcategoryid + "");
                return Service.DbFunctions.UpdateCommand(strUpdatestatus.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update Blogs Details --
        //======Delete category Details================================//
        public Int32 Delete_category_By_id(Int32 blogcategoryid)
        {

            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from blog_category where blogcategoryid=" + blogcategoryid + "");
                return Service.DbFunctions.DeleteCommand(strDelete.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }


        public Int32 Update_category_By_id(Cls_Blog_category objCat)
        {

            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update blog_category SET ");
                strUpdate.Append(" blogcategoryname=");
                strUpdate.Append(" '");
                strUpdate.Append(objCat.blogcategoryname.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" description=");
                strUpdate.Append(" '");
                strUpdate.Append(objCat.description.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" navigationurl=");
                strUpdate.Append(" '");
                strUpdate.Append(objCat.navigationurl.Trim().Replace(" '", "''"));
                strUpdate.Append(" ',");
                strUpdate.Append(" dorder=");
                strUpdate.Append(objCat.dorder);
                strUpdate.Append(" ,");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objCat.isactive);
                strUpdate.Append(" '");
                strUpdate.Append(" ,");
                strUpdate.Append(" lastmodifiedby=");
                strUpdate.Append(" '");
                strUpdate.Append(objCat.lastmodifiedby);
                strUpdate.Append("',");
                strUpdate.Append(" lastmodificationdate=");
                strUpdate.Append(" LOCALTIMESTAMP ");
                strUpdate.Append("  Where ");
                strUpdate.Append("  blogcategoryid = ");
                strUpdate.Append(objCat.blogcategoryid);
                return Service.DbFunctions.UpdateCommand(strUpdate.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }


        //======Fetch Blogs Details================================//
        public DataTable Fetch_category_Details()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select blogcategoryid,blogcategoryname,dorder,to_char(creationdate, 'DD/MM/YYYY') creationdate,isactive activeStatus, Case when isactive=True then 'Checked' else '' end isactive from blog_category order by blogcategoryid");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch Blog category order by display order
        public DataTable Fetch_Blog_category()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select * from blog_category");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Update dorder Blog Details --
        public void Reorder_category_Type(Int32 id, Int32 CurrentOrder, string OpType)
        {
            string query = "";
            DataTable Dtcontent = null;
            DataSet ds = null;
            Int64 MaxOrder = Get_Max_category_Type_dorder();
            if (OpType == "UP" && CurrentOrder != 1)
            {
                query = "select  blogcategoryid from blog_category where dorder=" + (CurrentOrder - 1) + " limit 1";
                ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["blogcategoryid"].ToString());
                    query = string.Format(" Update blog_category SET dorder=dorder-1 where blogcategoryid={0}", id);
                    Service.DbFunctions.UpdateCommand(query);
                    query = string.Format(" Update blog_category SET dorder=dorder+1 where blogcategoryid={0}", Previousid);
                    Service.DbFunctions.UpdateCommand(query);
                }
            }
            if (OpType == "DOWN" && CurrentOrder != MaxOrder)
            {
                query = "select blogcategoryid from blog_category where dorder=" + (CurrentOrder + 1) + " limit 1";
                ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["blogcategoryid"].ToString());
                    query = string.Format(" Update blog_category SET dorder=dorder+1 where blogcategoryid={0}", id);
                    Service.DbFunctions.UpdateCommand(query);
                    query = string.Format(" Update blog_category SET dorder=dorder-1 where blogcategoryid={0} ", Nextid);
                    Service.DbFunctions.UpdateCommand(query);
                }
            }
        }

        public Int32 Get_Max_category_Type_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from blog_category");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Update Display Order of blog_category
        public Int32 Update_category_dorder(List<Cls_Blog_category> objcategory)
        {
            Int32 retVal = 0;
            try
            {
                if (objcategory != null && objcategory.Count > 0)
                {
                    for (Int32 i = 0; i < objcategory.Count; i++)
                    {

                        StringBuilder strcategory = new StringBuilder();
                        strcategory.Append(" Update blog_category ");
                        strcategory.Append(" Set ");
                        strcategory.Append(" dorder=");
                        strcategory.Append(objcategory[i].dorder);
                        strcategory.Append(" ,");
                        strcategory.Append(" lastmodifiedby=");
                        strcategory.Append(" '");
                        strcategory.Append(objcategory[i].lastmodifiedby);
                        strcategory.Append("'");
                        strcategory.Append(" ,");
                        strcategory.Append(" lastmodificationdate=");
                        strcategory.Append(" LOCALTIMESTAMP ");
                        strcategory.Append(" Where ");
                        strcategory.Append(" blogcategoryid=");
                        strcategory.Append(objcategory[i].blogcategoryid);
                        retVal = Service.DbFunctions.UpdateCommand(strcategory.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Job Functions

        //Fetch cmsid of Job by Display Order
        public DataTable Fetch_Job_cmsid_By_pageid(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q2.cmsid,q3.fieldvalue FROM");
                strCMS.Append("   ( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append( " where pageid=" + pageid + " and fieldtext='Is Active' and fieldvalue='True'");
                strCMS.Append("  )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                strCMS.Append("  ) q2");
                strCMS.Append("  ON q1.cmsid = q2.cmsid");
                strCMS.Append("  LEFT JOIN ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append(" Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pageid=" + pageid + " and fieldtext like '%Name%' )");
                strCMS.Append(" q3 ON q3.cmsid = q2.cmsid ");
                strCMS.Append("  order by q2.fieldvalue asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch skill by category name
        public DataTable Fetch_skills_by_categoryname(Int32 pageid, string categoryname)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q2.cmsid,q3.fieldvalue FROM");
                strCMS.Append("  ( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtype='Dropdown' and fieldvalue like '" + categoryname + "'");
                strCMS.Append("  )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                strCMS.Append("  ) q2");
                strCMS.Append("  ON q1.cmsid = q2.cmsid");
                strCMS.Append("  LEFT JOIN ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append(" Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pageid=" + pageid + " and fieldtext like '%Skill Name%' )");
                strCMS.Append(" q3 ON q3.cmsid = q2.cmsid ");
                strCMS.Append("  order by q2.fieldvalue asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Job Details By CMS id
        public DataTable Fetch_Job_By_cmsid(Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select fieldtext,COALESCE(fieldvalue,'') Job,dorder from content_management_detail where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dtCMS = SortTable(dtBlog, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch All Job Details By CMS id
        public DataTable FetchAll_Job_By_cmsid(Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select fieldtext,fieldvalue as Job,dorder from content_management_detail where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dtCMS = SortTable(dtBlog, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Insert New Job Application
        public Int32 Insert_Job_Application(Cls_JobApplications clsjob)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strinsert = new StringBuilder();
                strinsert.Append(" Insert into JobApplications");
                strinsert.Append(" (jobtitle,firstname,lastname,phoneno,emailid,resumeheading,description,filename,skills,applydate)");
                strinsert.Append(" Values(" );
                strinsert.Append(" '");
                strinsert.Append(clsjob.jobtitle.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.firstname.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.lastname.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.phoneno.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.emailid.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.resumeheading.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.description.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.filename.Trim().Replace(" '", "''"));
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" '");
                strinsert.Append(clsjob.skills);
                strinsert.Append(" '");
                strinsert.Append(" ,");
                strinsert.Append(" LOCALTIMESTAMP");
                strinsert.Append(" )");
                retval = Service.DbFunctions.InsertCommand(strinsert.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Fetch Job Application Details By CMS id
        public DataTable Fetch_Job_Application()
        {
            DataTable dtJob = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select jobapplyid,jobtitle,firstname|| ' ' ||lastname as name,to_char(applydate, 'DD/MM/YYYY') applydate,description,filename,phoneno,emailid,Replace(skills, '~', ',') AS skills from JobApplications order by applydate desc");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtJob = ds.Tables[0];
                }
                return dtJob;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // to delete Job Application by job application id
        public Int32 Delete_Job_By_Applicationid(Int32 jobapplyid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from JobApplications ");
                strDelete.Append(" Where ");
                strDelete.Append(" jobapplyid=");
                strDelete.Append(jobapplyid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        #endregion

        #region Survey Function

        // Fetch Survey Master By Survey id
        public ClsSurvey Fetch_Survey_Master_Byid(Int32 surveyid)
        {
            ClsSurvey objSurvey = null;
            try
            {
                StringBuilder strFetch = new StringBuilder();
                strFetch.Append(" Select * from surveymaster where surveyid=" + surveyid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetch.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objSurvey = new ClsSurvey();
                        objSurvey.surveyname = Convert.ToString(dt.Rows[0]["surveyname"]);
                        objSurvey.description = Convert.ToString(dt.Rows[0]["description"]);
                        objSurvey.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["defaultactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objSurvey.defaultactive = true;
                            }
                            else
                            {
                                objSurvey.defaultactive = false;
                            }
                        }
                        else
                        {
                            objSurvey.defaultactive = false;
                        }
                        string showreport = Convert.ToString(Convert.ToString(dt.Rows[0]["showreport"]));
                        if (!string.IsNullOrEmpty(showreport))
                        {
                            if (showreport.ToUpper() == "TRUE")
                            {
                                objSurvey.showreport = true;
                            }
                            else
                            {
                                objSurvey.showreport = false;
                            }
                        }
                        else
                        {
                            objSurvey.showreport = false;
                        }
                    }
                }
                return objSurvey;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //======Insert Survey master================================//
        public Int32 Insert_Survey_Master(ClsSurvey objSurvey)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into surveymaster (surveyname,description,displayorder,showreport,defaultactive,createdby,creationdate) VALUES(" );
                strInsert.Append(" '");
                strInsert.Append(objSurvey.surveyname.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objSurvey.description.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(objSurvey.displayorder);
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objSurvey.showreport);
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objSurvey.defaultactive);
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objSurvey.createdby);
                strInsert.Append("'");
                strInsert.Append(",");
                strInsert.Append(" LOCALTIMESTAMP");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommandReturnidentity(strInsert.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }


        //TO CHANGE Set as default 
        public Int32 Update_Default_SurveyCheck()
        {
            try
            {
                StringBuilder strUpdateCheck = new StringBuilder();
                strUpdateCheck.Append(" Update surveymaster ");
                strUpdateCheck.Append(" Set ");
                strUpdateCheck.Append(" defaultactive=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" False");
                strUpdateCheck.Append(" '");
                return Service.DbFunctions.UpdateCommand(strUpdateCheck.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // get amx display orderr of survey master
        public Int32 Get_Max_Survey_Type_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from surveymaster");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }

        //Fetch survey detail for Survey Dashboard
        public DataTable Fetch_All_Survey()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from surveymaster order by displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }


        // Check Survey name
        public bool ChkDuplicate_surveyname(string surveyname)
        {
            bool SurveyExist = false;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select Count(*) From surveymaster where surveyname='" + surveyname.Replace(" '", "''") + "'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strname.ToString());
                if (retVal > 0)
                {
                    SurveyExist = true;
                }
                return SurveyExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch questiondetail for question Dashboard
        public DataTable Fetch_All_question(Int32 surveyid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from surveyquestions where surveyid=" + surveyid + " and isactive=True order by displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch questiondetail for question Dashboard for Report
        public DataTable Fetch_All_question_For_Report(Int32 surveyid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from surveyquestions where surveyid=" + surveyid + " and isactive=True and answertype<>'Text' and showreport='True' order by displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }


        public DataTable Fetch_All_question_Dashboard(Int32 surveyid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from surveyquestions where surveyid=" + surveyid + "  order by displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Insert question Detail for Survey
        public Int32 Insert_Survey_question(ClsSurveyquestions objSurvey)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into surveyquestions (surveyid,question,answertype,displayorder,isactive,showreport) VALUES(" );
                strInsert.Append(objSurvey.surveyid);
                strInsert.Append(" ,'");
                strInsert.Append(objSurvey.question.Trim().Replace(" '", "''"));
                strInsert.Append(" ','");
                strInsert.Append(objSurvey.answerType.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(objSurvey.displayorder);
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objSurvey.isactive);
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objSurvey.showreport);
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check question name
        public bool ChkDuplicate_question(string questionname, Int32 surveyid)
        {
            bool SurveyExist = false;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select Count(*) From surveyquestions where question='" + questionname + "' and surveyid=" + surveyid + "");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strname.ToString());
                if (retVal > 0)
                {
                    SurveyExist = true;
                }
                return SurveyExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // get amx display order for questions
        public Int32 Get_Max_question_dorder(Int32 surveyid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from surveyquestions where surveyid=" + surveyid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch question Detail By Survey question id
        public ClsSurveyquestions Fetch_question_Byid(Int32 surveyquestionid)
        {
            ClsSurveyquestions objQues = null;
            try
            {
                StringBuilder strques = new StringBuilder();
                strques.Append(" Select * from surveyquestions where surveyquestionid='" + surveyquestionid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strques.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objQues = new ClsSurveyquestions();
                        objQues.surveyid = Convert.ToInt32(dt.Rows[0]["surveyid"]);
                        objQues.surveyquestionid = Convert.ToInt32(dt.Rows[0]["surveyquestionid"]);
                        objQues.question = Convert.ToString(dt.Rows[0]["question"]);
                        objQues.answerType = Convert.ToString(dt.Rows[0]["answertype"]);
                        objQues.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objQues.isactive = true;
                            }
                            else
                            {
                                objQues.isactive = false;
                            }
                        }
                        else
                        {
                            objQues.isactive = false;
                        }
                        string showreport = Convert.ToString(Convert.ToString(dt.Rows[0]["showreport"]));
                        if (!string.IsNullOrEmpty(showreport))
                        {
                            if (showreport.ToUpper() == "TRUE")
                            {
                                objQues.showreport = true;
                            }
                            else
                            {
                                objQues.showreport = false;
                            }
                        }
                        else
                        {
                            objQues.showreport = false;
                        }
                    }
                }
                return objQues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //edit question detail
        public Int32 Update_Survey_question(ClsSurveyquestions clsquestion)
        {
            try
            {
                StringBuilder strUpdateCheck = new StringBuilder();
                strUpdateCheck.Append(" Update surveyquestions ");
                strUpdateCheck.Append(" Set ");
                strUpdateCheck.Append("  question=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsquestion.question);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append("  answertype=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsquestion.answerType);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append("  displayorder=");
                strUpdateCheck.Append(clsquestion.displayorder);
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" isactive=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsquestion.isactive);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" showreport=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsquestion.showreport);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append("  where surveyquestionid=" + clsquestion.surveyquestionid + "");
                return Service.DbFunctions.UpdateCommand(strUpdateCheck.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check duplicate answer for particular question
        public bool ChkDuplicate_answer(string answer, Int32 surveyquestionid)
        {
            bool answerExist = false;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select Count(*) From surveyanswers where answer='" + answer + "' and surveyquestionid=" + surveyquestionid + "");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strname.ToString());
                if (retVal > 0)
                {
                    answerExist = true;
                }
                return answerExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Insert answer Detail for Survey questions
        public Int32 Insert_Survey_answer(ClsSurveyanswers objanswer)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into surveyanswers (surveyquestionid,surveyid,answer,displayorder,isactive) VALUES(" );
                strInsert.Append(objanswer.surveyquestionid);
                strInsert.Append(" ,");
                strInsert.Append(objanswer.surveyid);
                strInsert.Append(" ,'");
                strInsert.Append(objanswer.answer.Trim().Replace(" '", "''"));
                strInsert.Append(" ',");
                strInsert.Append(objanswer.displayorder);
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objanswer.isactive);
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // get max display order for answer 
        public Int32 Get_Max_answer_dorder(Int32 surveyquestionid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from surveyanswers where surveyquestionid=" + surveyquestionid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch answer Detail for answer D
        public DataTable Fetch_All_answer_Byquestionid(Int32 surveyquestionid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from surveyanswers where surveyquestionid=" + surveyquestionid + " and isactive=True order by displayorder asc  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch answer Detail for answer D
        public DataTable Fetch_All_answer_Byquestionid_With_Response(Int32 surveyquestionid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                //strFetchData.Append("  Declare @TotalRecord Decimal ");
                //strFetchData.Append(" SET @TotalRecord=(Select COUNT(*) from survey_response_detail where questionid=" + surveyquestionid + ") ");
                //strFetchData.Append("  Select answer,surveyanswerid,surveyquestionid,surveyid, ");
                //strFetchData.Append("   (Select Cast(ROUND((COUNT(*)/@TotalRecord)*100,2) as Numeric(10,0)) from survey_response_detail where answerid=surveyanswers.surveyanswerid )as Total ");
                //strFetchData.Append("  from surveyanswers where surveyquestionid=" + surveyquestionid + " and isactive=True ");

                strFetchData.Append("select count(answerid) as TotalAnswer,sa.surveyanswerid,Cast(ROUND((count(answerid) * 100.0) / (Select COUNT(*)");
                strFetchData.Append(" from survey_response_detail where questionid = " + surveyquestionid + "), 2) as Numeric(10, 0)) as total from surveyanswers sa");
                strFetchData.Append(" left join survey_response_detail spd on spd.answerid = sa.surveyanswerid");
                strFetchData.Append(" where surveyquestionid = " + surveyquestionid + " group by sa.surveyanswerid");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        // get survey question by id
        public string Fetch_Survey_questionByid(Int32 surveyquestionid)
        {
            string retval = "";
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select question from surveyquestions where surveyquestionid=" + surveyquestionid + "");
                object obj = Service.DbFunctions.SelectString(strMax.ToString());
                if (obj != null)
                {
                    retval = Convert.ToString(obj);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch answer Detail for answer Dashboard 
        public DataTable Fetch_All_answer_Byquestionid_Dashboard(Int32 surveyquestionid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from surveyanswers where surveyquestionid=" + surveyquestionid + "  order by displayorder asc  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch answer Detail By Survey answer id
        public ClsSurveyanswers Fetch_answer_Byid(Int32 surveyanswerid)
        {
            ClsSurveyanswers objanswer = null;
            try
            {
                StringBuilder strques = new StringBuilder();
                strques.Append(" Select * from surveyanswers where surveyanswerid='" + surveyanswerid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strques.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objanswer = new ClsSurveyanswers();
                        objanswer.surveyid = Convert.ToInt32(dt.Rows[0]["surveyid"]);
                        objanswer.surveyanswerid = Convert.ToInt32(dt.Rows[0]["surveyanswerid"]);
                        objanswer.surveyquestionid = Convert.ToInt32(dt.Rows[0]["surveyquestionid"]);
                        objanswer.answer = Convert.ToString(dt.Rows[0]["answer"]);
                        objanswer.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objanswer.isactive = true;
                            }
                            else
                            {
                                objanswer.isactive = false;
                            }
                        }
                        else
                        {
                            objanswer.isactive = false;
                        }
                    }
                }
                return objanswer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //edit answer detail
        public Int32 Update_Survey_answer(ClsSurveyanswers clsanswer)
        {
            try
            {
                StringBuilder strUpdateCheck = new StringBuilder();
                strUpdateCheck.Append(" Update surveyanswers ");
                strUpdateCheck.Append(" Set ");
                strUpdateCheck.Append("  answer=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsanswer.answer);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append("  displayorder=");
                strUpdateCheck.Append(clsanswer.displayorder);
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" isactive=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsanswer.isactive);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append("  where surveyanswerid=" + clsanswer.surveyanswerid + "");
                return Service.DbFunctions.UpdateCommand(strUpdateCheck.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TO Update Survey Detail By_surveyid
        public Int32 Update_Survey_Detail_By_surveyid(ClsSurvey clsSurvey)
        {
            try
            {
                StringBuilder strUpdateCheck = new StringBuilder();
                strUpdateCheck.Append(" Update surveymaster ");
                strUpdateCheck.Append(" Set ");
                strUpdateCheck.Append(" surveyname=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsSurvey.surveyname);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" description=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsSurvey.description);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" displayorder=");
                strUpdateCheck.Append(clsSurvey.displayorder);
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" defaultactive=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsSurvey.defaultactive);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" showreport=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsSurvey.showreport);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" modifiedby=");
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(clsSurvey.modifiedby);
                strUpdateCheck.Append(" '");
                strUpdateCheck.Append(" ,");
                strUpdateCheck.Append(" modifieddate=");
                strUpdateCheck.Append(" LOCALTIMESTAMP");
                strUpdateCheck.Append("  where surveyid=" + clsSurvey.surveyid + "");
                return Service.DbFunctions.UpdateCommand(strUpdateCheck.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check Survey name for update
        public bool ChkDuplicate_surveyname_ForUpdate(string surveyname, Int32 surveyid)
        {
            bool SurveyExist = false;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select Count(*) From surveymaster where surveyname='" + surveyname + "' and surveyid!=" + surveyid + " ");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strname.ToString());
                if (retVal > 0)
                {
                    SurveyExist = true;
                }
                return SurveyExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // to delete Survey by surveyid id
        public Int32 Delete_Survey(Int32 surveyid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveyanswers ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyid=");
                strDelete.Append(surveyid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());

                strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveyquestions ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyid=");
                strDelete.Append(surveyid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());

                strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveymaster ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyid=");
                strDelete.Append(surveyid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // to delete Survey question and answer by surveyquestionid
        public Int32 Delete_SurveyByquestionid(Int32 surveyquestionid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveyquestions ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyquestionid=");
                strDelete.Append(surveyquestionid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveyanswers ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyquestionid=");
                strDelete.Append(surveyquestionid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // to delete Survey  answer by surveyquestionid
        public Int32 Delete_SurveyanswerByquestionid(Int32 surveyquestionid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveyanswers ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyanswerid=");
                strDelete.Append(surveyquestionid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // find default set Survey id
        public Int32 Fetch_DefaultSet_Survey()
        {
            Int32 surveyid = 0;
            try
            {
                StringBuilder strdefaultactive = new StringBuilder();
                strdefaultactive.Append(" Select surveyid From surveymaster where defaultactive=True  ");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strdefaultactive.ToString());
                if (retVal > 0)
                {
                    surveyid = retVal;
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check Show report active or not
        public Int32 CheckshowreportStatus_Bysurveyid(Int32 surveyid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strdefaultactive = new StringBuilder();
                strdefaultactive.Append("  Select showreport From surveymaster where surveyid=" + surveyid + "  ");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strdefaultactive.ToString());
                if (retVal > 0)
                {
                    retval = retVal;
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check question Show result status by question id
        public Int32 CheckshowreportStatus_Forquestion_Byquestionid(Int32 surveyquestionid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strdefaultactive = new StringBuilder();
                strdefaultactive.Append("  Select showreport from surveyquestions where surveyquestionid=" + surveyquestionid + "  ");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strdefaultactive.ToString());
                if (retVal > 0)
                {
                    retval = retVal;
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check response for  existing ip
        public bool Check_Exist_Response(string ipaddress, Int32 surveyid, string sipaddress)
        {
            bool checkexist = false;
            Int32 retVal;
            StringBuilder Query = new StringBuilder();
            Query.Append(" Select count(*) from survey_response where ipaddress='" + ipaddress + "'and sipaddress='" + sipaddress + "' and surveyid=" + surveyid + "");
            retVal = Service.DbFunctions.SelectCount(Query.ToString());
            if (retVal > 0)
            {
                checkexist = true;
            }
            return checkexist;
        }



        // Insert Voting Detail for Survey questions
        public string Insert_Survey_Vote(List<ClsSurveyResponse> objResponse)
        {
            string RetVal = "";
            bool checkexist = false;
            Int32 retVal;
            try
            {
                List<string> objQueryList = new List<string>();
                ClsSurveyResponse objMaster = objResponse[0];
                StringBuilder Query = new StringBuilder();
                if (objResponse != null && objResponse.Count > 0)
                {
                    Query.Append(" Select count(*) from survey_response where ipaddress='" + objMaster.ipaddress + "' and sipaddress='" + objMaster.sipaddress + "' and surveyid=" + objMaster.surveyid + ";");
                    retVal = Service.DbFunctions.SelectCount(Query.ToString());
                    if (retVal > 0)
                    {
                        checkexist = true;
                    }
                    if (checkexist == false)
                    {
                        Query.Append(" Insert Into survey_response (surveyid,ipaddress,sipaddress,responsedate) VALUES(" );
                        Query.Append(objMaster.surveyid);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objMaster.ipaddress);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objMaster.sipaddress);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" LOCALTIMESTAMP");
                        Query.Append(" )");
                        objQueryList.Add(Query.ToString());
                    }
                    for (Int32 i = 1; i < objResponse.Count; i++)
                    {
                        Query = new StringBuilder();
                        Query.Append(" Insert into survey_response_detail");
                        Query.Append(" (" );
                        Query.Append(" responseid,questionid,answerid,responsetext,responsetype,answeredOn,additionaltext");
                        Query.Append(" )");
                        Query.Append("  Select COALESCE(MAX(responseid),0) as responseid ");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objResponse[i].questionid);
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(objResponse[i].answerid);
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objResponse[i].responsetext.Trim().Replace(" '", "''"));
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objResponse[i].responsetype.Trim().Replace(" '", "''"));
                        Query.Append(" '");
                        Query.Append(" ,");
                        Query.Append(" LOCALTIMESTAMP");
                        Query.Append(" ,");
                        Query.Append(" '");
                        Query.Append(objResponse[i].additionaltext.Trim().Replace(" '", "''"));
                        Query.Append(" '");
                        Query.Append("  from survey_response where surveyid=" + objMaster.surveyid + "");
                        objQueryList.Add(Query.ToString());
                    }
                    if (objQueryList != null && objQueryList.Count > 0)
                    {
                        RetVal = Service.DbFunctions.Execute_Multiple_Query(objQueryList);
                    }
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Survey Reports
        //====================================================DETAILED SURVEY REPORT FUNCTION
        // To Bind the Survey Detail
        public DataTable Fetch_Survey_Detail_By_id(string surveyid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strData = new StringBuilder();
                strData.Append(" Select * from surveymaster where surveyid=" + surveyid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Survey Detail Dashboard
        public DataTable Fetch_Detailed_Survey_Dashboard(string surveyid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strData = new StringBuilder();
                strData.Append(" Select responseid,ipaddress,to_char(responsedate, 'DD/MM/YYYY') responsedate from survey_response where surveyid=" + surveyid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // to delete Survey answer by surveyid id
        public Int32 Delete_Survey_answer_Byquestionid(Int32 surveyquestionid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from surveyanswers ");
                strDelete.Append(" Where ");
                strDelete.Append(" surveyquestionid=");
                strDelete.Append(surveyquestionid);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Fetch Response question List in case of Multiple valued
        public DataTable Fetch_Response_By_questionList(string surveyid, string questionid, string fieldtype, string responseid)
        {
            DataTable dt = null;
            string RetVal = "";
            string Dataval = "";
            DataTable dtanswer = null;
            DataTable dtActResponse = null;
            try
            {
             
                StringBuilder strResponse = new StringBuilder();
                string ActResponse = "Select responsetext from survey_response_detail where responseid=" + responseid + " and responsetype='" + fieldtype + "' and questionid=" + questionid + " ";
                DataSet ds = Service.DbFunctions.SelectCommand(ActResponse.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtActResponse = ds.Tables[0];
                }
                foreach (DataRow rw in dtActResponse.Rows)
                {
                    RetVal = RetVal + Convert.ToString(rw["responsetext"]) + "~";
                }
                string[] strResponsedataList = RetVal.Split('~');
                strResponse = new StringBuilder();
                strResponse.Append(" select surveyanswers.answer from surveyanswers inner join surveyquestions on surveyquestions.surveyquestionid=surveyanswers.surveyquestionid where surveyquestions.surveyid=" + surveyid + " and surveyanswers.surveyquestionid=" + questionid + "");
                DataSet ds1 = Service.DbFunctions.SelectCommand(strResponse.ToString());
                if (ds1 != null && ds1.Tables.Count > 0)
                {
                    dtanswer = ds1.Tables[0];
                }
                dt = new DataTable();
                dt.Columns.Add(" dorder", typeof(Int32));
                dt.Columns.Add(" OptionVal", typeof(String));
                dt.Columns.Add(" Checked", typeof(String));
                dt.Columns.Add(" questionid", typeof(String));
                if (fieldtype == "Check")
                {
                    for (Int32 i = 0; i < dtanswer.Rows.Count; i++)
                    {
                        dt.Rows.Add(i + 1, Convert.ToString(dtanswer.Rows[i]["answer"]), "", questionid);
                    }
                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        for (Int32 j = 0; j < dtActResponse.Rows.Count; j++)
                        {
                            for (Int32 k = 0; k < strResponsedataList.Length - 1; k++)
                            {
                                if (Convert.ToString(strResponsedataList[k]) == Convert.ToString(dtanswer.Rows[i]["answer"]))
                                {
                                    dt.Rows[i]["Checked"] = "Checked";
                                    dt.AcceptChanges();
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (Int32 i = 0; i < dtanswer.Rows.Count; i++)
                    {
                        if (strResponsedataList[0].ToString() == Convert.ToString(dtanswer.Rows[i]["answer"]))
                        {
                            dt.Rows.Add(i + 1, Convert.ToString(dtanswer.Rows[i]["answer"]), "checked", questionid);
                        }
                        else
                        {
                            dt.Rows.Add(i + 1, Convert.ToString(dtanswer.Rows[i]["answer"]), "", questionid);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // To Fetch Response question List in case of Multiple valued
        public DataTable Fetch_Response_By_AllquestionList(string fieldtype, string questionid)
        {
            DataTable dt = null;
            string RetVal = "";
            string Dataval = "";
            DataTable dtanswer = null;
            DataTable dtActResponse = null;
            try
            {
                StringBuilder strResponse = new StringBuilder();
                string ActResponse = "Select responsetext from survey_response_detail where  responsetype='" + fieldtype + "'";
                DataSet ds = Service.DbFunctions.SelectCommand(ActResponse.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtActResponse = ds.Tables[0];
                }
                foreach (DataRow rw in dtActResponse.Rows)
                {
                    RetVal = RetVal + Convert.ToString(rw["responsetext"]) + "~";
                }
                string[] strResponsedataList = RetVal.Split('~');
                strResponse = new StringBuilder();
                strResponse.Append(" select surveyanswers.answer from surveyanswers inner join surveyquestions on surveyquestions.surveyquestionid=surveyanswers.surveyquestionid");
                DataSet ds1 = Service.DbFunctions.SelectCommand(strResponse.ToString());
                if (ds1 != null && ds1.Tables.Count > 0)
                {
                    dtanswer = ds1.Tables[0];
                }
                dt = new DataTable();
                dt.Columns.Add(" dorder", typeof(Int32));
                dt.Columns.Add(" OptionVal", typeof(String));
                dt.Columns.Add(" Checked", typeof(String));
                dt.Columns.Add(" questionid", typeof(String));
                if (fieldtype == "Check")
                {
                    for (Int32 i = 0; i < dtanswer.Rows.Count; i++)
                    {
                        dt.Rows.Add(i + 1, Convert.ToString(dtanswer.Rows[i]["answer"]), "", questionid);
                    }
                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        for (Int32 j = 0; j < dtActResponse.Rows.Count; j++)
                        {
                            for (Int32 k = 0; k < strResponsedataList.Length - 1; k++)
                            {
                                if (Convert.ToString(strResponsedataList[k]) == Convert.ToString(dtanswer.Rows[i]["answer"]))
                                {
                                    dt.Rows[i]["Checked"] = "Checked";
                                    dt.AcceptChanges();
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (Int32 i = 0; i < dtanswer.Rows.Count; i++)
                    {
                        if (strResponsedataList[0].ToString() == Convert.ToString(dtanswer.Rows[i]["answer"]))
                        {
                            dt.Rows.Add(i + 1, Convert.ToString(dtanswer.Rows[i]["answer"]), "checked", questionid);
                        }
                        else
                        {
                            dt.Rows.Add(i + 1, Convert.ToString(dtanswer.Rows[i]["answer"]), "", questionid);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // To Fetch Survey Response Year
        public DataTable Fetch_Survey_Response_Year(string surveyid)
        {
            DataTable dtReponseYear = null;
            try
            {
                StringBuilder strResponseYear = new StringBuilder();
                strResponseYear.Append(" Select '0' as ResponseYearid,'---Select Year---' as ReponseYear ");
                strResponseYear.Append("  UNION ");
                strResponseYear.Append(" Select DISTINCT  to_char(responsedate, 'YYYY') ResponseYearid,to_char(responsedate, 'YYYY') ResponseYear From survey_response ");
                strResponseYear.Append("  where surveyid=" + surveyid + " ");
                strResponseYear.Append(" Order by ResponseYearid Asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strResponseYear.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtReponseYear = ds.Tables[0];
                }
                return dtReponseYear;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Survey Response Year
        public DataTable Fetch_Survey_Response_Month(string surveyid)
        {
            DataTable dtReponseMonth = null;
            try
            {
                StringBuilder strResponseMonth = new StringBuilder();
                strResponseMonth.Append(" Select '0' as ReponseMonthid,'---Select Month---' as ResponseMonth ");
                strResponseMonth.Append("  UNION ");
                strResponseMonth.Append("  Select Distinctto_char(responsedate, 'MM') ReponseMonthid,to_char(responsedate, 'MM') ReponseMonth from survey_response ");
                strResponseMonth.Append("  where surveyid=" + surveyid + " ");
                strResponseMonth.Append("  order by ReponseMonthid asc ");
                DataSet ds = Service.DbFunctions.SelectCommand(strResponseMonth.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtReponseMonth = ds.Tables[0];
                }
                return dtReponseMonth;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Fetch the Fall year Summary and Chart data of the selected criteria 
        public DataTable Get_Survey_Summary_Fall_Year(string surveyid, Int32 Year, Int32 Month)
        {
            DataTable DtTempFallYear = new DataTable();
            try
            {
                DtTempFallYear.Columns.Add(" FallYear", typeof(int));
                DtTempFallYear.Columns.Add(" FallMonthid", typeof(int));
                DtTempFallYear.Columns.Add(" FallMonth", typeof(string));
                DtTempFallYear.Columns.Add(" TotalResponses", typeof(int));
                DtTempFallYear.Columns.Add(" NoOfReposesPart", typeof(int));
                DtTempFallYear.Columns.Add(" NoOfPartLeft", typeof(int));
                DtTempFallYear.Columns.Add(" TotalFYResponse", typeof(int));

                string Monthname = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month).ToString();
                string DateVal = Convert.ToString(Year) + "-" + Convert.ToString(Month) + "-01";
                DtTempFallYear.Rows.Add(Year, Month, Monthname, 0, 0, 0, 0);

                string strResCount = "Select COUNT(*) FROM survey_response where YEAR(responsedate)=" + Year + " and MONTH(responsedate)=" + Month + " and surveyid=" + surveyid;
                Int32 ReponseCount = Convert.ToInt32(Service.DbFunctions.SelectCount(strResCount));
                DtTempFallYear.Rows[0]["TotalResponses"] = ReponseCount;
                DtTempFallYear.AcceptChanges();

                string strRespondents = "Select COUNT(*) from survey_response where YEAR(responsedate)=" + Year + " and MONTH(responsedate)=" + Month + " and surveyid=" + surveyid;
                Int32 NoOfRespondents = Convert.ToInt32(Service.DbFunctions.SelectCount(strRespondents));
                DtTempFallYear.Rows[0]["NoOfReposesPart"] = NoOfRespondents;
                DtTempFallYear.AcceptChanges();

                string strRespondentsLeft = "Select COUNT(*) from Survey_Response_Left where YEAR(responsedate)=" + Year + " and MONTH(responsedate)=" + Month + " and surveyid=" + surveyid;
                Int32 NoOfRespondentsLeft = Convert.ToInt32(Service.DbFunctions.SelectCount(strRespondentsLeft));
                DtTempFallYear.Rows[0]["NoOfPartLeft"] = NoOfRespondentsLeft;
                DtTempFallYear.AcceptChanges();
                Int32 TotalFYResponses = NoOfRespondents + NoOfRespondentsLeft;
                DtTempFallYear.Rows[0]["TotalFYResponse"] = TotalFYResponses;
                DtTempFallYear.AcceptChanges();
                return DtTempFallYear;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch the Survey questions       
        public DataTable Fetch_Survey_questions(string surveyid)
        {
            DataTable dtquestion = null;
            try
            {
                StringBuilder strquestion = new StringBuilder();
                strquestion.Append(" Select surveyquestionid,surveyid,answertype,displayorder,question, ");
                strquestion.Append(" Case when answertype='Check' then 'Check'  when answertype='Option' then 'Option' WHEN answertype='DropDown' then 'DropDown' else 'Text'  END answertype ");
                strquestion.Append(" from surveyquestions where surveyid=" + surveyid);
                strquestion.Append(" and answertype NOt IN ('Section Break','Single Line Text','Upload image','Content','Section Break','Content','Title','Empty Space','Paragraph Text')");
                strquestion.Append(" order by displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strquestion.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtquestion = ds.Tables[0];
                }
                return dtquestion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch the Survey questions       
        public DataTable Fetch_Survey_Allquestions(string surveyid)
        {
            DataTable dtquestion = null;
            try
            {
                StringBuilder strquestion = new StringBuilder();
                strquestion.Append(" Select surveyquestionid,surveyid,answertype,displayorder,question, ");
                strquestion.Append(" Case when answertype='Check' then 'Check'  when answertype='Option' then 'Option' WHEN answertype='DropDown' then 'DropDown' else 'Text'  END answertype ");
                strquestion.Append(" from surveyquestions where ");
                strquestion.Append("  answertype NOt IN ('Section Break','Single Line Text','Upload image','Content','Section Break','Content','Title','Empty Space','Paragraph Text')");
                strquestion.Append(" order by displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strquestion.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtquestion = ds.Tables[0];
                }
                return dtquestion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_Survey_answer_Byquestionid(string questionid, string surveyid)
        {
            DataTable dtquestion = null;
            try
            {
                StringBuilder strquestion = new StringBuilder();
                strquestion.Append(" Select answer from surveyanswers where surveyquestionid=" + questionid + " and surveyid=" + surveyid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strquestion.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtquestion = ds.Tables[0];
                }
                return dtquestion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch the Survey Resp
        public DataTable Fetch_questions_Response(string surveyid, string questionType, string questionid, string fieldtype, string Year, string Month)
        {
            DataTable DtTempResponse = null;
            try
            {
                DataTable dtquestion = Fetch_Survey_questions(surveyid);
                string questionText = "";
                string dorder = "";
                string TotalResponse = "";
                string ResponseCount = "";
                string Percentage = "";
                Single perc = 0.0f;
                if (dtquestion != null && dtquestion.Rows.Count > 0)
                {

                    switch (questionType)
                    {
                        case "Text":
                            //============ FOR REPONSE TEXT
                            DtTempResponse = new DataTable();
                            DtTempResponse.Columns.Add(" answerText", typeof(string));
                            DtTempResponse.Columns.Add(" dorder", typeof(string));
                            DtTempResponse.Columns.Add(" TotalResponse", typeof(string));
                            DtTempResponse.Columns.Add(" ResponseCount", typeof(string));
                            DtTempResponse.Columns.Add(" Percentage", typeof(string));

                            questionText = "";
                            TotalResponse = GetResponseDataText(surveyid, questionType, questionid, fieldtype, Year, Month, "", "");
                            ResponseCount = GetResponseDataText(surveyid, questionType, questionid, fieldtype, Year, Month, "and fieldvalue<>''", "");
                            perc = Convert.ToSingle(ResponseCount) * 100 / Convert.ToSingle(TotalResponse);
                            Percentage = Convert.ToString(Math.Round(perc, 0)) + "%";
                            DtTempResponse.Rows.Add(questionText, "1", TotalResponse, ResponseCount, Percentage);

                            //============ FOR EMPTY RESPONSE
                            questionText = "Empty...";
                            TotalResponse = GetResponseDataText(surveyid, questionType, questionid, fieldtype, Year, Month, "", "");
                            ResponseCount = GetResponseDataText(surveyid, questionType, questionid, fieldtype, Year, Month, "and fieldvalue=''", "");
                            perc = Convert.ToSingle(ResponseCount) * 100 / Convert.ToSingle(TotalResponse);
                            Percentage = Convert.ToString(Math.Round(perc, 0)) + "%";
                            DtTempResponse.Rows.Add(questionText, "2", TotalResponse, ResponseCount, Percentage);

                            break;
                        default:
                            string Items = GetResponseDataText(surveyid, questionType, questionid, fieldtype, Year, Month, "", "heading");
                            DtTempResponse = GetResponseDataOption(surveyid, questionType, questionid, fieldtype, Year, Month, Items);
                            break;
                    }
                }
                return DtTempResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Response Data Text Type Field
        protected string GetResponseDataText(string surveyid, string questionType, string questionid, string fieldtype, string Year, string Month, string Condition, string returnType)
        {

            string RetVal = "";
            string Query = "";
            try
            {
                DataTable dt = null;
                if (returnType == "heading")
                {
                    Query = "Select answer heading from surveyanswers where surveyquestionid=" + questionid + "";
                }
                else
                {
                    switch (questionType)
                    {
                        case "Text":
                            Query = "Select answertype,COUNT(fieldvalue) TotalReponses from survey_response_detail INNER JOIN survey_response ON survey_response_detail.responseid=survey_response.responseid ";
                            Query = Query + " and YEAR(responsedate)=" + Year + " and MONTH(responsedate)=" + Month + " and surveyid=" + surveyid + "";
                            Query = Query + " and answertype='" + fieldtype + "' ";
                            Query = Query + " " + Condition + "";
                            Query = Query + " GROUP BY answertype";
                            break;
                        default:
                            break;
                    }
                }
                DataSet ds = Service.DbFunctions.SelectCommand(Query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (returnType == "heading")
                    {
                        foreach (DataRow rw in dt.Rows)
                        {
                            RetVal = RetVal + Convert.ToString(rw["heading"]) + "~";
                        }
                    }
                    else
                    {
                        RetVal = Convert.ToString(dt.Rows[0]["TotalReponses"]);
                    }
                }
                else
                {
                    if (returnType == "heading")
                    {
                        RetVal = "";
                    }
                    else
                    {
                        RetVal = "0";
                    }

                }
                return RetVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Response Data Option Type Field
        protected DataTable GetResponseDataOption(string surveyid, string questionType, string questionid, string fieldtype, string Year, string Month, string Items)
        {

            string[] ItemValList = null;
            string[] tempItemValList = null;
            string Query = "";
            DataTable dt = null;

            string questionText = "";
            string dorder = "";
            string TotalResponse = "0";
            string ResponseCount = "0";
            string Percentage = "0";
            Single perc = 0.0f;
            string FieldDataType = "";
            try
            {

                if (!string.IsNullOrEmpty(Items))
                {
                    ItemValList = Items.Split('~');
                }
                if (ItemValList != null && ItemValList.Length > 0)
                {
                    dt = new DataTable();
                    dt.Columns.Add(" answerText", typeof(string));
                    dt.Columns.Add(" dorder", typeof(string));
                    dt.Columns.Add(" TotalResponse", typeof(string));
                    dt.Columns.Add(" ResponseCount", typeof(string));
                    dt.Columns.Add(" Percentage", typeof(string));

                    Query = "Select responsetext,COUNT(*) TotalResponses from survey_response_detail ";
                    Query = Query + " INNER JOIN survey_response ON survey_response.responseid=survey_response_detail.responseid ";
                    Query = Query + " where questionid='" + questionid + "' and surveyid=" + surveyid + " and YEAR(responsedate)=" + Year + " and MONTH(responsedate)=" + Month + " and responsetext<>''";
                    Query = Query + " Group by responsetype,responsetext";

                    DataSet ds = Service.DbFunctions.SelectCommand(Query);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        DataTable dtTemp = ds.Tables[0];
                        if (dtTemp != null && dtTemp.Rows.Count > 0)
                        {

                            for (Int32 i = 0; i < ItemValList.Length - 1; i++)
                            {
                                dt.Rows.Add(ItemValList[i].Trim(), "0", "0", "0", "0");

                            }
                            for (Int32 i = 0; i < dt.Rows.Count; i++)
                            {

                                for (Int32 j = 0; j < dtTemp.Rows.Count; j++)
                                {
                                    if (dtTemp.Rows[j]["responsetext"] != null)
                                    {
                                        tempItemValList = dtTemp.Rows[j]["responsetext"].ToString().Trim().Split('~');
                                        for (int k = 0; k < tempItemValList.Length; k++)
                                        {

                                            if (Convert.ToString(dt.Rows[i]["answerText"]).Trim() == tempItemValList[k].ToString().Trim())
                                            {
                                                if (tempItemValList.Length > 1)
                                                {
                                                    dt.Rows[i]["ResponseCount"] = Convert.ToString(Convert.ToInt32(dt.Rows[i]["ResponseCount"]) + 1);
                                                    TotalResponse = Convert.ToString(Convert.ToInt32(TotalResponse) + Convert.ToInt32(dtTemp.Rows[j]["TotalResponses"].ToString()));
                                                    dt.AcceptChanges();
                                                }
                                                else
                                                {
                                                    dt.Rows[i]["dorder"] = Convert.ToString(i + 1);
                                                    if (dt.Rows[i]["ResponseCount"].ToString() == "0")
                                                    {
                                                        dt.Rows[i]["ResponseCount"] = Convert.ToString(dtTemp.Rows[j]["TotalResponses"]).Trim();
                                                    }
                                                    else
                                                    {
                                                        dt.Rows[i]["ResponseCount"] = Convert.ToString(Convert.ToInt32(dt.Rows[i]["ResponseCount"]) + 1);
                                                    }
                                                    TotalResponse = Convert.ToString(Convert.ToInt32(TotalResponse) + Convert.ToInt32(dtTemp.Rows[j]["TotalResponses"].ToString()));
                                                    dt.AcceptChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (Int32 i = 0; i < ItemValList.Length; i++)
                        {
                            dt.Rows.Add(questionText, Convert.ToString(i + 1), "0", "0", "0%");
                        }
                    }
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["TotalResponse"] = TotalResponse;
                        perc = Convert.ToSingle(Convert.ToString(dt.Rows[i]["ResponseCount"])) * 100 / Convert.ToSingle(TotalResponse);
                        Percentage = Convert.ToString(Math.Round(perc, 0)) + "%";
                        dt.Rows[i]["Percentage"] = Percentage;
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Headeing of a survey response
        public string Fetch_Response_By_questionheading(string responseid, string heading, string questionid)
        {
            string retval = "";
            DataTable dt = null;
            try
            {
                StringBuilder strResponse = new StringBuilder();
                strResponse.Append(" Select responsetext from survey_response_detail where responseid=" + responseid + " and responsetype='" + heading + "' and questionid=" + questionid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strResponse.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                foreach (DataRow rw in dt.Rows)
                {
                    retval = retval + Convert.ToString(rw["responsetext"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retval;
        }

        // To Fetch Headeing of a survey response
        public string Fetch_Response_By_Allquestionheading(string heading, string questionid)
        {
            string retval = "";
            DataTable dt = null;
            try
            {
                StringBuilder strResponse = new StringBuilder();
                strResponse.Append(" Select responsetext from survey_response_detail where responsetype='" + heading + "' and questionid=" + questionid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strResponse.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                foreach (DataRow rw in dt.Rows)
                {
                    retval = retval + Convert.ToString(rw["responsetext"]);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return retval;
        }

        // Function for Exporting All survey Reponse Details 20160203
        public DataTable Get_All_Survey_Responses(Int32 surveyid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strResponse = new StringBuilder();
                strResponse.Append(" Select surveymaster.surveyname,surveymaster.description,survey_response.ipaddress,to_char(survey_response.responsedate, 'DD/MM/YYYY') responsedate, ");
                strResponse.Append("  survey_response_detail.questionid,surveyquestions.question,surveyquestions.answertype ");
                strResponse.Append("  ,survey_response_detail.answerid,survey_response.responsetext answer ");
                strResponse.Append("  from survey_response  ");
                strResponse.Append("  left JOIN survey_response_detail ON survey_response_detail.responseid=survey_response.responseid ");
                strResponse.Append("  left JOIN surveymaster ON surveymaster.surveyid=survey_response.surveyid ");
                strResponse.Append("  left JOIN surveyquestions ON surveyquestions.surveyquestionid=survey_response_detail.questionid ");
                strResponse.Append("  left JOIN surveyanswers ON surveyanswers.surveyanswerid=survey_response_detail.answerid  ");
                strResponse.Append("  and surveyanswers.surveyquestionid=surveyquestions.surveyquestionid ");
                strResponse.Append("  where surveymaster.surveyid=" + surveyid + " ");
                strResponse.Append("  Order by survey_response.responsedate,survey_response.ipaddress,survey_response_detail.questionid ");
                strResponse.Append("  ,survey_response_detail.answerid");
                DataSet ds = Service.DbFunctions.SelectCommand(strResponse.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //================= REPORT FUNCTIONS END HERE
        #endregion

        #region Event Function
        //Fetch cmsid of even by Display Order and active status
        public DataTable Fetch_cmsid_For_Event(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q2.cmsid, cast(q2.fieldvalue as int) dorder FROM");
                strCMS.Append("  ( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtype='Checkbox' and fieldvalue='True'");
                strCMS.Append("  )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                strCMS.Append("  ) q2");
                strCMS.Append("  ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch event By CMS id

        public DataTable Fetch_For_Open_And_Close_Event_ByDate(Int32 pageid)
        {

            DataTable dtCMS = null;
            Int32 RetCount = 0;
            try
            {
                //Check the Main Menu active Status
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" SELECT name.fieldvalue Eventname,EventStatus.fieldvalue EventStatus  FROM");
                strCMS.Append("  ( Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + "  and fieldtext like '%From date%' )  FromDate ");
                strCMS.Append("  LEFT JOIN  ( Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%To date%' ) ToDate");
                strCMS.Append("  ON FromDate.cmsid = ToDate.cmsid ");
                strCMS.Append("  LEFT JOIN  ( Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where  pageid=" + pageid + " and fieldtext like '%Name%' ) name");
                strCMS.Append("  ON ToDate.cmsid = name.cmsid ");
                strCMS.Append("   LEFT JOIN  ( Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where  pageid=" + pageid + " and fieldtype like '%DropDown%' ) EventStatus");
                strCMS.Append("  ON name.cmsid = EventStatus.cmsid");
                strCMS.Append("   where to_char(LOCALTIMESTAMP, 'DD/MM/YYYY') between FromDate.fieldvalue and ToDate.fieldvalue ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // to fetch all event by cmsid
        public DataTable Fetch_All_Event_Bycmsid(Int32 pageid)
        {

            DataTable dtCMS = null;
            Int32 RetCount = 0;
            try
            {
                //Check the Main Menu active Status
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  Select * from (SELECT DISTINCT FromDate.cmsid cmsid,FromDate.fieldvalue EventDate, name.fieldvalue Eventname,EventStatus.fieldvalue EventStatus,active.fieldvalue active  ");
                strCMS.Append("  FROM (  ");
                strCMS.Append("  Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master  ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid where pageid=" + pageid + "  and fieldtext like '%From Date%')  FromDate   ");
                strCMS.Append("  LEFT JOIN  (  ");
                strCMS.Append("  Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master   ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where pageid=" + pageid + "  ");
                strCMS.Append("  and fieldtext like '%To Date%' ) ToDate ON FromDate.cmsid = ToDate.cmsid   ");
                strCMS.Append("  LEFT JOIN  (  ");
                strCMS.Append("  Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master   ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid   ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Name%' AND fieldtext NOT like '%Country Name%' ) name ON ToDate.cmsid = name.cmsid ");
                strCMS.Append("  LEFT JOIN  ( Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master   ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where  pageid=" + pageid + " and fieldtype like '%Dropdown%' )  ");
                strCMS.Append("  EventStatus ON name.cmsid = EventStatus.cmsid ");
                strCMS.Append("  LEFT JOIN  (  ");
                strCMS.Append("  Select content_management_detail.cmsid, content_management_detail.fieldvalue from content_management_master   ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where  pageid=" + pageid + " and fieldtype like '%Checkbox%' ");
                strCMS.Append("  and fieldvalue IN('True')) active  ");
                strCMS.Append("  ON EventStatus.cmsid = active.cmsid where active.fieldvalue='True' )A  ");
                strCMS.Append("  LEFT JOIN( ");
                strCMS.Append("  Select content_management_detail.cmsid, content_management_detail.fieldvalue as Countryname from content_management_master   ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid   ");
                strCMS.Append("  where pageid=" + pageid + " AND fieldtext like '%Country Name%') Country ON A.cmsid = Country.cmsid ");
                strCMS.Append("  order by A.EventDate ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Video
        //UPDATE Video name
        public Int32 Update_Video_name(Int32 cmsid, Int32 displayorder, string filename)
        {
            try
            {
                StringBuilder strquery = new StringBuilder();
                strquery.Append(" Update content_management_detail ");
                strquery.Append(" Set ");
                strquery.Append(" fieldvalue=");
                strquery.Append(" '");
                strquery.Append(filename.Trim().Replace(" '", "''"));
                strquery.Append(" '");
                strquery.Append("  Where");
                strquery.Append("  cmsid=");
                strquery.Append(cmsid);
                strquery.Append("  and");
                strquery.Append("  dorder=");
                strquery.Append(" '");
                strquery.Append(displayorder.ToString().Trim());
                strquery.Append(" '");
                return Service.DbFunctions.UpdateCommand(strquery.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        //Start of Front End Function
        #region Menus Functions for Menu
        // To Fetch cmsid for Header,Footer Menus By pageid order by Display order
        public DataTable Fetch_Main_Menu_Dashboard_Bypagename(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  Select content_management_detail.cmsid,cast(content_management_detail.fieldvalue as int) dorder from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch content active status
        public DataTable Fetch_displayorder_Bycmsid(Int32 pageid, Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  Select content_management_detail.cmsid,cast(content_management_detail.fieldvalue as nvarchar) isactive from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Is Active%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch content active status for draft
        public DataTable Fetch_displayorder_Bycmsid_forDraft(Int32 pageid, Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  Select content_management_detail_draft.cmsid,cast(content_management_detail_draft.fieldvalue as nvarchar) isactive from content_management_master_draft ");
                strCMS.Append("  Inner join content_management_detail_draft On");
                strCMS.Append("  content_management_detail_draft.cmsid=content_management_master_draft.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and content_management_detail_draft.cmsid=" + cmsid + " and fieldtext like '%Is Active%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Menu Details By CMS id
        public DataTable Fetch_Menu_Detail_By_cmsid(Int32 cmsid)
        {
            DataTable dtCMS = null;
            Int32 RetCount = 0;
            try
            {
                //Check the Main Menu active Status
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select COUNT(*) from content_management_detail where cmsid=" + cmsid + " and (fieldtype='Checkbox' and fieldvalue='True') ");
                object objCount = Service.DbFunctions.SelectCount(strCMS.ToString());
                if (objCount != null)
                {
                    RetCount = Convert.ToInt32(objCount);
                    if (RetCount > 0)
                    {
                        strCMS = new StringBuilder();
                        strCMS.Append(" Select fieldtext,COALESCE(fieldvalue,'') Menuname,dorder from content_management_detail where cmsid=" + cmsid + " and (fieldtext Like '%Name%' OR fieldtext Like '%URL%')");
                        DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            DataTable dt = ds.Tables[0];
                            dtCMS = SortTable(dt, "dorder");
                        }
                    }
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======FOR THE PURPOSE TO GET Menu name id BASED ON submenu name
        public DataTable Fetch_Menuname_By_submodulename(Int32 Menuid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select fieldvalue from content_management_detail where cmsid=" + Menuid + " and fieldtype='Dropdown'");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //// To Fetch Menu Details By CMS id

        public DataTable Fetch_Contentdisplayorder_By_cmsid(Int32 cmsid)
        {
            DataTable dtCMS = null;
            Int32 RetCount = 0;
            try
            {
                //Check the Main Menu active Status
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select COUNT(*) from content_management_detail where cmsid=" + cmsid + "  ");
                object objCount = Service.DbFunctions.SelectCount(strCMS.ToString());
                if (objCount != null)
                {
                    RetCount = Convert.ToInt32(objCount);
                    if (RetCount > 0)
                    {
                        strCMS = new StringBuilder();
                        strCMS.Append(" SELECT q1.cmsid ,q1.fieldvalue as ContentValue,cast(q2.fieldvalue as int) displayorder,cast(q2.fieldvalue as int) isactive FROM ");
                        strCMS.Append("  ( Select  content_management_detail.cmsid,content_management_detail.fieldtext,content_management_detail.fieldvalue,content_management_detail.dorder");
                        strCMS.Append("  from content_management_master  Inner join content_management_detail");
                        strCMS.Append("  On content_management_detail.cmsid=content_management_master.cmsid   ");
                        strCMS.Append("  where content_management_detail.cmsid=" + cmsid + " order by content_management_detail.dorder  )  q1");
                        strCMS.Append("  LEFT JOIN   ( Select content_management_detail.cmsid,content_management_detail.fieldtext,content_management_detail.fieldvalue  ");
                        strCMS.Append("  from content_management_master Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                        strCMS.Append("   where  content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Display Order%' )  q2  ON q1.cmsid = q2.cmsid limit 1 ");
                        DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            DataTable dt = ds.Tables[0];
                            dtCMS = SortTable(dt, "displayorder");
                        }
                    }
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Sub Menu Details By CMS id
        public DataTable Fetch_SubMenu_Detail_By_cmsid(Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select fieldtext,COALESCE(fieldvalue,'') SubMenuname,dorder from content_management_detail where cmsid=" + cmsid + "");
                strCMS.Append("  and (fieldtext Like '%Name%' OR fieldtext Like '%URL%' Or fieldtype like '%dropdown%' ) ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Check Whether Menu Has Sub Menu Or Not
        public Int32 Fetch_Submenucount_By_Menuname(string menuname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strSubMenu = new StringBuilder();
                strSubMenu.Append(" Select COUNT(*) from (Select content_management_detail.cmsid from content_management_detail inner join  content_management_master  ");
                strSubMenu.Append("  on content_management_detail.cmsid=content_management_master.cmsid ");
                strSubMenu.Append("  where fieldtype='Dropdown' and  fieldvalue='" + menuname.Trim().Replace(" '", "''") + "' and pagename='Sub Menu(Level-1) Name' ) q1 ");
                strSubMenu.Append("    left join (Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_detail inner join  content_management_master");
                strSubMenu.Append("   on content_management_detail.cmsid=content_management_master.cmsid where ");
                strSubMenu.Append("    fieldtype='Checkbox' and  fieldtext='Is Active'  and pagename='Sub Menu(Level-1) Name') q2");
                strSubMenu.Append("    on q1.cmsid=q2.cmsid where q2.fieldvalue='True'");
                object ObjCount = Service.DbFunctions.SelectCount(strSubMenu.ToString());
                if (ObjCount != null)
                {
                    retval = Convert.ToInt32(ObjCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Check Whether Menu Has Sub Menu Or Not
        public Int32 Fetch_SubMenuLevel2Count_By_Menuname(string menuname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strSubMenu = new StringBuilder();
                strSubMenu.Append(" Select COUNT(*) from (Select content_management_detail.cmsid from content_management_detail inner join  content_management_master  ");
                strSubMenu.Append("  on content_management_detail.cmsid=content_management_master.cmsid ");
                strSubMenu.Append("  where fieldtype='Dropdown' and  fieldvalue='" + menuname.Trim().Replace(" '", "''") + "' and pagename='Sub Menu(Level-2)' ) q1 ");
                strSubMenu.Append("    left join (Select content_management_detail.cmsid,content_management_detail.fieldvalue from content_management_detail inner join  content_management_master");
                strSubMenu.Append("   on content_management_detail.cmsid=content_management_master.cmsid where ");
                strSubMenu.Append("    fieldtype='Checkbox' and  fieldtext='Is Active'  and pagename='Sub Menu(Level-2)') q2");
                strSubMenu.Append("    on q1.cmsid=q2.cmsid where q2.fieldvalue='True'");
                object ObjCount = Service.DbFunctions.SelectCount(strSubMenu.ToString());
                if (ObjCount != null)
                {
                    retval = Convert.ToInt32(ObjCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch active SubModule and Display SubModule According to Display order in Ascending Order
        public DataTable Fetch_SubMenuactiveStatus_By_Menuname(string menuname)
        {
            DataTable dtCMS = null;
            DataTable dtSubMenuDetail = null;
            DataTable dtAddDetail = new DataTable();
            dtAddDetail.Columns.Add("CMSID", typeof(string));
            dtAddDetail.Columns.Add("SubMenuName", typeof(string));
            dtAddDetail.Columns.Add("SubMenuNavigationURL", typeof(string));
            Int32 RetCount = 0;
            try
            {
                StringBuilder strSubMenu = new StringBuilder();
                strSubMenu.Append(" SELECT q2.cmsid,cast(q2.fieldvalue as int) dorder FROM ( Select content_management_detail.cmsid from content_management_master");
                strSubMenu.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                strSubMenu.Append("  where pagename='Sub Menu(Level-1) Name' and fieldvalue='" + menuname + "' ) q1   ");
                strSubMenu.Append("  LEFT JOIN ( Select content_management_detail.cmsid,content_management_detail.fieldtext,content_management_detail.fieldvalue from content_management_master ");
                strSubMenu.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strSubMenu.Append("  where pagename='Sub Menu(Level-1) Name' and fieldtext like '%Display order%'  ) q2 ON q1.cmsid = q2.cmsid order by dorder");
               DataSet ds = Service.DbFunctions.SelectCommand(strSubMenu.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                if (dtCMS != null && dtCMS.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dtCMS.Rows.Count; i++)
                    {
                        strSubMenu = new StringBuilder();
                        strSubMenu.Append(" Select COUNT(*) from content_management_detail where cmsid=" + Convert.ToInt32(dtCMS.Rows[i]["cmsid"]) + " and (fieldtype='Checkbox' and fieldvalue='True') ");
                        object objCount = Service.DbFunctions.SelectCount(strSubMenu.ToString());
                        if (objCount != null)
                        {
                            RetCount = Convert.ToInt32(objCount);
                            if (RetCount > 0)
                            {
                                strSubMenu = new StringBuilder();
                                strSubMenu.Append(" Select cmsid,fieldtext,fieldvalue,dorder from content_management_detail where cmsid=" + Convert.ToInt32(dtCMS.Rows[i]["cmsid"]) + " ");
                                strSubMenu.Append("  and  (fieldtext like '%Sub%Menu%' OR fieldtext like '%URL%' )   ");
                                DataSet dsSubMenu = Service.DbFunctions.SelectCommand(strSubMenu.ToString());
                                if (dsSubMenu != null && dsSubMenu.Tables.Count > 0)
                                {
                                    DataTable dt = dsSubMenu.Tables[0];
                                    dtSubMenuDetail = SortTable(dt, "dorder");
                                    if (dtSubMenuDetail != null && dtSubMenuDetail.Rows.Count > 0)
                                    {
                                        string CMSID = Convert.ToString(dtSubMenuDetail.Rows[0]["cmsid"]);
                                        string Submenuname = Convert.ToString(dtSubMenuDetail.Rows[0]["fieldvalue"]);
                                        string NavigationURL = Convert.ToString(dtSubMenuDetail.Rows[1]["fieldvalue"]);
                                        dtAddDetail.Rows.Add(CMSID, Submenuname, NavigationURL);
                                    }
                                }
                            }
                        }
                    }
                }
                return dtAddDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch active Sub Menu Level2 and Display Sub Menu Level2 According to Display order in Ascending Order
        public DataTable Fetch_SubMenuLevel2activeStatus_By_Menuname(string SubMenuname)
        {
            DataTable dtCMS = null;
            DataTable dtSubMenuDetail = null;
            DataTable dtAddDetail = new DataTable();
            dtAddDetail.Columns.Add("CMSID", typeof(string));
            dtAddDetail.Columns.Add("SubMenuName", typeof(string));
            dtAddDetail.Columns.Add("SubMenuNavigationURL", typeof(string));
            Int32 RetCount = 0;
            try
            {
                StringBuilder strSubMenu = new StringBuilder();
                strSubMenu.Append(" SELECT q2.cmsid,cast(q2.fieldvalue as int) dorder FROM ( Select content_management_detail.cmsid from content_management_master");
                strSubMenu.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                strSubMenu.Append("  where pagename='Sub Menu(Level-2)' and fieldvalue='" + SubMenuname + "' ) q1   ");
                strSubMenu.Append("  LEFT JOIN ( Select content_management_detail.cmsid,content_management_detail.fieldtext,content_management_detail.fieldvalue from content_management_master ");
                strSubMenu.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strSubMenu.Append("  where pagename='Sub Menu(Level-2)' and fieldtext like '%Display order%'  ) q2 ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strSubMenu.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                if (dtCMS != null && dtCMS.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dtCMS.Rows.Count; i++)
                    {
                        strSubMenu = new StringBuilder();
                        strSubMenu.Append(" Select COUNT(*) from content_management_detail where cmsid=" + Convert.ToInt32(dtCMS.Rows[i]["cmsid"]) + " and (fieldtype='Checkbox' and fieldvalue='True') ");
                        object objCount = Service.DbFunctions.SelectCount(strSubMenu.ToString());
                        if (objCount != null)
                        {
                            RetCount = Convert.ToInt32(objCount);
                            if (RetCount > 0)
                            {
                                strSubMenu = new StringBuilder();
                                strSubMenu.Append(" Select cmsid,fieldtext,fieldvalue,dorder from content_management_detail where cmsid=" + Convert.ToInt32(dtCMS.Rows[i]["cmsid"]) + " ");
                                strSubMenu.Append("  and  (fieldtext like '%Sub%Menu%' OR fieldtext like '%URL%' )   ");
                                DataSet dsSubMenu = Service.DbFunctions.SelectCommand(strSubMenu.ToString());
                                if (dsSubMenu != null && dsSubMenu.Tables.Count > 0)
                                {
                                    DataTable dt = dsSubMenu.Tables[0];
                                    dtSubMenuDetail = SortTable(dt, "dorder");
                                    if (dtSubMenuDetail != null && dtSubMenuDetail.Rows.Count > 0)
                                    {
                                        string CMSID = Convert.ToString(dtSubMenuDetail.Rows[0]["cmsid"]);
                                        string Submenuname = Convert.ToString(dtSubMenuDetail.Rows[1]["fieldvalue"]);
                                        string NavigationURL = Convert.ToString(dtSubMenuDetail.Rows[2]["fieldvalue"]);
                                        dtAddDetail.Rows.Add(CMSID, Submenuname, NavigationURL);
                                    }
                                }
                            }
                        }
                    }
                }
                return dtAddDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get News showing Position based on page id
        public DataTable Fetch_News_Show_Position(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("   Select content_management_master.cmsid,fieldvalue as Position from content_management_master");
                strCMS.Append("  Inner join content_management_detail");
                strCMS.Append("  On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid='" + pageid + "' and fieldtype='Dropdown'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Footer Fuction
        // To Fetch cmsid for Footer Menu(address, LatestBlog,About us) and  DMS Benefits page 
        public DataTable Fetch_Menu_Dashboard_Bypagename(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  Select content_management_detail.cmsid, cast(content_management_detail.fieldvalue as int) dorder from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_cmsid_For_ContentPage(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select cmsid from content_management_master where pageid='" + pageid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "cmsid");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_cmsid_For_ContentPage_Except_HeadOfficecmsid(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtype='Checkbox' and fieldvalue='True' and fieldtext like '%Active%' and ");
                strCMS.Append("  content_management_master.cmsid<>(select cmsid from content_management_detail where fieldvalue='Head Office') ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "cmsid");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Fetch_cmsid_For_Head_Office()
        {
            string HeadOfficecmsid = Service.DbFunctions.SelectString(" select cmsid from content_management_detail where fieldvalue='Head Office'");
            return Convert.ToInt32(HeadOfficecmsid);
        }

        // Fetch cmsid for active content page
        public DataTable Fetch_cmsid_For_active_ContentPage_for_Published(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtype='Checkbox' and fieldvalue='True' and fieldtext like '%Published%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_cmsid_For_active_ContentPage(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtype='Checkbox' and fieldvalue='True' and fieldtext like '%Active%'");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_cmsid_For_active_ContentPage_Preview(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select  content_management_detail.cmsid from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " limit 1");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_cmsid_For_active_ContentPage_Preview_FromDraft(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select distinct content_management_detail_draft.cmsid from content_management_master_draft");
                strCMS.Append("  Inner join content_management_detail_draft On content_management_detail_draft.cmsid=content_management_master_draft.cmsid");
                strCMS.Append("  where pageid=" + pageid + " limit 1");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dtCMS = ds.Tables[0];
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch active Link Button order by display in asc
        public DataTable Fetch_Link_Button_Detail(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q1.Buttonname,cast(q3.dorder as int) dorder FROM");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue Buttonname from content_management_master");
                strCMS.Append("    Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Button name%'");
                strCMS.Append("   )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue dorder from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%' ");
                strCMS.Append("  ) q3");
                strCMS.Append("  ON q3.cmsid = q1.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue isactive from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Active%' and fieldvalue='True'");
                strCMS.Append("  ) q4");
                strCMS.Append("  ON q4.cmsid = q3.cmsid");
                strCMS.Append("  where q4.isactive='True'");
                strCMS.Append("  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Bind Page Content
        // To Fetch Page Content Details By CMS id
        public DataTable Fetch_Page_Contents_By_cmsid(Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select fieldtext,COALESCE(fieldvalue,'') PageContent, dorder from content_management_detail where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Page Content Details draft By CMS id 
        public DataTable Fetch_Page_Contents_By_cmsid_fromDraft(Int32 cmsid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select fieldtext,COALESCE(fieldvalue,'') PageContent, dorder from content_management_detail_draft where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Contact Us Functions
        // Fetch Enquiry 
        public DataTable Fetch_All_Enquiry()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUsers = new StringBuilder();
                strUsers.Append(" Select id,firstname|| ' ' ||lastname as name,emailid,subject,phoneno,message,to_char(contactus.creationdate, 'DD/MM/YYYY') creationdate from contactus order by creationdate ");
                DataSet ds = Service.DbFunctions.SelectCommand(strUsers.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert Enquiry
        public Int32 Insert_Enquiry(Cls_ContactUs contact)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strContact = new StringBuilder();
                strContact.Append(" Insert into contactus(firstname,lastname,phoneno,emailid,subject,message,creationdate) ");
                strContact.Append(" Values(" );
                strContact.Append(" '");
                strContact.Append(contact.firstname.Replace(" '", "''"));
                strContact.Append(" '");
                strContact.Append(" ,");
                strContact.Append(" '");
                strContact.Append(contact.lastname.Replace(" '", "''"));
                strContact.Append(" '");
                strContact.Append(" ,");
                strContact.Append(" '");
                strContact.Append(contact.phoneno.Replace(" '", "''"));
                strContact.Append(" '");
                strContact.Append(" ,");
                strContact.Append(" '");
                strContact.Append(contact.emailid.Replace(" '", "''"));
                strContact.Append(" '");
                strContact.Append(" ,");
                strContact.Append(" '");
                strContact.Append(contact.subject.Replace(" '", "''"));
                strContact.Append(" '");
                strContact.Append(" ,");
                strContact.Append(" '");
                strContact.Append(contact.message.Replace(" '", "''"));
                strContact.Append(" '");
                strContact.Append(" ,");
                strContact.Append(" LOCALTIMESTAMP");
                strContact.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strContact.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Pages Detail Function
        // To Fetch cmsid for Products
        public DataTable Fetch_cmsid_Bypageid(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q2.cmsid,cast(q2.dorder as int) dorder FROM");
                strCMS.Append("  ( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtype='Checkbox' and fieldvalue='True'");
                strCMS.Append("  )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue dorder from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                strCMS.Append("  ) q2");
                strCMS.Append("  ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To Fetch cmsid for Products by isactive
        public DataTable Fetch_cmsid_Bypageid_isactive(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q2.cmsid,cast(q2.dorder as int) dorder FROM");
                strCMS.Append("  ( Select content_management_detail.cmsid from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext='Is Active' and fieldvalue='True'");
                strCMS.Append("  )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  ( Select content_management_detail.cmsid,content_management_detail.fieldvalue dorder from content_management_master");
                strCMS.Append("  Inner join content_management_detail On");
                strCMS.Append("  content_management_detail.cmsid=content_management_master.cmsid");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%'");
                strCMS.Append("  ) q2");
                strCMS.Append("  ON q1.cmsid = q2.cmsid");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Products Details By CMS id
        public DataTable Fetch_Content_Data_By_cmsid(Int32 cmsid, string Alias)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append(" Select fieldtext,COALESCE(fieldvalue,'')" + Alias + " , dorder from content_management_detail where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Fetch Products CMS id by Product name
        public Int32 Fetch_Productsid_By_productname(string productname)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strBlog = new StringBuilder();
                strBlog.Append(" Select Distinct cmsid from content_management_detail where fieldvalue='" + productname + "'");
                DataSet Ds = Service.DbFunctions.SelectCommand(strBlog.ToString());
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    DataTable dt = Ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        retval = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion
        // End Of Front End Function

        #region Sorting Datatable according to the Column
        private DataTable SortTable(DataTable dt, string columnname)
        {
            DataView dataview = dt.DefaultView;
            dataview.Sort = columnname;
            return dataview.ToTable();
        }
        #endregion

        #region timeout functions
        //timeout insert
        public Int32 Insert_timeout(Clstimeout Obj_Clstimeout)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strInserttimeout = new StringBuilder();
                strInserttimeout.Append(" Insert into session_manager(timeout,creationdate,lastmodificationdate,createdby,modifiedby,isactive) Values(" );
                strInserttimeout.Append(Obj_Clstimeout.timeout);
                strInserttimeout.Append(" ,");
                strInserttimeout.Append(" GetDate()");
                strInserttimeout.Append(" ,");
                strInserttimeout.Append(" GetDate()");
                strInserttimeout.Append(" ,");
                strInserttimeout.Append(" '");
                strInserttimeout.Append(Obj_Clstimeout.createdby.Trim().Replace(" '", "''"));
                strInserttimeout.Append("'");
                strInserttimeout.Append(",");
                strInserttimeout.Append(" '");
                strInserttimeout.Append(Obj_Clstimeout.modifiedby.Trim().Replace(" '", "''"));
                strInserttimeout.Append(" '");
                strInserttimeout.Append(" ,");
                strInserttimeout.Append(" '");
                strInserttimeout.Append(Obj_Clstimeout.isactive);
                strInserttimeout.Append(" '");
                strInserttimeout.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strInserttimeout.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_timeout(Clstimeout Obj_Clstimeout)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update session_manager ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" timeout=");
                strUpdate.Append(Obj_Clstimeout.timeout);
                strUpdate.Append(" ,");
                strUpdate.Append(" lastmodificationdate=");
                strUpdate.Append(" GetDate()");
                strUpdate.Append(" ,");
                strUpdate.Append(" modifiedby='");
                strUpdate.Append(Obj_Clstimeout.modifiedby.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" ,");
                strUpdate.Append(" isactive='");
                strUpdate.Append(Obj_Clstimeout.isactive);
                strUpdate.Append(" '");
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch timeout
        public DataTable Fetchtimeout()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchSessiontimeout = new StringBuilder();
                strFetchSessiontimeout.Append(" select timeout,isactive from session_manager  where isactive='true'");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchSessiontimeout.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion

        #region Chat manager
        //Update chat manager
        public Int32 UpdateChatManager(ClsChatManager ObjClsChatManager)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update livechatmanager ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(ObjClsChatManager.isactive);
                strUpdate.Append(" '");
                strUpdate.Append(" ,");
                strUpdate.Append(" lastmodificationdate=");
                strUpdate.Append(" GetDate()");
                strUpdate.Append(" ,");
                strUpdate.Append(" modifiedby=");
                strUpdate.Append(" '");
                strUpdate.Append(ObjClsChatManager.modifiedby);
                strUpdate.Append(" '");
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind Chat Option Checkox;
        public string BindChatStatus()
        {
            string Status = "";
            DataTable dt = null;
            try
            {
                StringBuilder strChatOption = new StringBuilder();
                strChatOption.Append(" select isactive from livechatmanager");
                DataSet ds = Service.DbFunctions.SelectCommand(strChatOption.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        Status = Convert.ToString(dt.Rows[0]["isactive"]);
                    }
                }
                return Status;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
        #endregion

        #region UG Main Content
        public Int32 Insert_UGMainContent(Cls_UG_MainContent objMainContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Insert into ug_maincontent(heading,description,dorder,isactive,createdby,createddate)");
                strUGMainContent.Append("  Values(" );
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.heading.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.description.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(objMainContent.dorder);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.isactive);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.createdby);
                strUGMainContent.Append("'");
                strUGMainContent.Append(",");
                strUGMainContent.Append(" LOCALTIMESTAMP");
                strUGMainContent.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Delete_UGMainContent_By_id(int maincontentid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Delete from ug_maincontent ");
                strUGMainContent.Append(" Where ");
                strUGMainContent.Append(" maincontentid=");
                strUGMainContent.Append(maincontentid);
                retVal = Service.DbFunctions.DeleteCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_UGMainContent(Cls_UG_MainContent objMainContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Update ug_maincontent ");
                strUGMainContent.Append(" Set ");
                strUGMainContent.Append(" heading=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.heading.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" description=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.description.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" dorder=");
                strUGMainContent.Append(objMainContent.dorder);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" isactive=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.isactive);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" lastmodifiedby=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.lastmodifiedby);
                strUGMainContent.Append("'");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" lastmodificationdate=");
                strUGMainContent.Append(" LOCALTIMESTAMP ");
                strUGMainContent.Append(" Where ");
                strUGMainContent.Append(" maincontentid=");
                strUGMainContent.Append(objMainContent.maincontentid);
                retVal = Service.DbFunctions.UpdateCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        public Cls_UG_MainContent Fetch_UG_MainContent_Byid(Int32 maincontentid)
        {
            Cls_UG_MainContent objMainContent = null;

            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Select maincontentid,heading,description,dorder,isactive from ug_maincontent where maincontentid='" + maincontentid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strUGMainContent.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objMainContent = new Cls_UG_MainContent();
                        objMainContent.maincontentid = Convert.ToInt32(dt.Rows[0]["maincontentid"]);
                        objMainContent.heading = Convert.ToString(dt.Rows[0]["heading"]);
                        objMainContent.description = Convert.ToString(dt.Rows[0]["description"]);
                        objMainContent.dorder = Convert.ToInt32(dt.Rows[0]["dorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objMainContent.isactive = true;
                            }
                            else
                            {
                                objMainContent.isactive = false;
                            }
                        }
                        else
                        {
                            objMainContent.isactive = false;
                        }
                    }
                }
                return objMainContent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_UG_MainContent()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select maincontentid,heading,dorder,description,isactive ");
                strmodules.Append(" from ug_maincontent Order By dorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Get_Max_UG_MainContent_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from ug_maincontent");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_UG_Content_dorder(List<Cls_UG_MainContent> objMainContent)
        {
            Int32 retVal = 0;
            try
            {
                if (objMainContent != null && objMainContent.Count > 0)
                {
                    for (Int32 i = 0; i < objMainContent.Count; i++)
                    {
                        StringBuilder strmodules = new StringBuilder();
                        strmodules.Append(" Update ug_maincontent ");
                        strmodules.Append(" Set ");
                        strmodules.Append(" dorder=");
                        strmodules.Append(objMainContent[i].dorder);
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodifiedby=");
                        strmodules.Append(" '");
                        strmodules.Append(objMainContent[i].lastmodifiedby);
                        strmodules.Append("'");
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodificationdate=");
                        strmodules.Append(" LOCALTIMESTAMP ");
                        strmodules.Append(" Where ");
                        strmodules.Append(" maincontentid=");
                        strmodules.Append(objMainContent[i].maincontentid);
                        retVal = Service.DbFunctions.UpdateCommand(strmodules.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_isactive(Int32 maincontentid, bool isactive)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update ug_maincontent set isactive='" + isactive + "' where maincontentid=" + maincontentid + "");
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region UG Sub Content

        public Int32 Update_UG_SubContent_dorder(List<Cls_UG_SubContent> objSubContent)
        {
            Int32 retVal = 0;
            try
            {
                if (objSubContent != null && objSubContent.Count > 0)
                {
                    for (Int32 i = 0; i < objSubContent.Count; i++)
                    {

                        StringBuilder strmodules = new StringBuilder();
                        strmodules.Append(" Update ug_subcontent ");
                        strmodules.Append(" Set ");
                        strmodules.Append(" dorder=");
                        strmodules.Append(objSubContent[i].dorder);
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodifiedby=");
                        strmodules.Append(" '");
                        strmodules.Append(objSubContent[i].lastmodifiedby);
                        strmodules.Append("'");
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodifieddate=");
                        strmodules.Append(" LOCALTIMESTAMP ");
                        strmodules.Append(" Where ");
                        strmodules.Append(" subcontentid=");
                        strmodules.Append(objSubContent[i].subcontentid);
                        retVal = Service.DbFunctions.UpdateCommand(strmodules.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_UG_SubContent(Int32 maincontentid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsubContent = new StringBuilder();
                strsubContent.Append(" Select subcontentid,heading,description,dorder ");
                strsubContent.Append(" , isactive  ");
                strsubContent.Append("  from ug_subcontent where  maincontentid=" + maincontentid + " order by dorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubContent.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Insert_UG_SubContent(Cls_UG_SubContent objSubContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Insert into ug_subcontent(maincontentid,heading,description,dorder,isactive,createdby,createddate)");
                strUGMainContent.Append("  Values(" );
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.maincontentid);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.heading.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.description.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(objSubContent.dorder);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.isactive);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.createdby);
                strUGMainContent.Append("'");
                strUGMainContent.Append(",");
                strUGMainContent.Append(" LOCALTIMESTAMP");
                strUGMainContent.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_UG_SubContent(Cls_UG_SubContent objSubContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Update ug_subcontent ");
                strUGMainContent.Append(" Set ");
                strUGMainContent.Append(" heading=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.heading.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" description=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.description.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" dorder=");
                strUGMainContent.Append(objSubContent.dorder);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" isactive=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.isactive);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" lastmodifiedby=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objSubContent.lastmodifiedby);
                strUGMainContent.Append("'");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" lastmodifieddate=");
                strUGMainContent.Append(" LOCALTIMESTAMP ");
                strUGMainContent.Append(" Where ");
                strUGMainContent.Append(" subcontentid=");
                strUGMainContent.Append(objSubContent.subcontentid);
                retVal = Service.DbFunctions.UpdateCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        public Int32 Delete_UGSubContent_By_id(int subcontentid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Delete from ug_subcontent ");
                strUGMainContent.Append(" Where ");
                strUGMainContent.Append(" subcontentid=");
                strUGMainContent.Append(subcontentid);
                retVal = Service.DbFunctions.DeleteCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_UG_SubContent_order()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select * from ug_subcontent Order By dorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Cls_UG_SubContent Fetch_UG_SubContent_Byid(Int32 subcontentid)
        {
            Cls_UG_SubContent objMainContent = null;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Select subcontentid,heading,description,dorder,isactive from ug_subcontent where subcontentid='" + subcontentid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strUGMainContent.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objMainContent = new Cls_UG_SubContent();
                        objMainContent.subcontentid = Convert.ToInt32(dt.Rows[0]["subcontentid"]);
                        objMainContent.heading = Convert.ToString(dt.Rows[0]["heading"]);
                        objMainContent.description = Convert.ToString(dt.Rows[0]["description"]);
                        objMainContent.dorder = Convert.ToInt32(dt.Rows[0]["dorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objMainContent.isactive = true;
                            }
                            else
                            {
                                objMainContent.isactive = false;
                            }
                        }
                        else
                        {
                            objMainContent.isactive = false;
                        }
                    }
                }
                return objMainContent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_isactive_Content(Int32 subcontentid, bool isactive)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update ug_subcontent set isactive='" + isactive + "' where subcontentid=" + subcontentid + "");
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Get_Max_UG_SubContent_dorder(Int32 maincontentid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from ug_subcontent where maincontentid=" + maincontentid);
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UG Sub Content image

        public DataTable Fetch_SubContent_image(int subcontentid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Select id,subcontentid,imagename,dorder");
                strimage.Append(" , Case when isactive=True THEN 'Checked' else '' END isactive  ");
                strimage.Append("  from ug_subcontentimages  where  subcontentid=" + subcontentid);
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Delete_image_Data_By_id(int id)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSliderMasterItemDataDelete = new StringBuilder();
                strSliderMasterItemDataDelete.Append(" Delete from ug_subcontentimages ");
                strSliderMasterItemDataDelete.Append(" Where ");
                strSliderMasterItemDataDelete.Append(" id=");
                strSliderMasterItemDataDelete.Append(id);
                retVal = Service.DbFunctions.DeleteCommand(strSliderMasterItemDataDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        public Int32 Insert_UG_image_Item(Cls_UG_SubContentimages objClsimageItemMaster)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Insert into ug_subcontentimages(subcontentid,imagename,dorder,isactive) values(" );
                strimage.Append(objClsimageItemMaster.subcontentid);
                strimage.Append(" ,'");
                strimage.Append(objClsimageItemMaster.imagename.Trim().Replace(" '", "''"));
                strimage.Append(" ',");
                strimage.Append(objClsimageItemMaster.dorder);
                strimage.Append(" ,'");
                strimage.Append(objClsimageItemMaster.isactive);
                strimage.Append(" '");
                strimage.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strimage.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Get_Max_UG_imageContent_dorder(Int32 subcontentid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(dorder),0) as Maxdorder from ug_subcontentimages where subcontentid=" + subcontentid);
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_image_name_By_subcontentid(int subcontentid)
        {
            DataTable dt = new DataTable();
            string slidername = "";
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" select heading from ug_subcontent where subcontentid=" + subcontentid);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_isactive_images(Int32 id, bool isactive)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update ug_subcontentimages set isactive='" + isactive + "' where id=" + id + "");
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_image_For_Edit(Int32 id)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Select imagename,id,subcontentid,dorder,isactive from ug_subcontentimages  where id=" + id);
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Int32 Update_image_Item_Data(Cls_UG_SubContentimages objimage)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update ug_subcontentimages ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" imagename=");
                strUpdate.Append(" '");
                strUpdate.Append(objimage.imagename.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" dorder=");
                strUpdate.Append(objimage.dorder);
                strUpdate.Append(" , ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objimage.isactive);
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" id=");
                strUpdate.Append(objimage.id);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_UG_Contentimage_dorder(List<Cls_UG_SubContentimages> objSubContent)
        {
            Int32 retVal = 0;
            try
            {
                if (objSubContent != null && objSubContent.Count > 0)
                {
                    for (Int32 i = 0; i < objSubContent.Count; i++)
                    {

                        StringBuilder strimage = new StringBuilder();
                        strimage.Append(" Update ug_subcontentimages ");
                        strimage.Append(" Set ");
                        strimage.Append(" dorder=");
                        strimage.Append(objSubContent[i].dorder);
                        strimage.Append(" Where ");
                        strimage.Append(" id=");
                        strimage.Append(objSubContent[i].id);
                        retVal = Service.DbFunctions.UpdateCommand(strimage.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region
        public DataTable Fetch_MainContent_For_pdf()
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Select ug_maincontent.description from ug_maincontent inner join modules on ug_maincontent.heading=modules.modulename");
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_SubContent_For_pdf()
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Select ug_subcontent.description from ug_subcontent inner join submodules on ug_subcontent.heading = submodules.submodulename ");
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public DataTable Fetch_MainContent(Int32 roleid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Select ug_maincontent.maincontentid,ug_maincontent.heading,ug_maincontent.description,ug_maincontent.dorder from ");
                strimage.Append("  ug_maincontent where  heading in( Select submodulename ");
                strimage.Append(" from submodules where submoduleid in( Select submoduleid from roleprivileges where roleid="+roleid+" and isview='True')) Union ");
                strimage.Append(" Select maincontentid,heading,description,dorder from ug_maincontent");
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_UG_SubContent_for_html(Int32 maincontentid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsubContent = new StringBuilder();
                strsubContent.Append(" Select subcontentid,heading,description,dorder ");
                strsubContent.Append(" , Case when isactive=True THEN 'Checked' else '' END isactive  ");
                strsubContent.Append("  from ug_subcontent where isactive=True and maincontentid=" + maincontentid + " order by dorder ");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubContent.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Catalouge Functions

        // get Discounted amount by Purchase Order id
        public string FetchOrderNumer()
        {
            string retval = "";
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append("  select serialisekey from ordernumer");
                retval = Service.DbFunctions.SelectString(strMax.ToString());
                Int32 OldKey = Convert.ToInt32(retval) + 1;
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch active category order by display in asc
        public DataTable Fetch_Catalouge_category_cmsid(Int32 pageid)
        {
            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q1.cmsid,q1.categoryname ,cast(q3.dorder as int) dorder FROM");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue categoryname from content_management_master");
                strCMS.Append("    Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Name%'");
                strCMS.Append("   )  q1");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue dorder from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Display Order%' ");
                strCMS.Append("  ) q3");
                strCMS.Append("  ON q3.cmsid = q1.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue isactive from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where pageid=" + pageid + " and fieldtext like '%Active%' and fieldvalue='True'");
                strCMS.Append("  ) q4");
                strCMS.Append("  ON q4.cmsid = q3.cmsid");
                strCMS.Append("  where q4.isactive='True'");
                strCMS.Append(" ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Fetch Catlouge detail by cmsid
        public DataTable Fetch_Catalouge_By_SubType_id(Int32 cmsid) 
        {

            DataTable dtCMS = null;
            try
            {
                StringBuilder strCMS = new StringBuilder();
                strCMS.Append("  SELECT q1.cmsid,q1.productname ,q2.description,q3.dorder,q5.imagename,q6.categoryname,q7.price,q8.Showprice,q9.productnumber,q10.internaldescription,q11.cost FROM");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as productname from content_management_master");
                strCMS.Append("    Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                strCMS.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext = 'Product Name' ");
                strCMS.Append("   )  q1");
                strCMS.Append("   LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as description from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext = 'Description'");
                strCMS.Append("   ) q2");
                strCMS.Append("  ON q1.cmsid = q2.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  dorder from content_management_master");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Display Order%' ");
                strCMS.Append("  ) q3");
                strCMS.Append("  ON q3.cmsid = q2.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue isactive from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Is Active%' and fieldvalue='True'");
                strCMS.Append("  ) q4");
                strCMS.Append("  ON q4.cmsid = q3.cmsid");
                strCMS.Append("  LEFT JOIN");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as imagename from content_management_master");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Upload Image%'");
                strCMS.Append("  ) q5");
                strCMS.Append("  ON q5.cmsid = q4.cmsid");
                strCMS.Append("  LEFT JOIN (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  categoryname from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  and fieldtype like '%Dropdown%' and fieldvalue NOT IN('Public','Internal') ) q6 ");
                strCMS.Append("  ON q6.cmsid = q5.cmsid ");
                strCMS.Append("  LEFT JOIN (Select content_management_detail.cmsid,CAST(content_management_detail.fieldvalue AS DECIMAL(10, 2)) price from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  and fieldtext='Price'  ) q7 ");
                strCMS.Append("  ON q1.cmsid = q7.cmsid ");
                strCMS.Append("  LEFT JOIN (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  Showprice from content_management_master ");
                strCMS.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "   and fieldtext like '%Show Price%'   ) q8 ");
                strCMS.Append("  ON q7.cmsid = q8.cmsid ");
                strCMS.Append("  LEFT JOIN  ");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  productnumber from content_management_master  ");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid   ");
                strCMS.Append("   where content_management_detail.cmsid=" + cmsid + "   and fieldtext = 'Product Number'   ) q9  ON q8.cmsid = q9.cmsid  ");
                strCMS.Append("  LEFT JOIN  ");
                strCMS.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  internaldescription from content_management_master  ");
                strCMS.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid   ");
                strCMS.Append("   where content_management_detail.cmsid=" + cmsid + "   and fieldtext = 'Internal Description'   ) q10  ON q9.cmsid = q10.cmsid  ");
                strCMS.Append("  LEFT JOIN (Select content_management_detail.cmsid,content_management_detail.fieldvalue as cost from  ");
                strCMS.Append("  content_management_master  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strCMS.Append("  where content_management_detail.cmsid=" + cmsid + "  and fieldtext='Cost'  ) q11  ON q10.cmsid = q11.cmsid ");
                strCMS.Append("  where q4.isactive='True' ");
                strCMS.Append("  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strCMS.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    dtCMS = SortTable(dt, "dorder");
                }
                return dtCMS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //Fetch Catlouge cmsid By category name
        public DataTable Fetch_Catlouge_cmsid_By_category_name(string categoryname, Int32 pageid, string accesstype)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append("  Select B.cmsid from (Select Distinct content_management_master.cmsid from content_management_master  ");
                strFetchData.Append("  inner join content_management_detail on content_management_master.cmsid=content_management_detail.cmsid  ");
                strFetchData.Append("  where fieldvalue like '%" + categoryname + "%' and fieldtype='Dropdown' and pageid=" + pageid + " ) A ");
                strFetchData.Append("  INNER JOIN ");
                strFetchData.Append("  (Select Distinct content_management_master.cmsid from content_management_master  ");
                strFetchData.Append("  inner join content_management_detail on content_management_master.cmsid=content_management_detail.cmsid  ");
                strFetchData.Append("  where fieldtype='Dropdown' ");
                if (accesstype.ToUpper() != "ALL")
                {
                    strFetchData.Append("  and fieldvalue='Public' ");
                }
                strFetchData.Append("  and pageid=" + pageid + " ) B ");
                strFetchData.Append("  ON  A.cmsid=B.cmsid ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }

        //Fetch Catlouge cmsid By category name from CMS
        public DataTable Fetch_Catlouge_cmsid_By_category_name_FromCMS(string categoryname, Int32 pageid, string accesstype)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append("  Select B.cmsid from (Select Distinct content_management_master.cmsid from content_management_master  ");
                strFetchData.Append("  inner join content_management_detail on content_management_master.cmsid=content_management_detail.cmsid  ");
                strFetchData.Append("  where  fieldtype='Dropdown' and pageid=" + pageid + " ) A ");
                strFetchData.Append("  INNER JOIN ");
                strFetchData.Append("  (Select Distinct content_management_master.cmsid from content_management_master  ");
                strFetchData.Append("  inner join content_management_detail on content_management_master.cmsid=content_management_detail.cmsid  ");
                strFetchData.Append("  where fieldtype='Dropdown' ");
                if (accesstype.ToUpper() != "ALL")
                {
                    strFetchData.Append("  and fieldvalue='Public' ");
                }
                strFetchData.Append("  and pageid=" + pageid + " ) B ");
                strFetchData.Append("  ON  A.cmsid=B.cmsid ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }
        // Fetch Catlouge detail by Serach Text

        public DataTable Fetch_Catalouge_By_Search_Text(string SearchText, Int32 pageid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select q1.cmsid from (" );
                strFetchData.Append("  Select Distinct content_management_master.cmsid from content_management_master inner join content_management_detail");
                strFetchData.Append("  on content_management_master.cmsid=content_management_detail.cmsid where (fieldtext like '%" + SearchText + "%' and fieldtype='Dropdown') or fieldvalue like '%" + SearchText + "%'   and pageid=" + pageid + " )q1");
                strFetchData.Append("  left join  (" );
                strFetchData.Append("  Select Distinct content_management_master.cmsid,content_management_detail.fieldvalue from content_management_master inner join content_management_detail on ");
                strFetchData.Append("  content_management_master.cmsid=content_management_detail.cmsid where (fieldvalue='True'  and fieldtext='Is Active')   and pageid=" + pageid + ") q2");
                strFetchData.Append("  on q1.cmsid=q2.cmsid");
                strFetchData.Append("  where q2.fieldvalue='True'");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        //Insert new  shopping cart into temp table
        public Int32 Insert_Shopping_Temp_Cart_Detail(Cls_ShopingCart clsorder)
        {
            Int32 retVal = 0;

            try
            {
                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into shoppingcart(sessionid,productname,productnumber,internaldescription,category,quantity,description,cost,price,runningprice,size,color) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.sessionid.TrimEnd());
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productnumber.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.internaldescription.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.category.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(clsorder.qty);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.description.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.cost));
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.price));
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.price));
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.size);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.color);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strcontentPage.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert new  product detail to Order Detail
        public Int32 Insert_New_Product_Detail_toOrderDetail(Cls_OrderDetail clsorder)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into order_details(purchaseorderid,productname,productnumber,internaldescription,categoryname,quantity,description,cost,price,runningprice,size,color) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.purchaseorderid);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productnumber.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.internaldescription.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.categoryname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(clsorder.quantity);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.description.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.cost));
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.price));
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.runningprice));
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.size);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.color);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    
        //Insert new  product detail to Quote Detail
        public Int32 Insert_New_Product_Detail_toQuoteDetail(Cls_OrderDetail clsorder, Int32 retval)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into quotedetail(quotedetailid,purchaseorderid,productname,productnumber,internaldescription,categoryname,quantity,description,cost,price,runningprice,size,color) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(retval);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.purchaseorderid);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productnumber.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.internaldescription.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.categoryname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(clsorder.quantity);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.description.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.cost));
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.price));
                strcontentPage.Append(" ,");
                strcontentPage.Append(Convert.ToDouble(clsorder.runningprice));
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.size);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.color);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strcontentPage.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Insert new  Cart master data
        public Int32 Insert_Cart_Details(Cls_Order clsorder, string ShoppingCartid)
        {
            Int32 retDetailVal = 0;
            Int32 purchaseorderid = 0;
            DataTable dtShoppingData = null;
            Cls_OrderDetail clsDetail = new Cls_OrderDetail();
            Cls_QuoteDetail clsQuoteDetail = new Cls_QuoteDetail();
            try
            {
                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into order_master(orderno,companyname,purchasername,billingaddress1,billingaddress2 ");
                strcontentPage.Append(" ,billingcity,billingprovience,billingpostalcode,shippingaddress1,shippingaddress2,shippingcity,shippingprovience,shippingpostalcode,phone,fax,email,paymentmethod,isbackorderallowed,orderdate,grandtotal,requesttype,otherdetail,ponumber,orderstatus) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderno);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.companyname);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.purchasername.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.phone.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.fax.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.email.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.paymentmethod.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.isbackorderallowed);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" LOCALTIMESTAMP");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(Convert.ToString(ServicesFactory.DocCMSServices.Fetch_Shopping_Total(ShoppingCartid)));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.requesttype.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.otherdetail);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.ponumber);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderstatus);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                purchaseorderid = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                if (purchaseorderid > 0)
                {
                    /*****************FETCH SHOPPING CART FROM TEMP TABLE***************************************/
                    dtShoppingData = ServicesFactory.DocCMSServices.Fetch_Shopping_Temp_Cart_Detail(ShoppingCartid);
                    if (dtShoppingData != null && dtShoppingData.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtShoppingData.Rows)
                        {
                            clsDetail = new Cls_OrderDetail();
                            clsDetail.purchaseorderid = purchaseorderid;
                            clsDetail.categoryname = Convert.ToString(rw["category"]);
                            clsDetail.productname = Convert.ToString(rw["productname"]);
                            clsDetail.productnumber = Convert.ToString(rw["productnumber"]);
                            clsDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                            clsDetail.quantity = Convert.ToInt32(rw["quantity"]);
                            clsDetail.description = Convert.ToString(rw["description"]);
                            clsDetail.cost = Convert.ToString(rw["cost"]);
                            clsDetail.price = Convert.ToString(rw["price"]);
                            clsDetail.runningprice = Convert.ToString(rw["runningprice"]);
                            clsDetail.size = Convert.ToString(rw["size"]);
                            clsDetail.color = Convert.ToString(rw["color"]);
                            /*****************INSERT SHOPPING CART INTO ORDER DETAIL TABLE TABLE***************************************/
                            retDetailVal = ServicesFactory.DocCMSServices.Insert_Shopping_Cart_Details(clsDetail);
                            if (clsorder.requesttype == "Quote Request")
                            {
                                clsQuoteDetail = new Cls_QuoteDetail();
                                clsQuoteDetail.quotedetailid = retDetailVal;
                                clsQuoteDetail.purchaseorderid = purchaseorderid;
                                clsQuoteDetail.categoryname = Convert.ToString(rw["category"]);
                                clsQuoteDetail.productname = Convert.ToString(rw["productname"]);
                                clsQuoteDetail.productnumber = Convert.ToString(rw["productnumber"]);
                                clsQuoteDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                                clsQuoteDetail.quantity = Convert.ToInt32(rw["quantity"]);
                                clsQuoteDetail.description = Convert.ToString(rw["description"]);
                                clsQuoteDetail.cost = Convert.ToString(rw["cost"]);
                                clsQuoteDetail.price = Convert.ToString(rw["price"]);
                                clsQuoteDetail.runningprice = Convert.ToString(rw["runningprice"]);
                                clsQuoteDetail.size = Convert.ToString(rw["size"]);
                                clsQuoteDetail.color = Convert.ToString(rw["color"]);
                                ServicesFactory.DocCMSServices.Insert_CustomQuote_Detail_ByOrder_Detailid(clsQuoteDetail);
                            }
                        }
                    }
                    if (retDetailVal > 0)
                    {
                        //*************AFTER INSERTING DELETE DATA FROM TEMP TABLE
                        retDetailVal = ServicesFactory.DocCMSServices.Delete_Temp_Shopping_Detail_By_ShoppingCartid(ShoppingCartid);
                    }
                }
                if (retDetailVal > 0)
                {
                    return purchaseorderid;
                }
                return purchaseorderid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert new  Cart master data from CMS
        public Int32 Insert_Cart_Details_fromCMS(Cls_Order clsorder)
        {
            Int32 retDetailVal = 0;
            Int32 purchaseorderid = 0;
            DataTable dtShoppingData = null;
            Cls_OrderDetail clsDetail = new Cls_OrderDetail();
            Cls_QuoteDetail clsQuoteDetail = new Cls_QuoteDetail();
            try
            {
                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into order_master(orderno,companyname,purchasername,billingaddress1,billingaddress2 ");
                strcontentPage.Append(" ,billingcity,billingprovience,billingpostalcode,shippingaddress1,shippingaddress2,shippingcity,shippingprovience,shippingpostalcode,phone,fax,email,paymentmethod,isbackorderallowed,orderdate,grandtotal,discountedtotal,requesttype,otherdetail,ponumber,orderstatus) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderno);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.companyname);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.purchasername.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.phone.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.fax.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.email.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.paymentmethod.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.isbackorderallowed);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" LOCALTIMESTAMP");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.grandtotal);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.discountedtotal);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.requesttype.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.otherdetail);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.ponumber);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderstatus);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                purchaseorderid = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                if (purchaseorderid > 0)
                {
                    /*****************FETCH SHOPPING CART FROM TEMP TABLE***************************************/
                    dtShoppingData = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(clsorder.purchaseorderid);
                    if (dtShoppingData != null && dtShoppingData.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtShoppingData.Rows)
                        {
                            clsDetail = new Cls_OrderDetail();
                            clsDetail.purchaseorderid = purchaseorderid;
                            clsDetail.categoryname = Convert.ToString(rw["categoryname"]);
                            clsDetail.productname = Convert.ToString(rw["productname"]);
                            clsDetail.productnumber = Convert.ToString(rw["productnumber"]);
                            clsDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                            clsDetail.quantity = Convert.ToInt32(rw["quantity"]);
                            clsDetail.description = Convert.ToString(rw["description"]);
                            clsDetail.cost = Convert.ToString(rw["cost"]);
                            clsDetail.price = Convert.ToString(rw["price"]);
                            clsDetail.runningprice = Convert.ToString(rw["runningprice"]);
                            clsDetail.size = Convert.ToString(rw["size"]);
                            clsDetail.color = Convert.ToString(rw["color"]);
                            /*****************INSERT SHOPPING CART INTO ORDER DETAIL TABLE TABLE***************************************/
                            retDetailVal = ServicesFactory.DocCMSServices.Insert_Shopping_Cart_Details(clsDetail);
                            if (clsorder.requesttype == "Quote Request")
                            {
                                clsQuoteDetail = new Cls_QuoteDetail();
                                clsQuoteDetail.quotedetailid = retDetailVal;
                                clsQuoteDetail.purchaseorderid = purchaseorderid;
                                clsQuoteDetail.categoryname = Convert.ToString(rw["categoryname"]);
                                clsQuoteDetail.productname = Convert.ToString(rw["productname"]);
                                clsQuoteDetail.productnumber = Convert.ToString(rw["productnumber"]);
                                clsQuoteDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                                clsQuoteDetail.quantity = Convert.ToInt32(rw["quantity"]);
                                clsQuoteDetail.description = Convert.ToString(rw["description"]);
                                clsQuoteDetail.cost = Convert.ToString(rw["cost"]);
                                 clsQuoteDetail.price = Convert.ToString(rw["price"]);
                                clsQuoteDetail.runningprice = Convert.ToString(rw["runningprice"]);
                                clsQuoteDetail.discountedprice = Convert.ToString(rw["discountedprice"]);
                                clsQuoteDetail.discountedamount = Convert.ToString(rw["discountedamount"]);
                                clsQuoteDetail.discountedpercentage = Convert.ToString(rw["discountedpercentage"]);
                                clsQuoteDetail.size = Convert.ToString(rw["size"]);
                                clsQuoteDetail.color = Convert.ToString(rw["color"]);
                                ServicesFactory.DocCMSServices.Insert_CustomQuote_Detail_ByOrder_Detailid(clsQuoteDetail);
                            }
                        }
                    }
                }
                if (retDetailVal > 0)
                {
                    return purchaseorderid;
                }
                return purchaseorderid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert new  Cart master data from CMS
        public Int32 Insert_Cart_Details_fromCMS_OrderDashboard(Cls_Order clsorder)
        {
            Int32 retDetailVal = 0;
            Int32 purchaseorderid = 0;
            DataTable dtShoppingData = null;
            Cls_OrderDetail clsDetail = new Cls_OrderDetail();
            Cls_QuoteDetail clsQuoteDetail = new Cls_QuoteDetail();
            try
            {

                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into order_master(orderno,companyname,purchasername,billingaddress1,billingaddress2 ");
                strcontentPage.Append(" ,billingcity,billingprovience,billingpostalcode,shippingaddress1,shippingaddress2,shippingcity,shippingprovience,shippingpostalcode,phone,fax,email,paymentmethod,isbackorderallowed,orderdate,grandtotal,discountedtotal,requesttype,otherdetail,ponumber,orderstatus) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderno);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.companyname);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.purchasername.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.phone.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.fax.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.email.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.paymentmethod.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.isbackorderallowed);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" LOCALTIMESTAMP");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.grandtotal);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.discountedtotal);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.requesttype.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.otherdetail);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.ponumber);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderstatus);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                purchaseorderid = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                if (purchaseorderid > 0)
                {
                    /*****************FETCH SHOPPING CART FROM TEMP TABLE***************************************/
                    dtShoppingData = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid(clsorder.purchaseorderid);
                    if (dtShoppingData != null && dtShoppingData.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtShoppingData.Rows)
                        {
                            clsDetail = new Cls_OrderDetail();
                            clsDetail.purchaseorderid = purchaseorderid;
                            clsDetail.categoryname = Convert.ToString(rw["categoryname"]);
                            clsDetail.productname = Convert.ToString(rw["productname"]);
                            clsDetail.productnumber = Convert.ToString(rw["productnumber"]);
                            clsDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                            clsDetail.quantity = Convert.ToInt32(rw["quantity"]);
                            clsDetail.description = Convert.ToString(rw["description"]);
                            clsDetail.cost = Convert.ToString(rw["cost"]);
                            clsDetail.price = Convert.ToString(rw["price"]);
                            clsDetail.runningprice = Convert.ToString(rw["runningprice"]);
                            clsDetail.size = Convert.ToString(rw["size"]);
                            clsDetail.color = Convert.ToString(rw["color"]);
                            /*****************INSERT SHOPPING CART INTO ORDER DETAIL TABLE TABLE***************************************/
                            retDetailVal = ServicesFactory.DocCMSServices.Insert_Shopping_Cart_Details(clsDetail);
                            if (clsorder.requesttype == "Quote Request")
                            {
                                clsQuoteDetail = new Cls_QuoteDetail();
                                clsQuoteDetail.quotedetailid = retDetailVal;
                                clsQuoteDetail.purchaseorderid = purchaseorderid;
                                clsQuoteDetail.categoryname = Convert.ToString(rw["categoryname"]);
                                clsQuoteDetail.productname = Convert.ToString(rw["productname"]);
                                clsQuoteDetail.productnumber = Convert.ToString(rw["productnumber"]);
                                clsQuoteDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                                clsQuoteDetail.quantity = Convert.ToInt32(rw["quantity"]);
                                clsQuoteDetail.description = Convert.ToString(rw["description"]);
                                clsQuoteDetail.cost = Convert.ToString(rw["cost"]);
                                clsQuoteDetail.price = Convert.ToString(rw["price"]);
                                clsQuoteDetail.runningprice = Convert.ToString(rw["runningprice"]);
                                clsQuoteDetail.discountedprice = Convert.ToString(rw["discountedprice"]);
                                clsQuoteDetail.discountedamount = Convert.ToString(rw["discountedamount"]);
                                clsQuoteDetail.discountedpercentage = Convert.ToString(rw["discountedpercentage"]);
                                clsQuoteDetail.size = Convert.ToString(rw["size"]);
                                clsQuoteDetail.color = Convert.ToString(rw["color"]);
                                ServicesFactory.DocCMSServices.Insert_CustomQuote_Detail_ByOrder_Detailid(clsQuoteDetail);
                            }
                        }
                    }
                }
                if (retDetailVal > 0)
                {
                    return purchaseorderid;
                }
                return purchaseorderid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // get Discounted amount by Purchase Order id
        public string Get_discountedtotalprice(Int32 purchaseorderid)
        {
            string retval = "";
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append("  Select discountedtotal from order_master where purchaseorderid="+purchaseorderid+"");
                retval = Service.DbFunctions.SelectString(strMax.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert new  Cart master data from CMS and convert quote to order
        public Int32 Insert_Cart_Details_fromCMS_and_Convert_Quote_to_Order(Cls_Order clsorder)
        {
            Int32 retDetailVal = 0;
            Int32 purchaseorderid = 0;
            DataTable dtShoppingData = null;
            Cls_OrderDetail clsDetail = new Cls_OrderDetail();
            Cls_QuoteDetail clsQuoteDetail = new Cls_QuoteDetail();
            try
            {

                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into order_master(orderno,companyname,purchasername,billingaddress1,billingaddress2 ");
                strcontentPage.Append(" ,billingcity,billingprovience,billingpostalcode,shippingaddress1,shippingaddress2,shippingcity,shippingprovience,shippingpostalcode,phone,fax,email,paymentmethod,isbackorderallowed,orderdate,grandtotal,requesttype,otherdetail,ponumber,orderstatus) ");
                strcontentPage.Append(" Values(" );
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderno);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.companyname);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.purchasername.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.billingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress1.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingaddress2.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingcity.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingprovience.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.shippingpostalcode.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.phone.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.fax.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.email.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.paymentmethod.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.isbackorderallowed);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" LOCALTIMESTAMP");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.grandtotal);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.requesttype.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.otherdetail);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.ponumber);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.orderstatus);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                purchaseorderid = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                if (purchaseorderid > 0)
                {
                    /*****************FETCH SHOPPING CART FROM TEMP TABLE***************************************/
                    dtShoppingData = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(clsorder.purchaseorderid);
                    if (dtShoppingData != null && dtShoppingData.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtShoppingData.Rows)
                        {
                            clsDetail = new Cls_OrderDetail();
                            clsDetail.purchaseorderid = purchaseorderid;
                            clsDetail.categoryname = Convert.ToString(rw["categoryname"]);
                            clsDetail.productname = Convert.ToString(rw["productname"]);
                            clsDetail.productnumber = Convert.ToString(rw["productnumber"]);
                            clsDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                            clsDetail.quantity = Convert.ToInt32(rw["quantity"]);
                            clsDetail.description = Convert.ToString(rw["description"]);
                            clsDetail.cost = Convert.ToString(rw["cost"]);
                            clsDetail.price = Convert.ToString(rw["discountedprice"]);
                            clsDetail.runningprice = Convert.ToString(rw["discountedamount"]);
                            clsDetail.size = Convert.ToString(rw["size"]);
                            clsDetail.color = Convert.ToString(rw["color"]);
                            /*****************INSERT SHOPPING CART INTO ORDER DETAIL TABLE TABLE***************************************/
                            retDetailVal = ServicesFactory.DocCMSServices.Insert_Shopping_Cart_Details(clsDetail);
                            if (clsorder.requesttype == "Quote Request")
                            {
                                clsQuoteDetail = new Cls_QuoteDetail();
                                clsQuoteDetail.quotedetailid = retDetailVal;
                                clsQuoteDetail.purchaseorderid = purchaseorderid;
                                clsQuoteDetail.categoryname = Convert.ToString(rw["categoryname"]);
                                clsQuoteDetail.productname = Convert.ToString(rw["productname"]);
                                clsQuoteDetail.productnumber = Convert.ToString(rw["productnumber"]);
                                clsQuoteDetail.internaldescription = Convert.ToString(rw["internaldescription"]);
                                clsQuoteDetail.quantity = Convert.ToInt32(rw["quantity"]);
                                clsQuoteDetail.description = Convert.ToString(rw["description"]);
                                clsQuoteDetail.cost = Convert.ToString(rw["cost"]);
                                clsQuoteDetail.price = Convert.ToString(rw["price"]);
                                clsQuoteDetail.runningprice = Convert.ToString(rw["runningprice"]);
                                clsQuoteDetail.size = Convert.ToString(rw["size"]);
                                clsQuoteDetail.color = Convert.ToString(rw["color"]);
                                ServicesFactory.DocCMSServices.Insert_CustomQuote_Detail_ByOrder_Detailid(clsQuoteDetail);
                            }
                        }
                    }
                }
                if (retDetailVal > 0)
                {
                    return purchaseorderid;
                }
                return purchaseorderid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update cart master detail
        public Int32 Update_Cart_Details_fromCMS(Cls_Order clsorder)
        {
            Int32 retVal = 0;
            try
            {

                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update order_master ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" companyname=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.companyname.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" purchasername=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.purchasername.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" billingcity=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.billingcity.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" billingaddress1=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.billingaddress1.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" billingaddress2=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.billingaddress2.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" email=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.email.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" fax=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.fax.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" paymentmethod=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.paymentmethod.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" phone=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.phone.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" billingpostalcode=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.billingpostalcode.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" billingprovience=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.billingprovience.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" otherdetail=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.otherdetail.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" ponumber=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.ponumber.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" requesttype=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.requesttype.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" shippingcity=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.shippingcity.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" shippingaddress1=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.shippingaddress1.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" shippingaddress2=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.shippingaddress2.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" shippingpostalcode=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.shippingpostalcode.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" shippingprovience=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.shippingprovience.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" orderstatus=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.orderstatus.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" purchaseorderid=");
                strUpdate.Append(clsorder.purchaseorderid);
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Insert cart detail
        public Int32 Insert_Shopping_Cart_Details(Cls_OrderDetail clsorder)
        {
            Int32 retVal = 0;
            try
            {

                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into order_details(purchaseorderid,categoryname,productname,productnumber,internaldescription,quantity,description,cost,price,runningprice,size,color) Values(" );
                strcontentPage.Append(clsorder.purchaseorderid);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.categoryname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productnumber);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.internaldescription.Trim());
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(clsorder.quantity);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.description.Trim());
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.cost.Trim());
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.price.Trim());
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.runningprice.Trim());
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.size);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.color);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strcontentPage.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Insert Custom Quote detail
        public Int32 Insert_CustomQuote_Detail_ByOrder_Detailid(Cls_QuoteDetail clsorder)
        {
            Int32 retVal = 0;
            try
            {

                StringBuilder strcontentPage = new StringBuilder();
                strcontentPage.Append(" Insert into quotedetail(quotedetailid,purchaseorderid,categoryname,productname,productnumber,internaldescription,quantity,description,cost,price,runningprice,size,color,discountedprice,discountedamount,discountedpercentage) Values(" );
                strcontentPage.Append(clsorder.quotedetailid);
                strcontentPage.Append(" ,");
                strcontentPage.Append(clsorder.purchaseorderid);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.categoryname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productname.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.productnumber.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.internaldescription.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(clsorder.quantity);
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.description.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.cost.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.price.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.runningprice.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.size.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.color.Replace(" '", "''"));
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.discountedprice);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.discountedamount);
                strcontentPage.Append(" '");
                strcontentPage.Append(" ,");
                strcontentPage.Append(" '");
                strcontentPage.Append(clsorder.discountedpercentage);
                strcontentPage.Append(" '");
                strcontentPage.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strcontentPage.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update Custom Quote detail
        public Int32 Update_CustomQuote_Detail_ByOrder_Detailid(Cls_QuoteDetail clsorder)
        {
            Int32 retVal = 0;
            try
            {

                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update quotedetail ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" purchaseorderid=");
                strUpdate.Append(clsorder.purchaseorderid);
                strUpdate.Append(" , ");
                strUpdate.Append(" categoryname=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.categoryname.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" productnumber=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.productnumber.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" internaldescription=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.internaldescription.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" productname=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.productname.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" quantity=");
                strUpdate.Append(clsorder.quantity);
                strUpdate.Append(" , ");
                strUpdate.Append(" description=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.description.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" price=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.price.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" runningprice=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.runningprice.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" size=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.size.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" color=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.color.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" discountedprice=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.discountedprice.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" discountedamount=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.discountedamount.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" discountedpercentage=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.discountedpercentage.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" quotedetailid=");
                strUpdate.Append(clsorder.quotedetailid);
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update Custom Order detail
        public Int32 Update_CustomOrder_Detail_ByOrder_Detailid(Cls_OrderDetail clsorder)
        {
            Int32 retVal = 0;
            try
            {

                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update order_details ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" purchaseorderid=");
                strUpdate.Append(clsorder.purchaseorderid);
                strUpdate.Append(" , ");
                strUpdate.Append(" categoryname=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.categoryname.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" productnumber=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.productnumber.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" internaldescription=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.internaldescription.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" productname=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.productname.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" quantity=");
                strUpdate.Append(clsorder.quantity);
                strUpdate.Append(" , ");
                strUpdate.Append(" description=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.description.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" price=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.price.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" runningprice=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.runningprice.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" size=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.size.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" color=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.color);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" quantityshipped=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.quantityshipped);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" quantitybackorder=");
                strUpdate.Append(" '");
                strUpdate.Append(clsorder.quantitybackorder);
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" orderdetailid=");
                strUpdate.Append(clsorder.orderdetailid);
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update Discounted Total and Grand Total to Order Master Table
        public Int32 Update_discountedtotal_valueAnd_grandtotal(string discountedtotal,string grandtotal, Int32 purchaseorderid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update order_master set discountedtotal=" + discountedtotal + ",grandtotal="+grandtotal+" where purchaseorderid="+purchaseorderid+" ");
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Check for shopping Cart
        public Int32 CheckCount_For_Duplicate_ShoppingCart(Cls_ShopingCart clsorder)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*) CountNo from shoppingcart where sessionid='" + clsorder.sessionid + " ' and productname='" + clsorder.productname + "'  ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Check for shopping Cart While Adding From_CMS
        public Int32 CheckCount_For_Duplicate_Item_WhileAdding_From_CMS(Cls_OrderDetail clsorder)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append("   Select COUNT(*) CountNo from order_details where productname='"+clsorder.productname+"' and purchaseorderid="+clsorder.purchaseorderid+"");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Check for duplicate quotedetailid
        public Int32 CheckCount_For_Duplicate_quotedetailid(Int32 quotedetailid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*) from quotedetail where quotedetailid=" + quotedetailid + "");
                retval = Service.DbFunctions.SelectCount(strname.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Check for active category
        public bool Check_For_category_active(string SearchText, Int32 pageid)
        {
            bool retval = false;
            try
            {

                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select Count(q2.cmsid) from (" );
                strFetchData.Append("  Select Distinct content_management_master.cmsid from content_management_master inner join content_management_detail");
                strFetchData.Append("  on content_management_master.cmsid=content_management_detail.cmsid where (fieldtext like '%" + SearchText + "%' and fieldtype='DropDown') or fieldvalue like '%" + SearchText + "%'   and pageid=" + pageid + " )q1");
                strFetchData.Append("  left join  (" );
                strFetchData.Append("  Select Distinct content_management_master.cmsid,content_management_detail.fieldvalue from content_management_master inner join content_management_detail on ");
                strFetchData.Append("  content_management_master.cmsid=content_management_detail.cmsid where (fieldvalue='True'  and fieldtext='Is Active')   and pageid=" + pageid + ") q2");
                strFetchData.Append("  on q1.cmsid=q2.cmsid");
                strFetchData.Append("  where q2.fieldvalue='True'");
                Int32 retVal;
                retVal = Service.DbFunctions.SelectCount(strFetchData.ToString());
                if (retVal > 0)
                {
                    retval = true;
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetch temp shopping detail
        public DataTable Fetch_Shopping_Temp_Cart_Detail(string sessionid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from shoppingcart where sessionid='" + sessionid + " '");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetch total price
        public string Fetch_Shopping_Total(string sessionid)
        {
            string Total = "0";
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append("  SELECT  Sum(runningprice) as Totalprice from shoppingcart where sessionid='" + sessionid + " '");
                decimal retval = Service.DbFunctions.SelectTotal(strFetchData.ToString());
                if (retval > 0)
                {
                    Total = Convert.ToString(retval);
                }
                return Total;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        // fetch discounted total price
        public string Fetch_Discounted_Total(Int32 purchaseorderid)
        {
            string Total = "0";
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append("  SELECT SUM(cast(discountedamount as decimal(10,2))) as TotalDiscountprice from quotedetail where purchaseorderid="+purchaseorderid+"");
                decimal retval = Service.DbFunctions.SelectTotal(strFetchData.ToString());
                if (retval > 0)
                {
                    Total = Convert.ToString(retval);
                }
                return Total;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        // fetch grand total from Quote Detail
        public string Fetch_Grand_Total_from_QuoteDetail(Int32 purchaseorderid)
        {
            string Total = "0";
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append("  SELECT SUM(cast(runningprice as decimal(10,2))) as grandtotal from quotedetail where purchaseorderid=" + purchaseorderid + "");
                decimal retval = Service.DbFunctions.SelectTotal(strFetchData.ToString());
                if (retval > 0)
                {
                    Total = Convert.ToString(retval);
                }
                return Total;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        // fetch grand total from Order Detail
        public string Fetch_Grand_Total_from_OrderDetail(Int32 purchaseorderid)
        {
            string Total = "0";

            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append("  SELECT SUM(cast(runningprice as decimal(10,2))) as grandtotal from order_details where purchaseorderid=" + purchaseorderid + "");
                decimal retval = Service.DbFunctions.SelectTotal(strFetchData.ToString());
                if (retval > 0)
                {
                    Total = Convert.ToString(retval);
                }
                return Total;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        // To Delete User Roles By roleid
        public Int32 Delete_Temp_Shopping_Detail_By_ShoppingCartid(string ShoppingCartid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from shoppingcart ");
                strDelete.Append(" Where ");
                strDelete.Append(" sessionid='");
                strDelete.Append(ShoppingCartid);
                strDelete.Append(" '");
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //Update shopping cart qty by its id
        public string Update_Shopping_cart_By_id(Int32 id, Int32 qty, Decimal runningprice)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update shoppingcart ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" quantity=");
                strUpdate.Append(qty);
                strUpdate.Append(" ,");
                strUpdate.Append(" runningprice=");
                strUpdate.Append(runningprice);
                strUpdate.Append(" Where ");
                strUpdate.Append(" id=");
                strUpdate.Append(id);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Delete shopping cart  by its id
        public string Delete_Shopping_cart_By_id(Int32 id)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from shoppingcart ");
                strDelete.Append(" Where ");
                strDelete.Append(" id=");
                strDelete.Append(id);
                retval = Service.DbFunctions.UpdateCommand(strDelete.ToString());
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Maximum Order No
        public string Get_Max_Order_Number()
        {
            string retval = "";
            DataTable dt = null;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select 'DFI/'+REPLICATE(0,2-LEN(MONTH(LOCALTIMESTAMP)))+Convert(varchar,MONTH(LOCALTIMESTAMP),103)+'/'+Convert(varchar,YEAR(LOCALTIMESTAMP),103)+'/'+");
                strMax.Append(" Convert(varchar,COALESCE(MAX(purchaseorderid),0)+1,103) OrderNumber from order_master ");
                DataSet ds = Service.DbFunctions.SelectCommand(strMax.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    retval = Convert.ToString(dt.Rows[0]["OrderNumber"]);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // fetchall the order list from master table where Request type is Order
        public DataTable Fetch_Order_List()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select purchaseorderid,orderno,companyname,purchasername,to_char(orderdate, 'DD/MM/YYYY') orderdate,orderdate as orderdateTime,phone,email,orderstatus from order_master where requesttype='Order Request' order by orderdateTime desc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // fetchall the order list from master table where Request type is Quote
        public DataTable Fetch_Quote_List()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select purchaseorderid,orderno,companyname,purchasername,to_char(orderdate, 'DD/MM/YYYY') orderdate,orderdate as orderdateTime,phone,email,orderstatus from order_master where requesttype='Quote Request' order by orderdateTime desc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetchall the order list from master table
        public Cls_Order Fetch_Order_List_By_id(Int32 Purchaseid)
        {
            DataTable dt = null;
            Cls_Order clsorder = new Cls_Order();
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select purchaseorderid,orderno,purchasername,CONVERT(smalldatetime,orderdate,101) orderdate,phone,fax,email,billingaddress1,billingaddress2,billingcity,billingprovience,billingpostalcode,shippingaddress1,shippingaddress2,shippingcity,shippingprovience,shippingpostalcode,companyname,ponumber,otherdetail,paymentmethod,grandtotal,orderstatus from order_master where purchaseorderid=" + Purchaseid + " order by orderdate ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    clsorder.purchaseorderid = Convert.ToInt32(dt.Rows[0]["purchaseorderid"]);
                    clsorder.orderno = Convert.ToString(dt.Rows[0]["orderno"]);
                    clsorder.companyname = Convert.ToString(dt.Rows[0]["companyname"]);
                    clsorder.purchasername = Convert.ToString(dt.Rows[0]["purchasername"]);
                    clsorder.billingaddress1 = Convert.ToString(dt.Rows[0]["billingaddress1"]);
                    clsorder.billingaddress2 = Convert.ToString(dt.Rows[0]["billingaddress2"]);
                    clsorder.billingcity = Convert.ToString(dt.Rows[0]["billingcity"]);
                    clsorder.email = Convert.ToString(dt.Rows[0]["email"]);
                    clsorder.phone = Convert.ToString(dt.Rows[0]["phone"]);
                    clsorder.paymentmethod = Convert.ToString(dt.Rows[0]["paymentmethod"]);
                    clsorder.billingprovience = Convert.ToString(dt.Rows[0]["billingprovience"]);
                    clsorder.billingpostalcode = Convert.ToString(dt.Rows[0]["billingpostalcode"]);
                    clsorder.shippingaddress1 = Convert.ToString(dt.Rows[0]["shippingaddress1"]);
                    clsorder.shippingaddress2 = Convert.ToString(dt.Rows[0]["shippingaddress2"]);
                    clsorder.shippingcity = Convert.ToString(dt.Rows[0]["shippingcity"]);
                    clsorder.shippingprovience = Convert.ToString(dt.Rows[0]["shippingprovience"]);
                    clsorder.shippingpostalcode = Convert.ToString(dt.Rows[0]["shippingpostalcode"]);
                    clsorder.ponumber = Convert.ToString(dt.Rows[0]["ponumber"]);
                    clsorder.otherdetail = Convert.ToString(dt.Rows[0]["otherdetail"]);
                    clsorder.fax = Convert.ToString(dt.Rows[0]["fax"]);
                    clsorder.orderdate = Convert.ToDateTime(dt.Rows[0]["orderdate"]);
                    clsorder.grandtotal = Convert.ToString(dt.Rows[0]["grandtotal"]);
                    clsorder.orderstatus = Convert.ToString(dt.Rows[0]["orderstatus"]);
                }
                return clsorder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // fetchall the order list from master table in Data Table
        public DataTable Fetch_Order_List_By_id_DataTable(Int32 Purchaseid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select purchaseorderid,orderno,purchasername,CONVERT(smalldatetime,orderdate,101) orderdate,phone,fax,email,billingaddress1,billingaddress2,billingcity,billingprovience,billingpostalcode,shippingaddress1,shippingaddress2,shippingcity,shippingprovience,shippingpostalcode,companyname,ponumber,otherdetail,paymentmethod,grandtotal,orderstatus from order_master where purchaseorderid=" + Purchaseid + " order by orderdate ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // fetchall the order list from Order detail table by Purchase id
        public DataTable Fetch_Order_Detail_List_By_purchaseorderid(Int32 Purchaseid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from order_details where purchaseorderid=" + Purchaseid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // fetchall the order list from Order detail table by Purchase id From quotedetail
        public DataTable Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(Int32 Purchaseid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from quotedetail where purchaseorderid=" + Purchaseid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetchall the order list from Order detail table by order detail id
        public DataTable Fetch_Order_Detail_By_orderdetailid(Int32 quotedetailid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from quotedetail where quotedetailid=" + quotedetailid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetchall the order list from Order detail table by order detail id
        public DataTable Fetch_PartDetail_ByOrderDetalid_From_OrderDetailTable(Int32 orderdetailid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from order_details where orderdetailid=" + orderdetailid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetchall the comment by purchase order id
        public DataTable Fetch_Quote_comment_Bypurchaseorderid(Int32 purchaseorderid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from quotecomment where purchaseorderid=" + purchaseorderid + " order by dorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // fetch all the comment
        public DataTable Fetch_AllQuote_comment()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" Select * from quotes ");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Insert Order description 
        public Int32 Insert_Orderdescription(string Orderdescription, Int32 purchaseorderid)
        {
            Int32 retVal = 0;

            try
            {
                StringBuilder Str = new StringBuilder();
                Str.Append(" Insert into orderdescription(purchaseorderid,orderdescription) ");
                Str.Append(" Values(" );
                Str.Append(purchaseorderid);
                Str.Append(" ,");
                Str.Append(" '");
                Str.Append(Orderdescription.Replace(" '", "''"));
                Str.Append(" '");
                Str.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(Str.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Update Order description
        public Int32 Update_Orderdescription(string Orderdescription, Int32 purchaseorderid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder Str = new StringBuilder();
                Str.Append(" Update orderdescription ");
                Str.Append(" Set ");
                Str.Append(" orderdescription=");
                Str.Append(" '");
                Str.Append(Orderdescription.Replace(" '", "''"));
                Str.Append(" '");
                Str.Append(" Where ");
                Str.Append(" purchaseorderid=");
                Str.Append(purchaseorderid);
                retVal = Service.DbFunctions.UpdateCommand(Str.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        // To Get Order Detail Dashboard
        public string Get_Order_Detail_description(Int32 purchaseorderid)
        {
            string retval = "";
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append(" select orderdescription from orderdescription where purchaseorderid="+purchaseorderid+" ");
                retval = Service.DbFunctions.SelectString(str.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert New Quote comments 
        public Int32 Insert_Quotes(Cls_Quotes Quote)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder Str = new StringBuilder();
                Str.Append(" Insert into quotes(quotename,quotecomment,active) ");
                Str.Append(" Values(" );
                Str.Append(" '");
                Str.Append(Quote.quotename.Replace(" '", "''"));
                Str.Append(" '");
                Str.Append(" ,");
                Str.Append(" '");
                Str.Append(Quote.quotecomment.Replace(" '", "''"));
                Str.Append(" '");
                Str.Append(" ,");
                Str.Append(" '");
                Str.Append(Quote.active);
                Str.Append(" '");
                Str.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(Str.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //To Update Existing Quote
        public Int32 Update_Quotes(Cls_Quotes Quote)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder Str = new StringBuilder();
                Str.Append(" Update quotes ");
                Str.Append(" Set ");
                Str.Append(" quotename=");
                Str.Append(" '");
                Str.Append(Quote.quotename.Replace(" '", "''"));
                Str.Append(" '");
                Str.Append(" ,");
                Str.Append(" quotecomment=");
                Str.Append(" '");
                Str.Append(Quote.quotecomment.Replace(" '", "''"));
                Str.Append(" '");
                Str.Append(" ,");
                Str.Append(" active=");
                Str.Append(" '");
                Str.Append(Quote.active);
                Str.Append(" '");
                Str.Append(" Where ");
                Str.Append(" quoteid=");
                Str.Append(Quote.quoteid);
                retVal = Service.DbFunctions.UpdateCommand(Str.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

       //To Delete Quote
       public Int32 Delete_Quote(Int32 quoteid)
       {
           Int32 retVal = 0;
           try
           {
               StringBuilder strmodules = new StringBuilder();
               strmodules.Append(" Delete from quotes ");
               strmodules.Append(" Where ");
               strmodules.Append(" quoteid=");
               strmodules.Append(quoteid);
               retVal = Service.DbFunctions.DeleteCommand(strmodules.ToString());
               return retVal;
           }
           catch
           {
               return retVal;
           }
       }


       //Insert New Quote comments to quotecomment Table
       public Int32 Insert_Quotescomment_to_quotecommentTable(Cls_Quotescomment Quote)
       {
           Int32 retVal = 0;
           try
           {
               StringBuilder Str = new StringBuilder();
               Str.Append(" Insert into quotecomment(quotename,quotecomment,purchaseorderid,dorder) ");
               Str.Append(" Values(" );
               Str.Append(" '");
               Str.Append(Quote.quotename.Replace(" '", "''"));
               Str.Append(" '");
               Str.Append(" ,");
               Str.Append(" '");
               Str.Append(Quote.quotecomment.Replace(" '", "''"));
               Str.Append(" '");
               Str.Append(" ,");
               Str.Append(Quote.purchaseorderid);
               Str.Append(" ,");
               Str.Append(Quote.dorder);
               Str.Append(" )");
               retVal = Service.DbFunctions.InsertCommand(Str.ToString());
               return retVal;
           }
           catch
           {
               return retVal;
           }
       }
       //To Delete Quote comment table by purchaseorderid
       public Int32 Delete_Quotescomment_From_quotecommentTable(Int32 purchaseorderid)
       {
           Int32 retVal = 0;
           try
           {
               StringBuilder strmodules = new StringBuilder();
               strmodules.Append(" Delete from quotecomment ");
               strmodules.Append(" Where ");
               strmodules.Append(" purchaseorderid=");
               strmodules.Append(purchaseorderid);
               retVal = Service.DbFunctions.DeleteCommand(strmodules.ToString());
               return retVal;
           }
           catch
           {
               return retVal;
           }
       }

      

       // Fetch Quotes By Quotes id
       public Cls_Quotes Fetch_Quotes_Byid(Int32 quoteid)
       {
           Cls_Quotes objQuotes = null;
           try
           {
               StringBuilder strmodules = new StringBuilder();
               strmodules.Append(" Select quoteid,quotename,quotecomment,active from quotes where quoteid='" + quoteid + "'");
               DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
               if (ds != null && ds.Tables.Count > 0)
               {
                   DataTable dt = ds.Tables[0];
                   if (dt != null && dt.Rows.Count > 0)
                   {
                       objQuotes = new Cls_Quotes();
                       objQuotes.quoteid = Convert.ToInt32(dt.Rows[0]["quoteid"]);
                       objQuotes.quotename = Convert.ToString(dt.Rows[0]["quotename"]);
                       objQuotes.quotecomment = Convert.ToString(dt.Rows[0]["quotecomment"]);
                       string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["active"]));
                       if (!string.IsNullOrEmpty(activeStatus))
                       {
                           if (activeStatus.ToUpper() == "TRUE")
                           {
                               objQuotes.active = true;
                           }
                           else
                           {
                               objQuotes.active = false;
                           }
                       }
                       else
                       {
                           objQuotes.active = false;
                       }
                   }
               }
               return objQuotes;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

        //Check for comment by  Purchase Order id
        public Int32 CheckCount_For_comment_Bypurchaseorderid(Int32 purchaseorderid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*) from quotecomment where purchaseorderid=" + purchaseorderid + "");
                retval = Service.DbFunctions.SelectCount(strname.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Add size
        // Upload comment image
        public Int32 Insert_size(string size, string cmsid)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into productsize (cmsid,size) VALUES(" );
                strInsert.Append(" ");
                strInsert.Append(cmsid);
                strInsert.Append(" ,'");
                strInsert.Append(size);
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch size
        public DataTable Fetch_size(string cmsid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strsize = new StringBuilder();
                strsize.Append("  select sizeid,cmsid,size from productsize where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strsize.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Add size
        public Int32 Delete_size(string sizeid)
        {
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from productsize where sizeid='" + sizeid + "'");
                return Service.DbFunctions.DeleteCommand(strDelete.ToString());
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        #endregion

        #region Add color
        // Upload image
        public Int32 Insert_color_Details(string cmsid, string color, string image)
        {
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert Into productcolor (cmsid,color,image) VALUES(" );
                strInsert.Append(" ");
                strInsert.Append(cmsid);
                strInsert.Append(" ,'");
                strInsert.Append(color);
                strInsert.Append(" ','");
                strInsert.Append(image);
                strInsert.Append(" '");
                strInsert.Append(" )");
                return Service.DbFunctions.InsertCommand(strInsert.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Upload image
        public Int32 Update_color_image(string colorid, string cmsid, string color, string image)
        {
            try
            {
                Int32 retval = 0;
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update productcolor ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" cmsid=");
                strUpdate.Append(cmsid);
                strUpdate.Append(" , ");
                strUpdate.Append(" color=");
                strUpdate.Append(" '");
                strUpdate.Append(color.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" image=");
                strUpdate.Append(" '");
                strUpdate.Append(image.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" colorid=");
                strUpdate.Append(colorid);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch color Details
        public DataTable Fetch_color_Details(string cmsid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strsize = new StringBuilder();
                strsize.Append("  select colorid,cmsid,color,image from productcolor where cmsid=" + cmsid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strsize.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch color Details
        public DataTable Fetch_color_Details_By_colorid(string colorid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strcolor = new StringBuilder();
                strcolor.Append("  select colorid,cmsid,color,image from productcolor where colorid=" + colorid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strcolor.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch color image Data Using By colorid
        public string Fetch_color_By_colorid(string colorid)
        {
            string colorimage = "";
            try
            {
                StringBuilder strcolor = new StringBuilder();
                strcolor.Append("  select image from productcolor where colorid=" + colorid + "");
                colorimage = Service.DbFunctions.SelectCountString(strcolor.ToString());
                if (colorimage != null && colorimage != "")
                {
                    return colorimage;
                }
                return colorimage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Product Details Using By cmsid
        public DataTable Get_One_Product_Detail(Int32 cmsid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strProduct = new StringBuilder();
                strProduct.Append("  SELECT q1.cmsid,q1.productname ,q2.description,q3.dorder,q5.imagename,q6.categoryname,q7.price,q8.Showprice,q9.Detaildescription,q10.productnumber,q11.internaldescription,q12.cost FROM");
                strProduct.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as productname from content_management_master");
                strProduct.Append("    Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  ");
                strProduct.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext = 'Product Name' ");
                strProduct.Append("   )  q1");
                strProduct.Append("   LEFT JOIN");
                strProduct.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as description from content_management_master");
                strProduct.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strProduct.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext = 'Description'");
                strProduct.Append("   ) q2");
                strProduct.Append("  ON q1.cmsid = q2.cmsid");
                strProduct.Append("  LEFT JOIN");
                strProduct.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  dorder from content_management_master");
                strProduct.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strProduct.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Display Order%' ");
                strProduct.Append("  ) q3");
                strProduct.Append("  ON q3.cmsid = q2.cmsid");
                strProduct.Append("  LEFT JOIN");
                strProduct.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue isactive from content_management_master");
                strProduct.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strProduct.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Is Active%' and fieldvalue='True'");
                strProduct.Append("  ) q4");
                strProduct.Append("  ON q4.cmsid = q3.cmsid");
                strProduct.Append("  LEFT JOIN");
                strProduct.Append("  (Select content_management_detail.cmsid,content_management_detail.fieldvalue as imagename from content_management_master");
                strProduct.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strProduct.Append("  where content_management_detail.cmsid=" + cmsid + " and fieldtext like '%Upload Image%'");
                strProduct.Append("  ) q5");
                strProduct.Append("  ON q5.cmsid = q4.cmsid");
                strProduct.Append("  LEFT JOIN (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  categoryname from content_management_master ");
                strProduct.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  and fieldtype like '%DropDown%' and fieldvalue NOT IN('Public','Internal') ) q6 ");
                strProduct.Append("  ON q6.cmsid = q5.cmsid ");
                strProduct.Append("  LEFT JOIN (Select content_management_detail.cmsid,CAST(content_management_detail.fieldvalue AS DECIMAL(10, 2)) price from content_management_master ");
                strProduct.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  and fieldtext='Price'  ) q7 ");
                strProduct.Append("  ON q1.cmsid = q7.cmsid ");
                strProduct.Append("  LEFT JOIN (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  Showprice from content_management_master ");
                strProduct.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "   and fieldtext like '%Show Price%'   ) q8 ");
                strProduct.Append("  ON q7.cmsid = q8.cmsid ");
                strProduct.Append("  LEFT JOIN ");
                strProduct.Append("   (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  Detaildescription from content_management_master ");
                strProduct.Append("  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  ");
                strProduct.Append("   and fieldtext = 'Detail description'   ) q9  ON q8.cmsid = q9.cmsid ");
                strProduct.Append("  LEFT JOIN ");
                strProduct.Append("   (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  productnumber from content_management_master ");
                strProduct.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  ");
                strProduct.Append("   and fieldtext = 'Product Number' ) q10  ON q9.cmsid = q10.cmsid ");
                strProduct.Append("  LEFT JOIN ");
                strProduct.Append("   (Select content_management_detail.cmsid,content_management_detail.fieldvalue as  internaldescription from content_management_master ");
                strProduct.Append("   Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid  where content_management_detail.cmsid=" + cmsid + "  ");
                strProduct.Append("   and fieldtext = 'Internal Description' ) q11  ON q10.cmsid = q11.cmsid ");
                strProduct.Append("  LEFT JOIN (Select content_management_detail.cmsid,content_management_detail.fieldvalue as cost from  ");
                strProduct.Append("  content_management_master  Inner join content_management_detail On content_management_detail.cmsid=content_management_master.cmsid ");
                strProduct.Append("  where content_management_detail.cmsid=" + cmsid + "  and fieldtext='Cost'  ) q12  ON q11.cmsid = q12.cmsid ");
                strProduct.Append("  where q4.isactive='True' ");
                strProduct.Append("  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strProduct.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Delete color
        public Int32 Delete_color(string colorid)
        {
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from productcolor where colorid='" + colorid + "'");
                return Service.DbFunctions.DeleteCommand(strDelete.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Fetch Default size And color
        public DataTable Fetch_Product_Default_size_color(string cmsid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select Pcolor.cmsid, Pcolor.color,Psize.size from productcolor Pcolor inner join productsize Psize on Pcolor.cmsid=Psize.cmsid order by Pcolor.cmsid asc limit 1");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region new user guide

        //======FOR THE PURPOSE TO GET USER ROLE id BASED ON USER id
        public DataTable Fetch_LeftSide_submodules_By_roleid_UserGuide(Int32 roleid, Int32 moduleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select DISTINCT roleprivileges.moduleid,roleprivileges.submoduleid,submodules.submodulename,submodules.displayorder,roleprivileges.isadd,roleprivileges.isedit,isdelete from roleprivileges    ");
                strRolePrivilege.Append("  Inner Join submodules ON submodules.submoduleid= roleprivileges.submoduleid ");
                strRolePrivilege.Append("  and roleprivileges.isview=True and roleprivileges.moduleid=" + moduleid + " and roleid="+roleid+" ");
                strRolePrivilege.Append("  order by submodules.displayorder  ");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch All moduleid and modulename For DropDown od Module name
        public DataTable Fetch_ModulesName_UserGuide()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select '0' moduleid,'---Select Module---' modulename,0 displayorder ");
                strmodules.Append(" UNION ");
                strmodules.Append(" Select moduleid,modulename,displayorder from modules where active=True order by displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtModule = ds.Tables[0];
                    dt = SortTable(dtModule, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // fetch submodule on basis of module
        public DataTable Fetch_SubModulesname(int moduleid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append("  Select submoduleid,submodulename from submodules where moduleid=" + moduleid);
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_SubModulesname_UserGuide(int moduleid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strimage = new StringBuilder();
                strimage.Append(" Select '0' submoduleid,'---Select SubModule---'submodulename,0 displayorder ");
                strimage.Append("  UNION ");
                strimage.Append("  Select submoduleid,submodulename,displayorder from submodules where moduleid=" + moduleid + " order by displayorder ");
                DataSet ds = Service.DbFunctions.SelectCommand(strimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Fetch user guide module
        public DataTable Fetch_UserGuidemodules()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUserGuidemodules = new StringBuilder();
                strUserGuidemodules.Append(" Select userguidemodules.maincontentid,userguidemodules.moduleid,submodules.submodulename,userguidemodules.submoduleid,userguidemodules.displayorder,");
                strUserGuidemodules.Append(" userguidemodules.isactive,userguidemodules.isexternalcontent from userguidemodules  inner join ");
                strUserGuidemodules.Append(" submodules  ON submodules.submoduleid=userguidemodules.submoduleid Order By userguidemodules.displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strUserGuidemodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Maximum Display Order for userGuideModuleFetch_Modules_ById
        public Int32 Get_Max_UserGuidemodules_dorder()
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from userguidemodules");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert New User Guide Module
        public Int32 Insert_UserGuidemodules(Cls_UserGuidemodules clsUserGuidemodules)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserGuidemodules = new StringBuilder();
                strUserGuidemodules.Append(" Insert into userguidemodules(moduleid,submoduleid,description,isexternalcontent,displayorder,isactive,createdby,createddate) ");
                strUserGuidemodules.Append(" Values(" );
                strUserGuidemodules.Append(clsUserGuidemodules.moduleid);
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(clsUserGuidemodules.submoduleid);
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.description.Replace(" '", "''"));
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.isexternalcontent);
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(clsUserGuidemodules.displayorder);
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.isactive);
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.createdby);
                strUserGuidemodules.Append("'");
                strUserGuidemodules.Append(",");
                strUserGuidemodules.Append(" LOCALTIMESTAMP");
                strUserGuidemodules.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strUserGuidemodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Update Existing User Guide Module
        public Int32 Update_UserGuidemodules(Cls_UserGuidemodules clsUserGuidemodules)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserGuidemodules = new StringBuilder();
                strUserGuidemodules.Append(" Update userguidemodules ");
                strUserGuidemodules.Append(" Set ");
                strUserGuidemodules.Append(" moduleid=");
                strUserGuidemodules.Append(clsUserGuidemodules.moduleid);
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" submoduleid=");
                strUserGuidemodules.Append(clsUserGuidemodules.submoduleid);
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" description=");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.description.Replace(" '", "''"));
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" isactive=");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.isactive);
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" displayorder=");
                strUserGuidemodules.Append(clsUserGuidemodules.displayorder);
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" isexternalcontent=");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.isexternalcontent);
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" modifiedby=");
                strUserGuidemodules.Append(" '");
                strUserGuidemodules.Append(clsUserGuidemodules.modifiedby);
                strUserGuidemodules.Append("'");
                strUserGuidemodules.Append(" ,");
                strUserGuidemodules.Append(" lastmodificationdate=");
                strUserGuidemodules.Append(" LOCALTIMESTAMP ");
                strUserGuidemodules.Append(" Where ");
                strUserGuidemodules.Append(" maincontentid=");
                strUserGuidemodules.Append(clsUserGuidemodules.maincontentid);
                retVal = Service.DbFunctions.UpdateCommand(strUserGuidemodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // Fetch user guide modules By  id
        public Cls_UserGuidemodules Fetch_UserGuidemodules_Byid(Int32 maincontentid)
        {
            Cls_UserGuidemodules objUserGuideModule = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select maincontentid,moduleid,submoduleid,displayorder,isactive,description,isexternalcontent from userguidemodules where maincontentid='" + maincontentid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objUserGuideModule = new Cls_UserGuidemodules();
                        objUserGuideModule.moduleid = Convert.ToInt32(dt.Rows[0]["moduleid"]);
                        objUserGuideModule.submoduleid = Convert.ToInt32(dt.Rows[0]["submoduleid"]);
                        objUserGuideModule.description = Convert.ToString(dt.Rows[0]["description"]);
                        objUserGuideModule.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        string IsExternalStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isexternalcontent"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objUserGuideModule.isactive = true;
                            }
                            else
                            {
                                objUserGuideModule.isactive = false;
                            }
                        }
                        if (!string.IsNullOrEmpty(IsExternalStatus))
                        {
                            if (IsExternalStatus.ToUpper() == "TRUE")
                            {
                                objUserGuideModule.isexternalcontent = true;
                            }
                            else
                            {
                                objUserGuideModule.isexternalcontent = false;
                            }
                        }
                        else
                        {
                            objUserGuideModule.isactive = false;
                        }
                    }
                }
                return objUserGuideModule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 Update_UserGuideModule_dorder(List<Cls_UserGuidemodules> objUserGuidemodules)
        {
            Int32 retVal = 0;
            try
            {
                if (objUserGuidemodules != null && objUserGuidemodules.Count > 0)
                {
                    for (Int32 i = 0; i < objUserGuidemodules.Count; i++)
                    {

                        StringBuilder strUserGuidemodules = new StringBuilder();
                        strUserGuidemodules.Append(" Update userguidemodules ");
                        strUserGuidemodules.Append(" Set ");
                        strUserGuidemodules.Append(" displayorder=");
                        strUserGuidemodules.Append(objUserGuidemodules[i].displayorder);
                        strUserGuidemodules.Append(" ,");
                        strUserGuidemodules.Append(" modifiedby=");
                        strUserGuidemodules.Append(" '");
                        strUserGuidemodules.Append(objUserGuidemodules[i].modifiedby);
                        strUserGuidemodules.Append("'");
                        strUserGuidemodules.Append(" ,");
                        strUserGuidemodules.Append(" lastmodificationdate=");
                        strUserGuidemodules.Append(" LOCALTIMESTAMP ");
                        strUserGuidemodules.Append(" Where ");
                        strUserGuidemodules.Append(" maincontentid=");
                        strUserGuidemodules.Append(objUserGuidemodules[i].maincontentid);
                        retVal = Service.DbFunctions.UpdateCommand(strUserGuidemodules.ToString());
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch user guide module for dashboard
        public DataTable Fetch_UserGuidemodules_ForDashboard()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUserGuidemodules = new StringBuilder();
                strUserGuidemodules.Append(" Select userguidemodules.maincontentid,userguidemodules.moduleid,submodules.submodulename,modules.modulename,userguidemodules.submoduleid,userguidemodules.displayorder,");
                strUserGuidemodules.Append("  userguidemodules.isactive,userguidemodules.isexternalcontent from userguidemodules  inner join ");
                strUserGuidemodules.Append("  submodules  ON submodules.submoduleid=userguidemodules.submoduleid inner join modules  ON modules.moduleid=userguidemodules.moduleid");
                strUserGuidemodules.Append("  Order By userguidemodules.displayorder");
                DataSet ds = Service.DbFunctions.SelectCommand(strUserGuidemodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Reordering functions block for User Guide modules Display Order
        public void Reorder_UserGuidemodules(Int32 maincontentid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_UserGuidemodules_dorder();
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "select  maincontentid from userguidemodules where displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["maincontentid"].ToString());
                        query = string.Format(" Update userguidemodules SET displayorder=displayorder-1 where maincontentid={0}", maincontentid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguidemodules SET displayorder=displayorder+1 where maincontentid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "select  maincontentid from userguidemodules where displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["maincontentid"].ToString());
                        query = string.Format(" Update userguidemodules SET displayorder=displayorder+1 where maincontentid={0}", maincontentid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguidemodules SET displayorder=displayorder-1 where maincontentid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Check for Existing User Guide Module 
        public Int32 Check_Existing_UserGuideModule(Int32 moduleid, Int32 submoduleid, Int32 maincontentid, Int32 Mode)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                if (Mode == 1)// When Inserted
                {
                    strname.Append(" Select COUNT(*)  from userguidemodules where moduleid='" + moduleid + "' and submoduleid='" + submoduleid + "' ");
                }
                else// When Updated
                {
                    strname.Append(" Select COUNT(*)  from userguidemodules where moduleid='" + moduleid + "' and submoduleid='" + submoduleid + "' and maincontentid<>" + maincontentid + " ");
                }
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //fetch side menu using Roll id
        public DataTable Fetch_SubModules_By_roleid_UserGuide(Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strRolePrivilege = new StringBuilder();
                strRolePrivilege.Append(" Select DISTINCT roleprivileges.moduleid,roleprivileges.submoduleid,submodules.submodulename,submodules.displayorder,roleprivileges.isadd,roleprivileges.isedit,isdelete from roleprivileges    ");
                strRolePrivilege.Append("  Inner Join submodules ON submodules.submoduleid= roleprivileges.submoduleid ");
                strRolePrivilege.Append("  and roleprivileges.isview=True and roleid="+roleid+" ");
                DataSet ds = Service.DbFunctions.SelectCommand(strRolePrivilege.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region UserGuideimage

        // Fetch user guide image
        public DataTable Fetch_images_BasedOn_sourceidAndname(Int32 sourceid, string sourcename)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strUserGuideimages = new StringBuilder();
                strUserGuideimages.Append(" select userguideimages.imageid,userguideimages.imagename ,userguideimages.source,userguideimages.sourceid ,userguideimages.roleid ,userroles.rolename,");
                strUserGuideimages.Append("  userguideimages.displayorder,userguideimages.isactive,userguideimages.imagewidth,userguideimages.imageheight from userguideimages ");
                strUserGuideimages.Append("   inner join userroles  ON userroles.roleid=userguideimages.roleid where sourceid=" + sourceid + " and source='" + sourcename + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strUserGuideimages.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Delete image
        public Int32 Delete_UserGuideimage(Int32 imageid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append(" Delete from userguideimages ");
                str.Append(" Where ");
                str.Append(" imageid=");
                str.Append(imageid);
                retVal = Service.DbFunctions.DeleteCommand(str.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // Reordering image Display Order
        public void Reorder_UserGuideimage(Int32 imageid, Int32 displayorder, string OpType, Int32 sourceid, string sourcename)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_UserGuideimage_dorder(sourceid, sourcename);
                if (OpType == "UP" && displayorder != 1)
                {
                    query = "select  imageid from userguideimages where displayorder=" + (displayorder - 1) + " and sourceid=" + sourceid + " limit 1 ";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["imageid"].ToString());
                        query = string.Format(" Update userguideimages SET displayorder=displayorder-1 where imageid={0}", imageid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguideimages SET displayorder=displayorder+1 where imageid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
                if (OpType == "DOWN" && displayorder != MaxOrder)
                {
                    query = "select imageid from userguideimages where displayorder=" + (displayorder + 1) + " and sourceid=" + sourceid + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["imageid"].ToString());
                        query = string.Format(" Update userguideimages SET displayorder=displayorder+1 where imageid={0}", imageid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguideimages SET displayorder=displayorder-1 where imageid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Max image By sourceid for Display Order
        public Int32 Get_Max_UserGuideimage_dorder(Int32 sourceid, string source)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from userguideimages where sourceid=" + sourceid + " and source='" + source + "'");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Adjust user guide image Display Order 
        public void Adjust_image_dorder(Int32 sourceid, string source)
        {
            try
            {
                DataTable Dtcontent = null;
                string query = "select * from userguideimages where sourceid=" + sourceid + " and source='" + source + "' ";
                DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    Dtcontent = SortTable(dt, "displayorder");
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int index;
                    for (index = 0; index < Dtcontent.Rows.Count; index++)
                    {
                        Int32 id = Convert.ToInt32(Dtcontent.Rows[index]["imageid"]);
                        Int32 displayorder = Convert.ToInt32(Dtcontent.Rows[index]["displayorder"]);

                        if (displayorder != (index + 1))
                        {
                            query = string.Format(" Update userguideimages SET displayorder=" + (index + 1) + " where imageid={0}", id);
                            Service.DbFunctions.UpdateCommand(query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch All role
        public DataTable Fetch_Rolename()
        {
            DataTable dt = null;
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append(" select roleid,rolename,displayorder from userroles where active=True  order by displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(str.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtrole = ds.Tables[0];
                    dt = SortTable(dtrole, "displayorder");
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch user guide modules By  id
        public Cls_UserGuideimage Fetch_UserGuideimage_Byid(Int32 imageid)
        {
            Cls_UserGuideimage objUserGuideimage = null;
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append(" Select userguideimages.imageid,userguideimages.imagename,userguideimages.source,userguideimages.sourceid,userguideimages.isactive, ");
                str.Append("  userguideimages.displayorder,userguideimages.imageheight,userguideimages.imagewidth,userguideimages.roleid,userroles.rolename ");
                str.Append(" from userguideimagesinner join userroles  ON userroles.roleid=userguideimages.roleid where userguideimages.imageid='" + imageid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(str.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objUserGuideimage = new Cls_UserGuideimage();
                        objUserGuideimage.imageid = Convert.ToInt32(dt.Rows[0]["imageid"]);
                        objUserGuideimage.imagename = Convert.ToString(dt.Rows[0]["imagename"]);
                        objUserGuideimage.sourceid = Convert.ToInt32(dt.Rows[0]["sourceid"]);
                        objUserGuideimage.source = Convert.ToString(dt.Rows[0]["source"]);
                        objUserGuideimage.imageheight = Convert.ToInt32(dt.Rows[0]["imageheight"]);
                        objUserGuideimage.imagewidth = Convert.ToInt32(dt.Rows[0]["imagewidth"]);
                        objUserGuideimage.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        objUserGuideimage.roleid = Convert.ToInt32(dt.Rows[0]["roleid"]);
                        string rolename = Convert.ToString(dt.Rows[0]["rolename"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objUserGuideimage.isactive = true;
                            }
                            else
                            {
                                objUserGuideimage.isactive = false;
                            }
                        }
                    }
                }
                return objUserGuideimage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // check existing user guide image
        public Int32 Check_Existing_UserGuideimage(string imagename)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*)  from userguideimages where imagename='" + imagename + "' ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert New User Guide image
        public Int32 Insert_UserGuideimages(Cls_UserGuideimage clsUserGuideimages)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserGuideimages = new StringBuilder();
                strUserGuideimages.Append(" Insert into userguideimages(imagename,roleid,source,sourceid,imagewidth,imageheight,displayorder,isactive,createdby,createddate) ");
                strUserGuideimages.Append(" Values(" );
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.imagename.Replace(" '", "''"));
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(clsUserGuideimages.roleid);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.source.Replace(" '", "''"));
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(clsUserGuideimages.sourceid);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(clsUserGuideimages.imagewidth);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(clsUserGuideimages.imageheight);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(clsUserGuideimages.displayorder);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.isactive);
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.createdby);
                strUserGuideimages.Append("'");
                strUserGuideimages.Append(",");
                strUserGuideimages.Append(" LOCALTIMESTAMP");
                strUserGuideimages.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strUserGuideimages.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Update Existing User Guide images
        public Int32 Update_UserGuideimages(Cls_UserGuideimage clsUserGuideimages)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUserGuideimages = new StringBuilder();
                strUserGuideimages.Append(" Update userguideimages ");
                strUserGuideimages.Append(" Set ");
                strUserGuideimages.Append(" roleid=");
                strUserGuideimages.Append(clsUserGuideimages.roleid);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" imagename=");
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.imagename.Replace(" '", "''"));
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" isactive=");
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.isactive);
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" displayorder=");
                strUserGuideimages.Append(clsUserGuideimages.displayorder);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" imageheight=");
                strUserGuideimages.Append(clsUserGuideimages.imageheight);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" imagewidth=");
                strUserGuideimages.Append(clsUserGuideimages.imagewidth);
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" modifiedby=");
                strUserGuideimages.Append(" '");
                strUserGuideimages.Append(clsUserGuideimages.modifiedby);
                strUserGuideimages.Append("'");
                strUserGuideimages.Append(" ,");
                strUserGuideimages.Append(" lastmodifieddate=");
                strUserGuideimages.Append(" LOCALTIMESTAMP ");
                strUserGuideimages.Append(" Where ");
                strUserGuideimages.Append(" imageid=");
                strUserGuideimages.Append(clsUserGuideimages.imageid);
                retVal = Service.DbFunctions.UpdateCommand(strUserGuideimages.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        // update display order
        public Int32 Update_UserGuideimages_dorder(List<Cls_UserGuideimage> objUserGuideimages)
        {
            Int32 retVal = 0;
            try
            {
                if (objUserGuideimages != null && objUserGuideimages.Count > 0)
                {
                    for (Int32 i = 0; i < objUserGuideimages.Count; i++)
                    {

                        StringBuilder strUserGuidemodules = new StringBuilder();
                        strUserGuidemodules.Append(" Update userguideimages ");
                        strUserGuidemodules.Append(" Set ");
                        strUserGuidemodules.Append(" displayorder=");
                        strUserGuidemodules.Append(objUserGuideimages[i].displayorder);
                        strUserGuidemodules.Append(" ,");
                        strUserGuidemodules.Append(" modifiedby=");
                        strUserGuidemodules.Append(" '");
                        strUserGuidemodules.Append(objUserGuideimages[i].modifiedby);
                        strUserGuidemodules.Append("'");
                        strUserGuidemodules.Append(" ,");
                        strUserGuidemodules.Append(" lastmodifieddate=");
                        strUserGuidemodules.Append(" LOCALTIMESTAMP ");
                        strUserGuidemodules.Append(" Where ");
                        strUserGuidemodules.Append(" imageid=");
                        strUserGuidemodules.Append(objUserGuideimages[i].imageid);
                        retVal = Service.DbFunctions.UpdateCommand(strUserGuidemodules.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  User Guide Action description

        // Bind User Guid Action description data By ModelDetailid
        public DataTable Fetch_UG_ActionDesc_Byid(Int32 ModelDetailid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsubContent = new StringBuilder();
                strsubContent.Append(" Select * ");
                strsubContent.Append("  from userguideactiondescription where  moduledetailid=" + ModelDetailid + " order by displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubContent.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Delete From userguideactiondescription BY actiondetailid
        public Int32 Delete_UserGuideActiondescription_By_id(Int32 actiondetailid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Delete from userguideactiondescription ");
                strUGMainContent.Append(" Where ");
                strUGMainContent.Append(" actiondetailid=");
                strUGMainContent.Append(actiondetailid);
                retVal = Service.DbFunctions.DeleteCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Reordering functions block for User Guide Action description Display Order
        public void Reorder_UserGuideAction(Int32 actiondetailid, Int32 moduledetailid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_UserGuideAction_dorder(moduledetailid);
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "select  actiondetailid from userguideactiondescription where displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["actiondetailid"].ToString());
                        query = string.Format(" Update userguideactiondescription SET displayorder=displayorder-1 where actiondetailid={0}", actiondetailid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguideactiondescription SET displayorder=displayorder+1 where actiondetailid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "select actiondetailid from userguideactiondescription where displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["maincontentid"].ToString());
                        query = string.Format(" Update userguideactiondescription SET displayorder=displayorder+1 where actiondetailid={0}", actiondetailid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguideactiondescription SET displayorder=displayorder-1 where actiondetailid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Maximum Display Order 
        public Int32 Get_Max_UserGuideAction_dorder(Int32 moduledetailid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from userguideactiondescription where moduledetailid=" + moduledetailid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch User Guide Action description Data By actiondetailid
        public Cls_UGActionDesc Fetch_UG_ActionData_Byid(Int32 actiondetailid)
        {
            Cls_UGActionDesc objMainContent = null;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Select actiondetailid,heading,description,displayorder,isactive from userguideactiondescription where actiondetailid='" + actiondetailid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strUGMainContent.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objMainContent = new Cls_UGActionDesc();
                        objMainContent.actiondetailid = Convert.ToInt32(dt.Rows[0]["actiondetailid"]);
                        objMainContent.heading = Convert.ToString(dt.Rows[0]["heading"]);
                        objMainContent.description = Convert.ToString(dt.Rows[0]["description"]);
                        objMainContent.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objMainContent.isactive = true;
                            }
                            else
                            {
                                objMainContent.isactive = false;
                            }
                        }
                        else
                        {
                            objMainContent.isactive = false;
                        }
                    }
                }
                return objMainContent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Fetch_UG_ActionData()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select actiondetailid,heading,displayorder,description,isactive activeStatus ");
                strmodules.Append(" , Case when isactive=True THEN 'Checked' else '' END isactive  ");
                strmodules.Append(" from userguideactiondescription Order By displayorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert Into User Guide Action Desription
        public Int32 Insert_UG_Action(Cls_UGActionDesc objMainContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Insert into userguideactiondescription(heading,moduledetailid,description,displayorder,isactive,createdby,createddate)");
                strUGMainContent.Append("  Values(" );
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.heading.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(objMainContent.moduledetailid);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.description.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(objMainContent.displayorder);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.isactive);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.createdby);
                strUGMainContent.Append("'");
                strUGMainContent.Append(",");
                strUGMainContent.Append(" LOCALTIMESTAMP");
                strUGMainContent.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Update User Guide description Data
        public Int32 Update_UG_Action(Cls_UGActionDesc objMainContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUGMainContent = new StringBuilder();
                strUGMainContent.Append(" Update userguideactiondescription ");
                strUGMainContent.Append(" Set ");
                strUGMainContent.Append(" heading=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.heading.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" description=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.description.Replace(" '", "''"));
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" displayorder=");
                strUGMainContent.Append(objMainContent.displayorder);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" moduledetailid=");
                strUGMainContent.Append(objMainContent.moduledetailid);
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" isactive=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.isactive);
                strUGMainContent.Append(" '");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" modifiedby=");
                strUGMainContent.Append(" '");
                strUGMainContent.Append(objMainContent.modifiedby);
                strUGMainContent.Append("'");
                strUGMainContent.Append(" ,");
                strUGMainContent.Append(" lastmodificationdate=");
                strUGMainContent.Append(" LOCALTIMESTAMP ");
                strUGMainContent.Append(" Where ");
                strUGMainContent.Append(" actiondetailid=");
                strUGMainContent.Append(objMainContent.actiondetailid);
                retVal = Service.DbFunctions.UpdateCommand(strUGMainContent.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        //Update User Guide Action description Display Order
        public Int32 Update_UG_Action_displayorder(List<Cls_UGActionDesc> objMainContent)
        {
            Int32 retVal = 0;
            try
            {
                if (objMainContent != null && objMainContent.Count > 0)
                {
                    for (Int32 i = 0; i < objMainContent.Count; i++)
                    {

                        StringBuilder strmodules = new StringBuilder();
                        strmodules.Append(" Update userguideactiondescription ");
                        strmodules.Append(" Set ");
                        strmodules.Append(" displayorder=");
                        strmodules.Append(objMainContent[i].displayorder);
                        strmodules.Append(" ,");
                        strmodules.Append(" modifiedby=");
                        strmodules.Append(" '");
                        strmodules.Append(objMainContent[i].modifiedby);
                        strmodules.Append("'");
                        strmodules.Append(" ,");
                        strmodules.Append(" lastmodificationdate=");
                        strmodules.Append(" LOCALTIMESTAMP ");
                        strmodules.Append(" Where ");
                        strmodules.Append(" actiondetailid=");
                        strmodules.Append(objMainContent[i].actiondetailid);
                        retVal = Service.DbFunctions.UpdateCommand(strmodules.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region UserGuide Detail
        // Fetch user guide detail 
        public DataTable Fetch_ModuleDetailContent_Byid(Int32 maincontentid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("     Select * from userguidemoduledetail where maincontentid=" + maincontentid + " order by displayorder asc ");
                DataSet ds = Service.DbFunctions.SelectCommand(str.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Delete Mudule detail By id
        public Int32 Delete_ModuleDetailContent(Int32 moduledetailid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Delete from userguidemoduledetail ");
                strmodules.Append(" Where ");
                strmodules.Append(" moduledetailid=");
                strmodules.Append(moduledetailid);
                retVal = Service.DbFunctions.DeleteCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //Reordering functions block for ModuleDetail Content Display Order
        public void Reorder_ModuleDetailContent(Int32 moduledetailid, Int32 maincontentid, Int32 CurrentOrder, string OpType)
        {
            try
            {
                string query = "";
                DataTable Dtcontent = null;
                DataSet ds = null;
                Int64 MaxOrder = Get_Max_ModuleDetailContent_dorder(maincontentid);
                if (OpType == "UP" && CurrentOrder != 1)
                {
                    query = "select  moduledetailid from userguidemoduledetail where maincontentid=" + maincontentid + " and displayorder=" + (CurrentOrder - 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Previousid = Convert.ToInt32(Dtcontent.Rows[0]["moduledetailid"].ToString());
                        query = string.Format(" Update userguidemoduledetail SET displayorder=displayorder-1 where moduledetailid={0}", moduledetailid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguidemoduledetail SET displayorder=displayorder+1 where moduledetailid={0}", Previousid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                    else
                    {
                        Adjust_Submodules_Dorder(maincontentid);
                    }
                }
                if (OpType == "DOWN" && CurrentOrder != MaxOrder)
                {
                    query = "select  moduledetailid from userguidemoduledetail where maincontentid=" + maincontentid + " and displayorder=" + (CurrentOrder + 1) + " limit 1";
                    ds = Service.DbFunctions.SelectCommand(query.ToString());
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Dtcontent = ds.Tables[0];
                    }
                    if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                    {
                        int Nextid = Convert.ToInt32(Dtcontent.Rows[0]["moduledetailid"].ToString());
                        query = string.Format(" Update userguidemoduledetail SET displayorder=displayorder+1 where moduledetailid={0}", moduledetailid);
                        Service.DbFunctions.UpdateCommand(query);
                        query = string.Format(" Update userguidemoduledetail SET displayorder=displayorder-1 where moduledetailid={0} ", Nextid);
                        Service.DbFunctions.UpdateCommand(query);
                    }
                    else
                    {
                        Adjust_Submodules_Dorder(maincontentid);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Max  ModuleDetail By ModuleContent for Display Order
        public Int32 Get_Max_ModuleDetailContent_dorder(Int32 maincontentid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select COALESCE(MAX(displayorder),0) as Maxdorder from userguidemoduledetail where maincontentid=" + maincontentid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Adjust ModuleDetail content 
        public void Adjust_MuduleDetailContent_dorder(Int32 maincontentid)
        {
            try
            {
                DataTable Dtcontent = null;
                string query = "select * from userguidemoduledetail where maincontentid=" + maincontentid + " Order by displayorder";
                DataSet ds = Service.DbFunctions.SelectCommand(query.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    Dtcontent = ds.Tables[0];
                }
                if (Dtcontent != null && Dtcontent.Rows.Count > 0)
                {
                    int index;
                    for (index = 0; index < Dtcontent.Rows.Count; index++)
                    {
                        Int32 id = Convert.ToInt32(Dtcontent.Rows[index]["moduledetailid"]);
                        Int32 displayorder = Convert.ToInt32(Dtcontent.Rows[index]["displayorder"]);

                        if (displayorder != (index + 1))
                        {
                            query = string.Format(" Update userguidemoduledetail SET displayorder=" + (index + 1) + " where moduledetailid={0}", id);
                            Service.DbFunctions.UpdateCommand(query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Fetch UserGuide Module deatil
        public Cls_UserGuideModuleDetail Fetch_UserGuideModuleDetail_Byid(Int32 moduledetailid)
        {
            Cls_UserGuideModuleDetail objModuleDetail = null;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Select * from userguidemoduledetail where moduledetailid=" + moduledetailid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strsubmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objModuleDetail = new Cls_UserGuideModuleDetail();
                        objModuleDetail.moduledetailid = Convert.ToInt32(dt.Rows[0]["moduledetailid"]);
                        objModuleDetail.contentheading = Convert.ToString(dt.Rows[0]["contentheading"]);
                        objModuleDetail.description = Convert.ToString(dt.Rows[0]["description"]);
                        objModuleDetail.displayorder = Convert.ToInt32(dt.Rows[0]["displayorder"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objModuleDetail.isactive = true;
                            }
                            else
                            {
                                objModuleDetail.isactive = false;
                            }
                        }
                        else
                        {
                            objModuleDetail.isactive = false;
                        }
                    }
                }
                return objModuleDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Check for Existing user guide ContentDetail
        public Int32 Check_Existing_USerGuideContentDetail(string contentheading)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strname = new StringBuilder();
                strname.Append(" Select COUNT(*)  from userguidemoduledetail where contentheading='" + contentheading + "' ");
                object objCount = Service.DbFunctions.SelectCount(strname.ToString());
                if (objCount != null)
                {
                    retval = Convert.ToInt32(objCount);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // To Insert New Userguide Detail
        public Int32 Insert_UserGuideContentDetail(Cls_UserGuideModuleDetail clsUserGuideModuleDetail)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append(" Insert into userguidemoduledetail(contentheading,maincontentid,description,displayorder,isactive,createdby,createddate) ");
                str.Append(" Values(" );
                str.Append(" '");
                str.Append(clsUserGuideModuleDetail.contentheading.Replace(" '", "''"));
                str.Append(" '");
                str.Append(" ,");
                str.Append(clsUserGuideModuleDetail.maincontentid);
                str.Append(" ,");
                str.Append(" '");
                str.Append(clsUserGuideModuleDetail.description.Replace(" '", "''"));
                str.Append(" '");
                str.Append(" ,");
                str.Append(clsUserGuideModuleDetail.displayorder);
                str.Append(" ,");
                str.Append(" '");
                str.Append(clsUserGuideModuleDetail.isactive);
                str.Append(" '");
                str.Append(" ,");
                str.Append(" '");
                str.Append(clsUserGuideModuleDetail.createdby);
                str.Append("'");
                str.Append(",");
                str.Append(" LOCALTIMESTAMP");
                str.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(str.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Update Existing User guide detail content
        public Int32 Update_UserGuideContentDetail(Cls_UserGuideModuleDetail clsUserGuideModuleDetail)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strsubmodules = new StringBuilder();
                strsubmodules.Append(" Update userguidemoduledetail ");
                strsubmodules.Append(" Set ");
                strsubmodules.Append(" contentheading=");
                strsubmodules.Append(" '");
                strsubmodules.Append(clsUserGuideModuleDetail.contentheading.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" maincontentid=");
                strsubmodules.Append(clsUserGuideModuleDetail.maincontentid);
                strsubmodules.Append(" ,");
                strsubmodules.Append(" description=");
                strsubmodules.Append(" '");
                strsubmodules.Append(clsUserGuideModuleDetail.description.Replace(" '", "''"));
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" displayorder=");
                strsubmodules.Append(clsUserGuideModuleDetail.displayorder);
                strsubmodules.Append(" ,");
                strsubmodules.Append(" isactive=");
                strsubmodules.Append(" '");
                strsubmodules.Append(clsUserGuideModuleDetail.isactive);
                strsubmodules.Append(" '");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" modifiedby=");
                strsubmodules.Append(" '");
                strsubmodules.Append(clsUserGuideModuleDetail.modifiedby);
                strsubmodules.Append("'");
                strsubmodules.Append(" ,");
                strsubmodules.Append(" lastmodifieddate=");
                strsubmodules.Append(" LOCALTIMESTAMP ");
                strsubmodules.Append(" Where ");
                strsubmodules.Append(" moduledetailid=");
                strsubmodules.Append(clsUserGuideModuleDetail.moduledetailid);
                retVal = Service.DbFunctions.UpdateCommand(strsubmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // To Update Existing User Guide Detail Display order
        public Int32 Update_UserGuideDetail_dorder(List<Cls_UserGuideModuleDetail> objUserGuideDetail)
        {
            Int32 retVal = 0;
            try
            {
                if (objUserGuideDetail != null && objUserGuideDetail.Count > 0)
                {
                    for (Int32 i = 0; i < objUserGuideDetail.Count; i++)
                    {

                        StringBuilder strsubmodules = new StringBuilder();
                        strsubmodules.Append(" Update userguidemoduledetail ");
                        strsubmodules.Append(" Set ");
                        strsubmodules.Append(" displayorder=");
                        strsubmodules.Append(objUserGuideDetail[i].displayorder);
                        strsubmodules.Append(" ,");
                        strsubmodules.Append(" modifiedby=");
                        strsubmodules.Append(" '");
                        strsubmodules.Append(objUserGuideDetail[i].modifiedby);
                        strsubmodules.Append("'");
                        strsubmodules.Append(" ,");
                        strsubmodules.Append(" lastmodifieddate=");
                        strsubmodules.Append(" LOCALTIMESTAMP ");
                        strsubmodules.Append(" Where ");
                        strsubmodules.Append(" moduledetailid=");
                        strsubmodules.Append(objUserGuideDetail[i].moduledetailid);
                        retVal = Service.DbFunctions.UpdateCommand(strsubmodules.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // functions for Binding UserGuide on display Page start here
        //Step.1 TO Get User Guide Main modules Details
        public DataTable Get_Main_Module_Data(Int32 moduleid, Int32 submoduleid)
        {
            DataTable dt = null;
            try
            {
                DataSet ds = Service.DbFunctions.SelectCommand(" Select * from userguidemodules where moduleid=" + moduleid + " and submoduleid=" + submoduleid + "");
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Step.2 TO Get User Guide Main modules images Details
        public DataTable Get_User_Guide_images(Int32 sourceid, string sourcename, Int32 roleid)
        {
            DataTable dt = null;
            try
            {
                DataSet ds = Service.DbFunctions.SelectCommand(" Select * from userguideimages where sourceid=" + sourceid + " and source='" + sourcename.Replace(" '", "''") + "' and roleid="+roleid+"");
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Step.3 To Get User Guide Module Actions Details
        public DataTable Get_UserGuide_Module_Detail(Int32 maincontentid)
        {

            DataTable dt = null;
            try
            {
                DataSet ds = Service.DbFunctions.SelectCommand(" Select * from userguidemoduledetail where maincontentid='" + maincontentid + "'");
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Step.4 To Get User Guide Module Actions description
        public DataTable Get_UserGuide_Module_Action_description(Int32 moduledetailid)
        {
            DataTable dt = null;
            try
            {
                DataSet ds = Service.DbFunctions.SelectCommand(" Select * from userguideactiondescription where moduledetailid='" + moduledetailid + "'");
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // functions for Binding UserGuide on display Page End here
        #endregion

        #region Country Flag
        // To get Country Flag
        public string Get_Flag_By_Country_name(string Countryname)
        {
            try
            {
                StringBuilder strFlag = new StringBuilder();
                strFlag.Append(" Select  fieldvalue from content_management_detail where cmsid IN( ");
                strFlag.Append("  Select cmsid from content_management_master where pagename like '%Area Master%') ");
                strFlag.Append("  and (fieldtext='Country Name' and fieldvalue like '%" + Countryname + "%') ");
                strFlag.Append("  OR (fieldtext='Country Flag' and fieldvalue like '%" + Countryname + "%') ");
                strFlag.Append("  ORDER BY dorder desc limit 1 ");
                return Convert.ToString(Service.DbFunctions.SelectCountString(strFlag.ToString()));
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Application User

        // Fetch ApplicationUser By applicationuserid
        public Cls_ApplicationUser Fetch_ApplicationUser_Byid(Int32 applicationuserid)
        {
            Cls_ApplicationUser obj = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select * from applicationusers where applicationuserid='" + applicationuserid + "'");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        obj = new Cls_ApplicationUser();
                        obj.applicationuserid = Convert.ToInt32(dt.Rows[0]["applicationuserid"]);
                        obj.name = Convert.ToString(dt.Rows[0]["name"]);
                        obj.username = Convert.ToString(dt.Rows[0]["username"]);
                        obj.email = Convert.ToString(dt.Rows[0]["email"]);
                        obj.password = Convert.ToString(dt.Rows[0]["password"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                obj.isactive = true;
                            }
                            else
                            {
                                obj.isactive = false;
                            }
                        }
                        else
                        {
                            obj.isactive = false;
                        }
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Insert New applicationusers
        public Int32 Insert_ApplicationUsers(Cls_ApplicationUser cls)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Insert into applicationusers(username,password,name,email,isactive,createdby,creationdate) ");
                strmodules.Append(" Values(" );
                strmodules.Append(" '");
                strmodules.Append(cls.username.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(cls.password.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(cls.name.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(cls.email);
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(cls.isactive);
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" '");
                strmodules.Append(cls.createdby);
                strmodules.Append("'");
                strmodules.Append(",");
                strmodules.Append(" LOCALTIMESTAMP");
                strmodules.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //To Update Existing applicationusers
        public Int32 Update_ApplicationUsers(Cls_ApplicationUser cls)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Update applicationusers ");
                strmodules.Append(" Set ");
                strmodules.Append(" name=");
                strmodules.Append(" '");
                strmodules.Append(cls.name.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" email=");
                strmodules.Append(" '");
                strmodules.Append(cls.email.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" username=");
                strmodules.Append(" '");
                strmodules.Append(cls.username.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" password=");
                strmodules.Append(" '");
                strmodules.Append(cls.password.Replace(" '", "''"));
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" isactive=");
                strmodules.Append(" '");
                strmodules.Append(cls.isactive);
                strmodules.Append(" '");
                strmodules.Append(" ,");
                strmodules.Append(" lastmodifiedby=");
                strmodules.Append(" '");
                strmodules.Append(cls.lastmodifiedby);
                strmodules.Append("'");
                strmodules.Append(" ,");
                strmodules.Append(" lastmodificationdate=");
                strmodules.Append(" LOCALTIMESTAMP ");
                strmodules.Append(" Where ");
                strmodules.Append(" applicationuserid=");
                strmodules.Append(cls.applicationuserid);
                retVal = Service.DbFunctions.UpdateCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        // Fetch applicationusers
        public DataTable Fetch_ApplicationUsers()
        {
            DataTable dt = null;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Select * from applicationusers");
                DataSet ds = Service.DbFunctions.SelectCommand(strmodules.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Delete applicationusers
        public Int32 Delete_ApplicationUsers(Int32 applicationuserid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strmodules = new StringBuilder();
                strmodules.Append(" Delete from applicationusers ");
                strmodules.Append(" Where ");
                strmodules.Append(" applicationuserid=");
                strmodules.Append(applicationuserid);
                retVal = Service.DbFunctions.DeleteCommand(strmodules.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }
        #endregion
    } // CLass Ends here
}

