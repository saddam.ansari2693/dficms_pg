﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Web.UI;
using System.Web;


namespace DocCMS.Core.Implementation
{
    public class ClsimageResize
    {
        // Resize the image size
        public static Image resizeimage(int newwidth, int newheight, string stPhotoPath)
        {
            Image imgPhoto = Image.FromFile(stPhotoPath);
            int sourcewidth = imgPhoto.Width;
            int sourceheight = imgPhoto.Height;
            //==========================================//
            //=== FOR THE PURPOSE OF THE "N" height ====//
            if (newheight == 0)
            {
                newheight = imgPhoto.Height;
            }
            //==========================================//
            //======== Consider vertical pics ==========//
            if (sourcewidth < sourceheight)
            {
                int buff = newwidth;

                newwidth = newheight;
                newheight = buff;
            }
            int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
            float nPercent = 0, nPercentW = 0, nPercentH = 0;
            nPercentW = ((float)newwidth / (float)sourcewidth);
            nPercentH = ((float)newheight / (float)sourceheight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((newwidth -
                          (sourcewidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((newheight -
                          (sourceheight * nPercent)) / 2);
            }
            int destwidth = (int)(sourcewidth * nPercent);
            int destheight = (int)(sourceheight * nPercent);
            Bitmap bmPhoto = new Bitmap(newwidth, newheight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,imgPhoto.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Transparent);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destwidth, destheight),
                new Rectangle(sourceX, sourceY, sourcewidth, sourceheight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static Image resizeimages(int newwidth, int newheight, string stPhotoPath)
        {

            Image imgToResize = Image.FromFile(stPhotoPath);
            //==========================================//
            //=== FOR THE PURPOSE OF THE "N" height ====//
            if (newheight == 0)
            {
                newheight = imgToResize.Height;
            }
            //==========================================//
            int sourcewidth = imgToResize.Width;
            int sourceheight = imgToResize.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            nPercentW = ((float)newwidth / (float)sourcewidth);
            nPercentH = ((float)newheight / (float)sourceheight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destwidth = (int)(sourcewidth * nPercent);
            int destheight = (int)(sourceheight * nPercent);
            Bitmap b = new Bitmap(destwidth, destheight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(imgToResize, 0, 0, destwidth, destheight);
            g.Dispose();
            return (Image)b;
        }

        public static Image Scaleimage(int maxwidth, int maxheight, string stPhotoPath)
        {
            Image image = Image.FromFile(stPhotoPath);
            if (maxheight == 0)
            {
                maxheight = image.Height;
            }
            var ratioX = (double)maxwidth / image.Width;
            var ratioY = (double)maxheight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);
            var newwidth = (int)(image.Width * ratio);
            var newheight = (int)(image.Height * ratio);
            var newimage = new Bitmap(newwidth, newheight);
            Graphics.FromImage(newimage).DrawImage(image, 0, 0, newwidth, newheight);
            return newimage;
        }

        public static Image resizeimages_Banner(int newwidth, int newheight, string stPhotoPath)
        {

            Image imgToResize = Image.FromFile(stPhotoPath);
            //==========================================//
            //=== FOR THE PURPOSE OF THE "N" height ====//
            if (newheight == 0)
            {
                newheight = imgToResize.Height;
            }
            //==========================================//
            int sourcewidth = imgToResize.Width;
            int sourceheight = imgToResize.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            nPercentW = ((float)newwidth / (float)sourcewidth);
            nPercentH = ((float)newheight / (float)sourceheight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destwidth = (int)(sourcewidth * nPercent);
            int destheight = (int)(sourceheight * nPercent);
            Bitmap b = new Bitmap(destwidth, destheight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(imgToResize, 0, 0, destwidth, destheight);
            g.Dispose();
            return (Image)b;
        }
    }//=== CLASS ENDS HERE
}
