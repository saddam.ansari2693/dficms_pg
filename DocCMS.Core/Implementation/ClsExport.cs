﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocCMS.Core.Interface;
using System.Web;
using System.Data;

namespace DocCMS.Core.Implementation
{
    public partial class DocCMSServices : IDocCMSServices
    {
        // To export into excel 
        public void ExporttoExcel(DataTable table, string filename, string Col1, string Col2, string Col3, string Col4, string Col5, string Col6)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename.Replace(" ", string.Empty));
            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:9.0pt; font-family:Calibri;'>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table bgcolor='#ffffff' " +
              "bordercolor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:12.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers

            if (!(string.IsNullOrEmpty(Col1)))
            {
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(Col1);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            if (!(string.IsNullOrEmpty(Col2)))
            {
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(Col2);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            if (!(string.IsNullOrEmpty(Col3)))
            {
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(Col3);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            if (!(string.IsNullOrEmpty(Col4)))
            {
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(Col4);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            if (!(string.IsNullOrEmpty(Col5)))
            {
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(Col5);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            if (!(string.IsNullOrEmpty(Col6)))
            {
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(Col6);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {

                    if (i == 0)
                    {
                        HttpContext.Current.Response.Write("<Td style='height:32px;width:500px' align='center' valign='top'>");
                    }
                    else if (i == 2)
                    {
                        HttpContext.Current.Response.Write("<Td style='height:32px;width:210px' align='center' valign='top'>");
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<Td style='height:32px' align='center' valign='top'>");
                    }
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }
                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
        // 
        public void ExporttoExcel(DataTable table, string filename, List<string> ColArray)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename.Replace(" ", string.Empty));
            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:9.0pt; font-family:Calibri;'>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table bgcolor='#ffffff' " +
              "bordercolor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:12.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            if (ColArray != null)
            {
                for (Int32 i = 0; i < ColArray.Count; i++)
                {
                    if (!string.IsNullOrEmpty(ColArray[i]))
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        HttpContext.Current.Response.Write(ColArray[i]);
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");
                    }
                }
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {

                    if (i == 0)
                    {
                        HttpContext.Current.Response.Write("<Td style='height:32px;width:500px' align='left' valign='top'>");
                    }
                    else if (i == 2)
                    {
                        HttpContext.Current.Response.Write("<Td style='height:32px;width:210px' align='left' valign='top'>");
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<Td style='height:32px' align='left' valign='top'>");
                    }
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }
                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public void ExportToCSVFile(DataTable dtTable, string RType, string csvType)
        {
            StringBuilder sbldr = new StringBuilder();
            if (dtTable.Columns.Count != 0)
            {
                if (RType != "Summary")
                {
                    foreach (DataColumn col in dtTable.Columns)
                    {
                        sbldr.Append(col.ColumnName + csvType);
                    }
                    sbldr.Append("\r\n");
                }
                foreach (DataRow row in dtTable.Rows)
                {

                    foreach (DataColumn column in dtTable.Columns)
                    {
                        sbldr.Append(row[column].ToString() + csvType);
                    }
                    sbldr.Append("\r\n");
                }
            }
            string attachment = "";
            if (RType == "Summary")
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=JobApplication_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=JobApplication_SpaceSeperated.csv";
                }
            }
            else
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=JobApplication_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=JobApplication_SpaceSeperated.csv";
                }
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");
            HttpContext.Current.Response.Write(sbldr.ToString());
            HttpContext.Current.Response.End();
        }

        public void ExportEnquiryToCSVFile(DataTable dtTable, string RType, string csvType, string PrefixText)
        {
            StringBuilder sbldr = new StringBuilder();
            if (dtTable.Columns.Count != 0)
            {
                if (RType != "Summary")
                {
                    foreach (DataColumn col in dtTable.Columns)
                    {
                        sbldr.Append(col.ColumnName + csvType);
                    }
                    sbldr.Append("\r\n");
                }
                foreach (DataRow row in dtTable.Rows)
                {
                    foreach (DataColumn column in dtTable.Columns)
                    {
                        sbldr.Append(row[column].ToString() + csvType);
                    }
                    sbldr.Append("\r\n");
                }
            }
            string attachment = "";
            if (RType == "Summary")
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=JobApplication_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=JobApplication_SpaceSeperated.csv";
                }
            }
            else
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=JobApplication_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=JobApplication_SpaceSeperated.csv";
                }
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");
            HttpContext.Current.Response.Write(sbldr.ToString());
            HttpContext.Current.Response.End();
        }

        //Order Details
        public void ExportToCSVFile(DataTable dtTable, string RType, string csvType, string filename)
        {
            StringBuilder sbldr = new StringBuilder();
            if (dtTable.Columns.Count != 0)
            {
                if (RType != "Summary")
                {
                    foreach (DataColumn col in dtTable.Columns)
                    {
                        sbldr.Append(col.ColumnName + csvType);
                    }
                    sbldr.Append("\r\n");
                }
                foreach (DataRow row in dtTable.Rows)
                {
                    foreach (DataColumn column in dtTable.Columns)
                    {
                        sbldr.Append(row[column].ToString() + csvType);
                    }
                    sbldr.Append("\r\n");
                }
            }
            string attachment = "";
            if (RType == "Summary")
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=" + filename + "_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=" + filename + "_SpaceSeperated.csv";
                }
            }
            else
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=" + filename + "_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=" + filename + "_SpaceSeperated.csv";
                }
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");
            HttpContext.Current.Response.Write(sbldr.ToString());
            HttpContext.Current.Response.End();
        }

        public void ExportToCSVFileCustomerRegistration(DataTable dtTable, string RType, string csvType, string PrefixText)
        {
            StringBuilder sbldr = new StringBuilder();
            if (dtTable.Columns.Count != 0)
            {
                if (RType != "Summary")
                {
                    foreach (DataColumn col in dtTable.Columns)
                    {
                        sbldr.Append(col.ColumnName + csvType);
                    }
                    sbldr.Append("\r\n");
                }
                foreach (DataRow row in dtTable.Rows)
                {

                    foreach (DataColumn column in dtTable.Columns)
                    {
                        sbldr.Append(row[column].ToString() + csvType);
                    }
                    sbldr.Append("\r\n");
                }
            }
            string attachment = "";
            if (RType == "Summary")
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=" + PrefixText + "_CustomerRegistration_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=" + PrefixText + "_CustomerRegistration_SpaceSeperated.csv";
                }
            }
            else
            {
                if (csvType == ",")
                {
                    attachment = "attachment; filename=" + PrefixText + "_CustomerRegistration_CommaSeperated.csv";
                }
                else
                {
                    attachment = "attachment; filename=" + PrefixText + "_CustomerRegistration_SpaceSeperated.csv";
                }
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");
            HttpContext.Current.Response.Write(sbldr.ToString());
            HttpContext.Current.Response.End();
        }
    }// Class Ends here
}
