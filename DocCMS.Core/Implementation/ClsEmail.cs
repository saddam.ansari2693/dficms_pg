﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using DocCMS.Core.Interface;
using DocCMS.Core.DataTypes;
using System.Net;
using System.Web;

namespace DocCMS.Core.Implementation
{
    public partial class DocCMSServices : IDocCMSServices
    {
        // To Send email
        public bool Send_Mail(MailMessage mailContent, Cls_SMTP objSmtp)
        {
            bool retval = true;
            try
            {
                string password = ServicesFactory.DocCMSServices.Decrypt(Convert.ToString(objSmtp.senderpassword));
                mailContent.Priority = MailPriority.Normal;
                SmtpClient client = new SmtpClient();
                client.EnableSsl = objSmtp.smtpsslserver;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Host = objSmtp.smtphostserver;
                client.Port = Convert.ToInt32(objSmtp.smtpport);
                client.ServicePoint.MaxIdleTime = 5;
                client.Timeout = 50000;
                //Setup credentials to login to our sender email address ("username", "password")
                NetworkCredential credentials = new NetworkCredential(objSmtp.senderemailid, password);
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;
                //Send the msg
                client.Send(mailContent);
                retval = true;
            }
            catch (Exception ex)
            {
                retval = false;
                throw ex;
            }
            return retval;
        }

        public string Get_Domain_name()
        {
            string host = HttpContext.Current.Request.Url.GetComponents(UriComponents.HostAndPort, UriFormat.Unescaped);
            if (host.StartsWith("www."))
                return host.Substring(4);
            else
                return host;
        }
    }// Class Ends here
}
