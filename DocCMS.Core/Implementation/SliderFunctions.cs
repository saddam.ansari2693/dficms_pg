﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocCMS.Core.Interface;
using System.Data;
using DocCMS.Core.Factory;
using DocCMS.Core.DataTypes;

namespace DocCMS.Core.Implementation
{
    public partial class DocCMSServices : IDocCMSServices
    {
        #region slider Mangement

        /****************************************Start Master Slider Function**/
        //Insert Data into Slider Master Table
        public Int32 Insert_Slider_Master(ClsSliderMaster objClsSliderMaster)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strInsertSliderMaster = new StringBuilder();
                strInsertSliderMaster.Append(" Insert into slidermaster(slidername,sliderposition,height,width,imagewidth,imageheight,buttontextSection,isactive,createdby,creationdate) values(" );
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(objClsSliderMaster.slidername.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" ','");
                strInsertSliderMaster.Append(objClsSliderMaster.sliderposition.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" ','");
                strInsertSliderMaster.Append(objClsSliderMaster.height.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" ','");
                strInsertSliderMaster.Append(objClsSliderMaster.width.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" ','");
                strInsertSliderMaster.Append(objClsSliderMaster.imagewidth.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" ','");
                strInsertSliderMaster.Append(objClsSliderMaster.imageheight.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(" ,");
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(objClsSliderMaster.buttontextSection.Trim().Replace(" '", "''"));
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(" ,");
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(objClsSliderMaster.isactive);
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(" ,");
                strInsertSliderMaster.Append(" '");
                strInsertSliderMaster.Append(objClsSliderMaster.createdby);
                strInsertSliderMaster.Append("'");
                strInsertSliderMaster.Append(" ,");
                strInsertSliderMaster.Append(" LOCALTIMESTAMP");
                strInsertSliderMaster.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strInsertSliderMaster.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Slider Master Table Data
        public DataTable Fetch_Slider_Master_Data()
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderMaster = new StringBuilder();
                strSliderMaster.Append(" Select sliderid,slidername,height,width,imagewidth,imageheight,buttontextSection,isactive,createdby,creationdate,lastmodifiedby,lastmodificationdate from slidermaster");
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderMaster.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Slider Master Table Data Using by Slider id
        public DataTable Fetch_Slider_Master_Data_By_sliderid(int sliderid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderMaster = new StringBuilder();
                strSliderMaster.Append(" Select sliderid,sliderposition,slidername,height,width,imagewidth,imageheight,buttontextSection,isactive,fullwidth from slidermaster where sliderid=" + sliderid);
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderMaster.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //To Delete slider Master by Slider id
        public Int32 Delete_Slider_Master_Data_By_sliderid(int sliderid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSliderMasterDataDelete = new StringBuilder();
                strSliderMasterDataDelete.Append(" Delete from slidermaster ");
                strSliderMasterDataDelete.Append(" Where ");
                strSliderMasterDataDelete.Append(" sliderid=");
                strSliderMasterDataDelete.Append(sliderid);
                retVal = Service.DbFunctions.DeleteCommand(strSliderMasterDataDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        // Update slider Master 
        public Int32 Update_Slider_Master_Data(ClsSliderMaster objClsSliderMaster)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update slidermaster ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" slidername=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.slidername.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" sliderposition=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.sliderposition.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" height=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.height.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" width=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.width.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" imageheight=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.imageheight.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" buttontextSection=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.buttontextSection.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" imagewidth=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.imagewidth.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderMaster.isactive);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" lastmodifiedby=");
                strUpdate.Append("'");
                strUpdate.Append(objClsSliderMaster.lastmodifiedby);
                strUpdate.Append("',");
                strUpdate.Append(" lastmodificationdate=LOCALTIMESTAMP");
                strUpdate.Append("  where ");
                strUpdate.Append(" sliderid=");
                strUpdate.Append(objClsSliderMaster.sliderid);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Get Fetch Slider name By sliderid
        public DataTable Fetch_Slider_name_By_sliderid(int sliderid)
        {
            DataTable dt = new DataTable();
            string slidername = "";
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" select sliderid,imagewidth,sliderposition,imageheight,slidername from slidermaster where sliderid=" + sliderid);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Fetch Slider Details for Frontend
        public DataTable Fetch_Slider_size_Details(int sliderid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" select sliderid,slidername,fullwidth,sliderposition,height,width,imagewidth,imageheight,isactive from slidermaster where sliderid=" + sliderid);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        // get amx display order for Slider Special buttons
        public Int32 Get_Max_SpecialButton_dorder(Int32 sliderid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select ISNULL(Count(id),0) as Maxdorder from sliderspecialbuttons where sliderid=" + sliderid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //To Insert Special button Details
        public Int32 Insert_Slider_Special_Button_Detail(Cls_SliderSpecialButtons objCls_SliderSpecialButtons)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" Insert into sliderspecialbuttons(sliderid,buttontext,navigationurl,buttoncolor,isactive,createdby,creationdate) ");
                strInsert.Append(" Values(" );
                strInsert.Append(objCls_SliderSpecialButtons.sliderid);
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objCls_SliderSpecialButtons.buttontext.Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objCls_SliderSpecialButtons.navigationurl.Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objCls_SliderSpecialButtons.buttoncolor.Trim().Replace(" '", "''"));
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objCls_SliderSpecialButtons.isactive);
                strInsert.Append(" '");
                strInsert.Append(" ,");
                strInsert.Append(" '");
                strInsert.Append(objCls_SliderSpecialButtons.createdby);
                strInsert.Append("'");
                strInsert.Append(" ,");
                strInsert.Append(" Getdate()");
                strInsert.Append(" )");
                retVal = Service.DbFunctions.InsertCommand(strInsert.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Update Special button Details
        public Int32 Update_Slider_Special_Button_Detail(Cls_SliderSpecialButtons objCls_SliderSpecialButtons)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update sliderspecialbuttons ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" sliderid=");
                strUpdate.Append(" '");
                strUpdate.Append(objCls_SliderSpecialButtons.sliderid);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" buttontext=");
                strUpdate.Append(" '");
                strUpdate.Append(objCls_SliderSpecialButtons.buttontext.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" navigationurl=");
                strUpdate.Append(" '");
                strUpdate.Append(objCls_SliderSpecialButtons.navigationurl.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" buttoncolor=");
                strUpdate.Append(" '");
                strUpdate.Append(objCls_SliderSpecialButtons.buttoncolor.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objCls_SliderSpecialButtons.isactive);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" updateby=");
                strUpdate.Append(" '");
                strUpdate.Append(objCls_SliderSpecialButtons.updateby.Trim().Replace(" '", "''"));
                strUpdate.Append("'");
                strUpdate.Append(" , ");
                strUpdate.Append(" updationdate=");
                strUpdate.Append(" Getdate()");
                strUpdate.Append("  where");
                strUpdate.Append("  id=");
                strUpdate.Append(objCls_SliderSpecialButtons.id);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //To Delete Slider Special Button
        public Int32 Delete_Slider_Special_Button_by_id(Int32 id)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strDelete = new StringBuilder();
                strDelete.Append(" Delete from sliderspecialbuttons ");
                strDelete.Append(" Where ");
                strDelete.Append(" id=");
                strDelete.Append(id);
                retVal = Service.DbFunctions.DeleteCommand(strDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }

        //Check Special Button name
        public bool Check_Slider_Special_Button_name(Int32 sliderid, string buttontext)
        {
            bool retval = false;
            try
            {
                StringBuilder strQuery = new StringBuilder();
                strQuery.Append(" Select count(*) from sliderspecialbuttons where sliderid=" + sliderid + " and buttontext='" + buttontext + "'");
                Int32 objvalue = Service.DbFunctions.SelectCount(strQuery.ToString());
                if (objvalue != null && objvalue != 0)
                {
                    retval = true;
                }
                return retval;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //Get Fetch Slider Special Button
        public DataTable Fetch_Slider_Special_Buttons(Int32 sliderid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" Select id,sliderid,buttontext,navigationurl,buttoncolor,isactive from sliderspecialbuttons where sliderid=" + sliderid);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Fetch Slider Special Button Using id
        public DataTable Fetch_Slider_Special_Buttons_By_id(Int32 id)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" Select id,sliderid,buttontext,navigationurl,buttoncolor,isactive from sliderspecialbuttons where id=" + id);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //To Insert text Contain
        public Int32 Insert_Special_Contain(Cls_SliderContent objSliderContent)
        {
            Int32 retVal = 0;
            try
            {
                bool IsCheckContainText = false;
                IsCheckContainText = Check_Slider_Contain_Text(objSliderContent.sliderid);
                if (IsCheckContainText == false)
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" Insert into slidercontent(sliderid,textcontent,isactive,createdby,creationdate) ");
                    strInsert.Append(" Values(" );
                    strInsert.Append(objSliderContent.sliderid);
                    strInsert.Append(" ,");
                    strInsert.Append(" '");
                    strInsert.Append(objSliderContent.textcontent.Replace(" '", "''"));
                    strInsert.Append(" '");
                    strInsert.Append(" ,'");
                    strInsert.Append(" True");
                    strInsert.Append(" '");
                    strInsert.Append(" ,");
                    strInsert.Append(" '");
                    strInsert.Append(objSliderContent.createdby);
                    strInsert.Append(" '");
                    strInsert.Append(" ,");
                    strInsert.Append(" Getdate()");
                    strInsert.Append(" )");
                    retVal = Service.DbFunctions.InsertCommand(strInsert.ToString());
                    return retVal;
                }
                else
                {
                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" Update slidercontent ");
                    strUpdate.Append(" Set ");
                    strUpdate.Append(" sliderid=");
                    strUpdate.Append(" '");
                    strUpdate.Append(objSliderContent.sliderid);
                    strUpdate.Append(" '");
                    strUpdate.Append(" , ");
                    strUpdate.Append(" textcontent=");
                    strUpdate.Append(" '");
                    strUpdate.Append(objSliderContent.textcontent.Trim().Replace(" '", "''"));
                    strUpdate.Append(" '");
                    strUpdate.Append(" , ");
                    strUpdate.Append(" isactive=");
                    strUpdate.Append(" '");
                    strUpdate.Append(" True");
                    strUpdate.Append(" '");
                    strUpdate.Append(" , ");
                    strUpdate.Append(" updatedby=");
                    strUpdate.Append(" '");
                    strUpdate.Append(objSliderContent.updatedby.Trim().Replace(" '", "''"));
                    strUpdate.Append("'");
                    strUpdate.Append(" , ");
                    strUpdate.Append(" updationdate=");
                    strUpdate.Append(" Getdate()");
                    strUpdate.Append("  where");
                    strUpdate.Append("  sliderid=");
                    strUpdate.Append(objSliderContent.sliderid);
                    retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //To Update text Contain
        public Int32 Update_Special_Contain(Cls_SliderContent objSliderContent)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update slidercontent ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" sliderid=");
                strUpdate.Append(" '");
                strUpdate.Append(objSliderContent.sliderid);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" textcontent=");
                strUpdate.Append(" '");
                strUpdate.Append(objSliderContent.textcontent.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(" True");
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" updateby=");
                strUpdate.Append(" '");
                strUpdate.Append(objSliderContent.updatedby.Trim().Replace(" '", "''"));
                strUpdate.Append("'");
                strUpdate.Append(" , ");
                strUpdate.Append(" updationdate=");
                strUpdate.Append(" Getdate()");
                strUpdate.Append("  where");
                strUpdate.Append("  id=");
                strUpdate.Append(objSliderContent.id);
                retVal = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Check slider Content Text
        public bool Check_Slider_Contain_Text(Int32 sliderid)
        {
            bool retval = false;
            try
            {
                StringBuilder strQuery = new StringBuilder();
                strQuery.Append(" Select count(*) from slidercontent where sliderid=" + sliderid + "");
                Int32 objvalue = Service.DbFunctions.SelectCount(strQuery.ToString());
                if (objvalue >= 0 && objvalue != 0)
                {
                    retval = true;
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Fetch Slider Contain Text
        public DataTable Fetch_Slider_Contain_Text_By_sliderid(Int32 sliderid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" Select sliderid,textcontent from slidercontent where sliderid=" + sliderid);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //Get Fetch Slider Contain Text
        public string Fetch_Slider_Contain_Text_By_sliderid_Show(Int32 sliderid)
        {
            string RetString = "";
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                strsliderid.Append(" Select textcontent from slidercontent where sliderid=" + sliderid + " and isactive='True'");
                RetString = Service.DbFunctions.SelectString(strsliderid.ToString());
                if (RetString != null && RetString != "")
                {
                    return RetString;
                }
                return RetString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Fetch Slider Button
        public DataTable Fetch_Slider_SpecialButton_By_sliderid_Show(Int32 sliderid)
        {
            DataTable dtSliderButton = null;
            try
            {
                StringBuilder strQuery = new StringBuilder();
                strQuery.Append(" Select id,sliderid,buttontext,navigationurl,buttoncolor,isactive from sliderspecialbuttons where sliderid=" + sliderid + "and isactive='True'");
                DataSet dssliderButton = Service.DbFunctions.SelectCommand(strQuery.ToString());
                if (dssliderButton != null && dssliderButton.Tables[0].Rows.Count > 0)
                {
                    dtSliderButton = dssliderButton.Tables[0];
                }
                return dtSliderButton;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /****************************************End Master Slider Function**/
        /****************************************Start Slider Item Master Function**/

        //Insert Data into Slider Master Table
        public Int32 Insert_SliderItem_Master_Item(ClsSliderItemMaster objClsSliderItemMaster)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strInsertSliderItemMaster = new StringBuilder();
                strInsertSliderItemMaster.Append(" Insert into slideritemmaster(sliderid,imagename,dorder,isactive) values(" );
                strInsertSliderItemMaster.Append(objClsSliderItemMaster.sliderid);
                strInsertSliderItemMaster.Append(" ,'");
                strInsertSliderItemMaster.Append(objClsSliderItemMaster.imagename.Trim().Replace(" '", "''"));
                strInsertSliderItemMaster.Append(" ',");
                strInsertSliderItemMaster.Append(objClsSliderItemMaster.dorder);
                strInsertSliderItemMaster.Append(" ,'");
                strInsertSliderItemMaster.Append(objClsSliderItemMaster.isactive);
                strInsertSliderItemMaster.Append(" '");
                strInsertSliderItemMaster.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strInsertSliderItemMaster.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Fetch Slider image Using sliderid For Bind SliderItemDashboard
        public DataTable Fetch_SliderItem_Master_image(int sliderid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderItemMaster = new StringBuilder();
                strSliderItemMaster.Append("  Select slideritemmaster.slideritemid,slideritemmaster.sliderid,slideritemmaster.imagename as imagename");
                strSliderItemMaster.Append("  ,slideritemmaster.dorder,slideritemmaster.isactive,slidermaster.slidername  ");
                strSliderItemMaster.Append("  from slideritemmaster ");
                strSliderItemMaster.Append("  inner join slidermaster on slideritemmaster.sliderid=slidermaster.sliderid where  slideritemmaster.sliderid=" + sliderid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderItemMaster.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Fetch Using SliderItem For Edit
        public DataTable Fetch_SliderItem_For_Edit(int slideritemid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderItemMaster = new StringBuilder();
                strSliderItemMaster.Append(" Select slideritemmaster.slideritemid,slideritemmaster.sliderid,slideritemmaster.imagename as imagename");
                strSliderItemMaster.Append(" ,slideritemmaster.dorder,slideritemmaster.isactive,slidermaster.slidername");
                strSliderItemMaster.Append("  from slideritemmaster inner join slidermaster on slideritemmaster.sliderid=slidermaster.sliderid where slideritemmaster.slideritemid=" + slideritemid);
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderItemMaster.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch slideritemmaster order by display order
        public DataTable Fetch_SliderItemMaster_Order_By_dorder(int sliderid)
        {
            DataTable dt = null;
            try
            {
                StringBuilder strFetchData = new StringBuilder();
                strFetchData.Append(" select slidermaster.slidername,slideritemmaster.slideritemid,slideritemmaster.sliderid,slideritemmaster.imagename as imagename");
                strFetchData.Append(" ,slideritemmaster.dorder,slideritemmaster.isactive from slidermaster  inner join slideritemmaster  ");
                strFetchData.Append(" on slidermaster.sliderid=slideritemmaster.sliderid where slidermaster.sliderid=" + sliderid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchData.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dtBlog = ds.Tables[0];
                    dt = SortTable(dtBlog, "dorder");
                }
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
        }

        // Update slider Item Master 
        public Int32 Update_Slider_Item_Master_Data(ClsSliderItemMaster objClsSliderItemMaster)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update slideritemmaster ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" imagename=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderItemMaster.imagename.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" dorder=");
                strUpdate.Append(objClsSliderItemMaster.dorder);
                strUpdate.Append(" , ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderItemMaster.isactive);
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" slideritemid=");
                strUpdate.Append(objClsSliderItemMaster.slideritemid);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //To Delete image slides
        public Int32 Delete_SliderItemMaster_Data_By_slideritemid(int slideritemid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSliderMasterItemDataDelete = new StringBuilder();
                strSliderMasterItemDataDelete.Append(" Delete from slideritemmaster ");
                strSliderMasterItemDataDelete.Append(" Where ");
                strSliderMasterItemDataDelete.Append(" slideritemid=");
                strSliderMasterItemDataDelete.Append(slideritemid);
                retVal = Service.DbFunctions.DeleteCommand(strSliderMasterItemDataDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        //Get Slider Item Display order
        public Int32 Get_Max_SliderItem_displayorder(int sliderid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select ISNULL(MAX(dorder),0) as Maxdorder from slideritemmaster where sliderid=" + sliderid + "");
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //--Fetch slideritemmaster For Display Order--------//
        public ClsSliderItemMaster Fetch_SliderItemMaster_By_slideritemid(Int32 slideritemid)
        {
            ClsSliderItemMaster objNGO = null;
            try
            {
                StringBuilder strFetchDataByid = new StringBuilder();
                strFetchDataByid.Append(" Select * from slideritemmaster where slideritemid=" + slideritemid + "");
                DataSet ds = Service.DbFunctions.SelectCommand(strFetchDataByid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ClsSliderItemMaster objClsSliderItemMaster = new ClsSliderItemMaster();
                        objClsSliderItemMaster.slideritemid = Convert.ToInt32(dt.Rows[0]["slideritemid"]);
                        objClsSliderItemMaster.dorder = Convert.ToInt32(dt.Rows[0]["dorder"]);
                        objClsSliderItemMaster.sliderid = Convert.ToInt32(dt.Rows[0]["sliderid"]);
                        objClsSliderItemMaster.imagename = Convert.ToString(dt.Rows[0]["imagename"]);
                        string activeStatus = Convert.ToString(Convert.ToString(dt.Rows[0]["isactive"]));
                        if (!string.IsNullOrEmpty(activeStatus))
                        {
                            if (activeStatus.ToUpper() == "TRUE")
                            {
                                objNGO.isactive = "True";
                            }
                            else
                            {
                                objNGO.isactive = "False";
                            }
                        }
                        else
                        {
                            objNGO.isactive = "False";
                        }
                    }
                }
                return objNGO;
            }
            catch (Exception ex)
            { 
                throw ex; 
            }
        }



        // Update Display order of slideritemmaster
        public Int32 Update_SliderItemMaster_dorder(List<ClsSliderItemMaster> LstobjClsSliderItemMaster)
        {
            Int32 retVal = 0;
            try
            {
                if (LstobjClsSliderItemMaster != null && LstobjClsSliderItemMaster.Count > 0)
                {
                    for (Int32 i = 0; i < LstobjClsSliderItemMaster.Count; i++)
                    {

                        StringBuilder strSliderItemMaster = new StringBuilder();
                        strSliderItemMaster.Append(" Update slideritemmaster ");
                        strSliderItemMaster.Append(" Set ");
                        strSliderItemMaster.Append(" dorder=");
                        strSliderItemMaster.Append(LstobjClsSliderItemMaster[i].dorder);
                        strSliderItemMaster.Append(" Where ");
                        strSliderItemMaster.Append(" slideritemid=");
                        strSliderItemMaster.Append(LstobjClsSliderItemMaster[i].slideritemid);
                        retVal = Service.DbFunctions.UpdateCommand(strSliderItemMaster.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /****************************************End Slider Item Master Function**/
        /****************************************Start Slider Item Details Function**/
        public Int32 Insert_Slider_Details(ClsSliderDetails objClsSliderDetails)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strInsertSliderItemsDetails = new StringBuilder();
                strInsertSliderItemsDetails.Append(" insert into slideritemdetails (slideritemid,itemtype,itemcontent,contentposition,fontsize,fontcolor,");
                strInsertSliderItemsDetails.Append(" dorder,isactive,isbold,isitalic,isunderline,isemphasized,entrystyle,entrydelay,imagename,margintop,marginbottom,marginright,marginleft,textbackgroundcolor,navigationurl,dataeffect) values(" );
                strInsertSliderItemsDetails.Append(objClsSliderDetails.slideritemid);
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.itemtype.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.itemcontent.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.contentposition.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.fontsize.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.fontcolor.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.dorder);
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.isactive.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.isbold.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.isitalic.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.isunderline.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.isemphasized.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.entrystyle.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.entrydelay.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.image.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.margintop.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.marginbottom.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.marginleft.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.marginright.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.textbackgroundcolor.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.navigationurl.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" ,");
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(objClsSliderDetails.dataeffect.Trim().Replace(" '", "''"));
                strInsertSliderItemsDetails.Append(" '");
                strInsertSliderItemsDetails.Append(" )");
                retVal = Service.DbFunctions.InsertCommandReturnidentity(strInsertSliderItemsDetails.ToString());
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Slider Details Table Data Using by Slider id
        public DataTable Fetch_Slider_Details_Data_By_sliderid(int slideritemid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderItemDetails = new StringBuilder();
                strSliderItemDetails.Append(" select slidermaster.sliderid,slidermaster.slidername,slideritemdetails.* from slidermaster inner join ");
                strSliderItemDetails.Append("  slideritemmaster on slidermaster.sliderid=slideritemmaster.sliderid inner join slideritemdetails on ");
                strSliderItemDetails.Append("  slideritemmaster.slideritemid=slideritemdetails.slideritemid where slideritemdetails.slideritemid=" + slideritemid);
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderItemDetails.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Fetch Slider name Using UPload image into Item Details
        public string Fetch_Slider_name_By_slideritemid(int slideritemid)
        {
            DataTable dt = new DataTable();
            string slidername = "";
            try
            {
                StringBuilder strsliderid = new StringBuilder();
                //strClass.Append(" Select fieldvalue id,fieldvalue Value from content_management_detail where cmsid IN(Select cmsid from content_management_master where pagename like '%" + pagename + "%' and dorder=1)");
                strsliderid.Append("    select slidername from slidermaster inner join slideritemmaster on slidermaster.sliderid=slideritemmaster.sliderid  where slideritemmaster.slideritemid=" + slideritemid);
                DataSet ds = Service.DbFunctions.SelectCommand(strsliderid.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        slidername = Convert.ToString(dt.Rows[0]["slidername"]);
                    }
                }
                return slidername;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //To Delete slider Details
        public Int32 Delete_Slider_Details_By_slideritemcontentid(int slideritemcontentid)
        {
            Int32 retVal = 0;
            try
            {
                StringBuilder strSliderMasterDataDelete = new StringBuilder();
                strSliderMasterDataDelete.Append(" Delete from slideritemdetails ");
                strSliderMasterDataDelete.Append(" Where ");
                strSliderMasterDataDelete.Append(" slideritemcontentid=");
                strSliderMasterDataDelete.Append(slideritemcontentid);
                retVal = Service.DbFunctions.DeleteCommand(strSliderMasterDataDelete.ToString());
                return retVal;
            }
            catch
            {
                return retVal;
            }
        }


        // Update slider Item Details
        public Int32 Update_Slider_Item_Details(ClsSliderDetails objClsSliderDetails)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strUpdate = new StringBuilder();
                strUpdate.Append(" Update slideritemdetails ");
                strUpdate.Append(" Set ");
                strUpdate.Append(" slideritemid=");
                strUpdate.Append(objClsSliderDetails.slideritemid);
                strUpdate.Append(" , ");
                strUpdate.Append(" itemtype=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.itemtype.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" imagename=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.image.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" itemcontent=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.itemcontent.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" contentposition=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.contentposition.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" fontsize=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.fontsize);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" fontcolor=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.fontcolor.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isbold=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.isbold);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isitalic=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.isitalic);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isunderline=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.isunderline);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isemphasized=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.isemphasized);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" entrystyle=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.entrystyle.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" entrydelay=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.entrydelay.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" margintop=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.margintop.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" marginbottom=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.marginbottom.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" marginleft=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.marginleft.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" marginright=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.marginright.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" textbackgroundcolor=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.textbackgroundcolor.Trim().Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" dorder=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.dorder);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" isactive=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.isactive);
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" navigationurl=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.navigationurl.Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append(" , ");
                strUpdate.Append(" dataeffect=");
                strUpdate.Append(" '");
                strUpdate.Append(objClsSliderDetails.dataeffect.Replace(" '", "''"));
                strUpdate.Append(" '");
                strUpdate.Append("  where ");
                strUpdate.Append(" slideritemcontentid=");
                strUpdate.Append(objClsSliderDetails.slideritemcontentid);
                retval = Service.DbFunctions.UpdateCommand(strUpdate.ToString());
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Slider Details  Using by slideritemcontentid
        public DataTable Fetch_Slider_Details_Data_By_slideritemcontentid(int slideritemcontentid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderItemDetails = new StringBuilder();
                strSliderItemDetails.Append(" Select slidermaster.sliderid, slidermaster.slidername,slideritemcontentid,slideritemdetails.slideritemid,itemtype,slideritemdetails.imagename,itemcontent,contentposition,navigationurl,dataeffect,");
                strSliderItemDetails.Append(" fontsize,fontcolor,isbold,isitalic,isunderline,isemphasized,entrystyle,entrydelay,margintop,marginbottom,marginleft,marginright,textbackgroundcolor,slideritemdetails.dorder,slideritemdetails.isactive from ");
                strSliderItemDetails.Append(" slideritemdetails INNER JOIN slideritemmaster ON slideritemmaster.slideritemid=slideritemdetails.slideritemid INNER JOIN slidermaster ON slidermaster.sliderid=slideritemmaster.sliderid ");
                strSliderItemDetails.Append(" where slideritemcontentid=" + slideritemcontentid);
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderItemDetails.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /**************************display order**************************/
        //Get Slider Details  Using by slideritemcontentid
        public DataTable Fetch_Slider_Details_For_Display_Order(int slideritemid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderItemDetails = new StringBuilder();
                strSliderItemDetails.Append(" Select slidermaster.sliderid, slidermaster.slidername,slideritemcontentid,slideritemdetails.slideritemid,itemtype,slideritemdetails.imagename,itemcontent,contentposition,");
                strSliderItemDetails.Append(" fontsize,fontcolor,isbold,isitalic,isunderline,isemphasized,entrystyle,entrydelay,margintop,marginbottom,marginleft,marginright,slideritemdetails.dorder,slideritemdetails.isactive from ");
                strSliderItemDetails.Append(" slideritemdetails INNER JOIN slideritemmaster ON slideritemmaster.slideritemid=slideritemdetails.slideritemid INNER JOIN slidermaster ON slidermaster.sliderid=slideritemmaster.sliderid ");
                strSliderItemDetails.Append(" where slideritemdetails.slideritemid=" + slideritemid);
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderItemDetails.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get slideritemdetails Display order
        public Int32 Get_Max_SliderItemDetails_displayorder(int slideritemid)
        {
            Int32 retval = 0;
            try
            {
                StringBuilder strMax = new StringBuilder();
                strMax.Append(" Select ISNULL(MAX(dorder),0) as Maxdorder from slideritemdetails where slideritemid=" + slideritemid);
                object objMax = Service.DbFunctions.SelectCount(strMax.ToString());
                if (objMax != null)
                {
                    retval = Convert.ToInt32(objMax);
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update Display order of slideritemcontentid
        public Int32 Update_SliderItemMaster_dorder(List<ClsSliderDetails> LstobjClsSliderDetails)
        {
            Int32 retVal = 0;
            try
            {
                if (LstobjClsSliderDetails != null && LstobjClsSliderDetails.Count > 0)
                {
                    for (Int32 i = 0; i < LstobjClsSliderDetails.Count; i++)
                    {

                        StringBuilder strSliderItemMaster = new StringBuilder();
                        strSliderItemMaster.Append(" Update slideritemdetails ");
                        strSliderItemMaster.Append(" Set ");
                        strSliderItemMaster.Append(" dorder=");
                        strSliderItemMaster.Append(LstobjClsSliderDetails[i].dorder);
                        strSliderItemMaster.Append(" Where ");
                        strSliderItemMaster.Append(" slideritemcontentid=");
                        strSliderItemMaster.Append(LstobjClsSliderDetails[i].slideritemcontentid);
                        retVal = Service.DbFunctions.UpdateCommand(strSliderItemMaster.ToString());

                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**************************display order**************************/

        /****************************************End Slider Item Details Function**/



        /************************************************Start Slider Details show On Home Page**** ***************************************/
        //Get Top slider id for Homepage
        public int Fetch_Slider_sliderid_ForHomePage()
        {
            DataTable dt = new DataTable();
            int retval = 0;
            try
            {
                StringBuilder strSliderimage = new StringBuilder();
                strSliderimage.Append(" select sliderid from slidermaster order by sliderid asc limit 1");
                retval = Service.DbFunctions.SelectCount(strSliderimage.ToString());
                if (retval != null)
                {
                    return retval;
                }
                else
                {
                    return retval;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Slider image
        public DataTable Fetch_Slider_image(int sliderid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderimage = new StringBuilder();
                strSliderimage.Append(" select slidermaster.sliderid,slidermaster.buttontextSection,slidermaster.slidername,slidermaster.height,slidermaster.width,slidermaster.isactive,slidermaster.createdby,slidermaster.creationdate");
                strSliderimage.Append(" ,slideritemmaster.slideritemid,slideritemmaster.dorder,slideritemmaster.imagename as imagename from slidermaster inner join slideritemmaster on ");
                strSliderimage.Append(" slidermaster.sliderid=slideritemmaster.sliderid  where slidermaster.sliderid=" + sliderid + " and slidermaster.isactive='True' and slideritemmaster.isactive='True' order by slideritemmaster.dorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderimage.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Slider Items image
        public DataTable Fetch_Slider_Item_Details_images(int slideritemid)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder strSliderItemDetails = new StringBuilder();
                strSliderItemDetails.Append(" select * from slideritemdetails where slideritemid=" + slideritemid + " and isactive='True' order by dorder asc");
                DataSet ds = Service.DbFunctions.SelectCommand(strSliderItemDetails.ToString());
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /************************************************End Slider Details show On Home Page*******************************************/

        //Get sliderid from pagesliders Using by pagename and cmsid
        public Int32 Fetch_Slider_id_By_cmsid_pageid(string pagename, Int64 cmsid)
        {
            try
            {
                StringBuilder strFetchsliderid = new StringBuilder();
                strFetchsliderid.Append(" Select distinct sliderid from pagesliders where pageid='" + pagename + "' and cmsid='" + cmsid + "'");
                return Convert.ToInt32(Service.DbFunctions.SelectCount(strFetchsliderid.ToString()));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get sliderid from PageSlider Using by pageid
        public Int32 Fetch_Slider_id_By_pageid(string pagename, Int32 cmsid)
        {
            try
            {
                StringBuilder strFetchsliderid = new StringBuilder();
                strFetchsliderid.Append(" Select sliderid from pagesliders where pageid='" + pagename + "' and cmsid=" + cmsid + "");
                return Convert.ToInt32(Service.DbFunctions.SelectCount(strFetchsliderid.ToString()));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
