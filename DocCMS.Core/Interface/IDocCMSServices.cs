﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DocCMS.Core.DataTypes;
using System.Net.Mail;
using RMA.Core.DataTypes;

namespace DocCMS.Core.Interface
{
    public interface IDocCMSServices
    {
        #region Common Function
        string Get_Single_Value_String(string Colname, string tablename, string conCol, string conval, string paramtype);
        #endregion

        #region Function for user profiles
        string Fetch_Roles_name(Int32 roleid);
        //FETCH THE DATA
        DataTable Fetch_All_Users();
        DataTable Fetch_Roles(Int32 roleid);
        DataTable Fetch_All_Users_Except_SuperAdmin();
        DataTable Fetch_User_Profile_Details(string emailid, string password);
        Cls_UserProfiles Fetch_User_Details_Byid(string emailid);
        //FETCH SubModule for login redirect
        DataTable Get_SubModule_From_Login(string navigationurl);
        bool Check_User_Login_Details(string emailid, string password);
        // check for active user role
        bool Check_For_Active_UserRole(string emailid, string password);

        bool User_Available(string emailid);
        //CHECK emailid FOR change password
        bool Check_User_By_emailid(string emailid);


        //Change User password
        Int32 Change_User_password_ById(Guid userid, string password);
        //DELETE DATA
        void Delete_User_Profile(string emailid);

        //INSERT DATA
        Int32 Insert_User_Profile(Cls_UserProfiles userprofile);

        //UPDATE DATA
        Int32 Update_User_Profile(Cls_UserProfiles userprofile);
                
        // Compare old password with new password
        bool Compare_password_with_Old_password(string emailid, string password);
        // Fetch Top 1 Sub Module Navigation URL by role id to redirect from login page
        DataTable Fetch_SubModule_URL_By_Role_Id(Int32 roleid);
       
        #endregion

        #region Cryptography Functions
        String Encrypt(String strDecryptedString);
        String Decrypt(String strEncryptedString);
        String EncryptWithKey(String strDecryptedString, String strKey);
        String DecryptWithKey(String strEncryptedString, String strKey);
        #endregion encryption password

        #region User Roles Functions


        // fetch the Role Privilege
        DataTable Fetch_RolePrivilege(Int32 roleid);
        // To Fetch_Submodule by moduleid
        DataTable Fetch_SubModules(Int32 moduleid);
        // Fetch The User Roles
        DataTable Fetch_User_Roles(Int32 parentid);
        // Fetch The User Roles by id
        Cls_UserRoles Fetch_UserRole_ById(Int32 roleid);
        // To Insert User Roles
        Int32 Insert_User_Roles(Cls_UserRoles userRole);
        // To Update User Roles
        Int32 Update_UserRoles(Cls_UserRoles userRole);
        // To Update User Roles Display Order
        Int32 Update_Role_Dorder(List<Cls_UserRoles> objRole);
        // To Delete User Roles By roleid
        Int32 Delete_userroles(Int32 roleid);
        //Check for Existing Role name
        Int32 Check_Existing_Role(string Rolename);
        // Reordering functions block for UserRole Display Order
        void Reorder_UserRoles(Int32 roleid, Int32 CurrentOrder, string OpType);
        // To get Maximum Display Order for UserRoles
        Int32 Get_Max_User_Role_Dorder();
        // To get Minimum Display Order for UserRoles
        Int32 Get_Min_User_Role_Dorder();
        // To Adjust User Role Display Order
        void Adjust_User_Role_Dorder();
        // End Reordering functions block for User Roles Display Order


        #endregion

        #region modules Functions
        //Fetch modules
        DataTable Fetch_Modules();
        // Fetch modules By Module id
        Clsmodules Fetch_Modules_ById(Int32 moduleid);
        //Check for Existing Module name
        Int32 Check_Existing_Module(string modulename);
        //To Insert New Module
        Int32 Insert_Modules(Clsmodules modules);
        //To Update Existing Module
        Int32 Update_Modules(Clsmodules modules);
        //To Update Existing Module Display order
        Int32 Update_Module_Dorder(List<Clsmodules> objmodules);
        //To Delete Module
        Int32 Delete_Modules(Int32 moduleid);
        // Reordering functions block for modules Display Order
        void Reorder_Modules(Int32 moduleid, Int32 CurrentOrder, string OpType);
        // To Get Maximum Display Order for Module
        Int32 Get_Max_Modules_Dorder();
        // To Get Minimum Display Order for Module
        Int32 Get_Min_Modules_Dorder();
        // Adjust Module Display Order 
        void Adjust_Modules_Dorder();
        // End Reordering functions block for modules Display Order
        #endregion

        #region submodules Functions

        // Fetch All submodules
        DataTable Fetch_SubModules();
        // Fetch submodules by Module id
        DataTable Fetch_SubModules_ByModuleId(Int32 moduleid);
        // Fetch SubModule By SubModule id
        Clssubmodules Fetch_SubModules_ById(Int32 Submoduleid);
        // Fetch All moduleid and modulename For DropDown od Module name
        DataTable Fetch_ModulesName();
        //Check for Existing Sub Module name
        Int32 Check_Existing_SubModule(string submodulename);
        // To Insert New submodules
        Int32 Insert_SubModules(Clssubmodules submodules);
        // To Update Existing SubModule
        Int32 Update_SubModules(Clssubmodules submodules);
        // To Update Existing SubModule Display order
        Int32 Update_SubModule_Dorder(List<Clssubmodules> objsubmodules);
        // To Delete SubModule By Submoduleid
        Int32 Delete_SubModules(Int32 Submoduleid);
        //Reordering functions block for submodules Display Order
        void Reorder_SubModules(Int32 Submoduleid, Int32 moduleid, Int32 CurrentOrder, string OpType);
        // To Get Max Sub Module By moduleid for Display Order
        Int32 Get_Max_SubModules_Dorder(Int32 moduleid);
        // To Get Minimum Sub Module  By moduleid  for Display Order
        Int32 Get_Min_SubModules_Dorder(Int32 moduleid);
        // Adjust SubModule  By moduleid  Display order
        void Adjust_Submodules_Dorder(Int32 moduleid);
        // End Reordering functions block for submodules Display Order
        #endregion

        #region privileges Functions
        DataTable Fetch_Selected_Roles(Int32 roleid);
        DataTable Get_Roles_Privileges_Based_on_SubModuleId(Int32 submoduleid, Int32 roleid);
        DataTable Fetch_privileges_By_RoleId_For_Other_Admin(Int32 roleid, Int32 moduleid);
        // Fetch  All privileges
        DataTable Fetch_privileges();
        // Fetch privileges By privilgeid
        Clsprivileges Fetch_privileges_Byid(Int32 privilgeid);
        // To Insert new Privilege
        Int32 Insert_privileges(Clsprivileges privileges);
        // To Update Existing Privilege
        Int32 Update_privileges(Clsprivileges privileges);
        // to Delete Privilege by Privilege id
        Int32 Delete_privileges(Int32 privilgeid);
        // Reordering functions block for User Priviledges Display Order
        void Reorder_privileges(Int32 privilgeid, Int32 CurrentOrder, string OpType);
        //Get Max Privilege For Display order
        Int32 Get_Max_privileges_dorder();
        //Get Minimum Privilege For Display order
        Int32 Get_Min_privileges_dorder();
        // Adjust Privilege Display order
        void Adjust_privileges_dorder();
        #endregion

        #region Assigned privileges Functions

        //to Fetch the Assigned privileges 
        DataTable Fetch_privileges_By_roleid(Int32 roleid, Int32 moduleid);
        // To Fetch the Module List and privileges for the privileges Grid
        DataTable Fetch_Modules_For_Grid(Int32 roleid);
        // Fetch active Module By roleid
        Int32 Fetch_Checked_modules_By_roleid(Int32 roleid);
        // Count If All modules isactive, IsView,IsAdd,isdelete Checked or Not
        Int32 Fetch_CheckedAll_modules_By_roleid(Int32 roleid);
        // TO Insert Roles & privileges
        string Insert_Roles_privileges(string roleid, string PrivList);

        #endregion

        #region Content Pages Functions

        //Fetch Content Page Details
        DataTable Fetch_ContentPage();
        //Fetch for FrontEndContentPage Details
        DataTable Fetch_Content_For_FrontEndContentPage();
        // Fetch  Page name whose Status is active
        DataTable Fetch_Page_name();
        //isactive Update
        Int32 Update_isactive_ContentPage_By_pageid(Int32 pageid, bool isactive);
        // Fetch Content Page Master Detail By Page id
        DataTable Fetch_Page_Master_Data_By_pageid(string pageid);
        // Fetch Content Page Detail Detail By Page id
        DataTable Fetch_Page_Detail_Data_By_pageid(string pageid);
        //Check Existing Page name
        Int32 Check_Existing_pagename(string pagename);
        //Insert new  Content Page Master
        Int32 Insert_ContentPage(Cls_ContentPages contentPage);
        // To Insert Content Detail Page
        Int32 Insert_ContentPage_Detail(Cls_ContentPage_Detail contentPage);
        //Update Existing Content Page By pageid
        Int32 Update_Content_Page_Byid(string pageid);
        // Delete Content Page By pageid
        Int32 Delete_Content_Page_Byid(string pageid);
        //TO CHANGE Set as default 
        Int32 Update_Default_Check(Int32 pageid);
        #endregion

        #region Content management Page Function
        
        // get Discounted amount by Purchase Order id
        string FetchOrderNumer();
        // Get the pageid by pagename
        string Fetch_Page_id_By_name(string pagename);
        // Fetch Page name By pageid
        string Fetch_pagename_By_pageid(Int32 pageid);
        // Fetch Page name By pageid
        DataTable Get_cmsid_by_Projectcategoryname(Int32 pageid, string Projectcategory);
        // Check if the page is drafted or not
        bool Is_Content_Page_Drafted(Int32 pageid, Int32 cmsid);
        // To Fill Data into the Content management Page
        string Fetch_ContentValue_By_cmsid(Int32 cmsid, Int32 dorder);
        // TO Fetch the Content Title
        string Fetch_Content_Title(Int32 cmsid);
        // TO Fetch the Content Title Special Page
        string Fetch_Content_Title_SpecialPage(Int32 cmsid);
         // To Fill Data into the Content management Page for checked value
        DataTable Fetch_ContentValue_Checked_By_cmsid(Int32 cmsid, Int32 dorder);
        //Fetch Page Detail By CSMid
        DataTable Fetch_Page_Detail_ByContentid(Int32 CSMid);
        // To get the details for a control
        DataTable Fetch_Page_Data_By_fieldtype(string pageid, string fieldtype, string dorder);
        //Fetch Dropdown itemdata from Table
        DataTable Fetch_ItemValue_ForDropDown(string pagename);
        //Fetch Dropdown itemdata from Table for Catalouge
        DataTable Fetch_ItemValue_ForDropDown_by_fieldtext(string pagename, string fieldtext);
        //Fetch Dropdown itemdata from Table for special page
         DataTable Fetch_ItemValue_ForDropDown_forSpecialPage(string pagename);
        // Get max content dorder by menu name for level 2
        Int32 Get_Max_Content_dorder_ByMenuname_Level2(Int32 pageid, string MainMenuname, string SubMenuname);
        // To Bind Content management Dashboard
        DataTable Fetch_CMS_Dashboard(Int32 pageid);
        // Fetch Catelouge CMS Detail
        DataTable Fetch_Catalouge_Dashboard(Int32 pageid);
        // get Content Modified by roleid
        string Get_Content_modifiedby_pageidAndcmsid(Int32 pageid, Int32 cmsid);
        // To Bind Content management Dashboard
        DataTable Fetch_CMS_Dashboard_Fordisplayorder(Int32 pageid);
        // Count Max Display order for CMS
        Int32 Get_Max_Content_dorder(Int32 pageid);
        // Get Max content dorder by menu name
        Int32 Get_Max_Content_dorder_ByMenuname(Int32 pageid, string Menuname);
         // To Bind Content management Dashboard
        DataTable Fetch_CMS_Dashboard_Fordisplayorder(Int32 pageid, string Fieldname);
        // To Insert Page Content
        string Insert_CMS_Data(List<Cls_ContentManagement> objCMS, string userid);
         // Insert CMS Data draft
        string Insert_CMS_Data_Draft(List<Cls_ContentManagement> objCMS, string userid);
        // Remove Draft Content Data
        string Remove_CMS_Data_Draft(Int32 pageid, Int32 cmsid);
        //check for duplicate sub menu level 1 during save
        bool Duplicate_Sub_Menu_Level_1_Check(List<Cls_ContentManagement> lstCMS, string fieldtext, string fieldvalue);
        //check for duplicate sub menu level 1 during update
        bool Duplicate_Sub_Menu_Level_1_Check_Update(List<Cls_ContentManagement> lstCMS, string fieldtext, string fieldvalue);
        //To Update Page Content
        string Update_CMS_Data(List<Cls_ContentManagement> objCMS, string userid, Int32 cmsid);
        // To Update Content Management Page Display Order
        Int32 Update_Content_displayorder(List<Cls_ContentManagement> objContent);
        //To Delete Page Content By cmsid
        Int32 Delete_CMS_Data(Int32 cmsid);
        //check existing menu name for content page in case of update
        bool Check_Existing_Menu_For_Update_Bycmsid(string Menuname, Int32 cmsid, Int32 pageid);
         // Check existing Prduct Detail category by category name
        bool Check_Existing_ProductcategoryDetail_Update_Bycmsid(string categoryname, Int32 cmsid, Int32 pageid);
           //check existing existing Prduct Detail category by category name in case of save
        bool Check_Existing_ProductcategoryDetail_For_Save(string categoryname, Int32 pageid);
        //check existing menu name for content page in case of save
        bool Check_Existing_Menu_For_Save(string Menuname, Int32 pageid);
        // fetch item value for dropdown sub menu level 1
        DataTable Fetch_ItemValue_ForDropDown_SubMenu_Leve1(string pagename, string MainMenuname);
        // fetch menu name by page name
        DataTable Fetch_Main_Menu_Dashboard_Bypagename_Menuname(Int32 pageid, string Menuname);
        // To Fetch content active status
        DataTable Fetch_displayorder_Bycmsid(Int32 pageid, Int32 cmsid);
         // To Fetch content active status for draft
        DataTable Fetch_displayorder_Bycmsid_forDraft(Int32 pageid, Int32 cmsid);
        
        #endregion

        #region TinyMCE Function
        // To Fetch All images for image Upload 
        DataTable Fetch_All_images();
        #endregion

        #region Media management functions
        //========FETCH MEDIA FILES
        DataTable Fetch_All_MediaFiles();
        // Fetch Media File by id
        DataTable Fetch_Media_Files_By_id(string id);
        DataTable Fetch_Media_Files_By_name(string fname, Int32 id);
        //======= IN CASE OF EDIT FILE
        DataTable Fetch_Media_Files_By_name_and_id(string fname, Int32 id, Int32 filecategoryid);
        // To Verify the Deletion of the file
        bool Verify_Deletion(int id, string type);
        //====== FOR THE PURPOSE OF FETCH DATA FROM FILEcategory TABLE
        DataTable Fetch_File_category_All();
        //====== FOR THE PURPOSE OF FETCH DATA FROM AUDIO AND VidEO TABLE
        DataTable Fetch_Audio_Video_Filecategory_All();
        //=======BIND DATA IN MEDIA DASHBOARD
        DataTable Bind_Media_Dashboard(string query);
        //========INSERT NEW MEDIA FILES
        Int32 Insert_MediaFiles(Clsfileuploader fileUploader);
        // to Update Media Files
        Int32 Update_MediaFiles(Clsfileuploader fileUploader);
        // To delete media Files
        void Delete_MediaFile(Clsfileuploader fileUploader);
        #endregion

        #region Left Side Bar functions
        //======FOR THE PURPOSE TO GET MODULES DATA BASED ON USER ROLEid
        DataTable Fetch_LeftSide_modules_By_roleid(Int32 roleid);

        //======FOR THE PURPOSE TO GET SUBMODULES DATA BASED ON USER ROLEid AND MODULEid
        DataTable Fetch_LeftSide_submodules_By_roleid(Int32 roleid, Int32 moduleid);

        //======FOR THE PURPOSE TO GET USER ROLE id BASED ON USER id
        Int32 Fetch_roleid_By_userid(string userid);

        //======FOR THE PURPOSE TO GET MODULE id BASED ON SUBMODULE id
        DataTable Fetch_moduleId_By_SubModuleId(Int32 Submoduleid);
        #endregion

        #region company Functions

        // Fetch All company Detail
        DataTable Fetch_company();
        // Fetch Compnay detail By company id
        Cls_company Fetch_company_Byid(Int32 companyid);
        //Check for Existing company name
        Int32 Check_Existing_company(string companyname);
        //To Insert company
        Int32 Insert_company(Cls_company company);
        // To Update Existing company by company id
        Int32 Update_company(Cls_company company);
        // To delete company By company id
        Int32 Delete_company(Int32 companyid);
        #endregion

        #region SMTP Functions
         //Fetch SMTP detial by department name
        Cls_SMTP Fetch_SMTP_Detail_By_department(string department);
        // Fetch SMTP Detail
        Cls_SMTP Fetch_SMTP();
        //fetch smtp email detail by id
        Cls_SMTP Fetch_SMTP_email_Detail_By_id(Int32 id);
        //To Insert SMTP
        Int32 Insert_SMTP(Cls_SMTP smtp);
        // Delete SMPTP before Inserting any new smptp
        Int32 Delete_SMTP(Int32 companyid);
        // To Update SMTP
        Int32 Update_SMTP(Cls_SMTP clsSMTP);
        // get amx display order for SMTP email
        Int32 Get_Max_SMTP_email_dorder(Int32 smtpid);
        //To Insert SMTP email detail
        Int32 Insert_SMTP_email_Detail(Cls_SMTP smtp);
        //FEtch SMTP email Detail
        DataTable Fetch_SMTP_email_Detail(Int32 smtpid);
        //Check for SMTP department
        Int32 Check_Existing_department(string departmentname, Int32 smtpid);
        // To Update SMTP detail by id
        Int32 Update_SMTP_email_Detail(Cls_SMTP clsSMTP);
        Int32 Check_Existing_department_For_Update(string departmentname, Int32 smtpid, Int32 id);
        #endregion

        #region Export Functionality
        // To export into excel 
        void ExporttoExcel(System.Data.DataTable dt, string filename, string Col1, string Col2, string Col3, string Col4, string Col5, string Col6);
        // To Export Excel using dynamic column length
        void ExporttoExcel(DataTable table, string filename, List<string> ColArray);
        void ExportToCSVFile(DataTable dtTable, string RType, string csvType);
        void ExportEnquiryToCSVFile(DataTable dtTable, string RType, string csvType, string PrefixText);
        void ExportToCSVFile(DataTable dtTable, string RType, string csvType, string filename);
        void ExportToCSVFileCustomerRegistration(DataTable dtTable, string RType, string csvType, string PrefixText);
        #endregion


        #region Access Page function

        // TO FETCH PARTICULAR USER DETAILS BY GIVING emailid AND password
        DataTable Validate_Application_User(string username, string password);
        #endregion

        #region Plan Pages Functions
        //Fetch Plan Page Details
        DataTable Fetch_Plan();
        //Fetch Plan detail for Display order
        DataTable Fetch_Plan_For_displayorder();
        // Fetch  Plan name whose Status is active
        DataTable Fetch_Plan_name();
        // Fetch  Plan Master Detail By Plan id
        DataTable Fetch_Plan_Master_Data_By_planid(Int32 planid);
        // Fetch  Plan Master Detail By Plan id
        Cls_Plans Fetch_Plan_Master_Byid(Int32 planid);
        // Fetch Plan Detail Detail By Plan id
        DataTable Fetch_Plan_Detail_Data_By_planid(Int32 planid);
        // To Insert Plan function
        string Insert_Plan_Detail(List<Cls_Plans> objPlanList);
        //Update Existing Plan Page By planid
        string Update_Plan_Page_Byid(List<Cls_Plans> objPlanList);
        //To Update Existing Plan Display order
        Int32 Update_Plans_dorder(List<Cls_Plans> objPlanList);
        //Check for Existing Plan Page By Plan name
        Int32 Check_Existing_planname(string planname);
        // To Get Maximum Display Order for Plans
        Int32 Get_Max_Plans_dorder();
        // Delete Plans 
        Int32 Delete_Plans(Int32 planid);
        // To Get Maximum Display Order for Plans detail
        Int32 Get_FeatureCount(Int32 planid);
        #endregion

        #region Blogs Details Form
        //------Check Duplicate BlogTitle----------
        bool ChkDuplicate_BlogTitle(string name);
        //======Inser Blogs Details================================//
        Int32 Insert_BlogDetails(Cls_Blog ObjBlog);
        //======Adjust Blogs Details================================//
        void Adjust_Blogs_dorder();
        //--Fetch Blogs--BY id--------//
        Cls_Blog Fetch_Blog_By_id(Int32 blogid);
        // Blogs EXISTS OR NOT 
        bool ChkDuplicate_BlogTitleUpdate(string name, Int32 blogid);
        // TO Fetch Blog id By Blog name
        Int32 Fetch_blogid_Byname(string blogname);
        // Uodate Blogs Details --
        DataTable Fetch_All_Converted_VideosList(string memberid);
        Int32 Update_Blog_By_id(Cls_Blog objBlog);
        //======Fetch Blogs Details================================//
        DataTable Fetch_Blog_Details();
        //======Delete Blogs Details================================//
        Int32 Delete_Blog_By_id(Int32 blogid);
        // Update dorder Blog Details --
        void Reorder_Blog_Type(Int32 id, Int32 CurrentOrder, string OpType);
        // Get max dorder Blog Details --
        Int32 Get_Max_Blog_Type_dorder();
        //---------Get_Max_Blog_dorder-----------''
        Int32 Get_Max_Blogs_Type_dorder();
        //---------Get_Min_Blog_Type_dorder-----------''
        void Deleteimage(string imagename);
        Int32 Delete_Temp_Video(string videoname, string memberid);
        DataTable Fetch_All_Temp_VideosList(string memberid);
        Int32 Insert_comments(Cls_comment objcomment);
        DataTable Fetch_comment_Based_On_blogid(Int32 blogid);
        Int32 Insert_comments_image(Cls_comment objcomment);
        Int32 Get_Min_Blog_Type_dorder();
        //Count comment
        Int32 Count_comments(Int32 blogid);
        //Fetch converted Video
        DataTable GetConvertedVideo();
        // Upload comment Video 
        Int32 Insert_comments_Video(Cls_comment objcomment);
        //Truncate Tempvideo Table
        Int32 Truncate_TempVideo_ConvertedVideo();
        //Temp comment Video Converted comment Video
        Int32 Insert_Temp_comment_Video_Converted(Cls_TempcommentVideoConverted objTempcommentVideoConverted);
        //---------Update Blog Status-----------''
        Int32 Update_Blog_Status(Int32 blogid, bool Status);
        //======Fetch Blogs Details=for Front End===============================//
        DataTable Fetch_Blog_Details_FrontEnd(string accesstype);
        //======Fetch Blogs Details=for Front End=BY id==============================//
        DataTable Fetch_Blog_Details_FrontEnd_BYid(Int32 blogid, string accesstype);
        //======Fetch Blogs Details=for Front End=BY category id==============================//
        DataTable Fetch_Blog_Details_FrontEnd_BY_Blog_categoryid(Int32 blogcategoryid, string accesstype);
        // Select Top 3 Bog Detail for footer
        DataTable Fetch_Blog_Details_onFooter();
        #endregion

        #region Blogs category Details
        // Fetch All Blog category id and blogcategoryname For DropDown
        DataTable Fetch_Blog_category_name();
        // Fetch All Blog category id and blogcategoryname For DropDown order by dorder
        DataTable Fetch_Blog_category_name_By_dorder();
        // TO Fetch Blog categoryid By Blog category name
        Int32 Fetch_categoryid_Bycategoryname(string blogcategoryname);
        //Fetch Blog order by display order
        DataTable Fetch_Blog_Order_By_dorder();
        //======Check Duplicate Blogs category Details================================//
        bool ChkDuplicate_Blogcategory(string name);
        // Search for string based on category name 
        DataTable Search_Blog_Text(string SearchText);
        //======Inser Blogs Details================================//
        Int32 Insert_Blog_category(Cls_Blog_category ObjCat);
        //======Adjust Blogs Details================================//
        void Adjust_category_dorder();
        // Update Display order of Blog
        Int32 Update_Blog_dorder(List<Cls_Blog> objBlog);
          // TO Fetch Blog name By blogid
        string Fetch_blogname_Byid(string blogid);
        // Blog category During Update EXISTS OR NOT 
        bool ChkDuplicate_BlogcategoryUpdate(string blogcategoryname, Int32 blogcategoryid);
        // Update Blogs Details --
        Int32 Update_category_By_id(Cls_Blog_category objCat);
        //--Fetch category--BY id--------//
        Cls_Blog_category Fetch_category_By_id(Int32 blogcategoryid);
        //======Fetch Blogs Details================================//
        DataTable Fetch_category_Details();
        //Fetch Blog category order by display order
        DataTable Fetch_Blog_category();
        //======Delete category Details================================//
        Int32 Delete_category_By_id(Int32 blogcategoryid);
        // Update dorder Blog Details --
        void Reorder_category_Type(Int32 id, Int32 CurrentOrder, string OpType);
        // Uodate Blogs Status Details --
        Int32 Update_category_Status(Int32 blogcategoryid, bool Status);
        // Update Function for Display order
        Int32 Update_category_dorder(List<Cls_Blog_category> objcategory);
        // get max dorder of Blog category
        Int32 Get_Max_category_Type_dorder();
        #endregion

        #region Job Functions
        //Fetch cmsid of Job by Display Order
        DataTable Fetch_Job_cmsid_By_pageid(Int32 pageid);
        //Fetch skill by category name
        DataTable Fetch_skills_by_categoryname(Int32 pageid, string categoryname);
        // To Fetch Job Details By CMS id
        DataTable Fetch_Job_By_cmsid(Int32 cmsid);
        // To Fetch All Job Details By CMS id
        DataTable FetchAll_Job_By_cmsid(Int32 cmsid);
        //Insert New Job Application
        Int32 Insert_Job_Application(Cls_JobApplications clsjob);
        // gfetch job application for dashboard
        DataTable Fetch_Job_Application();
        // to delete Job Application by job application id
        Int32 Delete_Job_By_Applicationid(Int32 jobapplyid);
        #endregion

        #region Survey Function

        // Fetch Survey Master By Survey id
        ClsSurvey Fetch_Survey_Master_Byid(Int32 surveyid);
        //======Insert Survey master================================//
        Int32 Insert_Survey_Master(ClsSurvey objSurvey);
        //TO CHANGE Set as default 
        Int32 Update_Default_SurveyCheck();
        // get max display order
        Int32 Get_Max_Survey_Type_dorder();
        //Fetch survey detail for Survey Dashboard
        DataTable Fetch_All_Survey();
        DataTable Fetch_All_question_Dashboard(Int32 surveyid);
        //check Survey name
        bool ChkDuplicate_surveyname(string surveyname);
        //Fetch questiondetail for question Dashboard
        DataTable Fetch_All_question(Int32 surveyid);
         //Fetch questiondetail for question Dashboard for Report
         DataTable Fetch_All_question_For_Report(Int32 surveyid);
        // Insert question Detail for Survey
        Int32 Insert_Survey_question(ClsSurveyquestions objSurvey);
        // Check question name
        bool ChkDuplicate_question(string questionname, Int32 surveyid);
        // get max dorder for survey
        Int32 Get_Max_question_dorder(Int32 surveyid);
        // Fetch question Detail By Survey question id
        ClsSurveyquestions Fetch_question_Byid(Int32 surveyquestionid);
        //edit question detail
        Int32 Update_Survey_question(ClsSurveyquestions clsquestion);
        // Check duplicate answer for particular question
        bool ChkDuplicate_answer(string answer, Int32 surveyquestionid);
        // Insert answer Detail for Survey questions
        Int32 Insert_Survey_answer(ClsSurveyanswers objanswer);
        // get max display order for answer 
        Int32 Get_Max_answer_dorder(Int32 surveyquestionid);
        //Fetch answer Detail for answer Dashboard
        DataTable Fetch_All_answer_Byquestionid(Int32 surveyquestionid);
        //Fetch answer Detail for answer D
        DataTable Fetch_All_answer_Byquestionid_With_Response(Int32 surveyquestionid);
        // get survey question by id
        string Fetch_Survey_questionByid(Int32 surveyquestionid);
        // Fetch answer Detail By Survey answer id
        ClsSurveyanswers Fetch_answer_Byid(Int32 surveyanswerid);
        //Fetch answer Detail for answer Dashboard 
        DataTable Fetch_All_answer_Byquestionid_Dashboard(Int32 surveyquestionid);
        //edit answer detail
        Int32 Update_Survey_answer(ClsSurveyanswers clsanswer);
        //TO Update Survey Detail By_surveyid
        Int32 Update_Survey_Detail_By_surveyid(ClsSurvey clsSurvey);
        // Check Survey name for update
        bool ChkDuplicate_surveyname_ForUpdate(string surveyname, Int32 surveyid);
        // to delete Survey by surveyid id
        Int32 Delete_Survey(Int32 surveyid);
         // to delete Survey by surveyquestionid
        Int32 Delete_SurveyByquestionid(Int32 surveyquestionid);
        // to delete Survey  answer by surveyquestionid
        Int32 Delete_SurveyanswerByquestionid(Int32 surveyanswerid);
        // find default set Survey id
        Int32 Fetch_DefaultSet_Survey();
        // Check Show report active or not
        Int32 CheckshowreportStatus_Bysurveyid(Int32 surveyid);
        // Check question Show result status by question id
        Int32 CheckshowreportStatus_Forquestion_Byquestionid(Int32 surveyquestionid);
        // Insert Voting Detail for Survey questions
        string Insert_Survey_Vote(List<ClsSurveyResponse> objResponse);
        // Check response for  existing ip
        bool Check_Exist_Response(string ipaddress, Int32 surveyid, string sipaddress);
        #endregion

        #region Survey Reports
        //====================================================DETAILED SURVEY REPORT FUNCTION
        // To Bind the Survey Detail
        DataTable Fetch_Survey_Detail_By_id(string surveyid);
        // To Fetch Survey Response Year
        DataTable Fetch_Survey_Response_Year(string surveyid);
        // To Fetch Survey Response Year
        DataTable Fetch_Survey_Response_Month(string surveyid);
        // To Fetch the Survey questions       
        DataTable Fetch_Survey_questions(string surveyid);
        // To Fetch the Survey questions       
        DataTable Fetch_Survey_Allquestions(string surveyid);
        // To Fetch the Survey Resp
        DataTable Fetch_questions_Response(string surveyid, string questionType, string questionid, string fieldtype, string Year, string Month);
        // To Fetch the Fall year Summary and Chart data of the selected criteria 
        DataTable Get_Survey_Summary_Fall_Year(string surveyid, Int32 Year, Int32 Month);
        // To Fetch Survey Detail Dashboard
        DataTable Fetch_Detailed_Survey_Dashboard(string surveyid);
        // To Fetch Headeing of a survey response
        string Fetch_Response_By_questionheading(string responseid, string heading, string questionid);
        // To Fetch Headeing of a survey response
        string Fetch_Response_By_Allquestionheading(string heading, string questionid);
        // To Fetch Response question List in case of Multiple valued
        DataTable Fetch_Response_By_questionList(string surveyid, string questionid, string fieldtype, string responseid);
        // To Fetch Response question List in case of Multiple valued
        DataTable Fetch_Response_By_AllquestionList(string fieldtype, string questionid);
        // fetch answers based on questionid
        DataTable Fetch_Survey_answer_Byquestionid(string questionid, string surveyid);
        // to delete Survey answer by surveyid id
        Int32 Delete_Survey_answer_Byquestionid(Int32 surveyquestionid);
        // Function for Exporting All survey Reponse Details 20160203
        DataTable Get_All_Survey_Responses(Int32 surveyid);
        #endregion

        #region Event Function
        //Fetch cmsid of even by Display Order and active status
        DataTable Fetch_cmsid_For_Event(Int32 pageid);
        // To Fetch event By CMS id
        DataTable Fetch_For_Open_And_Close_Event_ByDate(Int32 pageid);
        // to fetch all event by cmsid
        DataTable Fetch_All_Event_Bycmsid(Int32 pageid);
        #endregion

        //Start of Front End Function
        #region Menus Functions
        // To Fetch cmsid for Header,Footer Menus By pageid order by Display order
        DataTable Fetch_Main_Menu_Dashboard_Bypagename(Int32 pageid);
        // To Fetch Menu Details By CMS id
        DataTable Fetch_Menu_Detail_By_cmsid(Int32 cmsid);
        // To Fetch Menu Details By CMS id 
        DataTable Fetch_Contentdisplayorder_By_cmsid(Int32 cmsid);
        // To Fetch Sub Menu Details By CMS id
        DataTable Fetch_SubMenu_Detail_By_cmsid(Int32 cmsid);
        // To Check Whether Menu Has Sub Menu Or Not
        Int32 Fetch_Submenucount_By_Menuname(string menuname);
        // To Check Whether Menu Has Sub Menu Or Not
        Int32 Fetch_SubMenuLevel2Count_By_Menuname(string menuname);
        // Fetch active SubModule and Display SubModule According to Display order in Ascending Order
        DataTable Fetch_SubMenuactiveStatus_By_Menuname(string menuname);
         // Fetch active Sub Menu Level2 and Display Sub Menu Level2 According to Display order in Ascending Order
        DataTable Fetch_SubMenuLevel2activeStatus_By_Menuname(string SubMenuname);
        //======FOR THE PURPOSE TO GET Menu name id BASED ON submenu name
        DataTable Fetch_Menuname_By_submodulename(Int32 Menuid);
        //Get News showing Position based on page id
        DataTable Fetch_News_Show_Position(Int32 pageid);
        // Fetch active Link Button order by display in asc
        DataTable Fetch_Link_Button_Detail(Int32 pageid);
        #endregion

        #region Footer menu and Header Functions
        // To Fetch Footer, header
        DataTable Fetch_Menu_Dashboard_Bypagename(Int32 pageid);
        DataTable Fetch_cmsid_For_ContentPage(Int32 pageid);
        DataTable Fetch_cmsid_For_ContentPage_Except_HeadOfficecmsid(Int32 pageid);
        // Fetch cmsid for active content page
        DataTable Fetch_cmsid_For_active_ContentPage(Int32 pageid);
        DataTable Fetch_cmsid_For_active_ContentPage_for_Published(Int32 pageid);
        Int32 Fetch_cmsid_For_Head_Office();
        DataTable Fetch_cmsid_For_active_ContentPage_Preview(Int32 pageid);
        DataTable Fetch_cmsid_For_active_ContentPage_Preview_FromDraft(Int32 pageid);
        #endregion

        #region Bind Page Content
        // To Fetch Page Content Details By CMS id
        DataTable Fetch_Page_Contents_By_cmsid(Int32 cmsid);
        // To Fetch Page Content Details draft By CMS id 
        DataTable Fetch_Page_Contents_By_cmsid_fromDraft(Int32 cmsid);
        #endregion

        #region Pages Detail Function
        // To Fetch cmsid for Products
        DataTable Fetch_cmsid_Bypageid(Int32 pageid);
        // To Fetch cmsid for Products by isactive
        DataTable Fetch_cmsid_Bypageid_isactive(Int32 pageid);
        // To Fetch Products Details By CMS id
        DataTable Fetch_Content_Data_By_cmsid(Int32 cmsid,string Alias);
        // To Fetch Products CMS id by Product name
        Int32 Fetch_Productsid_By_productname(string productname);
        #endregion

        #region Contact Us Functions
        // Fetch Enquiry Detail
        DataTable Fetch_All_Enquiry();
        //To Insert Enquiry
        Int32 Insert_Enquiry(Cls_ContactUs contact);
        #endregion
        // End Of Front End Function
        #region Mail Functionality
        // To Send Mail
        bool Send_Mail(MailMessage mailContent, Cls_SMTP objSmtp);
        // To Export Excel using dynamic column length
        string Get_Domain_name();
        #endregion


        #region Video
        //UPDATE Video name
        Int32 Update_Video_name(Int32 cmsid, Int32 displayorder, string filename);
        #endregion

        #region timeout functions
        //timeout insert
        Int32 Insert_timeout(Clstimeout Obj_Clstimeout);
        // Update timeout Details --
        Int32 Update_timeout(Clstimeout Obj_Clstimeout);
        // Fetch timeout
        DataTable Fetchtimeout();

        #endregion

        #region Chat manager
        //Update chat manager
        Int32 UpdateChatManager(ClsChatManager ObjClsChatManager);
        // Bind Chat Option Checkox;
        string BindChatStatus();

        #endregion


        //#region slider Mangement
        ////Insert Data into Slider Master Table
        //Int32 Insert_Slider_Master(ClsSliderMaster objClsSliderMaster);

        ////Get Slider Master Table Data
        //DataTable Fetch_Slider_Master_Data();

        ////Get Slider Master Table Data Using by Slider id
        //DataTable Fetch_Slider_Master_Data_By_sliderid(int sliderid);

        ////To Delete slider Master by Slider id
        //Int32 Delete_Slider_Master_Data_By_sliderid(int sliderid);

        //// Update slider Master 
        //Int32 Update_Slider_Master_Data(ClsSliderMaster objClsSliderMaster);

        ////Insert Data into Slider Details Table
        //Int32 Insert_Slider_Details(ClsSliderDetails objClsSliderDetails);


        ////Get Slider Details Table Data Using by Slider id
        //DataTable Fetch_Slider_Details_Data_By_sliderid(int sliderid);


        ////// Update slider Details 
        ////Int32 Update_Slider_Details_Data(Int32 sliderid, ClsSliderMaster objClsSliderMaster);

        ////Insert Data into Slider Master Table
        //Int32 Insert_SliderItem_Master_Item(ClsSliderItemMaster objClsSliderItemMaster);

        ////Get Fetch Slider name By sliderid
        //DataTable Fetch_Slider_name_By_sliderid(int sliderid);



        ////Get Fetch Slider image Using sliderid For Bind SliderItemDashboard
        //DataTable Fetch_SliderItem_Master_image(int sliderid);

        ////Get Fetch Using SliderItem For Edit
        //DataTable Fetch_SliderItem_For_Edit(int slideritemid);

        //// Update slider Item Master 
        //Int32 Update_Slider_Item_Master_Data(ClsSliderItemMaster objClsSliderItemMaster);

        ////Fetch SliderItemMaster order by display order
        //DataTable Fetch_SliderItemMaster_Order_By_dorder(int sliderid);

        //// Update Display order of SliderItemMaster
        //Int32 Update_SliderItemMaster_dorder(List<ClsSliderItemMaster> LstobjClsSliderItemMaster);

        ////To Delete image slides
        //Int32 Delete_SliderItemMaster_Data_By_slideritemid(int slideritemid);

        ////Get Slider Item Display order
        //Int32 Get_Max_SliderItem_displayorder(int sliderid);

        ////--Fetch SliderItemMaster For Display Order--------//
        //ClsSliderItemMaster Fetch_SliderItemMaster_By_slideritemid(Int32 slideritemid);


        ////Fetch Slider name Using UPload image into Item Details
        //Int32 Fetch_Slider_name_By_slideritemid(int slideritemid);


        ////To Delete slider Details
        //Int32 Delete_Slider_Details_By_slideritemcontentid(int slideritemcontentid);
        //// Update slider Item Details
        //Int32 Update_Slider_Item_Details(ClsSliderDetails objClsSliderDetails);





        ////Get Slider Details  Using by slideritemcontentid
        //DataTable Fetch_Slider_Details_Data_By_slideritemcontentid(int slideritemcontentid);


        ///**************************display order**************************/
        ////Get Slider Details  Using by slideritemcontentid
        //DataTable Fetch_Slider_Details_For_Display_Order(int slideritemid);

        ////Get SliderItemDetails Display order
        //Int32 Get_Max_SliderItemDetails_displayorder(int slideritemid);


        //// Update Display order of slideritemcontentid
        //Int32 Update_SliderItemMaster_dorder(List<ClsSliderDetails> LstobjClsSliderDetails);

        ///**************************display order**************************/


        ///************************************************Start Slider Details show On Home Page*******************************************/
        ////Get Slider image
        //DataTable Fetch_Slider_image(Int32 sliderid);

        ////Get Slider Items image
        //DataTable Fetch_Slider_Item_Details_images(int slideritemid);

        ///************************************************End Slider Details show On Home Page*******************************************/

        //#endregion

        #region UG_MainContent
        DataTable Fetch_UG_MainContent();
        Int32 Insert_UGMainContent(Cls_UG_MainContent objMainContent);
        Int32 Delete_UGMainContent_By_id(int maincontentid);
        Cls_UG_MainContent Fetch_UG_MainContent_Byid(Int32 maincontentid);
        Int32 Update_UGMainContent(Cls_UG_MainContent objMainContent);
        Int32 Get_Max_UG_MainContent_dorder();
        Int32 Update_UG_Content_dorder(List<Cls_UG_MainContent> objMainContent);
        Int32 Update_isactive(Int32 maincontentid, bool isactive);
        #endregion

        #region UG_SubContent
        DataTable Fetch_UG_SubContent(Int32 maincontentid);
        Int32 Insert_UG_SubContent(Cls_UG_SubContent objSubContent);
        Int32 Update_UG_SubContent(Cls_UG_SubContent objSubContent);
        Int32 Delete_UGSubContent_By_id(int subcontentid);
        DataTable Fetch_UG_SubContent_order();
        Int32 Get_Max_UG_SubContent_dorder(Int32 maincontentid);
        Cls_UG_SubContent Fetch_UG_SubContent_Byid(Int32 subcontentid);
        Int32 Update_isactive_Content(Int32 subcontentid, bool isactive);
        Int32 Update_UG_SubContent_dorder(List<Cls_UG_SubContent> objSubContent);
        #endregion

        #region UG-SubContent image
        DataTable Fetch_UG_SubContent_for_html(Int32 maincontentid);
        DataTable Fetch_SubContent_image(int subcontentid);
        Int32 Delete_image_Data_By_id(int id);
        Int32 Insert_UG_image_Item(Cls_UG_SubContentimages objClsimageItemMaster);
        DataTable Fetch_image_name_By_subcontentid(int subcontentid);
        Int32 Update_isactive_images(Int32 id, bool isactive);
        DataTable Fetch_image_For_Edit(Int32 id);
        Int32 Get_Max_UG_imageContent_dorder(Int32 subcontentid);
        Int32 Update_image_Item_Data(Cls_UG_SubContentimages objimage);
        Int32 Update_UG_Contentimage_dorder(List<Cls_UG_SubContentimages> objSubContent);
        #endregion

        #region for Dynamic Html
        DataTable Fetch_MainContent_For_pdf();
        DataTable Fetch_SubContent_For_pdf();
        DataTable Fetch_MainContent(Int32 roleid);
        #endregion


        #region Catalouge Functions
        // Fetch active category order by display in asc
        DataTable Fetch_Catalouge_category_cmsid(Int32 pageid);
        // Fetch Catlouge detail by cmsid
        DataTable Fetch_Catalouge_By_SubType_id(Int32 cmsid);
        //Fetch Catlouge cmsid By category name
        DataTable Fetch_Catlouge_cmsid_By_category_name(string categoryname, Int32 pageid, string accesstype);
        //Fetch Catlouge cmsid By category name from CMS
        DataTable Fetch_Catlouge_cmsid_By_category_name_FromCMS(string categoryname, Int32 pageid, string accesstype);
        // Fetch Catlouge detail by Serach Text
        DataTable Fetch_Catalouge_By_Search_Text(string SearchText, Int32 pageid);
        //Insert new  Cart master data
        Int32 Insert_Cart_Details(Cls_Order clsorder, string ShoppingCartid);
        //Insert new  Cart master data from CMS
        Int32 Insert_Cart_Details_fromCMS(Cls_Order clsorder);
        //Insert new  Cart master data from CMS Order Detail Dashboard page
        Int32 Insert_Cart_Details_fromCMS_OrderDashboard(Cls_Order clsorder);
        //Insert new  Cart master data from CMS and convert quote to order
        Int32 Insert_Cart_Details_fromCMS_and_Convert_Quote_to_Order(Cls_Order clsorder);
        // get Discounted amount by Purchase Order id
        string Get_discountedtotalprice(Int32 purchaseorderid);
        // Update cart master detail
        Int32 Update_Cart_Details_fromCMS(Cls_Order clsorder);
        // Insert cart detail
        Int32 Insert_Shopping_Cart_Details(Cls_OrderDetail clsorder);
        // Insert Custom Quote detail
        Int32 Insert_CustomQuote_Detail_ByOrder_Detailid(Cls_QuoteDetail clsorder);
         // Update Custom Quote detail
        Int32 Update_CustomQuote_Detail_ByOrder_Detailid(Cls_QuoteDetail clsorder);
         // Update Custom Order detail
        Int32 Update_CustomOrder_Detail_ByOrder_Detailid(Cls_OrderDetail clsorder);
         // Update Discounted Total and Grand Total to Order Master Table
        Int32 Update_discountedtotal_valueAnd_grandtotal(string discountedtotal, string grandtotal, Int32 purchaseorderid);
        // fetchall the order list from Order detail table by Purchase id From QuoteDetail
         DataTable Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(Int32 Purchaseid);
        //Insert new  shopping cart into temp table
        Int32 Insert_Shopping_Temp_Cart_Detail(Cls_ShopingCart clsorder);
        //Insert new  product detail to Order Detail
        Int32 Insert_New_Product_Detail_toOrderDetail(Cls_OrderDetail clsorder);
        //Insert new  product detail to Quote Detail
        Int32 Insert_New_Product_Detail_toQuoteDetail(Cls_OrderDetail clsorder,Int32 retval);
        //Check for shopping Cart
        Int32 CheckCount_For_Duplicate_ShoppingCart(Cls_ShopingCart clsorder);
         //Check for shopping Cart While Adding From_CMS
        Int32 CheckCount_For_Duplicate_Item_WhileAdding_From_CMS(Cls_OrderDetail clsorder);
        //Check for duplicate quotedetailid
        Int32 CheckCount_For_Duplicate_quotedetailid(Int32 quotedetailid);
        // fetch temp shopping detail
        DataTable Fetch_Shopping_Temp_Cart_Detail(string sessionid);
        // To Delete User Roles By roleid
        Int32 Delete_Temp_Shopping_Detail_By_ShoppingCartid(string ShoppingCartid);
        //Update shopping cart qty by its id
        string Update_Shopping_cart_By_id(Int32 id, Int32 qty, Decimal runningprice);
        //Delete shopping cart  by its id
        string Delete_Shopping_cart_By_id(Int32 id);
        // To Get Maximum Order No
        string Get_Max_Order_Number();
        //Check for active category
        bool Check_For_category_active(string SearchText, Int32 pageid);
        // fetchall the order list from master table
        DataTable Fetch_Order_List();
         // fetchall the order list from master table where Request type is Quote
        DataTable Fetch_Quote_List();
        // fetchall the order list from Order detail table by Purchase id
        DataTable Fetch_Order_Detail_List_By_purchaseorderid(Int32 Purchaseid);
        // fetchall the order list from Order detail table by order detail id
        DataTable Fetch_Order_Detail_By_orderdetailid(Int32 quotedetailid);
        // fetchall the order list from Order detail table by order detail id
         DataTable Fetch_PartDetail_ByOrderDetalid_From_OrderDetailTable(Int32 orderdetailid);
        // fetchall the comment by purchase order id
        DataTable Fetch_Quote_comment_Bypurchaseorderid(Int32 purchaseorderid);
         //Check for comment by  Purchase Order id
        Int32 CheckCount_For_comment_Bypurchaseorderid(Int32 purchaseorderid);
        // fetch all the comment
        DataTable Fetch_AllQuote_comment();
        //Insert Order description 
        Int32 Insert_Orderdescription(string Orderdescription, Int32 purchaseorderid);
        //To Update Order description
        Int32 Update_Orderdescription(string Orderdescription, Int32 purchaseorderid);
        // To Get Order Detail Dashboard
        string Get_Order_Detail_description(Int32 purchaseorderid);
        //Insert New Quote comments 
        Int32 Insert_Quotes(Cls_Quotes Quote);
        //To Update Existing Quote
        Int32 Update_Quotes(Cls_Quotes Quote);
        //To Delete Quote
        Int32 Delete_Quote(Int32 quoteid);
        // fetch discounted total price
         string Fetch_Discounted_Total(Int32 purchaseorderid);
        // fetch grand total from Quote Detail
        string Fetch_Grand_Total_from_QuoteDetail(Int32 purchaseorderid);
        // fetch grand total from Order Detail
        string Fetch_Grand_Total_from_OrderDetail(Int32 purchaseorderid);
        //Insert New Quote comments to quotecomment Table
        Int32 Insert_Quotescomment_to_quotecommentTable(Cls_Quotescomment Quote);
        //To Delete Quote comment table by purchaseorderid
        Int32 Delete_Quotescomment_From_quotecommentTable(Int32 purchaseorderid);
        // Fetch Quotes By Quotes id
        Cls_Quotes Fetch_Quotes_Byid(Int32 quoteid);
        // fetchall the order list from master table
        Cls_Order Fetch_Order_List_By_id(Int32 Purchaseid);
        // fetchall the order list from master table in Data Table
        DataTable Fetch_Order_List_By_id_DataTable(Int32 Purchaseid);
        //Check for SMTP department for Update 
        // fetch total price
        string Fetch_Shopping_Total(string sessionid);
        #endregion

        #region Add size
        // Add size
        Int32 Insert_size(string size, string cmsid);
        // Fetch size
        DataTable Fetch_size(string cmsid);
        // Add size
        Int32 Delete_size(string sizeid);
        #endregion

        #region Add color
        // Upload image
        Int32 Insert_color_Details(string cmsid, string color, string image);
        // Fetch color
        DataTable Fetch_color_Details(string cmsid);
        //Fetch color image Data Using By colorid
        string Fetch_color_By_colorid(string colorid);
        //Product Details Using By cmsid
        DataTable Get_One_Product_Detail(Int32 cmsid);
        //color Details Using By colorid
        DataTable Fetch_color_Details_By_colorid(string colorid);
        // Upload image
        Int32 Update_color_image(string colorid, string cmsid, string color, string image);
        // Delete color
        Int32 Delete_color(string colorid);
        //Fetch Default size And color
        DataTable Fetch_Product_Default_size_color(string cmsid);
        #endregion

        #region new user guide
        DataTable Fetch_LeftSide_submodules_By_roleid_UserGuide(Int32 roleid, Int32 moduleid);
        DataTable Fetch_ModulesName_UserGuide();
        // fetch submodule on basis of module
        DataTable Fetch_SubModulesname(int moduleid);
        DataTable Fetch_SubModulesname_UserGuide(int moduleid);
        // Fetch user guide module
        DataTable Fetch_UserGuidemodules();
        // To Get Maximum Display Order for userGuideModule
        Int32 Get_Max_UserGuidemodules_dorder();
        //To Insert New User Guide Module
        Int32 Insert_UserGuidemodules(Cls_UserGuidemodules clsUserGuidemodules);
        // To Update Existing User Guide Module
        Int32 Update_UserGuidemodules(Cls_UserGuidemodules clsUserGuidemodules);
        // Fetch user guide modules By  id
        Cls_UserGuidemodules Fetch_UserGuidemodules_Byid(Int32 maincontentid);
        Int32 Update_UserGuideModule_dorder(List<Cls_UserGuidemodules> objUserGuidemodules);
        // Fetch user guide module for dashboard
        DataTable Fetch_UserGuidemodules_ForDashboard();
        // Reordering functions block for User Guide modules Display Order
        void Reorder_UserGuidemodules(Int32 maincontentid, Int32 CurrentOrder, string OpType);
        //Check for Existing User Guide Module 
        Int32 Check_Existing_UserGuideModule(Int32 moduleid, Int32 Submoduleid, Int32 maincontentid, Int32 Mode);
        // functions for Binding UserGuide on display Page start here
        //Step.1 TO Get User Guide Main modules Details
        DataTable Get_Main_Module_Data(Int32 moduleid, Int32 Submoduleid);
        //Step.2 TO Get User Guide Main modules images Details
        DataTable Get_User_Guide_images(Int32 sourceid, string sourcename,Int32 roleid);
        // Step.3 To Get User Guide Module Actions Details
        DataTable Get_UserGuide_Module_Detail(Int32 maincontentid);
        // Step.4 To Get User Guide Module Actions description
        DataTable Get_UserGuide_Module_Action_description(Int32 moduledetailid);
        // functions for Binding UserGuide on display Page End here
        //fetch side menu using Roll id
        DataTable Fetch_SubModules_By_roleid_UserGuide(Int32 roleid);
         #endregion

        #region UserGuideimage

        // Fetch user guide image
        DataTable Fetch_images_BasedOn_sourceidAndname(Int32 sourceid, string sourcename);
        //To Delete image
        Int32 Delete_UserGuideimage(Int32 imageid);
        // Reordering image Display Order
        void Reorder_UserGuideimage(Int32 imageid, Int32 displayorder, string OpType, Int32 sourceid, string sourcename);
        // To Get Max image By sourceid for Display Order
        Int32 Get_Max_UserGuideimage_dorder(Int32 sourceid, string source);
        // Adjust user guide image Display Order 
        void Adjust_image_dorder(Int32 sourceid,string source);
        // Fetch All role
        DataTable Fetch_Rolename();
        // Fetch user guide modules By  id
        Cls_UserGuideimage Fetch_UserGuideimage_Byid(Int32 imageid);
        // check existing user guide image
        Int32 Check_Existing_UserGuideimage(string imagename);
        //To Insert New User Guide image
        Int32 Insert_UserGuideimages(Cls_UserGuideimage clsUserGuideimages);
        // To Update Existing User Guide images
        Int32 Update_UserGuideimages(Cls_UserGuideimage clsUserGuideimages);
        // update display order
        Int32 Update_UserGuideimages_dorder(List<Cls_UserGuideimage> objUserGuideimages);
        #endregion

        #region User Guide Action description
         // Bind User Guid Action description data by ModelDetailid
         DataTable Fetch_UG_ActionDesc_Byid(Int32 ModelDetailid);
         //Delete From UserGuideActiondescription By actiondetailid
         Int32 Delete_UserGuideActiondescription_By_id(Int32 actiondetailid);
         // Reordering functions block for User Guide Action description Display Order
         void Reorder_UserGuideAction(Int32 actiondetailid, Int32 moduledetailid, Int32 CurrentOrder, string OpType);
         //Fetch User Guide Action description Data By actiondetailid
         Cls_UGActionDesc Fetch_UG_ActionData_Byid(Int32 actiondetailid);
         //Insert Into User Guide Action Desription
         Int32 Insert_UG_Action(Cls_UGActionDesc objMainContent);
         //Update User Guide description Data
         Int32 Update_UG_Action(Cls_UGActionDesc objMainContent);
         //Update User Guide Action description Display Order
         Int32 Update_UG_Action_displayorder(List<Cls_UGActionDesc> objMainContent);
         // To Get Maximum Display Order 
         Int32 Get_Max_UserGuideAction_dorder(Int32 moduledetailid);
        #endregion

        #region UserGuide Detail
        // Fetch user guide detail 
            DataTable Fetch_ModuleDetailContent_Byid(Int32 maincontentid);
         // To Delete Mudule detail By id
            Int32 Delete_ModuleDetailContent(Int32 moduledetailid);

          // To Get Max  ModuleDetail By ModuleContent for Display Order
            Int32 Get_Max_ModuleDetailContent_dorder(Int32 maincontentid);
         // Adjust ModuleDetail content 
            void Adjust_MuduleDetailContent_dorder(Int32 ModuleContentid);
         //Reordering functions block for ModuleDetail Content Display Order
            void Reorder_ModuleDetailContent(Int32 moduledetailid, Int32 maincontentid, Int32 CurrentOrder, string OpType);
           // Fetch UserGuide Module deatil
              Cls_UserGuideModuleDetail Fetch_UserGuideModuleDetail_Byid(Int32 moduledetailid);
         //Check for Existing user guide ContentDetail
            Int32 Check_Existing_USerGuideContentDetail(string contentheading);
            // To Insert New Userguide Detail
            Int32 Insert_UserGuideContentDetail(Cls_UserGuideModuleDetail clsUserGuideModuleDetail);
        // To Update Existing User guide detail content
            Int32 Update_UserGuideContentDetail(Cls_UserGuideModuleDetail clsUserGuideModuleDetail);
           // To Update Existing User Guide Detail Display order
            Int32 Update_UserGuideDetail_dorder(List<Cls_UserGuideModuleDetail> objUserGuideDetail);
          #endregion

        #region Country Flag
            // To get Country Flag
         string Get_Flag_By_Country_name(string Countryname);

         #endregion

        #region slider Mangement

         /****************************************Start Master Slider Function**/
         //Insert Data into Slider Master Table
         Int32 Insert_Slider_Master(ClsSliderMaster objClsSliderMaster);


         //Get Slider Master Table Data
         DataTable Fetch_Slider_Master_Data();

         //Get Slider Master Table Data Using by Slider id
         DataTable Fetch_Slider_Master_Data_By_sliderid(int sliderid);


         //To Delete slider Master by Slider id
         Int32 Delete_Slider_Master_Data_By_sliderid(int sliderid);


         // Update slider Master 
         Int32 Update_Slider_Master_Data(ClsSliderMaster objClsSliderMaster);

         //Get Fetch Slider name By sliderid
         DataTable Fetch_Slider_name_By_sliderid(int sliderid);


         //Get Fetch Slider Details for Frontend
         DataTable Fetch_Slider_size_Details(int sliderid);

         // get amx display order for Slider Special buttons
         Int32 Get_Max_SpecialButton_dorder(Int32 sliderid);

         //To Insert Special button Details
         Int32 Insert_Slider_Special_Button_Detail(Cls_SliderSpecialButtons objCls_SliderSpecialButtons);

         // Update Special button Details
         Int32 Update_Slider_Special_Button_Detail(Cls_SliderSpecialButtons objCls_SliderSpecialButtons);

         //To Delete Slider Special Button
         Int32 Delete_Slider_Special_Button_by_id(Int32 id);

         //Check Special Button name
         bool Check_Slider_Special_Button_name(Int32 sliderid, string buttontext);


         //Get Fetch Slider Special Button
         DataTable Fetch_Slider_Special_Buttons(Int32 sliderid);

         //Get Fetch Slider Special Button Using id
         DataTable Fetch_Slider_Special_Buttons_By_id(Int32 id);

         //To Insert text Contain
         Int32 Insert_Special_Contain(Cls_SliderContent objSliderContent);

         //To Update text Contain
         Int32 Update_Special_Contain(Cls_SliderContent objSliderContent);


         //Check slider Content Text
         bool Check_Slider_Contain_Text(Int32 sliderid);

         //Get Fetch Slider Contain Text
         DataTable Fetch_Slider_Contain_Text_By_sliderid(Int32 sliderid);



         //Get Fetch Slider Contain Text
         string Fetch_Slider_Contain_Text_By_sliderid_Show(Int32 sliderid);

         //Get Fetch Slider Button
         DataTable Fetch_Slider_SpecialButton_By_sliderid_Show(Int32 sliderid);


         /****************************************End Master Slider Function**/

         /****************************************Start Slider Item Master Function**/

         //Insert Data into Slider Master Table
         Int32 Insert_SliderItem_Master_Item(ClsSliderItemMaster objClsSliderItemMaster);

         //Get Fetch Slider image Using sliderid For Bind SliderItemDashboard
         DataTable Fetch_SliderItem_Master_image(int sliderid);


         //Get Fetch Using SliderItem For Edit
         DataTable Fetch_SliderItem_For_Edit(int slideritemid);


         //Fetch SliderItemMaster order by display order
         DataTable Fetch_SliderItemMaster_Order_By_dorder(int sliderid);

         // Update slider Item Master 
         Int32 Update_Slider_Item_Master_Data(ClsSliderItemMaster objClsSliderItemMaster);

         //To Delete image slides
         Int32 Delete_SliderItemMaster_Data_By_slideritemid(int slideritemid);

         //Get Slider Item Display order
         Int32 Get_Max_SliderItem_displayorder(int sliderid);

         //--Fetch SliderItemMaster For Display Order--------//
         ClsSliderItemMaster Fetch_SliderItemMaster_By_slideritemid(Int32 slideritemid);

         // Update Display order of SliderItemMaster
         Int32 Update_SliderItemMaster_dorder(List<ClsSliderItemMaster> LstobjClsSliderItemMaster);

         /****************************************End Slider Item Master Function**/

         /****************************************Start Slider Item Details Function**/
         Int32 Insert_Slider_Details(ClsSliderDetails objClsSliderDetails);

         //Get Slider Details Table Data Using by Slider id
         DataTable Fetch_Slider_Details_Data_By_sliderid(int slideritemid);

         //Fetch Slider name Using UPload image into Item Details
         string Fetch_Slider_name_By_slideritemid(int slideritemid);

         //To Delete slider Details
         Int32 Delete_Slider_Details_By_slideritemcontentid(int slideritemcontentid);


         // Update slider Item Details
         Int32 Update_Slider_Item_Details(ClsSliderDetails objClsSliderDetails);


         //Get Slider Details  Using by slideritemcontentid
         DataTable Fetch_Slider_Details_Data_By_slideritemcontentid(int slideritemcontentid);


         /**************************display order**************************/
         //Get Slider Details  Using by slideritemcontentid
         DataTable Fetch_Slider_Details_For_Display_Order(int slideritemid);

         //Get SliderItemDetails Display order
         Int32 Get_Max_SliderItemDetails_displayorder(int slideritemid);

         // Update Display order of slideritemcontentid
         Int32 Update_SliderItemMaster_dorder(List<ClsSliderDetails> LstobjClsSliderDetails);

         /**************************display order**************************/

         /****************************************End Slider Item Details Function**/

         /************************************************Start Slider Details show On Home Page**** ***************************************/
         //Get Top slider id for Homepage
         int Fetch_Slider_sliderid_ForHomePage();

         //Get Slider image
         DataTable Fetch_Slider_image(int sliderid);

         //Get Slider Items image
         DataTable Fetch_Slider_Item_Details_images(int slideritemid);

         /************************************************End Slider Details show On Home Page*******************************************/

         //Get sliderid from PageSliders Using by pagename and cmsid
         Int32 Fetch_Slider_id_By_cmsid_pageid(string pagename, Int64 cmsid);

         //Get sliderid from PageSlider Using by pageid
         Int32 Fetch_Slider_id_By_pageid(string pagename, Int32 cmsid);

         #endregion
        
       #region Application User

        // Fetch ApplicationUser By applicationuserid
        Cls_ApplicationUser Fetch_ApplicationUser_Byid(Int32 applicationuserid);

        //To Insert New ApplicationUsers
         Int32 Insert_ApplicationUsers(Cls_ApplicationUser cls);

         //To Update Existing ApplicationUsers
         Int32 Update_ApplicationUsers(Cls_ApplicationUser cls);

         // Fetch ApplicationUsers
          DataTable Fetch_ApplicationUsers();

         //To Delete ApplicationUsers
           Int32 Delete_ApplicationUsers(Int32 applicationuserid);
       #endregion

    } // Interface Ends Here
}
