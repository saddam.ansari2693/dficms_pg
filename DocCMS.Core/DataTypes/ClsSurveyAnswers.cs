﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsSurveyanswers
    {
        public Int32 surveyanswerid { get; set; }
        public Int32 surveyquestionid { get; set; }
        public Int32 surveyid { get; set; }
        public string answer { get; set; }
        public Int32 displayorder { get; set; }
        public bool isactive { get; set; }

    }
}
