﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class Cls_Video
    {
        public string videoname { get; set; }
        public string videovalue { get; set; }
        public string videopath { get; set; }
        public string coverimage { get; set; }
        public string returnvalue { get; set; }
        public string coverimagePath { get; set; }
        public string oldfilepath { get; set; }
    }
}
