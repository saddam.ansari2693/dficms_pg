﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsSliderMaster
    {
        public int sliderid { get; set; }
        public string slidername { get; set; }
        public string fullwidth { get; set; }
        public string sliderposition { get; set; }
        public string height { get; set; }
        public string width { get; set; }
        public string imageheight { get; set; }
        public string imagewidth { get; set; }
        public string buttontextSection { get; set; }
        public string isactive { get; set; }
        public string createdby { get; set; }
        public DateTime creationdate { get; set; }
        public string lastmodifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }
}
