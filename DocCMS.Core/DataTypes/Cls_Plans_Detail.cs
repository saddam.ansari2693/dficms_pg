﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class Cls_Plans_Detail
    {
        public Int32 plandetailid { get; set; }
        public Int32 planid { get; set; }
        public string featurename { get; set; }
        public Int32 displayorder { get; set; }
        public bool active { get; set; }
    }
}
