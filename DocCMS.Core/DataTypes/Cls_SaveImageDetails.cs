﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class Cls_SaveimageDetails
    {
        public string filename { get; set; }
        public string imageheight { get; set; }
        public string imagewidth { get; set; }
        public string filepath { get; set; }
    }
}
