﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Clstimeout
    {
        public Int32 timeoutid { get; set; }
        public Int32 timeout { get; set; }
        public string creationdate { get; set; }
        public string lastmodificationdate { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
        public bool isactive { get; set; }
    }
}
