﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_UGActionDesc
    {
        public Int32 actiondetailid { get; set; }
        public Int32 moduledetailid { get; set; }
        public string heading { get; set; }
        public string description { get; set; }
        public bool isactive { get; set; }
        public Int32 displayorder { get; set; }
        public string createdby { get; set; }
        public DateTime createddate { get; set; }
        public string modifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }
}
