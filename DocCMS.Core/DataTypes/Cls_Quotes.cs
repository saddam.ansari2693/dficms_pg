﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class Cls_Quotes
    {
       public Int32 quoteid { get; set; }
       public string quotename { get; set; }
       public string quotecomment { get; set; }
       public bool active { get; set; }
    }
}
