﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_ContentManagement
    {
        public Int32 cmsid { get; set; }
        public Int32 pageid { get; set; }
        public string pagename { get; set; }
        public string responsedate { get; set; }
        
        // CMS Details
        public Int32 cmsdetailid { get; set; }
        public Int32 dorder { get; set; }  
        
        public string fieldtext { get; set; }
        public string fieldtype { get; set; }
        public string fieldvalue { get; set; }
        public string displayorder { get; set; }


        public Guid createdby { get; set; }
        public DateTime creationdate { get; set; }
        public Guid lastmodifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }// Class ends Here
}