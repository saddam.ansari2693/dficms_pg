﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_ApplicationUser
    {
        public Int32 applicationuserid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool isactive { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string createdby { get; set; }
        public DateTime creationdate { get; set; }
        public string lastmodifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
      
    }
}
