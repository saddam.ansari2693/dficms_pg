﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
  public class Cls_UserGuideModuleDetail
    {
        public Int32 moduledetailid { get; set; }
        public Int32 maincontentid { get; set; }
        public string contentheading { get; set; }
        public string description { get; set; }
        public Int32 displayorder { get; set; }
        public bool isactive { get; set; }
        public Guid createdby { get; set; }
        public DateTime creationdate { get; set; }
        public Guid modifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }
}
