﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_Blog_category
    {
        public Int32 blogcategoryid { get; set; }
        public string blogcategoryname { get; set; }
        public string description { get; set; }
        public string navigationurl { get; set; }
        public Int32 dorder { get; set; }
        public bool isactive { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string lastmodifiedby { get; set; }
        public string LastModifictiondate { get; set; }


    }
}
