﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class Cls_Quotescomment
    {
       public Int32 quotecommentid { get; set; }
        public string quotename { get; set; }
        public string quotecomment { get; set; }
        public Int32 purchaseorderid { get; set; }
        public Int32 dorder { get; set; }
    }
}
