﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public  class Cls_UG_MainContent
    {
        public Int32 maincontentid { get; set; }
        public string heading { get; set; }
        public string description { get; set; }
        public Int32 dorder { get; set; }
        public bool isactive { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string lastmodifiedby { get; set; }
        public string lastmodificationdate { get; set; }
    }
}
