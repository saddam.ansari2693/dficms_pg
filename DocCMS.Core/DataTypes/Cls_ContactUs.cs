﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_ContactUs
    {
        public Int32 id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string emailid { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public string phoneno { get; set; }
        public DateTime creationdate { get; set; }
    }
}