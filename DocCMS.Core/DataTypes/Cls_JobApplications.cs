﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_JobApplications
    {
        public Int32 jobapplyid { get; set; }
        public string jobtitle { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string phoneno { get; set; }
        public string emailid { get; set; }
        public string resumeheading { get; set; }
        public string description { get; set; }
        public string filename { get; set; }
        public DateTime applydate { get; set; }
        public string skills { get; set; }
    }
}
