﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    [Serializable]
  public class Cls_UserGuidemodules
    {
        public Int32 maincontentid { get; set; }
        public Int32 moduleid { get; set; }
        public Int32 submoduleid { get; set; }
        public string description { get; set; }
        public Int32 displayorder { get; set; }
        public bool isexternalcontent { get; set; }
        public bool isactive { get; set; }
        public Guid createdby { get; set; }
        public DateTime creationdate { get; set; }
        public Guid modifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }
}
