﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class Cls_UserGuideimage
    {
       public Int32 imageid { get; set; }
       public string imagename { get; set; }
       public string source { get; set; }
       public Int32 sourceid { get; set; }
       public Int32 roleid { get; set; }
       public Int32 imagewidth { get; set; }
       public Int32 imageheight { get; set; }
        public Int32 displayorder { get; set; }
        public bool isactive { get; set; }
        public Guid createdby { get; set; }
        public DateTime createddate { get; set; }
        public Guid modifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }
}
