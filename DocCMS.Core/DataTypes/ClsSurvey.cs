﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsSurvey
    {
        public Int32 surveyid { get; set; }
        public string surveyname { get; set; }
        public string description { get; set; }
        public int displayorder{get;set;}
        public bool defaultactive { get; set; }
        public bool showreport { get; set; }
        public string createdby { get; set; }
        public DateTime creationdate { get; set; }
        public string modifiedby { get; set; }
        public DateTime modifieddate { get; set; }

    }
}
