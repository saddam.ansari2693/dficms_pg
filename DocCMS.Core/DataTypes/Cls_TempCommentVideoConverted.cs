﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_TempcommentVideoConverted
    {
        public Int64 videoid { get; set; }
        public string sessionid { get; set; }
        public string memberid { get; set; }
        public string videoname { get; set; }
        public string coverimage { get; set; }
    }
}