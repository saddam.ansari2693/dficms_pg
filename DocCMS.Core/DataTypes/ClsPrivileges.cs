﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    [Serializable]
    public class Clsprivileges
    {
        public Int32 privilgeid { get; set; }
        public string privilegename { get; set; }
        public Int32 displayorder { get; set; }
        public bool active { get; set; }
        public Guid createdby { get; set; }
        public DateTime creationdate { get; set; }
        public Guid lastmodifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }//Class ends here
}
