﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_ShopingCart
    {
        public Int32 id { get; set; }
        public string sessionid { get; set; }
        public string productname { get; set; }
        public string productnumber { get; set; }
        public string internaldescription { get; set; }
        public string category { get; set; }
        public int qty { get; set; }
        public string description { get; set; }
        public decimal cost { get; set; }
        public decimal  price { get; set; }
        public decimal runningprice { get; set; }
        public string size { get; set; }
        public string color { get; set; }

    }
}
