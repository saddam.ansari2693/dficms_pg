﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsSliderDetails
    {
        public int slideritemcontentid { get; set; }
        public int slideritemid { get; set; }
        public string itemtype { get; set; }
        public string image { get; set; }
        public string itemcontent { get; set; }
        public string contentposition { get; set; }
        public string fontsize { get; set; }
        public string fontcolor { get; set; }
        public string isbold { get; set; }
        public string isitalic { get; set; }
        public string isunderline { get; set; }
        public string isemphasized { get; set; }
        public string entrystyle { get; set; }
        public string entrydelay { get; set; }
        public string margintop { get; set; }
        public string marginbottom { get; set; }
        public string marginright { get; set; }
        public string marginleft { get; set; }
        public string textbackgroundcolor { get; set; }
        public string navigationurl { get; set; }
        public int dorder { get; set; }
        public string isactive { get; set; }
        public string dataeffect { get; set; }
    }
}
