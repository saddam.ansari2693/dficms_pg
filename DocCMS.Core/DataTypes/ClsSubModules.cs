﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
     [Serializable]
    public class Clssubmodules
        {
            public Int32 submoduleid { get; set; }
            public Int32 moduleid { get; set; }
            public string submodulename { get; set; }
            public string modulename { get; set; }
            public string description { get; set; }
            public string navigationurl { get; set; }
            public Int32 displayorder { get; set; }
            public bool active { get; set; }
            public Guid createdby { get; set; }
            public DateTime creationdate { get; set; }
            public Guid lastmodifiedby { get; set; }
            public DateTime lastmodificationdate { get; set; }
        }//Class ends here
    
}
