﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_Blog
    {
        public Int32 blogid { get; set; }
        public Int32 blogcategoryid { get; set; }
        public string blogname { get; set; }
        public string blogpostedby { get; set; }
        public string navigationurl { get; set; }
        public string imagename { get; set; }
        public string description { get; set; }
        public Int32 dorder { get; set; }
        public bool isactive { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string lastmodifiedby { get; set; }
        public string lastmodificationdate { get; set; }
        public bool allowposting { get; set; }
        public int blogtype { get; set; }
        public string accesstype { get; set; }

    }
}
