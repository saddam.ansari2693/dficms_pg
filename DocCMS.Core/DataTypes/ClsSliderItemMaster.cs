﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   public class ClsSliderItemMaster
    {
        public int slideritemid { get; set; }
        public int sliderid { get; set; }
        public string imagename { get; set; }
        public int dorder { get; set; }
        public string isactive { get; set; }
    }
}
