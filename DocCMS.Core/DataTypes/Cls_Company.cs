﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    [Serializable]
    public class Cls_company
    {
        public Int32 companyid { get; set; }
        public string companyname { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string city{ get; set; }
        public string state { get; set; }
        public string companylogo { get; set; }
        public bool isactive { get; set; }
        public Guid createdby { get; set; }
        public string creationdate { get; set; }
        public Guid lastmodifiedby { get; set; }
        public string lastmodificationdate { get; set; }

    }
}
