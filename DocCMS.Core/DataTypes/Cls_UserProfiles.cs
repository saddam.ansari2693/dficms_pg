﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    [Serializable]
    public class Cls_UserProfiles
    {
        public string userid { get; set; }
        public string password { get; set; }
        public string emailid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string roleid { get; set; }
        public string rolename { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public bool active { get; set; }
        public string phonenumber { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string modifiedby { get; set; }
        public string modifieddate { get; set; }
        public string imagename { get; set; }
        
    }//Class ends here
}
