﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsSurveyResponse
    {
        public Int32 responseid { get; set; }
        public Int32 surveyid { get; set; }
        public string sipaddress { get; set; }
        public string ipaddress { get; set; }
        public DateTime responsedate { get; set; }

        /// for survey response detail
        public Int32 surveryresponseid { get; set; }
        public Int32 questionid { get; set; }
        public Int32 answerid { get; set; }
        public string responsetext { get; set; }
        public string responsetype { get; set; }
        public DateTime answeredOn { get; set; }
        public string additionaltext { get; set; }


    }
}