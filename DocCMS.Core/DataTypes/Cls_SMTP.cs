﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Net;

namespace DocCMS.Core.DataTypes
{
    public class Cls_SMTP
    {
        public Int32 smtpid { get; set; }
        public string smtphostserver { get; set; }
        public string smtpport { get; set; }
        public string senderemailid { get; set; }
        public string senderpassword { get; set; }
        public bool smtpsslserver { get; set; }

        //SMTP Detail emails starts from here 
        public Int32 id { get; set; }
        public string department { get; set; }
        public string toemail { get; set; }
        public string ccemail { get; set; }
        public string bccemail { get; set; }
        public bool isactive { get; set; }

    }// Class ends Here
}
