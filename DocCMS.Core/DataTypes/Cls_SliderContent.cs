﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_SliderContent
    {
        public Int32 id { get; set; }
        public Int32 sliderid { get; set; }
        public string textcontent { get; set; }
        public string isactive { get; set; }
        public string createdby { get; set; }
        public DateTime creationdate { get; set; }
        public string updatedby { get; set; }
        public DateTime updationdate { get; set; }
    }
}
