﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_UG_SubContentimages
    {
        public int id { get; set; }
        public int subcontentid { get; set; }
        public string imagename { get; set; }
        public int dorder { get; set; }
        public string isactive { get; set; }
    }
}
