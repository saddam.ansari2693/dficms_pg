﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsSurveyquestions
    {
        public Int32 surveyquestionid { get; set; }
        public Int32 surveyid { get; set; }
        public string question { get; set; }
        public string answerType { get; set; }
        public int displayorder { get; set; }
        public bool isactive { get; set; }
        public bool showreport { get; set; }
    }
}
