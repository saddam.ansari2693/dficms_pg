﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_comment
    {
        public Int64 commentid { get; set; }
        public Int32 blogid { get; set; }
        public string memberid { get; set; }
        public string comment { get; set; }
        public DateTime posteddate { get; set; }
        public string imageurl { get; set; }
        public string imagethumbnailurl { get; set; }
        public string videourl { get; set; }
        public string videocoverimageurl { get; set; }
        public string accesstype { get; set; }
    }
}
