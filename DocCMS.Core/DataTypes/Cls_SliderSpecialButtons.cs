﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_SliderSpecialButtons
    {
        public Int32 id { get; set; }
        public Int32 sliderid { get; set; }
        public string buttontext { get; set; }
        public string navigationurl { get; set; }
        public string buttoncolor { get; set; }
        public string isactive { get; set; }
        public Int32 dorder { get; set; }
        public string createdby { get; set; }
        public DateTime creationdate { get; set; }
        public string updateby { get; set; }
        public DateTime updationdate { get; set; }
    }
}
