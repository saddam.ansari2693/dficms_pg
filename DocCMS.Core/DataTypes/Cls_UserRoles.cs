﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    [Serializable]
    public class Cls_UserRoles
    {
        public Int32 roleid { get; set; }
        public string rolename { get; set; }
        public string description { get; set; }
        public Int32 displayorder { get; set; }
        public bool active { get; set; }
        public Int32 parentid { get; set; }
        public Guid createby { get; set; }
        public DateTime creationdate { get; set; }
        public Guid lastmodifiedby { get; set; }
        public DateTime lastmodificationdate { get; set; }
    }//Class ends here
}
