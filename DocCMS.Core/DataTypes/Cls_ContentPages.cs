﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{

    public class Cls_ContentPages
    {
        public string pageid { get; set; }
        public string pagename { get; set; }
        public string pagedescription { get; set; }
        public string expirationdate { get; set; }
        public bool ismultipage { get; set; }
        public bool isactive { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string lastmodifiedby { get; set; }
        public string lastmodificationdate { get; set; }


    }
}
