﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_OrderDetail
    {
        /***** Order Detail varible---*****/
        public Int32 orderdetailid { get; set; }
        public Int32 purchaseorderid { get; set; }
        public string categoryname { get; set; }
        public string productname { get; set; }
        public string productnumber { get; set; }
        public string internaldescription { get; set; }
        public Int32 quantity { get; set; }
        public string description { get; set; }
        public string price { get; set; }
        public string cost { get; set; }
        public string runningprice { get; set; }
        public string size { get; set; }
        public string color { get; set; }
        public string quantityshipped { get; set; }
        public string quantitybackorder { get; set; }
    }
}
