﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
   
    [Serializable]
    public class Cls_roleprivileges
    {
        public Int32 id{ get; set; }
        public Int32 roleid { get; set; }
        public Int32 moduleid { get; set; }
        public Int32 submoduleid { get; set; }
        public bool isview { get; set; }
        public bool isadd { get; set; }
        public bool isedit { get; set; }
        public bool isdelete { get; set; }
  
    }//Class ends here
}
