﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class ClsChatManager
    {
        public int livechatmanagerid { get; set; }
        public string isactive { get; set; }
        public string creationdate { get; set; }
        public string lastmodificationdate { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
    }
}
