﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_Plans
    {
        public Int32 planid { get; set; }
        public string planname { get; set; }
        public string plandescription { get; set; }
        public string imagename { get; set; }
        public string price { get; set; }
        public Int32 dorder { get; set; }
        public bool isactive { get; set; }
        public Guid createdby { get; set; }
        public string creationdate { get; set; }
        public Guid lastmodifiedby { get; set; }
        public string lastmodificationdate { get; set; }

        // for Plan Detail 
        public Int32 plandetailid { get; set; }
        public string featurename { get; set; }
        public Int32 displayorder { get; set; }
        public bool active { get; set; }
      


    }//Class ends here
}
