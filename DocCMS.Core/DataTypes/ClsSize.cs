﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Clssize
    {
        public Int64 sizeid { get; set; }
        public Int64 cmsid { get; set; }
        public string size { get; set; }
    }
}
