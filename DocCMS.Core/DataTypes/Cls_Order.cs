﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Cls_Order
    {
        public Int32 purchaseorderid { get; set; }
        public string orderno { get; set; }
        public string companyname { get; set; }
        public string purchasername { get; set; }
        public string billingaddress1 { get; set; }
        public string billingaddress2 { get; set; }
        public string billingcity { get; set; }
        public string billingprovience { get; set; }
        public string billingpostalcode { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string paymentmethod { get; set; }
        public bool isbackorderallowed { get; set; }
        public DateTime orderdate { get; set; }
        public string grandtotal { get; set; }
        public string requesttype { get; set; }
        public string otherdetail { get; set; }
        public string ponumber { get; set; }
        public string shippingaddress1 { get; set; }
        public string shippingaddress2 { get; set; }
        public string shippingcity { get; set; }
        public string shippingprovience { get; set; }
        public string shippingpostalcode { get; set; }
        public string orderstatus { get; set; }
        public string discountedtotal { get; set; }
    }
}
