﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
  
   public class Cls_ContentPage_Detail
    {
        public Int32 pagedetailid { get; set; }
        public Int32 pageid { get; set; }
        public string filedtype { get; set; }
        public Int32 dorder { get; set; }
        public string textfieldheading1 { get; set; }
        public string textfieldheadingvalue1 { get; set; }
        public string textfieldheading2 { get; set; }
        public string textfieldheadingvalue2 { get; set; }
        public string textfieldheading3 { get; set; }
        public string textfieldheadingvalue3 { get; set; }
        public string textfieldheading4 { get; set; }
        public string textfieldheadingvalue4 { get; set; }
        public string textfieldheading5 { get; set; }
        public string textfieldheadingvalue5 { get; set; }
        public string textfieldheading6 { get; set; }
        public string textfieldheadingvalue6 { get; set; }
        public string textfieldheading7 { get; set; }
        public string textfieldheadingvalue7 { get; set; }
        public string textfieldheading8 { get; set; }
        public string textfieldheadingvalue8 { get; set; }
        public string textfieldheading9 { get; set; }
        public string textfieldheadingvalue9 { get; set; }
        public string textfieldheading10 { get; set; }
        public string textfieldheadingvalue10 { get; set; }
        public string textfieldheading11 { get; set; }
        public string textfieldheadingvalue11 { get; set; }
        public string textfieldheading12 { get; set; }
        public string textfieldheadingvalue12 { get; set; }
        public string textfieldheading13 { get; set; }
        public string textfieldheadingvalue13 { get; set; }
        public string textfieldheading14 { get; set; }
        public string textfieldheadingvalue14 { get; set; }
        public string textfieldheading15 { get; set; }
        public string textfieldheadingvalue15 { get; set; }
        public string textfieldheading16 { get; set; }
        public string textfieldheadingvalue16 { get; set; }
        public string textfieldheading17 { get; set; }
        public string textfieldheadingvalue17 { get; set; }
        public string textfieldheading18 { get; set; }
        public string textfieldheadingvalue18 { get; set; }
        public string textfieldheading19 { get; set; }
        public string textfieldheadingvalue19 { get; set; }
        public string textfieldheading20 { get; set; }
        public string textfieldheadingvalue20 { get; set; }
        public string textfieldheading21 { get; set; }
        public string textfieldheadingvalue21 { get; set; }
        public string textfieldheading22 { get; set; }
        public string textfieldheadingvalue22 { get; set; }
        public string textfieldheading23 { get; set; }
        public string textfieldheadingvalue23 { get; set; }
        public string textfieldheading24 { get; set; }
        public string textfieldheadingvalue24 { get; set; }
        public string textfieldheading25 { get; set; }
        public string textfieldheadingvalue25 { get; set; }
        public string textfieldheading26 { get; set; }
        public string textfieldheadingvalue26 { get; set; }
        public string textfieldheading27 { get; set; }
        public string textfieldheadingvalue27 { get; set; }
        public string textfieldheading28 { get; set; }
        public string textfieldheadingvalue28 { get; set; }
        public string textfieldheading29 { get; set; }
        public string textfieldheadingvalue29 { get; set; }
        public string textfieldheading30 { get; set; }
        public string textfieldheadingvalue30 { get; set; }

    }
}
