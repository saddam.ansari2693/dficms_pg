﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCMS.Core.DataTypes
{
    public class Clsfileuploader
    {
        public string id { get; set; }
        public string filecategoryid { get; set; }
        public string filename { get; set; }
        public string filepath { get; set; }
        public string creationdate { get; set; }
        public string modificationdate { get; set; }
        public string alternatetext { get; set; }
        public string description { get; set; }
        public string uploadedby { get; set; }
        public string videolink { get; set; }
    }
}
