﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain
{
    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
           {
               Redirect_On_Active_First_Page(1);
           }
        }

        protected void Redirect_On_Active_First_Page(Int32 PageID)
        {
            DataTable dtMainMenu = ServicesFactory.DocCMSServices.Fetch_Main_Menu_Dashboard_Bypagename(PageID);
            if (dtMainMenu != null && dtMainMenu.Rows.Count > 0)
            {
                Int32 Count = 0;
                for (Int32 i = 0; i < dtMainMenu.Rows.Count; i++)
                {
                    DataTable dtMenu = ServicesFactory.DocCMSServices.Fetch_Menu_Detail_By_cmsid(Convert.ToInt32(dtMainMenu.Rows[i]["CMSID"]));
                    if (dtMenu != null && dtMenu.Rows.Count > 0)
                    {
                        if (Count == 0)
                        {
                            Session["CurrPage"] = Convert.ToString(dtMenu.Rows[0]["MenuName"]).Trim();
                            Response.Redirect(Convert.ToString(dtMenu.Rows[1]["MenuName"]).Trim().Replace(".aspx",".html"));
                            Count = 1;
                        }
                    }
                    if (Count == 0)
                    {
                        Response.Redirect("/DFI/Error404.aspx");
                    }
                }
            }
        }
    }
}