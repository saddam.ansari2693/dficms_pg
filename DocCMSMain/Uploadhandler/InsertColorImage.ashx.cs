﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace RMAMain.Uploadhandler
{
    /// <summary>
    /// Summary description for InsertColorImage
    /// </summary>
    public class InsertColorImage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string ImageNewName = "";
            string ImageExtension = "";
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                      string[] testfiles = file.FileName.Split(new char[] { '\\' });
                      fname = testfiles[testfiles.Length - 1];
                      ImageExtension=Path.GetExtension(fname);
                      ImageNewName = Guid.NewGuid().ToString();
                      ImageNewName = ImageNewName + ImageExtension;
                    }
                    else
                    {
                        fname = file.FileName;
                        ImageExtension = Path.GetExtension(fname);
                        ImageNewName = Guid.NewGuid().ToString();
                        ImageNewName = ImageNewName + ImageExtension;
                    }
                  string ImagePath = HttpContext.Current.Server.MapPath("~/UploadedFiles/ColorImages/");
                  if (!File.Exists(HttpContext.Current.Server.MapPath("~/UploadedFiles/ColorImages/")))
                  {
                      Directory.CreateDirectory(ImagePath);
                  }
                  fname = Path.Combine(context.Server.MapPath("~/UploadedFiles/ColorImages/"), ImageNewName);
                  file.SaveAs(fname);
                }
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(ImageNewName);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}