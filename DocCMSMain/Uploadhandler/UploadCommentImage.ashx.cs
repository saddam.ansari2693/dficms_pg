﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace RMAMain.Uploadhandler
{
    /// <summary>
    /// Summary description for UploadCommentImage
    /// </summary>
    public class UploadCommentImage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Expires = -1;
            try
            {
                HttpPostedFile postedFile = context.Request.Files["Filedata"];
                string savepath = "";
                string tempPath = "";
                tempPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                savepath = context.Server.MapPath(tempPath);
                string filename = postedFile.FileName;
                if (!Directory.Exists(savepath))
                {
                    Directory.CreateDirectory(savepath);
                }
                postedFile.SaveAs(savepath + @"\" + filename);
                context.Response.Write(tempPath + "/" + filename);
                context.Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                context.Response.Write("Error: " + ex.Message);
            }
        }

        public void ThumbNailImageGenerate(HttpPostedFile UploadedFile)
        {
            string ImagePath = "~/UploadedFiles/CommentImage/";
            System.Drawing.Image img1 = System.Drawing.Image.FromFile("~/UploadedFiles/CommentImage/" + UploadedFile.FileName);
            System.Drawing.Image bmp1 = img1.GetThumbnailImage(50, 50, null, IntPtr.Zero);
            bmp1.Save("~/UploadedFiles/CommentImage/" + UploadedFile.FileName);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}