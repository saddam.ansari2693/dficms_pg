﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace DocCMSMain.Emails
{
    public class ClsEmail
    { 
        // General Properties
        public string fromEmail { get; set; }
        public string FromPassword { get; set; }
        public string fromName { get; set; }
        public string ToEmail { get; set; }
        public string ToName { get; set; }
        public string CCEmail { get; set; }
        public string CCName { get; set; }
        public string EmailSubject { get; set; }
        public string smtpServer { get; set; }
        public bool SSL { get; set; }
        public int port { get; set; }
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public NetworkCredential smtpCredentials { get; set; }

        // To Send Email on Account Creation
        public bool SendRequestConfirmationDetails(string toEmail, string toName)
        {
            //=== METHOD TO SEND HTML EMIAL ======================//
            try
            {
                SmtpClient sc = new SmtpClient();
                MailMessage Message = new MailMessage();
                Message.IsBodyHtml = true;
                Message.To.Add(new MailAddress(toEmail, toName));
                Message.From = (new MailAddress(this.fromEmail, this.fromName));
                Message.Subject = this.EmailSubject;
                Message.Body = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("/Emails/OrderReceipt.htm"));
                sc.Host = this.smtpServer;
                sc.Port = this.port;
                sc.Credentials = smtpCredentials;
                sc.EnableSsl = true;
                sc.Send(Message);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }// Class Ends here
}