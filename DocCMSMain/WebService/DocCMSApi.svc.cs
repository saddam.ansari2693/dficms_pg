﻿using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Data;
using System.Configuration;
using System.Web.Script.Serialization;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Web.Script.Services;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using NReco.VideoConverter;
using RMA.Core.DataTypes;
using System.Net;

namespace DocCMSMain.WebService
{

    [ServiceContract(Namespace = " DocCMSMain.WebService", Name = "Api")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]

    #endif
    public class DocCMSApi
    {
        //  To Get Json Object from Datatable
        public string GetJson(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
            new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Check_Existing_PageName(string PageName)
        {
            string Retval = "0";
            try
            {
                Retval = Convert.ToString(ServicesFactory.DocCMSServices.Check_Existing_pagename(PageName));                
                return Retval;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        [OperationContract]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Int64 Insert_Page_Master_Data(Cls_ContentPages objContentMaster)
        {
            Int64 Retval = 0;
            string Result = "";
            try
            {
                if (objContentMaster != null)
                {
                    Retval = ServicesFactory.DocCMSServices.Insert_ContentPage(objContentMaster);
                }
                else
                {
                    Retval = 0;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationContract]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Int64 Insert_Page_Detail_Data(Cls_ContentPage_Detail objPageDetailData)
        {
            Int64 Retval = 0;
            try
            {
                if (objPageDetailData != null)
                {
                    Retval = ServicesFactory.DocCMSServices.Insert_ContentPage_Detail(objPageDetailData);
                }
                else
                {
                    Retval = 0;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Insert_Roles_Privileges(string roleID,string privList)
        {
            string Retval = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Insert_Roles_privileges(roleID, privList);                
                return Retval;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        // Delete the Page Content 

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Delete_Content_Page_ById(string PageID)
        {
            string Retval = "";
            try
            {
                if (!string.IsNullOrEmpty(PageID))
                {
                    Retval = Convert.ToString(ServicesFactory.DocCMSServices.Delete_Content_Page_Byid(PageID));
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Update_Content_Page_ById(string PageID)
        {
            string Retval = "";
            try
            {
                if (!string.IsNullOrEmpty(PageID))
                {
                    Retval = Convert.ToString(ServicesFactory.DocCMSServices.Update_Content_Page_Byid(PageID));
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // To display The Main page Content header section
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Page_Master_Data(string PageID)
        {
            string Retval = "";
            try
            {
                DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_Page_Master_Data_By_pageid(PageID);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        // display the Detail Page Content section
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Page_Detail_Data(string PageID)
        {
            string Retval = "";
            try
            {
                DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_Page_Detail_Data_By_pageid(PageID);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Insert_Survey_Vote(string SurveyID, string IPAddress,string SIPAddress, string SurveyQuestionID, string Ans, string ResponseText, string AdditionalText, string ResponseType)
        {
            try
            {
            string[] SurveyAnswerID = Ans.Split(',');
            string[] SplitResponsetext = null;
            if (ResponseType.ToUpper() == "CHECK")
            {
                SplitResponsetext = ResponseText.Split('~');
            }
            string Retval = "";
            List<ClsSurveyResponse> objResponse = new List<ClsSurveyResponse>();
            ClsSurveyResponse clsresponse = new ClsSurveyResponse();
            clsresponse.surveyid = Convert.ToInt32(SurveyID);
            clsresponse.ipaddress = IPAddress;
            clsresponse.sipaddress = SIPAddress;
            objResponse.Add(clsresponse);
            for (Int32 i = 0; i < SurveyAnswerID.Length-1; i++)
            {
                    clsresponse = new ClsSurveyResponse();
                    clsresponse.questionid = Convert.ToInt32(SurveyQuestionID);
                    if (!string.IsNullOrEmpty(SurveyAnswerID[i]))
                    {
                        clsresponse.answerid = Convert.ToInt32(SurveyAnswerID[i].Trim());
                    }
                    else
                    {
                    }
                    if (ResponseType.ToUpper() == "CHECK")
                    {
                        clsresponse.responsetext = SplitResponsetext[i];
                    }
                    else
                    {
                        clsresponse.responsetext = ResponseText;
                    }
                    clsresponse.additionaltext = AdditionalText;
                    clsresponse.responsetype = ResponseType;
                    objResponse.Add(clsresponse);
                }
                if (objResponse != null && objResponse.Count > 1)
                {
                    Retval = ServicesFactory.DocCMSServices.Insert_Survey_Vote(objResponse);
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Default_Survey(string IPAddress)
        {
            string Retval = "";
            try
            {
                Retval = Convert.ToString(ServicesFactory.DocCMSServices.Fetch_DefaultSet_Survey());       
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string CheckShowReportStatus_BySurveyID(string SurveyID)
        {
            string Retval = "";
            try
            {
                Retval = Convert.ToString(ServicesFactory.DocCMSServices.CheckshowreportStatus_Bysurveyid(Convert.ToInt32(SurveyID)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string CheckShowReportStatus_ByQuestionID(string SurveyQuestionID)
        {
            string Retval = "";
            try
            {
                Retval = Convert.ToString(ServicesFactory.DocCMSServices.CheckshowreportStatus_Forquestion_Byquestionid(Convert.ToInt32(SurveyQuestionID)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Check_Exist_IPAddress(string IPAddress,string SurveyID,string SIPAddress)
        {
            string Retval = "";
            try
            {
                Retval = Convert.ToString(ServicesFactory.DocCMSServices.Check_Exist_Response(IPAddress, Convert.ToInt32(SurveyID), SIPAddress));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Survey_Question(string SurveyID)
        {
            string Retval = "";
            try
            {
                DataTable dtQues = ServicesFactory.DocCMSServices.Fetch_All_question(Convert.ToInt32(SurveyID));
                if (dtQues != null && dtQues.Rows.Count > 0)
                {
                    Retval = GetJson(dtQues);
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Survey_QuestionFor_Report(string SurveyID)
        {
            string Retval = "";
            try
            {
                DataTable dtQues = ServicesFactory.DocCMSServices.Fetch_All_question_For_Report(Convert.ToInt32(SurveyID));
                if (dtQues != null && dtQues.Rows.Count > 0)
                {
                    Retval = GetJson(dtQues);
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Survey_Answer(string SurveyQuesionID)
        {
            string Retval = "";
            try
            {
                DataTable dtAnswer = ServicesFactory.DocCMSServices.Fetch_All_answer_Byquestionid(Convert.ToInt32(SurveyQuesionID));
                if (dtAnswer != null && dtAnswer.Rows.Count > 0)
                {
                    Retval = GetJson(dtAnswer);
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Survey_AnswerWith_Response(string SurveyQuestionID)
        {
            string Retval = "";
            try
            {
                DataTable dtAnswer = ServicesFactory.DocCMSServices.Fetch_All_answer_Byquestionid_With_Response(Convert.ToInt32(SurveyQuestionID));
                if (dtAnswer != null && dtAnswer.Rows.Count > 0)
                {
                    Retval = GetJson(dtAnswer);
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Current_Survey_QuestionDetail(string SurveyQuestionID)
        {
            string Retval = "";
            try
            {
                DataTable dtAnswer = ServicesFactory.DocCMSServices.Fetch_All_answer_Byquestionid_With_Response(Convert.ToInt32(SurveyQuestionID));
                if (dtAnswer != null && dtAnswer.Rows.Count > 0)
                {
                    Retval = GetJson(dtAnswer);
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Survey_Question_ByID(string SurveyQuestionID)
        {
            string Retval = "";
            try
            {
                string dtAnswer = ServicesFactory.DocCMSServices.Fetch_Survey_questionByid(Convert.ToInt32(SurveyQuestionID));
                if (dtAnswer != null )
                {
                    Retval = dtAnswer;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Retval;
        }

        #region Image Editing Inner CMS Pages


        #region Resizing Function for Slider
        // Crop and Resize for Slider
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails CropImageWithResizeSlider(string FilePath, string ImageHeight, string ImageWidth, string XAxis, string YAxis, string CropImageWidth, string CropImageHeight, string BlogId, string userID, string SliderPosition, string sliderid, string slideritemid)
        {
            Cls_SaveimageDetails objSaveImageDetails = new Cls_SaveimageDetails();
            string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1).Replace("/Resize/", "/Crop/");
            string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            System.Drawing.Image resized = Bitmap.FromFile(HttpContext.Current.Server.MapPath(FilePath));
            Bitmap NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
            string OriginalPath = serverPath;
            string NewImagePath = serverPath.Replace("/Original/", "/NewImage/");
            string CropImagePath = serverPath.Replace("/Original/", "/Croped/");
            string ThumbnailImage = serverPath.Replace("/Original/", "/Thumbnail/");
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(NewImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(NewImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CropImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CropImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImage)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImage));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            Bitmap OriginalImage = new Bitmap(resized);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + fileName), ImageFormat.Jpeg);
            NewImage.Save(HttpContext.Current.Server.MapPath(NewImagePath + fileName), ImageFormat.Jpeg);
            System.Drawing.Image orgImg = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(NewImagePath + fileName));
            Rectangle CropArea = new Rectangle(
                 Convert.ToInt32(XAxis) * 6,
                 Convert.ToInt32(YAxis) * 6,
                 Convert.ToInt32(CropImageWidth) * 6,
                 Convert.ToInt32(CropImageHeight) * 6);
            if (!string.IsNullOrEmpty(SliderPosition))
            {
                if (SliderPosition.ToLower() == "fullwidth")
                {
                    CropArea = new Rectangle(
                    Convert.ToInt32(XAxis) * 6,
                    Convert.ToInt32(YAxis) * 6,
                    Convert.ToInt32(CropImageWidth) * 6,
                    Convert.ToInt32(CropImageHeight) * 6);
                    fileName = fileName.ToLower().Replace("org_", "fullwidth_");
                    fileName = fileName.ToLower().Replace("left_", "fullwidth_");
                }
                else
                {
                    CropArea = new Rectangle(
                    Convert.ToInt32(XAxis) * 3,
                    Convert.ToInt32(YAxis) * 3,
                    Convert.ToInt32(CropImageWidth) * 3,
                    Convert.ToInt32(CropImageHeight) * 3);
                    fileName = fileName.ToLower().Replace("org_", "left_");
                    fileName = fileName.ToLower().Replace("fullwidth_", "left_");
                }
            }
            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
            using (Graphics g = Graphics.FromImage(bitMap))
            {
                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
            }
            bitMap.Save(HttpContext.Current.Server.MapPath(CropImagePath + fileName), ImageFormat.Jpeg);
            //ThumnailImage Create
            HttpContext.Current.Server.MapPath(NewImagePath + fileName);
            System.Drawing.Image ThumbImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CropImagePath + fileName));
            Bitmap BitThumbImage = new Bitmap(ThumbImage, new Size(536, 335));
            BitThumbImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImage + fileName), ImageFormat.Jpeg);
            objSaveImageDetails.filename = fileName;
            objSaveImageDetails.filepath = CropImagePath;
            if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName)))
            {
                File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            }
            File.Copy(HttpContext.Current.Server.MapPath(CropImagePath + fileName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            return objSaveImageDetails;
        }

      //Custom Image Create
      [OperationBehavior]
      [WebGet(ResponseFormat = WebMessageFormat.Json)]
      public Cls_SaveimageDetails Custome_Image_Create_Slider(string FilePath, Int32 R, Int32 G, Int32 B, string SliderID, string SliderItemID, string SliderType)
        {
            Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
            string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1).Replace("/Resize/", "/CustomImage/");
            string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            //Background Image Path
            string BackGroundImage = "";
            if (SliderType.ToLower() == "fullwidth")
            {
                fileName = fileName.ToLower().Replace("org", "fullwidth");
                fileName = fileName.ToLower().Replace("left", "fullwidth");
                BackGroundImage = HttpContext.Current.Server.MapPath("~/images/BackGroundImage/FullWidth_BGImage.jpg");
            }
            else
            {
                fileName = fileName.ToLower().Replace("org", "left");
                fileName = fileName.ToLower().Replace("fullwidth", "left");
                BackGroundImage = HttpContext.Current.Server.MapPath("~/images/BackGroundImage/Left_BGImage.jpg");
            }
            //InsertedImage  Image Path
            string InsertedImage = HttpContext.Current.Server.MapPath(FilePath);
            System.Drawing.Image BackGroundImageNew = Bitmap.FromFile(BackGroundImage);
            Graphics gra = Graphics.FromImage(BackGroundImageNew);
            gra.Clear(Color.FromArgb(R, G, B));
            System.Drawing.Image InsertedImageNew = Bitmap.FromFile(InsertedImage);

            Bitmap logo = new Bitmap(InsertedImageNew, new Size(InsertedImageNew.Width, InsertedImageNew.Height));
            //Background Image Height and Width
            int backImageheight = BackGroundImageNew.Height;
            int backImageWidth = BackGroundImageNew.Width;
            //Inserted Image Height and Width
            int FrontImageHeight = InsertedImageNew.Height;
            int FrontImageWidth = InsertedImageNew.Width;

            //Calculate XPosition
            int xPostition = (backImageWidth - FrontImageWidth) / 2;
            //Calculate YPosition
            int yPostition = (backImageheight - FrontImageHeight) / 2;

            gra.DrawImage(logo, new Rectangle(new Point(xPostition, yPostition), new Size(FrontImageWidth, FrontImageHeight)));
            string OriginalPath = serverPath;
            string CustomImagePath = serverPath.Replace("/Original/", "/Customed/");
            string ThumbnailImagePath = serverPath.Replace("/Original/", "/Thumbnail/");

            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CustomImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CustomImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImagePath));
            }
            Bitmap OriginalImage = new Bitmap(InsertedImageNew);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + fileName));
            objSaveImagedetails.filename = fileName;
            objSaveImagedetails.filepath = CustomImagePath;
            BackGroundImageNew.Save(HttpContext.Current.Server.MapPath(CustomImagePath + fileName), ImageFormat.Jpeg);
            //Start Thumbnail Image Section
            System.Drawing.Image ThumbnailImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CustomImagePath + fileName));
            Bitmap BitThumbnailImage = new Bitmap(ThumbnailImage, new Size(536, 335));
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImagePath));
            }
            BitThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImagePath + fileName), ImageFormat.Jpeg);
            if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName)))
            {
                File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            }
            File.Copy(HttpContext.Current.Server.MapPath(CustomImagePath + fileName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            return objSaveImagedetails;
        }

      [WebGet(ResponseFormat = WebMessageFormat.Json)]
      public string ProcessFiles_Slider(string FilePath, string SliderID, string SliderItemID)
        {
            string Retval = "";
            try
            {
                string serverPath = HttpContext.Current.Server.MapPath(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1));
                string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
                string destFile = "";
                if (!Directory.Exists(serverPath + "/ProcessedImages/"))
                {
                    Directory.CreateDirectory(serverPath + "/ProcessedImages/");
                }
                if (!Directory.Exists(serverPath + "/ProcessedImages/" + SliderID + "/"))
                {
                    Directory.CreateDirectory(serverPath + "/ProcessedImages/" + SliderID + "/");
                }
                if (SliderItemID != null && SliderItemID != "")
                {
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + SliderID + "/" + SliderItemID + "/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + SliderID + "/" + SliderItemID + "/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + SliderID + "/" + SliderItemID + "/" + "Resize/" + "Original/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + SliderID + "/" + SliderItemID + "/" + "Resize/" + "Original/");
                    }
                    destFile = System.IO.Path.Combine(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1) + "ProcessedImages/" + SliderID + "/" + SliderItemID + "/" + "Resize/" + "Original/", fileName);
                }
                else
                {
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + SliderID + "/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + SliderID + "/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + SliderID + "/" + "Resize/" + "Original/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + SliderID + "/" + "Resize/" + "Original/");
                    }
                    destFile = System.IO.Path.Combine(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1) + "ProcessedImages/" + "Resize/" + "Original/", fileName);
                }
                string sourceFile = System.IO.Path.Combine(FilePath);
                File.Copy(HttpContext.Current.Server.MapPath(sourceFile), HttpContext.Current.Server.MapPath(destFile), true);
                Retval = destFile;
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }
            #endregion

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string ProcessFiles(string FilePath, string pageID, string CMSID)
        {
            string Retval = "";
            try
            {
                string serverPath = HttpContext.Current.Server.MapPath(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1));
                string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
                string destFile = "";
                if (!Directory.Exists(serverPath + "/ProcessedImages/"))
                {
                    Directory.CreateDirectory(serverPath + "/ProcessedImages/");
                }
                if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/"))
                {
                    Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/");
                }
                if (CMSID != null && CMSID != "")
                {
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/" + CMSID + "/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/" + CMSID + "/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/" + CMSID + "/" + "Resize/" + "Original/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/" + CMSID + "/" + "Resize/" + "Original/");
                    }
                     destFile = System.IO.Path.Combine(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1) + "ProcessedImages/" + pageID + "/" + CMSID + "/" + "Resize/" + "Original/", fileName);
                }
                else
                {
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/"  + "Resize/" + "Original/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/" + "Resize/" + "Original/");
                    }
                     destFile = System.IO.Path.Combine(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1) + "ProcessedImages/" +  "Resize/" + "Original/", fileName);
                }
                string sourceFile = System.IO.Path.Combine(FilePath);
                File.Copy(HttpContext.Current.Server.MapPath(sourceFile), HttpContext.Current.Server.MapPath(destFile), true);
                Retval = destFile;
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails CropImageWithResize(string FilePath, string ImageHeight, string ImageWidth, string XAxis, string YAxis, string CropImageWidth, string CropImageHeight, string BlogId, string userID)
        {
            Cls_SaveimageDetails objSaveImageDetails = new Cls_SaveimageDetails();
            string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1).Replace("/Resize/", "/Crop/");
            string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            System.Drawing.Image resized = Bitmap.FromFile(HttpContext.Current.Server.MapPath(FilePath));
            Bitmap NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
            string OriginalPath = serverPath;
            string NewImagePath = serverPath.Replace("/Original/", "/NewImage/");
            string CropImagePath = serverPath.Replace("/Original/", "/Croped/");
            string ThumbnailImage = serverPath.Replace("/Original/", "/Thumbnail/");
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(NewImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(NewImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CropImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CropImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImage)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImage));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            Bitmap OriginalImage = new Bitmap(resized);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + fileName), ImageFormat.Jpeg);
            NewImage.Save(HttpContext.Current.Server.MapPath(NewImagePath + fileName), ImageFormat.Jpeg);
            System.Drawing.Image orgImg = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(NewImagePath + fileName));
            Rectangle CropArea = new Rectangle(
                Convert.ToInt32(XAxis) * 6,
                Convert.ToInt32(YAxis) * 6,
                Convert.ToInt32(CropImageWidth) * 6,
                Convert.ToInt32(CropImageHeight) * 6);

            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
            using (Graphics g = Graphics.FromImage(bitMap))
            {
                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
            }
            bitMap.Save(HttpContext.Current.Server.MapPath(CropImagePath + fileName), ImageFormat.Jpeg);
            //ThumnailImage Create
            HttpContext.Current.Server.MapPath(NewImagePath + fileName);
            System.Drawing.Image ThumbImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CropImagePath + fileName));
            Bitmap BitThumbImage = new Bitmap(ThumbImage, new Size(536, 335));
            BitThumbImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImage + fileName), ImageFormat.Jpeg);
            objSaveImageDetails.filename = fileName;
            objSaveImageDetails.filepath = CropImagePath;
            if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName)))
            {
                File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            }
            File.Copy(HttpContext.Current.Server.MapPath(CropImagePath + fileName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            return objSaveImageDetails;
        }

        //Resize Image Save
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails ResizeImage(string FilePath, string RatioStatus, string ImageHeight, string ImageWidth, string pageID, string CMSID)
        {
            string ImagePath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1);
            string ImageName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            System.Drawing.Image resized = Bitmap.FromFile(HttpContext.Current.Server.MapPath(ImagePath + ImageName));
            if (string.IsNullOrEmpty(ImageHeight) || string.IsNullOrEmpty(ImageWidth))
            {
                ImageHeight = "150";
                ImageWidth = "450";
            }
            else
            {
            }
            if (RatioStatus == "True")
            {
                var ExpectedWidth = ImageWidth;
                var ExpectedHeight = ImageHeight;
                var ratioX = (double)Convert.ToInt16(ExpectedWidth) / resized.Width;
                var ratioY = (double)Convert.ToInt16(ExpectedHeight) / resized.Height;
                var ratio = Math.Max(ratioX, ratioY);
                var newWidth = (int)(resized.Width * ratio);
                var newHeight = (int)(resized.Height * ratio);
                Bitmap NewImage = new Bitmap(resized, new Size(newWidth, newHeight));
                string ResizeImagePath = ImagePath.Replace("Original", "ResizedImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ResizeImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ResizeImagePath));
                }
                NewImage.Save(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName), ImageFormat.Jpeg);
                //Thumbnail Image Section
                Bitmap ThumbnailImage = new Bitmap(resized, new Size(536, 335));
                string ThumnailImagePath = ImagePath.Replace("Original", "ThumbNailImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
                }
                ThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + ImageName), ImageFormat.Jpeg);
                Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
                objSaveImagedetails.filename = ImageName;
                objSaveImagedetails.filepath = ThumnailImagePath;
                objSaveImagedetails.imageheight = Convert.ToString(newHeight);
                objSaveImagedetails.imagewidth = Convert.ToString(newWidth);
                if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName));
                }
                File.Copy(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName));
                return objSaveImagedetails;
            }
            else
            {
                var ExpecedWidth = ImageWidth;
                var ExpectedHeight = ImageHeight;
                Bitmap NewImage = null;
                if (!string.IsNullOrEmpty(ImageHeight) && Convert.ToInt32(ImageHeight) > 0)
                {
                    NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
                }
                else
                {
                    NewImage = new Bitmap(resized, new Size(450, 150));
                }
                string ResizeImagePath = ImagePath.Replace("Original", "ResizedImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ResizeImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ResizeImagePath));
                }
                NewImage.Save(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName), ImageFormat.Jpeg);
                //Start Thumbnail Image Section
                Bitmap ThumbnailImage = new Bitmap(resized, new Size(536, 335));
                string ThumnailImagePath = ImagePath.Replace("Original", "ThumbNailImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
                }
                ThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + ImageName), ImageFormat.Jpeg);
                //End Thumbnail Image Section
                Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
                objSaveImagedetails.filename = ImageName;
                objSaveImagedetails.filepath = ResizeImagePath;
                objSaveImagedetails.imageheight = Convert.ToString(ExpectedHeight);
                objSaveImagedetails.imagewidth = Convert.ToString(ExpecedWidth);
                if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName));
                }
                File.Copy(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName));
                return objSaveImagedetails;
            }
        }


        //Custom Image Create
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails Custome_Image_Create(string FilePath, Int32 R, Int32 G, Int32 B, string BlogID, string userID)
        {
            Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
            string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1).Replace("/Resize/", "/CustomImage/");
            string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            //Background Image Path
            string BackGroundImage = HttpContext.Current.Server.MapPath("~/images/BackGroundImage/BackGroundImage.jpg");
            //InsertedImage  Image Path
            string InsertedImage = HttpContext.Current.Server.MapPath(FilePath);
            System.Drawing.Image BackGroundImageNew = Bitmap.FromFile(BackGroundImage);
            Graphics gra = Graphics.FromImage(BackGroundImageNew);
            gra.Clear(Color.FromArgb(R, G, B));
            System.Drawing.Image InsertedImageNew = Bitmap.FromFile(InsertedImage);
            Bitmap logo = new Bitmap(InsertedImageNew, new Size(InsertedImageNew.Width, InsertedImageNew.Height));
            //Background Image Height and Width
            int backImageheight = BackGroundImageNew.Height;
            int backImageWidth = BackGroundImageNew.Width;
            //Inserted Image Height and Width
            int FrontImageHeight = InsertedImageNew.Height;
            int FrontImageWidth = InsertedImageNew.Width;
            //Calculate XPosition
            int xPostition = (backImageWidth - FrontImageWidth) / 2;
            //Calculate YPosition
            int yPostition = (backImageheight - FrontImageHeight) / 2;
            gra.DrawImage(logo, new Rectangle(new Point(xPostition, yPostition), new Size(FrontImageWidth, FrontImageHeight)));
            string OriginalPath = serverPath;
            string CustomImagePath = serverPath.Replace("/Original/", "/Customed/");
            string ThumbnailImagePath = serverPath.Replace("/Original/", "/Thumbnail/");
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CustomImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CustomImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImagePath));
            }
            Bitmap OriginalImage = new Bitmap(InsertedImageNew);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + fileName));
            objSaveImagedetails.filename = fileName;
            objSaveImagedetails.filepath = CustomImagePath;
            BackGroundImageNew.Save(HttpContext.Current.Server.MapPath(CustomImagePath + fileName), ImageFormat.Jpeg);
            //Start Thumbnail Image Section
            System.Drawing.Image ThumbnailImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CustomImagePath + fileName));
            Bitmap BitThumbnailImage = new Bitmap(ThumbnailImage, new Size(536, 335));
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImagePath));
            }
            BitThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImagePath + fileName), ImageFormat.Jpeg);
            if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName)))
            {
                File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            }
            File.Copy(HttpContext.Current.Server.MapPath(CustomImagePath + fileName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            return objSaveImagedetails;
        }

        #endregion

        #region Image Editing Media management page
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string ProcessFiles_MediaManagementPage(string FilePath, string pageID)
        {
            string Retval = "";
            try
            {
                string serverPath = HttpContext.Current.Server.MapPath(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1));
                string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
                string destFile = "";
                if (!string.IsNullOrEmpty(pageID))
                {
                    if (!Directory.Exists(serverPath + "/ProcessedImages/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/");
                    }
                    if (!Directory.Exists(serverPath + "/ProcessedImages/" + pageID + "/" + "Resize/" + "Original/"))
                    {
                        Directory.CreateDirectory(serverPath + "/ProcessedImages/" + pageID + "/" + "Resize/" + "Original/");
                    }
                    destFile = System.IO.Path.Combine(FilePath.Substring(0, FilePath.LastIndexOf("/") + 1) + "ProcessedImages/" + "Resize/" + "Original/", fileName);
                    string sourceFile = System.IO.Path.Combine(FilePath);
                    File.Copy(HttpContext.Current.Server.MapPath(sourceFile), HttpContext.Current.Server.MapPath(destFile), true);
                    Retval = destFile;
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }


        //Resize Image Save
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails ResizeImage_MediaManagementPage(string FilePath, string RatioStatus, string ImageHeight, string ImageWidth, string pageID, string FileName, string Description, string AlternateText, string UserID)
        {
            string ImagePath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1);
            string ImageName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            System.Drawing.Image resized = Bitmap.FromFile(HttpContext.Current.Server.MapPath(ImagePath + ImageName));
             var FileExtention=System.IO.Path.GetExtension(ImageName);
            if (string.IsNullOrEmpty(ImageHeight) || string.IsNullOrEmpty(ImageWidth))
            {
                ImageHeight = "150";
                ImageWidth = "450";
            }
            else
            {
            }
            if (RatioStatus == "True")
            {
                var ExpectedWidth = ImageWidth;
                var ExpectedHeight = ImageHeight;
                var ratioX = (double)Convert.ToInt16(ExpectedWidth) / resized.Width;
                var ratioY = (double)Convert.ToInt16(ExpectedHeight) / resized.Height;
                var ratio = Math.Max(ratioX, ratioY);
                var newWidth = (int)(resized.Width * ratio);
                var newHeight = (int)(resized.Height * ratio);
                Bitmap NewImage = new Bitmap(resized, new Size(newWidth, newHeight));

                string ResizeImagePath = ImagePath.Replace("Original", "ResizedImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ResizeImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ResizeImagePath));
                }
                NewImage.Save(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName), ImageFormat.Jpeg);

                //Thumbnail Image Section
                Bitmap ThumbnailImage = new Bitmap(resized, new Size(536, 335));
                string ThumnailImagePath = ImagePath.Replace("Original", "ThumbNailImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
                }
                ThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + ImageName), ImageFormat.Jpeg);
                Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
                objSaveImagedetails.filename = ImageName;
                objSaveImagedetails.filepath = ThumnailImagePath;
                objSaveImagedetails.imageheight = Convert.ToString(newHeight);
                objSaveImagedetails.imagewidth = Convert.ToString(newWidth);
                if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName));
                }
                File.Copy(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + ImageName));
                return objSaveImagedetails;
            }
            else
            {
                var ExpecedWidth = ImageWidth;
                var ExpectedHeight = ImageHeight;
                string ExpectedSize = ImageWidth.ToString()+"X"+ImageHeight.ToString();
                Bitmap NewImage = null;
                if (!string.IsNullOrEmpty(ImageHeight) && Convert.ToInt32(ImageHeight) > 0)
                {
                    NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
                }
                else
                {
                    NewImage = new Bitmap(resized, new Size(450, 150));
                }
                string ResizeImagePath = ImagePath.Replace("Original", "ResizedImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ResizeImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ResizeImagePath));
                }
                NewImage.Save(HttpContext.Current.Server.MapPath(ResizeImagePath + ImageName.Replace(FileExtention, "").Replace("Org", ExpectedSize)) + FileExtention, ImageFormat.Jpeg);

                //Start Thumbnail Image Section
                Bitmap ThumbnailImage = new Bitmap(resized, new Size(536, 335));
                string ThumnailImagePath = ImagePath.Replace("Original", "ThumbNailImage");
                if (!File.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
                }
                ThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + ImageName), ImageFormat.Jpeg);
                //End Thumbnail Image Section
                Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
                objSaveImagedetails.filename = ImageName;
                objSaveImagedetails.filepath = ResizeImagePath;
                objSaveImagedetails.imageheight = Convert.ToString(ExpectedHeight);
                objSaveImagedetails.imagewidth = Convert.ToString(ExpecedWidth);
                // to delete previous image
                string NewImageName = ImageName.Replace(FileExtention, "").Replace("Org", ExpectedSize) + FileExtention;
                File.Copy(HttpContext.Current.Server.MapPath(ResizeImagePath + NewImageName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + NewImageName));
                Clsfileuploader fileUploader = new Clsfileuploader();
                fileUploader.filecategoryid = "1";
                fileUploader.id = pageID.ToString();
                // Location of image
                fileUploader.filepath = @"../UploadedFiles/image" + @"/";
                fileUploader.description = Description;
                fileUploader.alternatetext = AlternateText;
                fileUploader.uploadedby = UserID;
                fileUploader.modificationdate = DateTime.Now.ToString();
                fileUploader.filename = ImageName;
                //Insert the original image if image is edited
                ServicesFactory.DocCMSServices.Insert_MediaFiles(fileUploader);
                fileUploader.filename = NewImageName;
                // Update the old image without replacing
                ServicesFactory.DocCMSServices.Update_MediaFiles(fileUploader);
                return objSaveImagedetails;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails CropImageWithResize_MediaManagementPage(string FilePath, string ImageHeight, string ImageWidth, string XAxis, string YAxis, string CropImageWidth, string CropImageHeight)
        {
            Cls_SaveimageDetails objSaveImageDetails = new Cls_SaveimageDetails();
            string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1).Replace("/Resize/", "/Crop/");
            string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            System.Drawing.Image resized = Bitmap.FromFile(HttpContext.Current.Server.MapPath(FilePath));
            Bitmap NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
            string OriginalPath = serverPath;
            string NewImagePath = serverPath.Replace("/Original/", "/NewImage/");
            string CropImagePath = serverPath.Replace("/Original/", "/Croped/");
            string ThumbnailImage = serverPath.Replace("/Original/", "/Thumbnail/");
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(NewImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(NewImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CropImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CropImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImage)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImage));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            Bitmap OriginalImage = new Bitmap(resized);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + fileName), ImageFormat.Jpeg);
            NewImage.Save(HttpContext.Current.Server.MapPath(NewImagePath + fileName), ImageFormat.Jpeg);
            System.Drawing.Image orgImg = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(NewImagePath + fileName));
            Rectangle CropArea = new Rectangle(
                Convert.ToInt32(XAxis) * 6,
                Convert.ToInt32(YAxis) * 6,
                Convert.ToInt32(CropImageWidth) * 6,
                Convert.ToInt32(CropImageHeight) * 6);

            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
            using (Graphics g = Graphics.FromImage(bitMap))
            {
                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
            }
            bitMap.Save(HttpContext.Current.Server.MapPath(CropImagePath + fileName), ImageFormat.Jpeg);
            //ThumnailImage Create
            HttpContext.Current.Server.MapPath(NewImagePath + fileName);
            System.Drawing.Image ThumbImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CropImagePath + fileName));
            Bitmap BitThumbImage = new Bitmap(ThumbImage, new Size(536, 335));
            BitThumbImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImage + fileName), ImageFormat.Jpeg);
            objSaveImageDetails.filename = fileName;
            objSaveImageDetails.filepath = CropImagePath;
            if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName)))
            {
                File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            }
            File.Copy(HttpContext.Current.Server.MapPath(CropImagePath + fileName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            return objSaveImageDetails;
        }

        //Custom Image Create
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails Custome_Image_Create_MediaManagementPage(string FilePath, Int32 R, Int32 G, Int32 B, string userID)
        {
            Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
            string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1).Replace("/Resize/", "/CustomImage/");
            string fileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
            string FinalImageSavePath = FilePath.Substring(0, FilePath.LastIndexOf("ProcessedImages") - 1);
            //Background Image Path
            string BackGroundImage = HttpContext.Current.Server.MapPath("~/images/BackGroundImage/BackGroundImage.jpg");
            //InsertedImage  Image Path
            string InsertedImage = HttpContext.Current.Server.MapPath(FilePath);
            System.Drawing.Image BackGroundImageNew = Bitmap.FromFile(BackGroundImage);
            Graphics gra = Graphics.FromImage(BackGroundImageNew);
            gra.Clear(Color.FromArgb(R, G, B));
            System.Drawing.Image InsertedImageNew = Bitmap.FromFile(InsertedImage);
            Bitmap logo = new Bitmap(InsertedImageNew, new Size(InsertedImageNew.Width, InsertedImageNew.Height));
            //Background Image Height and Width
            int backImageheight = BackGroundImageNew.Height;
            int backImageWidth = BackGroundImageNew.Width;
            //Inserted Image Height and Width
            int FrontImageHeight = InsertedImageNew.Height;
            int FrontImageWidth = InsertedImageNew.Width;

            //Calculate XPosition
            int xPostition = (backImageWidth - FrontImageWidth) / 2;
            //Calculate YPosition
            int yPostition = (backImageheight - FrontImageHeight) / 2;
            gra.DrawImage(logo, new Rectangle(new Point(xPostition, yPostition), new Size(FrontImageWidth, FrontImageHeight)));
            string OriginalPath = serverPath;
            string CustomImagePath = serverPath.Replace("/Original/", "/Customed/");
            string ThumbnailImagePath = serverPath.Replace("/Original/", "/Thumbnail/");
            if (!File.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CustomImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CustomImagePath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImagePath));
            }
            Bitmap OriginalImage = new Bitmap(InsertedImageNew);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + fileName));
            objSaveImagedetails.filename = fileName;
            objSaveImagedetails.filepath = CustomImagePath;
            BackGroundImageNew.Save(HttpContext.Current.Server.MapPath(CustomImagePath + fileName), ImageFormat.Jpeg);
            //Start Thumbnail Image Section
            System.Drawing.Image ThumbnailImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CustomImagePath + fileName));
            Bitmap BitThumbnailImage = new Bitmap(ThumbnailImage, new Size(536, 335));

            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumbnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImagePath));
            }
            BitThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImagePath + fileName), ImageFormat.Jpeg);
            if (File.Exists(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName)))
            {
                File.Delete(HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            }
            File.Copy(HttpContext.Current.Server.MapPath(CustomImagePath + fileName), HttpContext.Current.Server.MapPath(FinalImageSavePath + "/" + fileName));
            return objSaveImagedetails;
       }

        #endregion

        #region Video Convertion
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_Video Convert_Video(string FilePath, string Formattype, string pageID, string CMSID)
        {
            try
            {
                Cls_Video objCls_Video = new Cls_Video();
                string serverPath = FilePath.Substring(0, FilePath.LastIndexOf("/") + 1);
                string OldFileName = FilePath.Split('/')[FilePath.Split('/').Length - 1].ToString();
                string fileName = OldFileName.Substring(0, OldFileName.LastIndexOf(".") + 1);
                string OutputVideoFile = "";
                string VideoThumbnail = "";
                string VideoPath = serverPath + "ProcessedVideo/" + pageID + "/" + CMSID + "/" + "VideoConvert" + "/";
                string VideoCoverImage = serverPath + "ProcessedVideo/" + pageID + "/" + CMSID + "/" + "VideoCoverImage" + "/";
                if (!File.Exists(HttpContext.Current.Server.MapPath(VideoPath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(VideoPath));
                }
                if (!File.Exists(HttpContext.Current.Server.MapPath(VideoCoverImage)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(VideoCoverImage));
                }
                if (VideoPath != null && VideoPath != "" && fileName != "" && fileName != null)
                {
                    OutputVideoFile = HttpContext.Current.Server.MapPath(VideoPath + fileName + "mp4");
                    VideoThumbnail = HttpContext.Current.Server.MapPath(VideoCoverImage + fileName + "jpg");
                    objCls_Video.videoname = fileName;
                }
                else
                {
                    string Datetime = DateTime.Now.ToString("d").Replace("/", "-");
                    OutputVideoFile = HttpContext.Current.Server.MapPath(VideoPath + fileName + "mp4");
                    VideoThumbnail = HttpContext.Current.Server.MapPath(VideoCoverImage + fileName + "jpg");
                    objCls_Video.videoname = fileName;
                }
                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                if (Formattype == "mp4")
                {
                    ffMpeg.ConvertMedia(HttpContext.Current.Server.MapPath(FilePath), OutputVideoFile, Format.mp4);
                }
                else if (Formattype == "mpeg")
                {
                    ffMpeg.ConvertMedia(HttpContext.Current.Server.MapPath(FilePath), OutputVideoFile, Format.mpeg);
                }
                else
                {
                    ffMpeg.ConvertMedia(HttpContext.Current.Server.MapPath(FilePath), OutputVideoFile, Format.mp4);
                }
                ffMpeg.GetVideoThumbnail(OutputVideoFile, VideoThumbnail);
                objCls_Video.videopath = VideoPath + fileName + "mp4";
                objCls_Video.coverimagePath = VideoCoverImage + fileName + "jpg";
                objCls_Video.oldfilepath = FilePath;
                objCls_Video.returnvalue = "Success";
                return objCls_Video;
            }
            catch (Exception ex)
            {
                Cls_Video objCls_Video = new Cls_Video();
                return objCls_Video;
            }
        }
        #endregion

        #region Size
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Insert_Product_Size(string Size, string CMSID)
        {
            Int32 Retval = 0;
            string RetvalStr = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Insert_size(Size, CMSID);
                if (Retval > 0)
                {
                    RetvalStr = "Success";
                }
                else
                {
                    RetvalStr = "Error";
                }
                return RetvalStr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Product_Size(string CMSID)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                dtresult = ServicesFactory.DocCMSServices.Fetch_size(CMSID);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
       }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Remove_Product_Size(string SizeId)
        {
            Int32 Retval = 0;
            string RetString = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Delete_size(SizeId);
                if (Retval > 0)
                {
                    RetString = "Success";
                }
                else
                {
                    RetString = "Error";
                }
                return RetString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region color
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Insert_Color_Details(string CMSID, string Color, string ImageName)
        {
            Int32 Retval = 0;
            string RetvalStr = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Insert_color_Details(CMSID, Color, ImageName);
                if (Retval > 0)
                {
                    RetvalStr = "Success";
                }
                else
                {
                    RetvalStr = "Error";
                }
                return RetvalStr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Update_Color_Image(string ColorId, string CMSID, string Color, string ImageName)
        {
            Int32 Retval = 0;
            string RetvalStr = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Update_color_image(ColorId, CMSID, Color, ImageName);
                if (Retval > 0)
                {
                    RetvalStr = "Success";
                }
                else
                {
                    RetvalStr = "Error";
                }
                return RetvalStr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Color_Details(string CMSID)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                dtresult = ServicesFactory.DocCMSServices.Fetch_color_Details(CMSID);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Color_By_ColorId(string ColorId)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                Retval = ServicesFactory.DocCMSServices.Fetch_color_By_colorid(ColorId);
                return Retval;
            }
            catch
            {
                return Retval;
            }
       }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Color_Details_By_ColorId(string ColorId)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                dtresult = ServicesFactory.DocCMSServices.Fetch_color_Details_By_colorid(ColorId);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Remove_Product_Color(string ColorId)
        {
            Int32 Retval = 0;
            string RetString = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Delete_color(ColorId);
                if (Retval > 0)
                {
                    RetString = "Success";
                }
                else
                {
                    RetString = "Error";
                }
                return RetString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Shopping Cart
        [OperationContract]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Int32 Insert_Shopping_Temp_Cart_Detail(Cls_ShopingCart Cls_ShopingCart)
        {
            Int32 CheckCount = 0;
            Int32 Retval = 0;
            try
            {
                if (Cls_ShopingCart != null)
                {
                    CheckCount = ServicesFactory.DocCMSServices.CheckCount_For_Duplicate_ShoppingCart(Cls_ShopingCart);
                    if (CheckCount > 0)
                    {
                        Retval = 0;
                    }
                    else
                    {
                        Retval = ServicesFactory.DocCMSServices.Insert_Shopping_Temp_Cart_Detail(Cls_ShopingCart);
                    }
                }
                else
                {
                    Retval = 0;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationContract]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Int32 Insert_New_Product_fromCMS(Cls_OrderDetail Cls_OrderDetail)
        {
            Int32 CheckCount = 0;
            Int32 Retval = 0;
            try
            {
                if (Cls_OrderDetail != null)
                {
                    CheckCount = ServicesFactory.DocCMSServices.CheckCount_For_Duplicate_Item_WhileAdding_From_CMS(Cls_OrderDetail);
                    if (CheckCount > 0)
                    {
                        Retval = 0;
                    }
                    else
                    {
                        Retval = ServicesFactory.DocCMSServices.Insert_New_Product_Detail_toOrderDetail(Cls_OrderDetail);
                        if (Retval > 0)
                        {
                            ServicesFactory.DocCMSServices.Insert_New_Product_Detail_toQuoteDetail(Cls_OrderDetail, Retval);
                        }
                    }
                }
                else
                {
                    Retval = 0;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [OperationContract]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Int32 Insert_New_Product_fromCMS2(Cls_OrderDetail Cls_OrderDetail)
        {
            Int32 CheckCount = 0;
            Int32 Retval = 0;
            try
            {
                if (Cls_OrderDetail != null)
                {
                    CheckCount = ServicesFactory.DocCMSServices.CheckCount_For_Duplicate_Item_WhileAdding_From_CMS(Cls_OrderDetail);
                    if (CheckCount > 0)
                    {
                        Retval = 0;
                    }
                    else
                    {
                        Retval = ServicesFactory.DocCMSServices.Insert_New_Product_Detail_toOrderDetail(Cls_OrderDetail);
                    }
                }
                else
                {
                    Retval = 0;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Shopping_Temp_Cart_Detail(string SessionID)
        {
            string Retval = "";
            try
            {
                DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_Shopping_Temp_Cart_Detail(SessionID);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Delete_Shopping_cart_By_Id(string Id)
        {
            string Retval = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Delete_Shopping_cart_By_id(Convert.ToInt32(Id));
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Delete_Shopping_cart(string ShoppingCartId)
        {
            Int32 Retval = 0;
            try
            {
                Retval = ServicesFactory.DocCMSServices.Delete_Temp_Shopping_Detail_By_ShoppingCartid(ShoppingCartId);
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Shopping_Total(string SessionID)
        {
            string Retval = "0";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Fetch_Shopping_Total(SessionID);
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Update_Shopping_cart_By_Id(string Id, string Qty, string RunningPrice)
        {
            string Retval = "";
            try
            {
                Retval = ServicesFactory.DocCMSServices.Update_Shopping_cart_By_id(Convert.ToInt32(Id), Convert.ToInt32(Qty), Convert.ToDecimal(RunningPrice));
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Data_Show_By_Using_Roles(Int32 RoleId, Int32 ModuleId)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                dtresult = ServicesFactory.DocCMSServices.Fetch_privileges_By_roleid(RoleId, ModuleId);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Roles(Int32 RoleID)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                DataTable dtModules = ServicesFactory.DocCMSServices.Fetch_Modules_For_Grid(RoleID);
                if (dtModules != null && dtModules.Rows.Count > 0)
                {
                    Retval = GetJson(dtModules);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }


        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Privileges_By_RoleId(Int32 RoleId, Int32 ModuleId, string MasterRole)
        {
            string Retval = "";
            DataTable dtSubmodule = new DataTable();
            try
            {
                dtSubmodule = ServicesFactory.DocCMSServices.Fetch_privileges_By_roleid(Convert.ToInt32(MasterRole), Convert.ToInt32(ModuleId));
                if (dtSubmodule != null && dtSubmodule.Rows.Count > 0)
                {
                    Retval = GetJson(dtSubmodule);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

      
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public void DeleteImage(string ImageName)
        {
            try
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/TempImage/" + ImageName)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~/TempImage/" + ImageName));
                }
                ServicesFactory.DocCMSServices.Deleteimage(ImageName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Save Crop Image 
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails SaveCropImageForBlog(string filepath, string fileName, string XAxis, string YAxis, string Width, string Height, string BlogID, string userID)
        {
            Cls_SaveimageDetails objSaveImageDetails = new Cls_SaveimageDetails();
            if (XAxis == "" && YAxis == "" && Width == "" && Height == "")
            {
                return objSaveImageDetails;
            }
            string filePath = Path.Combine(HttpContext.Current.Server.MapPath(filepath), fileName);
            string cropFileName = "";
            string cropFilePath = "";
            string originalFile = "";
            string originalFilePath = "";
            if (!string.IsNullOrEmpty(fileName.Trim()))
            {
                string compFileName = Guid.NewGuid().ToString();
            }
            if (File.Exists(filePath))
            {
                System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                Rectangle CropArea = new Rectangle(
                    Convert.ToInt32(XAxis),
                    Convert.ToInt32(YAxis),
                    Convert.ToInt32(Width),
                    Convert.ToInt32(Height));
                try
                {
                    Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                    using (Graphics g = Graphics.FromImage(bitMap))
                    {
                        g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                    }
                    cropFileName = Guid.NewGuid().ToString() + ".jpg";
                    cropFilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/TempImage/Crop/CropImage/" + BlogID + "/" + userID + "/"), cropFileName);
                    string cropDirectoryCheck = HttpContext.Current.Server.MapPath("~/TempImage/Crop/CropImage/" + BlogID + "/" + userID + "/");
                    originalFilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/TempImage/Crop/Original/" + BlogID + "/" + userID + "/"), cropFileName);
                    string OriginalDirectoryCheck = HttpContext.Current.Server.MapPath("~/TempImage/Crop/Original/" + BlogID + "/" + userID + "/");
                    if (!Directory.Exists(OriginalDirectoryCheck))
                    {
                        Directory.CreateDirectory(OriginalDirectoryCheck);
                    }
                    if (File.Exists(originalFilePath))
                    {
                        File.Delete(originalFilePath);
                    }
                    if (!Directory.Exists(cropDirectoryCheck))
                    {
                        Directory.CreateDirectory(cropDirectoryCheck);
                    }
                    if (File.Exists(cropFilePath))
                    {
                        File.Delete(cropFilePath);
                    }
                    File.Copy(filePath, originalFilePath, true);
                    bitMap.Save(cropFilePath);
                    objSaveImageDetails.filename = cropFileName;
                    objSaveImageDetails.imageheight = Height;
                    objSaveImageDetails.imagewidth = Width;
                    objSaveImageDetails.filepath = "/TempImage/Crop/CropImage/" + BlogID + "/" + userID + "/";
                    return objSaveImageDetails;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return objSaveImageDetails;
        }

        //Crop Image Save
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails UserInputImageSize(string ImageName, string RatioStatus, string ImageHeight, string ImageWidth, string BlogId, string userID)
        {
            string ResizeImageName = "Resize_" + ImageName;
            string ImagePath = "~/TempImage/" + ImageName;
            string Image = HttpContext.Current.Server.MapPath(ImagePath);
            System.Drawing.Image resized = Bitmap.FromFile(Image);
            if (string.IsNullOrEmpty(ImageHeight) || string.IsNullOrEmpty(ImageWidth))
            {
                ImageHeight = "150";
                ImageWidth = "450";
                ResizeImageName = Guid.NewGuid().ToString() + ".jpg";
            }
            else
            {
                ResizeImageName = Guid.NewGuid().ToString() + ".jpg";
            }
            string OrignalImagePath = "~/TempImage/ResizeImage/Original/" + BlogId + "/" + userID + "/";
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(OrignalImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OrignalImagePath));
            }
            resized.Save(HttpContext.Current.Server.MapPath("~/TempImage/ResizeImage/Original/" + BlogId + "/" + userID + "/" + ResizeImageName), ImageFormat.Jpeg);
            if (RatioStatus == "True")
            {
                var ExpectedWidth = ImageWidth;
                var ExpectedHeight = ImageHeight;
                var ratioX = (double)Convert.ToInt16(ExpectedWidth) / resized.Width;
                var ratioY = (double)Convert.ToInt16(ExpectedHeight) / resized.Height;
                var ratio = Math.Max(ratioX, ratioY);
                var newWidth = (int)(resized.Width * ratio);
                var newHeight = (int)(resized.Height * ratio);
                Bitmap NewImage = new Bitmap(resized, new Size(newWidth, newHeight));
                string ResizeImagePath = "~/TempImage/ResizeImage/ResizedImage/" + BlogId + "/" + userID + "/";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ResizeImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ResizeImagePath));
                }
                NewImage.Save(HttpContext.Current.Server.MapPath("~/TempImage/ResizeImage/ResizedImage/" + BlogId + "/" + userID + "/" + ResizeImageName), ImageFormat.Jpeg);
                //Thumbnail Image Section
                Bitmap ThumbnailImage = new Bitmap(resized, new Size(536, 335));
                string ThumnailImagePath = "~/TempImage/ResizeImage/Thumbnail/" + BlogId + "/" + userID + "/";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
                }
                ThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + ResizeImageName), ImageFormat.Jpeg);
                Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
                objSaveImagedetails.filename = ResizeImageName;
                objSaveImagedetails.filepath = ResizeImagePath;
                objSaveImagedetails.imageheight = Convert.ToString(newHeight);
                objSaveImagedetails.imagewidth = Convert.ToString(newWidth);
                return objSaveImagedetails;
            }
            else
            {
                var ExpecedWidth = ImageWidth;
                var ExpectedHeight = ImageHeight;
                Bitmap NewImage = null;
                if (!string.IsNullOrEmpty(ImageHeight) && Convert.ToInt32(ImageHeight) > 0)
                {
                    NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
                }
                else
                {
                    NewImage = new Bitmap(resized, new Size(450, 150));
                }
                string ResizeImagePath = "~/TempImage/ResizeImage/ResizedImage/" + BlogId + "/" + userID + "/";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ResizeImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ResizeImagePath));
                }
                NewImage.Save(HttpContext.Current.Server.MapPath("~/TempImage/ResizeImage/ResizedImage/" + BlogId + "/" + userID + "/" + ResizeImageName), ImageFormat.Jpeg);
                //Start Thumbnail Image Section
                Bitmap ThumbnailImage = new Bitmap(resized, new Size(536, 335));
                string ThumnailImagePath = "~/TempImage/ResizeImage/Thumbnail/" + BlogId + "/" + userID + "/";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
                }
                ThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + ResizeImageName), ImageFormat.Jpeg);
                //End Thumbnail Image Section
                Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
                objSaveImagedetails.filename = ResizeImageName;
                objSaveImagedetails.filepath = ResizeImagePath;
                objSaveImagedetails.imageheight = Convert.ToString(ExpectedHeight);
                objSaveImagedetails.imagewidth = Convert.ToString(ExpecedWidth);
                return objSaveImagedetails;
            }
        }

        //Custome Image Create
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails Custome_Image_Create_forBlog(string ImageName, Int32 R, Int32 G, Int32 B, string BlogID, string userID)
        {
            Cls_SaveimageDetails objSaveImagedetails = new Cls_SaveimageDetails();
            //Background Image Path
            string BackGroundImage = HttpContext.Current.Server.MapPath("~/images/BackGroundImage/BackGroundImage.jpg");
            //InsertedImage  Image Path
            string InsertedImage = HttpContext.Current.Server.MapPath("~/TempImage/" + ImageName);
            System.Drawing.Image BackGroundImageNew = Bitmap.FromFile(BackGroundImage);
            Graphics gra = Graphics.FromImage(BackGroundImageNew);
            gra.Clear(Color.FromArgb(R, G, B));
            System.Drawing.Image InsertedImageNew = Bitmap.FromFile(InsertedImage);
            Bitmap logo = new Bitmap(InsertedImageNew, new Size(InsertedImageNew.Width, InsertedImageNew.Height));
            //Background Image Height and Width
            int backImageheight = BackGroundImageNew.Height;
            int backImageWidth = BackGroundImageNew.Width;
            //Inserted Image Height and Width
            int FrontImageHeight = InsertedImageNew.Height;
            int FrontImageWidth = InsertedImageNew.Width;
            //Calculate XPosition
            int xPostition = (backImageWidth - FrontImageWidth) / 2;
            //Calculate YPosition
            int yPostition = (backImageheight - FrontImageHeight) / 2;
            gra.DrawImage(logo, new Rectangle(new Point(xPostition, yPostition), new Size(FrontImageWidth, FrontImageHeight)));
            string ImagePath = "~/TempImage/CustomImage/Original/" + BlogID + "/" + userID + "/";
            if (!File.Exists(HttpContext.Current.Server.MapPath(ImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ImagePath));
            }
            HttpContext.Current.Response.ContentType = "image/JPEG";
            string CreateImageName = Guid.NewGuid().ToString() + ".jpg";
            string ImageOriginalPath = "~/TempImage/CustomImage/Original/" + BlogID + "/" + userID + "/";
            string CustomImagePath = "~/TempImage/CustomImage/Customed/" + BlogID + "/" + userID + "/";
            if (!File.Exists(HttpContext.Current.Server.MapPath(ImageOriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ImageOriginalPath));
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath(CustomImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CustomImagePath));
            }
            Bitmap OriginalImage = new Bitmap(InsertedImageNew);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(ImageOriginalPath + CreateImageName));
            objSaveImagedetails.filename = CreateImageName;
            objSaveImagedetails.filepath = "~/TempImage/CustomImage/Customed/" + BlogID + "/" + userID + "/";
            BackGroundImageNew.Save(HttpContext.Current.Server.MapPath(CustomImagePath + CreateImageName), ImageFormat.Jpeg);
            //Start Thumbnail Image Section
            System.Drawing.Image ThumbnailImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CustomImagePath + CreateImageName));
            Bitmap BitThumbnailImage = new Bitmap(ThumbnailImage, new Size(536, 335));
            string ThumnailImagePath = "~/TempImage/CustomImage/Thumbnail/" + BlogID + "/" + userID + "/";
            if (!File.Exists(HttpContext.Current.Server.MapPath(ThumnailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumnailImagePath));
            }
            BitThumbnailImage.Save(HttpContext.Current.Server.MapPath(ThumnailImagePath + CreateImageName), ImageFormat.Jpeg);
            //End Thumbnail Image Section
            return objSaveImagedetails;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Cls_SaveimageDetails CropImageWithResize_ForBlog(string ImageName, string ImageHeight, string ImageWidth, string XAxis, string YAxis, string CropImageWidth, string CropImageHeight, string BlogId, string userID)
        {
            Cls_SaveimageDetails objSaveImageDetails = new Cls_SaveimageDetails();
            string ImagePath = "~/TempImage/" + ImageName;
            string Image = HttpContext.Current.Server.MapPath(ImagePath);
            string CreateImageName = Guid.NewGuid().ToString() + ".jpg";
            System.Drawing.Image resized = Bitmap.FromFile(Image);
            Bitmap NewImage = new Bitmap(resized, new Size(Convert.ToInt32(ImageWidth), Convert.ToInt32(ImageHeight)));
            string OriginalPath = "~/TempImage/Crop/Original/" + BlogId + "/" + userID + "/";
            string NewImagePath = "~/TempImage/Crop/NewImage/" + BlogId + "/" + userID + "/";
            string CropImagePath = "~/TempImage/Crop/Croped/" + BlogId + "/" + userID + "/";
            string ThumbnailImage = "~/TempImage/Crop/Thumbnail/" + BlogId + "/" + userID + "/";
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(NewImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(NewImagePath));
            }
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(CropImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(CropImagePath));
            }
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(ThumbnailImage)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbnailImage));
            }
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(OriginalPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OriginalPath));
            }
            Bitmap OriginalImage = new Bitmap(resized);
            OriginalImage.Save(HttpContext.Current.Server.MapPath(OriginalPath + CreateImageName), ImageFormat.Jpeg);
            NewImage.Save(HttpContext.Current.Server.MapPath(NewImagePath + CreateImageName), ImageFormat.Jpeg);
            System.Drawing.Image orgImg = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(NewImagePath + CreateImageName));
            Rectangle CropArea = new Rectangle(
                Convert.ToInt32(XAxis) * 6,
                Convert.ToInt32(YAxis) * 6,
                Convert.ToInt32(CropImageWidth) * 6,
                Convert.ToInt32(CropImageHeight) * 6);

            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
            using (Graphics g = Graphics.FromImage(bitMap))
            {
                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
            }
            bitMap.Save(HttpContext.Current.Server.MapPath(CropImagePath + CreateImageName), ImageFormat.Jpeg);
            //ThumnailImage Create
            HttpContext.Current.Server.MapPath(NewImagePath + CreateImageName);
            System.Drawing.Image ThumbImage = Bitmap.FromFile(HttpContext.Current.Server.MapPath(CropImagePath + CreateImageName));
            Bitmap BitThumbImage = new Bitmap(ThumbImage, new Size(Convert.ToInt32(200), Convert.ToInt32(150)));
            BitThumbImage.Save(HttpContext.Current.Server.MapPath(ThumbnailImage + CreateImageName), ImageFormat.Jpeg);
            objSaveImageDetails.filename = CreateImageName;
            objSaveImageDetails.filepath = CropImagePath;
            return objSaveImageDetails;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string DeleteVideo(string VideoName, string MemberId)
        {
            try
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/TempVideo/" + VideoName)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~/TempVideo/" + VideoName));
                    ServicesFactory.DocCMSServices.Delete_Temp_Video(VideoName, MemberId);
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string VideosList(string MemberId)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                dtresult = ServicesFactory.DocCMSServices.Fetch_All_Temp_VideosList(MemberId);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        // Insert comment
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Insert_Comment_Details(Int32 BlogId, string Comment, string AccessType)
        {
            try
            {
                Int64 Retval = 0;
                string RetvalString = "";
                Cls_comment objComment = new Cls_comment();
                objComment.blogid = BlogId;
                objComment.comment = Comment;
                objComment.accesstype = AccessType;
                if (HttpContext.Current.Session["MemberID"] != null && Convert.ToString(HttpContext.Current.Session["MemberID"]) != "")
                {
                    objComment.memberid = Convert.ToString(HttpContext.Current.Session["MemberID"]);
                    Retval = ServicesFactory.DocCMSServices.Insert_comments(objComment);
                    if (Retval > 0)
                    {
                        RetvalString = "Success";
                    }
                    else
                    {
                        RetvalString = "Error";
                    }
                }
                else
                {
                    RetvalString = Convert.ToString(BlogId);
                }
                return RetvalString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_Comment_Based_On_BlogId(Int32 BlogId)
        {
            string Retval = "";
            DataTable dtresult = new DataTable();
            try
            {
                dtresult = ServicesFactory.DocCMSServices.Fetch_comment_Based_On_blogid(BlogId);
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public ResponseMemberLogin MemberIdCheck(string MemberID, string BlogId)
        {
            string Retvalue = "";
            ResponseMemberLogin objResponseMemberLogin = new ResponseMemberLogin();
            if (MemberID != null && MemberID != "" && MemberID != "0")
            {
                objResponseMemberLogin.retval = "Success";
                objResponseMemberLogin.blogid = BlogId;
            }
            else
            {
                objResponseMemberLogin.retval = "Error";
                objResponseMemberLogin.blogid = BlogId;
            }
            return objResponseMemberLogin;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string SaveBlogImage(string BlogId, string MemberID, string ImageUrl, string AccessType)
        {

            string Retvalue = "";
            Int32 Retvalueint = 0;
            string Image = HttpContext.Current.Server.MapPath(ImageUrl);
            string ThumbNailImage = "Thumb" + Guid.NewGuid().ToString() + ".jpg";
            System.Drawing.Image resized = Bitmap.FromFile(Image);
            Bitmap NewImage = new Bitmap(resized, new Size(536, 335));
            NewImage.Save(HttpContext.Current.Server.MapPath("/UploadedFiles/Blog/Comment/Crop/" + BlogId + "/" + MemberID + "/" + ThumbNailImage), ImageFormat.Jpeg);
            string[] UrlSplit = ImageUrl.Split('/');
            string ImageName = UrlSplit[UrlSplit.Length - 1];
            Cls_comment objComment = new Cls_comment();
            objComment.imagethumbnailurl = ThumbNailImage;
            objComment.imageurl = ImageName;
            objComment.blogid = Convert.ToInt32(BlogId);
            objComment.memberid = Convert.ToString(MemberID);
            objComment.accesstype = Convert.ToString(AccessType);
            Retvalueint = ServicesFactory.DocCMSServices.Insert_comments_image(objComment);
            if (Retvalueint > 0)
            {
                Retvalue = "Success";
            }
            else
            {
                Retvalue = "Error";
            }
            return Retvalue;
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string SaveBlogImage_New(string ResizeImagePath, string ResizeImageName, string BlogId, string MemberID, string ImageUrl, string AccessType)
        {
            string Retvalue = "";
            Int32 Retvalueint = 0;
            string ImageNameNew = "";
            ImageNameNew = Guid.NewGuid().ToString() + ".jpg";
            string TempModifiedImagePath = ResizeImagePath;
            string TempOriginalImagePath = "";
            string TempThumbnailImagePath = "";
            if (TempModifiedImagePath.Contains("CustomImage/"))
            {
                TempModifiedImagePath = TempModifiedImagePath.Replace("/Original/", "/Customed/").Replace("/Thumbnail/", "/Customed/").Replace("/Croped/", "/Customed/");
                TempOriginalImagePath = TempModifiedImagePath.Replace("/Customed/", "/Original/").Replace("/Thumbnail/", "/Original/").Replace("/Croped/", "/Original/");
                TempThumbnailImagePath = TempModifiedImagePath.Replace("/Original/", "/Thumbnail/").Replace("/Customed/", "/Thumbnail/").Replace("/Croped/", "/Thumbnail/");
            }
            if (TempModifiedImagePath.Contains("ResizeImage/"))
            {
                TempModifiedImagePath = TempModifiedImagePath.Replace("/Original/", "/ResizedImage/").Replace("/Thumbnail/", "/ResizedImage/").Replace("/Croped/", "/ResizedImage/");
                TempOriginalImagePath = TempModifiedImagePath.Replace("/ResizedImage/", "/Original/").Replace("/Thumbnail/", "/Original/").Replace("/Croped/", "/Original/");
                TempThumbnailImagePath = TempModifiedImagePath.Replace("/Original/", "/Thumbnail/").Replace("/ResizedImage/", "/Thumbnail/").Replace("/Croped/", "/Thumbnail/");
            }
            if (TempModifiedImagePath.Contains("Crop/"))
            {
                TempModifiedImagePath = TempModifiedImagePath.Replace("/Original/", "/Croped/").Replace("/Thumbnail/", "/Croped/").Replace("/ResizedImage/", "/Croped/");
                TempOriginalImagePath = TempModifiedImagePath.Replace("/ResizedImage/", "/Original/").Replace("/Thumbnail/", "/Original/").Replace("/Croped/", "/Original/");
                TempThumbnailImagePath = TempModifiedImagePath.Replace("/Original/", "/Thumbnail/").Replace("/ResizedImage/", "/Thumbnail/").Replace("/Croped/", "/Thumbnail/");
            }
            string ModifiedImagePath = "/UploadedFiles/Blog/Comment/" + TempModifiedImagePath.Replace("~/TempImage/", "");
            string OrignalImagePath = "/UploadedFiles/Blog/Comment/" + TempOriginalImagePath.Replace("~/TempImage/", "");
            string ThumbNailImagePath = "/UploadedFiles/Blog/Comment/" + TempThumbnailImagePath.Replace("~/TempImage/", "");
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(ModifiedImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ModifiedImagePath));
            }
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(OrignalImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(OrignalImagePath));
            }
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(ThumbNailImagePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ThumbNailImagePath));
            }
            File.Copy(HttpContext.Current.Server.MapPath(TempModifiedImagePath + ResizeImageName), HttpContext.Current.Server.MapPath(ModifiedImagePath + ResizeImageName));
            File.Copy(HttpContext.Current.Server.MapPath(TempOriginalImagePath + ResizeImageName), HttpContext.Current.Server.MapPath(OrignalImagePath + ResizeImageName));
            File.Copy(HttpContext.Current.Server.MapPath(TempThumbnailImagePath + ResizeImageName), HttpContext.Current.Server.MapPath(ThumbNailImagePath + ResizeImageName));
            Cls_comment objComment = new Cls_comment();
            objComment.imagethumbnailurl = ThumbNailImagePath;
            objComment.imageurl = ResizeImageName;
            objComment.blogid = Convert.ToInt32(BlogId);
            objComment.memberid = Convert.ToString(MemberID);
            objComment.accesstype = Convert.ToString(AccessType);
            Retvalueint = ServicesFactory.DocCMSServices.Insert_comments_image(objComment);
            if (Retvalueint > 0)
            {
                Retvalue = "Success";
            }
            else
            {
                Retvalue = "Error";
            }
            return Retvalue;
        }


        // To fetch Part detail by OderDetail ID From Quote Detail
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_PartDetail_ByOrderDetalID(string QuoteDetailID)
        {
            string Retval = "";
            try
            {
                DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_Order_Detail_By_orderdetailid(Convert.ToInt32(QuoteDetailID));
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        // To fetch Part detail by OderDetail ID From Quote Detail
        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public string Fetch_PartDetail_ByOrderDetalID_From_OrderDetailTable(string OrderDetailID)
        {
            string Retval = "";
            try
            {
                DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_PartDetail_ByOrderDetalid_From_OrderDetailTable(Convert.ToInt32(OrderDetailID));
                if (dtresult != null && dtresult.Rows.Count > 0)
                {
                    Retval = GetJson(dtresult);
                }
                return Retval;
            }
            catch
            {
                return Retval;
            }
        }

        [OperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public Int32 Add_NewQuote_to_QuotesTable(string QuoteName, string QuoteComment)
        {
            Int32 Retval = 0;
            try
            {
                Cls_Quotes Cls_Quotes = new Cls_Quotes();
                Cls_Quotes.quotename = QuoteName;
                Cls_Quotes.quotecomment = QuoteComment;
                Cls_Quotes.active = true;
                if (Cls_Quotes != null)
                {
                    Retval = ServicesFactory.DocCMSServices.Insert_Quotes(Cls_Quotes);
                }
                else
                {
                    Retval = 0;
                }
                return Retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }// Class ends here
}


 