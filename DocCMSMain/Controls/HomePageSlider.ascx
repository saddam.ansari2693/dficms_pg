﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePageSlider.ascx.cs"
    Inherits="DocCMSMain.Controls.HomePageSlider" %>
<div class="mstslider">
    <div class="master-slider ms-skin-default" id="masterslider" runat="server" clientidmode="Static">
        <!-- slide 1 -->
        <div class="ms-slide slide-1" data-delay="8">
            <!-- slide background -->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/dfiproduct/background1.png"
                alt="Slide1 background" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/dfiproduct/dfi-base-new.png"
                alt="Slide1 background" style="left: 112px; top: 200px;" class="ms-layer" data-type="image"
                data-delay="400" data-effect="bottom(300)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/dfiproduct/service-it-new.png"
                alt="Slide1 background" style="left: 50px; top: -180px;" class="ms-layer" data-type="image"
                data-delay="1200" data-effect="top(300)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/dfiproduct/view-it-new.png"
                alt="Slide1 background" style="left: 290px; top: -150px;" class="ms-layer" data-type="image"
                data-delay="2000" data-effect="top(600)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/dfiproduct/sign-it-new.png"
                alt="Slide1 background" style="left: 520px; top: -90px;" class="ms-layer" data-type="image"
                data-delay="2800" data-effect="top(900)" data-duration="1800" data-ease="easeOutExpo" />
            <h1 class="ms-layer stext1" style="left: 1000px; top: 90px;" data-type="text" data-duration="900"
                data-delay="400" data-ease="easeOutExpo" data-effect="right(3000)">
                DMS <em>We provide Easy,</em>
                <br />
                Efficient <em>and </em>Affordable Solution<br />
                <em>for Document Management System.</em>
            </h1>
            <h3 class="ms-layer stext2" style="left: 1030px; top: 260px;" data-type="text" data-duration="900"
                data-delay="900" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
                <strong><i>“Cheap things are not good, good things are not cheap.” </i></strong>
            </h3>
            <h3 class="ms-layer stext2" style="left: 990px; top: 290px;" data-type="text" data-duration="900"
                data-delay="1400" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
                <strong><i>“The best time to use DMS was 18 years ago. The second best time is now.”</i></strong>
            </h3>
            <h3 class="ms-layer stext2" style="left: 1050px; top: 320px;" data-type="text" data-duration="900"
                data-delay="1900" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
                <strong><i>“If it's worth doing, it's worth documenting.”</i></strong>
            </h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 380px;" data-type="text" data-duration="900"
                data-delay="2400" data-ease="easeOutExpo" data-effect="right(3000)">
                Revolutionary product that will increase your business productivity by 40%.
            </h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 410px;" data-type="text" data-duration="900"
                data-delay="3000" data-ease="easeOutExpo" data-effect="right(3000)">
                We have the knowledge + You have the requirement= Results….bridge between your ideas
                and success.</h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 440px;" data-type="text" data-duration="900"
                data-delay="3600" data-ease="easeOutExpo" data-effect="right(3000)">
                Anytime, anywhere access.</h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 470px;" data-type="text" data-duration="900"
                data-delay="4200" data-ease="easeOutExpo" data-effect="right(3000)">
                Time/cost efficiency.
            </h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 500px;" data-type="text" data-duration="900"
                data-delay="4800" data-ease="easeOutExpo" data-effect="right(3000)">
                Air-tight Security.</h3>
            </h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 530px;" data-type="text" data-duration="900"
                data-delay="5400" data-ease="easeOutExpo" data-effect="right(3000)">
                “Green” Business Practice.
            </h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 560px;" data-type="text" data-duration="900"
                data-delay="6000" data-ease="easeOutExpo" data-effect="right(3000)">
                Right Information at Right Time to Make Better Decisions..</h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 590px;" data-type="text" data-duration="900"
                data-delay="6600" data-ease="easeOutExpo" data-effect="right(3000)">
                Process Invoice Faster.</h3>
            <h3 class="ms-layer stext2" style="left: 1000px; top: 620px;" data-type="text" data-duration="900"
                data-delay="7200" data-ease="easeOutExpo" data-effect="right(3000)">
                Mobile Accounting.</h3>
        </div>
        <!-- end of slide -->
        <!-- slide 2 -->
        <div class="ms-slide slide-2" data-delay="8">
            <!-- slide background -->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/development.jpg"
                alt="Slide1 background" />
            <h1 class="ms-layer stext1" style="left: 50px; top: 40px; color: #E54D24;" data-type="text"
                data-duration="900" data-delay="1300" data-ease="easeOutExpo" data-effect="bottom(40)">
                We believe on “Don’t expect, Inspect!"<br />
                We provide Platform you need…for the service you Deserve.
                <br />
                <strong><em>Dream….</em></strong>, <strong><em>Build….</em></strong>, <strong><em>Design</em></strong>
            </h1>
            <h3 class="ms-layer stext2" style="left: 100px; top: 200px; color: #E54D24;" data-type="text"
                data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                You’re Dream of Success…DFI.
            </h3>
            <h3 class="ms-layer" style="left: 100px; top: 230px;" data-type="text" data-delay="2500"
                data-ease="easeOutExpo" data-duration="900" data-effect="scale(1.5,1.6)">
                Creating solution for Needs
            </h3>
        </div>
        <!-- end of slide -->
        <!-- slide 3-->
        <div class="ms-slide slide-3" data-delay="8">
            <!-- slide background -->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/mobiledev/bg.jpg"
                alt="" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/mobiledev/andriod.png"
                alt="Slide1 background" style="right: 600px; top: 500px;" class="ms-layer" data-type="image"
                data-delay="1200" data-effect="bottom(300)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/mobiledev/ios.png"
                alt="Slide1 background" style="right: 400px; top: 500px;" class="ms-layer" data-type="image"
                data-delay="1800" data-effect="Right(300)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/mobiledev/windows.png"
                alt="Slide1 background" style="right: 200px; top: 500px;" class="ms-layer" data-type="image"
                data-delay="2200" data-effect="bottom(300)" data-duration="1800" data-ease="easeOutExpo" />
            <h1 class="ms-layer stext1" style="left: 250px; top: 100px; color: #FFF; font-size: 65px;"
                data-type="text" data-duration="1300" data-delay="0" data-ease="easeOutExpo"
                data-effect="rotatefront(-40,900,tr)">
                Mobile App Development
            </h1>
            <h3 class="ms-layer stext1" style="left: 300px; top: 182px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="600" data-ease="easeOutExpo"
                data-effect="right(3000)">
                Rome Anywhere Connect Everywhere.
            </h3>
            <h3 class="ms-layer stext1" style="left: 300px; top: 222px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="1200" data-ease="easeOutExpo"
                data-effect="right(3000)">
                Solution…….Little but Mighty.
            </h3>
            <h3 class="ms-layer stext1" style="left: 300px; top: 262px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo"
                data-effect="right(3000)">
                Be Smart Go Mobile.
            </h3>
            <h3 class="ms-layer stext1" style="left: 300px; top: 302px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="2400" data-ease="easeOutExpo"
                data-effect="right(3000)">
                Real Time….Real People….Real Connection</h3>
            <h3 class="ms-layer stext1" style="left: 300px; top: 342px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="3000" data-ease="easeOutExpo"
                data-effect="right(3000)">
                We Shape your App Expert in development of enterprise,</h3>
            <h3 class="ms-layer stext1" style="left: 300px; top: 382px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="3000" data-ease="easeOutExpo"
                data-effect="right(3000)">
                e-commerce, educational and utility mobile apps.</h3>
            <h3 class="ms-layer stext1" style="left: 300px; top: 422px; color: #fff; font-size: 25px;"
                data-type="text" data-duration="900" data-delay="3600" data-ease="easeOutExpo"
                data-effect="right(3000)">
                From Android to IOS….From App to SuperApp</h3>
        </div>
        <!-- end of slide -->
        <!-- slide 4-->
        <div class="ms-slide slide-3" data-delay="8">
            <!-- slide background -->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/windows/windows.jpg"
                alt="" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/windows/apps.png"
                alt="Slide1 background" style="right: 750px; top: 75px;" class="ms-layer" data-type="image"
                data-delay="600" data-effect="bottom(300)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/windows/pc.png"
                alt="Slide1 background" style="left: 0px; top: 160px;" class="ms-layer" data-type="image"
                data-delay="1200" data-effect="left(300)" data-duration="1800" data-ease="easeOutExpo" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/windows/window-logo.png"
                alt="Slide1 background" style="right: 200px; top: 200px;" class="ms-layer" data-type="image"
                data-delay="1800" data-effect="Right(300)" data-duration="1800" data-ease="easeOutExpo" />
            <h1 class="ms-layer stext1" style="left: 1100px; top: 50px; color: #FFF; font-size: 65px;"
                data-type="text" data-duration="1300" data-delay="0" data-ease="easeOutExpo"
                data-effect="rotatefront(-40,900,tr)">
                Window App Development
            </h1>
        </div>
        <!-- end of slide -->

        <!-- slide 5 -->
        <div class="ms-slide slide-4" data-delay="8">
            <!-- slide background -->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/bg.jpg"
                alt="Slide1 background" />
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/6.png"
                alt="Slide1 background" style="right: 250px; top: 227px;" class="ms-layer animate-spin-horizontal"
                data-type="image" data-delay="400" data-effect="bottom(300)" data-duration="1800"
                data-ease="easeOutExpo" />
            <!--Screw Image Start Here--->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/1.png"
                alt="Slide1 background" style="right: 580px; top: 500px;" class="ms-layer animate-spin test-img"
                data-type="image" data-delay="2500" data-effect="top(900)" data-duration="1800"
                data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1 test-text" style="right: 780px; top: 540px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Troubleshooting
            </h3>
            <!--Screw Image End Here--->
            <!--Bug Image Start Here--->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/2.png"
                alt="Slide1 background" style="right: 100px; top: 200px;" class="ms-layer animate-spin test-img"
                data-type="image" data-delay="1600" data-effect="top(600)" data-duration="1800"
                data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1 test-text" style="right: 80px; top: 120px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Bug Fixing
            </h3>
            <!--Bug Image Start Here--->
            <!--User Image Start Here--->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/3.png"
                alt="Slide1 background" style="right: 340px; top: 27px;" class="ms-layer animate-spin test-img"
                data-type="image" data-delay="600" data-effect="top(300)" data-duration="1800"
                data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1 test-text" style="left: 1050px; top: 20px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Virtual Assistance
            </h3>
            <!--User Image Start Here--->
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/4.png"
                alt="Slide1 background" style="right: 555px; top: 200px;" class="ms-layer animate-spin test-img"
                data-type="image" data-delay="3000" data-effect="top(600)" data-duration="1800"
                data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1 test-text" style="left: 850px; top: 250px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Technical Support
            </h3>
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/5.png"
                alt="Slide1 background" style="right: 80px; top: 500px;" class="ms-layer animate-spin test-img"
                data-type="image" data-delay="3500" data-effect="bottom(900)" data-duration="1800"
                data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1 test-text" style="left: 750px; bottom: 35px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Dial 780.444.5407 to speak to a DFI Representative
            </h3>
         <h3 class="ms-layer stext1 test-text" style="left: 850px; top: 220px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Technical Support
            </h3>
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/5.png"
                alt="Slide1 background" style="right: 80px; top: 500px;" class="ms-layer animate-spin test-img"
                data-type="image" data-delay="3500" data-effect="bottom(900)" data-duration="1800"
                data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1 test-text" style="left: 750px; bottom: 35px; color: #FFF;"
                data-type="text" data-duration="1300" data-delay="400" data-ease="easeOutExpo"
                data-effect="rotate3dright(10,50,0,50)">
                Dial 780.444.5407 to speak to a DFI Representative
            </h3>
            <h1 class="ms-layer stext1" style="left: 180px; top: 100px; color: #FFF; font-size: 55px;"
                data-type="text" data-duration="600" data-delay="400" data-ease="easeOutExpo"
                data-effect="right(3000)" data-effect="rotate3dright(10,50,0,50)">
                TECHNICAL SUPPORT
            </h1>
            <h3 class="ms-layer stext1" style="left: 265px; top: 180px; color: #FFF; font-size:30px;" data-type="text"
                data-duration="1300" data-delay="600" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
               We Love to help you
            </h3>
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/check.png"
                alt="Slide1 background" style="left: 180px; top: 186px;" class="ms-layer" data-type="image"
                data-delay="1000" data-effect="left(900)" data-duration="2000" data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1" style="left: 265px; top: 250px; color: #FFF; font-size:30px;" data-type="text"
                data-duration="2500" data-delay="2000" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
              We reply with a solution  
            </h3>
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/check.png"
                alt="Slide1 background" style="left: 180px; top: 256px;" class="ms-layer" data-type="image"
                data-delay="2300" data-effect="left(900)" data-duration="2300" data-ease="easeOutExpo" />
            <h3 class="ms-layer stext1" style="left: 265px; top: 320px; color: #FFF; font-size:30px;" data-type="text"
                data-duration="3000" data-delay="3500" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
               Have a problem we'll fix it, 780.444.5407 
            </h3>
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/check.png"
                alt="Slide1 background" style="left: 180px; top: 326px;" class="ms-layer" data-type="image"
                data-delay="3300" data-effect="left(900)" data-duration="3300" data-ease="easeOutExpo" />

                  <h3 class="ms-layer stext1" style="left: 265px; top: 390px; color: #FFF; font-size:30px;" data-type="text"
                data-duration="5000" data-delay="5000" data-ease="easeOutExpo" data-effect="scale(1.5,1.6)">
              We Don’t just fix the bugs; We Keep Our promise
            </h3>
            <img src="../DFI/masterslider/blank.gif" data-src="../DFI/masterslider/techsupport/check.png"
                alt="Slide1 background" style="left: 180px; top: 396px;" class="ms-layer" data-type="image"
                data-delay="4700" data-effect="left(900)" data-duration="4700" data-ease="easeOutExpo" />
        </div>
        <!-- end of slide -->
    </div>
    <style type="text/css">
.animate-spin:hover
{
        animation: infinite-spinning 1s infinite;
}
        
@keyframes infinite-spinning {
from {
transform: rotate(0deg);
}
to {
transform: rotate(360deg);
}
}

    .animate-spin-horizontal:hover
    {
        animation: infinite-spinning-new 2s infinite;
    }

@keyframes infinite-spinning-new {
        from {
        transform: rotateY(0deg);
            }
        to {
        transform: rotateY(360deg);
            }
}

.test-text
{
    display:none;
    background:rgba(0,0,0,1);
    padding:5px 8px;
    border-radius:5px;
    position:relative;
    transition: display .2s;
}

.testing
{
    background:url('../DFI/images/circle-sq.png');
    width:500px;
    height:210px;
    transition:all .2s;
    display:none;
    animation:round 1s;
    }

.test-img:hover + h3.testing
{
    display:block;
}

@keyframes round {
        from {
        transform: rotateZ(-65deg);
            }
        to {
        transform: rotateZ(0deg);
            }
}

.test-text:after
{
    position:absolute;
    content:'';
    width:19px;
    height:19px;
    right:-9px;
    /*background:rgba(0,0,0,.80);*/
    top:35%;
    transform:rotate(45deg);
    z-index:-1;
    transition:all .1s;
}

.test-img:hover + h3.test-text
{
    display:block;
}


</style>
</div>
