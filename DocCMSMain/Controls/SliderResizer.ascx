﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SliderResizer.ascx.cs"
    Inherits="DocCMSMain.Controls.SliderResizer" %>
<script src="../js/Resizer/SliderResizer.js" type="text/javascript"></script>
<!---------------------Resize Section--------------------------->
<div id="divModelResize" class="modalResizeEidt" style="height: auto% !important;
    max-width: auto% !important;">
    <div class="modalResizeEidt-content" style="height: auto !important; max-width: auto !important;">
        <span class="ResizeHeaderEdit">Resize Your Image to get a best fit result</span>
        <span class="cross" onclick="CloseImageResizeModel();">×</span>
        <div style="width: auto !important;">
            <img src="" id="ImgResizeImage" style="width: auto; height: auto" />
            <div style="width: 100%;">
                Resize Image Option
                <input type="checkbox" id="chkResizeImageCreate" />
                Create Custom Image
                <input type="checkbox" id="chkCustomeImageCreate" />
                <div id="divCustomResizeImageCreate">
                    <input type="checkbox" id="chkAspectRatioResize" checked="checked" />
                    Keep Aspect Ratio<br />
                    Image Width :<input type="text" id="txtCustomWidth" placeholder="Width in Pixels"
                        onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('w',event);"
                        style="width: 18%" />
                    Image Height :<input type="text" id="txtCustomHeight" placeholder="Height in Pixels"
                        onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('h',event);"
                        style="width: 18%" />
                    <div id="divResizeSection">
                        Select Slider Position<select id="ddlSizeResize" style="width: 90px; height: 36px;"
                            onchange="SetReSizeValues('S');">
                            <option value="1900x700">Full Width</option>
                            <option value="497x288">Left/Right</option>
                        </select>
                    </div>
                    <input type="button" id="btnResizeImage" class="btn btn-danger btn-lg" value="Resize"
                        onclick="ResizeImage();" style="margin-top: 10px; margin-bottom: 10px;" />
                    <span id="spnFinalCrop" style="display: none;"><span style="color: Red; font-weight: 400;">
                        The previewed image exceeds the required dimensions.You need to crop the image to
                        get the required dimensions.</span><br />
                        <input type="checkbox" id="chkFinalCrop" />Crop Image
                        <input type="button" class="btn btn-danger btn-lg" style="display: none;" id="btnCropWithResize"
                            value="Crop Image" onclick="CropImageWithResize();" />
                    </span>
                    <input type="button" id="Button1" class="btn btn-danger btn-lg" value="Preview" onclick="PreviewResizedImage();"
                        style="margin-top: 10px; margin-bottom: 10px; display: none;" />
                    <asp:HiddenField ID="hdnResizeAsepctRatio" runat="server" ClientIDMode="Static" Value="True" />
                    <div id="divResizePreviewPane" style="margin-left: 20px; margin-bottom: 20px;">
                        <span><b>Image Preview</b></span>
                        <div class="preview-container" id="divResizePreview" style="width: 316px; height: 116px;
                            border: 1px solid black;">
                            <img src="" id="ImgResizePreview" alt="Preview" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="divCustomImageCreate" style="display: none;">
            Select Background Image Color:
            <asp:TextBox ID="txtFontColor" runat="server" CssClass="input-large demo" ClientIDMode="Static"
                Style="width: 200px; height: 24px; margin-bottom: 0;" placeholder="Enter Font Color"
                onchange="ColorChange(this)"></asp:TextBox>
            <asp:DropDownList ID="ddlSizeCustomImage" runat="server" onchange="return SetCustomImageOuterSize();"
                ClientIDMode="Static" Style="width: 200px;">
                <asp:ListItem Value="1900x700" Text="Full Width Slider"></asp:ListItem>
                <asp:ListItem Value="497x288" Text="Left/Right Slider"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <input type="button" id="btncustomeImage" class="btn btn-danger btn-lg" value="Create Image"
                onclick="CustomeImage();" style="margin-top: 10px; margin-bottom: 10px;" />
            <div id="divCustomImagePreviewRunning" style="width: 316px; height: 116px; background-color: #FFFFFF;
                margin-left: 5%; margin-bottom: 10px; border: 1px solid #000">
                <center>
                    <img src="" id="ImgCustomeImagePreviewRunning" />
                </center>
            </div>
        </div>
    </div>
    <div id="Div4" runat="server" clientidmode="Static">
    </div>
</div>
<!---------------------Resize Section--------------------------->
