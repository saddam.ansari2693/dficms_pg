﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogRightSideBar.ascx.cs"
    Inherits="DocCMSMain.Controls.BlogRightSideBar" %>
  <!--/.search-->
  <div class="widget search">
    <form role="form">
    <div class="input-group">
        <input type="text" class="form-control"  autocomplete="off"
            placeholder="Search" onkeypress="return validateSearch(event);" id="txtSearchBlog" 
            runat="server" clientidmode="Static" />
        <span class="input-group-btn">
            <div class="btn btn-danger Searchbtn " onclick="return search_Results();">
                <i class="icon-search"></i>
            </div>
        </span>
        <asp:Button ID="btnSearchBlock" runat="server" ClientIDMode="Static" Style="display: none;"
            Text="Search Blogs" OnClick="btnSearchBlock_Click" />
    </div>
    </form>
</div>

<div class="widget categories">
    <h3>
        Blog Categories</h3>
    <div class="row">
        <div class="col-sm-6 col-xs-6">
            <ul class="arrow" id="ulBlogCategoryeven" runat="server">
            </ul>
        </div>
        <div class="col-sm-6 col-xs-6">
            <ul class="arrow" id="ulBlogCategoryodd" runat="server">
            </ul>
        </div>
    </div>
</div>
<style type="text/css">
    .Searchbtn
    {
       padding:8px;
       border-radius:0;
    }
</style>
<script type="text/javascript" language="javascript">
function validateSearch(event) {
      switch (event.keyCode) {
            case 8:  // Backspace
            case 9:  // Tab
            case 13: // Enter
            case 37: // Left
            case 38: // Up
            case 39: // Right
            case 40: // Down
                break;
            default:
                var regex = new RegExp("^[a-zA-Z0-9.,/ $@()]+$");
                var key = event.key;
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
                break;
        } 
    }
    function search_Results() {
        var SerchVal = document.getElementById("txtSearchBlog").value;
        if (SerchVal.toString().trim() != "") {
            var btn = document.getElementById("btnSearchBlock");
            btn.click();
        }
    }
</script>