﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.Controls
{
    public partial class FullWidthSlider : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string path = HttpContext.Current.Request.Url.AbsolutePath;
                string[] PageNameArray = path.Split('/');
                string PageName = PageNameArray.Last();
                Int32 SliderId = 0;
                if (PageName.ToLower() == "default.aspx")
                {
                    SliderId = ServicesFactory.DocCMSServices.Fetch_Slider_sliderid_ForHomePage();
                }
                HiddenField hdnSliderPosition = this.Parent.FindControl("hdnSliderPosition") as HiddenField;
                if (hdnSliderPosition.Value.ToLower() == "full width")
                {
                    Bind_Slider(SliderId);
                }
            }
        }

        // Fetch SliderId Using PageName and CMSID
        public Int32 FetchSliderId(string PageName, Int32 CMSID)
        {
            Int32 SliderId = ServicesFactory.DocCMSServices.Fetch_Slider_id_By_cmsid_pageid(PageName, CMSID);
            return SliderId;
        }

        // Binding Home slider
        protected void Bind_Slider(int SliderId)
        {
            try
            {
                DataTable dtSlider = ServicesFactory.DocCMSServices.Fetch_Slider_image(SliderId);
                if (dtSlider != null && dtSlider.Rows.Count > 0)
                {
                    string Side1 = "";
                    string Side2 = "";
                    string SliderID = "";
                    SliderID = Convert.ToString(dtSlider.Rows[0]["SliderID"]);
                    string sliderPosition = "FULLWIDTH_";
                    StringBuilder strBanner = new StringBuilder();
                    for (Int32 i = 0; i < dtSlider.Rows.Count; i++)
                    {
                        strBanner.Append(" <div class='ms-slide slide-7' data-delay='8'>");
                        strBanner.Append(" <img src='../dfi/masterslider/blank.gif' data-src='../UploadedFiles/SliderImage/" + SliderID + "/" + Convert.ToString(dtSlider.Rows[i]["SliderItemID"]) + "/" + sliderPosition.ToString() + Convert.ToString(dtSlider.Rows[i]["ImageName"]) + "' alt='' />");
                        DataTable dtSliderItem = ServicesFactory.DocCMSServices.Fetch_Slider_Item_Details_images(Convert.ToInt16(dtSlider.Rows[i]["SliderItemID"]));
                        if (dtSliderItem != null && dtSliderItem.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtSliderItem.Rows.Count; j++)
                            {
                                if (Convert.ToString(dtSliderItem.Rows[j]["ItemType"]) == "Text")
                                {
                                    string style = "";
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsBold"]) == "True")
                                    {
                                        style += " font-weight:bold;";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsItalic"]) == "True")
                                    {
                                        style += " font-style :italic;";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsUnderLine"]) == "True")
                                    {
                                        style += " text-decoration: underline;";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsEmphasized"]) == "True")
                                    {
                                        style += " background-color:#" + Convert.ToString(dtSliderItem.Rows[j]["TextBackgroundColor"]) + ";";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["ItemContent"]) != "" && dtSliderItem.Rows[j]["ItemContent"] != null)
                                    {
                                        strBanner.Append("  <h1 class='ms-layer stext1' style='color: " + Convert.ToString(dtSliderItem.Rows[j]["FontColor"]) + "; font-size: " + Convert.ToString(dtSliderItem.Rows[j]["FontSize"]) + "px;" + style + "");
                                        strBanner.Append(" margin-top:" + Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) + "px; margin-bottom:" + Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) + "px; margin-left:" + Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) + "px; margin-right:" + Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) + "px;'");
                                        strBanner.Append("  data-type='text' data-duration='600' data-delay='" + Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) + "' data-ease='easeOutExpo'");
                                        if (!string.IsNullOrEmpty(Convert.ToString(dtSliderItem.Rows[j]["DataEffect"])))
                                        {
                                            strBanner.Append("  data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["DataEffect"]) + "'>");
                                        }
                                        else
                                        {
                                            strBanner.Append("  data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) + "(3000)' data-effect='rotate3dright(10,50,0,50)'>");
                                        }
                                        strBanner.Append("  " + Convert.ToString(dtSliderItem.Rows[j]["ItemContent"]) + "");
                                        strBanner.Append("  </h1>");
                                    }
                                }
                                else
                                {
                                    string style = "";
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsBold"]) == "True")
                                    {
                                        style += " font-weight:bold;";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsItalic"]) == "True")
                                    {
                                        style += " font-style :italic;";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsUnderLine"]) == "True")
                                    {
                                        style += " text-decoration: underline;";
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["IsEmphasized"]) == "True")
                                    {
                                        style += "";
                                    }
                                     if (dtSliderItem.Rows[j]["ContentPosition"] != null && Convert.ToString(dtSliderItem.Rows[j]["ContentPosition"]) != "")
                                    {
                                        string[] ContentPositionArray = Convert.ToString(dtSliderItem.Rows[j]["ContentPosition"]).Split('-');
                                        Side1 = ContentPositionArray[0];
                                        Side2 = ContentPositionArray[1];
                                    }
                                    if (Convert.ToString(dtSliderItem.Rows[j]["ImageName"]) != "" && dtSliderItem.Rows[j]["ImageName"] != null && SliderID != null && SliderID != "" && dtSliderItem.Rows[j]["MarginBottom"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) != "" && Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) != "" && dtSliderItem.Rows[j]["MarginTop"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) != "" && dtSliderItem.Rows[j]["MarginBottom"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) != "" && dtSliderItem.Rows[j]["MarginLeft"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) != "" && dtSliderItem.Rows[j]["MarginRight"] != null && Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) != "" && dtSliderItem.Rows[j]["EntryDelay"] != null && Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) != "" && dtSliderItem.Rows[j]["EntryStyle"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(dtSliderItem.Rows[j]["NavigationUrl"])))
                                        {
                                            strBanner.Append(" <h3 class='ms-layer' ");
                                            strBanner.Append("style='margin-top:" + Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) + "px; margin-bottom:" + Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) + "px; margin-left:" + Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) + "px; margin-right:" + Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) + "px;'");
                                            strBanner.Append("data-duration='900'");
                                            strBanner.Append(" data-delay='" + Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) + "' data-ease='easeOutExpo' data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) + "(900)'>");
                                            strBanner.Append(" <a href='../" + Convert.ToString(dtSliderItem.Rows[j]["NavigationUrl"]) + "'>");
                                            strBanner.Append("<img src='../UploadedFiles/SliderItemDetails/" + SliderID + "/" + Convert.ToString(dtSliderItem.Rows[j]["SliderItemID"]) + "/" + Convert.ToString(dtSliderItem.Rows[j]["ImageName"]) + "'/></a></h3>");
                                        }
                                        else
                                        {
                                            strBanner.Append("<img src='../RMA/masterslider/blank.gif' data-src='../UploadedFiles/SliderItemDetails/" + SliderID + "/" + Convert.ToString(dtSliderItem.Rows[j]["SliderItemID"]) + "/" + Convert.ToString(dtSliderItem.Rows[j]["ImageName"]) + "'");
                                            strBanner.Append("alt='Slide1 background'  class='ms-layer' data-type='image'");
                                            strBanner.Append("style='margin-top:" + Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) + "px; margin-bottom:" + Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) + "px; margin-left:" + Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) + "px; margin-right:" + Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) + "px;'");
                                            strBanner.Append("data-delay='" + Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) + "' data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) + "(900)' data-duration='2000' data-ease='easeOutExpo' />");
                                        }
                                    }
                                }
                            }
                        }
                        strBanner.Append(" </div>");
                    }
                    masterslider.Visible = true;
                    masterslider.InnerHtml = strBanner.ToString();
                }
                else
                {
                    masterslider.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}