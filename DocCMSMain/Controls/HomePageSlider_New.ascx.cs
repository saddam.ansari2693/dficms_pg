﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;
using System.Text;

namespace DocCMSMain.Controls
{
    public partial class HomePageSlider_New : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Banner(1);
            }
        }

        // Binding Home Page Slider Inside masterslider Control
          protected void Bind_Banner_Old(Int32 PageID)
          {
              try
              {
                  DataTable dtSlider = ServicesFactory.DocCMSServices.Fetch_Main_Menu_Dashboard_Bypagename(PageID);
                  if (dtSlider != null && dtSlider.Rows.Count > 0)
                  {
                      //=========BINDING BANNER CONTENT
                      StringBuilder strBannerContent = new StringBuilder();
                      for (Int32 i = 0; i < dtSlider.Rows.Count; i++)
                      {
                          DataTable dsliderContent = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtSlider.Rows[i]["CMSID"]));
                          if (dsliderContent != null && dsliderContent.Rows.Count > 0)
                          {
                              string SliderName = Convert.ToString(dsliderContent.Rows[0]["BannerName"]);
                              string Heading1 = Convert.ToString(dsliderContent.Rows[1]["BannerName"]);
                              string Heading2 = Convert.ToString(dsliderContent.Rows[2]["BannerName"]);
                              string Heading3 = Convert.ToString(dsliderContent.Rows[3]["BannerName"]);
                              string Heading4 = Convert.ToString(dsliderContent.Rows[4]["BannerName"]);
                              string BackgroundBanner = Convert.ToString(dsliderContent.Rows[5]["BannerName"]);
                              string Image1 = Convert.ToString(dsliderContent.Rows[6]["BannerName"]);
                              string Image2 = Convert.ToString(dsliderContent.Rows[7]["BannerName"]);
                              string MainBannerPath = "../UploadedFiles/ContentImages/BannerSlider/" + BackgroundBanner;
                              string ImagePath1 = "../UploadedFiles/ContentImages/BannerSlider/" + Image1;
                              string ImagePath2 = "../UploadedFiles/ContentImages/BannerSlider/" + Image2;
                              string DisplayOrder = Convert.ToString(dsliderContent.Rows[8]["BannerName"]);
                              //---<!-- slide 1 -->
                              if (DisplayOrder == "1")
                              {
                                  strBannerContent.Append("<div class='ms-slide slide-1' data-delay='9'>");

                                  if (string.IsNullOrEmpty(BackgroundBanner))
                                      strBannerContent.Append("<img src='../DFI/masterslider/blank.gif' data-src='http://placehold.it/1920x750' alt='Slide1 background'/>");
                                  else
                                      strBannerContent.Append("<img src='" + MainBannerPath + "' data-src='" + MainBannerPath + "' alt='Slide1 background'/>");

                                  if (string.IsNullOrEmpty(Image1))
                                  {
                                      strBannerContent.Append("<img src='../DFI/masterslider/blank.gif' data-src='http://placehold.it/700x540' alt='Slide1 background'");
                                  }
                                  else
                                  {
                                      strBannerContent.Append("<img src='" + ImagePath1 + "' data-src='" + ImagePath1 + "' alt='Slide1 background'");
                                  }
                                  strBannerContent.Append(" style='left: 112px; top: 40px;' class='ms-layer' data-type='image' data-delay='400' data-effect='bottom(300)'");
                                  strBannerContent.Append("data-duration='1800' data-ease='easeOutExpo' />");

                                  if (!string.IsNullOrEmpty(Heading1))
                                  {
                                      strBannerContent.Append("<h1 class='ms-layer stext1'  style='left: 820px; top: 162px;'  data-type='text' data-duration='900' data-delay='1800'");
                                      strBannerContent.Append("data-ease='easeOutExpo' data-effect='bottom(40)'>" + Heading1 + " </h1>");
                                  }

                                  if (!string.IsNullOrEmpty(Heading2))
                                  {
                                      strBannerContent.Append(" <h4 class='ms-layer stext2'");
                                      strBannerContent.Append("style='left: 820px; top: 327px;'");
                                      strBannerContent.Append("   data-type='text' data-duration='900'  data-delay='2500' data-ease='easeOutExpo' data-effect='bottom(40)' >");
                                      strBannerContent.Append(" " + Heading2 + ".  </h4>");
                                  }
                                  if (!string.IsNullOrEmpty(Heading3))
                                  {
                                      strBannerContent.Append(" <div class='ms-layer' style='left: 825px; top: 404px;' data-type='text' data-delay='3500' data-ease='easeOutExpo' data-duration='900'");
                                      strBannerContent.Append(" data-effect='scale(1.5,1.6)'  > <a href='#' class='sbutton1'>" + Heading3 + "</a> </div></div>");
                                  }
                              }
                              //--<!-- end of slide1 -->



                              //======<!-- slide 2 -->
                              if (DisplayOrder == "2")
                              {
                                  strBannerContent.Append("<div class='ms-slide slide-2' data-delay='8'>");

                                  //===== <!-- slide2 background -->
                                  if (string.IsNullOrEmpty(BackgroundBanner))
                                  {
                                      strBannerContent.Append(" <img src='../DFI/masterslider/blank.gif' data-src='http://placehold.it/1920x750' alt='Slide2 background' /> ");
                                  }
                                  else
                                  {
                                      strBannerContent.Append("<img src='" + MainBannerPath + "' data-src='" + MainBannerPath + "' alt='Slide2 background'/> ");
                                  }
                                  if (string.IsNullOrEmpty(Image1))
                                  {
                                      strBannerContent.Append("<img src='../DFI/masterslider/blank.gif' data-src='../DFI/images/portfolio/thumb/item1.jpg'  alt='' style='left: 800px; top: 81px;'");
                                      strBannerContent.Append("class='ms-layer' data-type='image' data-effect='right(100)' data-ease='easeOutExpo' />");
                                  }
                                  else
                                  {
                                      strBannerContent.Append("<img src='" + ImagePath1 + "/700x540' data-src='" + ImagePath1 + "' alt='' style='left: 800px; top: 81px;' ");
                                      strBannerContent.Append("class='ms-layer' data-type='image' data-effect='right(100)' data-ease='easeOutExpo' />");
                                  }
                                  if (string.IsNullOrEmpty(Image2))
                                  {
                                      strBannerContent.Append("  <img src='../DFI/masterslider/blank.gif' data-src='../DFI/images/portfolio/thumb/item1.jpg' alt='' ");
                                      strBannerContent.Append("style='left: 720px; top: 239px;' class='ms-layer' data-type='image' data-effect='right(300)'  data-ease='easeOutExpo' data-duration='900' data-delay='400' />");
                                  }
                                  else
                                  {
                                      strBannerContent.Append("  <img src='" + ImagePath2 + "' data-src='" + ImagePath2 + "' alt='' ");
                                      strBannerContent.Append("style='left: 720px; top: 239px;' class='ms-layer' data-type='image' data-effect='right(300)'  data-ease='easeOutExpo' data-duration='900' data-delay='400' />");

                                  }
                                  strBannerContent.Append(" <h1 class='ms-layer stext1' style='left: 130px; top: 162px;' data-type='text'data-duration='900' data-delay='1300' data-ease='easeOutExpo'");
                                  if (!string.IsNullOrEmpty(Heading1))
                                  {
                                      strBannerContent.Append("data-effect='bottom(40)' > " + Heading1 + "</h1>");
                                  }

                                  strBannerContent.Append(" <h4 class='ms-layer stext2'  style='left: 130px; top: 327px;' data-type='text'  data-duration='900' data-delay='1800' data-ease='easeOutExpo'");
                                  if (!string.IsNullOrEmpty(Heading2))
                                  {
                                      strBannerContent.Append(" data-effect='bottom(40)'   >" + Heading2 + " </h4>");
                                  }
                                  strBannerContent.Append(" <h6 class='ms-layer'  style='left: 130px; top: 404px;'  data-type='text' data-delay='2500'data-ease='easeOutExpo' data-duration='900' data-effect='scale(1.5,1.6)' >");
                                  if (!string.IsNullOrEmpty(Heading3))
                                  {
                                      strBannerContent.Append("<a href='#' class='sbutton1'>" + Heading3 + "</a></h6> </div>");
                                  }
                                  //===== <!-- end of slide2 -->
                              }

                              if (DisplayOrder == "3")
                              {
                                  strBannerContent.Append("<div class='ms-slide slide-video' data-delay='15'>");

                                  //===== <!-- slide2 background -->
                                  if (string.IsNullOrEmpty(BackgroundBanner))
                                  {
                                      strBannerContent.Append(" <img src='../DFI/masterslider/blank.gif' data-src='http://placehold.it/1920x750' alt='Slide2 background' /> ");
                                  }
                                  else
                                  {
                                      strBannerContent.Append("<img src='" + MainBannerPath + "' data-src='" + MainBannerPath + "' alt='Slide2 background'/> ");
                                  }
                                  strBannerContent.Append("<div class='ms-layer hps-vicapton-box' style='left: 400px; top: 223px' data-effect='bottom(150)'");
                                  strBannerContent.Append("data-duration='2800' data-ease='easeOutExpo'> </div>");
                                  if (!string.IsNullOrEmpty(Heading1))
                                  {
                                      strBannerContent.Append("<h3 class='ms-layer hps-videotitle' style='left: 435px; top: 237px;' data-delay='900'");
                                      strBannerContent.Append("data-duration='3000' data-ease='easeOutExpo' data-effect='rotate3dright(0,20,0,0)'>" + Heading1 + "</h3>");
                                  }

                                  if (string.IsNullOrEmpty(Heading2))
                                  {
                                      strBannerContent.Append("<p class='ms-layer hps-videotext' style='left: 300px; top: 300px;' data-delay='1800'");
                                      strBannerContent.Append(" data-ease='easeOutExpo' data-duration='1300' data-effect='bottom(150)'>" + Heading2 + "</p>");
                                  }
                                  strBannerContent.Append("<video data-autopause='false' data-mute='true' data-loop='true' data-fill-mode='fill'>");
                                  strBannerContent.Append("<source id='mp4' src='../DFI/images/video2.mp4' type='video/mp4'/>");
                                  strBannerContent.Append(" <source id='webm' src='../DFI/images/video.webm' type='video/webm'/>");
                                  strBannerContent.Append("  <source id='ogv' src='../DFI/images/video.ogv' type='video/ogg'/>");
                                  strBannerContent.Append("</video>   </div>");
                              }
                              if (DisplayOrder == "4")
                              {
                                  strBannerContent.Append(" <div class='ms-slide slide-3' data-delay='8'>");
                                  strBannerContent.Append(" <img src='../DFI/masterslider/blank.gif' data-src='http://placehold.it/1920x750'  alt='' />");
                                  strBannerContent.Append("<h1 class='ms-layer stext1' style='left: 365px; top: 74px;' data-type='text' data-duration='1300'");
                                  strBannerContent.Append("data-delay='0' data-ease='easeOutExpo' data-effect='rotatefront(-40,900,tr)'> </h1>");
                                  strBannerContent.Append(" <h1 class='ms-layer stext1' style='left: 445px; top: 126px;' data-type='text' data-duration='1300'");
                                  strBannerContent.Append("data-delay='400' data-ease='easeOutExpo' data-effect='rotate3dright(10,50,0,50)'> </h1>");
                                  strBannerContent.Append(" <h4 class='ms-layer stext2' style='left: 475px; top: 190px;' data-type='text' data-duration='900'");
                                  strBannerContent.Append("data-delay='900' data-ease='easeOutExpo' data-effect='bottom(40)'></h4>");
                                  strBannerContent.Append("<div class='ms-layer scircle1' style='left: 300px; top: 260px;' data-type='text'data-duration='900' data-delay='1300'");
                                  strBannerContent.Append("data-ease='easeOutExpo' data-effect='bottom(40)'> <div> <span></span></div></div>");
                                  strBannerContent.Append("<div class='ms-layer scircle1' style='left: 345px; top: 287px;' data-type='text'  data-duration='900' data-delay='1400'");
                                  strBannerContent.Append("data-ease='easeOutExpo' data-effect='bottom(40)'><i class='fa fa-umbrella'></i> </div>");
                                  strBannerContent.Append(" <div class='ms-layer scircle1' style='left: 318px; top: 335px;' data-type='text'");
                                  strBannerContent.Append("  data-duration='900' data-delay='1500' data-ease='easeOutExpo' data-effect='bottom(40)'><h6></h6></div> ");
                                  strBannerContent.Append("<div class='ms-layer scircle1' style='left: 470px; top: 260px;' data-type='text' data-duration='900' data-delay='1300'");
                                  strBannerContent.Append("data-ease='easeOutExpo' data-effect='bottom(40)'>  <div>  <span></span>  </div> </div>");
                                  strBannerContent.Append("<div class='ms-layer scircle1' style='left: 513px; top: 287px;' data-type='text'  data-duration='900' data-delay='1400'");
                                  strBannerContent.Append("data-ease='easeOutExpo' data-effect='bottom(40)'> <i class='fa fa-cog'></i> </div>");
                                  strBannerContent.Append("<div class='ms-layer scircle1' style='left: 495px; top: 335px;' data-type='text' data-duration='900' data-delay='1500'");
                                  strBannerContent.Append("data-ease='easeOutExpo' data-effect='bottom(40)'>  <h6></h6> </div>  </div>");

                              }
                          }
                      }
                      masterslider.InnerHtml = strBannerContent.ToString();
                  }
              }
              catch (Exception ex)
              {
                  throw ex;
              }
          }
        //Get height width for slider
          public void SliderHeightWidthSet()
          {
            DataTable dtGetSliderMasterDetails= ServicesFactory.DocCMSServices.Fetch_Slider_Master_Data_By_sliderid(1);
            if (dtGetSliderMasterDetails != null && dtGetSliderMasterDetails.Rows.Count > 0)
            {
                masterslider.Attributes.Add("style", "width: " + Convert.ToString(dtGetSliderMasterDetails.Rows[0]["width"]) + "%;");
                masterslider.Attributes.Add("style", "height: " + Convert.ToString(dtGetSliderMasterDetails.Rows[0]["height"]) + "%;");
            }
          }

          //New Banner Bind Function
          protected void Bind_Banner(Int32 SliderId)
          {
              try
              {
                  bool IsCheckData = true;
                  DataTable dtSlider = ServicesFactory.DocCMSServices.Fetch_Slider_image(SliderId);
                  if (dtSlider != null && dtSlider.Rows.Count > 0)
                  {
                      string Side1 = "";
                      string Side2 = "";
                      string SliderItemID = "";
                      string SliderID = "";
                      SliderItemID = Convert.ToString(dtSlider.Rows[0]["SliderItemID"]);
                      SliderID = Convert.ToString(dtSlider.Rows[0]["SliderID"]);
                      StringBuilder strBanner = new StringBuilder();
                      for (Int32 i = 0; i < dtSlider.Rows.Count; i++)
                      {
                          strBanner.Append(" <div class='ms-slide slide-7' data-delay='8'>");
                          if (SliderID != null && SliderID != "" && dtSlider.Rows[i]["ImageName"] != null && Convert.ToString(dtSlider.Rows[i]["ImageName"]) != "")
                          {
                              strBanner.Append(" <img src='../RMA/masterslider/blank.gif' data-src='../UploadedFiles/SliderImage/" + SliderID + "/" + Convert.ToString(dtSlider.Rows[i]["ImageName"]) + "' alt='' />");
                          }
                          DataTable dtSliderItem = ServicesFactory.DocCMSServices.Fetch_Slider_Item_Details_images(Convert.ToInt16(dtSlider.Rows[i]["SliderItemID"]));
                          if (dtSliderItem != null && dtSliderItem.Rows.Count > 0)
                          {
                              for (int j = 0; j < dtSliderItem.Rows.Count; j++)
                              {
                                  if (Convert.ToString(dtSliderItem.Rows[j]["ItemType"]) == "Text")
                                  {
                                      string style = "";
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsBold"]) == "True")
                                      {
                                          style += " font-weight:bold;";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsItalic"]) == "True")
                                      {
                                          style += " font-style :italic;";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsUnderLine"]) == "True")
                                      {
                                          style += " text-decoration: underline;";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsEmphasized"]) == "True")
                                      {
                                          style += " background-color:" + Convert.ToString(dtSliderItem.Rows[j]["TextBackgroundColor"]) + ";";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["ItemContent"]) != "" && dtSliderItem.Rows[j]["ItemContent"] != null)
                                      {
                                          strBanner.Append("  <h1 class='ms-layer stext1' style='color: " + Convert.ToString(dtSliderItem.Rows[j]["FontColor"]) + "; font-size: " + Convert.ToString(dtSliderItem.Rows[j]["FontSize"]) + "px;" + style + "");
                                          strBanner.Append(" margin-top:" + Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) + "px; margin-bottom:" + Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) + "px; margin-left:" + Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) + "px; margin-right:" + Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) + "px;'");
                                          strBanner.Append("  data-type='text' data-duration='600' data-delay='" + Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) + "' data-ease='easeOutExpo'");
                                          if (!string.IsNullOrEmpty(Convert.ToString(dtSliderItem.Rows[j]["DataEffect"])))
                                          {
                                              strBanner.Append("  data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["DataEffect"]) + "'>");
                                          }
                                          else
                                          {
                                              strBanner.Append("  data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) + "(3000)' data-effect='rotate3dright(10,50,0,50)'>");
                                          }
                                          strBanner.Append("  " + Convert.ToString(dtSliderItem.Rows[j]["ItemContent"]) + "");
                                          strBanner.Append("  </h1>");
                                      }
                                  }
                                  else
                                  {
                                      string style = "";
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsBold"]) == "True")
                                      {
                                          style += " font-weight:bold;";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsItalic"]) == "True")
                                      {
                                          style += " font-style :italic;";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsUnderLine"]) == "True")
                                      {
                                          style += " text-decoration: underline;";
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["IsEmphasized"]) == "True")
                                      {
                                          style += "";
                                      }

                                      if (dtSliderItem.Rows[j]["ContentPosition"] != null && Convert.ToString(dtSliderItem.Rows[j]["ContentPosition"]) != "")
                                      {
                                          string[] ContentPositionArray = Convert.ToString(dtSliderItem.Rows[j]["ContentPosition"]).Split('-');
                                          Side1 = ContentPositionArray[0];
                                          Side2 = ContentPositionArray[1];
                                      }
                                      if (Convert.ToString(dtSliderItem.Rows[j]["ImageName"]) != "" && dtSliderItem.Rows[j]["ImageName"] != null && SliderID != null && SliderID != "" && dtSliderItem.Rows[j]["MarginBottom"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) != "" && Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) != "" && dtSliderItem.Rows[j]["MarginTop"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) != "" && dtSliderItem.Rows[j]["MarginBottom"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) != "" && dtSliderItem.Rows[j]["MarginLeft"] != null && Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) != "" && dtSliderItem.Rows[j]["MarginRight"] != null && Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) != "" && dtSliderItem.Rows[j]["EntryDelay"] != null && Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) != "" && dtSliderItem.Rows[j]["EntryStyle"] != null)
                                      {
                                          if (!string.IsNullOrEmpty(Convert.ToString(dtSliderItem.Rows[j]["NavigationUrl"])))
                                          {
                                              strBanner.Append(" <h3 class='ms-layer' ");
                                              strBanner.Append("style='margin-top:" + Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) + "px; margin-bottom:" + Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) + "px; margin-left:" + Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) + "px; margin-right:" + Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) + "px;'");
                                              strBanner.Append("data-duration='900'");
                                              strBanner.Append(" data-delay='" + Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) + "' data-ease='easeOutExpo' data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) + "(900)'>");
                                              strBanner.Append(" <a href='../" + Convert.ToString(dtSliderItem.Rows[j]["NavigationUrl"]) + "'>");
                                              strBanner.Append("<img src='../UploadedFiles/SliderItemDetails/" + SliderID + "/" + Convert.ToString(dtSliderItem.Rows[j]["SliderItemID"]) + "/" + Convert.ToString(dtSliderItem.Rows[j]["ImageName"]) + "'/></a></h3>");
                                          }
                                          else
                                          {
                                              strBanner.Append("<img src='../RMA/masterslider/blank.gif' data-src='../UploadedFiles/SliderItemDetails/" + SliderID + "/" + Convert.ToString(dtSliderItem.Rows[j]["SliderItemID"]) + "/" + Convert.ToString(dtSliderItem.Rows[j]["ImageName"]) + "'");
                                              strBanner.Append("alt='Slide1 background'  class='ms-layer' data-type='image'");
                                              strBanner.Append("style='margin-top:" + Convert.ToString(dtSliderItem.Rows[j]["MarginTop"]) + "px; margin-bottom:" + Convert.ToString(dtSliderItem.Rows[j]["MarginBottom"]) + "px; margin-left:" + Convert.ToString(dtSliderItem.Rows[j]["MarginLeft"]) + "px; margin-right:" + Convert.ToString(dtSliderItem.Rows[j]["MarginRight"]) + "px;'");
                                              strBanner.Append("data-delay='" + Convert.ToString(dtSliderItem.Rows[j]["EntryDelay"]) + "' data-effect='" + Convert.ToString(dtSliderItem.Rows[j]["EntryStyle"]) + "(900)' data-duration='2000' data-ease='easeOutExpo' />");
                                          }
                                      }
                                  }
                              }
                          }
                          strBanner.Append(" </div>");
                      }
                      masterslider.Visible = true;
                      masterslider.InnerHtml = strBanner.ToString();
                  }
                  else
                  {
                      masterslider.Visible = false;
                  }
              }
              catch (Exception ex)
              {
                  throw ex;
              }
          }

    }// Class Ends here
}