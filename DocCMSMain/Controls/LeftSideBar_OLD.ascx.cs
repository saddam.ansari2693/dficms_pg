﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.Controls
{
    public partial class LeftSideBar_OLD : System.Web.UI.UserControl
    {
        public static string Liid = "";
        public static string Ulid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["linkname"] != null)
            {
                Liid = Convert.ToString(Session["linkname"]);
            }
            if (Session["Ulname"] != null)
            {
                Ulid = Convert.ToString(Session["Ulname"]);
            }
            if (!string.IsNullOrEmpty(Liid))
            {
                foreach (Control ctrl in this.Controls)
                {
                    HtmlGenericControl liItem = (HtmlGenericControl)ctrl.FindControl(Liid.ToString());
                    if (liItem != null)
                    {
                        liItem.Attributes.Add("class", "dropdown active");
                    }
                    HtmlGenericControl ULItem = (HtmlGenericControl)ctrl.FindControl(Ulid.ToString());
                    if (ULItem != null)
                    {
                        ULItem.Attributes.Add("style", "display: block;");
                    }
                }
            }
        }
    }//== CLASS ENDS HERE 
}