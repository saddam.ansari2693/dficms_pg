﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.Controls
{
    public partial class BlogRightSideBar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_BlogCategory();
            }
        }

        //Bind Blog CategoryFunction
        protected void Bind_BlogCategory()
        {
            try
            {
                DataTable dtBlogCategory = ServicesFactory.DocCMSServices.Fetch_Blog_category_name();
                if (dtBlogCategory != null && dtBlogCategory.Rows.Count > 0)
                {
                    StringBuilder strCaregory = new StringBuilder();
                    Session.Add("CurrPage", "Blogs");
                    for (Int32 i = 0; i < dtBlogCategory.Rows.Count; i++)
                    {
                        string BlogCategoryName = Convert.ToString(dtBlogCategory.Rows[i]["BlogCategoryName"]);
                        string NavigationURL = Convert.ToString(dtBlogCategory.Rows[i]["NavigationURL"]);
                        string BlogCategoryId = Convert.ToString(dtBlogCategory.Rows[i]["BlogCategoryID"]);
                        if (i % 2 == 0)
                        {
                            strCaregory.Append(" <li class='link2'><a  href='blogs-" + BlogCategoryName + ".html'>" + BlogCategoryName + "</a></li>");
                        }
                    }
                    ulBlogCategoryeven.InnerHtml = strCaregory.ToString();
                    strCaregory = new StringBuilder();
                    for (Int32 i = 0; i < dtBlogCategory.Rows.Count; i++)
                    {
                        string BlogCategoryName = Convert.ToString(dtBlogCategory.Rows[i]["BlogCategoryName"]);
                        string NavigationURL = Convert.ToString(dtBlogCategory.Rows[i]["NavigationURL"]);
                        string BlogCategoryId = Convert.ToString(dtBlogCategory.Rows[i]["BlogCategoryID"]);
                        if (i % 2 != 0)
                        {
                            strCaregory.Append(" <li class='link2'><a  href='blogs-" + BlogCategoryName + ".html'>" + BlogCategoryName + "</a></li>");
                        }
                    }
                    ulBlogCategoryodd.InnerHtml = strCaregory.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSearchBlock_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearchBlog.Value))
            {
                DataTable dt = ServicesFactory.DocCMSServices.Search_Blog_Text(txtSearchBlog.Value.Trim());
                Session.Add("SearchData", dt);
                Response.Redirect("Blogs-Search-" + txtSearchBlog.Value.Trim() + ".html");
            }
        }
    }
}