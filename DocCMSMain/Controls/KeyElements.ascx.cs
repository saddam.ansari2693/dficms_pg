﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.Text;

namespace DocCMSMain.Controls
{
    public partial class KeyElements : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_KeyElements(9);
            }
        }

        // Binding key Element Icon inside DivKeyElements Control
        protected void Bind_KeyElements(Int32 PageID)
        {
            try
            {
                DataTable dtKeyElements = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtKeyElements != null && dtKeyElements.Rows.Count > 0)
                {
                    StringBuilder strContent = new StringBuilder();
                    for (Int32 ItemCount = 0; ItemCount < dtKeyElements.Rows.Count; ItemCount++)
                    {
                        DataTable dtKeyContent = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtKeyElements.Rows[ItemCount]["CMSID"]), "KeyName");
                        if (dtKeyContent != null && dtKeyContent.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtKeyContent.Rows[0]["KeyName"]);
                            string IconName = Convert.ToString(dtKeyContent.Rows[2]["KeyName"]).ToLower();
                            string HoverIconName = Convert.ToString(dtKeyContent.Rows[3]["KeyName"]).ToLower();
                            string MouseOverValue = "divKey" + Convert.ToString(ItemCount + 1) + "~../UploadedFiles/ContentImages/KeyElement/" + HoverIconName;
                            string MouseOutValue = "divKey" + Convert.ToString(ItemCount + 1) + "~../UploadedFiles/ContentImages/KeyElement/" + IconName;
                            string ActiveStatus = Convert.ToString(dtKeyContent.Rows[4]["KeyName"]).ToUpper();
                            if (ActiveStatus == "TRUE")
                            {
                                if (dtKeyElements.Rows.Count % 3 == 0)
                                {
                                    strContent.Append("<div class='col-md-4 dms'>");
                                }
                                else if (dtKeyElements.Rows.Count % 3 == 2)
                                {
                                    if (ItemCount < dtKeyElements.Rows.Count - 2)
                                    {
                                        strContent.Append("<div class='col-md-4 dms'>");
                                    }
                                    else
                                    {
                                        strContent.Append("<div class='col-md-6 dms'>");
                                    }
                                }
                                else if (dtKeyElements.Rows.Count % 3 == 1)
                                {
                                    if (ItemCount < dtKeyElements.Rows.Count - 1)
                                    {
                                        strContent.Append("<div class='col-md-4 dms'>");
                                    }
                                    else
                                    {
                                        strContent.Append("<div class='col-md-4 dms'></div>");
                                        strContent.Append("<div class='col-md-4 dms'>");
                                    }
                                }
                                else
                                {
                                    strContent.Append("<div class='col-md-4 dms'></div>");
                                    strContent.Append("<div class='col-md-4 dms'>");
                                }
                                strContent.Append("<div id='divKey" + Convert.ToString(ItemCount + 1) + "' class='dms-icon' clientidmode='Static'");
                                strContent.Append(" style='background:#fff url(../UploadedFiles/ContentImages/KeyElement/" + IconName + "); '");
                                strContent.Append(" onmouseover=changeImageIn('" + MouseOverValue + "');");
                                strContent.Append(" onmouseout=changeImageOut('" + MouseOutValue + "');");
                                strContent.Append("></div><h3 id='heading_divKey" + Convert.ToString(ItemCount + 1) + "'>" + Heading + "</h3></div> ");
                            }
                        }
                    }
                    DivKeyElements.InnerHtml = strContent.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
