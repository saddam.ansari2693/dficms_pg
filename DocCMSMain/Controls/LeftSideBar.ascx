﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftSideBar.ascx.cs"
    Inherits="DocCMSMain.Controls.LeftSideBar1" %>
<div class="leftpanel">
    <div class="leftmenu">
        <ul class="nav nav-tabs nav-stacked">
            <li class="nav-header" id="liheader">Navigation</li>
            <asp:Repeater ID="rptrLeftSideMenus" runat="server" ClientIDMode="Static" OnItemDataBound="rptrLeftSideMenus_ItemDataBound">
                <ItemTemplate>
                    <li class="dropdown" id="licontent" runat="server">
                        <asp:Label ID="lblRoleID" runat="server" ClientIDMode="Static" Text='<%# Eval("RoleID") %>'
                            Visible="false"></asp:Label>
                        <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("ModuleID") %>'
                            Visible="false"></asp:Label>
                            <asp:Label ID="lblModuleName" runat="server" ClientIDMode="Static" Text='<%# Eval("ModuleName") %>'
                            Visible="false"></asp:Label>
                        <a href="" id="LinkModuleName" runat="server" clientidmode="Static" style="cursor:pointer;"><span id="spnMenuIcon" runat="server" class="iconfa-pencil"></span>
                        </a>
                        <ul id="ulcontent" runat="server">
                            <asp:Repeater ID="rptrLeftSideSubMenu" runat="server" OnItemDataBound="rptrLeftSideSubMenu_ItemDataBound" OnItemCommand="rptrLeftSideSubMenu_ItemCommand">
                                <ItemTemplate>
                                    <li>
                                     <asp:Label ID="lblSubModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("SubModuleID") %>' Visible="false"></asp:Label>
                                     <asp:LinkButton ID="lnkSubMenu" runat="server" ClientIDMode="Static" Text='<%# Eval("SubModuleName")%>' CommandName="NavigateToPage"></asp:LinkButton>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </li>
                </ItemTemplate>                
            </asp:Repeater>
        </ul>
    </div>
    <!--leftmenu-->
</div>
