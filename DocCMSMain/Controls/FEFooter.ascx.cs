﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;
using System.Text;

namespace DocCMSMain.Controls
{
    public partial class FEFooter : System.Web.UI.UserControl
    {
        protected static string CMSID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                labyear.Text = DateTime.Now.Year.ToString();
                Bind_AboutUs(15);
                Bind_FooterMenu(2);
                Bind_Blog(8);
                Bind_Address(29);
            }
        }

        // Binding About Us Footer inside DivAboutUs
        protected void Bind_AboutUs(Int32 PageID)
        {
            try
            {

                DataTable dtAboutUs = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(PageID);
                if (dtAboutUs != null && dtAboutUs.Rows.Count > 0)
                {
                    StringBuilder strAboutUs = new StringBuilder();
                    for (Int32 i = 0; i < dtAboutUs.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtAboutUs.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string Detail = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string LinkName = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]).Replace(".aspx",".html");
                            if (Detail.Length > 218)
                            {
                                Detail = Detail.Substring(0, 218);
                            }
                            Detail = Detail.Replace("class=", "");
                            Detail = Detail.Replace("about-content", "");
                            strAboutUs.Append(" <h4>" + Heading + "</h4>");
                            strAboutUs.Append(Detail);
                            strAboutUs.Append("<br /><br /><a href='.." + NavigationURL + "' class='product-read-more'>" + LinkName + " <i class='fa fa-chevron-circle-right'></i></a>");
                        }
                    }
                    DivAboutUs.InnerHtml = strAboutUs.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Binding Footer menu inside rptrFooterMenu Repeater Control
        protected void Bind_FooterMenu(Int32 PageID)
        {

            try
            {
                DataTable dtFooterMenu = ServicesFactory.DocCMSServices.Fetch_Menu_Dashboard_Bypagename(PageID);
                if (dtFooterMenu != null && dtFooterMenu.Rows.Count > 0)
                {
                    rptrFooterMenu.DataSource = dtFooterMenu;
                    rptrFooterMenu.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Binding Blog Control Inside DivBlog Control

        protected void Bind_Blog(Int32 PageID)
        {
            try
            {
                StringBuilder strBlog = new StringBuilder();
                DataTable dtBlog = ServicesFactory.DocCMSServices.Fetch_Blog_Details_onFooter();
                if (dtBlog != null && dtBlog.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dtBlog.Rows.Count; i++)
                    {
                        string BlogName=Convert.ToString(dtBlog.Rows[i]["BlogName"]);
                        string Comment = Convert.ToString(dtBlog.Rows[i]["Description"]);
                        string ImageURL = "../UploadedFiles/UploadedFiles/Blogs/" + Convert.ToString(dtBlog.Rows[i]["ImageName"]);
                        string PostedDate = Convert.ToString(dtBlog.Rows[i]["CreationDate"]);
                        string BlogID=Convert.ToString(dtBlog.Rows[i]["BlogID"]);
                        Session["CurrPage"] = "Blogs";
                        string NavigationURL = "blog-" + BlogName + ".html";
                        if (Comment.Length >=60)
                        {
                           Comment = Comment.Substring(0, 60)+"...";
                        }
                        strBlog.Append("<a class='divfooterBlog' href='" + NavigationURL + "' ><div class='media' style='padding-top:10px;'>  <div class='pull-left'><img src='" + ImageURL + "' alt='' Style='width:50px;height:60px;'> </div>");
                        strBlog.Append("<div class='media-body'> <span class='media-heading'>" + Comment + "</span>");
                        strBlog.Append("<p><small class='muted'>Posted " + PostedDate + "</small></p></div></div></a>");
                    }
                }
                DivBlog.InnerHtml = strBlog.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Binding Address Control inside DivAddress
        protected void Bind_Address(Int32 PageID)
        {
            try
            {
                DataTable dtAddress = ServicesFactory.DocCMSServices.Fetch_Menu_Dashboard_Bypagename(PageID);
                if (dtAddress != null && dtAddress.Rows.Count > 0)
                {
                    StringBuilder strAddress = new StringBuilder();
                    for (Int32 i = 0; i < dtAddress.Rows.Count; i++)
                    {
                        DataTable dtAddressDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtAddress.Rows[i]["CMSID"]));
                        if (dtAddressDetail != null && dtAddressDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtAddressDetail.Rows[7]["PageContent"]);
                            string Detail = Convert.ToString(dtAddressDetail.Rows[7]["PageContent"]);
                            strAddress.Append(" <h4> Address</h4>");
                            strAddress.Append(Detail);
                        }
                    }
                    DivAddress.InnerHtml = strAddress.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        protected void rptrFooterMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                HtmlGenericControl liMenuName = (HtmlGenericControl)e.Item.FindControl("liMenuName");
                LinkButton lnkNavigation = (LinkButton)e.Item.FindControl("lnkNavigation");
                if (lblCMSID != null && liMenuName != null && lnkNavigation != null)
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        liMenuName.Attributes.Add("class", "active");
                    }
                    DataTable dtFooterMenu = ServicesFactory.DocCMSServices.Fetch_Menu_Detail_By_cmsid(Convert.ToInt32(lblCMSID.Text));
                    if (dtFooterMenu != null && dtFooterMenu.Rows.Count > 0)
                    {
                        liMenuName.Visible = true;
                        lnkNavigation.CommandArgument = Convert.ToString(dtFooterMenu.Rows[1]["MenuName"]).Trim();
                        lnkNavigation.Text = Convert.ToString(dtFooterMenu.Rows[0]["MenuName"]).Trim();
                    }
                    else
                    {
                        liMenuName.Visible = false;
                    }
                }
            }
        }
        protected void rptrFooterMenu_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                if (e.CommandName == "NavigateToURL")
                {
                    LinkButton lnkNavigation = (LinkButton)e.Item.FindControl("lnkNavigation");
                    if (lnkNavigation != null)
                    {
                        Session["CurrPage"] = lnkNavigation.Text;
                    }
                    Response.Redirect(Convert.ToString("~"+ e.CommandArgument.ToString().Replace(".aspx", ".html")));
                }
            }
        }
    }
}