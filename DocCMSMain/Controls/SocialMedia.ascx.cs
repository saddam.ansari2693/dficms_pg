﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.Controls
{
    public partial class SocialMedia : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Bind_SocialMedia(7);
            }
        }

        // Binding Social Media Function inside DivSocialMedia
        protected void Bind_SocialMedia(Int32 PageID)
        {

            try
            {
                DataTable dtSocialMedia = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtSocialMedia != null && dtSocialMedia.Rows.Count > 0)
                {
                    StringBuilder strSocialMedia = new StringBuilder();
                    for (Int32 i = 0; i < dtSocialMedia.Rows.Count; i++)
                    {
                        DataTable dtSocialMediaContent = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtSocialMedia.Rows[i]["CMSID"]), "SocialMedia");
                        if (dtSocialMediaContent != null && dtSocialMediaContent.Rows.Count > 0)
                        {
                            string IconName =Convert.ToString(dtSocialMediaContent.Rows[0]["SocialMedia"]);
                            string NavigationURl = Convert.ToString(dtSocialMediaContent.Rows[1]["SocialMedia"]);
                            string Heading = Convert.ToString(dtSocialMediaContent.Rows[0]["SocialMedia"]);
                            string Description = Convert.ToString(dtSocialMediaContent.Rows[2]["SocialMedia"]);
                            //Binding DivSocialMedia Starts here...
                            if (dtSocialMedia.Rows.Count % 4 == 0)
                            {
                                strSocialMedia.Append("<div class='col-md-3 col-sm-6'>");
                            }
                            else
                            {
                                strSocialMedia.Append("<div class='col-md-4 col-sm-6'>");
                            }
                            strSocialMedia.Append("<div class='media'>");
                            strSocialMedia.Append("<div class='pull-left'>");
                            if (IconName.ToUpper().Contains("TWITTER"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-twitter'></i></a>");
                            }
                            else if (IconName.ToUpper().Contains("FACEBOOK"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-facebook'></i></a>");
                            }
                            else if (IconName.ToUpper().Contains("GOOGLE PLUS"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-google-plus'></i></a>");
                            }
                            else if (IconName.ToUpper().Contains("LINKED IN"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-linkedin'></i></a>");
                            }
                            else
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa'></i></a>");
                            }
                            strSocialMedia.Append("</div>");
                            strSocialMedia.Append("<a  style='color:#fff;' href='" + NavigationURl + "' class='link'><div class='media-body'>");
                            strSocialMedia.Append(" <h3 class='media-heading'>" + Heading + "</h3>");
                            strSocialMedia.Append("<p>" + Description + ".</p>");
                            strSocialMedia.Append("</div> </a></div></div><!--/.col-md-4-->");
                        }
                    }
                    DivSocialMedia.InnerHtml = strSocialMedia.ToString();
                    //Binding DivSocialMedia Ends here...
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}