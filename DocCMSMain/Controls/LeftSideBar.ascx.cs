﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DocCMS.Core;
using System.Data;
using System.Text;
using DocCMS.Core.DataTypes;

namespace DocCMSMain.Controls
{
    public partial class LeftSideBar1 : System.Web.UI.UserControl
    {

        public static string Liid = "";
        public static string Ulid = "";
        DataTable dtModules = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    Int32 RoleID = 0;
                    RoleID = ServicesFactory.DocCMSServices.Fetch_roleid_By_userid(Convert.ToString(Session["UserID"]));
                    dtModules = ServicesFactory.DocCMSServices.Fetch_LeftSide_modules_By_roleid(RoleID);
                    if (dtModules != null && dtModules.Rows.Count > 0)
                    {
                        rptrLeftSideMenus.DataSource = dtModules;
                        rptrLeftSideMenus.DataBind();
                    }
                }
            }
            // To set Active Module 
            if (rptrLeftSideMenus.Items.Count > 0)
            {
                if (Session["SubModuleID"] != null)
                {
                    Liid = Convert.ToString(Session["SubModuleID"]);
                    DataTable dtModule = ServicesFactory.DocCMSServices.Fetch_moduleId_By_SubModuleId(Convert.ToInt32(Liid));
                    if (dtModule != null && dtModule.Rows.Count > 0)
                    {
                        Ulid = Convert.ToString(dtModule.Rows[0][0]);
                    }
                    for (Int32 i = 0; i < rptrLeftSideMenus.Items.Count; i++)
                    {
                        HtmlGenericControl licontent = (HtmlGenericControl)rptrLeftSideMenus.Items[i].FindControl("licontent");
                        Label lblModuleID = (Label)rptrLeftSideMenus.Items[i].FindControl("lblModuleID");
                        HtmlGenericControl ulcontent = (HtmlGenericControl)rptrLeftSideMenus.Items[i].FindControl("ulcontent");
                        if (lblModuleID != null && licontent != null)
                        {
                            if (lblModuleID.Text.Trim() == Ulid.Trim())
                            {
                                licontent.Attributes.Add("class", "dropdown active");
                                if (ulcontent != null)
                                {
                                    ulcontent.Attributes.Add("style", "display: block;");
                                }
                            }
                        }
                    }
                }
                else
                {
                    HtmlGenericControl licontent = (HtmlGenericControl)rptrLeftSideMenus.Items[0].FindControl("licontent");
                    HtmlGenericControl ulcontent = (HtmlGenericControl)rptrLeftSideMenus.Items[0].FindControl("ulcontent");
                    if (licontent != null && ulcontent != null)
                    {
                        licontent.Attributes.Add("class", "dropdown active");
                        ulcontent.Attributes.Add("style", "display: block;");
                    }
                }
            }
        }

        protected void rptrLeftSideMenus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Int32 CountModule = e.Item.ItemIndex + 1;
                Label lblModuleID = (Label)e.Item.FindControl("lblModuleID");
                Label lblModuleName = (Label)e.Item.FindControl("lblModuleName");
                Label lblRoleID = (Label)e.Item.FindControl("lblRoleID");
                HtmlAnchor LinkModuleName = (HtmlAnchor)e.Item.FindControl("LinkModuleName");
                if (lblModuleID != null && lblRoleID != null && lblModuleName != null && LinkModuleName != null)
                {
                    LinkModuleName.InnerText = "Step " + CountModule + " : " + lblModuleName.Text;
                    DataTable dtSubModule = ServicesFactory.DocCMSServices.Fetch_LeftSide_submodules_By_roleid(Convert.ToInt32(lblRoleID.Text), Convert.ToInt32(lblModuleID.Text));
                    if (dtSubModule != null && dtSubModule.Rows.Count > 0)
                    {
                        Repeater rptrLeftSideSubMenu = (Repeater)e.Item.FindControl("rptrLeftSideSubMenu");
                        if (rptrLeftSideSubMenu != null)
                        {
                            rptrLeftSideSubMenu.DataSource = dtSubModule;
                            rptrLeftSideSubMenu.DataBind();
                        }
                    }
                }
            }
        }

        protected void rptrLeftSideSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblSubModuleID = (Label)e.Item.FindControl("lblSubModuleID");

                if (lblSubModuleID != null)
                {
                    Clssubmodules objSubModule = ServicesFactory.DocCMSServices.Fetch_SubModules_ById(Convert.ToInt32(lblSubModuleID.Text));
                    if (objSubModule != null)
                    {
                        LinkButton lnkSubMenu = (LinkButton)e.Item.FindControl("lnkSubMenu");
                        if (lnkSubMenu != null)
                        {
                            lnkSubMenu.CommandArgument = objSubModule.navigationurl.Trim();
                        }
                    }
                }
            }
        }

        protected void rptrLeftSideSubMenu_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                if (e.CommandName == "NavigateToPage")
                {
                    Label lblSubModuleID = (Label)e.Item.FindControl("lblSubModuleID");
                    if (lblSubModuleID != null)
                    {
                        Session["SubModuleID"] = lblSubModuleID.Text.Trim();
                    }
                    string[] argList = Convert.ToString(e.CommandArgument).Split('=');
                    if (argList.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(argList[1]))
                        {
                            string[] pageID = Convert.ToString(argList[1]).Split('_');
                            if (pageID.Length > 1)
                            {
                                Response.Redirect(".." + Convert.ToString(argList[0]) + "=" + pageID[1]);
                            }
                            else
                            {
                                Response.Redirect(Convert.ToString(argList[0]) + "=" + pageID[0]);
                            }
                        }
                        else
                        {
                            Response.Redirect(".." + Convert.ToString(e.CommandArgument));
                        }
                    }
                    else
                    {
                        Response.Redirect(".." + Convert.ToString(e.CommandArgument));
                    }
                }
            }
        }
    }// Class Ends Here
}