﻿<%@ Register Src="~/Controls/SurveyPopup.ascx" TagName="SurveyPopup" TagPrefix="UC4" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FEHeader.ascx.cs" Inherits="DocCMSMain.Controls.FEHeader" %>
<style type="text/css">
.Btnblinker
{
    margin-left: 515px;
    font-weight: 300;
    text-align: center;
    /* width: 399px; */
    cursor: pointer;
    margin-top: 2px;
}
.SpanBilnker {
    color: blue !important;
    animation: blink 1s steps(2, start) infinite;
}
#divbtnNotification .SpanBilnker
{
    margin-top: 5px;
    background: yellow;
    font-weight: bold;
    border-radius: 4px;
}
</style>
<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt main-header" role="banner">
      <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                    
                </button>
                <a class="navbar-brand" href="home.html"><img  runat="server" id="ImgLogo" width="126" alt="logo"></a>
            </div>
            <div id="divSitePages" runat="server" clientidmode="Static" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                <asp:Repeater ID="rptrMainMenu" runat="server" ClientIDMode="Static" 
                        onitemdatabound="rptrMainMenu_ItemDataBound" OnItemCommand="rptrMainMenu_ItemCommand">
                <ItemTemplate>
  
                 <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                    <li id="liMenuName" runat="server" clientidmode="Static">
                    <asp:LinkButton ID="lnkNavigation" runat="server" CommandName="NavigateToURL" ClientIDMode="Static"></asp:LinkButton>
                    </li>                    
                    <li class="dropdown" id="LiMainSubMenu" runat="server" clientidmode="Static" visible="false">                     
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Literal ID="LitMenuName" runat="server" ClientIDMode="Static"></asp:Literal> <i class="icon-angle-down"></i></a>
                  <ul class="dropdown-menu">
                    <asp:Repeater ID="rptrSubMenu" runat="server" ClientIDMode="Static"  onitemdatabound="rptrSubMenu_ItemDataBound" OnItemCommand="rptrSubMenu_ItemCommand" >
                    <ItemTemplate>                    
                            <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblMainMenuName" runat="server" ClientIDMode="Static" Text='<%# Eval("MainMenuName") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblSubMenuName" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuName") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblSubmenuNavURL" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuNavigationURL") %>' Visible="false"></asp:Label>
                          <li class="sub-menu">
                          <asp:LinkButton ID="lnkSubmenu" runat="server" CommandName="SubNavigateToURL" ClientIDMode="Static"></asp:LinkButton> 
                          <div id="divSubMenuLevel1" runat="server">
                          <a href='<%# Eval("SubMenuNavigationURL") %>' class="dropdown-toggle" ><asp:Literal ID="LitSubMenuLevel1" runat="server" ClientIDMode="Static"></asp:Literal> <i class="icon-angle-down"></i></a>
                          </div>
                          
                           <ul class="nestedmenu" style="left: 0;position: relative;top: 0;">
                            <asp:Repeater ID="rptrSubMenuLevel2" runat="server" ClientIDMode="Static"  onitemdatabound="rptrSubMenuLevel2_ItemDataBound" OnItemCommand="rptrSubMenuLevel2_ItemCommand" >
                            <ItemTemplate>     
                               <asp:Label ID="lblSubMenuLevel2Name" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuName") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblSubLevel2CMSId" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblSubmenuNavURLLevel2" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuNavigationURL") %>' Visible="false"></asp:Label>
                                 <li style="float:left;width:100%;padding-left:25px;"> <asp:LinkButton ID="lnkSubmenuLevel2" runat="server" CommandName="SubLevel2NavigateToURL" ClientIDMode="Static"></asp:LinkButton> </li>
                                  </ItemTemplate>
                                  </asp:Repeater>
                                </ul>                         
                          </li>                    
                    </ItemTemplate>
                    </asp:Repeater>                   
                    </ul>
                    </li>
                </ItemTemplate>   
                </asp:Repeater>  
                <div class="blinker" onclick="return Popup();" id="divSurveyNotification" style="display:none" runat="server" clientidmode="Static">                
                Hello! we have a survey 
                </div> 
                   <div class="Btnblinker"  id="divbtnNotification"  runat="server" clientidmode="Static">                
               </div> 
                
                </ul> 
            </div>
            <div id="divAccessPage" runat="server" clientidmode="Static" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                <li id="liHome" runat="server" clientidmode="Static" >                    
                  <a href="Home.htm" >Home</a>
                </li >
                    <li id="liLogout" runat="server" clientidmode="Static">                    
                  <asp:LinkButton ID="lnkLogOut" runat="server" ClientIDMode="Static" Text="Sign Out"   onclick="lnkLogOut_Click"  ></asp:LinkButton>
                    </li >
                </ul>
                </div>
        </div>
    </header>
    <script src="../DFI/js/Pages/SurveyPopup.js" type="text/javascript"></script>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/Pages/SurveyPopup.js" type="text/javascript"></script>
<asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
<UC4:SurveyPopup id="surveyHome" runat="server"></UC4:SurveyPopup>
  <style type="text/css">
#divSurveyNotification {
color: blue;
animation: blink 1s steps(2, start) infinite;
}
 @keyframes blink {
to { color: red; }
}
  </style>

  <script type="text/javascript">
        function Popup() {
            $("#cover").show();
            $("#userPoll").show();
            Check_Exist_IP_ByID();
        }
        $(document).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $("#userPoll").css("top", ($(document).scrollTop() + 135).toString() + "px");
            }
        });
        function DisplayIP() {
            document.getElementById("hdnClientIPAddress").value = myip;
            IPAddress = myip;
      };
  </script>
    <script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>
       
<!--/header-->
