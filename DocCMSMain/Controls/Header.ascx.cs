﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DocCMSMain.Controls
{
    public partial class Header : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // do nothing
                if (Session["UserID"] != null && Session["FName"] != null)
                {
                    lblUserName.InnerHtml = Convert.ToString(Session["FName"]);
                }
            }
        }

        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Session["UserID"] = null;
            Session["SubModuleID"] = null;
            Response.Redirect("../cadmin/default.aspx");
        }
    }//== CLASS ENDS HERE 
}