﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.Controls
{
    public partial class NewsFeed : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_NewsFeed(41);
        }
        // bind news feed 
        protected void Bind_NewsFeed(Int32 PageID)
        {
            try
            {
                string textSize = "";
                DataTable dtNews = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtNews != null && dtNews.Rows.Count > 0)
                {
                    StringBuilder strnews = new StringBuilder();
                    strnews.Append("<marquee id='Marquee2' onmouseover='this.stop()' onmouseout='this.start()' direction='left'>");
                    for (Int32 i = 0; i < dtNews.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtNews.Rows[i]["CMSID"]), "FieldValue");
                        if (Convert.ToString(dtDetail.Rows[2]["FieldValue"]).ToUpper() == "BOTH")
                        {
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                          
                                if (!string.IsNullOrEmpty(Convert.ToString(dtDetail.Rows[3]["FieldValue"])))
                                {
                                    if (string.IsNullOrEmpty(textSize))
                                    {
                                        switch (Convert.ToString(dtDetail.Rows[3]["FieldValue"]).ToUpper())
                                        {
                                            case "SMALL":
                                                textSize = "16px";
                                                break;
                                            case "MEDIUM":
                                                textSize = "26px";

                                                break;
                                            case "LARGE":
                                                textSize = "36px";
                                                break;
                                        }
                                    }
                                }
                            }
                            string NewsDescription = Convert.ToString(dtDetail.Rows[1]["FieldValue"]);
                            strnews.Append(NewsDescription);
                            markeepanel.Style.Add("font-size", textSize.ToString());
                        }
                    }
                    strnews.Append("</marquee>");
                    markeepanel.InnerHtml = strnews.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}