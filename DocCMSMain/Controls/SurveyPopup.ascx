﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SurveyPopup.ascx.cs" Inherits="DocCMSMain.Controls.SurveyPopup" %>

<link href="../DFI/css/Popupstylesheet.css" rel="stylesheet" type="text/css" />
<link href="../DFI/css/UserVotingStyleSheet.css" rel="stylesheet" type="text/css" />
<script src="../DFI/js/jquery.js" type="text/javascript"></script>
<style type="text/css">
.popupquesreport
{
    background-color:yellowgreen;
}
.skill-3::-webkit-progress-value {
  background: #ff6b6b;
}

.skill-3::-moz-progress-bar {
  background: #ff6b6b;
}
.progress
{
  margin-bottom: 0px !important; 
  width: 88%;
  height: 18px !important;
  background: white;
  float:left;
}

.progress-bar
{
    background-color: #299629;
  height: 40px;
 
}
.PercentValue
{
    width: 12%;
    float: right;
    text-align: right; 
}
</style>
<asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
<div id="DivMainPopup" runat="server" clientidmode="Static" style="display: none;">
    <div id="userPoll">
        <div class="white">
            <div class="blue-sec" id="DivSurvey" runat="server" clientidmode="Static">
            </div>
            <p class="paraalign">
                DOCFOCUS</p>
        </div>
        <span class="cancel" onclick="CloseSurvey();">&times;</span>
    </div>
    <div id="cover">
    </div>
</div>
<div id="DivResultPopup" runat="server" clientidmode="Static" style="display: none;">
    <div id="ResultPoll">
        <div class="white">
            <div class="blue-sec" id="DivResult" runat="server" clientidmode="Static">
            <div id="divResultQuestion">
            </div>
            </div>
            <p class="paraalign">
                DOCFOCUS</p>
        </div>
        <span class="cancel" onclick="CloseSurveyResult();">&times;</span>
    </div>
    <div id="cover2">
    </div>
</div>
