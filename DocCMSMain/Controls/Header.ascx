﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="DocCMSMain.Controls.Header" %>
<div class="header" id="header" runat="server">
    <div class="logo">
        <a href="../cadmin/ContentManagementDashboard.aspx?PID=41">
            <img src="../images/Logos/DocLogo2.png" alt="" style="height: 70px; margin-top: -20px" /></a>
    </div>
    <div class="headerinner">
        <ul class="headmenu">
            <li class="right">
                <div class="userloggedinfo">
                    <div class="userinfo" style="color: #000;">
                        <h5>
                            Welcome !<small><label runat="server" id="lblUserName" style="color: #000;"></label></small></h5>
                        <ul>
            </li>
            <li>
                <asp:LinkButton ID="lnkLogOut" runat="server" ClientIDMode="Static" Text="Sign Out"
                    OnClick="lnkLogOut_Click"></asp:LinkButton>
            </li>
        </ul>
    </div>
</div>
</li> </ul><!--headmenu-->
</div> </div>
<style type="text/css">
    #lnkLogOut
    {
        background: #6e6e6e none repeat scroll 0 0;
        border-radius: 4px;
        color: #fff;
        text-align: center;
    }
    #lnkLogOut:hover
    {
        background: #949494 none repeat scroll 0 0;
        border-radius: 4px;
        color: #000;
        text-align: center;
        transition: 0.3s;
    }
</style>
