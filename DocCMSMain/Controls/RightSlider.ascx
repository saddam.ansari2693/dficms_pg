﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RightSlider.ascx.cs" Inherits="DocCMSMain.Controls.RightSlider" %>
<link href="../css/CustomRightSlider.css" rel="stylesheet" type="text/css" />
<!--slider div-->
<div class="container RMASlider_Slider_Container" >
   <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators" runat="server" id="crouselIndicators">
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox" runat="server" id="RightSliderDiv">
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="margin-top: 150%;">
            </span><span class="sr-only">Previous</span> </a><a class="right carousel-control"
                href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"
                    aria-hidden="true" style="margin-top: 150%;"></span><span class="sr-only">Next</span>
            </a>
    </div>
</div>
<!--button div-->
<div class="RMASlider_Cust_button" >
<div class="col-lg-6 RMASlider_Cust_button_Child1" >
    <ul class="list-unstyled" runat="server" id="ulSpecialButtons">
    </ul>
    <div id="divTextSection" class="TextRightSection" runat="server">
     </div>
     </div>
     </div>
