﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftSideBar_OLD.ascx.cs"
    Inherits="DocCMSMain.Controls.LeftSideBar_OLD" %>
<div class="leftpanel">
    <div class="leftmenu">
        <ul class="nav nav-tabs nav-stacked">
            <li class="nav-header" id="liheader" runat="server">Navigation</li>
            <li class="dropdown" id="licontent" runat="server"><a href=""><span class="iconfa-pencil">
            </span>Step 1: Admin Pages</a>
                <ul id="ulcontent" runat="server">
                    <li><a href="../cadmin/ContentPageDashboard.aspx">Content Pages</a></li>
                    <li><a href="#">Sub Menu(Level-1)</a></li>
                    <li><a href="#">Sub Menu(Level-2)</a></li>
                </ul>
            </li>
            <li class="dropdown" id="lihyperlink" runat="server"><a href="#"><span class="iconfa-book">
            </span>Step 2: HyperLinks</a>
                <ul id="ulhyperlink" runat="server">
                    <li><a href="../cadmin/ContentManagementDashboard.aspx?PID=Main Menu">Main Menu</a></li>
                    <li><a href="../cadmin/ContentManagementDashboard.aspx?PID=Sub Menu(Level-1)">Sub Menu(Level-1)</a></li>
                    <li><a href="../cadmin/ContentManagementDashboard.aspx?PID=Sub Menu(Level-2)">Sub Menu(Level-2)</a></li>
                </ul>
            </li>
            <li class="dropdown" id="litemplate" runat="server"><a href=""><span class="iconfa-picture">
            </span>Step 3: Template Mgmt.</a>
                <ul id="ultemplate" runat="server">
                    <li><a href="/Modules/Homepage.aspx">Home Page</a></li>
                </ul>
            </li>
            <li class="dropdown" id="liadministration" runat="server"><a href=""><span class="iconfa-hand-up">
            </span>Step 4: Administration</a>
                <ul id="uladministration" runat="server">
                    <li><a href="/Cadmin/UserRolesDashboard.aspx">User Roles</a></li>
                    <li><a href="/Cadmin/ModuleDashboard.aspx">Module Dashboard </a></li>
                    <li><a href="/Cadmin/SubModulesDashboard.aspx">Sub Module Dashboard </a></li>
                    <li><a href="/cadmin/PrivilegesDashboard.aspx">Privilege Dashboard </a></li>
                    <li><a href="../cadmin/UserDashboard.aspx">Users Accounts</a></li>
                    <li><a href="../cadmin/Assign_Privileges.aspx">Assign Privileges</a></li>
                    <li><a href="../cadmin/ChangePassword.aspx">Change Password</a></li>
                </ul>
            </li>
            <li class="dropdown" id="li1" runat="server"><a href="#"><span class="iconfa-book"></span>
                Step 5: Media Mgmt.</a>
                <ul id="ul1" runat="server">
                    <li><a href="../cadmin/MediaManagementDashboard.aspx?tid=1">Image Management</a></li>
                    <li><a href="../cadmin/MediaManagementDashboard.aspx?tid=4">Doc. Management</a></li>
                    <li><a href="../cadmin/MediaManagementDashboard.aspx?tid=3">Audio Management</a></li>
                    <li><a href="../cadmin/MediaManagementDashboard.aspx?tid=2">Video Management</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--leftmenu-->
</div>
