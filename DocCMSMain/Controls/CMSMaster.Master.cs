﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Configuration;
using System.Web.Configuration;

namespace DocCMSMain.Controls
{
    public partial class CMSMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Timeout();
            if (!IsPostBack)
            {
                string url = Request.Url.AbsoluteUri.ToString();
                if (url.Contains("ForgotPassword.aspx"))
                {
                    // Do nothing
                }
                else
                {
                    if (Session["UserID"] != null)
                    {
                        // Do nothing
                    }
                    else
                    {
                        Response.Redirect("~/cadmin/Default.aspx");
                    }
                }
            }
        }

        // Session timeout
        public void Timeout()
        {
            int timeout = 0;
            DataTable dtTime = ServicesFactory.DocCMSServices.Fetchtimeout();
            if (dtTime != null && dtTime.Rows.Count > 0)
            {
                int Time = Convert.ToInt16(dtTime.Rows[0]["Timeout"]);
                if (Time != 0)
                {
                    timeout = (int)Time * 1000 * 60;
                }
                else
                {
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                    SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                    timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                }
            }
            else
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
        }

    }//== CLASS ENDS HERE 
}