﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.Controls
{
    public partial class LeftSlider : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string[] PageNameArray = path.Split('/');
            string PageName = PageNameArray.Last();
            Int32 SliderId = 0;
            if (PageName.ToLower() == "default.aspx")
            {
                SliderId = ServicesFactory.DocCMSServices.Fetch_Slider_sliderid_ForHomePage();
            }
            HiddenField hdnSliderPosition = this.Parent.FindControl("hdnSliderPosition") as HiddenField;
            if (hdnSliderPosition.Value.ToLower() == "left")
            {
                GetSliderPositions(SliderId);
            }
        }

        public void GetSliderPositions(Int32 SliderId)
        {
            StringBuilder strSlider = new StringBuilder();
            StringBuilder strIndicators = new StringBuilder();
            DataTable dtSlider = ServicesFactory.DocCMSServices.Fetch_Slider_image(SliderId);
            if (dtSlider != null && dtSlider.Rows.Count > 0)
            {

                for (Int32 i = 0; i < dtSlider.Rows.Count; i++)
                {
                    string SliderName = Convert.ToString(dtSlider.Rows[0]["SliderName"]);
                    string SliderID = Convert.ToString(dtSlider.Rows[0]["SliderID"]);
                    if (i == 0)
                    {
                        strSlider.Append(" <div class='item active'>");
                        strSlider.Append(" <img src='../UploadedFiles/SliderImage/" + SliderID + "/" + Convert.ToString(dtSlider.Rows[i]["SliderItemID"]) + "/LEFT_" + Convert.ToString(dtSlider.Rows[i]["ImageName"]) + "'");
                        strSlider.Append(" alt='Image'/> ");                        
                        strSlider.Append(" </div>");
                        strIndicators.Append("<li data-target='#myCarousel' data-slide-to='" + i + "' class='active'></li>");
                    }
                    else
                    {
                        strSlider.Append("<div class='item'>");
                        strSlider.Append(" <img src='../UploadedFiles/SliderImage/" + SliderID + "/" + Convert.ToString(dtSlider.Rows[i]["SliderItemID"]) + "/LEFT_" + Convert.ToString(dtSlider.Rows[i]["ImageName"]) + "'");
                        strSlider.Append(" alt='Image'/> ");
                        strSlider.Append("</div>");
                        strIndicators.Append("<li data-target='#myCarousel' data-slide-to='" + i + "' class=''></li>");
                    }
                }
                crouselIndicators.InnerHtml = strIndicators.ToString();
                LeftSliderDiv.InnerHtml = strSlider.ToString();
                Bind_SpecialButton_Or_Text(SliderId, Convert.ToString(dtSlider.Rows[0]["ButtonTextSection"]));
            }
        }

        public void Bind_SpecialButton_Or_Text(Int32 SliderId, string SliderAnotherSection)
        {
            StringBuilder StrSpecialButton = new StringBuilder();
            if (SliderAnotherSection == "Button")
            {
                DataTable dtSpecialButton = ServicesFactory.DocCMSServices.Fetch_Slider_SpecialButton_By_sliderid_Show(SliderId);
                if (dtSpecialButton != null && dtSpecialButton.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtSpecialButton.Rows)
                    {
                        StrSpecialButton.Append("<li><a href='" + dr["NavigationURL"] + "'><input type='button' style='background-color:#" + dr["ButtonColor"] + ";' value='" + dr["ButtonText"] + "' class='sliderbutton3 btn-responsive' /></a></li>");
                    }
                    ulSpecialButtons.InnerHtml = StrSpecialButton.ToString();
                    divTextSection.Style.Add("display", "none");
                }
            }
            else if (SliderAnotherSection == "Content")
            {
                string SliderContainText = ServicesFactory.DocCMSServices.Fetch_Slider_Contain_Text_By_sliderid_Show(SliderId);
                if (SliderContainText != null && SliderContainText != "")
                {
                    divTextSection.InnerHtml = SliderContainText;
                }
                ulSpecialButtons.Style.Add("display", "none");
            }
            else
            {
            }
        }
    }
}