﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePageSlider_New.ascx.cs"
    Inherits="DocCMSMain.Controls.HomePageSlider_New" %>
<div class="mstslider">
    <div class="master-slider ms-skin-default" id="masterslider" runat="server" clientidmode="Static">
    </div>
    <style type="text/css">
.animate-spin:hover
{
        animation: infinite-spinning 1s infinite;
}
        
@keyframes infinite-spinning {
from {
transform: rotate(0deg);
}
to {
transform: rotate(360deg);
}
}

.animate-spin-horizontal:hover
{
    animation: infinite-spinning-new 2s infinite;
}

@keyframes infinite-spinning-new {
        from {
        transform: rotateY(0deg);
            }
        to {
        transform: rotateY(360deg);
            }
}

.test-text
{
    display:none;
    background:rgba(0,0,0,1);
    padding:5px 8px;
    border-radius:5px;
    position:relative;
    transition: display .2s;
}

.testing
{
    background:url('../DFI/images/circle-sq.png');
    width:500px;
    height:210px;
    transition:all .2s;
    display:none;
    animation:round 1s;
    }

.test-img:hover + h3.testing
{
    display:block;
}

@keyframes round {
        from {
        transform: rotateZ(-65deg);
            }
        to {
        transform: rotateZ(0deg);
            }
}

.test-text:after
{
    position:absolute;
    content:'';
    width:19px;
    height:19px;
    right:-9px;
    /*background:rgba(0,0,0,.80);*/
    top:35%;
    transform:rotate(45deg);
    z-index:-1;
    transition:all .1s;
}

.test-img:hover + h3.test-text
{
    display:block;
}
</style>
</div>
