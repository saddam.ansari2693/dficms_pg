﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FEFooter.ascx.cs" Inherits="DocCMSMain.Controls.FEFooter" %>
<section id="bottom" class="footer-top">
            <div class="container">
                <div class="row">
                  <!--About Us Starts Here-->
                    <div class="col-md-3 col-sm-6" id="DivAboutUs" runat="server">
                    </div>
                  <!--About Us End Here-->
                  <!--Footer Menu Starts Here-->
                    <div class="col-md-3 col-sm-6">
                        <h4>Menu Overview</h4>
                        <div>
                          <ul class="arrow">
                <asp:Repeater ID="rptrFooterMenu" runat="server" ClientIDMode="Static" 
                            onitemcommand="rptrFooterMenu_ItemCommand" 
                            onitemdatabound="rptrFooterMenu_ItemDataBound">
                <ItemTemplate>
               <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                    <li id="liMenuName" runat="server" clientidmode="Static">
                    <asp:LinkButton CssClass="divfooterBlog" ID="lnkNavigation" runat="server" CommandName="NavigateToURL" ClientIDMode="Static"></asp:LinkButton>
                    </li>         
                </ItemTemplate>   
                </asp:Repeater>
                    </ul>
                        </div>
                    </div>
                     <!--Footer Menu Ends Here-->
                      <!--Latest Blog Starts Here-->
                         <div class="col-md-3 col-sm-6">
                        <h4>Latest Blog</h4>
                        <div  runat="server" id="DivBlog">
                        </div>  
                    </div>
                    <!--Latest Blog Ends Here-->
                      <!--For Address and Newsletter Div-->
                    <div class="col-md-3 col-sm-6" >
                     <!--Address Starts Here-->
                        <div id="DivAddress" runat="server"> </div> 
                        <!--Address Ends Here-->
                          <!--NewsLetter Starts Here-->
                        <div id="DivNewsletter" runat="server">
                        <h4>Newsletter</h4>
                        <form role="form">
                            <div class="input-group">
                                <input type="text" class="form-control" autocomplete="off" placeholder="Enter your email">
                                <span class="input-group-btn">
                                    <button class="btn btn-success" type="button">Go!</button>
                                 </span>
                              </div>
                           </form>
                       </div>
                   </div> 
                </div>
            </div>
        </section>
        <footer id="footer" class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="copyright">&copy; <asp:Label id="labyear" runat="server" Text=""></asp:Label> <a style="color: #fff; text-decoration:none; cursor:pointer;"  href="Home.htm" title="DOCFOCUS">DOCFOCUS</a>. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
<!--/#footer-->
<!--/#bottom-->
