﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KeyElements.ascx.cs" Inherits="DocCMSMain.Controls.KeyElements" %>
    <section class="dms-section">
            <div class="container" >
                <h1>Keys Element of DMS <hr class="fancy-divider"></h1>
                <div class="row" id="DivKeyElements" runat="server" >                
                   </div>
            </div>
        </section>
<script language="javascript" type="text/javascript">
    function changeImageIn(paramsval) {    
        var paraArray = paramsval.toString().split('~');        
        if (paraArray.length > 1) {
            var DivID = paraArray[0];
            var imagePath = paraArray[1];
            $("#" + DivID).css("background", "#11A48D  url(" + imagePath + ") no-repeat");
            $("#heading_" + DivID).css("color","#11A48D");
        }
    }
    function changeImageOut(paramsval) {
        var paraArray = paramsval.toString().split('~');
        if (paraArray.length > 1) {
            var DivID = paraArray[0];
            var imagePath = paraArray[1];
            $("#" + DivID).css("background", "#FFF  url(" + imagePath + ") no-repeat");
            $("#heading_" + DivID).css("color", "#34495E");
        }
    }
</script>