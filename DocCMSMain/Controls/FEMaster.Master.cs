﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Net;

namespace DocCMSMain.Controls
{
    public partial class FEMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RetrieveLiveChatManager();
            }
        }

        //Retrieve Live chat Manager Status
        public void RetrieveLiveChatManager()
        {
            string Status = "";
            Status = ServicesFactory.DocCMSServices.BindChatStatus();
            hndLivechatmanager.Value = Status;
        }
    }
}