﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="demo.aspx.cs" Inherits="DocCMSMain.demo" %>
<%@ Register Src="~/Controls/FullWidthSlider.ascx" TagName="FullWidthSlider" TagPrefix="UcFullSlider" %>
<%@ Register Src="~/Controls/RightSlider.ascx" TagName="RightSliderSlider" TagPrefix="UcSliderRight" %>
<%@ Register Src="~/Controls/LeftSlider.ascx" TagName="LeftSlider" TagPrefix="UcSliderLeft" %>
<%@ Register Src="~/Controls/SocialMedia.ascx" TagName="SocialMedia" TagPrefix="UC2" %>
<%@ Register Src="~/Controls/KeyElements.ascx" TagName="KeyElements" TagPrefix="UC3" %>
<%@ Register Src="~/Controls/SurveyPopup.ascx" TagName="SurveyPopup" TagPrefix="UC4" %>
<%@ Register Src="~/Controls/NewsFeedTop.ascx" TagName="NewsFeedTop" TagPrefix="UC6" %>
<%@ Register Src="~/Controls/NewsFeedBottom.ascx" TagName="NewsFeedBottom" TagPrefix="UC7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/Pages/SurveyPopup.js" type="text/javascript"></script>
    <style type="text/css">
      .fa-twitter
   {
       color:#0084b4;
   }
   .fa-facebook
   {
       color:#3b5998;
   }
   .fa-google-plus
   {
       color:#d34836;
   }
   .fa-linkedin
   {
       color:#0077B5;
   }
   #ContentPlaceHolder1_DivProjectCategory .col-md-4
   {
       min-height:420px !important;
   }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="divPublished"  runat="server" clientidmode="Static">
    <UC6:NewsFeedTop ID="NewsFeedTopNew" runat="server" Visible="false"></UC6:NewsFeedTop>
   <section class="feature_section1" id="SilderSection" style="">
    <UcFullSlider:FullWidthSlider ID="FullWidthSlider" runat="server" Visible="false" ></UcFullSlider:FullWidthSlider>
     <UcSliderRight:RightSliderSlider ID="RightSliderSlider" runat="server" Visible="false"></UcSliderRight:RightSliderSlider>
      <UcSliderLeft:LeftSlider ID="LeftSlider" runat="server" Visible="false"></UcSliderLeft:LeftSlider>
      <asp:HiddenField ID="hdnSliderPosition" runat="server"></asp:HiddenField>
   </section>
    <UC2:SocialMedia ID="SocialMedia" runat="server" Visible="false"></UC2:SocialMedia>
    <!-- Product Section Starts Here -->
    <section class="product" id="SectionProduct" runat="server" visible="false">
        <div class="container">
            <div class="center">
                <h2 id="HeadingProduct"><strong>DFI Products and Services</strong></h2>
            </div>
            <div class="gap"></div>
            <div class="row" id="DivProducts" runat="server" style="display:none">
            </div>
               <div  id="DivProjectCategory" runat="server">
                 </div>
        </div>
    </section>
    <!-- Product Section Ends here -->
    <!-- Pricing Table Section Starts Here -->
    <section class="feature_section1" id="SectionPlan" visible="false" runat="server">
        <div class="container">
       <div class="">
            <div class="pricing-tables-main">
                        <div class="center">
                            <h2>Pricing <strong>Tables</strong></h2>
                        </div>
                        <div class="gap"></div>
                        <div id="DivPriceTable" runat="server"></div>
            </div>
         </div>
     </div>
    </section>
    <!-- Pricing Table Section Ends Here -->
    <!-- Testimonial Section Starts Here -->
    <section id="testimonial" visible="false" class="testimonial" runat="server">
        <div class="container">
            <div class="center">
                <h2>What Our <strong>Client's Says</strong></h2>
            </div>
            <div class="gap"></div>
            <div class="row">
            <div class="col-md-12" data-wow-delay="0.2s">
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel" >
                            <!-- Bottom Carousel Indicators -->
                            <ol class="carousel-indicators" id="LstIndicator" runat="server">
                            </ol>
                           <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner text-center" id="DivClient" runat="server">
                            </div>
                            <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section ends Here -->
    <!-- Client Slider Section Starts Here -->
    <section class="client-slider" visible="false" id="SectionPartner" runat="server">
        <div class="container" id="DivPartners" runat="server">
        </div>
        <div class="customNavigation">
  <a class="btn prev"><i class="fa fa-chevron-left"></i></a>
  <a class="btn next"><i class="fa fa-chevron-right"></i></a>
  </div>
    </section>
<UC7:NewsFeedBottom ID="NewsFeedBottomNew" runat="server" Visible="false"></UC7:NewsFeedBottom>
<asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
<UC4:SurveyPopup id="surveyHome" runat="server" Visible="true"></UC4:SurveyPopup>
</div>
 <div id="divunpublished" runat="server" visible="false" align="center">
     <div class="center">
      <h2 id="H1">The Page is under construction</h2>
     </div>
     <div class="gap"></div>
   <img src="images/underConstruction.jpg" alt="" />
   <div class="gap"></div>
   <div class="center">
   <h2 id="H2">Comming Soon...</h2>
   </div>
   </div>
  <script type="text/javascript">
      var IPAddress = "";
      $(document).ready(function () {
          DisplayIP();
          $("#cover").hide();
          $("#userPoll").hide();
          Check_Exist_IP_ByID();
          Bind_Survey();
      });
      function DisplayIP() {
          document.getElementById("hdnClientIPAddress").value = myip;
          IPAddress = myip;
      };
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var DateTime = Date.now();
        alert(DateTime);
    });
</script>
<script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>
</asp:Content>