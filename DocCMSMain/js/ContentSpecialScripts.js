﻿var unsavedContent = null;
var postTimer = null;
function FillObjectFromForm(obj) {
    obj.Id = jQuery('#hdnContentID').val();   
    obj.Active = jQuery('#chkactive').is(':checked');
    //==========FOR THE PURPOSE OF SETTING THE VALUES IN EDITOR ON CONDITION =============================
    obj.IsBasicContent = jQuery('#chkbasiccontent').is(':checked');
    if (obj.IsBasicContent == true) {
        obj.ContentText = jQuery('#txtareabasic').val();
    }
    else {
        obj.ContentText = tinyMCE.activeEditor.getContent();
    }
    //=================================================================
    obj.FromDate = jQuery('#datepicker').val();
    obj.MetaDescription = jQuery('#txtMetaDescription').val();
    obj.MetaKeywords = jQuery('#txtMetaKeyword').val();
    obj.PageScripts = jQuery('#txtpagecode').val();
    obj.PageTitle = jQuery('#txttitle').val();
    obj.Published = jQuery('#chkpublished').is(':checked');
    obj.Searchable = jQuery('#chkSearchable').is(':checked');
    obj.SibeBarID = jQuery('#drpSideBarpage').val();
    obj.ContentOwnerId = jQuery('#drpContentOwner').val();
    obj.UntilDate = jQuery('#until_datepicker').val();
}

function FillFormFromObject(obj) {
    jQuery('#hdnContentID').val(obj.Id);
    if (obj.Active == true)
        jQuery('#chkactive').prop("checked", true).uniform();
    else {
        jQuery("#chkactive").prop('checked', false).uniform();
    }

    //==========FOR THE PURPOSE OF SETTING THE VALUES IN EDITOR ON CONDITION =============================
    if (obj.IsBasicContent == true) {
        jQuery('#chkbasiccontent').prop("checked", true).uniform();
        jQuery('#txtareabasic').val(obj.ContentText);
    }

    else {
        jQuery("#chkbasiccontent").prop('checked', false).uniform();
        tinyMCE.activeEditor.setContent(obj.ContentText);
    }  
    //=====================================================================================================

   
    jQuery('#datepicker').val(obj.FromDate);
    jQuery('#txtMetaDescription').val(obj.MetaDescription);
    jQuery('#txtMetaKeyword').val(obj.MetaKeywords);
    jQuery('#txtpagecode').val(obj.PageScripts);
    jQuery('#txttitle').val(obj.PageTitle);

    if (obj.Published == true)
        jQuery('#chkpublished').prop("checked", true).uniform();
    else
        jQuery('#chkpublished').prop('checked', false).uniform();

    if (obj.Searchable == true)
        jQuery('#chkSearchable').prop("checked", true).uniform();
    else
        jQuery('#chkSearchable').prop('checked', false).uniform();

    jQuery('#drpSideBarpage').val(obj.SibeBarID);
    jQuery('#drpContentOwner').val(obj.ContentOwnerId);
    jQuery('#until_datepicker').val(obj.UntilDate);
}

function SaveContentState() {
    var pageData = {};
    if (unsavedContent == null) {
        alert("No previous unsaved content");
        unsavedContent = {};
        FillObjectFromForm(unsavedContent);
    }
    else {
        FillObjectFromForm(pageData);
        if (IsObjectMatch(pageData, unsavedContent) == false) {
            var contentId = parseInt(pageData.Id) || 0;
            if (contentId > 0) {
                var tmp = Math.floor(Math.random() * 100000);
                var url = location.protocol + "//" + location.host + "/ajax/CommonServices.aspx?service=saveunsavedcontent&t=" + tmp;
                jQuery.ajax({
                    type: 'POST',
                    url: url,
                    data: pageData,
                    success: function (data) {
                        if (data == "1") {
                            FillObjectFromForm(unsavedContent);
                        }
                    }
                });
            }
        }
        else {
        }
    }
}

function IsObjectMatch(pageData, unsavedData) {
    var matched = true;
    if (pageData != null && unsavedData != null) {
        if (pageData.Id != unsavedData.Id) {
            matched = false;
        }
        if (pageData.Active != unsavedData.Active) {
            matched = false;
        }
        if (pageData.ContentText != unsavedData.ContentText) {
            matched = false;
        }
        if (pageData.FromDate != unsavedData.FromDate) {
            matched = false;
        }
        if (pageData.MetaDescription != unsavedData.MetaDescription) {
            matched = false;
        }
        if (pageData.MetaKeywords != unsavedData.MetaKeywords) {
            matched = false;
        }
        if (pageData.PageScripts != unsavedData.PageScripts) {
            matched = false;
        }
        if (pageData.PageTitle != unsavedData.PageTitle) {
            matched = false;
        }
        if (pageData.Published != unsavedData.Published) {
            matched = false;
        }
        if (pageData.Searchable != unsavedData.Searchable) {
            matched = false;
        }
        if (pageData.SibeBarID != unsavedData.SibeBarID) {
            matched = false;
        }
        if (pageData.UntilDate != unsavedData.UntilDate) {
            matched = false;
        }
        if (pageData.ContentOwnerId != unsavedData.ContentOwnerId) {
            matched = false;
        }
    }
    return matched;
}

function GetUnsavedContent() {
    postTimer = null;
    var tmp = Math.floor(Math.random() * 100000);
    var Id = jQuery('#hdnContentID').val();
    var url = location.protocol + "//" + location.host + "/ajax/CommonServices.aspx?service=getunsavedcontent&t=" + tmp;
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: { id: Id },
        success: function (data) {
            if (data != "") {
                unsavedContent = eval("(" + data + ")");
                jConfirm("There was previous unsaved content. Do you want to restore the previous unsaved content?", "", function (r) {
                    if (r == true) {
                        FillFormFromObject(unsavedContent);
                        FillObjectFromForm(unsavedContent);
                        var urlDel = location.protocol + "//" + location.host + "/ajax/CommonServices.aspx?service=deleteunsavedcontent&t=" + tmp;
                        jQuery.post(urlDel, { id: Id }, function (dataDel) {
                        });
                    }
                    StartTimer();
                });
            }
            else {
                StartTimer();
            }
        }
    });
}

function StartTimer() {
    postTimer = setInterval(function () { SaveContentState() }, 6000 * 5); // 6000 * 5 10sec
}

function InitContentMonitor() {
    if (unsavedContent == null) {
        unsavedContent = {};
        FillObjectFromForm(unsavedContent);
    }
    GetUnsavedContent();
}





