﻿//----------------- URLS--------------------------
urlInsert_Roles_Privileges = "../WebService/DocCMSApi.svc/Insert_Roles_Privileges";
//----------------- END URLS--------------------------

function Insert_Privileges(ViewValue) {
    var roleID = jQuery("#hdnRoleID").val().toString();
    var privList = ViewValue.toString();
    jQuery.ajax({
        contentType: "application/json; charset=utf-8",
        url: urlInsert_Roles_Privileges,
        data: { "roleid": roleID.toString(),
            "privList": privList.toString()
        },
        dataType: "json",
        success: function (data) {
            alert("Privileges Assigned to this Role Successfully Updated. ");
            window.location = "../cadmin/Assign_Privileges.aspx";
        },
        error: function (result) {
            alert("Error during operatrion");
        }
    });
}