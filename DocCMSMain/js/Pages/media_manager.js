﻿// For Image Upload
jQuery(function () {
    jQuery("#ImageUploadControl").change(function () {
        jQuery("#dvPreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (jQuery(this).val() == "") {
           jQuery("#imgLogo").attr("src", "/images/image-not-found.png");
        }
        else {
            if (regex.test(jQuery(this).val().toLowerCase())) {
                if (jQuery.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                    jQuery("#imgLogo").show();
                    jQuery("#imgLogo")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = jQuery(this).val();
                }

                else {
                    if (typeof (FileReader) != "undefined") {
                        jQuery("#imgLogo").show();
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            jQuery("#imgLogo").attr("src", e.target.result);
                        }
                        reader.readAsDataURL(jQuery(this)[0].files[0]);
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                }
            }
            
             else {
                alert("Please upload a valid image file.");
            }
        }
    });

});
// For File Upload
jQuery(function () {
    jQuery("#FileUploadControl").change(function () {
        jQuery("#dvPreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.doc|.xls|.pdf|.txt|.cvc)$/;
        var Imageregex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (jQuery(this).val() == "") {
            jQuery("#imgLogo").attr("src", "/images/image-not-found.png");
        }
        else {
            if (regex.test(jQuery(this).val().toLowerCase())) {
                if (jQuery.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                    jQuery("#imgLogo").show();
                    jQuery("#imgLogo")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = jQuery(this).val();
                }

                else {
                    if (typeof (FileReader) != "undefined") {
                        jQuery("#imgLogo").show();
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            jQuery("#imgLogo").attr("src", e.target.result);
                        }
                        reader.readAsDataURL(jQuery(this)[0].files[0]);
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                }
            }
            if (Imageregex.test(jQuery(this).val().toLowerCase())) {
                alert("Please upload a valid Document file.");
            }
        }
    });

});

// To Preview Image on change of FileUploader Control in Content management Page
function Show_UploadedImage(fileInput) {
    var fileinputidval = fileInput.id;    
    var thumbnilid = "";
    var Get_FileinputID = fileinputidval.toString().split('_');
    if (Get_FileinputID.length > 3) {
        thumbnilid = Get_FileinputID[0] + "_" + Get_FileinputID[1] + "_ImgCMSFileUploader_" + Get_FileinputID[3];
    }
   
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var ext = file.name.split('.').pop().toLowerCase();
        var imageType = /image.*/;
        var filetype=file.type
        var img = document.getElementById(thumbnilid.toString());
        img.file = file;
        var reader = new FileReader();
        if (file.type.match(imageType)) {
              reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }
        else if (ext == "pdf") {
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = '../UploadedFiles/pdf.png';
                };
            })(img);
            reader.readAsDataURL(file);
        }
        else if (ext == "doc" || ext=="txt") {
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = '../UploadedFiles/doc.png';
                };
            })(img);
            reader.readAsDataURL(file);
        }

        else if (ext == "xls" || ext == "cvc") {
           reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = '../UploadedFiles/xls.png';
                };
            })(img);
            reader.readAsDataURL(file);
        }
    }
}


