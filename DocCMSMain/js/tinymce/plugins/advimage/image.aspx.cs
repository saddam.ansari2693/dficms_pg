﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using DocCMS.Core.Implementation;
using System.Web.Security;
using System.Data;

namespace DocCMSMain.js.tinymce.plugins.advimage
{
    public partial class image : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            listdocument.Attributes["onChange"] = "Evnet_drpiamge();";
            if (!IsPostBack)
            {
                Get_MediaImageInfo();
            }
        }
        protected void Update_File()
        {
            string DirPath = HttpContext.Current.Server.MapPath("~/UploadEditorFiles");
            string targetFolder = DirPath;
            Create_Folder(targetFolder);

            //================ FOR THE SAVING OF ORIGINAL FILE FIRST ==============
            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFile postedFile = Request.Files[i];
                    string fileExt = Path.GetExtension(postedFile.FileName).ToLower();
                    string fileName = Path.GetFileName(postedFile.FileName);
                    if (fileName != string.Empty &&
                        (fileExt == ".png" || fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".gif" ||
                         fileExt == ".bmp"))
                    {
                        if (File.Exists(targetFolder + "\\" + postedFile.FileName))
                        {
                            File.Delete(targetFolder + "\\" + postedFile.FileName);
                        }
                        postedFile.SaveAs(targetFolder + "\\" + postedFile.FileName);
                        string Scrpath = "../UploadEditorFiles/" + postedFile.FileName;
                        src.Value += Scrpath;
                        alt.Value = postedFile.FileName;
                        title.Value = postedFile.FileName;
                        fileUploadtodb();
                    }
                }
            }
        }

        protected void Create_Folder(string targetFolder)
        {
            if (!Directory.Exists(targetFolder))
            {
                Directory.CreateDirectory(targetFolder);
            }
        }

        protected void uploadImg_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                Update_File();

            }
        }


        protected void Update2_File()
        {
            string DirPath = HttpContext.Current.Server.MapPath("~/UploadEditorFiles");
            string targetFolder = DirPath;
            Create_Folder(targetFolder);

            //================ FOR THE SAVING OF ORIGINAL FILE FIRST ==============
            if (Request.Files.Count > 0 && fileupload2.PostedFile.ContentLength > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files[i].FileName == fileupload2.FileName)
                    {
                        HttpPostedFile postedFile = Request.Files[i];
                        string fileExt = Path.GetExtension(postedFile.FileName).ToLower();
                        string fileName = Path.GetFileName(postedFile.FileName);
                        if (fileName != string.Empty &&
                            (fileExt == ".png" || fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".gif" ||
                             fileExt == ".bmp"))
                        {
                            if (File.Exists(targetFolder + "\\" + postedFile.FileName))
                            {
                                File.Delete(targetFolder + "\\" + postedFile.FileName);
                            }
                            postedFile.SaveAs(targetFolder + "\\" + postedFile.FileName);
                            string Scrpath = "../UploadEditorFiles/" + postedFile.FileName;
                            onmouseoversrc.Value += Scrpath;
                        }
                    }
                }
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.Files.Count > 0 && fileupload2.PostedFile.ContentLength > 0)
            {
                Update2_File();

            }
        }

        protected void Update3_File()
        {
            string DirPath = HttpContext.Current.Server.MapPath("~/UploadEditorFiles");
            string targetFolder = DirPath;
            Create_Folder(targetFolder);

            //================ FOR THE SAVING OF ORIGINAL FILE FIRST ==============
            if (Request.Files.Count > 0 && fileupload3.PostedFile.ContentLength > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files[i].FileName == fileupload3.FileName)
                    {
                        HttpPostedFile postedFile = Request.Files[i];
                        string fileExt = Path.GetExtension(postedFile.FileName).ToLower();
                        string fileName = Path.GetFileName(postedFile.FileName);
                        if (fileName != string.Empty &&
                            (fileExt == ".png" || fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".gif" ||
                             fileExt == ".bmp"))
                        {
                            if (File.Exists(targetFolder + "\\" + postedFile.FileName))
                            {
                                File.Delete(targetFolder + "\\" + postedFile.FileName);
                            }
                            postedFile.SaveAs(targetFolder + "\\" + postedFile.FileName);
                            string Scrpath = "../UploadEditorFiles/" + postedFile.FileName;
                            onmouseoutsrc.Value += Scrpath;
                        }
                    }
                }
            }
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.Files.Count > 0 && fileupload3.PostedFile.ContentLength > 0)
            {
                Update3_File();

            }
        }

        public void fileUploadtodb()
        {
            //string fname = "";


            ////try
            ////{
            ////    if (fileupload1.PostedFile != null && !string.IsNullOrEmpty(fileupload1.PostedFile.FileName))
            ////    {
            ////        fname = fileupload1.PostedFile.FileName;
            ////        string serverpath = Server.MapPath("~/UploadedFiles/image/");
            ////        DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
            ////        bool isDirCreated = directoryInfo.Exists;
            ////        if (!isDirCreated)
            ////        {
            ////            directoryInfo.Create();
            ////        }
            ////        fileupload1.PostedFile.SaveAs(serverpath + @"\\" + fname);
            ////    }

            ////    if (!string.IsNullOrEmpty(fname))
            ////    {
            ////        ClsFileUploader fileUploader = new ClsFileUploader();
            ////        fileUploader.Filename = fname;
            ////        fileUploader.CreationDate = DateTime.Now.ToString();
            ////        fileUploader.FilePath = @"/UploadedFiles/image/";

            ////        fileUploader.FileCategoryId = "1";
            ////        fileUploader.ModificationDate = DateTime.Now.ToString();
            ////        fileUploader.AlternateText = Convert.ToString(fname.ToString().Substring(0, fname.IndexOf('.')));
            ////        fileUploader.Description = Convert.ToString(fname.ToString().Substring(0, fname.IndexOf('.')));
            ////        //==== Memeber ship user =====//
            ////        MembershipUser Muser = Membership.GetUser(true);
            ////        if (Muser != null)
            ////        {
            ////            fileUploader.Uploadedby = Muser.ProviderUserKey.ToString();
            ////        }
            ////        int FileInsertid = ServicesFactory.CalohiiServices.InsertMediaFiles(fileUploader);
            ////        src.Value = "../UploadedFiles/image/" + fname;
            ////        Get_MediaImageInfo();
            ////        if (FileInsertid > 0)
            ////        {
            ////            listdocument.SelectedValue = FileInsertid.ToString();
            ////        }
            ////        //if (Mode == 1)
            ////        //{

            ////        //}
            ////        //if (Mode == 2)
            ////        //{
            ////        //    if (Request.QueryString["id"] != null)
            ////        //    {
            ////        //        id = Convert.ToInt32(Request.QueryString["id"]);
            ////        //    }
            ////        //    fileUploader.Id = id.ToString();
            ////        //    ServicesFactory.CalohiiServices.UpdateMediaFiles(fileUploader);
            ////        //}
            ////    }
            ////    else
            ////    {

            ////        return;
            ////    }
            ////}
            ////catch (Exception ex)
            ////{
            ////    Response.Write(ex.Message);
            ////    //StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            ////}



        }

        public void Get_MediaImageInfo()
        {
            DataTable dtMedia = ServicesFactory.DocCMSServices.Fetch_All_images();
            if (dtMedia != null && dtMedia.Rows.Count > 0)
            {
                listdocument.DataSource = dtMedia;
                listdocument.DataTextField = "FileName";
                listdocument.DataValueField = "Id";
                listdocument.DataBind();
            }
        }

        //protected void listdocument_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(listdocument.SelectedItem.Text))
        //    {
        //        src.Value += "../UploadedFiles/image/" + listdocument.SelectedItem.Text;
        //        alt.Value = listdocument.SelectedItem.Text;
        //        title.Value = listdocument.SelectedItem.Text;
        //    }
        //}

    }//===CLASS ENDS HERE 
}