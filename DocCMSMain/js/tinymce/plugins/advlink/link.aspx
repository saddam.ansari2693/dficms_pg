﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="link.aspx.cs" Inherits="DocCMSMain.js.tinymce.plugins.advlink.link" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">   
    <title>{#advlink_dlg.title}</title>
    <script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.10.3.min.js"></script>

	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="../../utils/mctabs.js"></script>
	<script type="text/javascript" src="../../utils/form_utils.js"></script>
	<script type="text/javascript" src="../../utils/validate.js"></script>
	<script type="text/javascript" src="js/advlink.js"></script>
	<link href="css/advlink.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function Evnet_drptemplate() {
            // alert("Hello");
            var item = jQuery("#ListTemplatePage").val();
            var val = jQuery("#ListTemplatePage  option:selected").val();
            var url = "";
            if (item != null && item != "-1") {
                url = "../calohi/" + item + ".htm";
                var redirectionUrl = GetRedirectedUrl(url);
                jQuery("#href").val(redirectionUrl);
                jQuery("#title").val(item);
            }
            LoadTemplateAnchor(val);
            /*switch (item) {
            case "None":
            case " None":
            jQuery("#href").val("");
            jQuery("#title").val("");
            break;
            case "About":
            case "About Us":
            case "About CalOHII":
            jQuery("#href").val("/calohi/About.aspx");
            jQuery("#title").val("About Us");
            break;
            case "Data-Movement":
            jQuery("#href").val("/calohi/data-movement.aspx");
            jQuery("#title").val("Data-Movement");
            break;
            case "Ehealth-Partners":
            case "Ehealth Resources":
            case "eHealth Resources":
            jQuery("#href").val("/calohi/ehealth_partners.aspx");
            jQuery("#title").val("Ehealth-Partners");
            break;
            case "Generic Secondary":
            jQuery("#href").val("/calohi/generic_secondary.aspx");
            jQuery("#title").val("Generic Secondary");
            break;
            case "Index":
            case "Home":
            case "home":
            jQuery("#href").val("/calohi/home.aspx");
            jQuery("#title").val("Home");
            break;
            case "Patients": case "Patients and Caregivers":
            jQuery("#href").val("/calohi/patients.aspx");
            jQuery("#title").val("Patients");
            break;
            case "Providers":
            jQuery("#href").val("/calohi/Providers.aspx");
            jQuery("#title").val("Provider Secondary");
            break;
            case "Provider Secondary":
            jQuery("#href").val("/calohi/providers_secondary.aspx");
            jQuery("#title").val("Providers");
            break;
            case "State Department":
            jQuery("#href").val("/calohi/state-departments.aspx");
            jQuery("#title").val("State Department");
            break;
            case "Terms and conditions":
            case "Terms and Conditions":
            case "Terms of Use":
            jQuery("#href").val("/calohi/TermsConditions.aspx");
            jQuery("#title").val("Terms of Use");
            break;
            case "Privacy":
            jQuery("#href").val("/calohi/Privacy.aspx");
            jQuery("#title").val("Privacy");
            break;
            case "Contact us":
            case "Contact Us":
            jQuery("#href").val("/calohi/ContactUs.aspx");
            jQuery("#title").val("Contact Us");
            break;

            }*/
        }


        function Evnet_drpcontent() {
            var item = jQuery("#ListContentPage  option:selected").text();
            var val = jQuery("#ListContentPage  option:selected").val();

            if (item != null && item != "-1") {
                // url = "../calohi/ohii-" + item + ".htm/"+jQuery("#ListContentPage").val();
                url = "../calohi/ohii" + jQuery("#ListContentPage").val() + "-" + item + ".htm";
                var redirectionUrl = GetRedirectedUrl(url);
                jQuery("#href").val(redirectionUrl);
                jQuery("#title").val(item);
            }
            else {
                jQuery("#href").val("");
                jQuery("#title").val("");
            }

            LoadContentAnchor(val);

        }


        //----------------
        function LoadTemplateAnchor(tmplt) {

            if (tmplt.length > 0) {
                var tmp = Math.floor(Math.random() * 100000);
                var url = location.protocol + "//" + location.host + "/ajax/CommonServices.aspx?service=anchornames&template=" + tmplt + "&t=" + tmp;
                var optionHtml = "";
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        if (data.length > 0) {
                            var arrAnchorName = data.split(",");
                            for (i = 0, len = arrAnchorName.length; i < len; i++) {
                                optionHtml += '<option value="#' + arrAnchorName[i] + '">' + arrAnchorName[i] + '</option>';
                            }
                        }

                        if (optionHtml.length > 0) {
                            optionHtml = "<option value=''>---</option>" + optionHtml;
                            jQuery("#tpltanchorlist").html(optionHtml);
                            jQuery("#tpltanchorlistrow").show();
                        }
                        else {
                            jQuery("#tpltanchorlistrow").hide();
                        }
                    },
                    async: false
                });

            }
        }
        function LoadContentAnchor(contentId) {

            var id = parseInt(contentId) || 0;
            var tmp = Math.floor(Math.random() * 100000);
            var url = location.protocol + "//" + location.host + "/ajax/CommonServices.aspx?service=anchornames&id=" + id + "&t=" + tmp;
            var optionHtml = "";

            //TO DO show Ajax loading image
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.length > 0) {
                        var arrAnchorName = data.split(",");
                        for (i = 0, len = arrAnchorName.length; i < len; i++) {
                            optionHtml += '<option value="#' + arrAnchorName[i] + '">' + arrAnchorName[i] + '</option>';
                        }
                    }

                    if (optionHtml.length > 0) {
                        optionHtml = "<option value=''>---</option>" + optionHtml;
                        jQuery("#cntanchorlist").html(optionHtml);
                        jQuery("#cntanchorlistrow").show();
                    }
                    else {

                        jQuery("#cntanchorlistrow").hide();
                    }
                },
                async: false
            });


        }
        //----------------


        //Evnet_drppolicy
        function Evnet_drppolicy() {
            //            var item = jQuery("#Listpolicy").val();
            //            if (item != null && item != "-1" && item != "None") {
            //                jQuery("#href").val("/calohi/downloadfile.aspx?id=" + item.toString());
            //                var listtext = jQuery('#Listpolicy option:selected').text()
            //                jQuery("#title").val(listtext);
            //            }
            //            else {
            //                jQuery("#href").val("");
            //                jQuery("#title").val("");
            //            }

            var item = jQuery("#Listpolicy  option:selected").text();

            if (item != null && item != "-1") {

                //  url = "/calohi/download" + jQuery("#Listpolicy").val() + "-" + item + ".htm";
                url = "/calohi/Calohii-Content-transfer.aspx?id=" + jQuery("#Listpolicy").val();
                var redirectionUrl = GetRedirectedUrl(url);
                jQuery("#href").val(redirectionUrl);
                jQuery("#title").val(item);
            }
            else {
                jQuery("#href").val("");
                jQuery("#title").val("");
            }
        }

        //Evnet_drpdocument
        function Evnet_drpdocument() {
            var item = jQuery("#listdocument  option:selected").text();

            if (item != null && item != "-1") {

                //  url = "/calohi/download" + jQuery("#listdocument").val()+"-" + item + ".htm";
                url = "/Content-transfer.aspx?id=" + jQuery("#listdocument").val();

                var redirectionUrl = GetRedirectedUrl(url);
                jQuery("#href").val(redirectionUrl);
                jQuery("#title").val(item);
            }
            else {
                jQuery("#href").val("");
                jQuery("#title").val("");
            }

            /* var item = jQuery("#listdocument").val();
            if (item != null && item != "-1" && item != "None") {
            jQuery("#href").val("/calohi/downloadfile.aspx?id=" + item.toString());
            var listtext = jQuery('#listdocument option:selected').text()
            jQuery("#title").val(listtext);
            }
            else {
            jQuery("#href").val("");
            jQuery("#title").val("");
            }*/
        }
        function GetRedirectedUrl(originalUrl) {
            return originalUrl.toString().replace(/ /g, "-").toLowerCase();
        }
       
    </script>

    <script type="text/javascript">
        //        $(document).ready(function () {
        //            // alert("hello");
        //            var inst = tinyMCEPopup.editor;
        //            var elm = inst.selection.getNode();
        //            elm = inst.dom.getParent(elm, "A");
        //            var href = inst.dom.getAttrib(elm, 'href');
        //            alert(href.toString());
        //            if (elm != null && elm.nodeName == "A") {
        //             
        //            }
        //           
        //           
        //        });
    </script>
</head>
<body id="advlink" style="display: none" role="application" onload="javascript:mcTabs.displayTab('general_tab','general_panel', true);" aria-labelledby="app_label">
<span class="mceVoiceLabel" id="app_label" style="display:none;">{#advlink_dlg.title}</span>
    <form id="form1" runat="server" onsubmit="insertAction();return false;" action="#">
    <asp:ScriptManager ID="scrmgr1" runat="server"></asp:ScriptManager>
  <div class="tabs" role="presentation">
			<ul>
				<li id="general_tab" class="current" aria-controls="general_panel" ><span><a href="javascript:mcTabs.displayTab('general_tab','general_panel');" onmousedown="return false;">{#advlink_dlg.general_tab}</a></span></li>
				<li id="popup_tab" aria-controls="popup_panel" ><span><a href="javascript:mcTabs.displayTab('popup_tab','popup_panel');" onmousedown="return false;">{#advlink_dlg.popup_tab}</a></span></li>
				<li id="events_tab" aria-controls="events_panel"><span><a href="javascript:mcTabs.displayTab('events_tab','events_panel');" onmousedown="return false;">{#advlink_dlg.events_tab}</a></span></li>
				<li id="advanced_tab" aria-controls="advanced_panel"><span><a href="javascript:mcTabs.displayTab('advanced_tab','advanced_panel');" onmousedown="return false;">{#advlink_dlg.advanced_tab}</a></span></li>
			</ul>
		</div>
      
		<div class="panel_wrapper" role="presentation" style="height:450px;">
			<div id="general_panel" class="panel current">
				<fieldset>
					<legend>{#advlink_dlg.general_props}</legend>
                    
					<table border="0" cellpadding="4" cellspacing="0" role="presentation">
                       <%--<tr id="linktemplate">
							<td class="column1"><label for="linklisthref">Template Page:</label></td>
							<td colspan="2" id="linktemplatecontainer">
                             <asp:DropDownList ID="ListTemplatePage" DataTextField="ContentHeading" 
                             DataValueField="ContentHeading" runat="server" Width="290" AutoPostBack="false"></asp:DropDownList>                                   
                            </td>
						</tr>
                        <tr id="tpltanchorlistrow" style="display:none">
							<td class="column1"><label for="anchorlist">{#advlink_dlg.anchor_names}</label></td>
							<td colspan="2" id="tpltanchorlistcontainer"><select id="tpltanchorlist" style="width:290px;" onchange="onAnchorDrpdChange(this)"><option value=""></option></select></td>
						</tr>                   
                           <tr id="trcontent">
							<td class="column1"><label for="linklisthref">Content Page</label></td>
							<td colspan="2" id="linkcontnetcontainer">
                             <asp:DropDownList ID="ListContentPage" DataTextField="PageTitle" 
                                DataValueField="Id" runat="server" Width="290"></asp:DropDownList>
                            </td>
						</tr>
                        <tr id="cntanchorlistrow" style="display:none">
							<td class="column1"><label for="anchorlist">{#advlink_dlg.anchor_names}</label></td>
							<td colspan="2" id="cntanchorlistcontainer"><select id="cntanchorlist" style="width:290px;" onchange="onAnchorDrpdChange(this)"><option value=""></option></select></td>
						</tr>

                          <tr id="trpolicy">
							<td class="column1"><label for="linklisthref">Policy Memorandum</label></td>
							<td colspan="2" id="linkpolicycontainer">
                             <asp:DropDownList ID="Listpolicy" DataTextField="Title" DataValueField="Id"
                              runat="server" Width="290"></asp:DropDownList>
                            </td>
						</tr>--%>

                        <%-- <tr id="trdocuments">
							<td class="column1"><label for="linklisthref">Documents</label></td>
							<td colspan="2" id="linkDocumentscontainer">
                             <asp:DropDownList ID="listdocument" DataTextField="Filename" DataValueField="Id"
                              runat="server" Width="290"></asp:DropDownList>
                            </td>
						</tr>--%>
                        

						<tr>
							<td class="nowrap"><label id="hreflabel" for="href">{#advlink_dlg.url}</label></td>
								<td><table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><input id="href" name="href" type="text" class="mceFocus" value="" onchange="selectByValue(this.form,'linklisthref',this.value);" aria-required="true" runat="server"/></td>
								<td id="hrefbrowsercontainer">&nbsp;</td>
							</tr>
							</table></td>
						</tr>

                        <tr>
							<td class="nowrap"><label id="Label1" for="href">{#advlink_dlg.linkname}</label></td>
								<td><table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><input id="linkname" name="href" type="text" style="width:280px" class="mceFocus" value="" onchange="" aria-required="true" runat="server"/></td>
								
							</tr>
							</table></td>
						</tr>

						<tr id="linklisthrefrow">
							<td class="column1"><label for="linklisthref">{#advlink_dlg.list}</label></td>
							<td colspan="2" id="linklisthrefcontainer"><select id="linklisthref"><option value=""></option></select></td>
						</tr>
						<tr id="anchorlistrow" style="display:none">
							<td class="column1"><label for="anchorlist">{#advlink_dlg.anchor_names}</label></td>
							<td colspan="2" id="anchorlistcontainer"><select id="anchorlist"><option value=""></option></select></td>
						</tr>
						<tr>
							<td><label id="targetlistlabel" for="targetlist">{#advlink_dlg.target}</label></td>
							<td id="targetlistcontainer" style="width:290px;"><select id="targetlist" style="width:290px;"><option value=""></option></select></td>
						</tr>
						<tr>
							<td class="nowrap"><label id="titlelabel" for="title">{#advlink_dlg.titlefield}</label></td>
							<td><input id="title" name="title" type="text" value="" runat="server" /></td>
						</tr>
						<tr>
							<td><label id="classlabel" for="classlist">{#class_name}</label></td>
							<td>
								 <select id="classlist" name="classlist" onchange="changeClass();" style="width:290px;">
									<option value="" selected="selected">{#not_set}</option>
								 </select>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>

			<div id="popup_panel" class="panel">
				<fieldset>
					<legend>{#advlink_dlg.popup_props}</legend>

					<input type="checkbox" id="ispopup" name="ispopup" class="radio" onclick="setPopupControlsDisabled(!this.checked);buildOnClick();" />
					<label id="ispopuplabel" for="ispopup">{#advlink_dlg.popup}</label>

					<table border="0" cellpadding="0" cellspacing="4" role="presentation" >
						<tr>
							<td class="nowrap"><label for="popupurl">{#advlink_dlg.popup_url}</label>&nbsp;</td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><input type="text" name="popupurl" id="popupurl" value="" onchange="buildOnClick();" /></td>
										<td id="popupurlbrowsercontainer">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="nowrap"><label for="popupname">{#advlink_dlg.popup_name}</label>&nbsp;</td>
							<td><input type="text" name="popupname" id="popupname" value="" onchange="buildOnClick();" /></td>
						</tr>
						<tr role="group" aria-labelledby="popup_size_label">
							<td class="nowrap"><label id="popup_size_label">{#advlink_dlg.popup_size}</label>&nbsp;</td>
							<td class="nowrap">
								<span style="display:none" id="width_voiceLabel">{#advlink_dlg.width}</span>
								<input type="text" id="popupwidth" name="popupwidth" value="" onchange="buildOnClick();" aria-labelledby="width_voiceLabel" /> x
								<span style="display:none" id="height_voiceLabel">{#advlink_dlg.height}</span>
								<input type="text" id="popupheight" name="popupheight" value="" onchange="buildOnClick();" aria-labelledby="height_voiceLabel" /> px
							</td>
						</tr>
						<tr role="group" aria-labelledby="popup_position_label center_hint">
							<td class="nowrap" id="labelleft"><label id="popup_position_label">{#advlink_dlg.popup_position}</label>&nbsp;</td>
							<td class="nowrap">
								<span style="display:none" id="x_voiceLabel">X</span>
								<input type="text" id="popupleft" name="popupleft" value="" onchange="buildOnClick();" aria-labelledby="x_voiceLabel" /> /                                
								<span style="display:none" id="y_voiceLabel">Y</span>
								<input type="text" id="popuptop" name="popuptop" value="" onchange="buildOnClick();" aria-labelledby="y_voiceLabel" /> <span id="center_hint">(c /c = center)</span>
							</td>
						</tr>
					</table>

					<fieldset>
						<legend>{#advlink_dlg.popup_opts}</legend>

						<table border="0" cellpadding="0" cellspacing="4" role="presentation" >
							<tr>
								<td><input type="checkbox" id="popuplocation" name="popuplocation" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popuplocationlabel" for="popuplocation">{#advlink_dlg.popup_location}</label></td>
								<td><input type="checkbox" id="popupscrollbars" name="popupscrollbars" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popupscrollbarslabel" for="popupscrollbars">{#advlink_dlg.popup_scrollbars}</label></td>
							</tr>
							<tr>
								<td><input type="checkbox" id="popupmenubar" name="popupmenubar" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popupmenubarlabel" for="popupmenubar">{#advlink_dlg.popup_menubar}</label></td>
								<td><input type="checkbox" id="popupresizable" name="popupresizable" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popupresizablelabel" for="popupresizable">{#advlink_dlg.popup_resizable}</label></td>
							</tr>
							<tr>
								<td><input type="checkbox" id="popuptoolbar" name="popuptoolbar" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popuptoolbarlabel" for="popuptoolbar">{#advlink_dlg.popup_toolbar}</label></td>
								<td><input type="checkbox" id="popupdependent" name="popupdependent" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popupdependentlabel" for="popupdependent">{#advlink_dlg.popup_dependent}</label></td>
							</tr>
							<tr>
								<td><input type="checkbox" id="popupstatus" name="popupstatus" class="checkbox" onchange="buildOnClick();" /></td>
								<td class="nowrap"><label id="popupstatuslabel" for="popupstatus">{#advlink_dlg.popup_statusbar}</label></td>
								<td><input type="checkbox" id="popupreturn" name="popupreturn" class="checkbox" onchange="buildOnClick();" checked="checked" /></td>
								<td class="nowrap"><label id="popupreturnlabel" for="popupreturn">{#advlink_dlg.popup_return}</label></td>
							</tr>
						</table>
					</fieldset>
				</fieldset>
			</div>

			<div id="advanced_panel" class="panel">
			<fieldset>
					<legend>{#advlink_dlg.advanced_props}</legend>

					<table border="0" cellpadding="0" cellspacing="4" role="presentation" >
						<tr>
							<td class="column1"><label id="idlabel" for="id">{#advlink_dlg.id}</label></td> 
							<td><input id="id" name="id" type="text" value="" /></td> 
						</tr>

						<tr>
							<td><label id="stylelabel" for="style">{#advlink_dlg.style}</label></td>
							<td><input type="text" id="style" name="style" value="" /></td>
						</tr>

						<tr>
							<td><label id="classeslabel" for="classes">{#advlink_dlg.classes}</label></td>
							<td><input type="text" id="classes" name="classes" value="" onchange="selectByValue(this.form,'classlist',this.value,true);" /></td>
						</tr>

						<tr>
							<td><label id="targetlabel" for="target">{#advlink_dlg.target_name}</label></td>
							<td><input type="text" id="target" name="target" value="" onchange="selectByValue(this.form,'targetlist',this.value,true);" /></td>
						</tr>

						<tr>
							<td class="column1"><label id="dirlabel" for="dir">{#advlink_dlg.langdir}</label></td> 
							<td>
								<select id="dir" name="dir"> 
										<option value="">{#not_set}</option> 
										<option value="ltr">{#advlink_dlg.ltr}</option> 
										<option value="rtl">{#advlink_dlg.rtl}</option> 
								</select>
							</td> 
						</tr>

						<tr>
							<td><label id="hreflanglabel" for="hreflang">{#advlink_dlg.target_langcode}</label></td>
							<td><input type="text" id="hreflang" name="hreflang" value="" /></td>
						</tr>

						<tr>
							<td class="column1"><label id="langlabel" for="lang">{#advlink_dlg.langcode}</label></td> 
							<td>
								<input id="lang" name="lang" type="text" value="" />
							</td> 
						</tr>

						<tr>
							<td><label id="charsetlabel" for="charset">{#advlink_dlg.encoding}</label></td>
							<td><input type="text" id="charset" name="charset" value="" /></td>
						</tr>

						<tr>
							<td><label id="typelabel" for="type">{#advlink_dlg.mime}</label></td>
							<td><input type="text" id="type" name="type" value="" /></td>
						</tr>

						<tr>
							<td><label id="rellabel" for="rel">{#advlink_dlg.rel}</label></td>
							<td><select id="rel" name="rel"> 
									<option value="">{#not_set}</option> 
									<option value="lightbox">Lightbox</option> 
									<option value="alternate">Alternate</option> 
									<option value="designates">Designates</option> 
									<option value="stylesheet">Stylesheet</option> 
									<option value="start">Start</option> 
									<option value="next">Next</option> 
									<option value="prev">Prev</option> 
									<option value="contents">Contents</option> 
									<option value="index">Index</option> 
									<option value="glossary">Glossary</option> 
									<option value="copyright">Copyright</option> 
									<option value="chapter">Chapter</option> 
									<option value="subsection">Subsection</option> 
									<option value="appendix">Appendix</option> 
									<option value="help">Help</option> 
									<option value="bookmark">Bookmark</option>
									<option value="nofollow">No Follow</option>
									<option value="tag">Tag</option>
								</select> 
							</td>
						</tr>

						<tr>
							<td><label id="revlabel" for="rev">{#advlink_dlg.rev}</label></td>
							<td><select id="rev" name="rev"> 
									<option value="">{#not_set}</option> 
									<option value="alternate">Alternate</option> 
									<option value="designates">Designates</option> 
									<option value="stylesheet">Stylesheet</option> 
									<option value="start">Start</option> 
									<option value="next">Next</option> 
									<option value="prev">Prev</option> 
									<option value="contents">Contents</option> 
									<option value="index">Index</option> 
									<option value="glossary">Glossary</option> 
									<option value="copyright">Copyright</option> 
									<option value="chapter">Chapter</option> 
									<option value="subsection">Subsection</option> 
									<option value="appendix">Appendix</option> 
									<option value="help">Help</option> 
									<option value="bookmark">Bookmark</option> 
								</select> 
							</td>
						</tr>

						<tr>
							<td><label id="tabindexlabel" for="tabindex">{#advlink_dlg.tabindex}</label></td>
							<td><input type="text" id="tabindex" name="tabindex" value="" /></td>
						</tr>

						<tr>
							<td><label id="accesskeylabel" for="accesskey">{#advlink_dlg.accesskey}</label></td>
							<td><input type="text" id="accesskey" name="accesskey" value="" /></td>
						</tr>
					</table>
				</fieldset>
			</div>

			<div id="events_panel" class="panel">
			<fieldset>
					<legend>{#advlink_dlg.event_props}</legend>

					<table border="0" cellpadding="0" cellspacing="4" role="presentation" >
						<tr>
							<td class="column1"><label for="onfocus">onfocus</label></td> 
							<td><input id="onfocus" name="onfocus" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onblur">onblur</label></td> 
							<td><input id="onblur" name="onblur" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onclick">onclick</label></td> 
							<td><input id="onclick" name="onclick" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="ondblclick">ondblclick</label></td> 
							<td><input id="ondblclick" name="ondblclick" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onmousedown">onmousedown</label></td> 
							<td><input id="onmousedown" name="onmousedown" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onmouseup">onmouseup</label></td> 
							<td><input id="onmouseup" name="onmouseup" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onmouseover">onmouseover</label></td> 
							<td><input id="onmouseover" name="onmouseover" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onmousemove">onmousemove</label></td> 
							<td><input id="onmousemove" name="onmousemove" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onmouseout">onmouseout</label></td> 
							<td><input id="onmouseout" name="onmouseout" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onkeypress">onkeypress</label></td> 
							<td><input id="onkeypress" name="onkeypress" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onkeydown">onkeydown</label></td> 
							<td><input id="onkeydown" name="onkeydown" type="text" value="" /></td> 
						</tr>

						<tr>
							<td class="column1"><label for="onkeyup">onkeyup</label></td> 
							<td><input id="onkeyup" name="onkeyup" type="text" value="" /></td> 
						</tr>
					</table>
				</fieldset>
			</div>
		</div>

		<div class="mceActionPanel">
			<input type="submit" id="insert" name="insert" value="{#insert}" />
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
		</div>

         <%--<asp:SqlDataSource ID="SqlTemplatePageList" runat="server" ConnectionString="<%$ ConnectionStrings:CalohiiWebsiteSqlServer %>"
        ProviderName="<%$ ConnectionStrings:CalohiiWebsiteSqlServer.ProviderName %>"
        SelectCommand="select ContentHeading from  Module_Homepage union select ContentHeading 
        from Module_About union select ContentHeading from Module_DataMovement  union select ContentHeading from 
         Module_EhealthPartners union select ContentHeading from  Module_GenericSecondary 
         union select ContentHeading from  Module_Patients union select ContentHeading from  
         Module_Providers union select ContentHeading from  Module_ProvidersSecondary union select ContentHeading from  
         Module_Statedepartment  
         union select ContentHeading from  Module_TermConditions
         union select ContentHeading from  Module_Privacy
         union select ContentHeading from  Module_contactus
            Union Select ' None' as ContentHeading order by ContentHeading" />

              <asp:SqlDataSource ID="Sqldocumentlist" runat="server" ConnectionString="<%$ ConnectionStrings:CalohiiWebsiteSqlServer %>"
        ProviderName="<%$ ConnectionStrings:CalohiiWebsiteSqlServer.ProviderName %>"
        SelectCommand="select Id,Filename from FileUpload where FileCategoryID=4 Union select -1 as Id,' None' as Fimename order by Filename" />
                
            
        
    <asp:SqlDataSource ID="SqlDSContentPage" runat="server" ConnectionString="<%$ ConnectionStrings:CalohiiWebsiteSqlServer %>"
                       ProviderName="<%$ ConnectionStrings:CalohiiWebsiteSqlServer.ProviderName %>" 
                       SelectCommand="select Id ,Replace(Replace(Replace(replace(replace(replace(replace(replace(replace(replace(PageTitle,':',''),'`',''),'!',''),')',''),'(',''),'*',''),'&',''),'$',''),'''',''),'-',' ') as    PageTitle from CalohiiContent where active=1  Union select -1 as Id,' None' as PageTitle order by PageTitle,Id"
        />
            
          <asp:SqlDataSource ID="SqlDataSourcepolicy" runat="server" ConnectionString="<%$ ConnectionStrings:CalohiiWebsiteSqlServer %>"
                       ProviderName="<%$ ConnectionStrings:CalohiiWebsiteSqlServer.ProviderName %>" 
                       SelectCommand="select FileId as Id ,Title from PolicyMemorandums where active=1  Union select -1 as Id,' None' as Title order by Title,Id"
        />--%>

      

    </form>


</body>
</html>
