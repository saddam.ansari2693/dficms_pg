﻿var urlFetchSurveyMasterDataUrl = "/WebService/CalohiiServiceApi.svc/Fetch_Survey_Master_Data";
var urlFetchSurveyDetailDataUrl = "/WebService/CalohiiServiceApi.svc/Fetch_Survey_Detail_Data";
var iCnt = 0;
var draggablecount = 0;
$(document).ready(function () {
    var path = window.location;
    var modSurveyID = "";
    var urlstrings = path.toString().split('?');
    if (urlstrings.length > 1) {
        var urlstringList = urlstrings[1].toString().split('=');
        if (urlstringList.length > 1) {
            modSurveyID = urlstringList[1].toString();            
            if (modSurveyID.toString().trim() != "") {
                Get_Survey_Master_Data(modSurveyID.toString());
            }
            else {
                alert("There is no active survey");
            }
        }
    }
    else {
        alert("No querystring");
    }
});

function Get_Survey_Master_Data(SurveyPopupID) {
    $.ajax({
        contentType: "application/json; charset=utf-8",
        url: urlFetchSurveyMasterDataUrl,
        data: {
            "surveypopupid": SurveyPopupID
        },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            var result = jQuery.parseJSON(data);
            var list="";
            if (result.length > 0) {
                $("#divMainPopup").css("width", result[0].windowwidth);
                $("#divMainPopup").css("height", result[0].windowheight);
                $("#divMainPopup").css("background-color", "#" + result[0].backgroundcolor);
                $("#divMainPopup").css("border", "1px solid");
                $("#divHeader").css("background-image", "url(../images/savedate_new_header.png)");                
              }
              Get_Survey_Detail_Data(SurveyPopupID);
        }
        else {
        }
    }
    function ajaxFailed() {
        alert("There is an error during operation.");
    }
   
}
function Get_Survey_Detail_Data(SurveyPopupID) {
     $.ajax({
        contentType: "application/json; charset=utf-8",
        url: urlFetchSurveyDetailDataUrl,
        data: {
            "surveypopupid": SurveyPopupID
        },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            var result = jQuery.parseJSON(data);
            if (result.length > 0) {
                var list = "";
                for (var i = 0; i < result.length; i++) {
                    var CaseName = result[i].FiledType;
                    var LabelHeading = "";
                    switch (CaseName) {
                        case "Title":
                            list = list + "<div style='font-size:" + result[i].textfieldheadingvalue2 + "px;text-align:" + result[i].textfieldheadingvalue3 + ";color:#" + result[i].textfieldheadingvalue4 + ";font-weight:bold; margin-left: 20px;'>";
                            list = list + result[i].textfieldheadingvalue1;
                            list = list + "</div>";
                            break;
                        case "Content":
                            list = list + "<div style='font-size:" + result[i].textfieldheadingvalue2 + "px;text-align:" + result[i].textfieldheadingvalue3 + ";color:#" + result[i].textfieldheadingvalue4 + ";font-weight:bold; margin-left: 20px;'>";
                            list = list + result[i].textfieldheadingvalue1;
                            list = list + "</div>";
                            break;
                        case "Empty Space":
                           break;
                        case "Single Line Text":
                           break;
                        case "Full Name":                          
                            break;
                        case "First Name":
                           break;                            
                        case "Last Name":
                            break;
                        case "Email Address":
                            break;
                        case "Company":
                            break;
                        case "Phone Number":
                            break;
                        case "Address 1":
                            break;
                        case "Address 2":
                            break;
                        case "City":
                            break;
                        case "State":
                            break;
                        case "Zip":
                            break;
                        case "Custom 1":
                            break;
                        case "Custom 2":
                            break;
                        case "Custom 3":
                            break;
                        case "Paragraph Text":
                            break;
                        case "Upload Image":
                            break;
                        case "Section Break":
                            <div style="width:500px;height:10px;background-color:Red;" align="center"></div>
                            break;
                        case "MultipleChoice":
                        case "Single Choice":
                            break;
                        case "Checkbox":
                            break;
                        case "Dropdown":
                            break;
                    }
                }
                $("#divPopupDesign").append(list);
            }
        }
        else {
        }
    }
    function ajaxFailed() {
        alert("There is an error during operation.");
    }
}
