﻿/*---------------ready function------------*/
var iCnt =3;
var draggablecount = 0;
$(document).ready(function () {
    $("#divMain").sortable({
        revert: true
    });
    $('#datepicker').datepicker();
    Draggable_DroppableSource1();
    Draggable_DroppableSource();
    for (i = 1; i < 4; i++) {
        $("#dvDest" + i).droppable({
            drop: function (event, ui) {
                if ($("#dvDest" + i + " img").length == 0) {
                    $("#dvDest" + i).html("");
                }
                var ID1 = event.target.id;
                $("#hid1").val(event.target.id);
            }
        });
    }
});

/*------------------------------------To Create Radion Button----------------*/
function createradio(id) {
    var radioBtn = "";
    var v = $("#radio").val();
    for (i = 1; i < v; i++) {
        radioBtn = radioBtn + "<div class='pull-left space-50' style='padding-top: 10px;'> <input type='radio'></div><span class='pull-left'><input type='text' placeholder='label2 class='space-10'></span>  <br clear='all' />";
    }
    $("#append").html(radioBtn);
}
/*------------------------------------To create Checkbox ----------------*/
function createcheckbox(id) {
    var checkBtn = "";
    var v = $("#apncheck").val();
    for (i = 0; i < v; i++) {
        var checkBtn = checkBtn + "<div class='pull-left space-50' style='padding-top:10px'><input type='checkbox'></div><span class='pull-left'><input type='text' placeholder='label 1' class='space-10'></span> <br clear='all' />";
    }
    $("#appendcheckbox").html(checkBtn);
}

    /*------------------------------------To create list ----------------*/
    function createdropdown(id) {
        var dropdown = "";
        var v = $("#apndropdown").val();
        for (i = 0; i < v; i++) {
            var dropdown = dropdown + ' <ul><li>Item List 1</li></ul>';
        }
        $("#appendlist").html(dropdown);
    }

/*------------ To add div on plus button click------------------*/
function adddiv() {
    iCnt = iCnt + 1;
    $("#divMain").append("<div class='drop-panel-block ui-droppable' id='dvDest" + iCnt + "'> </div>");
    $("#dvDest" + iCnt).droppable({
        drop: function (event, ui) {
            if ($("#dvDest" + iCnt + " img").length == 0) {
                $("#dvDest" + iCnt).html("");
            }
            var ID1 = event.target.id;
            $("#hid1").val(event.target.id);
        }
    });
}



            /*-------------------------------function for color picker in section break-------------------*/

            function getheight(sel) {
            var Hrobj = $('#exampleInputName2').val();
            var myPicker = new jscolor.color(document.getElementById('ColorPicker'), {})
            var obj = {
                'width': Hrobj.toString(),
                'height': sel.value.toString(),
                'border-radius': '5px',
                'background-color': '#' + myPicker.toString()
            }
            $('#HRRule').css(obj);
        }

        function getwidth(sel) {
            var Hrobj = $('#ddlName').val();
            var myPicker = new jscolor.color(document.getElementById('ColorPicker'), {})
            var obj = {
                'width': sel.value.toString(),
                'height': Hrobj.toString(),
                'border-radius': '5px',
                'background-color': '#' + myPicker.toString()
            }
            $('#HRRule').css(obj);
        }
        

        function getcolor(color) {         
            var Hrheightobj = $('#ddlName').val();
             var HrWidthobj = jQuery('#exampleInputName2').val();
            var myPicker = new jscolor.color(document.getElementById('ColorPicker'), {})
               alert(myPicker.toString());

            var obj = {
                'width': HrWidthobj.value.toString(),
                'height': Hrheightobj.toString(),
                'border-radius': '5px',
                'background-color': '#' + myPicker.toString()
            }
            $('#HRRule').css(obj)
        }

        function showMyImage(fileInput) {
            var files = fileInput.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    continue;
                }
                var img = document.getElementById("thumbnil");
                img.file = file;
                var reader = new FileReader();
                reader.onload = (function (aImg) {
                    return function (e) {
                        aImg.src = e.target.result;
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        }
 





        function Draggable_DroppableSource() {
             $("#dvSource img").draggable({
                    revert: "invalid",
                    refreshPositions: true,
                    drag: function (event, ui) {
                        ui.helper.addClass("draggable");
                    },
                    stop: function (event, ui) {
                        ui.helper.removeClass("draggable");
                        var image = this.src.split("/")[this.src.split("/").length - 1];
                        if (jQuery.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {
                            ui.helper.removeAttr("style");
                            ui.helper.removeClass("ui-draggable");
                            if (image.toString() == "Content1.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'>Content<span class='pull-right cross-btn' id='spn23' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'><div class='span3 pull-left'>";
                                litem += "<input type='text' placeholder='label'><br clear='all' />";
                                litem += "<input type='checkbox' class='width-10'><span> Required</span></div>";
                                litem += "<div class='span7 pull-left'>";
                                litem += "<textarea rows='3' placeholder='multiple line'></textarea></div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }
                            else if (image.toString() == "Title1.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'>Title<span class='pull-right cross-btn' id='spn24' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'><div class='span3 pull-left'>";
                                litem += "<input type='text' placeholder='label'><br clear='all' />";
                                litem += "<input type='checkbox' class='width-10'><span> Required</span></div>";
                                litem += "<div class='span7 pull-left'>";
                                litem += "<input type='text' placeholder='label'></div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }
                            else if (image.toString() == "upload-img.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                litem = "<div class='clearfix btn-lg'>Upload Image <span class='pull-right cross-btn' id='spn25' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'> <div class='span3 pull-left'> <input  id='exampleInputFile' type='file' class='no-border'  accept='image/*'  onchange='showMyImage(this);'><img id='thumbnil' style='width:75%; height:20%px; border-radius:5px; margin-left:250px; margin-top:-32px'/></div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);

                            }

                            else if (image.toString() == "section-break.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'> Section Break<span class='pull-right cross-btn' id='spn26' onclick=crossdelete(id);>&nbsp;</span> </div>";
                                litem += "<div class='drop-panel'> <div class='span3 pull-left'> <label style='float: left; padding: 7px 5px 0 0;'>  Width:</label><input type='text' class='width-80' id='exampleInputName2' placeholder='width' onkeyup='getwidth(this);'> </div>";
                                litem += "<div class='span2 pull-left'> <label style='float: left; padding: 7px 5px 0 0;'>  Height:</label>";
                                litem += "<select  id='ddlName' onchange='getheight(this);'> <option>1</option>  <option>2</option>  <option>3</option> <option>4</option> <option>5</option> <option>6</option> <option>7</option>  <option>8</option> <option>9</option>  <option>10</option>  </select>  </div>";
                                litem += "<div class='span3 pull-left'> <input class='color' value='66ff00' id='ColorPicker' onchange='getheight(this);'></div></br><hr id='HRRule' margin='30px' style='width:100px;height;5px;border-radius:5px;background-color:Black; margin: 35px 0px;'>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }
                            else if (image.toString() == "empty-space.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'> Empty Space <span class='pull-right cross-btn' id='spn27' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'> <div class='clearfix'></div>";
                                $("#" + divid).append(litem);
                            }

                           else if (image.toString() == "paragraph-text.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg' >Paragraph Text<span class='pull-right cross-btn' id='spn1' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'><div class='span3 pull-left'>";
                                litem += "<input type='text' placeholder='label'><br clear='all' />";
                                litem += "<input type='checkbox' class='width-10'><span> Required</span></div>";
                                litem += "<div class='span7 pull-left'>";
                                litem += "<textarea rows='3' placeholder='multiple line'></textarea></div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }



                            else if (image.toString() == "single-line.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'>Single Line Text<span class='pull-right cross-btn' id='spn2' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'><div class='span3 pull-left'>";
                                litem += "<input type='text' placeholder='label'><br clear='all' />";
                                litem += "<input type='checkbox' class='width-10'><span> Required</span></div>";
                                litem += "<div class='span7 pull-left'>";
                                litem += "<input type='text' placeholder='label'></div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }



                            else if (image.toString() == "multiple-choice.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = " <div class='clearfix btn-lg'>Multiple Choice <span class='pull-right cross-btn'  id='spn5' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'> <div class='span3 pull-left'><input type='text' placeholder='label'><br clear='all' />  </div>";
                                litem += "<div class='span6 pull-left'> <label style='float: left; padding: 7px 5px 0 0;'>   Total Number of Radio:</label>";
                                litem += "<select id='radio' onchange='createradio();'> <option>1</option> <option>2</option>  <option>3</option>   <option>4</option>   </select>";
                                litem += "<br clear='all' />   <div class='pull-left space-50' style='padding-top: 10px;'>";
                                litem += "<input type='radio'></div>  <span class='pull-left'><input type='text' placeholder='label 1' class='space-10'></span>";
                                litem += "<br clear='all' />";
                                litem += "<div id='append'>";
                                litem += "<div class='pull-left space-50' style='padding-top: 10px;'> </div>";
                                litem += "</div>";
                                litem += "</div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);

                            }


                            else if (image.toString() == "checkbox.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'>Checkbox<span class='pull-right cross-btn' id='spn6' onclick=crossdelete(id);>&nbsp;</span> </div>";
                                litem += "<div class='drop-panel'><div class='span6 pull-left'>";
                                litem += "<label style='float: left; padding: 7px 5px 0 0;'> Total Number of Check:</label>";
                                litem += "<select id='apncheck'  onchange='createcheckbox(this);'><> <option>1</option>  <option>2</option><option>3</option>  <option>4</option> </select>";
                                litem += "<br clear='all' />";
                                litem += "<div id='appendcheckbox'>";
                                litem += "<div class='pull-left space-50' style='padding-top: 10px;'> <input type='checkbox'></div>";
                                litem += "<span class='pull-left'  > <input type='text' placeholder='label 1' class'space-10'></span>";
                                litem += "<br clear='all' />";
                                litem += "</div>";
                                litem += "</div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }


                            else if (image.toString() == "dropdown.png") {
                                var divid = jQuery("#hid1").val();
                                $("#" + divid).removeClass("drop-panel-block ui-droppable");
                                $("#" + divid).addClass("drop-panel-cont ui-droppable");
                                var litem = "<div class='clearfix btn-lg'> Dropdown <span class='pull-right cross-btn' id='spn7' onclick=crossdelete(id);>&nbsp;</span></div>";
                                litem += "<div class='drop-panel'><div class='span4 pull-left'>";
                                litem += "<label style='float: left; padding: 7px 5px 0 0;'> Total Number of List:</label>";
                                litem += "<select class='width60 space-btm' onchange='createdropdown(this);' id='apndropdown'> <option>Item List 1</option><option>2</option><option>3</option> <option>4</option></select></br></br>";
                                litem += "<input type='text' placeholder='label' style='width:160px' class='space-50'> <div id='appendlist' class='space-50'><ul> <li>Item List 1</li></ul></div>";
                                litem += "</div>";
                                litem += "<div class='clearfix'>";
                                litem += "</div>";
                                litem += "</div>";
                                $("#" + divid).append(litem);
                            }
                        }
                        else {

                        }
                    }
                });
          //make Destination Div droppable
          jQuery("#dvDest").droppable({
              drop: function (event, ui) {
                  if ($("#dvDest img").length == 0) {
                      $("#dvDest").html("");
                  }
                  $("#dvDest").append(ui.draggable);
                  }
            });
        }



      /*-------------------------------------Cross Button click to hide the current div------------------------*/


      function crossdelete(id) {
             id1 = '#' + id;
             var parentid = ($(id1).parent().parent().attr("id"));
             var k = $(divhide).html();
             var divhide = '#' + parentid;
             if (id == "spn9") {
                $("#Dvfull-name").append('<img src="../images/full-name.png">');
                Draggable_DroppableSource1();
            }

            else if  (id == "spn10") {
                $("#Dvfirst-name").append('<img src="../images/first-name.png">');
     
                Draggable_DroppableSource1();
            }

            else if   (id == "spn11"){
                $("#Dvlast-name").append('<img src="../images/last-name.png">');
                Draggable_DroppableSource1();
            }

            else if   (id == "spn12") {
                $("#Dvemail-address").append('<img src="../images/email-address.png">');
                Draggable_DroppableSource1();
            }

            else if (id == "spn13") {
                $("#Dvcompany").append('<img src="../images/company.png">');
                Draggable_DroppableSource1();
            }

            else if   (id == "spn14") {
                $("#Dvphone-number").append('<img src="../images/phone-number.png">');
                Draggable_DroppableSource1();
            }

            else if (id == "spn15") {
                $("#Dvadress-1").append('<img src="../images/adress-1.png">');
                Draggable_DroppableSource1();
            }


            else if (id == "spn16") {
                $("#Dvaddress-2").append('<img src="../images/address-2.png">');
                Draggable_DroppableSource1();
            }

            else if (id == "spn17") {
                $("#Dvcity").append('<img src="../images/city.png">');
                Draggable_DroppableSource1();
            }
            else if (id == "spn18") {
                $("#Dvstate").append('<img src="../images/state.png">');
                Draggable_DroppableSource1();
            }
            else if (id == "spn19") {
                $("#Dvzip").append('<img src="../images/zip.png">');
                Draggable_DroppableSource1();
            }


            else if (id == "spn20") {
                $("#Dvcustom-1").append('<img src="../images/custom-1.png">');
                Draggable_DroppableSource1();
            }



            else if (id == "spn21") {
                $("#Dvcustom-2").append('<img src="../images/custom-2.png">');
                Draggable_DroppableSource1();
            }



            else if (id == "spn22") {
                $("#Dvcustom-3").append('<img src="../images/custom-3.png">');
                Draggable_DroppableSource1();
            }
          
            $(divhide).remove();
           iCnt = iCnt - 1;
      }


      /*-------------------------For Cancel button---------------------------*/
      function cancel() {
          for (i = 1; i <= iCnt; i++) {
              var parent = '#' + 'dvDest' + i;
              var str = $(parent).text();
              alert("string is" + str);
              $('#dvDest' + i).remove();
               }

                  for (i = 1; i < 4; i++) {
                      $("#divMain").append("<div class='drop-panel-block ui-droppable' id='dvDest" + i + "'> </div>");
                      $("#dvDest" + i).droppable({
                          drop: function (event, ui) {
                              if ($("#dvDest" + i + " img").length == 0) {
                                  $("#dvDest" + i).html("");
                              }
                              var ID1 = event.target.id;
                              $("#hid1").val(event.target.id);
                          }
                      });
                 }
            }

