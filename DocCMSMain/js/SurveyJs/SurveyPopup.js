﻿var urlInsertSurveyDataUrl = "/WebService/CalohiiServiceApi.svc/Insert_Survey_Master_Data";
var urlInsertSurveyDetailDataUrl = "/WebService/CalohiiServiceApi.svc/Insert_Survey_Detail_Data";
var urlFetchSurveyMasterDataUrl = "/WebService/CalohiiServiceApi.svc/Fetch_Survey_Master_Data";
var urlFetchSurveyDetailDataUrl = "/WebService/CalohiiServiceApi.svc/Fetch_Survey_Detail_Data";
var urlFetchSurveyDetailUrl = "/WebService/CalohiiServiceApi.svc/Fetch_Survey_Response_By_ID";
var urlDeleteSurveyDetailUrl = "/WebService/CalohiiServiceApi.svc/Delete_Current_Survey";


var iCnt = 0;
var draggablecount = 0;
var objSurveyPopupDetailData;

$(document).ready(function () {
    var path = window.location;
    var modSurveyID = "";
    var urlstrings = path.toString().split('?');

    if (urlstrings.length > 1) {
        var urlstringList = urlstrings[1].toString().split('=');
        if (urlstringList.length > 1) {
            modSurveyID = urlstringList[1].toString().split('&')[0];
            if (modSurveyID.toString().trim() != "") {
                $("#hdnModSurveyID").val(modSurveyID.toString());
                $("#hdnMode").val(urlstringList[2].toString());
            }
            else {
                $("#hdnModSurveyID").val("");
                $("#hdnMode").val("");
            }
        }
    }
    else {
    }
    $("#divMain").sortable({
        revert: true
    });

    //==== FOR THE PURPOSE TO CONVERT ALL IMAGES ON LEFT SIDE TO DRAGGABLE
    //==== For Standard Elements ----------------------------------------------

    Convert_Left_Side_Images_Draggable();
    //==== FOR THE PURPOSE TO ADD DEFAULT 3 DRAGABLE DIVS =====//
    if ($("#hdnModSurveyID").val().toString().trim() != "") {
        Display_Popup($("#hdnModSurveyID").val().toString().trim());
    }
    else {
        Add_Tree_Divs_on_Page_Load();
    }
});


function Add_Tree_Divs_on_Page_Load() {
    for (i = 1; i < 4; i++) {
        adddiv();
    }

} /// Function closed here
/*------------ To add div on plus button click------------------*/
function adddiv() {    
    iCnt = iCnt + 1;
    $("#divMain").append("<div class='drop-panel-block ui-droppable' id='dvDest" + iCnt + "'> </div>");
    $("#dvDest" + iCnt).droppable({
        drop: function (event, ui) {        
            if ($("#dvDest" + iCnt + " img").length == 0) {
                $("#dvDest" + iCnt).html("");
            }
            var ID1 = event.target.id;
            $("#hid1").val(event.target.id);
        }
    });
}

function Convert_Left_Side_Images_Draggable() {
    $("#divLeftSide img").draggable({
        revert: "invalid",
        refreshPositions: true,
        drag: function (event, ui) {
            ui.helper.addClass("draggable");
        },
        stop: function (event, ui) {
            ui.helper.removeClass("draggable");
            var image = this.src.split("/")[this.src.split("/").length - 1];
            if ($.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {

                var Get_Image_Section_Type = image.toString().split('_');
                if (Get_Image_Section_Type.length > 0) {
                    if (Get_Image_Section_Type[0] == "SE") {
                        //=== Not Hide the Source Image
                        ui.helper.removeAttr("style");
                        ui.helper.removeClass("ui-draggable ui-draggable-dragging");
                        ui.helper.addClass("ui-draggable");
                        ui.helper.css({ 'position': "relative" });

                        var CaseName = Get_Case_Name(Get_Image_Section_Type[1]);                        
                        Add_Control_block(CaseName);
                    }
                    else {

                        ui.helper.removeAttr("style");
                        ui.helper.removeClass("ui-draggable");
                        ui.helper.css('display', 'none');

                        var CaseName = Get_Case_Name(Get_Image_Section_Type[1]);
                        Add_Control_block(CaseName);
                    }
                }
                //===== Selected Image Design Adding from this Function ==================              
            }
            else {

            }
        }
    });   

} /// Function Closed here

// To Get drop Down accoding to the ddl type
function Get_DropDown_Data(DdlName,ddlID, DdlType) {
    var List = "";
    var ddlID = DdlName + "_" + ddlID;
    switch (DdlType) {
        case "FontSize":
            List = List + "<select ID='" + ddlID + "' style='width:80px;'>";
            for (var i = 8; i <= 72; i = i + 2) {
            List = List + "<option value='"+i.toString()+"'>"+i.toString()+"</option>";
            }
            break;
        case "Alignment":
            List = List + "<select ID='" + ddlID + "' style='width:100px;'>";
            List = List + "<option value='Left'>Left</option>";
            List = List + "<option value='Right'>Right</option>";
            List = List + "<option value='Center'>Center</option>";
            List = List + "<option value='Justified'>Justified</option>";
            break;
    }
    
    List = List + "</select>";
    return List.toString();
}
// ==== End Code
// To Get the Color Picker Text Box for Text color
function Get_Colorpciker_textbox(txtName, txtID) {
    var List = "";
    var txtID = txtName + "_" + txtID;
    List = List +"<input id='"+txtID+"' class='color' value='020500' autocomplete='off' style='background-image: none; background-color: rgb(0,0,0); color: rgb(0,0,0);'>"
}

// To Get the Color Picker Text Box for Text color Ends Here
//=== FOR THE PURPOSE OF THE BLOCK DESIGN CREATION ====//
function Add_Control_block( CaseName) {    
        if (CaseName != null && CaseName.length > 0) {
        //==== Common Design Code Here ===============//
        var divid = jQuery("#hid1").val();
        var dividval = jQuery("#hid1").val();
        $("#" + divid).removeClass("drop-panel-block ui-droppable");
        $("#" + divid).addClass("drop-panel-cont ui-droppable");
        //------------------------------------------------------
        var litem = "";
        //------- CONTROL SPECIFIC CODE SHOULD BE HERE ---------
        switch (CaseName) {
            case "Title":
                litem = "<div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;' >" + CaseName + "<span class='pull-right cross-btn' id='spn24' onclick=crossdelete(" + divid + ");>&nbsp;</span></div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'><div class='span10 pull-left'>";
                litem += "<input type='text' id=txt_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " placeholder='Title' onchange='Set_Section_Break(" + dividval + ");'><br clear='all' />";
                litem += "<div>";
                litem += "<div style='float:left;'>";
                litem += "<label id=lblTitleSize" + divid + " style='width:60px; vertical-align:top;'>Font Size:</label>" + Get_DropDown_Data('ddlTitleSize', divid, 'FontSize'); +"&nbsp;";
                litem += "</div>";
                litem += "<div style='float:left;'>";
                litem += "<label id=lblTitleAlign_" + divid + " style='width:60px;vertical-align:top;'>Alignment:</label>" + Get_DropDown_Data('ddlTitleAlign', divid, 'Alignment'); +"&nbsp;<br clear='all' />";
                litem += "</div>";
                litem += "<div style='float:left;'>";
                litem += "<label id=lblTitleColor_" + divid + " style='width:60px;vertical-align:top;'>Color:</label> &nbsp;";                 
                litem += " <input class='color' style='width:83%;height:25px;' value='66ff00' id='ColorPicker_" + divid + "' onchange='Set_Section_Break(" + dividval + ");' >";
                litem += "</div>";                
                litem += "<input type='checkbox' id=ChkRequired_" + divid + " name=ChkRequired_" + divid + " style='width:3%;margin-top:20px;'><span style='margin-left: -9px;margin-top: 19px;width: 60px;'> Required</span></div>";
                litem += "</div>"
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";
                break;
             case "Single Line Text":
             case "Full Name":
             case "First Name":
             case "Last Name":
             case "Email Address":
             case "Company":
             case "Phone Number":
             case "Address 1":
             case "Address 2":
             case "City":
             case "State":
             case "Zip":
             case "Custom 1":
             case "Custom 2":
             case "Custom 3":
                 litem = "<div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;' >" + CaseName + "<span class='pull-right cross-btn' id='spn24' onclick=crossdelete(" + divid + ");>&nbsp;</span></div>";
                 litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'><div class='span3 pull-left'>";
                 litem += "<input type='text' id=txtLabel_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " placeholder='Label'><br clear='all' />";
                 litem += "<input type='checkbox' id=ChkRequired_" + divid + " class='width-10'><span> Required</span></div>";
                litem += "<div class='span7 pull-left'>";
                litem += "<input type='text'  id=txtValue_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " placeholder='Text Line'></div>";
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";
                break;
            case "Content":
                litem = "<div  id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;' >" + CaseName + "<span class='pull-right cross-btn' id='spn24' onclick=crossdelete(" + divid + ");>&nbsp;</span></div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'><div class='span10 pull-left'>";
                litem += "<textarea id=txt_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " rows='3' placeholder='Description' onchange='Set_Section_Break(" + dividval + ");'></textarea><br clear='all' />";
                litem += "<div style='float:left;'>";
                litem += "<label id=lblContentSize" + divid + " style='width:60px;'>Font Size:</label>" + Get_DropDown_Data('ddlContentSize', divid, 'FontSize'); +"&nbsp;";
                litem += "</div>";
                litem += "<div style='float:left;'>";
                litem += "<label id=lblContentAlign_" + divid + " style='width:60px;vertical-align:top;'>Alignment:</label>" + Get_DropDown_Data('ddlContentAlign', divid, 'Alignment'); +"&nbsp;<br clear='all' />";
                litem += "</div>";
                litem += "<div style='float:left;'>";
                litem += "<label id=lblContentColor_" + divid + " style='width:60px;vertical-align:top;'>Color:</label> &nbsp;";
                litem += " <input class='color' value='66ff00' id='ColorPicker_" + divid + "' onchange='Set_Section_Break(" + dividval + ");' style='width:83%;height:25px;'>";                
                litem += "</div>";                
                litem += "<input type='checkbox' id=ChkRequired_" + divid + " style='width:3%;margin-top:20px;'><span style='margin-left: -9px;margin-top: 19px;width: 60px;'> Required</span></div>";
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";
                break;
            case "Paragraph Text":
                litem = "<div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;' >" + CaseName + "<span class='pull-right cross-btn' id='spn1' onclick=crossdelete(" + divid + ");>&nbsp;</span></div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'><div class='span3 pull-left'>";
                litem += "<input type='text' id=txtHeading_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " placeholder='Label'><br clear='all' />";
                litem += "<input type='checkbox' id=ChkRequired_" + divid + " class='width-10'><span> Required</span></div>";
                litem += "<div class='span7 pull-left'>";
                litem += "<textarea id=txt_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " rows='3' placeholder='Multiple Line'></textarea></div>";
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";
                break;
            case "Upload Image":

                litem = "<div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;' >" + CaseName + "<span class='pull-right cross-btn' id='spn25' onclick=crossdelete(" + divid + ");>&nbsp;</span></div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'> <div id='DivImgCtrl_" + divid + "' class='span3 pull-left'>";
                litem += "</div>";
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";
                $("#hdnCaseName").val(CaseName + "~DivImgCtrl_" + divid + "~" + divid);
                break;
            case "Section Break":
                litem = "<div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;' >" + CaseName + " <span class='pull-right cross-btn' id='spn26' onclick=crossdelete(" + dividval + ");>&nbsp;</span> </div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'> <div class='span3 pull-left'> <label style='float: left; padding: 7px 5px 0 0;'>  Width:</label>";
                litem += "<input type='text' class='width-80' id='Width_" + dividval + "' placeholder='width' onkeyup='Set_Section_Break(" + dividval + ");'> </div>";
                litem += "<div class='span2 pull-left'><label style='float: left; padding: 7px 5px 0 0;'>  Height:</label>";
                litem += "<select  id='ddlHeight_" + dividval + "' onchange='Set_Section_Break(" + dividval + ");'><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></div>";
                litem += "<div class='span3 pull-left'> <input class='color' value='66ff00' id='ColorPicker_" + divid + "' onchange='Set_Section_Break(" + dividval + ");'></div></br>";
                litem += "<hr id=HR_" + dividval + " style='width:100px;height;5px;border-radius:5px;background-color:Black; margin: 60px 0px;' >";                
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";
                break;
            case "Empty Space":
                litem = "<div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;'>" + CaseName + "<span class='pull-right cross-btn' id='spn8' onclick=crossdelete(" + dividval + ");>&nbsp;</span></div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'> <div class='clearfix'></div>";
                break;
            case "MultipleChoice":
            case "Checkbox":
            case "Single Choice":
            case "Dropdown":
                var DisplayName = "";
                var OptionType = "";
                if (CaseName == "MultipleChoice") {
                    OptionType = " Total Number of Radio:";
                    DisplayName = "Single Choice";
                }
                if (CaseName == "Checkbox") {
                    OptionType = " Total Number of CheckBox:";
                    DisplayName = "Multiple Choice";
                }
                if (CaseName == "Dropdown") {
                    OptionType = " Total Number of List Items:";
                    DisplayName = "Dropdown";
                }
                litem = " <div id=DivHeading_" + divid + " class='clearfix btn-lg' style='height:25px;width:640px;'>" + DisplayName + "<span class='pull-right cross-btn'  id='spn5' onclick=crossdelete(" + dividval + ");>&nbsp;</span></div>";
                litem += "<div id=DivContent_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " class='drop-panel'> <div class='span3 pull-left'>";
                litem += "<input type='text' id=txtlabel_" + divid + "_" + CaseName.toString().replace(/\s/g, '_') + " placeholder='label'><br clear='all' />  </div>";
                litem += "<div class='span6 pull-left'> <label style='float: left; padding: 7px 5px 0 0;'> " + OptionType + " </label>";
                litem += "<select id='Ctrllist_" + divid + "' style='width:100px;' onchange=Create_MultipleOptionControls('" + CaseName.toString().trim() + "~" + dividval + "');><option>Select</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select>";

                litem += "<br clear='all' />";
                //=== Controls will be Added in side of this Div ======
                litem += "<div id='Div_Controls_" + divid + "' >";
                litem += "</div>";
                //-----------------------------------------------------

                litem += "</div>";
                litem += "<div class='clearfix'>";
                litem += "</div>";
                litem += "</div>";

                break;
         }
        //------------------------------------------------------
        $("#" + divid).append(litem);
        Set_Image_Content($("#hdnCaseName").val());
        //------------------------------------------------------
    }//== IF Closed Here

}//=== Function Closed Here


function Set_Image_Content(ImgCtrlVal) {
    var paramList = ImgCtrlVal.toString().split('~');
    if (paramList.length > 1) {
        if (paramList[0].toString().trim() == "Upload Image") {
            var divElem = document.createElement('DIV');
            divElem.innerHTML = "<input  id=UploadFile_" + paramList[2] + " name=UploadFile_" + paramList[2] + " type='file' class='no-border'  accept='image/*'  onchange='showMyImage(this);'><img id='thumbnil_" + paramList[2] + "' style='width:75%; height:20%px; border-radius:5px; margin-left:250px; margin-top:-32px'/><label id='lblBgImage_" + paramList[2] + "'></label>";
            document.getElementById(paramList[1]).appendChild(divElem);
            $("#hdnCaseName").val("");
        }
    }
}
function Get_Case_Name( imagename) {
    if (imagename.toString() == "Title.png") {
        return "Title";
    }
    if (imagename.toString() == "Content.png") {
        return "Content";
    }
    if (imagename.toString() == "upload-img.png") {
        return "Upload Image";
    }
    if (imagename.toString() == "section-break.png") {
        return "Section Break";
    }
    if (imagename.toString() == "empty-space.png") {
        return "Empty Space";
    }
    if (imagename.toString() == "paragraph-text.png") {
        return "Paragraph Text";
    }
    if (imagename.toString() == "single-line.png") {
        return "Single Line Text";
    }
    if (imagename.toString() == "multiple-choice.png") {
        return "Checkbox";
    }

    if (imagename.toString() == "Single-choice.png") {
        return "MultipleChoice";
    }
    if (imagename.toString() == "dropdown.png") {
        return "Dropdown";
    }
    //=== FOR THE PURPOSE OF THE FORM FIELDS =================//
    if (imagename.toString() == "full-name.png") {
        return "Full Name";
    }
    if (imagename.toString() == "first-name.png") {
        return "First Name";
    }
    if (imagename.toString() == "last-name.png") {
        return "Last Name";
    }
    if (imagename.toString() == "email-address.png") {
        return "Email Address";
    }
    if (imagename.toString() == "company.png") {
        return "Company";
    }
    if (imagename.toString() == "phone-number.png") {
        return "Phone Number";
    }
    if (imagename.toString() == "address-1.png") {
        return "Address 1";
    }
    if (imagename.toString() == "address-2.png") {
        return "Address 2";
    }
    if (imagename.toString() == "city.png") {
        return "City";
    }
    if (imagename.toString() == "state.png") {
        return "State";
    }
    if (imagename.toString() == "zip.png") {
        return "Zip";
    }
    if (imagename.toString() == "custom-1.png") {
        return "Custom 1";
    }
    if (imagename.toString() == "custom-2.png") {
        return "Custom 2";
    }
    if (imagename.toString() == "custom-3.png") {
        return "Custom 3";
    }
}//== Function Closed here


//=== FUNCTION TO DISPLAY THE IMAGE ====
function showMyImage(fileInput) {
    var fileinputidval = fileInput.id;
    var thumbnilid = "";
    var Get_FileinputID = fileinputidval.toString().split('_');
    if (Get_FileinputID.length > 0) {
       thumbnilid = "thumbnil_" + Get_FileinputID[1]
    }
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {
            continue;
        }
        var img = document.getElementById(thumbnilid.toString());
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
} //==== Function Closed here

/*-------------------------------function for color picker in section break-------------------*/
function Set_Section_Break(divid) {
    var WidthVal = $("#Width_" + divid.id).val();
    var myPicker = new jscolor.color(document.getElementById('ColorPicker_' + divid.id), {})
    var HeightVal = $("#ddlHeight_" + divid.id).val();
    var obj = {
        'width': WidthVal.toString() + 'px',
        'height': HeightVal.toString(),
        'border-radius': '5px',
        'margin': '60px 0px',
        'background-color': '#' + myPicker.toString()
    }
    $('#HR_' + divid.id).css(obj);
}


/*-------------------------------function for color picker in section break-------------------*/
function Set_Color_Picker_Data(divid) {    
    var myPicker = new jscolor.color(document.getElementById('ColorPicker_' + divid), {})

    var obj = {
        'background-color': '#' + myPicker.toString()
    }
    $('#ColorPicker_' + divid).css(obj);
}

function Create_MultipleOptionControls(divvals) {
   //======================
     var Sectiontype="";
     var DivID="";
     var ControlType="";
     var Get_Section_Type_And_Div = divvals.toString().split('~');
     if (Get_Section_Type_And_Div.length > 0) {
        Sectiontype = Get_Section_Type_And_Div[0];
        DivID = Get_Section_Type_And_Div[1];
     }
     if(Sectiontype != null && Sectiontype.length > 0 && DivID != null && DivID.length > 0)
     {
        if(Sectiontype.toString() == "MultipleChoice")
        {
        ControlType = "radio";
        }
         if(Sectiontype.toString() == "Checkbox")
        {
        ControlType = "checkbox";
        }
       
         if(Sectiontype.toString() == "Dropdown")
        {
         ControlType = "text";
        }
     }
              
   //======================
    var CtrListVals = "";
    var v = $("#Ctrllist_" + DivID).val();
    if (v != "Select") {
        for (i = 1; i <= v; i++) {

            CtrListVals += "<div class='pull-left space-50' style='padding-top: 10px;'>";
            if (Sectiontype.toString() == "Dropdown") {
                CtrListVals += "<input type='" + ControlType + "' id='txt_" + DivID + "_" + i.toString() + "' placeholder='List Item " + i.toString() + "'>";
            }
            else {
                CtrListVals += "<input type='" + ControlType + "' id='Ctr_" + DivID + "_" + i.toString() + "'>";
            }
            CtrListVals += "</div>";


            if (Sectiontype.toString() != "Dropdown") {
                CtrListVals += "<span class='pull-left'><input type='text'  id='txt_" + DivID + "_" + i.toString() + "' placeholder='label " + i.toString() + "' class='space-10'></span> <br clear='all' />";
            }
        }

        $("#Div_Controls_" + DivID).html(CtrListVals);
    }
    else {
        $("#Div_Controls_" + DivID).html("");
    }
}
/*-------------------------------function for color picker in section break Closed hEre-------------------*/


//==== For the Purpose to Save the Data =====//
function Save_PopupWindow() {    
    var flag = ValidateForm();
    if (flag == false) {
        return false;
    }
    $("#btnSubmit").hide();
    Delete_Current_Survey($("#hdnModSurveyID").val());
    Save_Master_Popup_Data_val();
}
// To Delete Survey before Update//
function Delete_Current_Survey(PopupID) {
    $.ajax({
        contentType: "application/json; charset=utf-8",
        url: urlDeleteSurveyDetailUrl,
        data: { "popupid": PopupID
        },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {            
        }
    }
    function ajaxFailed() {

        alert("There is an error during operation.");
    }
}
function Save_PopupWindow_Detail(CurSurveyID) {
    var Divs = $("#divMain").children();
    var CaseName = "";
    var DivID = "";    
    if (CurSurveyID!="") {
        for (var i = 0; i < Divs.length; i++) {
            DivID = "DivHeading_" + Divs[i.toString()].id;
            var curDorder = parseInt(i) + 1;
            CaseName = $("#" + DivID).html();
            if (CaseName.toString().indexOf("<") > 0) {
                var LastStringLength = CaseName.indexOf("<");
                CaseName = CaseName.toString().substring(0, LastStringLength);
                if (!isNaN(CaseName)) {
                }
                else {
                    Save_Popup_Detail_Data_val(Divs[i.toString()].id, CaseName, CurSurveyID, curDorder);
                }
            }
        }

        setTimeout(function () {
            alert("Data Saved Successfully");
            var btn = document.getElementById("btnSubmitMain");
            btn.click();
        }, 2000);
    }
    else {
    }
}
function ValidateForm() {
    if ($("#txtpopupname").val().trim() == "") {
        return false;
    }
    else {
        return true;
    }
}


// To Insert Popup Detail Data
function Save_Popup_Detail_Data_val(DivID,CaseName,CurSurveyID,CurDorderData) {
    var CurDorder = CurDorderData;    
    var isrequired;
    switch (CaseName.trim()) {
        case "Title":            
            var TitleTextbox = "txt_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
            var ddlTitleSize = "ddlTitleSize_" + DivID;
            var ddlTitleAlign = "ddlTitleAlign_" + DivID;
            var txtColorPicker = "ColorPicker_" + DivID.replace(/\s/g, '_'); ;
            var ChkRequired = "ChkRequired_" + DivID;
            var isrequired;
            if ($("#" + ChkRequired).prop('checked')) {
                isrequired = "TRUE";
            }
            else {
                isrequired = "FALSE";
            }

            var ClsPopupBuilderDetail = {
                surveydetailid: "0",
                surveypopupid: CurSurveyID,
                filedtype: CaseName,
                dorder: CurDorder,
                textfieldheading1: CaseName,
                textfieldheadingvalue1: $("#" + TitleTextbox).val(),
                textfieldheading2: "FontSize",
                textfieldheadingvalue2: $("#" + ddlTitleSize).val(),
                textfieldheading3: "Alignment",
                textfieldheadingvalue3: $("#" + ddlTitleAlign).val(),
                textfieldheading4: "FontColor",
                textfieldheadingvalue4: $("#" + txtColorPicker).val(),
                textfieldheading5: "Required",
                textfieldheadingvalue5: isrequired,
                textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
            };
            Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Single Line Text":
        case "Full Name":
        case "First Name":
        case "Last Name":
        case "Email Address":
        case "Company":
        case "Phone Number":
        case "Address 1":
        case "Address 2":
        case "City":
        case "State":
        case "Zip":
        case "Custom 1":
        case "Custom 2":
        case "Custom 3":
            var HeadingText = "txtLabel_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
            var HeadingValue = "txtValue_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
            var ChkRequired = "ChkRequired_" + DivID;
            if ($("#" + ChkRequired).prop('checked')) {
                isrequired = "TRUE";
            }
            else {
                isrequired = "FALSE";
            }
            var ClsPopupBuilderDetail = {
                surveydetailid: "0",                
                surveypopupid: CurSurveyID,
                filedtype: CaseName,
                dorder: CurDorder,
                textfieldheading1: CaseName,
                textfieldheadingvalue1: CaseName,
                textfieldheading2: "HeadingLabel",
                textfieldheadingvalue2: $("#" + HeadingText).val(),
                textfieldheading3: "HeadingTextLine",
                textfieldheadingvalue3: $("#" + HeadingValue).val(),
                textfieldheading4: "Required",
                textfieldheadingvalue4: isrequired,
                textfieldheading5: "",
                textfieldheadingvalue5: "",
                textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
            };
            Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Content":            
            var ContentTextbox = "txt_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
            var ddlContentSize = "ddlContentSize_" + DivID;
            var ddlContentAlign = "ddlContentAlign_" + DivID;
            var txtColorPicker = "ColorPicker_" + DivID.replace(/\s/g, '_'); ;
            var ChkRequired = "ChkRequired_" + DivID;
            var isrequired;
            if ($("#" + ChkRequired).prop('checked')) {
                isrequired = "TRUE";
            }
            else {
                isrequired = "FALSE";
            }

            var ClsPopupBuilderDetail = {
                surveydetailid: "0",
                surveypopupid: CurSurveyID,
                filedtype: CaseName,
                dorder: CurDorder,
                textfieldheading1: CaseName,
                textfieldheadingvalue1: $("#" + ContentTextbox).val(),
                textfieldheading2: "FontSize",
                textfieldheadingvalue2: $("#" + ddlContentSize).val(),
                textfieldheading3: "Alignment",
                textfieldheadingvalue3: $("#" + ddlContentAlign).val(),
                textfieldheading4: "FontColor",
                textfieldheadingvalue4: $("#" + txtColorPicker).val(),
                textfieldheading5: "Required",
                textfieldheadingvalue5: isrequired,
                textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
            };
            Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Paragraph Text":
            var txtLabel = "txtHeading_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
            var txtTextLine = "txt_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');           
            var ChkRequired = "ChkRequired_" + DivID;
            var isrequired;
            if ($("#" + ChkRequired).prop('checked')) {
                isrequired = "TRUE";
            }
            else {
                isrequired = "FALSE";
            }

            var ClsPopupBuilderDetail = {
                surveydetailid: "0",                
                surveypopupid: CurSurveyID,
                filedtype: CaseName,
                dorder: CurDorder,
                textfieldheading1: CaseName,
                textfieldheadingvalue1: CaseName,
                textfieldheading2: "HeadingLabel",
                textfieldheadingvalue2: $("#" + txtLabel).val(),
                textfieldheading3: "HeadingTextLine",
                textfieldheadingvalue3: $("#" + txtTextLine).val(),
                textfieldheading4: "Required",
                textfieldheadingvalue4: isrequired,
                textfieldheading5: "",
                textfieldheadingvalue5: "",
                textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
            };
            Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Upload Image":
            var FileUpdaload = "UploadFile_" + DivID.toString();
            var lblGridBgImage = "lblBgImage_" + DivID.toString();
            var ImgName = "";
            if ($("#" + FileUpdaload).val().trim() != "") {
                ImgName = $("#" + FileUpdaload).val();
            }
            else {
                if ($("#" + lblGridBgImage).text().trim() != "") {
                    ImgName = $("#" + lblGridBgImage).text();
                }
            }

            var ImageControl = "thumbnil_" + DivID;
            var ClsPopupBuilderDetail = {
                surveydetailid: "0",
                surveypopupid: CurSurveyID,
                filedtype: CaseName,
                dorder: CurDorder,
                textfieldheading1: CaseName,
                textfieldheadingvalue1: CaseName,
                textfieldheading2: "ImageName",
                textfieldheadingvalue2: ImgName,
                textfieldheading3: "",
                textfieldheadingvalue3: "",
                textfieldheading4: "",
                textfieldheadingvalue4: "",
                textfieldheading5: "",
                textfieldheadingvalue5: "",
                textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
            };
            Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Section Break":
                var txtWidth="Width_" + DivID;
                var ddlHeight="ddlHeight_" + DivID;
                var txtColor="ColorPicker_" + DivID;
                    var ClsPopupBuilderDetail = {
                            surveydetailid: "0",
                            surveypopupid: CurSurveyID,
                            filedtype: CaseName,
                            dorder: CurDorder,
                            textfieldheading1: CaseName,
                            textfieldheadingvalue1: CaseName,
                            textfieldheading2: "Width",
                            textfieldheadingvalue2:$("#"+txtWidth).val(),
                            textfieldheading3: "Height",
                            textfieldheadingvalue3: $("#"+ddlHeight).val(),
                            textfieldheading4: "Color",
                            textfieldheadingvalue4: $("#"+txtColor).val(),
                             textfieldheading5: "",
                textfieldheadingvalue5: "",
                 textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
             };
             Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Empty Space":
                        var ClsPopupBuilderDetail = {
                            surveydetailid: "0",
                            surveypopupid: CurSurveyID,
                            filedtype: CaseName,
                            dorder: CurDorder,
                            textfieldheading1: CaseName,
                            textfieldheadingvalue1: CaseName,
                            textfieldheading2: "",
                            textfieldheadingvalue2: "",
                            textfieldheading3: "",
                            textfieldheadingvalue3: "",
                            textfieldheading4: "",
                            textfieldheadingvalue4: "",
                            textfieldheading5: "",
                            textfieldheadingvalue5: "",
                            textfieldheading6: "",
                            textfieldheadingvalue6: "",
                            textfieldheading7: "",
                            textfieldheadingvalue7: "",
                            textfieldheading8: "",
                            textfieldheadingvalue8: "",
                            textfieldheading9: "",
                            textfieldheadingvalue9: "",
                            textfieldheading10: "",
                            textfieldheadingvalue10: "",
                            optionvalue1: "",
                            optionvalue2: "",
                            optionvalue3: "",
                            optionvalue4: "",
                            optionvalue5: "",
                            optionvalue6: "",
                            optionvalue7: "",
                            optionvalue8: "",
                            optionvalue9: "",
                            optionvalue10: ""
                        };
                    Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
        case "Multiple Choice":
        case "Checkbox":
        case "Single Choice":
        case "Dropdown":
            if (CaseName == "Single Choice") {
                CaseName = "MultipleChoice";
            }
            if (CaseName == "Multiple Choice") {
                CaseName = "Checkbox";
            }
            var txtHeading = "txtlabel_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
            var ddlControlVal = "Ctrllist_" + DivID;
            var ItemCount = $("#" + ddlControlVal).val();
            var TextItem = "";
            var ItemText = "";
            if (ItemCount != "Select") {
                for (var i = 1; i <= parseInt(ItemCount); i++) {
                    TextItem = "txt_" + DivID + "_" + i.toString();
                    ItemText = ItemText + "~" + $("#" + TextItem).val();
                }
            }
           ItemText = ItemText.toString().substring(1);
            var ClsPopupBuilderDetail = {
                surveydetailid: "0",
                surveypopupid: CurSurveyID,
                filedtype: CaseName,
                dorder: CurDorder,
                textfieldheading1: "Heading",
                textfieldheadingvalue1: $("#" + txtHeading).val(),
                textfieldheading2: "ItemCount",
                textfieldheadingvalue2: ItemCount.toString(),
                textfieldheading3: "ItemValue",
                textfieldheadingvalue3: ItemText,
                textfieldheading4: "",
                textfieldheadingvalue4: "",
                textfieldheading5: "",
                textfieldheadingvalue5: "",
                textfieldheading6: "",
                textfieldheadingvalue6: "",
                textfieldheading7: "",
                textfieldheadingvalue7: "",
                textfieldheading8: "",
                textfieldheadingvalue8: "",
                textfieldheading9: "",
                textfieldheadingvalue9: "",
                textfieldheading10: "",
                textfieldheadingvalue10: "",
                optionvalue1: "",
                optionvalue2: "",
                optionvalue3: "",
                optionvalue4: "",
                optionvalue5: "",
                optionvalue6: "",
                optionvalue7: "",
                optionvalue8: "",
                optionvalue9: "",
                optionvalue10: ""
            };
            Insert_Survey_Detail(ClsPopupBuilderDetail);
            break;
    }
}

// To Insert the Popup Window Details 
function Insert_Survey_Detail(ClsPopupBuilderDetail) {
   objSurveyPopupDetailData = JSON.stringify(ClsPopupBuilderDetail);
        $.ajax({
            url: urlInsertSurveyDetailDataUrl,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: objSurveyPopupDetailData,
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });
        function ajaxSucceeded(data) {
            if (data != null) {
           }
            else {

            }
        }
        function ajaxFailed() {
          alert("There is an error during operation.");
        }
}

// To Insert Popup Master Data
function Save_Master_Popup_Data_val() {
    var CurSurveyID = "0";
    if ($("#hdnModSurveyID").val() != "") {
        CurSurveyID = $("#hdnModSurveyID").val();
    }

    var BgImgname = "";
    if ($("#PopupFileUpload").val().trim() != "") {
        BgImgname = $("#PopupFileUpload").val();
    }
    else {
        if ($("#lblBgImage").text().trim() != "") {
            BgImgname = $("#lblBgImage").text();
        }
    }
    
    var ClsPopupBuilderMaster = {
        surveypopupid: CurSurveyID,
        surveyname: $("#txtpopupname").val(),
        surveydescription: $("#txtpopupdescription").val(),
        windowheight: $("#txtWindowHeight").val(),
        windowwidth: $("#txtWindowWidth").val(),
        backgroundcolor: $("#txtBackgroundColor").val(),
        backgroundimage: BgImgname,
        expirationdate: $("#datepicker").val(),
        createdby: $("#hdnCurUser").val()
    };
    var objPopupMaster = JSON.stringify(ClsPopupBuilderMaster);
    $.ajax({
        url: urlInsertSurveyDataUrl,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: objPopupMaster,
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            Save_PopupWindow_Detail(data.toString());
        }
        else {

        }
    }
    function ajaxFailed() {

        alert("There is an error during operation.");
    }
}

    // For the purpose of Delete the control and Div
    function crossdelete(ctrlID) {
        var divID = ctrlID.id.toString();
        var CtrlType = $("#DivHeading_" + divID).html();
        if (CtrlType.toString().indexOf("<") > 0) {
                var LastStringLength = CtrlType.indexOf("<");
                CtrlType = CtrlType.toString().substring(0, LastStringLength);
                if (!isNaN(CtrlType)) {
                }
                else {
                    switch (CtrlType.trim()) {
                        case "Full Name":
                            $("#" + divID).remove();
                            $("#Dvfull-name").append("<img class='ui-draggable' src='../images/SurveyImages/FF_full-name.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;

                        case "First Name":
                            $("#" + divID).remove();
                            $("#Dvfirst-name").append("<img class='ui-draggable' src='../images/SurveyImages/FF_first-name.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        
                         case "Last Name":
                            $("#" + divID).remove();
                            $("#Dvlast-name").append("<img class='ui-draggable' src='../images/SurveyImages/FF_last-name.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Email Address":
                            $("#" + divID).remove();
                            $("#Dvemail-address").append("<img class='ui-draggable' src='../images/SurveyImages/FF_email-address.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Company":
                            $("#" + divID).remove();
                            $("#Dvcompany").append("<img class='ui-draggable' src='../images/SurveyImages/FF_company.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Phone Number":
                            $("#" + divID).remove();
                            $("#Dvphone-number").append("<img class='ui-draggable' src='../images/SurveyImages/FF_phone-number.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Address 1":
                            $("#" + divID).remove();
                            $("#Dvadress-1").append("<img class='ui-draggable' src='../images/SurveyImages/FF_address-1.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Address 2":
                            $("#" + divID).remove();
                            $("#Dvaddress-2").append("<img class='ui-draggable' src='../images/SurveyImages/FF_address-2.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "City":
                            $("#" + divID).remove();
                            $("#Dvcity").append("<img class='ui-draggable' src='../images/SurveyImages/FF_city.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "State":
                            $("#" + divID).remove();
                            $("#Dvstate").append("<img class='ui-draggable' src='../images/SurveyImages/FF_state.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Zip":
                            $("#" + divID).remove();
                            $("#Dvzip").append("<img class='ui-draggable' src='../images/SurveyImages/FF_zip.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Custom 1":
                            $("#" + divID).remove();
                            $("#Dvcustom-1").append("<img class='ui-draggable' src='../images/SurveyImages/FF_custom-1.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Custom 2":
                            $("#" + divID).remove();
                            $("#Dvcustom-2").append("<img class='ui-draggable' src='../images/SurveyImages/FF_custom-2.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                        case "Custom 3":
                            $("#" + divID).remove();
                            $("#Dvcustom-3").append("<img class='ui-draggable' src='../images/SurveyImages/FF_custom-3.png' style='position: relative;'>");
                            Convert_Left_Side_Images_Draggable();
                            break;
                            
                        default:
                            $("#" + divID).remove();
                            break;
                    }
                }
            }
       }

    // To Display the Popup Window 
    function Display_Popup(PopupID) {
        Display_Popup_Master_Data(PopupID);
        Display_Popup_Detail_Data(PopupID);
        if ($("#hdnMode").val() == "View") {
            $("#btnSubmit").hide();
        }
        else {
            $("#btnSubmit").show();
        }
    }

    // To Get Popup Window responses
    function Get_PopupResponses(PopupID) {
        $.ajax({
            url: urlFetchSurveyDetailUrl,
            contentType: 'application/json; charset=utf-8',
            data: { "surveypopupid": PopupID.toString() },
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });
        function ajaxSucceeded(data) {
            if (data != null) {
                if (parseInt(data) > 0) {
                    $("#btnSubmit").hide();
                }

                else {
                    $("#btnSubmit").show();
                }
            }
            else {

            }
        }
        function ajaxFailed() {
            alert("There is an error during operation.");
        }
    }
    // To Display the Popup Window Master Data

    function Display_Popup_Master_Data(PopupID) {
        $.ajax({
            url: urlFetchSurveyMasterDataUrl,
            contentType: 'application/json; charset=utf-8',
            data: { "surveypopupid": PopupID.toString() },
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });
        function ajaxSucceeded(data) {
            if (data != null) {
                var result = jQuery.parseJSON(data);
                if (result.length > 0) {
                    $("#txtpopupname").val(result[0].surveyname);
                    $("#txtpopupdescription").val(result[0].surveydescription);
                    $("#txtWindowHeight").val(result[0].windowheight);
                    $("#txtWindowWidth").val(result[0].windowwidth);
                    $("#txtBackgroundColor").focus();
                    $("#txtBackgroundColor").val(result[0].backgroundcolor);
                    $("#datepicker").val(result[0].expirationdate);
                    var ImgagePath = "/UploadedFiles/image/" + result[0].backgroundimage.trim().replace("C:~fakepath~","");
                    $("#lblBgImage").text(result[0].backgroundimage);
                    $("#imgBackground").attr("src", ImgagePath);
                    $("#txtpopupname").focus();
                }
            }
            else {

            }
        }
        function ajaxFailed() {
             alert("There is an error during operation.");
        }
    }

    // To Display the Popup Window Details Grid

    function Display_Popup_Detail_Data(PopupID) {
        $.ajax({
            url: urlFetchSurveyDetailDataUrl,
            contentType: 'application/json; charset=utf-8',
            data: { "surveypopupid": PopupID.toString() },
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });
        function ajaxSucceeded(data) {
            if (data != null) {
                  var result = jQuery.parseJSON(data);
                  if (result.length > 0) {
                      for (var i = 0; i < result.length; i++) {
                          adddiv();
                          jQuery("#hid1").val("dvDest" + result[i].dorder);

                          Add_Control_block(result[i].filedtype.trim());
                          var DivID = jQuery("#hid1").val();
                          var CaseName = result[i].filedtype.trim();
                          switch (CaseName) {
                              case "Title":
                                  var TitleTextbox = "txt_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ddlTitleSize = "ddlTitleSize_" + DivID;
                                  var ddlTitleAlign = "ddlTitleAlign_" + DivID;
                                  var txtColorPicker = "ColorPicker_" + DivID.replace(/\s/g, '_'); ;
                                  var ChkRequired = "ChkRequired_" + DivID;
                                  $("#" + TitleTextbox).val(result[i].textfieldheadingvalue1);
                                  $("#" + ddlTitleSize).val(result[i].textfieldheadingvalue2);
                                  $("#" + ddlTitleAlign).val(result[i].textfieldheadingvalue3);
                                  $("#" + txtColorPicker).val(result[i].textfieldheadingvalue4);
                                  if (result[i].textfieldheadingvalue5 == "TRUE") {
                                      $("#" + ChkRequired).attr('checked', true);
                                  }
                                  else {
                                      $("#" + ChkRequired).attr('checked', false);
                                  }
                                  Set_Color_Picker_Data(DivID);
                                  break;
                              case "Single Line Text":
                              case "Full Name":
                              case "First Name":
                              case "Last Name":
                              case "Email Address":
                              case "Company":
                              case "Phone Number":
                              case "Address 1":
                              case "Address 2":
                              case "City":
                              case "State":
                              case "Zip":
                              case "Custom 1":
                              case "Custom 2":
                              case "Custom 3":
                                  var HeadingText = "txtLabel_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var HeadingValue = "txtValue_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ChkRequired = "ChkRequired_" + DivID;
                                  $("#" + HeadingText).val(result[i].textfieldheadingvalue2);
                                  $("#" + HeadingValue).val(result[i].textfieldheadingvalue3);
                                  if (result[i].textfieldheadingvalue4 == "TRUE") {
                                      $("#" + ChkRequired).attr('checked', true);
                                  }
                                  else {
                                      $("#" + ChkRequired).attr('checked', false);
                                  }

                                  break;
                              case "Content":
                                  var ContentTextbox = "txt_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ddlContentSize = "ddlContentSize_" + DivID;
                                  var ddlContentAlign = "ddlContentAlign_" + DivID;
                                  var txtColorPicker = "ColorPicker_" + DivID.replace(/\s/g, '_'); ;
                                  var ChkRequired = "ChkRequired_" + DivID;
                                  var isrequired;
                                  if ($("#" + ChkRequired).prop('checked')) {
                                      isrequired = "TRUE";
                                  }
                                  else {
                                      isrequired = "FALSE";
                                  }

                                  $("#" + ContentTextbox).val(result[i].textfieldheadingvalue1);
                                  $("#" + ddlContentSize).val(result[i].textfieldheadingvalue2);
                                  $("#" + ddlContentAlign).val(result[i].textfieldheadingvalue3);
                                  $("#" + txtColorPicker).val(result[i].textfieldheadingvalue4);
                                  if (result[i].textfieldheadingvalue5 == "TRUE") {
                                      $("#" + ChkRequired).attr('checked', true);
                                  }
                                  else {
                                      $("#" + ChkRequired).attr('checked', false);
                                  }
                                  Set_Color_Picker_Data(DivID);
                                  break;
                              case "Paragraph Text":
                                  var txtLabel = "txtHeading_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var txtTextLine = "txt_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ChkRequired = "ChkRequired_" + DivID;
                                  $("#" + txtLabel).val(result[i].textfieldheadingvalue2);
                                  $("#" + txtTextLine).val(result[i].textfieldheadingvalue3);
                                  if (result[i].textfieldheadingvalue4 == "TRUE") {
                                      $("#" + ChkRequired).attr('checked', true);
                                  }
                                  else {
                                      $("#" + ChkRequired).attr('checked', false);
                                  }

                                  break;
                              case "Upload Image":
                                  var ImageControl = "thumbnil_" + DivID;
                                  var lblDetailBgImage = "lblBgImage_" + DivID;
                                  var ImgagePath = "/UploadedFiles/image/" + result[i].textfieldheadingvalue2;
                                  ImgagePath = ImgagePath.toString().replace("C:~fakepath~", "");
                                  $("#" + ImageControl).attr("src", ImgagePath);
                                  $("#" + lblDetailBgImage).text(result[i].textfieldheadingvalue2);
                                  $("#" + lblDetailBgImage).hide();
                                  break;
                              case "Section Break":
                                  var txtWidth = "Width_" + DivID;
                                  var ddlHeight = "ddlHeight_" + DivID;
                                  var txtColor = "ColorPicker_" + DivID;
                                  $("#" + txtColor).val(result[i].textfieldheadingvalue4);

                                  $("#" + ddlHeight).val(result[i].textfieldheadingvalue3);

                                  $("#" + txtWidth).val(result[i].textfieldheadingvalue2);
                                  break;
                              case "Empty Space":
                                  break;
                              case "Multiple Choice":
                              case "MultipleChoice":
                              case "SingleChoice":
                              case "Single Choice":
                                  var txtHeading = "txtlabel_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ddlControlVal = "Ctrllist_" + DivID;
                                  $("#" + txtHeading).val(result[i].textfieldheadingvalue1);
                                  var ItemCount = result[i].textfieldheadingvalue2;
                                  $("#" + ddlControlVal).val(ItemCount.toString());
                                  var ItemText = result[i].textfieldheadingvalue3;
                                  Create_MultipleOptionControls_WithValue(CaseName + "#" + DivID + "#" + ItemText);
                                  break;
                              case "Checkbox":
                                  var txtHeading = "txtlabel_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ddlControlVal = "Ctrllist_" + DivID;
                                  $("#" + txtHeading).val(result[i].textfieldheadingvalue1);
                                  var ItemCount = result[i].textfieldheadingvalue2;
                                  $("#" + ddlControlVal).val(ItemCount.toString());
                                  var ItemText = result[i].textfieldheadingvalue3;
                                  Create_MultipleOptionControls_WithValue(CaseName + "#" + DivID + "#" + ItemText);
                                  break;

                              case "Dropdown":
                                  var txtHeading = "txtlabel_" + DivID + "_" + CaseName.toString().replace(/\s/g, '_');
                                  var ddlControlVal = "Ctrllist_" + DivID;
                                  $("#" + txtHeading).val(result[i].textfieldheadingvalue1);
                                  var ItemCount = result[i].textfieldheadingvalue2;
                                  $("#" + ddlControlVal).val(ItemCount.toString());
                                  var ItemText = result[i].textfieldheadingvalue3;
                                  Create_MultipleOptionControls_WithValue(CaseName + "#" + DivID + "#" + ItemText);
                                  break;
                          }
                      }
                  }
            }
            else {

            }
        }
        function ajaxFailed() {

            alert("There is an error during operation.");
        }
    }


    // To Create the Multiple Controls with Values 
    function Create_MultipleOptionControls_WithValue(divvals) {
        //======================
        var Sectiontype = "";
        var DivID = "";
        var ControlType = "";
        var Get_Section_Type_And_Div = divvals.toString().split('#');
        if (Get_Section_Type_And_Div.length > 0) {
            Sectiontype = Get_Section_Type_And_Div[0];
            DivID = Get_Section_Type_And_Div[1];
        }
        if (Sectiontype != null && Sectiontype.length > 0 && DivID != null && DivID.length > 0) {
            if (Sectiontype.toString() == "MultipleChoice") {
                ControlType = "radio";
            }
            if (Sectiontype.toString() == "Checkbox") {
                ControlType = "checkbox";
            }

            if (Sectiontype.toString() == "Dropdown") {
                ControlType = "text";
            }
        }



////======================
        var CtrListVals = "";
        var ControlItems = Get_Section_Type_And_Div[2];
        var ItemList = ControlItems.toString().split('~');
        if (ItemList.length>0) {
            for (var i = 1; i <= ItemList.length; i++) {
                CtrListVals += "<div class='pull-left space-50' style='padding-top: 10px;'>";
                if (Sectiontype.toString() == "Dropdown") {
                    CtrListVals += "<input type='" + ControlType + "' id='txt_" + DivID + "_" + i.toString() + "' placeholder='List Item " + i.toString() + "' value='" + ItemList[i - 1].toString().trim() + "'>";
                }
                else {
                    CtrListVals += "<input type='" + ControlType + "' id='Ctr_" + DivID + "_" + i.toString() + "'>";
                }
                CtrListVals += "</div>";
                if (Sectiontype.toString() != "Dropdown") {
                    CtrListVals += "<span class='pull-left'><input type='text'  id='txt_" + DivID + "_" + i.toString() + "' placeholder='label " + i.toString() + "' class='space-10' value='" + ItemList[i - 1].toString().trim() + "'></span> <br clear='all' />";
                }
            }

            $("#Div_Controls_" + DivID).html(CtrListVals);
        }
        else {
            $("#Div_Controls_" + DivID).html("");
        }
    }


    // To upload Files
    function MyDatFileUpload(ctrl) {
       var btnupload = document.getElementById("btnUploadFile");
    }