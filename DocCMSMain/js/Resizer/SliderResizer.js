﻿jQuery(document).ready(function () {
    var jcrop_api;
    OnPageLoad();
    jQuery("#chkResizeImageCreate").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#chkCustomeImageCreate").prop("checked", false);
            jQuery("#divCustomResizeImageCreate").show();
            jQuery("#divCustomImageCreate").hide();
            jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
            jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
            CheckResizeValues();
        } else {
            jQuery("#chkCustomeImageCreate").prop("checked", true);
            jQuery("#divCustomResizeImageCreate").hide();
            jQuery("#divCustomImageCreate").show();
            jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
            jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
        }
    });

    jQuery("#chkCustomeImageCreate").click(function () {//
        if (jQuery(this).is(':checked')) {
            jQuery("#chkResizeImageCreate").prop("checked", false);
            jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
            jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
            jQuery("#divCustomImageCreate").show();
            jQuery("#divCustomResizeImageCreate").hide();
            var sliderImageSize = jQuery("#ddlSizeResize").val();
            var MiddleImageWidth = 0;
            var MiddleImageHeight = 0;
            var MarginTopAndBottom = 0;
            if (sliderImageSize.toString().indexOf("1900") > -1) {
                MiddleImageWidth = parseInt(jQuery("#hdnActualImageWidth").val()) / 6;
                MiddleImageHeight = parseInt(jQuery("#hdnActualImageHeight").val()) / 6;
                MarginTopAndBottom = parseInt(116 - MiddleImageHeight) / 2;
            }
            else {
                MiddleImageWidth = parseInt(jQuery("#hdnActualImageWidth").val()) / 3;
                MiddleImageHeight = parseInt(jQuery("#hdnActualImageHeight").val()) / 3;
                MarginTopAndBottom = parseInt(96 - MiddleImageHeight) / 2;
            }
            jQuery("#ImgCustomeImagePreviewRunning").css("margin-top", parseInt(MarginTopAndBottom));
            jQuery("#ImgCustomeImagePreviewRunning").css("margin-bottom", parseInt(MarginTopAndBottom));
            jQuery("#ImgCustomeImagePreviewRunning").css("width", parseInt(MiddleImageWidth));
            jQuery("#ImgCustomeImagePreviewRunning").css("height", parseInt(MiddleImageHeight));
        } else {
            jQuery("#chkResizeImageCreate").prop("checked", true);
            jQuery("#divCustomImageCreate").hide();
            jQuery("#divCustomResizeImageCreate").show();
            jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
            jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
        }
    });

    jQuery("#chkFinalCrop").click(function () {//
        if (jQuery(this).is(':checked')) {
             Set_Cropper(1);
            jQuery("#btnCropWithResize").show();
        } else {
            Set_Cropper(0);
            jQuery("#btnCropWithResize").hide();
        }
    });

    jQuery("#chkAspectRatioResize").click(function () {//
        if (jQuery(this).is(':checked')) {
            jQuery("#hdnResizeAsepctRatio").val("True");
            setTimeout(function () { PreviewResizedImage(); }, 200);
            jQuery("#uniform-chkAspectRatioResize").find("span").addClass("checked");

        } else {
            jQuery("#hdnResizeAsepctRatio").val("False");
            jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
            setTimeout(function () { PreviewResizedImage(); }, 200);
        }
    });

    jQuery("#chkFinalCrop").click(function () {//
        if (jQuery(this).is(':checked')) {
            Set_Cropper(1);
            jQuery("#btnCropWithResize").show();
        } else {
            Set_Cropper(0);
            jQuery("#btnCropWithResize").hide();
        }
    });
});

// SET Customer Image outer Size for slider

function CustomImageChecked() {
    if (jQuery("#chkResizeImageCreate").prop("checked") == true) {

    }
    else {

    }
}
function SetCustomImageOuterSize() {
    if (jQuery("#ddlSizeCustomImage").val().toString().trim().indexOf("1900") > -1) {
        jQuery("#divCustomImagePreviewRunning").css("width", "316px");
        jQuery("#divCustomImagePreviewRunning").css("height", "116px");
    }
    else {
        jQuery("#divCustomImagePreviewRunning").css("width", "165px");
        jQuery("#divCustomImagePreviewRunning").css("height", "98px");
    }
}

function OnPageLoad() {
    jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
    jQuery("#chkResizeImageCreate").prop("checked", true);
    jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
    jQuery("#chkCustomeImageCreate").prop("checked", false);
    jQuery("#txtCustomWidth").val("1900");
    jQuery("#txtCustomHeight").val("700");
    jQuery("#chkAspectRatioResize").prop("checked", false);
    jQuery("#hdnResizeAsepctRatio").val("False");
    jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
}
//-------image------
function ColorChange(Textbox) {
              var colorName = jQuery("#" + Textbox.id).val();
              var Image = jQuery("#ImgResizeImage").attr("src");
              jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
              jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
          }

          function ManageAspectRatio(ratioparam, e) {
              var key;
              if (window.event) {
                  key = window.event.keyCode;     //IE
              }
              else {
                  key = e.which;      //firefox              
              }
              if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
                  if (jQuery("#chkAspectRatioResize").is(':checked')) {
                      if (ratioparam == "w") {
                          var width = jQuery("#txtCustomWidth").val();
                          if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                              jQuery("#txtCustomWidth").css("color", "Red");
                              jQuery("#txtCustomHeight").val("0");
                          }
                          else {
                              var height = parseFloat(jQuery("#txtCustomWidth").val()) / 1.333;
                              jQuery("#txtCustomHeight").val(parseInt(height));
                          }
                      }
                      else {
                          var height = jQuery("#txtCustomHeight").val();
                          if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                              jQuery("#txtCustomHeight").css("color", "Red");
                              jQuery("#txtCustomWidth").val("0");
                          }
                          else {
                              var width = parseFloat(jQuery("#txtCustomHeight").val()) * 1.333;
                              jQuery("#txtCustomWidth").val(parseInt(width));
                          }
                      }

                  }
                  else {
                      jQuery("#ImgResizePreview").Jcrop({
                          onSelect: SelectCropArea,
                          bgFade: true

                      }, function () {
                          jcrop_api = this;
                      });
                      jcrop_api.destroy();
                      PreviewResizedImage();
                      jQuery("#ImgResizePreview").css("visibility", "visible");
                  }
              }
              else {
                  return false;
              }
          }

          function CheckResizeValues() {
              setTimeout(function () { PreviewResizedImage(); }, 200);
          }

          function PreviewResizedImage() {
              var actWidth = jQuery("#hdnActualImageWidth").val();
              var actHeight = jQuery("#hdnActualImageHeight").val();
              var ResizeDDLVal = jQuery("#ddlSizeResize").val();
              var resizeWidth = jQuery("#txtCustomWidth").val();
              var resizeHeight = jQuery("#txtCustomHeight").val();
              if (ResizeDDLVal.indexOf("1900") > -1) {
                  resizeWidth = parseInt(resizeWidth / 6);
                  resizeHeight = parseInt(resizeHeight / 6);
                  actWidth = parseInt(actWidth / 6);
                  actHeight = parseInt(actHeight / 6);
              }
              else {
                  resizeWidth = parseInt(resizeWidth / 3);
                  resizeHeight = parseInt(resizeHeight / 3);
                  actWidth = parseInt(actWidth / 3);
                  actHeight = parseInt(actHeight / 3);
              }
              if (jQuery("#hdnResizeAsepctRatio").val() == "True") {
                  var ratioX = resizeWidth / actWidth;
                  var ratioY = resizeHeight / actHeight;
                  var ratio = Math.max(ratioX, ratioY);
                  var newWidth = parseInt(parseInt(resizeWidth) * ratio);
                  var newHeight = parseInt(parseInt(resizeHeight) * ratio);
                  newWidth = parseInt(resizeWidth);
                  newHeight = parseInt(resizeHeight);
                  jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                  jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                  jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                  jQuery("#ImgResizePreview").css("max-width", "none");

              }
              else {
                  var newWidth = parseInt(resizeWidth);
                  var newHeight = parseInt(resizeHeight);
                  jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                  jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                  jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                  jQuery("#ImgResizePreview").css("max-width", "none");
              }
              CheckImageFits(newHeight, newWidth);
          }

          function CheckImageFits(newHeight, newWidth) {
              var RequiredWidth = parseInt(jQuery("#divResizePreview").css("width"));
              var Requiredheight = parseInt(jQuery("#divResizePreview").css("height"));
              if ((RequiredWidth < newWidth) || (Requiredheight < newHeight)) {
                  jQuery("#lblUnfitMsg").show();
                  jQuery("#spnFinalCrop").show();
                  jQuery("#btnCropWithResize").show();
                  jQuery("#btnResizeImage").hide();
              }
              else {
                  jQuery("#lblUnfitMsg").hide();
                  jQuery("#spnFinalCrop").hide();
                  jQuery("#btnResizeImage").show();
                  jQuery("#btnCropWithResize").hide();
              }
              return false;
          }

          //Ready Function Close

          function Set_Cropper(setVal) {
              if (setVal == 1) {
                  var reqsize = jQuery("#ddlSizeResize").val().split('x');
                  var cropWidth = 0;
                  var cropHeight = 0;
                  var CropSize = "";
                  var cropHeight = 0;
                  if(reqsize[0]=="1900")
                  {
                      cropWidth = 316;
                      cropHeight = 116;
                  }
                  else
                  {
                    cropWidth = 165;
                    cropHeight = 98;
                  }
                  CropSize = reqsize[0] + ' x ' + reqsize[1];

                  jQuery("#maxHeight").val(cropHeight.toString());
                  jQuery("#maxWidth").val(cropWidth.toString());
                  var imagewidth = parseInt(jQuery("#txtCustomWidth").val());
                  var imageHeight = parseInt(jQuery("#txtCustomHeight").val());
                  
                  jQuery("#ImgResizePreview").Jcrop({
                      onSelect: SelectCropArea,
                      onChange: SelectCropArea,
                      bgFade: true

                       }, function () {
                      jcrop_api = this;
                      jcrop_api.setOptions({ allowSelect: true });
                      jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
                      jcrop_api.animateTo([0, 0, cropWidth, cropHeight]);
                      // Use the API to get the real image size
                      jcrop_api.focus();
                      var bounds = this.getBounds();
                      boundx = bounds[0];
                      boundy = bounds[1];
                  });
              }
              else {
                  jcrop_api.destroy();
                  jQuery("#ImgResizePreview").css("visibility", "visible");
              }
          }

          var boundx, boundy;
          function SelectCropArea(c) {
              // Grab some information about the preview pane            
              var preview = jQuery('#preview-pane');
              var pcnt = jQuery('#preview-pane .preview-container');
              var pimg = jQuery('#preview-pane .preview-container img');
              var xsize = pcnt.width(),
              ysize = pcnt.height();
              jQuery('#XAxis').val(parseInt(c.x));
              jQuery('#YAxis').val(parseInt(c.y));
              jQuery('#Width').val(parseInt(c.w));
              jQuery('#Height').val(parseInt(c.h));
              jQuery('#x1').val(c.x);
              jQuery('#y1').val(c.y);
              jQuery('#x2').val(c.x2);
              jQuery('#y2').val(c.y2);
              jQuery('#w').val(c.w);
              jQuery('#h').val(c.h);
              if (parseInt(c.w) > 0) {
                  var rx = xsize / c.w;
                  var ry = ysize / c.h;
                  pimg.css({
                      width: Math.round(rx * boundx) + 'px',
                      height: Math.round(ry * boundy) + 'px',
                      marginLeft: '-' + Math.round(rx * c.x) + 'px',
                      marginTop: '-' + Math.round(ry * c.y) + 'px'
                  });
              }
          }
          function SaveChanges() {
              ResizeImageUsingByCrop();
              modal.style.display = "none";
              jcrop_api.destroy();
          }

             function SetReSizeValues(paramval) {
              jQuery("#ImgResizePreview").Jcrop({
                  onSelect: SelectCropArea,
                  onChange: SelectCropArea,
                  bgFade: true

              }, function () {
                  jcrop_api = this;
              });
              jcrop_api.destroy();
              jQuery("#ImgResizePreview").css("visibility", "visible");
              var AutoSize = "";
              var reqsize = "";
              var orientation = "";
              var ResizeWidth = 0;
              var ResizeHeight = 0;
              if (paramval == "S") {
                  AutoSize = jQuery("#ddlSizeResize").val();
                  reqsize = jQuery("#ddlSizeResize").val().split('x');
                  ResizeWidth = parseInt(reqsize[0]);
                  ResizeHeight = parseInt(reqsize[1]);
              }
              jQuery("#txtCustomHeight").val(ResizeHeight);
              jQuery("#txtCustomWidth").val(ResizeWidth);
              if (ResizeWidth == "1900") {
                  jQuery("#divResizePreview").css("width", "316px");
                  jQuery("#divResizePreview").css("height", "116px");
                  
              }
              else {
                  jQuery("#divResizePreview").css("width", "165px");
                  jQuery("#divResizePreview").css("height", "96px");
              }
              setTimeout(function () { PreviewResizedImage(); }, 200);
              setTimeout(function () {
                  if (jQuery("#chkFinalCrop").prop("checked") == true) {
                      Set_Cropper(1);
                  }
              }, 400);
          }

          //Crop Image With Resize Image
          function CropImageWithResize() {
              var sliderposition = "";
              if (jQuery("#ddlSizeResize").val().toString().trim().indexOf("1900") > -1) {
                  sliderposition = "fullwidth";
              }
              else {
                  sliderposition = "leftright";
              }
              jQuery.ajax({
                  url: "../WebService/DocCMSApi.svc/CropImageWithResizeSlider",
                  contentType: 'application/json; charset=utf-8',
                  data: { 'filepath': jQuery("#ImgResizeImage").attr("src"), 'imageheight': jQuery("#txtCustomHeight").val(), 'imagewidth': jQuery("#txtCustomWidth").val(), 'xaxis': document.getElementById('XAxis').value, 'yaxis': document.getElementById('YAxis').value, 'cropimagewidth': document.getElementById('Width').value, 'cropImageheight': document.getElementById('Height').value, 'blogid': jQuery('#hndBlogId').val(), 'userid': jQuery('#hdnCurUserID').val(), 'sliderposition': sliderposition, "sliderid": jQuery("#hdnSliderId").val(), "slideritemid": jQuery("#hdnSliderItemId").val() },
                  dataType: "json",
                  async: false,
                  success: function (data) {
                      if (data.filename == null) {
                          alert("Please Crop Image");
                      }
                      else {
                          location.reload();
                      }
                  },
                  error: function (result) {
                      alert("No Match");
                  }
              });
          }

          function ResizeImage() {
              jQuery.ajax({
                  url: "../WebService/DocCMSApi.svc/ResizeImage",
                  contentType: 'application/json; charset=utf-8',
                  data: { 'filepath': jQuery("#ImgResizeImage").attr("src"), 'ratiostatus': document.getElementById('hdnResizeAsepctRatio').value, 'imageheight': document.getElementById('txtCustomHeight').value, 'imagewidth': document.getElementById('txtCustomWidth').value, "pageid": jQuery("#hdnCurPageID").val(), "cmsid": jQuery("#hdnCurCMSID").val() },
                  dataType: "json",
                  success: function (data) {
                      if (data != null && data != undefined && data != "") {
                          location.reload();                          
                      }
                      else {
                          //Start Save Image Preview//
                          jQuery("#divFinalPreview").hide();
                          jQuery("#divCropFinalImage").hide();
                          jQuery("#imgGetHeightWidth").attr("src", "");
                          jQuery("#dZUpload").show();
                      }
                  },
                  error: function (result) {
                      alert("No Match");
                  }
              });
          }


          // Custome Image Create
          function CustomeImage() {
              var ImageNameArray = jQuery("#ImgResizeImage").attr("src").split('/');
              var ImageName = ImageNameArray[ImageNameArray.length - 1];
              var SliderType = jQuery("#ddlSizeCustomImage").val();
              if (SliderType.toString().indexOf("1900") > -1) {
                  SliderType = "FullWidth";
              }
              else {
                  SliderType = "Left";
              }
              var Color = jQuery('#txtFontColor').val();
              var hex = parseInt(Color.substring(1), 16);
              var r = (hex & 0xff0000) >> 16;
              var g = (hex & 0x00ff00) >> 8;
              var b = hex & 0x0000ff;
              jQuery.ajax({
                  type: "GET",
                  url: "../WebService/DocCMSApi.svc/Custome_Image_Create_Slider",
                  data: { 'filepath': jQuery("#ImgResizeImage").attr("src"), 'R': r, 'G': g, 'B': b, 'sliderid': jQuery("#hdnCurPageID").val(), 'slideritemid': jQuery("#hdnCurCMSID").val(), 'slidertype': SliderType },
                  dataType: "json",
                  async: false,
                  success: function (data) {
                      if (data.filename != null && data.filename != undefined) {
                          location.reload();
                           }
                      else {
                          jQuery("#ImgCustomeImagePreview").attr("src", "");
                          jQuery("#ImgCustomeImagePreview").hide();
                          jQuery("#imgFinalPreview").attr("src", "");
                          jQuery("#EditImageOnModelPopup").attr("src", "");
                          jQuery("#divFinalPreview").hide();
                          jQuery("#divCropFinalImage").hide();
                          jQuery("#dZUpload").show();
                          modalResize.style.display = "block";
                          jQuery("#imgGetHeightWidth").attr("src", "");
                          jQuery("#btnPostImage").prop("disabled", true);
                      }
                  },
                  error: function (result) {
                      alert("No Match");
                  }
              });
          }
          //-------image------

          function ColorChange(Textbox) {
              var colorName = jQuery("#" + Textbox.id).val();
              var Image = jQuery("#ImgResizeImage").attr("src");
              jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
              jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
          }

          function ManageAspectRatio(ratioparam, e) {
              var key;
              if (window.event) {
                  key = window.event.keyCode;     //IE
              }
              else {
                  key = e.which;      //firefox              
              }
              if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
                  if (jQuery("#chkAspectRatioResize").is(':checked')) {
                      if (ratioparam == "w") {
                          var width = jQuery("#txtCustomWidth").val();
                          if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                              jQuery("#txtCustomWidth").css("color", "Red");
                              jQuery("#txtCustomHeight").val("0");
                          }
                          else {
                              var height = parseFloat(jQuery("#txtCustomWidth").val()) / 1.333;
                              jQuery("#txtCustomHeight").val(parseInt(height));
                          }
                      }
                      else {
                          var height = jQuery("#txtCustomHeight").val();
                          if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                              jQuery("#txtCustomHeight").css("color", "Red");
                              jQuery("#txtCustomWidth").val("0");
                          }
                          else {
                              var width = parseFloat(jQuery("#txtCustomHeight").val()) * 1.333;
                              jQuery("#txtCustomWidth").val(parseInt(width));
                          }
                      }
                  }
                  else {
                      jQuery("#ImgResizePreview").Jcrop({
                          onSelect: SelectCropArea,
                          bgFade: true

                      }, function () {
                          jcrop_api = this;
                      });
                      jcrop_api.destroy();
                      PreviewResizedImage();
                      jQuery("#ImgResizePreview").css("visibility", "visible");
                  }
              }
              else {
                  return false;
              }
          }
          //Ready Function Close

          function SetTextImageValues(cntrl) {
              jQuery("#ImgResizePreview").Jcrop({
                  onSelect: SelectCropArea,
                  onChange: SelectCropArea,
                  bgFade: true

              }, function () {
                  jcrop_api = this;
              });
              jcrop_api.destroy();
              jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
              jQuery("#ImgResizePreview").css('visibility', 'visible');
              switch (cntrl) {
                  case "Left Picture":
                      jQuery("#txtCustomWidth").val("373");
                      jQuery("#txtCustomHeight").val("280");
                      CheckResizeValues();
                      break;
                  case "Center Picture":
                      jQuery("#txtCustomWidth").val("373");
                      jQuery("#txtCustomHeight").val("280");
                      CheckResizeValues();
                      break;
                  case "Right Picture":
                      jQuery("#txtCustomWidth").val("373");
                      jQuery("#txtCustomHeight").val("280");
                      CheckResizeValues();
                      break;
                  case "Full Picture":
                      jQuery("#txtCustomWidth").val("1119");
                      jQuery("#txtCustomHeight").val("840");
                      CheckResizeValues();
                      break;
                  default:
                      jQuery("#txtCustomWidth").val("1800");
                      jQuery("#txtCustomHeight").val("1200");
                      CheckResizeValues();
                      break;
              }
          }

          function EditImage(ctrl) {
              var ctrlArray = ctrl.split('_');
              OpenModelPopupCropAndResize(ctrlArray[ctrlArray.length - 1]);
              return false;
          }
          //Open Model Popup Crop and Resize
          function OpenModelPopupCropAndResize(prefix) {
              var FileType = jQuery("#ddlSliderPosition").val();
              var FilePath = jQuery("#imgLogo").attr("src");
              if (FilePath.toString().toLowerCase().indexOf(FileType.toString().toLowerCase()) > -1) {
                  // Do nothing
              }
              else {
                  FilePath = FilePath.toString().toLowerCase().replace("org", FileType.toString().toLowerCase());
                  FilePath = FilePath.toString().toLowerCase().replace("left", FileType.toString().toLowerCase());
                  FilePath = FilePath.toString().toLowerCase().replace("fullwidth", FileType.toString().toLowerCase());
              }

              var modalResize = document.getElementById('divModelResize');
              modalResize.style.display = "block";
              jQuery.ajax({
                  url: "../WebService/DocCMSApi.svc/ProcessFiles_Slider",
                  contentType: 'application/json; charset=utf-8',
                  data: { "filepath": FilePath, "sliderid": jQuery("#hdnCurPageID").val(), "slideritemid": jQuery("#hdnCurCMSID").val() },
                  dataType: "json",
                  success: ajaxSucceeded,
                  error: ajaxFailed
              });
              function ajaxSucceeded(data) {
                  if (data != null) {
                      setTimeout(function () { ResizeImagePopup(data); }, 200);
                  }
              }
              function ajaxFailed() {
                  alert("There is an error during operation.");
              }
          }

          function ResizeImagePopup(fileName) {
              var jcrop_api;
              jQuery("#ImgResizePreview").attr("src", fileName);
              jQuery("#ImgResizeImage").attr("src", fileName);
              jQuery("#ImgCustomeImagePreviewRunning").attr("src", fileName);
              setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
              CheckResizeValues();
              return false;
          }

          function Get_Actual_Uploaded_Image_Size() {
              var myImg = document.querySelector("#ImgResizeImage");
              var realWidth = myImg.naturalWidth;
              var realHeight = myImg.naturalHeight;
              jQuery("#hdnActualImageWidth").val(realWidth);
              jQuery("#hdnActualImageHeight").val(realHeight);
          }

          var boundx, boundy;
          function SelectCropArea(c) {
              // Grab some information about the preview pane            
              var preview = jQuery('#preview-pane');
              var pcnt = jQuery('#preview-pane .preview-container');
              var pimg = jQuery('#preview-pane .preview-container img');
              var xsize = pcnt.width(),
              ysize = pcnt.height();
              jQuery('#XAxis').val(parseInt(c.x));
              jQuery('#YAxis').val(parseInt(c.y));
              jQuery('#Width').val(parseInt(c.w));
              jQuery('#Height').val(parseInt(c.h));
              jQuery('#x1').val(c.x);
              jQuery('#y1').val(c.y);
              jQuery('#x2').val(c.x2);
              jQuery('#y2').val(c.y2);
              jQuery('#w').val(c.w);
              jQuery('#h').val(c.h);
              if (parseInt(c.w) > 0) {
                  var rx = xsize / c.w;
                  var ry = ysize / c.h;
                  pimg.css({
                      width: Math.round(rx * boundx) + 'px',
                      height: Math.round(ry * boundy) + 'px',
                      marginLeft: '-' + Math.round(rx * c.x) + 'px',
                      marginTop: '-' + Math.round(ry * c.y) + 'px'
                  });
              }
          }

          function CloseImageResizeModel() {
              jQuery("#ImgResizeImage").attr("src", "");
              var modalResize = document.getElementById('divModelResize');
              modalResize.style.display = "none";
          }