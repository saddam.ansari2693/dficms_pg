﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.IO;

namespace DocCMSMain.Handler
{
    /// <summary>
    /// Summary description for ImageUpload
    /// </summary>
    public class ImageUpload : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            string ImageName = "";
            string ImageExtension = "";
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                        ImageExtension = Path.GetExtension(fname);
                        ImageName = Guid.NewGuid().ToString();
                        ImageName = ImageName + ImageExtension;
                    }
                    else
                    {
                        fname = file.FileName;
                        ImageExtension = Path.GetExtension(fname);
                        ImageName = Guid.NewGuid().ToString();
                        ImageName = ImageName + ImageExtension;
                    }
                    string VideoPath = HttpContext.Current.Server.MapPath("~/TempImage/");
                    if (!File.Exists(HttpContext.Current.Server.MapPath("~/TempImage/")))
                    {
                        Directory.CreateDirectory(VideoPath);
                    }
                    fname = Path.Combine(context.Server.MapPath("~/TempImage/"), ImageName);
                    file.SaveAs(fname);
                }
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(ImageName);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}