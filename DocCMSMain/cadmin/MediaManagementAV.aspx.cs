﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Text;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class MediaManagementAV : System.Web.UI.Page
    {
        public Int32 id { get; set; }
        public string Userid { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["linkname"] = "licontent";
                Session["Ulname"] = "ulcontent";
                if (Request.QueryString["tid"] != null)
                {
                    labtid.Text = Convert.ToInt32(Request.QueryString["tid"]).ToString();
                    set_page_headings();
                }
                if (Request.QueryString["id"] != null)
                {
                    id = Convert.ToInt32(Request.QueryString["id"]);
                    Set_Data();
                    btnEdit.Visible = true;
                    btnsubmit.Visible = false;
                }
                else
                {
                    //==  DO NOTHING ===//
                    btnEdit.Visible = false;
                    btnsubmit.Visible = true;
                }
                Set_Roles();
            }
        }
        // Set page Headings Based on Cases
        private void set_page_headings()
        {
            StringBuilder query = new StringBuilder();
            Int32 tid = Convert.ToInt32(labtid.Text.ToString());
            if (tid > 0)
            {
                switch (tid)
                {
                    case 1:
                        h5heading1.InnerHtml = "Media Image Files";
                        h1heading1.InnerHtml = "Manage Media Image Files";
                        labheading1.InnerText = "Media Image Files";
                        labheading2.InnerText = "Manage Media Image Files";
                        h4heading1.InnerHtml = "Media Image Details";
                        lblPageName.Text = "image";
                        divyoutube.Visible = false;
                        pselectfile.Visible = true;

                        break;
                    case 2:
                        h5heading1.InnerHtml = "Video Files ";
                        h1heading1.InnerHtml = "Manage Video Files ";
                        labheading1.InnerText = "Video Files ";
                        labheading2.InnerText = "Manage Video Files ";
                        h4heading1.InnerHtml = "Video File Details";
                        lblPageName.Text = "video";
                        labfileformat.Visible = true;
                        paraImage.Visible = true;
                        pselectfile.Visible = false;
                        divyoutube.Visible = true;
                        PageType.InnerText = "Video Link";
                        break;

                    case 3:

                        h5heading1.InnerHtml = "Audio Files ";
                        h1heading1.InnerHtml = "Manage Audio Files ";
                        labheading1.InnerText = "Audio Files ";
                        labheading2.InnerText = "Manage Audio Files ";
                        h4heading1.InnerHtml = "Audio File Details";
                        lblPageName.Text = "Audio";
                        labfileformat.Visible = true;
                        paraImage.Visible = true;
                        pselectfile.Visible = false;
                        divyoutube.Visible = true;
                        PageType.InnerText = "Audio Link";
                        break;
                    case 4:
                        h5heading1.InnerHtml = "Documents Files ";
                        h1heading1.InnerHtml = "Manage Documents Files ";
                        labheading1.InnerText = "Documents Files ";
                        labheading2.InnerText = "Manage Documents Files ";
                        h4heading1.InnerHtml = "Document File Details";
                        lblPageName.Text = "doc";
                        divyoutube.Visible = false;
                        pselectfile.Visible = true;
                        break;
                }
            }
        }

        // Set Data Function On existing Page
        private void Set_Data()
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
                if (id > 0)
                {

                    DataTable Dt = ServicesFactory.DocCMSServices.Fetch_Media_Files_By_id(id.ToString());
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        labtid.Text = Convert.ToInt32(Dt.Rows[0]["FileCategoryID"]).ToString();
                        string FileandImagename = Convert.ToString(Dt.Rows[0]["Filename"]);
                        if (FileandImagename.Contains('~'))
                        {
                            string[] Names = FileandImagename.Split('~');
                            if (Names.Length > 0)
                            {
                                labfilename.Text = Names[0].ToString();
                                Labimagename.Text = Names[1].ToString();
                                if (!string.IsNullOrEmpty(labfilename.Text.Trim()))
                                {
                                    ppreviousfile.Visible = true;
                                }
                                if (!string.IsNullOrEmpty(Labimagename.Text.Trim()))
                                {
                                    parepreviousImage.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            labfilename.Text = Convert.ToString(Dt.Rows[0]["Filename"]);
                            if (!string.IsNullOrEmpty(labfilename.Text.Trim()))
                            {
                                ppreviousfile.Visible = true;
                            }
                            parepreviousImage.Visible = false;
                        }
                        txtalternatetext.Text = Convert.ToString(Dt.Rows[0]["AlternateText"]);
                        txtdescription.Text = Convert.ToString(Dt.Rows[0]["Description"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(Dt.Rows[0]["VideoLink"])))
                        {
                            txtvideolink.Text = Convert.ToString(Dt.Rows[0]["VideoLink"]);
                        }
                    }
                }
            }
        }

        protected void btnsubmit_click(Object sender, EventArgs e)
        {

            try
            {
                String ext = "";
                if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                {
                    ext = System.IO.Path.GetExtension(FileUploadControl.PostedFile.FileName);
                    if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only document file are allowed');</script>");
                        return;
                    }
                    else
                    {
                    }
                    if (!checkfileExsist(FileUploadControl.PostedFile.FileName.ToString(), labtid.Text))
                    {
                        //=== do nothing 
                    }
                    else
                    {
                        string script = "alert('File Already Exsist !Please try again')";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Message", script, true);
                        lblText.Text = " File Already Exsist ! Please try again";
                        return;
                    }
                }
                if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                {
                    ext = System.IO.Path.GetExtension(ImageUploadControl.PostedFile.FileName);

                    if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                    {
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only image file are allowed');</script>");
                        return;
                    }
                    if (!checkfileExsist(ImageUploadControl.PostedFile.FileName.ToString(), labtid.Text))
                    {
                        //=== do nothing 
                    }
                    else
                    {
                        string script = "alert('Image File Already Exsist !Please try again')";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Message", script, true);
                        lblText.Text = "Image File Already Exsist ! Please try again";
                        return;
                    }
                }
                fileUpload(1);
                Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
            }
            catch
            {
                lblText.Text = " Error in File Insertion !Please try again";
                return;
            }
        }
        protected void btnreset_click(Object sender, EventArgs e)
        {
            reset_form();
        }
        protected void reset_form()
        {
            txtdescription.Text = "";
            txtalternatetext.Text = "";
            ppreviousfile.Visible = false;
            parepreviousImage.Visible = false;
            txtvideolink.Text = "";
        }
        protected void btnCancel_click(Object sender, EventArgs e)
        {
            Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
        }
        protected void btnEdit_click(Object sender, EventArgs e)
        {
            String ext = "";
            try
            {
                if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                {
                    ext = System.IO.Path.GetExtension(FileUploadControl.PostedFile.FileName);
                    if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only document file are allowed');</script>");
                        return;
                    }
                    else
                    {
                    }
                    if (!checkfileExsist_Edit(FileUploadControl.PostedFile.FileName.ToString(), Convert.ToInt32(Request.QueryString["id"]), Convert.ToInt32(labtid.Text)))
                    {
                        //=== do nothing 
                    }
                    else
                    {
                        string script = "alert( File Already Exsist !Please try again')";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Message", script, true);
                        lblText.Text = "'File Already Exsist ! Please try again";
                        return;
                    }
                }
                if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                {
                    ext = System.IO.Path.GetExtension(ImageUploadControl.PostedFile.FileName);
                    if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                    {

                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only image file are allowed');</script>");
                        return;
                    }
                    if (!checkfileExsist_Edit(ImageUploadControl.PostedFile.FileName.ToString(), Convert.ToInt32(Request.QueryString["id"]), Convert.ToInt32(labtid.Text)))
                    {
                        //=== do nothing 
                    }
                    else
                    {
                        string script = "alert('Image File Already Exsist !Please try again')";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Message", script, true);
                        lblText.Text = "Image File Already Exsist ! Please try again";
                        return;
                    }
                }
                fileUpload(2);
                Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
            }
            catch
            {
                lblText.Text = " Error in File Updation !Please try again";
                return;
            }
        }

        public void fileUpload(int Mode)
        {
            string fname = "";
            string fimagename = "";
            try
            {

                //========= FOR THE IMAGE UPLOAD ====================================
                if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                {
                    fname = ImageUploadControl.PostedFile.FileName;
                    string serverpath = "";
                    if (Convert.ToInt32(Request.QueryString["tid"]) == 3)
                    {
                        serverpath = Server.MapPath("../UploadedFiles/audio");
                    }
                    else if (Convert.ToInt32(Request.QueryString["tid"]) == 2)
                    {
                        serverpath = Server.MapPath("../UploadedFiles/video");
                    }
                    DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                    bool isDirCreated = directoryInfo.Exists;
                    if (!isDirCreated)
                    {
                        directoryInfo.Create();
                    }
                    ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fname);
                }
                //==============================================================================
                if (!string.IsNullOrEmpty(labfilename.Text))
                {
                    fname = labfilename.Text;
                }
                Clsfileuploader fileUploader = new Clsfileuploader();
                if (string.IsNullOrEmpty(fname))
                {
                    fname = " ";
                }
                if (!string.IsNullOrEmpty(fimagename))
                {
                    fileUploader.filename = fname + "~" + fimagename;
                }
                else
                {
                    fileUploader.filename = fname;
                }
                fileUploader.creationdate = DateTime.Now.ToString();
                fileUploader.filepath = @"../UploadedFiles/" + lblPageName.Text + @"/";

                fileUploader.filecategoryid = labtid.Text.ToString();
                fileUploader.modificationdate = DateTime.Now.ToString();
                if (txtalternatetext.Text.Length > 75)
                {
                    fileUploader.alternatetext = Convert.ToString(txtalternatetext.Text.Substring(0, 75));
                }
                else
                {
                    fileUploader.alternatetext = Convert.ToString(txtalternatetext.Text);
                }
                if (txtdescription.Text.Length > 75)
                {
                    fileUploader.description = Convert.ToString(txtdescription.Text.Substring(0, 75));
                }
                else
                {
                    fileUploader.description = Convert.ToString(txtdescription.Text);
                }

                fileUploader.videolink = Convert.ToString(txtvideolink.Text);
                //==== Memeber ship user =====//                    
                if (Session["UserID"] != null)
                {
                    Userid = Convert.ToString(Session["UserID"]);
                }
                fileUploader.uploadedby = Userid.ToString();
                if (Mode == 1)
                {
                    ServicesFactory.DocCMSServices.Insert_MediaFiles(fileUploader);
                }
                if (Mode == 2)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        id = Convert.ToInt32(Request.QueryString["id"]);
                    }
                    fileUploader.id = id.ToString();
                    ServicesFactory.DocCMSServices.Update_MediaFiles(fileUploader);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }


        private void Set_Roles()
        {
          
        }

        private bool checkfileExsist(string filename, string ID)
        {
            bool retval = false;
            DataTable dt = ServicesFactory.DocCMSServices.Fetch_Media_Files_By_name(filename, Convert.ToInt32(ID));
            if (dt != null && dt.Rows.Count > 0)
            {
                retval = true;
            }
            else
            {
                retval = false;
            }
            return retval;
        }

        private bool checkfileExsist_Edit(string filename, Int32 id, Int32 FileCategoryID)
        {
            bool retval = false;
            DataTable dt = ServicesFactory.DocCMSServices.Fetch_Media_Files_By_name_and_id(filename, id, FileCategoryID);
            if (dt != null && dt.Rows.Count > 0)
            {
                retval = true;
            }
            else
            {
                retval = false;
            }
            return retval;
        }
    }//==== class ends here 
}