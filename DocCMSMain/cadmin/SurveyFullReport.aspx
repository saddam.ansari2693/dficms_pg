﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyFullReport.aspx.cs" Inherits="DocCMSMain.cadmin.SurveyFullReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DOCFOCUS SURVEY SUMMARY REPORT</title>
    <!-- Bootstrap -->
    <link href="../js/Survey/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/Survey/css/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function PrintPage() {
            window.print();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="wrapper" id="MainDiv" runat="server" clientidmode="Static">
        <h2>
            Survey Results for Business Groups Survey</h2>
        <div class="report-box pull-left">
            <label>
                <strong>SurveyName:</strong>
            </label>
            <asp:Literal ID="LitSurveyName" runat="server"></asp:Literal>
            <br />
            <label>
                <strong>Survey Description:</strong>
            </label>
            <asp:Literal ID="LitSurveyDescription" runat="server"></asp:Literal>
            <div class="pull-right" style="margin-top: -30px;">
                <asp:LinkButton ID="LinkButton1" runat="server" Text="Back" CssClass="btn btn-default btn-sm"
                    OnClick="lnkback_Click"></asp:LinkButton>
                <asp:Button ID="btnExportReport" runat="server" ClientIDMode="Static" Text="Export To Excel"
                    CssClass="btn btn-default btn-sm" OnClick="btnExportReport_Click" />
                <input type="button" class="btn btn-default btn-sm" value="Print Report" onclick="PrintPage();" />
            </div>
        </div>
        <h2>
            Detailed Survey Response</h2>
        <div class="space-10">
        </div>
        <div>
            <hr />
        </div>
        <div id="content-area">
            <asp:Repeater runat="server" ID="RepeaterQuestion" OnItemDataBound="RepeaterQuestion_ItemDataBound1">
                <ItemTemplate>
                    <div id='Div_<%# Eval("SurveyQuestionID") %>' align="left" style="border-bottom: 2px solid;
                        border-bottom-color: #b6b6b6; margin-bottom: 15px; margin-top: 15px;">
                        <div id="divQuestionText" style="font-size: 14px; color: #444444; font-weight: bold;
                            text-align: left;" runat="server">
                            <label style="font-size: 16px; font-weight: bold; color: Black;">
                                <%# Container.ItemIndex +1 %>.
                                <%# Eval("Question")%></label>
                        </div>
                        <asp:Label ID="lblQuestionID" runat="server" Text='<%# Eval("SurveyQuestionID")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblSurveyID" runat="server" Text='<%# Eval("SurveyID") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFieldType" runat="server" Text='<%# Eval("AnswerType") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblQuestionType" runat="server" Text='<%# Eval("AnswerType") %>'
                            Visible="false"></asp:Label>
                        <asp:TextBox ID="txtAnswerText" runat="server" Style="font-size: 12px; margin-top: 15px;
                            color: #444444; line-height: 1em; margin-left: 55px; max-height: 130px; max-width: 550px;
                            min-height: 40px; width: 550px;"  Enabled="false"></asp:TextBox>
                        <asp:Repeater runat="server" ID="RepeaterRadio">
                            <ItemTemplate>
                                <div id='DivAns_<%# Container.ItemIndex %>' class="space-20">
                                    <div class="checkbox" style="margin-left: 40px;">
                                        <label style="color: #444444; text-align: left; margin-left: 10px;">
                                            <input type="radio" id='Rpt_<%# Eval("Dorder")%>~<%# Eval("Optionval")%>' name='radioqst_<%# Eval("QuestionID")%>'
                                                disabled="disabled" <%# Eval("Checked") %> />
                                            <label for='Rpt_<%# Eval("Dorder")%>~<%# Eval("Optionval")%>' id="labqst">
                                                <%# Eval("OptionVal")%>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:Repeater runat="server" ID="RepeaterCheckbox">
                            <ItemTemplate>
                                <div id='DivAns_<%# Container.ItemIndex %>' class="space-20">
                                    <div class="space-15">
                                    </div>
                                    <div class="checkbox" style="margin-left: 70px;">
                                        <label>
                                            <input type="checkbox" id='Chk_<%# Eval("Dorder")%>~<%# Eval("Optionval")%>' onchange="set_ans_val(this);"
                                                disabled="disabled" <%# Eval("Checked") %> />
                                            <label for='Chk_<%# Eval("Dorder")%>~<%# Eval("Optionval")%>' id="labckhbtn">
                                            </label>
                                            <asp:Label ID="lblAnsText" Style="font-size: 12px; color: #444444; margin-left: 40px;
                                                display: block; margin-right: 100px; margin-top: -22px;" runat="server" Text='<%# Eval("OptionVal")%>'></asp:Label>
                                        </label>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div style="width: 100%; height: 20px;">
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/Survey/js/bootstrap.min.js" type="text/javascript"></script>
    </form>
</body>
</html>