﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class MediaManagementDashboard : System.Web.UI.Page
    {
        public Int32 tid { get; set; }
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["linkname"] = "licontent";
                Session["Ulname"] = "ulcontent";
                Bind_Dashboard();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
                
                SetControls();
            }
            
        }
   
        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    Button1.Visible = true;
                    cmdInsert.Visible = true;
                }
                else
                {
                    Button1.Visible = false;
                    cmdInsert.Visible = false;
                }
                foreach (RepeaterItem Item in RepeaterContent.Items)
                {

                    LinkButton linktitle = (LinkButton)Item.FindControl("linktitle");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton LinkEdit = (ImageButton)Item.FindControl("LinkEdit");
                    ImageButton linkdelete = (ImageButton)Item.FindControl("linkdelete");
                    if (lblTitle != null && linktitle != null && LinkEdit != null && linkdelete != null )
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            LinkEdit.Enabled = true;
                            linktitle.Visible = true;
                            lblTitle.Visible = false;
                        }
                        else
                        {
                            LinkEdit.Enabled = false;
                            linktitle.Visible = false;
                            lblTitle.Visible = true;
                        }

                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            linkdelete.Enabled = true;
                        else
                            linkdelete.Enabled = false;
                    }

                }

            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");


            }
        }
        // Bind Dashboard inside RepeaterContent Repeater
        protected void Bind_Dashboard()
        {
            if (Request.QueryString["tid"] != null)
            {
                tid = Convert.ToInt32(Request.QueryString["tid"]);
                string queryval = set_page_headings(tid);
                if (!string.IsNullOrEmpty(queryval))
                {
                    DataTable dt = ServicesFactory.DocCMSServices.Bind_Media_Dashboard(queryval);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        RepeaterContent.DataSource = dt;
                        RepeaterContent.DataBind();
                    }
                }

            }
        }

        // Set page Headings Function
        private string set_page_headings(Int32 tid)
        {
            StringBuilder query = new StringBuilder();
            string retval = "";
            try
            {
                if (Request.QueryString["tid"] != null)
            {
                tid = Convert.ToInt32(Request.QueryString["tid"]);
                if (tid > 0)
                {
                    switch (tid)
                    {
                        case 1:
                            h5heading1.InnerHtml = "Media Image Files";
                            h1heading1.InnerHtml = "Manage Media Image Files";
                            h4heading1.InnerHtml = "Media Image Details";
                            labheading1.InnerText = "Media Image Files";
                            labheading2.InnerText = "Manage Media Image Files";
                            cmdInsert.Text = "Add Image File";
                            query = new StringBuilder();
                            query.Append("SELECT     fileupload.id, fileupload.filename, fileupload.filepath, fileupload.modificationdate, fileupload.alternatetext,");
                            query.Append("fileupload.description,  fileuploadcategory.categoryname,fileupload.filename OrignalFileName, userprofiles.fname || ' ' || userprofiles.lname as Uploadedby ");
                            query.Append(" FROM         fileupload LEFT OUTER JOIN ");
                            query.Append("  fileuploadcategory ON fileupload.filecategoryid = fileuploadcategory.categoryid LEFT OUTER JOIN ");
                            query.Append("   userprofiles ON fileupload.uploadedby= userprofiles.userid::varchar where fileupload.filecategoryid=1");
                            query.Append("   and fileupload.uploadedby is not null and fileupload.uploadedby<>''");
                            break;
                        case 2:
                               h5heading1.InnerHtml = "Video Files ";
                               h1heading1.InnerHtml = "Manage Video Files ";
                               h4heading1.InnerHtml = "Video File Details";
                               labheading1.InnerText = "Video Files ";
                               labheading2.InnerText = "Manage Video Files ";
                               cmdInsert.Text = "Add Video File";
                            query = new StringBuilder();
                            query.Append("SELECT     fileupload.id, AlternateText || ' ' || Description as Filename , fileupload.filepath, fileupload.modificationdate, fileupload.alternatetext,");
                            query.Append("fileupload.description,  fileuploadcategory.categoryname,fileupload.filename OrignalFileName, userprofiles.fname || ' ' || userprofiles.lname as Uploadedby ");
                            query.Append(" FROM         fileupload LEFT OUTER JOIN ");
                            query.Append("  fileuploadcategory ON fileupload.filecategoryid = fileuploadcategory.categoryid LEFT OUTER JOIN ");
                            query.Append("   userprofiles ON fileupload.uploadedby= userprofiles.userid::varchar where fileupload.filecategoryid=2");
                            query.Append("   and fileupload.uploadedby is not null and fileupload.uploadedby<>''");
                            break;
                        case 3:
                            h5heading1.InnerHtml = "Audio Files ";
                            h1heading1.InnerHtml = "Manage Audio Files ";
                            h4heading1.InnerHtml = "Audio File Details";
                            labheading1.InnerText = "Audio Files ";
                            labheading2.InnerText = "Manage Audio Files ";
                            cmdInsert.Text = "Add Audio File";
                            query = new StringBuilder();
                            query.Append("SELECT     fileupload.id, AlternateText || ' ' || Description as Filename , fileupload.filepath, fileupload.modificationdate, fileupload.alternatetext,");
                            query.Append("fileupload.description,  fileuploadcategory.categoryname, fileupload.filename OrignalFileName,userprofiles.fname || ' ' || userprofiles.lname as Uploadedby ");
                            query.Append(" FROM         fileupload LEFT OUTER JOIN ");
                            query.Append("  fileuploadcategory ON fileupload.filecategoryid = fileuploadcategory.categoryid LEFT OUTER JOIN ");
                            query.Append("   userprofiles ON fileupload.uploadedby= userprofiles.userid::varchar where fileupload.filecategoryid=3");
                            query.Append("   and fileupload.uploadedby is not null and fileupload.uploadedby<>''");
                            break;
                        case 4:
                            h5heading1.InnerHtml = "Documents Files ";
                            h1heading1.InnerHtml = "Manage Documents Files ";
                            h4heading1.InnerHtml = "Document File Details";
                            labheading1.InnerText = "Documents Files ";
                            labheading2.InnerText = "Manage Documents Files ";
                            cmdInsert.Text = "Add Documents File";
                            query = new StringBuilder();
                            query.Append("SELECT     fileupload.id, fileupload.filename, fileupload.filepath, fileupload.modificationdate, fileupload.alternatetext,");
                            query.Append("fileupload.description,  fileuploadcategory.categoryname, fileupload.filename OrignalFileName,userprofiles.fname || ' ' || userprofiles.lname as Uploadedby ");
                            query.Append(" FROM         fileupload LEFT OUTER JOIN ");
                            query.Append("  fileuploadcategory ON fileupload.filecategoryid = fileuploadcategory.categoryid LEFT OUTER JOIN ");
                            query.Append("   userprofiles ON fileupload.uploadedby= userprofiles.userid::varchar where fileupload.filecategoryid=4");
                            query.Append("   and fileupload.uploadedby is not null and fileupload.uploadedby<>''");
                            break;
                    }
                    retval = query.ToString();
                }
            }
            return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RepeaterContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "EditLink")
            {
                if (Request.QueryString["tid"] != null)
                {
                    tid = Convert.ToInt32(Request.QueryString["tid"]);
                    if (tid > 0)
                    {
                        if (tid == 2 || tid == 3)
                        {
                            Response.Redirect("../cadmin/MediaManagementAV.aspx?Id=" + e.CommandArgument.ToString() + "&tid=" + Convert.ToString(tid.ToString()));
                        }
                        else
                        {
                            Response.Redirect("../cadmin/MediaManagementPage.aspx?Id=" + e.CommandArgument.ToString() + "&tid=" + Convert.ToString(tid.ToString()));
                        }

                    }
                }
            }
            if (e.CommandName == "Delete")
            {
                if (Set_Roles())
                {
                    Clsfileuploader objcontent = new Clsfileuploader();
                    objcontent.id = Convert.ToString(e.CommandArgument);
                    LinkButton linktitle = (LinkButton)e.Item.FindControl("linktitle");
                    string Uploadpath = "";
                    ServicesFactory.DocCMSServices.Delete_MediaFile(objcontent);
                        if (Request.QueryString["tid"] != null)
                        {
                            tid = Convert.ToInt32(Request.QueryString["tid"]);
                        }
                        if (tid == 1)
                        {
                            if (linktitle != null)
                            {
                                Uploadpath = "../UploadedFiles/image/" + linktitle.Text.Trim();
                            }
                            
                        }
                        else if (tid == 2 || tid == 3)
                        {
                         
                        }
                        else if (tid == 4)
                        {
                            Uploadpath = "../UploadedFiles/doc/" + linktitle.Text.Trim();
                        }
                        if (File.Exists(Server.MapPath(Uploadpath)))
                        {
                            File.Delete(Server.MapPath(Uploadpath));
                        }
                        set_page_headings(tid);
                        Bind_Dashboard();
                        Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + Request.QueryString["tid"]);
                      
                }
            }
        }

        protected void cmdInsert_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["tid"] != null)
            {
                tid = Convert.ToInt32(Request.QueryString["tid"]);
                if (tid > 0)
                {
                    if (tid == 2 || tid == 3)
                    {
                        Response.Redirect("../cadmin/MediaManagementAV.aspx?tid=" + tid.ToString());

                    }
                    else
                    {
                        Response.Redirect("../cadmin/MediaManagementPage.aspx?tid=" + tid.ToString());
                    }
                }
            }
        }
        // Set Roles Function
        private bool Set_Roles()
        {
            bool Retval = true;
            return Retval;
        }

    }//=== class ends here ===
}