﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class CompanySettingPage : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Company();
            }
        }

        // Binding Company Detail with Controls when Company Page Load
        protected void Bind_Company()
        {
            try
            {
                // Check Company Page for Already Existing Company id
                if (Request.QueryString["CID"] != null)
                {
                    hdnCurPageID.Value = Convert.ToString((Convert.ToInt32(Request.QueryString["CID"])));
                    Cls_company objCompany = ServicesFactory.DocCMSServices.Fetch_company_Byid(Convert.ToInt32(Request.QueryString["CID"]));
                    if (objCompany != null)
                    {

                        txtCompanyName.Value = objCompany.companyname;
                        txtDescription.Value = objCompany.description;
                        txtAddress.Value = objCompany.address;
                        txtCity.Value = objCompany.city;
                        txtState.Value = objCompany.state;
                        string serverpath = "";
                        if (!string.IsNullOrEmpty(objCompany.companylogo))
                        {
                            lblFileUploader.Text = objCompany.companylogo;
                            serverpath = @"../UploadedFiles/CompanyLogo/" + objCompany.companylogo;
                            imgLogo.Src = serverpath;
                        }
                        else
                        {
                            lblFileUploader.Text = "";
                            serverpath = @"../UploadedFiles/No_image_available.png";
                            imgLogo.Src = serverpath;
                        }
                        ChkActiveStatus.Checked = objCompany.isactive;
                        btnupdate.Visible = true;
                    }
                }
                else
                {
                    txtCompanyName.Value = "";
                    txtDescription.Value = "";
                    txtAddress.Value = "";
                    txtCity.Value = "";
                    txtState.Value = "";
                    btnsubmit.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Check the file format consist only Image File
        protected void Check_FileFormat()
        {
            if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
            {
                string[] fvals = FileUploadControl.PostedFile.FileName.ToString().Split('.');
                if (fvals.Length > 0)
                {
                    switch (Convert.ToString(fvals[1]).ToUpper())
                    {
                        case "PNG":
                        case "JPEG":
                        case "JPG":
                        case "TIF":
                        case "GIF":
                        case "DOC":
                            pError.Visible = false;
                            break;

                        default:
                            pError.Visible = true;
                            pError.InnerText = ".mp4, .ogg, .webm, .mpeg, .doc, .xls,.ods & .mp3 file formats are not supported ! Please try again";
                            break;
                    }
                }
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_company clsCompany = new Cls_company();
                clsCompany.companyname= txtCompanyName.Value;
                clsCompany.description = txtDescription.Value;
                clsCompany.address = txtAddress.Value;
                clsCompany.city = txtCity.Value;
                clsCompany.state = txtState.Value;
                clsCompany.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsCompany.isactive = true;
                else
                    clsCompany.isactive = false;
                string fname = "";

                // Check FileUpload contain any file or not
                if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                {
                    Check_FileFormat();
                    fname = FileUploadControl.PostedFile.FileName;
                    string serverpath = Server.MapPath("../UploadedFiles/CompanyLogo/");
                    DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                    bool isDirCreated = directoryInfo.Exists;
                    if (!isDirCreated)
                    {
                        directoryInfo.Create();
                    }
                    FileUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fname);
                    clsCompany.companylogo = fname;
                }
                else
                {
                    clsCompany.companylogo = lblFileUploader.Text;
                }
                 // Check Company name Existing or Not
                 Int32 RetCheck = 0;
                 RetCheck = ServicesFactory.DocCMSServices.Check_Existing_company(txtCompanyName.Value.Trim());
                 if (RetCheck > 0)
                 {
                     ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Company with similar Name Already Exists...');</script>");
                     return;
                   
                 }
                 else
                 {
                     // if No duplicate Company name then New Company will insert
                     Int32 retVal = ServicesFactory.DocCMSServices.Insert_company(clsCompany);
                     if (retVal > 0)
                     {
                         ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SaveMsg();</script>");
                     }
                     else
                     {
                         ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Error occur in Saving');</script>");
                         return;
                     }
                 }
             }
            catch (Exception ex)
            {
                throw ex;
            }
       }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_company clsCompany = new Cls_company();
                clsCompany.companyid = Convert.ToInt32(Request.QueryString["CID"]);
                clsCompany.companyname = txtCompanyName.Value;
                clsCompany.description = txtDescription.Value;
                clsCompany.address = txtAddress.Value;
                clsCompany.city = txtCity.Value;
                clsCompany.state = txtState.Value;
                clsCompany.lastmodifiedby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsCompany.isactive = true;
                else
                    clsCompany.isactive = false;
                string fname = "";

                // Check fileUpload Contain any File Or Not
                if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                {
                    Check_FileFormat();
                    fname = FileUploadControl.PostedFile.FileName;
                    string serverpath = Server.MapPath("../UploadedFiles/CompanyLogo/");
                    DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                    bool isDirCreated = directoryInfo.Exists;
                    if (!isDirCreated)
                    {
                        directoryInfo.Create();
                    }
                    FileUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fname);
                    clsCompany.companylogo = fname;
                }
                else
                {
                    clsCompany.companylogo = lblFileUploader.Text;
                }
                // Update Company Details
                Int32 retVal = ServicesFactory.DocCMSServices.Update_company(clsCompany);
                if (retVal > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>UpdateMsg();</script>");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Error occur in Saving');</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/CompanyDashboard.aspx");
        }
    }
}