﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="MediaManagementAV.aspx.cs" Inherits="DocCMSMain.cadmin.MediaManagementAV" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="../Controls/Footer.ascx" tagname="CadminFooter" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="../css/style.default.css" type="text/css" />
<link rel="stylesheet" href="../css/isotope.css" type="text/css" />
<link rel="stylesheet" href="../css/bootstrap-fileupload.min.css" type="text/css" />
<script type="text/javascript" src="../js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="../js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        //Replaces data-rel attribute to rel.
        //We use data-rel because of w3c validation issue
        jQuery('a[data-rel]').each(function () {
            jQuery(this).attr('rel', jQuery(this).data('rel'));
        });
        jQuery("#medialist a").colorbox();
    });
    jQuery(window).load(function () {
        jQuery('#medialist').isotope({
            itemSelector: 'li',
            layoutMode: 'fitRows'
        });
        // Media Filter
        jQuery('#mediafilter a').click(function () {
            var filter = (jQuery(this).attr('href') != 'all') ? '.' + jQuery(this).attr('href') : '*';
            jQuery('#medialist').isotope({ filter: filter });
            jQuery('#mediafilter li').removeClass('current');
            jQuery(this).parent().addClass('current');
            return false;
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
         <ul class="breadcrumbs">
            <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li><a href="#"><lable id="labheading1" runat="server"></lable></a> <span class="separator"></span></li>
            <li><lable id="labheading2" runat="server"></lable></li>
        </ul>
         <div class="pageheader">
            <form action="" method="post" class="searchbar">
             </form>
            <div class="pageicon"><span class="iconfa-pencil"></span></div>
            <div class="pagetitle">
                <h5 id="h5heading1" runat="server">Heading1</h5>
                <h1 id="h1heading1" runat="server">Heading2</h1>
            </div>
        </div><!--pageheader-->
            <div class="maincontent">
            <div class="maincontentinner">
             <div class="widget">
              <h4 class="widgettitle" id="h4heading1" runat="server">Heading3</h4>
            <div class="widgetcontent">
                <div class="stdform">
                <asp:Label id="labtid" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblPageName" Visible="false" runat="server" />
                <p id="ppreviousfile" runat="server" visible="false">
                 <label style="width:100px;">File Name</label>
                            <span class="field" style="margin-left:130px;"><asp:Label ID="labfilename" runat="server" Text=""></asp:Label>
                            </span>             
                 </p>
                <p id="pselectfile" runat="server" visible="false">
                <label style="width:100px;">Select File</label>
              <span class="field" style="margin-left:130px; width:56.2%;">
             <div class="par" style="margin-left:130px; width:56.2%;">
			                            <div class="fileupload fileupload-new" data-provides="fileupload">
				                        <div class="input-append">
				                        <div class="uneditable-input span8" style=" width:773.8px;">
				                            <i class="iconfa-file fileupload-exists"></i>
				                            <span class="fileupload-preview"></span>
				                        </div>
				                        <span class="btn btn-file"><span class="fileupload-new">Select File</span>
				                        <span class="fileupload-exists">Change</span>
				                        <input type="file" id="FileUploadControl" size="500" runat="server"  /></span>
				                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
				                        </div>
			                            </div>
			                        </div>
                                     </span> 

                </p>
               <p id="parepreviousImage" runat="server" visible="false">
                 <label style="width:100px;">Cover Image</label>
                            <span class="field" style="margin-left:130px; width:60%;">
                                    <asp:Label ID="Labimagename" runat="server" Text=""></asp:Label>
                            </span>             
                </p>
                <p id="paraImage" runat="server" visible="false">
                <label style="width:100px;">Cover Image</label>
                  <span class="field" style="margin-left:130px;">
                   <div class="par" style="margin-left:130px;">
			                            <div class="fileupload fileupload-new" data-provides="fileupload">
				                        <div class="input-append">
				                        <div class="uneditable-input span8" style=" width:758px;">
				                            <i class="iconfa-file fileupload-exists"></i>
				                            <span class="fileupload-preview"></span>
				                        </div>
				                        <span class="btn btn-file"><span class="fileupload-new">Select Image</span>
				                        <span class="fileupload-exists">Change</span>
				                        <input type="file" id="ImageUploadControl" size="500" runat="server"  /></span>
				                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
				                        </div>
			                            </div>
			                        </div>
                                     </span> 

                </p>
                <div id="divyoutube" runat="server" visible="true">
                   <p>
                            <label style="width:100px;" id="PageType" runat="server"></label>
                             <span class="formwrapper" style="margin-left:150px;margin-top: -4px; width:56.2%;">
                           <asp:TextBox ID="txtvideolink" runat="server" CssClass="input-block-level" 
                                placeholder="Enter Link Address"   />
                           <asp:RequiredFieldValidator ID="reqvideolink" runat="server" ControlToValidate="txtvideolink" 
                             style="color: Red; font-size: 24px; font-weight: bold;" ValidationGroup="grpsubmit">*</asp:RequiredFieldValidator>
                                </span>
                        </p>
                </div>
                     <p>
                            <label style="width:100px;">Alternate text</label>
                            <span class="field" style="margin-left:150px;margin-top: -4px; width:56.2%;">
                                <asp:TextBox ID="txtalternatetext" runat="server" CssClass="input-block-level" MaxLength="75" placeholder="Alternate Text"   />                              
                             </span>
                    </p>
                     <p>
                            <label style="width:100px;">Description</label>
                            <span class="field" style="margin-left:150px;margin-top: -4px; width:56.2%;">
                                <asp:TextBox ID="txtdescription" runat="server" CssClass="input-block-level" placeholder="Description"   />                              
                            </span>
                    </p>
                    <p class="stdformbutton">
                      <button class="btn btn-primary" id="btnsubmit" runat="server"  OnServerClick="btnsubmit_click" ValidationGroup="grpsubmit">Save Information</button>
                        <button class="btn btn-primary" id="btnEdit" runat="server"  OnServerClick="btnEdit_click" ValidationGroup="grpsubmit" Visible="False">Update Information</button>
                           <button class="btn btn-primary" id="Button1" runat="server"  OnServerClick="btnCancel_click"  Visible="True">Cancel</button>
                    </p>
                    <p class="stdformbutton">
                    <asp:Label ID="labfileformat" runat="server" Visible="false" Font-Bold="false" Font-Size="14" ForeColor="Red"></asp:Label>
                    </p>
                </div>
            </div><!--widgetcontent-->
            </div><!--widget-->
               <asp:Label ID="lblText" runat="server" />
                <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            </div><!--maincontentinner-->
        </div><!--maincontent-->
</asp:Content>

