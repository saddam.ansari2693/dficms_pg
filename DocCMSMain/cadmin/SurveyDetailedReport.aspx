﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="SurveyDetailedReport.aspx.cs" Inherits="DocCMSMain.cadmin.SurveyDetailedReport1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              <div>
                    Survey Detailed Report Dashboard
                    <asp:Button ID="BtnBack" runat="server" Text="Back" CssClass="btn" Style="float: right;
                        vertical-align: middle; margin-top: -5px; margin-left: 5px;" OnClick="BtnBack_Click" />
                    <asp:Button ID="btnExportAll" runat="server" Text="Export Report" CssClass="btn"
                        Style="float: right; vertical-align: middle; margin-top: -5px;" OnClick="btnExportAll_Click"  />
                </div>
            </h4>
            <div class="survey-name" style="margin-top:5px;font-size:14px;">
                <strong>Survey Name:&nbsp;&nbsp;</strong><asp:Label ID="LblSurveyName" runat="server"></asp:Label></div>
            <div class="survey-description" style="height: 25px; border-bottom: 2px solid; border-bottom-color: #b6b6b6; font-size:14px;">
                <strong>Survey Description:&nbsp;&nbsp;</strong><asp:Label ID="LblSurveyDescription"
                    runat="server"></asp:Label></div>      
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                   <col class="con0" style="align: right;" />
                    <col class="con1" style="align: right;" />
                    <col class="con1" style="align: center; width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Participant IP
                        </th>
                        <th class="head0">
                            Participation Date
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                <asp:Repeater ID="rptr_DetailedReport" runat="server" onitemcommand="rptr_DetailedReport_ItemCommand" >
                <ItemTemplate>
                <tr class="gradeX">
                        <td>
                            <asp:Label ID="lblResponseID" runat="server" Text='<%# Eval("ResponseID") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblIPAddress" runat="server" Text='<%# Eval("IPAddress") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblResponseDate" runat="server" Text='<%# Eval("ResponseDate") %>'></asp:Label>
                        </td>
                        <td class="center">
                            <asp:LinkButton runat="server" ID="lnkViewReport" CssClass="btn btn-primary" CommandName="ViewReport" CommandArgument='<%# Eval("ResponseID") %>'
                                ToolTip="View Detailed Report" Text="View Report" ></asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                <tr class="gradeA">
                        <td>
                            <asp:Label ID="lblResponseID" runat="server" Text='<%# Eval("ResponseID") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblIPAddress" runat="server" Text='<%# Eval("IPAddress") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblResponseDate" runat="server" Text='<%# Eval("ResponseDate") %>'></asp:Label>
                        </td>
                        <td class="center">
                            <asp:LinkButton runat="server" ID="lnkViewReport" CssClass="btn btn-primary" CommandName="ViewReport" CommandArgument='<%# Eval("ResponseID") %>'
                                ToolTip="View Detailed Report" Text="View Report" ></asp:LinkButton>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                </asp:Repeater>
                </tbody>
            </table>
          
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>