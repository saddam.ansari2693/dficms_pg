﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="SliderManagementPage.aspx.cs" Inherits="DocCMSMain.cadmin.SliderManagementPage" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wysiwyg.js"></script>
    <script type="text/javascript" src="../js/ContentSpecialScripts.js"></script>
    <script src="../js/jscolor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            SelectedSectionShow();
            var colpick = $('.demo').each(function () {
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: 'lowercase',
                    opacity: false,
                    change: function (hex, opacity) {
                        if (!hex) return;
                        if (opacity) hex += ', ' + opacity;
                        try {
                            console.log(hex);
                        } catch (e) { }
                        $(this).select();
                    },
                    theme: 'bootstrap'
                });
            });

            var $inlinehex = $('#inlinecolorhex h3 small');
            $('#inlinecolors').minicolors({
                inline: true,
                theme: 'bootstrap',
                change: function (hex) {
                    if (!hex) return;
                    $inlinehex.html(hex);
                }
            });
        });
        function isNumber(evt) {
            var key;
            if (window.evt) {
                key = window.evt.keyCode;     //IE
            }
            else {
                key = evt.which;      //firefox              
            }
            if ((key >= 48 && key <= 57)) {
                return true;
            }
            else if (key == 0 || key == 8 || key == 118 || key == 45) {
                return true;
            }
            else {
                return false;
            }
        }

        function SaveMsg(Alias) {
            alert("SMTP Detail " + Alias + " Suceessfully");
            var url = "";
            url = "../cadmin/SMTPSettings.aspx";
            $(location).attr('href', url);
        }
        function ManageDimensions() {
            if ($("#chkFullWidth").is(":checked")) {
                $("#txtWidth").val("0");
                $("#txtWidth").attr("disabled", true);
            }
            else if ($("#chkFullWidth").is(":not(:checked)")) {
                $("#txtWidth").attr("disabled", false);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="../cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Manage Slider</a> <span class="separator"></span></li>
        <li>Slider Managment Settings</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Slider</h5>
            <h1>
                Slider Managment Settings</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent" id="DivSurveyContent" runat="server">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <asp:HiddenField ID="hdnSliderMasterId" runat="server" />
            <div class="widget">
                <h4 class="widgettitle">
                    Slider Managment Settings</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform" id="DivSMTPPage" runat="server">
                        <div class="par control-group">
                            <label class="control-label" for="txtImageName">
                                Slider Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtSliderName" runat="server" CssClass="input-large" ClientIDMode="Static"
                                    ValidationGroup="grpsubmit" placeholder="Enter Image name "></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtSliderName" runat="server" Style="color: Red; font-size: 24px;
                                    font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="ddlSliderPosition">
                                Slider Position</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSliderPosition" runat="server" AutoPostBack="true" 
                                    onselectedindexchanged="ddlSliderPosition_SelectedIndexChanged">
                                    <asp:ListItem Text="Left" Value="Left"></asp:ListItem>
                                    <asp:ListItem Text="Right" Value="Right"></asp:ListItem>
                                    <asp:ListItem Text="Full Width" Value="Full Width"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>                      
                        <div class="par control-group">
                            <label class="control-label" for="txtheight">
                                Slider Height (In Pixels)</label>
                            <div class="controls">
                            <asp:Label ID="txtHeight" runat="server" ClientIDMode="Static" CssClass="input-large"></asp:Label>                                
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtWidth">
                                Slider Width (In Pixels)</label>
                            <div class="controls">
                            <asp:Label ID="txtWidth" runat="server" ClientIDMode="Static" CssClass="input-large"></asp:Label>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtImageHeight">
                                Image Height (In Pixels)</label>
                            <div class="controls">
                             <asp:Label ID="txtImageHeight" runat="server" ClientIDMode="Static" CssClass="input-large"></asp:Label>                               
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtImagewidth">
                                Image Width (In Pixels)</label>
                            <div class="controls">
                            <asp:Label ID="txtImagewidth" runat="server" ClientIDMode="Static" CssClass="input-large"></asp:Label>                            
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus" style="margin-top: -10px;">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    clientidmode="Static" style="opacity: 1" checked />

                            </div>
                        </div>

                        <div runat="server" id="DivSliderEmptyAreaDetails">
                         <div class="par control-group">                              
                            <div class="controls">
                                Fill Empty Area with buttons or Text content(Only In case of Left/Right Slider)
                                <asp:RadioButton runat="server" ID="rbtButtonContent" onclick="ChangeContentType('rbtButtonContent');" Checked="true" ClientIDMode="Static" />Buttons
                                <asp:RadioButton runat="server" ID="rbtnTextContent"  onclick="ChangeContentType('rbtnTextContent');" ClientIDMode="Static"/>Special Text 
                            </div>
                        </div>
                        <div class="par control-group" id="divSpecialButtons" ClientIDMode="Static" >                              
                          
                <div class="par control-group" id="DivSlider" runat="server">
                            <label class="control-label" for="tblFeature">
                                Special Buttons Details</label>
                            <div class="controls">
                                        <table id="tblSMTP" class="table table-bordered responsive " style="margin-left: 150px; 
                                            width: 90%; max-width:90%;">
                                            <colgroup>
                                                <col class="con1" style="align: center; width: 18%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                            </colgroup>
                                            <thead id="tblSMTPDetailhead" runat="server">
                                                <tr>
                                                    <th class="head1" style="height: 10px;">
                                                        <div style="display: inline; cursor: pointer;" class="AddFeature" id="AddFeature"
                                                            runat="server" onclick="return AddQues();">
                                                        <asp:ImageButton ID="ImgAddSpecialButton" runat="server" ClientIDMode="Static" ImageUrl="../images/plus-icon.png"
                                                                ToolTip="Add Special Button" OnClick="ImgAddSpecialButton_Click" />
                                                        <span style="margin-top:10px;"> Add Special Button</span></div>
                                                        <label id="lblQuestion" runat="server" clientidmode="Static" visible="false" style="text-align: left;">
                                                          S.No</label>
                                                    </th>
                                                    <th class="head1" style="height: 10px;">
                                                      Button Text
                                                    </th>
                                                    <th class="head1" style="height: 10px">
                                                      Navigation URL
                                                    </th>
                                                     <th class="head1" style="height: 10px">
                                                      Button Color
                                                    </th>
                                                    <th class="head1" style="height: 10px">
                                                      Is Active
                                                    </th>
                                                    <th class="head1" style="height: 10px;">
                                                        Operations
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="tblSMTPBody" runat="server">
                                                <asp:Repeater ID="rptrSpecialButton" runat="server" ClientIDMode="Static" OnItemCommand="rptrSpecialButton_ItemCommand"
                                                    OnItemDataBound="rptrSpecialButton_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr class="gradeX">
                                                        <td>
                                                        <%#Container.ItemIndex+1 %>
                                                        </td>
                                                            <td>
                                                                <asp:LinkButton ID="LnkButtonText" CommandName="EditSpecialButton"   CommandArgument='<%# Eval("ID")%>'
                                                                    Text='<%# Eval("ButtonText") %>' runat="server"></asp:LinkButton>
                                                            </td>
                                                            <td style="height: 10px;max-width:120px;word-wrap: break-word;">
                                                                <asp:Label ID="lblNavigationURL" runat="server" Text='<%# Eval("NavigationURL") %>'></asp:Label>
                                                            </td>
                                                              <td>
                                                                <asp:Label ID="lblButtonColor" runat="server" Text='<%# Eval("ButtonColor") %>'></asp:Label>
                                                              </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                            <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                                               CommandArgument='<%# Eval("ID")%>' CommandName="EditSpecialButton" ToolTip="Edit Special Link" />
                                                            <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                                               CommandArgument='<%# Eval("ID")%>' CommandName="DeleteSpecialButton" ToolTip="Delete Special Link" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr class="gradeA">
                                                        <td>
                                                        <%#Container.ItemIndex+1 %>
                                                        </td>
                                                            <td>
                                                                <asp:LinkButton ID="LnkButtonText" CommandName="EditSpecialButton"   CommandArgument='<%# Eval("ID")%>'
                                                                    Text='<%# Eval("ButtonText") %>' runat="server"></asp:LinkButton>
                                                            </td>
                                                            <td style="height: 10px;max-width:120px;word-wrap: break-word;">
                                                                <asp:Label ID="lblNavigationURL" runat="server" Text='<%# Eval("NavigationURL") %>'></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="lblButtonColor" runat="server" Text='<%# Eval("ButtonColor") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                                                   CommandArgument='<%# Eval("ID")%>' CommandName="EditSpecialButton" ToolTip="Edit Special Link" />
                                                               <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                                                   CommandArgument='<%# Eval("ID")%>' CommandName="DeleteSpecialButton" ToolTip="Delete Special Link" />
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                </asp:Repeater>
                                                <tr class="gradeX" id="trAdditionalRow" runat="server" clientidmode="Static" visible="false">
                                                <td>
                                                    <asp:HiddenField ID="hdnSpecialButtonId" runat="server" />
                                                   <asp:TextBox ID="txtSpecialButtonDisplayOrder" placeholder="Enter Display Order" runat="server" onkeypress="return onlyNumbers(event)" readonly
                                                    style="width:90%;" ClientIDMode="Static"></asp:TextBox>
                                                </td>
                                                    <td>
                                                    <asp:TextBox ID="txtSpecialButtonText" placeholder="Enter Button Display Text" runat="server" style="width:90%;" ClientIDMode="Static"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqdSpecialText" runat="server" ForeColor="Red" ValidationGroup="ValidateEntry" ControlToValidate="txtSpecialButtonText" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                         <asp:TextBox ID="txtSpecialButtonNavigationURL" runat="server" placeholder="Enter Navigation URL" ClientIDMode="Static" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rqdSpecialButtonNavigation" runat="server" ForeColor="Red" ValidationGroup="ValidateEntry" ControlToValidate="txtSpecialButtonNavigationURL" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                       <td>
                                                         <asp:TextBox ID="txtColor" runat="server" CssClass="input-large jscolor" placeholder="Color" ClientIDMode="Static" ></asp:TextBox>
                                                  
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkButtonActive" runat="server" ClientIDMode="Static" Checked/>
                                                        Active
                                                    </td>
                                                   
                                                    <td>
                                                     <asp:ImageButton runat="server" ID="btnSpecialButtonSave"  ImageUrl="~/cadmin/images/Button-Ok-icon.png" style="height:30px; width:30%;" 
                                                            onclick="btnSpecialButtonSave_Click" ValidationGroup="ValidateEntry" ToolTip="Save Special Button Detail"/>
                                                        <asp:ImageButton runat="server" ID="btnUpdateSpecialButtons" ImageUrl="~/cadmin/images/Button-Ok-icon.png" style="height:30px; width:30%;"  ToolTip="Update Special Button Detail"    onclick="btnUpdateSpecialButtons_Click" ValidationGroup="ValidateEntry" />
                                                         <asp:ImageButton runat="server" ID="btnCancelSpecialButton" 
                                                            ImageUrl="~/cadmin/images/button_cancel_256.png" 
                                                            ToolTip="Cancel SMTP Emails" style="height:30px; width:30%;"
                                                            onclick="btnCancelSpecialButton_Click" />
                                                    </td>
                                                </tr>                                     
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                        <br />

                        </div>
                        <div class="par control-group" id="specialContent" clientidmode="Static" runat="server" style="display:none;">                              
                            <div class="controls">
                            <textarea id="elm2" name="elm2"  rows="15" class="tinymce" validationgroup="grpsubmit"  
                                                        runat="server" style="height: 140px; width: 100px; max-height: 200px; min-height: 200px;
                                                        max-width: 200px; min-width: 100px;" ></textarea>
                                                     
                            </div>
                        </div>
                        </div>
                        <div class="par control-group">
                            <p class="stdformbutton">
                                <asp:Button ID="btnsubmitMaster" runat="server" class="btn btn-primary" Visible="false"
                                    OnClick="btnsubmitMaster_onclick" Text="Save" Width="150px" ValidationGroup="grpsubmit" />
                                <asp:Button ID="btnMasterupdate" runat="server" class="btn btn-primary" OnClick="btnMasterupdate_onclick"
                                    Text="Update" Width="150px" ValidationGroup="grpsubmit" Visible="false" />
                                <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                    OnClick="btnCancel_Click" />
                            </p>
                        </div>
                    </div>
                </div>
                <%-- </form>--%>
            </div>
            <!--widgetcontent-->
        </div>
        <!--widget-->
        <asp:HiddenField ID="hdnSMTPId" runat="server" />
        <asp:HiddenField ID="hdnImageHeight" runat="server" />
        <asp:HiddenField ID="hdnImageWidth" runat="server" />
        <asp:HiddenField ID="hdnSliderHeight" runat="server" />
        <asp:HiddenField ID="hdnSliderWidth" runat="server" />
        <asp:HiddenField ID="hdnSMTPDetailId" runat="server" />
        <asp:HiddenField ID="hdnButtonTextSection" runat="server" ClientIDMode="Static" />
        <uc1:CadminFooter ID="CadminFooter1" runat="server" />
        <!-- for Display Order Popup -->
    </div>
    <!--maincontentinner-->
    <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
        role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
        top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
    </ul>
    </div>
    <!--maincontent-->
    <!----Question Content ends here--->
    <script language="javascript" type="text/javascript">

        function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
        }

        function AddQues() {
            var ImgAddQuestion = document.getElementById("ImgAddQuestion");
            ImgAddQuestion.click();
        }
        function AddAns() {
            var ImgAddAnswer = document.getElementById("ImgAddAnswer");
            ImgAddAnswer.click();
        }

        function RemoveFeature(elem) {
            var id = $(elem).attr("id");
            var GetId = id.toString().split('_');
            $("#divFeature_" + GetId[1]).style.visibility = "hidden";
        }

        function ChangeContentType(Control) {
            if (Control == "rbtButtonContent") {
                jQuery("#divSpecialButtons").show();
                jQuery("#specialContent").hide();
                jQuery("#rbtnTextContent").attr("checked", false);
                jQuery("#rbtButtonContent").attr("checked", true);
            }
            else if (Control == "rbtnTextContent") {
                jQuery("#specialContent").show();
                jQuery("#divSpecialButtons").hide();
                jQuery("#rbtButtonContent").attr("checked", false);
                jQuery("#rbtnTextContent").attr("checked", true);
            }
            else {
                jQuery("#divSpecialButtons").hide();
                jQuery("#specialContent").hide();
                jQuery("#rbtnTextContent").attr("checked", false);
                jQuery("#rbtButtonContent").attr("checked", false);
            }
        }

        function chkCharecter(cntrl) {
            alert(JSON.stringify(cntrl));
        };


        function SelectedSectionShow() {
            if ($("#hdnButtonTextSection").val() == "Button") {
                jQuery("#divSpecialButtons").show();
                jQuery("#specialContent").hide();
                jQuery("#rbtnTextContent").attr("checked", false);
                jQuery("#rbtButtonContent").attr("checked", true);
            }
            else if ($("#hdnButtonTextSection").val() == "Content") {
                jQuery("#divSpecialButtons").hide();
                jQuery("#specialContent").show();
                jQuery("#rbtnTextContent").attr("checked", true);
                jQuery("#rbtButtonContent").attr("checked", false);
            }
            else {
                jQuery("#divSpecialButtons").show();
                jQuery("#specialContent").hide();
                jQuery("#rbtnTextContent").attr("checked", false);
                jQuery("#rbtButtonContent").attr("checked", true);
            }
        }
    </script>
    <script src="../js/jscolor.js" type="text/javascript"></script>
</asp:Content>
