﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class SurveyDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";

            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Survey();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
                SetControls();
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddSurveyTop.Visible = true;
                    btnAddSurveyBottom.Visible = true;

                }
                else
                {
                    btnAddSurveyTop.Visible = false;
                    btnAddSurveyBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrSurvey.Items)
                {

                    LinkButton lnkSurveyName = (LinkButton)Item.FindControl("lnkSurveyName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    LinkButton lnkEdit = (LinkButton)Item.FindControl("lnkEdit");
                    LinkButton lnkReport = (LinkButton)Item.FindControl("lnkReport");
                    LinkButton lnkWelcome = (LinkButton)Item.FindControl("lnkWelcome");
                    LinkButton lnkDelete = (LinkButton)Item.FindControl("lnkDelete");
                    if (lblTitle != null && lnkSurveyName != null && btnOrderUP != null && btnOrderDown != null && lnkEdit != null && lnkDelete != null && lnkReport != null && lnkWelcome != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {

                            lnkSurveyName.Visible = true;
                            lblTitle.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;
                            lnkEdit.Visible = true;
                            lnkDelete.Visible = true;
                            lnkReport.Visible = true;
                            lnkWelcome.Visible = true;
                        }
                        else
                        {

                            lnkSurveyName.Visible = false;
                            lblTitle.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                            lnkWelcome.Visible = false;
                            lnkReport.Visible = false;
                            lnkEdit.Visible = false;
                            lnkDelete.Visible = false;
                        
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // Bind Survey inside RptrSurvey Repeater
        protected void Bind_Survey()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_Survey();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrSurvey.DataSource = dt;
                    RptrSurvey.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void RptrSurvey_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditSurvey":
                        Response.Redirect("~/cadmin/Survey.aspx?SurveyID=" + e.CommandArgument.ToString());
                        break;
                    case "ViewReport":
                        Response.Redirect("~/cadmin/SurveyReports.aspx?PWID=" + e.CommandArgument.ToString());
                        break;
                    case "DeleteSurvey":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_Survey(Convert.ToInt32(e.CommandArgument));
                        Response.Redirect("~/cadmin/SurveyDashboard.aspx");
                        Bind_Survey();
                        break;
                    case "Welcome":
                        Response.Redirect("~/cadmin/ContentManagementDashboard.aspx?PID=48");
                        break;
                    case "OrderUp":
                        if (lblDorder != null)
                        {
                            
                        }
                        Bind_Survey();
                        break;
                    case "OrderDown":
                        if (lblDorder != null)
                        {
                            
                        }
                        Bind_Survey();
                        break;
                    default:
                        break;

                }
            }
        }


        protected void RptrSurvey_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

            }
        }

        protected void btnAddSurveyBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Survey.aspx");

        }
    }
}