﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SliderItemDetailsDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.SliderItemDetailsDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }

        function PreviewImage(ctrlID) {
            var ctrlNew = ctrlID.replace('imgSliderItem', 'divPreview');
            jQuery("#" + ctrlID).css("display", "none");
            jQuery("#" + ctrlNew).css("display", "");
            ctrlID.replace('divPreview', 'imgSliderItems');
        }
        function ClosePreviewImage(ctrlID) {
            var ctrlNew = ctrlID.replace('lblClosePreview', 'divPreview');
            var ctrlSmallImage = ctrlID.replace('lblClosePreview', 'imgSliderItem');
            jQuery("#" + ctrlSmallImage).css("display", "");
            jQuery("#" + ctrlNew).css("display", "none");
            ctrlID.replace('imgSliderItem', 'lblClosePreview');
        }

        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
        }       
    </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;
            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage slider</a> <span class="separator"></span></li>
        <li>Slider Item Details Dashboard</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage slider</h5>
            <h1>
                Slider Item Details Dashboard</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <asp:HiddenField ID="hdnSliderItemID" runat="server" />
            <h4 class="widgettitle">
                <!--Filer by Status starts here--->
                <div class="par_ctrl_sec">
                    <div class="par control-group">
                        <label class="control-label" for="ddlStatus">
                            Filer by Status</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                                <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!--Filer by Status ends here--->
                Slider Item Details Dashboard
                <asp:Button ID="btnBack" runat="server" CssClass="btn" Width="100" Text="Back" Style="float: right;
                    margin-top: -5px;" OnClick="btnBack_Click" />
                <asp:Button ID="btnAddSlider" runat="server" CssClass="btn btn-primary" Width="150"
                    Text="Add Sub Item" Style="float: right; margin-top: -5px;" OnClick="btnAddSlider_Click" />
             </h4>
              <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 5%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 8%;" />
                    <col class="con1" style="width: 5%;" />
                    <col class="con1" style="width: 5%;" />
                    <col class="con1" style="align: center; width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Sr. No
                        </th>
                        <th class="head1">
                            Item Type
                        </th>
                        <th class="head1">
                            Item Content
                        </th>
                        <th class="head1">
                            Item Position
                        </th>
                        <th class="head1">
                            Display Order
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                            <center>
                                Functions
                            </center>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrSliderDetails" OnItemCommand="RptrSliderDetails_ItemCommand"
                        OnItemDataBound="RptrSliderDetails_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:HiddenField ID="hdnSliderItemContentID" runat="server" Value='<%# Eval("SliderItemContentID") %>' />
                                    <asp:HiddenField ID="hdnSliderItemID" runat="server" Value='<%# Eval("SliderItemID") %>' />
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td>
                                    <asp:Label ID="lblItemType" runat="server" ClientIDMode="Static" Text='<%# Eval("ItemType") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblImagename" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                                    <img id="imgSliderItem" src='<%#"../UploadedFiles/SliderItemDetails/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>'
                                        runat="server" onclick="PreviewImage(this.id);" style="width: 60px; height: 60px;" />
                                    <div style="max-width: 400px; max-height: 355px; z-index: 10000000; display: none;"
                                        id="divPreview" runat="Server">
                                        <label id="lblClosePreview" runat="server" class="closePreview" onclick="return ClosePreviewImage(this.id);">X</label>
                                        <img id="imgSliderItemPreview" runat="server" src='<%#"../UploadedFiles/SliderItemDetails/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>' />
                                    </div>
                                    <asp:Label ID="lblItemContent" runat="server" ClientIDMode="Static" Text='<%# Eval("ItemContent") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblContentPosition" runat="server" ClientIDMode="Static" Text='<%# Eval("ContentPosition") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SliderItemContentID")%>' CommandName="EditSlider"
                                        ToolTip="Edit Slider" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SliderItemContentID")%>' CommandName="DeleteSlider"
                                        ToolTip="Delete Slider" OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:HiddenField ID="hdnSliderItemContentID" runat="server" Value='<%# Eval("SliderItemContentID") %>' />
                                    <asp:HiddenField ID="hdnSliderItemID" runat="server" Value='<%# Eval("SliderItemID") %>' />
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td>
                                    <asp:Label ID="lblItemType" runat="server" ClientIDMode="Static" Text='<%# Eval("ItemType") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblImagename" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                                    <img id="imgSliderItem" src='<%#"../UploadedFiles/SliderItemDetails/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>'
                                        runat="server" onclick="PreviewImage(this.id);" style="width: 60px; height: 60px;" />
                                    <div style="max-width: 400px; max-height: 355px; z-index: 10000000; display: none;"
                                        id="divPreview" runat="Server">
                                        <label id="lblClosePreview" runat="server" class="closePreview" onclick="return ClosePreviewImage(this.id);">X</label>
                                        <img id="imgSliderItemPreview" runat="server" src='<%#"../UploadedFiles/SliderItemDetails/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>' />
                                    </div>
                                    <asp:Label ID="lblItemContent" runat="server" ClientIDMode="Static" Text='<%# Eval("ItemContent") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblContentPosition" runat="server" ClientIDMode="Static" Text='<%# Eval("ContentPosition") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SliderItemContentID")%>' CommandName="EditSlider"
                                        ToolTip="Edit Slider" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SliderItemContentID")%>' CommandName="DeleteSlider"
                                        ToolTip="Delete Slider" OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <style type="text/css">
        .closePreview
        {
            border: 1px solid Green;
            width: 20px;
            color: #000;
            background: #fff;
            position: absolute;
            font-weight: 400;
            border-radius: 4px;
        }
        .closePreview:hover
        {
            border: 1px solid Green;
            width: 20px;
            color: #fff;
            background: Red;
            position: absolute;
            font-weight: 400;
            border-radius: 4px;
        }
    </style>
    <!--maincontent-->
</asp:Content>
