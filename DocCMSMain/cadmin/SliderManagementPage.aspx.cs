﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class SliderManagementPage : System.Web.UI.Page
    {
        dynamic UserId;
        protected static Int32 SMTPDetailID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                btnsubmitMaster.Visible = true;
                if (Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    btnsubmitMaster.Visible = false;
                    btnMasterupdate.Visible = true;
                    Bind_SpecialButtons(Convert.ToInt32(Request.QueryString["SliderID"]));
                    DivSliderEmptyAreaDetails.Visible = true;
                    BindlblDefaultValue();
                    BindContainText(Convert.ToInt32(Request.QueryString["SliderID"]));
                    Bind_Master_Slider(Convert.ToInt16(Request.QueryString["SliderID"]));
                }
                else
                {
                    btnsubmitMaster.Visible = true;
                    btnMasterupdate.Visible = false;
                    BindlblDefaultValue();
                    DivSliderEmptyAreaDetails.Visible = false;
                }
            }
        }


        public void BindContainText(Int32 SliderID)
        {
            DataTable dtContainText = ServicesFactory.DocCMSServices.Fetch_Slider_Contain_Text_By_sliderid(SliderID);
            if (dtContainText != null && dtContainText.Rows.Count > 0)
            {
                elm2.Value = Convert.ToString(dtContainText.Rows[0]["TextContent"]);
            }
            else
            {
                elm2.Value = "";
            }

        }

        public void BindlblDefaultValue()
        {
            if (ddlSliderPosition.SelectedValue.ToLower() == "left" || ddlSliderPosition.SelectedValue.ToLower() == "right")
            {
                txtHeight.Text = "288";
                txtWidth.Text = "497";
                txtImageHeight.Text = "288";
                txtImagewidth.Text = "497";
                if (Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    DivSliderEmptyAreaDetails.Visible = true;
                }
                else
                {
                    DivSliderEmptyAreaDetails.Visible = false;
                }
            }
            else
            {
                txtHeight.Text = "700";
                txtWidth.Text = "1900";
                txtImageHeight.Text = "700";
                txtImagewidth.Text = "1900";
                DivSliderEmptyAreaDetails.Visible = false;
            }
        }

        protected void btnsubmitMaster_onclick(object sender, EventArgs e)
        {
            try
            {
                string CheckBoxValue = "";
                ClsSliderMaster objClsSliderMaster = new ClsSliderMaster();
                objClsSliderMaster.slidername = txtSliderName.Text;
                objClsSliderMaster.sliderposition = Convert.ToString(ddlSliderPosition.SelectedValue);
                objClsSliderMaster.height = Convert.ToString(txtHeight.Text != "" ? txtHeight.Text : "0");
                if (Convert.ToString(ddlSliderPosition.SelectedValue) == "Left" || Convert.ToString(ddlSliderPosition.SelectedValue) == "Right")
                {
                    if (Convert.ToInt32(txtWidth.Text) > 650)
                    {
                        objClsSliderMaster.width = "650";
                    }
                    else
                    {
                        objClsSliderMaster.width = Convert.ToString(txtWidth.Text != "" ? txtWidth.Text : "0");
                    }
                    if (rbtButtonContent.Checked)
                    {
                        CheckBoxValue = "Button";
                    }
                    else if (rbtnTextContent.Checked)
                    {
                        CheckBoxValue = "Content";
                    }
                    else
                    {
                        CheckBoxValue = "";
                    }
                }
                else
                {
                    objClsSliderMaster.width = Convert.ToString(txtWidth.Text != "" ? txtWidth.Text : "0");
                    CheckBoxValue = "";
                }
                objClsSliderMaster.imageheight = Convert.ToString(txtImageHeight.Text != "" ? txtImageHeight.Text : "0");
                objClsSliderMaster.imagewidth = Convert.ToString(txtImagewidth.Text != "" ? txtImagewidth.Text : "0");
                objClsSliderMaster.createdby = Convert.ToString(Session["UserID"]);
                objClsSliderMaster.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
                objClsSliderMaster.buttontextSection = CheckBoxValue;
                int Retvalue = ServicesFactory.DocCMSServices.Insert_Slider_Master(objClsSliderMaster);
                if (Retvalue > 0)
                {
                    Response.Redirect("SliderManagementDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnMasterupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                string CheckBoxValue = "";
                ClsSliderMaster objClsSliderMaster = new ClsSliderMaster();
                objClsSliderMaster.sliderid = Convert.ToInt32(Request.QueryString["SliderID"]);
                objClsSliderMaster.slidername = txtSliderName.Text;
                objClsSliderMaster.sliderposition = Convert.ToString(ddlSliderPosition.SelectedValue);
                objClsSliderMaster.height = Convert.ToString(txtHeight.Text != "" ? txtHeight.Text : "0");
                if (Convert.ToString(ddlSliderPosition.SelectedValue) == "Left" || Convert.ToString(ddlSliderPosition.SelectedValue) == "Right")
                {
                    if (Convert.ToInt32(txtWidth.Text) > 650)
                    {
                        objClsSliderMaster.width = "650";
                    }
                    else
                    {
                        objClsSliderMaster.width = Convert.ToString(txtWidth.Text != "" ? txtWidth.Text : "0");
                    }
                    if (rbtButtonContent.Checked)
                    {
                        CheckBoxValue = "Button";
                    }
                    else if (rbtnTextContent.Checked)
                    {
                        CheckBoxValue = "Content";
                    }
                    else
                    {
                        CheckBoxValue = "";
                    }
                }
                else
                {
                    objClsSliderMaster.width = Convert.ToString(txtWidth.Text != "" ? txtWidth.Text : "0");
                    CheckBoxValue = "";
                }
                objClsSliderMaster.imageheight = Convert.ToString(txtImageHeight.Text != "" ? txtImageHeight.Text : "0");
                objClsSliderMaster.imagewidth = Convert.ToString(txtImagewidth.Text != "" ? txtImagewidth.Text : "0");
                objClsSliderMaster.lastmodifiedby = Convert.ToString(Session["UserID"]);
                objClsSliderMaster.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
                objClsSliderMaster.buttontextSection = CheckBoxValue;
                int Retvalue = ServicesFactory.DocCMSServices.Update_Slider_Master_Data(objClsSliderMaster);
                // Start Text Contain Section
                Cls_SliderContent objSliderContent = new Cls_SliderContent();
                objSliderContent.sliderid = Convert.ToInt32(Request.QueryString["SliderID"]);
                objSliderContent.textcontent = elm2.Value;
                objSliderContent.createdby = Convert.ToString(Session["UserID"]);
                objSliderContent.updatedby = Convert.ToString(Session["UserID"]);
                //End Text Contain Section
                if (Retvalue > 0)
                {
                    if (rbtnTextContent.Checked)
                    {
                        int retval = ServicesFactory.DocCMSServices.Insert_Special_Contain(objSliderContent);
                        if (retval >= 0 && retval != 0)
                        {

                        }
                    }
                    Response.Redirect("SliderManagementDashboard.aspx");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SliderManagementDashboard.aspx");
        }


        //Bind All Master Slider
        private void Bind_Master_Slider(int SliderID)
        {
            DataTable DtGetMasterSliderDetails = ServicesFactory.DocCMSServices.Fetch_Slider_Master_Data_By_sliderid(SliderID);
            if (DtGetMasterSliderDetails != null && DtGetMasterSliderDetails.Rows.Count > 0)
            {
                txtSliderName.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["SliderName"]);
                //txtHeight.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["FullWidth"]);
                ddlSliderPosition.SelectedValue = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["SliderPosition"]);
                txtHeight.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["Height"]);
                txtImagewidth.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ImageWidth"]);
                txtImageHeight.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ImageHeight"]);
                txtWidth.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["Width"]);
                ChkActiveStatus.Checked = Convert.ToBoolean(DtGetMasterSliderDetails.Rows[0]["IsActive"]);
                hdnImageHeight.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ImageHeight"]);
                hdnImageWidth.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ImageWidth"]);
                hdnSliderHeight.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["Height"]);
                hdnSliderWidth.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["Width"]);
                hdnButtonTextSection.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ButtonTextSection"]);
            }
        }

        protected void ddlSliderPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSliderPosition.SelectedValue.ToLower() == "left" || ddlSliderPosition.SelectedValue.ToLower() == "right")
            {
                txtHeight.Text = "288";
                txtWidth.Text = "497";
                txtImageHeight.Text = "288";
                txtImagewidth.Text = "497";
                if (Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    DivSliderEmptyAreaDetails.Visible = true;
                }
                else
                {
                    DivSliderEmptyAreaDetails.Visible = false;
                }
            }
            else
            {
                txtHeight.Text = "700";
                txtWidth.Text = "1900";
                txtImageHeight.Text = "700";
                txtImagewidth.Text = "1900";
                DivSliderEmptyAreaDetails.Visible = false;
            }
        }


        protected void ImgAddSpecialButton_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["SliderID"] != null)
            {
                trAdditionalRow.Visible = true;
                txtSpecialButtonText.Text = "";
                txtSpecialButtonNavigationURL.Text = "";
                chkButtonActive.Checked = true;
                txtSpecialButtonDisplayOrder.Text = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_SpecialButton_dorder(Convert.ToInt32(Request.QueryString["SliderID"])) + 1);
                btnUpdateSpecialButtons.Visible = false;
                btnSpecialButtonSave.Visible = true;
                Bind_SpecialButtons(Convert.ToInt32(Request.QueryString["SliderID"]));
                AddFeature.Visible = false;
                lblQuestion.Visible = true;

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "javascript:alert('First create a Slider then add Special Buttons detail');", true);
                return;
            }
        }
        protected void Bind_SpecialButtons(Int32 sliderId)
        {
            try
            {
                DataTable dtSpecialButtons = ServicesFactory.DocCMSServices.Fetch_Slider_Special_Buttons(sliderId);
                if (dtSpecialButtons != null && dtSpecialButtons.Rows.Count > 0)
                {
                    if (dtSpecialButtons.Rows.Count >= 4)
                    {
                        AddFeature.Visible = false;
                        lblQuestion.Visible = true;
                    }
                    else
                    {
                        AddFeature.Visible = true;
                        lblQuestion.Visible = false;
                    }
                    rptrSpecialButton.DataSource = dtSpecialButtons;
                    rptrSpecialButton.DataBind();
                }
                else
                {
                    rptrSpecialButton.DataSource = "";
                    rptrSpecialButton.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSpecialButtonSave_Click(object sender, EventArgs e)
        {
            bool CheckExistDepatment = ServicesFactory.DocCMSServices.Check_Slider_Special_Button_name(Convert.ToInt32(Request.QueryString["SliderID"]), Convert.ToString(txtSpecialButtonText.Text));
            if (CheckExistDepatment == false)
            {
                Cls_SliderSpecialButtons objCls_SliderSpecialButtons = new Cls_SliderSpecialButtons();
                objCls_SliderSpecialButtons.sliderid = Convert.ToInt32(Request.QueryString["SliderID"]);
                objCls_SliderSpecialButtons.buttontext = txtSpecialButtonText.Text.Trim();
                objCls_SliderSpecialButtons.navigationurl = txtSpecialButtonNavigationURL.Text.Trim();
                objCls_SliderSpecialButtons.isactive = Convert.ToString(chkButtonActive.Checked);
                objCls_SliderSpecialButtons.dorder = Convert.ToInt32(txtSpecialButtonDisplayOrder.Text);
                objCls_SliderSpecialButtons.createdby = Convert.ToString(Session["UserID"]);
                Int32 retVal = ServicesFactory.DocCMSServices.Insert_Slider_Special_Button_Detail(objCls_SliderSpecialButtons);
                if (retVal > 0)
                {
                    trAdditionalRow.Visible = false;
                    AddFeature.Visible = true;
                    lblQuestion.Visible = false;
                    Bind_SpecialButtons(Convert.ToInt32(Request.QueryString["SliderID"]));
                }
            }
            else
            {
                string message = "alert('Button Name Already Exist.')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
        }

        protected void btnUpdateSpecialButtons_Click(object sender, EventArgs e)
        {
            try
            {
                Cls_SliderSpecialButtons objCls_SliderSpecialButtons = new Cls_SliderSpecialButtons();
                objCls_SliderSpecialButtons.id = Convert.ToInt32(hdnSpecialButtonId.Value);
                objCls_SliderSpecialButtons.sliderid = Convert.ToInt32(Request.QueryString["SliderID"]);
                objCls_SliderSpecialButtons.buttontext = txtSpecialButtonText.Text;
                objCls_SliderSpecialButtons.navigationurl = txtSpecialButtonNavigationURL.Text;
                objCls_SliderSpecialButtons.isactive = Convert.ToString(chkButtonActive.Checked);
                objCls_SliderSpecialButtons.updateby = Convert.ToString(Session["UserID"]);
                objCls_SliderSpecialButtons.buttoncolor = Convert.ToString(txtColor.Text);
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Slider_Special_Button_Detail(objCls_SliderSpecialButtons);
                if (retVal > 0)
                {
                    trAdditionalRow.Visible = false;
                    AddFeature.Visible = true;
                    lblQuestion.Visible = false;
                    Bind_SpecialButtons(Convert.ToInt32(Request.QueryString["SliderID"]));
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Special Button Details Update successfully');</script>");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Error occur in Updating...');</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnCancelSpecialButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/SliderManagementPage.aspx?SliderID=" + Request.QueryString["SliderID"]);
        }

        protected void rptrSpecialButton_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                switch (e.CommandName)
                {
                    case "DeleteSpecialButton":
                        Int32 retDel = ServicesFactory.DocCMSServices.Delete_Slider_Special_Button_by_id(Convert.ToInt32(e.CommandArgument));
                        if (retDel > 0)
                        {
                            Bind_SpecialButtons(Convert.ToInt32(Request.QueryString["SliderID"]));
                        }
                        break;
                    case "EditSpecialButton":
                        e.Item.Visible = false;
                        Edit_Slider_Special_Buttons(Convert.ToInt32(e.CommandArgument));
                        Session["HiddenRow"] = Convert.ToString(e.Item.ItemIndex);
                        Bind_SpecialButtons(Convert.ToInt32(Request.QueryString["SliderID"]));
                        break;
                }
            }
        }

        protected void Edit_Slider_Special_Buttons(Int32 ID)
        {
            DataTable dtSliderSpecial = ServicesFactory.DocCMSServices.Fetch_Slider_Special_Buttons_By_id(ID);
            if (dtSliderSpecial != null && dtSliderSpecial.Rows.Count > 0)
            {
                trAdditionalRow.Visible = true;
                btnUpdateSpecialButtons.Visible = true;
                btnSpecialButtonSave.Visible = false;
                txtSpecialButtonDisplayOrder.Text = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_SpecialButton_dorder(Convert.ToInt32(Request.QueryString["SliderID"])));
                hdnSpecialButtonId.Value = Convert.ToString(dtSliderSpecial.Rows[0]["ID"]);
                txtSpecialButtonText.Text = Convert.ToString(dtSliderSpecial.Rows[0]["ButtonText"]);
                txtColor.Text = Convert.ToString(dtSliderSpecial.Rows[0]["ButtonColor"]) != null ? Convert.ToString(dtSliderSpecial.Rows[0]["ButtonColor"]) : "";
                txtSpecialButtonNavigationURL.Text = Convert.ToString(dtSliderSpecial.Rows[0]["NavigationURL"]);
                chkButtonActive.Checked = Convert.ToBoolean(dtSliderSpecial.Rows[0]["IsActive"]);

            }

        }
        protected void rptrSpecialButton_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }
    }// Class Ends here
}