﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="PlanDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.PlanDashboard" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Plans</a> <span class="separator"></span></li>
        <li>Manage Plans</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Plans</h5>
            <h1>
                Manage Plans</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
               Plans Details
                <asp:Button ID="btnAddPlansTop" runat="server" CssClass="btn btn-primary" Width="150" Text="Add New Plans"
                    Style="float: right; margin-top: -5px;" OnClick="btnAddPlansBottom_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 55%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 5%;" />                    
                    <col class="con1" style="align: center; width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Display Order
                        </th>
                        <th class="head1">
                            Plans
                        </th>
                           <th class="head1">
                           Price
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrPlans" OnItemCommand="RptrPlans_ItemCommand"
                        OnItemDataBound="RptrPlans_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:Label ID="lblPlanID" runat="server" ClientIDMode="Static" Text='<%# Eval("PlanID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'></asp:Label>                                
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPlanName" CommandArgument='<%# Eval("PlanID")%>' CommandName="EditPlan"
                                        runat="server" Text='<%# Eval("PlanName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("PlanName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                     <asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("PlanID")%>' CommandName="EditPlan" ToolTip="Edit Plan" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("PlanID")%>' CommandName="DeletePlan" ToolTip="Delete Plan" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                 <td>
                                    <asp:Label ID="lblPlanID" runat="server" ClientIDMode="Static" Text='<%# Eval("PlanID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'></asp:Label>                                
                                   
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPlanName" CommandArgument='<%# Eval("PlanID")%>' CommandName="EditPlan"
                                        runat="server" Text='<%# Eval("PlanName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("PlanName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                     <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("PlanID")%>' CommandName="EditPlan" ToolTip="Edit Plan" />
                                     <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("PlanID")%>' CommandName="DeletePlan" ToolTip="Delete Plan" />
                                     <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <asp:Button ID="btnAddPlansBottom" runat="server" CssClass="btn btn-primary" Width="150"
                Text="Add New Plan" OnClick="btnAddPlansBottom_Click" />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>