﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.Factory;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class CompanyDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Company();
                SetControls();
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddCompanyTop.Visible = true;
                    btnAddCompanyBottom.Visible = true;
                }
                else
                {
                    btnAddCompanyTop.Visible = false;
                    btnAddCompanyBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrCompanyPage.Items)
                {
                    LinkButton lnkCompanyName = (LinkButton)Item.FindControl("lnkCompanyName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    if (lblTitle != null && lnkCompanyName != null && lnkEdit != null && lnkDelete != null )
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkCompanyName.Visible = true;
                            lblTitle.Visible = false;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkCompanyName.Visible = false;
                            lblTitle.Visible = true;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // Binding Company inside RptrCompanyPage repeater Control
        protected void Bind_Company()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_company();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrCompanyPage.DataSource = dt;
                    RptrCompanyPage.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrCompanyPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditCompany":
                        Response.Redirect("~/cadmin/CompanySettingPage.aspx?CID=" + e.CommandArgument.ToString());
                        break;
                    case "DeleteCompany":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_company(Convert.ToInt32(e.CommandArgument));
                        Bind_Company();
                        break;
                    default:
                        break;
                }
            }
        }

        protected void RptrCompanyPage_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void btnAddCompanyBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/CompanySettingPage.aspx");
        }
    }
}