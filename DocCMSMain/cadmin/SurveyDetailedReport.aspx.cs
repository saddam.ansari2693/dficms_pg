﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class SurveyDetailedReport1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "licontent";
            Session["Ulname"] = "ulcontent";
            if (!IsPostBack)
            {
                if (Request.QueryString["PWID"] != null)
                {
                   using (DataTable dtSurveyDetail = ServicesFactory.DocCMSServices.Fetch_Survey_Detail_By_id(Convert.ToString(Request.QueryString["PWID"])))
                    {
                        if (dtSurveyDetail != null && dtSurveyDetail.Rows.Count > 0)
                        {
                            LblSurveyName.Text = Convert.ToString(dtSurveyDetail.Rows[0]["SurveyName"]);
                            LblSurveyDescription.Text = Convert.ToString(dtSurveyDetail.Rows[0]["Description"]);
                        }
                    }
                    using (DataTable dtDashboard = ServicesFactory.DocCMSServices.Fetch_Detailed_Survey_Dashboard(Convert.ToString(Request.QueryString["PWID"])))
                    {
                        if (dtDashboard != null && dtDashboard.Rows.Count > 0)
                        {
                            rptr_DetailedReport.DataSource = dtDashboard;
                            rptr_DetailedReport.DataBind();
                            rptr_DetailedReport.Visible = true;
                        }
                    }
                }
                else
                {
                    Response.Redirect("SurveyResponseDashboard.aspx");
                }
            }
        }
        protected void lnkViewSummaryReport_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("SurveySummaryReport.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]) + "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lnkViewDetailedReport_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("DetailedSurveyReport.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]) + "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("SurveyReports.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]) + "");
        }

        protected void btnExportAll_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtExport = new DataTable();
                dtExport.Columns.Add("SurveyName", typeof(string));
                dtExport.Columns.Add("Description", typeof(string));
                dtExport.Columns.Add("RespondentIP", typeof(string));
                dtExport.Columns.Add("ResponseDate", typeof(string));
                dtExport.Columns.Add("Question", typeof(string));
                dtExport.Columns.Add("QuestionType", typeof(string));
                dtExport.Columns.Add("Answer", typeof(string));
                DataTable dtAllResponses = ServicesFactory.DocCMSServices.Get_All_Survey_Responses(Convert.ToInt32(Request.QueryString["PWID"]));
                if (dtAllResponses != null && dtAllResponses.Rows.Count > 0)
                {
                    string PrevRespIP = "";
                    for (Int32 i = 0; i < dtAllResponses.Rows.Count; i++)
                    {
                        string SurveyName = "";
                        string SurveyDesc = "";
                        string RespondentIP = "";
                        string ResponseDate = "";
                        string Question = "";
                        string QuestionType = "";
                        string Answer = "";
                        if (i == 0)
                        {
                            SurveyName = Convert.ToString(dtAllResponses.Rows[i]["SurveyName"]);
                            SurveyDesc = Convert.ToString(dtAllResponses.Rows[i]["Description"]);
                            PrevRespIP = Convert.ToString(dtAllResponses.Rows[i]["IPAddress"]);
                            RespondentIP = Convert.ToString(dtAllResponses.Rows[i]["IPAddress"]);
                            ResponseDate = Convert.ToString(dtAllResponses.Rows[i]["ResponseDate"]);
                        }
                        else
                        {
                            RespondentIP = Convert.ToString(dtAllResponses.Rows[i]["IPAddress"]);
                            if (RespondentIP == PrevRespIP)
                            {
                                RespondentIP = "";
                            }
                            else
                            {
                                PrevRespIP = RespondentIP;
                            }                            
                            ResponseDate = Convert.ToString(dtAllResponses.Rows[i]["ResponseDate"]);
                        }
                        // For Survey IP and Response Date
                        Question = Convert.ToString(dtAllResponses.Rows[i]["Question"]);
                        QuestionType = Convert.ToString(dtAllResponses.Rows[i]["AnswerType"]);
                        Answer = Convert.ToString(dtAllResponses.Rows[i]["Answer"]);
                        dtExport.Rows.Add(SurveyName, SurveyDesc, RespondentIP, ResponseDate, Question, QuestionType, Answer);
                    }
                }
                if (dtExport != null && dtExport.Rows.Count > 0)
                {
                    List<string> colArray = new List<string>();
                    colArray.Add("Survey Name"); 
                    colArray.Add("Description"); 
                    colArray.Add("Respondent IP"); 
                    colArray.Add("Response Date"); 
                    colArray.Add("Question");
                    colArray.Add("QuestionType"); 
                    colArray.Add("Answer");
                    ServicesFactory.DocCMSServices.ExporttoExcel(dtExport, "CompleteSurveyDetailedReport.xls", colArray);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptr_DetailedReport_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                if (e.CommandName == "ViewReport")
                {
                    Response.Redirect("SurveyFullReport.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]) + "&RID=" + Convert.ToString(e.CommandArgument));
                }
            }
        }
    }// Class Ends here
}