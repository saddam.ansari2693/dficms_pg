﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class UserDashboard : System.Web.UI.Page
    {
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (!IsPostBack)
            {
                if (Session["RoleId"] != null && Convert.ToString(Session["RoleID"]) != "")
                {
                    Bind_Users(Convert.ToInt32(Session["RoleID"]));
                    Set_Assign_Roles();
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    
                    SetControls();

                }
               

            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddUserTop.Visible = true;
                    btnAddUserBottom.Visible = true;
                }
                else
                {
                    btnAddUserTop.Visible = false;
                    btnAddUserBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrUsers.Items)
                {

                    LinkButton lnkEmail = (LinkButton)Item.FindControl("lnkEmail");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    Label lblRoleId = (Label)Item.FindControl("lblRoleId");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    Label lblUserId = (Label)Item.FindControl("lblUserId");
                    if (lblTitle != null && lnkEmail != null && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            if (Convert.ToString(Session["RoleID"]) == "1")
                            {
                                lnkEdit.Enabled = true;
                                lnkEmail.Visible = true;
                                lblTitle.Visible = false;
                            }
                            else
                            {
                                if (lblUserId.Text != Convert.ToString(Session["UserID"]))
                                {
                                    lnkEdit.Visible = false;
                                    lnkDelete.Visible = false;
                                    lnkEmail.Enabled = false;
                                }
                                else
                                {
                                    lnkEdit.Enabled = true;
                                    lnkEmail.Visible = true;
                                    lblTitle.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkEmail.Visible = false;
                            lblTitle.Visible = true;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // Bind Users inside RptrUsers repeater
        protected void Bind_Users(Int32 RoleID)
        {
            DataTable dtUser = null;
            string SuperAdminName = ServicesFactory.DocCMSServices.Fetch_Roles_name(Convert.ToInt32(Session["RoleID"]));
            if (Convert.ToString(SuperAdminName).ToUpper() != "SuperAdmin".ToUpper())
            {
                dtUser = ServicesFactory.DocCMSServices.Fetch_All_Users_Except_SuperAdmin();
            }
            else
            {
                dtUser = ServicesFactory.DocCMSServices.Fetch_All_Users();
            }
            if (dtUser != null && dtUser.Rows.Count > 0)
            {
                RptrUsers.DataSource = dtUser;
                RptrUsers.DataBind();
            }
        }

        protected void RptrUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                Label lblRoleID = (Label)e.Item.FindControl("lblRoleId");
                switch (e.CommandName)
                {
                    case "EditUser":
                        Response.Redirect("~/cadmin/UserAccounts.aspx?EmailID=" + e.CommandArgument.ToString() + "&RoleId=" + Convert.ToInt32(lblRoleID.Text));
                        break;
                    case "CloneUser":
                         Response.Redirect("~/cadmin/UserAccounts.aspx?CloneEmailID=" + e.CommandArgument.ToString() + "&RoleId=" + Convert.ToInt32(lblRoleID.Text));
                           break;
                    case "DeleteUser":
                         ServicesFactory.DocCMSServices.Delete_User_Profile(Convert.ToString(e.CommandArgument));
                         Bind_Users(Convert.ToInt32(Session["RoleID"]));
                        break;
                    default:
                        break;
                }
            }
        }

        protected void RptrUsers_databound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void Set_Assign_Roles()
        {
            foreach (RepeaterItem item in RptrUsers.Items)
            {
                Label lblRoleId = (Label)item.FindControl("lblRoleId");
                ImageButton lnkEdit = (ImageButton)item.FindControl("lnkEdit");
                ImageButton lnkDelete = (ImageButton)item.FindControl("lnkDelete");
                HiddenField hdnParentId = (HiddenField)item.FindControl("hdnParentId");
                LinkButton lnkEmail = (LinkButton)item.FindControl("lnkEmail");
                string SuperAdminName = ServicesFactory.DocCMSServices.Fetch_Roles_name(Convert.ToInt32(Session["RoleID"]));
                if (Convert.ToString(lblRoleId.Text).ToUpper() == Convert.ToString(Session["RoleID"]).ToUpper())//Convert.ToString(hdnParentId.Value).ToUpper() == Convert.ToString(Session["RoleID"]).ToUpper() ||
                {
                    lnkDelete.Visible = true;
                    lnkEdit.Visible = true;
                    lnkEmail.Enabled = true;
                }
                else
                {
                    lnkDelete.Visible = false;
                    lnkEdit.Visible = false;
                    lnkEmail.Enabled = false;
                }
                if (SuperAdminName.ToUpper() == "SuperAdmin".ToUpper())
                {
                    lnkDelete.Visible = true;
                    lnkEdit.Visible = true;
                    lnkEmail.Enabled = true;
                }
                else
                {
                }
            }
        }
        protected void btnAddUserBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserAccounts.aspx");
        }
    }
}