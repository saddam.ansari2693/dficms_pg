﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="New_UserGuide.aspx.cs" Inherits="DocCMSMain.cadmin.New_UserGuide" %>

<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var count = 0;
            var modal = document.getElementById('myModal');
            jQuery(".userGuideDiv img").click(function () {
                var modalImg = document.getElementById("img01");
                modalImg.src = jQuery(this).attr("src");
                modal.style.display = "block";

            });
        });
        function SetWidthHeight() {
            jQuery("#ContentPlaceHolder1_UserGuideDiv img").css("width", "70%");
            jQuery("#ContentPlaceHolder1_UserGuideDiv img").css("height", "100%");
        }

        function DisplayPreview(ctrl) {
            alert(ctrl);
        }

        function Close() {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: "Lato" , sans-serif;
        }
        .sidenav
        {
            height: 100%;
            width: 0;
            position: absolute;
            z-index: 1;
            top: 0;
            right: 18px;
            background-color: #111;
            overflow-x: hidden;
            transition: 0.5s;
        }
        
        .sidenav li
        {
            text-decoration: none;
            font-size: 15px;
            color: #818181;
            display: block;
            transition: 0.3s;
            border-bottom: 1px solid rgba(255,255,255,0.1);
            margin: 0;
            padding: 0;
        }
        .sidenav li a
        {
            display: block;
            padding: 6px 20px 1px 20px;
        }
        .sidenav a:hover, .offcanvas a:focus
        {
            color: #f1f1f1;
            text-decoration: none !important;
        }
        
        .sidenav .closebtn
        {
            position: absolute;
            top: 4px;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }
        
        @media screen and (max-height: 450px)
        {
            .sidenav
            {
                padding-top: 15px;
            }
            .sidenav a
            {
                font-size: 18px;
            }
        }
       
    </style>
    <style type="text/css">
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
    <script type="text/javascript">
        // Get the modal
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('ctrl');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function () {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
    </script>
    <script type="text/javascript">
        function openNav() {
            if (document.getElementById("mySidenav").style.width == "0px") {
                document.getElementById("mySidenav").style.width = "250px";
            }
            else {
                document.getElementById("mySidenav").style.width = "0px";
            }
        }
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0px";
        }
    </script>
    <script type="text/javascript">
        function myFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            ul = document.getElementById("ContentPlaceHolder1_maincontents");
            li = ul.getElementsByTagName("li");
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    </script>
    <script type="text/javascript">
        var Count = 0;
        jQuery(function () {
            jQuery('#text-search').bind('keyup change', function (ev) {
                // pull in the new value
                var searchTerm = jQuery(this).val();
                jQuery(".highlight").parent().parent().css("border-right", "0px solid Red");
                // remove any old highlighted terms
                jQuery('#ContentPlaceHolder1_UserGuideDiv').removeHighlight();
                // disable highlighting if empty
                if (searchTerm != "") {
                    // highlight the new term
                    Count = 0;
                    jQuery('#ContentPlaceHolder1_UserGuideDiv').highlight(searchTerm);
                    jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(0);
                    jQuery("#lblSeachCount").show();
                    jQuery("#lblSeachCount").html(jQuery("#ContentPlaceHolder1_UserGuideDiv .highlight").length + " result(s) found");
                    jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(jQuery('.highlight:first').position().top - 150);
                    jQuery(".highlight").parent().parent().css("border-right", "5px solid Red");
                }
                else {
                    jQuery("#lblSeachCount").html(jQuery("#ContentPlaceHolder1_UserGuideDiv .highlight").length + " result(s) found");
                    jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(0);
                    jQuery("#lblSeachCount").hide();
                    Count = 0;
                }
                return true;
            });
        });
        // find next highlighted and scroll to it
        function findNext() {
            Count++;
            if (Count < jQuery("#ContentPlaceHolder1_UserGuideDiv .highlight").length) {
                jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(0);
                jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(jQuery('.highlight:eq(' + Count + ')').position().top - 150);
            }
            else {
                alert("No Match Found");
                nextCount = 0;
             }
        }
        // find previous highlighted and scroll to it
        function findPrev() {
            Count--;
            if (Count <= jQuery("#ContentPlaceHolder1_UserGuideDiv .highlight").length) {
                jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(0);
                jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(jQuery('.highlight:eq(' + Count + ')').position().top - 150);
            }
            else {
                alert("No Match Found");
                nextCount = 0;
            }
        }
        // find first highlighted and scroll to it
        function findFirst() {
            jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(0);
            jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(jQuery('.highlight:first').position().top - 150);
        }
        // find last highlighted and scroll to it
        function findLast() {
            jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(0);
            jQuery("#ContentPlaceHolder1_UserGuideDiv").scrollTop(jQuery('.highlight:last').position().top - 150);
        }
    </script>
    <script type="text/javascript">
        jQuery.fn.highlight = function (pat) {
            function innerHighlight(node, pat) {
                var skip = 0;
                if (node.nodeType == 3) {
                    var pos = node.data.toUpperCase().indexOf(pat);
                    if (pos >= 0) {
                        var spannode = document.createElement('span');
                        spannode.className = 'highlight';
                        var middlebit = node.splitText(pos);
                        var endbit = middlebit.splitText(pat.length);
                        var middleclone = middlebit.cloneNode(true);
                        spannode.appendChild(middleclone);
                        middlebit.parentNode.replaceChild(spannode, middlebit);
                        skip = 1;
                    }
                }
                else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
                    for (var i = 0; i < node.childNodes.length; i++) {
                        i += innerHighlight(node.childNodes[i], pat);
                    }
                }
                return skip;
            }
            return this.each(function () {
                innerHighlight(this, pat.toUpperCase());
            });
        };

        jQuery.fn.removeHighlight = function () {
            function newNormalize(node) {
                for (var i = 0, children = node.childNodes, nodeCount = children.length; i < nodeCount; i++) {
                    var child = children[i];
                    if (child.nodeType == 1) {
                        newNormalize(child);
                        continue;
                    }
                    if (child.nodeType != 3) { continue; }
                    var next = child.nextSibling;
                    if (next == null || next.nodeType != 3) { continue; }
                    var combined_text = child.nodeValue + next.nodeValue;
                    new_node = node.ownerDocument.createTextNode(combined_text);
                    node.insertBefore(new_node, child);
                    node.removeChild(child);
                    node.removeChild(next);
                    i--;
                    nodeCount--;
                }
            }

            return this.find("span.highlight").each(function () {
                var thisParent = this.parentNode;
                thisParent.replaceChild(this.firstChild, this);
                newNormalize(thisParent);
            }).end();
        };
    </script>
    <style type="text/css">
        .highlight
        {
            background-color: #fff34d;
            -moz-border-radius: 5px; /* FF1+ */
            -webkit-border-radius: 5px; /* Saf3-4 */
            border-radius: 5px; /* Opera 10.5, IE 9, Saf5, Chrome */
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* FF3.5+ */
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Saf3.0+, Chrome */
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Opera 10.5+, IE 9.0 */
        }
        .highlight
        {
            padding: 1px 4px;
            margin: 0 -4px;
        }
    </style>
</asp:Content>

 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="margin-top: 4px">
        <div id="myModal" class="modal" style="margin-left: 10px">
            <span class="close" onclick="Close();">&times;</span>
            <img class="modal-content" id="img01" style="padding-left: 265px; width: 96%; max-width: 62%;
                height: 82%;">
            <div id="caption">
            </div>
        </div>
        <asp:Button ID="btnExport" Style="margin-left: 30px" class="btn btn-primary" runat="server"
            Text="Export Pdf" OnClick="btnExport_Click" Height="32px" />
        Search:
        <input type="text" class="SearchCSS" id="text-search" placeholder="Enter your text" />
        <span id="spanButton">
            <input type="button" id="btnFirst" onclick="findFirst();" value="First" />
            <input type="button" id="btnNext" onclick="findNext();" value="Next" />
            <input type="button" id="btnPrev" onclick="findPrev();" value="Prev" />
            <input type="button" id="btnLast" onclick="findLast();" value="Last" />
        </span>
        <asp:Label ID="lblAuthority" runat="server" ClientIDMode="Static" Style="display: none;"></asp:Label>
        <asp:Label ID="lblSeachCount" runat="server" ClientIDMode="Static"></asp:Label>
    </div>
    <span style="font-size: 30px; cursor: pointer; margin-left: 92%;" onclick="openNav()">
        GoTo&#9776;</span>
    <div style="position: relative;">
        <div runat="server" id="UserGuideDiv" style="margin-left: 50px; padding-top: 8px;
            padding-left: 20px; margin-top: 10px; overflow-y: auto; max-height: 746px; border: 1px solid #000;
            position: relative;" class="userGuideDiv">
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <div id="mySidenav" style="width: 0px" class="sidenav">
            <div style="position: sticky; width: 250px; background: #111; top: 0;">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.."
                    style="margin-left: 4px; margin-top: 30px;" title="Type in a name" />
            </div>
            <ul id="maincontents" runat="server" style="padding-bottom: 100%">
            </ul>
        </div>
    </div>
</asp:Content>
