﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DocCMS.Core;
using System.IO;
using DocCMS.Core.DataTypes;
using DocCMS.Core.Implementation;

namespace DocCMSMain.cadmin
{
    public partial class MediaManagementPage : System.Web.UI.Page
    {
        public Int32 id { get; set; }
        public string Userid { get; set; }
        protected static string strwordRemoved = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["linkname"] = "licontent";
                Session["Ulname"] = "ulcontent";
                hdnUserID.Value = Convert.ToString(Session["UserID"]);
                if (Request.QueryString["tid"] != null)
                {
                    labtid.Text = Convert.ToInt32(Request.QueryString["tid"]).ToString();
                    set_page_headings();
                    BindFileTypeDropDown();
                    drpFileType.SelectedValue = labtid.Text.ToString();

                }
                if (Request.QueryString["id"] != null)
                {
                    id = Convert.ToInt32(Request.QueryString["id"]);
                    hdnCurPageID.Value = Convert.ToString(Request.QueryString["id"]);
                    hdnCurCMSID.Value = Convert.ToString(Request.QueryString["tid"]);
                    Set_Data();
                    btnEdit.Visible = true;
                    btnSubmit.Visible = false;
                    if (imgLogo.ImageUrl.ToLower().IndexOf(".gif") > -1)                    
                    imgEdit.Visible = false;                    
                }
                else
                {
                    //==  DO NOTHING ===//
                    imgEdit.Visible = false;
                    btnEdit.Visible = false;
                    btnSubmit.Visible = true;
                }

            }
        }
        private void set_page_headings()
        {
            StringBuilder query = new StringBuilder();

            Int32 tid = Convert.ToInt32(labtid.Text.ToString());
            if (tid > 0)
            {
                switch (tid)
                {
                    case 1:
                        h5heading1.InnerHtml = "Media Image Files";
                        h1heading1.InnerHtml = "Manage Media Image Files";
                        labheading1.InnerText = "Media Image Files";
                        labheading2.InnerText = "Manage Media Image Files";
                        h4heading1.InnerHtml = "Media Image Details";
                        pmediatype.Visible = false;
                        PImage.Visible = true;
                        FileUploadControl.Visible = false;
                        ImageUploadControl.Visible = true;
                        break;

                    case 4:
                        h5heading1.InnerHtml = "Documents Files ";
                        h1heading1.InnerHtml = "Manage Documents Files ";
                        labheading1.InnerText = "Documents Files ";
                        labheading2.InnerText = "Manage Documents Files ";
                        h4heading1.InnerHtml = "Document File Details";
                        pmediatype.Visible = false;
                        PImage.Visible = false;
                        ImageUploadControl.Visible = false;
                        FileUploadControl.Visible = true;
                        break;
                }
            }
        }

        private void BindFileTypeDropDown()
        {

            DataTable dt = new DataTable();

            Int32 tid = Convert.ToInt32(labtid.Text.ToString());
            if (tid == 2 || tid == 3)
            {
                dt = ServicesFactory.DocCMSServices.Fetch_Audio_Video_Filecategory_All();

            }
            else
            {
                dt = ServicesFactory.DocCMSServices.Fetch_File_category_All();
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.Rows.Add(new Object[] { 0, "Select File Type" });
                }
            }
            dt.DefaultView.Sort = "CategoryID";
            drpFileType.DataTextField = "CategoryName";
            drpFileType.DataValueField = "CategoryID";
            drpFileType.DataSource = dt;
            drpFileType.DataBind();
        }

        private void Set_Data()
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
                if (id > 0)
                {
                    ppreviousfile.Visible = true;
                    DataTable Dt = ServicesFactory.DocCMSServices.Fetch_Media_Files_By_id(id.ToString());
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        if (Convert.ToInt32(Dt.Rows[0]["FileCategoryID"]) == 2)
                        {
                            pmediatype.Visible = true;
                        }
                        labtid.Text = Convert.ToInt32(Dt.Rows[0]["FileCategoryID"]).ToString();
                        drpFileType.SelectedValue = Convert.ToString(Dt.Rows[0]["FileCategoryID"]);
                        labfilename.Text = Convert.ToString(Dt.Rows[0]["Filename"]);
                        string serverpath = @"../UploadedFiles/image/" + labfilename.Text.Replace(" ","");
                        imgLogo.ImageUrl = serverpath;
                        txtalternatetext.Text = Convert.ToString(Dt.Rows[0]["AlternateText"]);
                        txtdescription.Text = Convert.ToString(Dt.Rows[0]["Description"]);
                        DataTable dtCmsId = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(id);
                        hdnCurPageID.Value = Convert.ToString(id);
                        {
                            if(dtCmsId!=null && dtCmsId.Rows.Count>0)
                            {
                                hdnCurCMSID.Value = Convert.ToString(dtCmsId.Rows[0]["CMSID"]);
                                hdnCurPageID.Value=Convert.ToString(id);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(Dt.Rows[0]["VideoLink"])))
                        {
                            txtvideolink.Text = Convert.ToString(Dt.Rows[0]["VideoLink"]);
                        }
                    }
                }
            }
        }




        protected void btnsubmit_click(Object sender, EventArgs e)
        {

            try
            {
                String ext = "";
                if (labtid.Text.ToString() == "1")
                {

                    if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                    {
                        ext = System.IO.Path.GetExtension(ImageUploadControl.PostedFile.FileName);

                        if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                        {
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only Image file are allowed');</script>");
                            return;
                        }

                        if (!checkfileExsist(ImageUploadControl.PostedFile.FileName.ToString(), labtid.Text))
                        {
                            //=== do nothing 
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('A File with the same name already exists.! Please try again by renaming the file');", true);
                            return;
                        }

                    }
                    else
                    {
                        labfileformat.Text = "Please Select a file to upload.";
                        labfileformat.Visible = true;
                        return;
                    }
                    string ImageName = ImageUploadControl.PostedFile.FileName.Replace(ext,"");
                    string Uploadpath = "../UploadedFiles/image/" + ImageName;
                    string fname = "";
                    Clsfileuploader fileUploader = new Clsfileuploader();
                    fileUpload(1);
              
                }
                else if (labtid.Text.ToString() == "4")
                {
                    if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                    {

                        ext = System.IO.Path.GetExtension(FileUploadControl.PostedFile.FileName);
                        if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only document file are allowed');</script>");
                            return;
                        }
                        else
                        {
                        }

                        if (!checkfileExsist(FileUploadControl.PostedFile.FileName.ToString(), labtid.Text))
                        {
                            //=== do nothing 
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('A File with the same name already exists.! Please try again by renaming the file');", true);
                            return;
                        }

                    }
                    else
                    {
                        labfileformat.Text = "Please Select a file to upload.";
                        labfileformat.Visible = true;
                        return;
                    }
                    string ImageName = ImageUploadControl.PostedFile.FileName + "_Org".ToString();
                    string Uploadpath = "../UploadedFiles/document/" + ImageName;
                    fileUpload(1);
                }

                Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert(' Error in File Insertion !Please try again');", true);
                return;

            }
        }

        //============= SAMPLE CODE
        protected Int32 checkSection508Compliance(string fileText, string altText)
        {
            try
            {
                strwordRemoved = "";
                string[] altTextArray = altText.Split(' ');
                Int32 retFlag = 0;
                if (altTextArray.Length < 5)
                {
                    retFlag = 1;
                }
                else
                {
                    for (Int32 arrayStart = 0; arrayStart < altTextArray.Length; arrayStart++)
                    {
                        if (fileText.Contains(altTextArray[arrayStart]))
                        {
                            strwordRemoved = strwordRemoved + "," + altTextArray[arrayStart];
                            retFlag = 2;
                        }
                    }
                }

                return retFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // END SAMPLE CODE
        // END Code For Section 508 Compliance
        protected void btnreset_click(Object sender, EventArgs e)
        {
            reset_form();
        }
        protected void reset_form()
        {
            txtdescription.Text = "";
            txtalternatetext.Text = "";
            pmediatype.Visible = false;
            ppreviousfile.Visible = false;
        }
        protected void btnCancel_click(Object sender, EventArgs e)
        {
            Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
        }
        protected void btnEdit_click(Object sender, EventArgs e)
        {

            try
            {
                String ext = "";
                string Uploadpath = "";
                if (labtid.Text.ToString() == "1")
                {

                    if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                    {
                        ext = System.IO.Path.GetExtension(ImageUploadControl.PostedFile.FileName);

                        if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                        {


                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only Image file are allowed');</script>");
                            return;

                        }
                        if (!checkfileExsist_Edit(ImageUploadControl.PostedFile.FileName.ToString(), Convert.ToInt32(Request.QueryString["id"]), Convert.ToInt32(labtid.Text)))
                        {
                            //=== do nothing 
                        }


                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('File Already Exsist with Another ID.! Please try again with another file name');", true);
                            return;
                        }

                    }
                    else
                    {
                        Uploadpath = "../UploadedFiles/image/" + labfilename.Text;
                    }

                    if (!string.IsNullOrEmpty(labfilename.Text) && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                    {
                        if (File.Exists(Server.MapPath(@"../UploadedFiles/image/" + labfilename.Text)))
                        {
                            File.Delete(Server.MapPath(@"../UploadedFiles/image/" + labfilename.Text));
                        }
                        fileUpload(2);
                    }
                    else
                    {
                        fileUpload(2);
                    }
                    Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
                }
                else if (labtid.Text.ToString() == "4")
                {
                    if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                    {

                        ext = System.IO.Path.GetExtension(FileUploadControl.PostedFile.FileName);
                        if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".gif" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".bmp")
                        {

                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only document file are allowed');</script>");
                            return;


                        }
                        else
                        {


                        }


                        if (!checkfileExsist_Edit(FileUploadControl.PostedFile.FileName.ToString(), Convert.ToInt32(Request.QueryString["id"]), Convert.ToInt32(labtid.Text)))
                        {
                            //=== do nothing 
                        }
                        else
                        {

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('File Already Exsist with Another ID.! Please try again with another file name');", true);
                            return;
                        }

                    }
                    else
                    {
                        Uploadpath = "../UploadedFiles/image/" + labfilename.Text;
                    }

                    if (!string.IsNullOrEmpty(labfilename.Text) && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                    {
                        if (File.Exists(Server.MapPath(@"../UploadedFiles/document/" + labfilename.Text)))
                        {
                            File.Delete(Server.MapPath(@"../UploadedFiles/document/" + labfilename.Text));
                        }
                        fileUpload(2);
                    }
                    else
                    {
                        fileUpload(2);
                    }
                    Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());

                }

                Response.Redirect("../cadmin/MediaManagementDashboard.aspx?tid=" + labtid.Text.ToString());
            }
            catch
            {

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('Error in File Updation !Please try again');", true);
                return;

            }



        }

        public void fileUpload(int Mode)
        {
            string fname = "";
            string ext;
            Clsfileuploader fileUploader = new Clsfileuploader();

            try
            {
                if (labtid.Text.ToString() == "1")
                {
                    ext = System.IO.Path.GetExtension(ImageUploadControl.PostedFile.FileName);
                    if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName))
                    {
                        fname = ImageUploadControl.PostedFile.FileName.Replace(ext, "") + "_Org" + ext;
                        fname = fname.Replace(" ", "");
                        string serverpath = Server.MapPath("../UploadedFiles/image");
                        DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                        bool isDirCreated = directoryInfo.Exists;
                        if (!isDirCreated)
                        {
                            directoryInfo.Create();
                        }
                        ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fname);
                    }
                    else
                    {
                        fname = labfilename.Text;
                    }
                    fileUploader.filepath = @"../UploadedFiles/image" + @"/";
                }
                else if (labtid.Text.ToString() == "4")
                {

                    if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                    {
                        fname = FileUploadControl.PostedFile.FileName;
                        string serverpath = Server.MapPath("../UploadedFiles/document");
                        DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                        bool isDirCreated = directoryInfo.Exists;
                        if (!isDirCreated)
                        {
                            directoryInfo.Create();
                        }
                        FileUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fname);

                    }
                    else
                    {
                        fname = labfilename.Text;
                    }
                    fileUploader.filepath = @"../UploadedFiles/document" + @"/";
                }
                if (!string.IsNullOrEmpty(fname))
                {
                    fileUploader.filename = fname;
                    fileUploader.creationdate = DateTime.Now.ToString();
                    if (pmediatype.Visible)
                    {
                        labtid.Text = Convert.ToInt32(drpFileType.SelectedValue).ToString();
                    }
                    fileUploader.filecategoryid = labtid.Text.ToString();
                    fileUploader.modificationdate = DateTime.Now.ToString();
                    fileUploader.alternatetext = Convert.ToString(txtalternatetext.Text);
                    fileUploader.description = Convert.ToString(txtdescription.Text);
                    //==== Memeber ship user =====//

                    if (Session["UserID"] != null)
                    {
                        Userid = Convert.ToString(Session["UserID"]);
                    }
                    fileUploader.uploadedby = Userid.ToString();
                    if (Mode == 1)
                    {
                        ServicesFactory.DocCMSServices.Insert_MediaFiles(fileUploader);
                    }
                    if (Mode == 2)
                    {
                        if (Request.QueryString["id"] != null)
                        {
                            id = Convert.ToInt32(Request.QueryString["id"]);
                        }
                        fileUploader.id = id.ToString();
                        ServicesFactory.DocCMSServices.Update_MediaFiles(fileUploader);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('Please select file first !');", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                
            }

        }

        private bool checkfileExsist(string filename, string ID)
        {
            bool retval = false;
            DataTable dt = ServicesFactory.DocCMSServices.Fetch_Media_Files_By_name(filename, Convert.ToInt32(ID));
            if (dt != null && dt.Rows.Count > 0)
            {
                retval = true;
            }
            else
            {
                retval = false;
            }

            return retval;
        }


        private bool checkfileExsist_Edit(string filename, Int32 id, Int32 FileCategoryID)
        {
            bool retval = false;
            DataTable dt = ServicesFactory.DocCMSServices.Fetch_Media_Files_By_name_and_id(filename, id, FileCategoryID);
            if (dt != null && dt.Rows.Count > 0)
            {
                retval = true;
            }
            else
            {
                retval = false;
            }

            return retval;
        }

        public void convert_VideoFile(string Filetoconvert, string Filepath)
        {
            try
            {
                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                ffMpeg.ConvertMedia(Filetoconvert, Filepath + @"\\" + "1.ogg", NReco.VideoConverter.Format.ogg);

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


    }//=== CLASS ENDS HERE ====
}