﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="Privileges.aspx.cs" Inherits="DocCMSMain.cadmin.Privileges" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="CadminFooter" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
     <script type="text/javascript">
         function onlyNumbers(e) {
             var key;
             if (window.event) {
                 key = window.event.keyCode;     //IE
             }
             else {
                 key = e.which;      //firefox              
             }
             if (key == 0 || key == 8) {
                 return true;
             }
             if (key > 31 && (key < 48 || key > 57))
                 return false;
             //             
             return true;
         }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
       <ul class="breadcrumbs">
            <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li><a href="#">Privilege</a> <span class="separator"></span></li>
            <li>Manage Privilege</li>
      </ul>
         <div class="pageheader">
            <form action="" method="post" class="searchbar">
            </form>
            <div class="pageicon"><span class="iconfa-pencil"></span></div>
            <div class="pagetitle">
                <h5>Privilege</h5>
                <h1>Manage Privilege</h1>
            </div>
         </div><!--pageheader-->
         <div class="maincontent">
            <div class="maincontentinner">
             <div class="widget">
                <h4 class="widgettitle">Privilege Details</h4>
                <div class="widgetcontent">
                  <div class="inputwrapper login-alert" id="msgDiv" runat="server" >
                    <div class="alert alert-error" style="height:25px;" align="center"><label id="labmsg" name="labmsg" runat="server" style="margin-top:1px;color:#DA5251;"></label></div>
                </div>
                <div class="stdform">
                           <div class="par control-group">
                                    <label class="control-label" for="txtPrivilegeName">Privilege Name</label>
                                <div class="controls">
                                <input type="text" runat="server" name="txtPrivilegeName" id="txtPrivilegeName" class="input-large" clientidmode="Static" placeholder="Enter Privilege Name"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit" ControlToValidate="txtPrivilegeName" runat="server" ForeColor="Red">*</asp:RequiredFieldValidator>
                           </div>
                           </div>
                            <div class="par control-group">
                                    <label class="control-label" for="txtDisplayOrder">Display Order</label>
                                   <div class="controls"><input type="text" runat="server" name="txtDisplayOrder" id="txtDisplayOrder" onkeypress="return onlyNumbers(event)" class="input-large" placeholder="Enter Order Number" />
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="grpsubmit" ControlToValidate="txtDisplayOrder" runat="server" ForeColor="Red">*</asp:RequiredFieldValidator>
                                  </div>
                            </div>
                           <div class="par control-group">
                                    <label class="control-label" for="ChkActiveStatus">Active Status</label>
                                <div class="controls"><input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus"  class="input-large" style="opacity:1" checked/> </div>
                            </div>
                          <p class="stdformbutton">
                       <asp:Button ID="btnsubmit" runat="server"  class="btn btn-primary" Visible="false" 
                        onclick="btnsubmit_onclick"   Text="Submit" Width="150px" ValidationGroup="grpsubmit" />
                       <asp:Button ID="btnupdate" runat="server"  class="btn btn-primary" Visible="false" 
                        onclick="btnupdate_onclick"   Text="Update" Width="150px" ValidationGroup="grpsubmit"/>
                       <asp:Button ID="btnCancel" runat="server"  class="btn" Text="Cancel" Width="150px" onclick="btnCancel_Click"/>
                        </p>
                        </div>
                </div><!--widgetcontent-->
            </div><!--widget-->
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            </div><!--maincontentinner-->
        </div><!--maincontent-->
</asp:Content>
