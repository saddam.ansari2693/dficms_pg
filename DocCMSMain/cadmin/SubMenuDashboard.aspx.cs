﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class SubMenuDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 pageID = 0;
            if (Request.QueryString["PID"] != null)
            {
                pageID = Convert.ToInt32(ServicesFactory.DocCMSServices.Fetch_Page_id_By_name(Convert.ToString(Request.QueryString["PID"])));
                if (!IsPostBack)
                {
                    Bind_Dashboard_Content(pageID);
                }
            }
        }
        // Bind Dashboard Content Function
        protected void Bind_Dashboard_Content(Int32 PageID)
        {
            DataTable dtContent = ServicesFactory.DocCMSServices.Fetch_CMS_Dashboard(PageID);
            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                RptrContentPage.DataSource = dtContent;
                RptrContentPage.DataBind();
            }
        }


        protected void RptrContentPage_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                if (e.CommandName == "EditPageContent")
                {
                    Response.Redirect("~/cadmin/ContentManagement.aspx?" + e.CommandArgument);
                }
                if (e.CommandName == "DeletePageContent")
                {
                    // Do nothing
                }
            }
        }
        protected void RptrContentPage_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                LinkButton lblContentTitle = (LinkButton)e.Item.FindControl("lblContentTitle");
                Label lnkPageName = (Label)e.Item.FindControl("lnkPageName");
                ImageButton lnkEdit = (ImageButton)e.Item.FindControl("lnkEdit");
                ImageButton lnkDelete = (ImageButton)e.Item.FindControl("lnkDelete");

                if (lblCMSID != null && lblPageID != null && lblContentTitle != null && lnkPageName != null && lnkEdit != null && lnkDelete != null)
                {
                    lblContentTitle.Text = ServicesFactory.DocCMSServices.Fetch_Content_Title(Convert.ToInt32(lblCMSID.Text));
                    lnkEdit.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                    lnkDelete.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                    lblContentTitle.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                }
            }
        }

        protected void btnAddContentPageBottom_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["PID"] != null)
            {
                Response.Redirect("~/cadmin/ContentManagement.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]));
            }
        }
    }
}