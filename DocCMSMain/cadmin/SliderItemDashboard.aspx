﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SliderItemDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.SliderItemDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            SettitleOption();
            jQuery('#titleOption1').change(function () {
                jQuery('.linLeftImage').hide();
                jQuery('.linFullImage').hide();
                jQuery('.linOrgImage').show();
                jQuery('#hdnSliderImageType').val("ORG");
            });

            jQuery('#titleOption2').change(function () {
                jQuery('.linLeftImage').hide();
                jQuery('.linOrgImage').hide();
                jQuery('.linFullImage').show();
                jQuery('#hdnSliderImageType').val("FULLWIDTH");
            });
            jQuery('#titleOption3').change(function () {
                jQuery('.linOrgImage').hide();
                jQuery('.linFullImage').hide();
                jQuery('.linLeftImage').show();
                jQuery('#hdnSliderImageType').val("LEFT_");
            });

            jQuery('#OriginalImage').change(function () {
                var ImageSource = jQuery("#imgPanelPreview").attr("src");
                ImageSource = ImageSource.toString().trim().toLowerCase().replace("left", "org");
                ImageSource = ImageSource.toString().trim().toLowerCase().replace("fullwidth", "org");
                jQuery("#imgPanelPreview").attr("src", ImageSource);

            });

            jQuery('#FullImage').change(function () {
                var ImageSource = jQuery("#imgPanelPreview").attr("src");
                ImageSource = ImageSource.toString().trim().toLowerCase().replace("org", "fullwidth");
                ImageSource = ImageSource.toString().trim().toLowerCase().replace("left", "fullwidth");
                jQuery("#imgPanelPreview").attr("src", ImageSource);
            });

            jQuery('#LeftImage').change(function () {
                var ImageSource = jQuery("#imgPanelPreview").attr("src");
                ImageSource = ImageSource.toString().trim().toLowerCase().replace("org", "left");
                ImageSource = ImageSource.toString().trim().toLowerCase().replace("fullwidth", "left");
                jQuery("#imgPanelPreview").attr("src", ImageSource);
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
        function SettitleOption() {
            var titleOption = '<div style="position:absolute;top:15px;left:200px;width:300px;">' +
                                      '<input type="radio" id="titleOption1" name="titleOption" checked="checked" value="Original">&nbsp;Original' +
                                      '&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="titleOption2" name="titleOption" value="Full Width">&nbsp;Full Width' +
                                      '&nbsp;&nbsp;<input type="radio" id="titleOption3" name="titleOption" value="Left/Right">&nbsp;Left/Right' +
                                      '</div>';

            jQuery(titleOption).insertAfter('#dyntable_length');
        }
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        function PreviewImage(ctrlID) {
            var imagesource = jQuery("#" + ctrlID).attr("src");
            jQuery("#ContentPlaceHolder1_myModalLabel span").removeClass("checked");
            if (imagesource.toString().trim().toLowerCase().indexOf("org") > -1) {
                jQuery("#uniform-OriginalImage >span").addClass("checked");
            }
            else if (imagesource.toString().trim().toLowerCase().indexOf("fullwidth") > -1) {
                jQuery("#uniform-FullImage >span").addClass("checked");
            }
            else {
                jQuery("#uniform-LeftImage  >span").addClass("checked");
            }
            jQuery("#imgPanelPreview").attr("src", "");
            jQuery("#imgPanelPreview").attr("src", jQuery("#" + ctrlID).attr("src"));
            jQuery("#pnlImagePreview").show();
        }
        function ClosePreviewImage(ctrlID) {
            jQuery("#imgPanelPreview").attr("src", "");
            jQuery("#pnlImagePreview").hide();
        }
        // method to set Session for showing data in data table
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {

                }
            });
        }       
    </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;

            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSliderImageType" runat="server" ClientIDMode="Static" Value="ORG" />
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage slider</a> <span class="separator"></span></li>
        <li>Slider Item Dashboard</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage slider</h5>
            <h1>
                Slider Item Dashboard</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                <!--Filer by Status starts here--->
                <div class="par_ctrl_sec">
                    <div class="par control-group">
                        <label class="control-label" for="ddlStatus">
                            Filer by Status</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                                <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!--Filer by Status ends here--->
                Slider Item Dashboard
                <asp:HiddenField ID="hndSliderID" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnBack" runat="server" Width="100" Text="Back" CssClass="btn" Style="float: right;
                    margin-top: -5px;" OnClick="btnBack_Click" />
                <asp:Button ID="btnAddSliderImage" runat="server" CssClass="btn btn-primary" Width="150"
                    Text="Add New Slider Image" Style="float: right; margin-top: -5px;" OnClick="btnAddSliderImage_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 4%;" />
                    <col class="con1" style="text-align: center; width: 20%;" />
                    <col class="con1" style="text-align: center; width: 25%;" />
                    <col class="con1" style="width: 5%;" />
                    <col class="con1" style="width: 10%;" />
                    <col class="con1" style="align: center; width: 5%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Sr. No.
                        </th>
                        <th class="head1" style="text-align: center;">
                            Image Name
                        </th>
                        <th class="head1" style="text-align: center;">
                            Preview
                        </th>
                        <th class="head1">
                            Display Order
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                          <center>
                             Functions
                          </center>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrPlans" OnItemCommand="RptrPlans_ItemCommand"
                        OnItemDataBound="RptrPlans_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="lblSliderID" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderItemID") %>'
                                        Visible="false"></asp:Label>
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <asp:LinkButton ID="lnkImageName" runat="server" ClientIDMode="Static" Text='<%# Eval("ImageName") %>'
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="EditSlider"></asp:LinkButton>
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <img src='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/ORG_"+Eval("ImageName")%>'
                                        alt='<%# Eval("ImageName")%>' id="imgSliderItemOrg" runat="server" onclick="PreviewImage(this.id);"
                                        style="width: 100px; height: 100px; cursor: pointer;" class="linOrgImage" title="Click image to preivew" />
                                    <img src='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/LEFT_"+Eval("ImageName")%>'
                                        alt='<%# Eval("ImageName")%>' id="imgSliderItemLeft" runat="server" onclick="PreviewImage(this.id);"
                                        style="width: 80px; height: 60px; display: none; cursor: pointer;" class="linLeftImage"
                                        title="Click image to preivew" />
                                    <img src='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/FULLWIDTH_"+Eval("ImageName")%>'
                                        alt='<%# Eval("ImageName")%>' id="imgSliderItemFull" runat="server" onclick="PreviewImage(this.id);"
                                        style="width: 120px; height: 80px; display: none; cursor: pointer;" class="linFullImage"
                                        title="Click image to preivew" />
                                    <asp:HiddenField ID="hdnSliderItem" runat="server" Value='<%#"../UploadedFiles/"+Eval("SliderName")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>' />
                                </td>
                                <td style="vertical-align: middle;">
                                    <asp:Label runat="server" Text='<%# Eval("Dorder")%>' ID="lblheight"></asp:Label>
                                </td>
                                <td style="vertical-align: middle;">
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblwidth"></asp:Label>
                                </td>
                                <td class="center" style="vertical-align: middle;">
                                    <asp:ImageButton runat="server" ID="lnkNavigate" ImageUrl="~/images/icon-add.png"
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="AddSlide" ToolTip="Add Slides" />
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="EditSlider" ToolTip="Edit Slider" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="DeleteSlider" ToolTip="Delete Slider"
                                        OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:Label ID="lblSliderID" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderItemID") %>'
                                        Visible="false"></asp:Label>
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td style="text-align: center;">
                                    <asp:LinkButton ID="lnkImageName" runat="server" ClientIDMode="Static" Text='<%# Eval("ImageName") %>'
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="EditSlider"></asp:LinkButton>
                                </td>
                                <td style="text-align: center;">
                                    <img src='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/ORG_"+Eval("ImageName")%>'
                                        alt='<%# Eval("ImageName")%>' id="imgSliderItemOrg" runat="server" onclick="PreviewImage(this.id);"
                                        style="width: 100px; height: 100px; cursor: pointer;" class="linOrgImage" title="Click image to preivew" />
                                    <img src='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/LEFT_"+Eval("ImageName")%>'
                                        alt='<%# Eval("ImageName")%>' id="imgSliderItemLeft" runat="server" onclick="PreviewImage(this.id);"
                                        style="width: 80px; height: 60px; display: none; cursor: pointer;" class="linLeftImage"
                                        title="Click image to preivew" />
                                    <img src='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/FULLWIDTH_"+Eval("ImageName")%>'
                                        alt='<%# Eval("ImageName")%>' id="imgSliderItemFull" runat="server" onclick="PreviewImage(this.id);"
                                        style="width: 120px; height: 80px; display: none; cursor: pointer;" class="linFullImage"
                                        title="Click image to preivew" />
                                    <asp:HiddenField ID="hdnSliderItem" runat="server" Value='<%#"../UploadedFiles/"+Eval("SliderName")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>' />
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Dorder")%>' ID="lblheight"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblwidth"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkNavigate" ImageUrl="~/images/icon-add.png"
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="AddSlide" ToolTip="Add Slides" />
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="EditSlider" ToolTip="Edit Slider" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SliderItemID")%>' CommandName="DeleteSlider" ToolTip="Delete Slider"
                                        OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
            <div id="pnlImagePreview" class="modal fade in" style="display: none; text-align: center;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <div id="myModalLabel" class="modal-title" runat="server" style="color: #FFF;">
                                <input type="radio" id="OriginalImage" name="PanelPreview" value="ORG" /><label for="OriginalImage"
                                    style="display: inline-block;">Original</label>
                                <input type="radio" id="FullImage" name="PanelPreview" value="FullWidth" />&nbsp;<label
                                    for="FullImage" style="display: inline-block;">Full Width</label>
                                <input type="radio" id="LeftImage" name="PanelPreview" value="Left" />&nbsp;<label
                                    for="LeftImage" style="display: inline-block;">Left/Right</label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <img style="height: auto; width: auto;" id="imgPanelPreview" alt="" />
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnCloseModel" class="btn" onclick="ClosePreviewImage();"
                                title="Close Preview Window" value="Close Preview" />
                        </div>
                        <!--maincontentinner-->
                        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
                            role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
                            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--maincontentinner-->
    </div>
    <style type="text/css">
        .closePreview
        {
            border: 1px solid Green;
            width: 20px;
            color: #000;
            background: #fff;
            position: absolute;
            font-weight: 400;
            border-radius: 4px;
        }
        .closePreview:hover
        {
            border: 1px solid Green;
            width: 20px;
            color: #fff;
            background: Red;
            position: absolute;
            font-weight: 400;
            border-radius: 4px;
        }
    </style>
    <!--maincontent-->
</asp:Content>
