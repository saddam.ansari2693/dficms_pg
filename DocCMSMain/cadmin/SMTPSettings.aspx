﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SMTPSettings.aspx.cs" Inherits="DocCMSMain.cadmin.SMTPSettings" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8) {
                return true;
            }
            if (key > 31 && (key < 48 || key > 57))
                return false;
                       
            return true;
        }
         function SaveMsg(Alias) {
            alert("SMTP Detail "+Alias+" Suceessfully");
            var url = "";
                url = "../cadmin/SMTPSettings.aspx";
                $(location).attr('href', url);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">SMTP Settings</a> <span class="separator"></span></li>
        <li>Manage SMTP Settings</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                SMTP Settings</h5>
            <h1>
                Manage SMTP Settings</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent" id="DivSurveyContent" runat="server">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <div class="widget">
                <h4 class="widgettitle">
                    SMTP Settings</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                   <div class="stdform" id="DivSMTPPage" runat="server">
                     <div class="par control-group">
                            <label class="control-label" for="txthost">
                                SMTP Server</label>
                            <div class="controls">
                               <asp:TextBox ID="txthost" runat="server" CssClass="input-large" 
                                    ClientIDMode="Static" ValidationGroup="grpsubmit" placeholder="Enter SMTP Server name "></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpsubmit"
                                    ControlToValidate="txthost" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                           
                            </div>
                        </div>
                      <div class="par control-group">
                            <label class="control-label" for="txtPort">
                                SMTP Port Number</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtPort" id="txtPort" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Port Number" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtPort" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                           
                            </div>
                        </div>
                 
                              <div class="par control-group">
                            <label class="control-label" for="txtSenderEmail">
                                Sender Email Address</label>
                            <div class="controls">
                                <asp:TextBox ID="txtSenderEmail" runat="server" CssClass="input-large" 
                                    ClientIDMode="Static" ValidationGroup="grpsubmit" placeholder="Enter Sender Email Address"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtSenderEmail" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter valid email Id"
                                    ControlToValidate="txtSenderEmail" style="color: Red; font-size: 12px;" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="grpsubmit"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                              <div class="par control-group">
                            <label class="control-label" for="txtPswd">
                                Sender Password</label>
                            <div class="controls">
                            <asp:Label ID="lblPassword" runat="server" ClientIDMode="Static" Visible="false"></asp:Label>
                                <input type="password" runat="server" name="txtPswd" id="txtPswd" class="input-large" autocomplete="off"
                                   placeholder="New Password" clientidmode="Static" />
                                   <asp:Label ID="lblReqPass" runat="server" ClientIDMode="Static" Text="*" Visible="false"
                                   style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;"></asp:Label>
                               <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtPswd"
                                    ID="RegularExpressionValidator3" ValidationExpression="^[\s\S]{7,}$" ValidationGroup="grpsubmit"
                                    runat="server" ErrorMessage="Password must be 7 characters long." style="color: Red; font-size: 12px;"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                            <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus" style="margin-top:-10px;">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large" clientidmode="Static"
                                    style="opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group" id="DivSMTP" runat="server">
                            <label class="control-label" for="tblFeature">
                                SMTP Email Details</label>
                            <div class="controls">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ClientIDMode="Static">
                                    <ContentTemplate>
                                        <table id="tblSMTP" class="table table-bordered responsive " style="margin-left: 150px;
                                            width: 90%;">
                                            <colgroup>
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                            </colgroup>
                                            <thead id="tblSMTPDetailhead" runat="server">
                                                <tr>
                                                    <th class="head1" style="height: 10px;">
                                                        <div style="display: inline; cursor: pointer;" class="AddFeature" id="AddFeature"
                                                            runat="server" onclick="return AddQues();">
                                                            <asp:ImageButton ID="ImgAddSMTPDetail" runat="server" ClientIDMode="Static" ImageUrl="../images/plus-icon.png"
                                                                ToolTip="Add Email Detail" OnClick="ImgAddSMTPDetail_Click" />

                                                           <span style="margin-top:10px; "> Add Email Detail</span></div>
                                                        <label id="lblQuestion" runat="server" clientidmode="Static" visible="false" style="text-align: left;">
                                                          S.No</label>
                                                    </th>
                                                    <th class="head1" style="height: 10px;">
                                                      Department
                                                    </th>
                                                    <th class="head1" style="height: 10px;">
                                                      To Email
                                                    </th>
                                                     <th class="head1" style="height: 10px;">
                                                     CC
                                                    </th>
                                                     <th class="head1" style="height: 10px;">
                                                    Bcc
                                                    </th>
                                                    <th class="head1" style="height: 10px;">
                                                        Status
                                                    </th>
                                                    <th class="head1" style="height: 10px;">
                                                        Operations
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="tblSMTPBody" runat="server">
                                                <asp:Repeater ID="rptrSMTP" runat="server" ClientIDMode="Static" OnItemCommand="rptrSMTP_ItemCommand"
                                                    OnItemDataBound="rptrSMTP_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr class="gradeX">
                                                        <td>
                                                          <asp:Label ID="lblDorder" runat="server"  ></asp:Label>
                                                        </td>
                                                            <td>
                                                                <asp:LinkButton ID="LnkDepatment" CommandName="EditEmail"   CommandArgument='<%# Eval("ID")%>'
                                                                    Text='<%# Eval("Department") %>' runat="server"></asp:LinkButton>
                                                                <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("Department")%>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblToEmail" runat="server" Text='<%# Eval("ToEmail") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCC" runat="server" Text='<%# Eval("CcEmail") %>'></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="lblBcc" runat="server" Text='<%# Eval("BccEmail") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                                                   CommandArgument='<%# Eval("ID")%>' CommandName="EditEmail" ToolTip="Edit Module" />
                                                               <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                                                   CommandArgument='<%# Eval("ID")%>' CommandName="DeleteSMTPDetail" ToolTip="Delete SMTP Detail" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr class="gradeA">
                                                           <td>
                                                          <asp:Label ID="lblDorder" runat="server"  ></asp:Label>
                                                        </td>
                                                            <td>
                                                                <asp:LinkButton ID="LnkDepatment" CommandName="EditEmail"   CommandArgument='<%# Eval("ID")%>'
                                                                    Text='<%# Eval("Department") %>' runat="server"></asp:LinkButton>
                                                                <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("Department")%>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblToEmail" runat="server" Text='<%# Eval("ToEmail") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCC" runat="server" Text='<%# Eval("CcEmail") %>'></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="lblBcc" runat="server" Text='<%# Eval("BccEmail") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                                                   CommandArgument='<%# Eval("ID")%>' CommandName="EditEmail" ToolTip="Edit Module" />
                                                               <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                                                   CommandArgument='<%# Eval("ID")%>' CommandName="DeleteSMTPDetail" ToolTip="Delete SMTP Detail" />
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                </asp:Repeater>
                                                <tr class="gradeX" id="trAdditionalRow" runat="server" clientidmode="Static" visible="false">
                                                <td>
                                                   <asp:TextBox ID="txtEmailDorder" placeholder="Enter Display Order" runat="server" onkeypress="return onlyNumbers(event)" readonly
                                                    style="width:90%;" ClientIDMode="Static"></asp:TextBox>
                                                </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpDepartment" runat="server" ClientIDMode="Static">
                                                            <asp:ListItem Text="Production" Value="Production"></asp:ListItem>
                                                            <asp:ListItem Text="Sales" Value="Sales"></asp:ListItem>
                                                            <asp:ListItem Text="Human Resource " Value="Human Resource"></asp:ListItem>
                                                             <asp:ListItem Text="Support " Value="Support"></asp:ListItem>
                                                              <asp:ListItem Text="Enquiry " Value="Enquiry"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                      <asp:TextBox ID="txtToEmail" runat="server" placeholder="Enter To Email Id" ClientIDMode="Static" ></asp:TextBox>
                                                      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter valid email Id"
                                                            ControlToValidate="txtToEmail" style="color: Red; font-size: 12px;" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ValidationGroup="ValidateEntry"></asp:RegularExpressionValidator>   
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCC" placeholder="Enter CC" runat="server"  
                                                            ClientIDMode="Static" ></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please enter valid email Id"
                                                            ControlToValidate="txtCC" style="color: Red; font-size: 12px;" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ValidationGroup="ValidateEntry"></asp:RegularExpressionValidator>   
                                                    </td>
                                                     <td>
                                                        <asp:TextBox ID="txtBcc" placeholder="Enter Bcc" runat="server" 
                                                            ClientIDMode="Static"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Please enter valid email Id"
                                                            ControlToValidate="txtBcc" style="color: Red; font-size: 12px;" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ValidationGroup="ValidateEntry"></asp:RegularExpressionValidator>
                                                    </td>
                                                  
                                                    <td>
                                                        <asp:CheckBox ID="chkEmailActive" runat="server" ClientIDMode="Static" Checked/>
                                                        Active
                                                    </td>
                                                   
                                                    <td>
                                                     <asp:ImageButton runat="server" ID="btnSaveSMTPDetail"  ImageUrl="~/cadmin/images/Button-Ok-icon.png" ToolTip="Save SMTP Emails" style="height:30px; width:30%;" 
                                                            onclick="btnSaveSMTPDetail_Click" ValidationGroup="ValidateEntry"/>
                                                        <asp:ImageButton runat="server" ID="btnUpdateSMTPDetail" ImageUrl="~/cadmin/images/Button-Ok-icon.png" style="height:30px; width:30%;"  ToolTip="Update SMTP Emails"    onclick="btnUpdateSMTPDetail_Click" ValidationGroup="ValidateEntry" />
                                                         <asp:ImageButton runat="server" ID="btnCancelSMTPDetail" 
                                                            ImageUrl="~/cadmin/images/button_cancel_256.png" 
                                                            ToolTip="Cancel SMTP Emails" style="height:30px; width:30%;" />
                                                
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                              
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <br />
                        <p class="stdformbutton">
                            <asp:Button ID="btnsubmitMaster" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnsubmitMaster_onclick" Text="Save" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnMasterupdate" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnMasterupdate_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit" />
                           <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                OnClick="btnCancel_Click" />
                      
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->

            <asp:HiddenField ID="hdnSMTPId" runat="server" />
                <asp:HiddenField ID="hdnSMTPDetailId" runat="server" />
                 <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            <!-- for Display Order Popup -->
        </div>
        <!--maincontentinner-->
        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
            role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
        </ul>
    </div>
    <!--maincontent-->
  
    <!----Question Content ends here--->
    <script language="javascript" type="text/javascript">

        function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
        }

        function AddQues() {
            var ImgAddQuestion = document.getElementById("ImgAddQuestion");
            ImgAddQuestion.click();
        }
        function AddAns() {
            var ImgAddAnswer = document.getElementById("ImgAddAnswer");
            ImgAddAnswer.click();
        }

        function RemoveFeature(elem) {
            var id = $(elem).attr("id");
            var GetId = id.toString().split('_');
            $("#divFeature_" + GetId[1]).style.visibility = "hidden";
        }
    </script>
    </asp:Content>