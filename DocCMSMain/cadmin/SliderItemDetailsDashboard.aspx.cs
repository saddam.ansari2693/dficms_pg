﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.Web.UI.HtmlControls;
namespace DocCMSMain.cadmin
{
    public partial class SliderItemDetailsDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";

            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["SliderItemID"] != null && Request.QueryString["SliderItemID"] != "")
                {
                    Bind_SliderDetails(Convert.ToInt32(Request.QueryString["SliderItemID"]));
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                }
            }
        }

        // Bind all Plan Details
        protected void Bind_SliderDetails(int SliderItemID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Slider_Details_Data_By_sliderid(SliderItemID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrSliderDetails.DataSource = dt;
                    RptrSliderDetails.DataBind();
                }
                else
                {
                    RptrSliderDetails.DataSource = "";
                    RptrSliderDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void RptrSliderDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditSlider":
                        Response.Redirect("SliderItemDetailsPage.aspx?SliderItemContentID=" + Convert.ToString(e.CommandArgument) + "&SliderItemID=" + Convert.ToString(Request.QueryString["SliderItemID"]) + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
                        break;
                    case "DeleteSlider":
                        Int32 retval = ServicesFactory.DocCMSServices.Delete_Slider_Details_By_slideritemcontentid(Convert.ToInt32(e.CommandArgument));
                        Bind_SliderDetails(Convert.ToInt32(Request.QueryString["SliderItemID"]));
                        break;

                    default:
                        break;
                }
            }
        }

        protected void RptrSliderDetails_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                HiddenField hdnSliderItemContentID = (HiddenField)e.Item.FindControl("hdnSliderItemContentID");
                HiddenField hdnSliderItemID = (HiddenField)e.Item.FindControl("hdnSliderItemID");
                Label lblItemType = (Label)e.Item.FindControl("lblItemType");
                Label lblItemContent = (Label)e.Item.FindControl("lblItemContent");
                Label lblImagename = (Label)e.Item.FindControl("lblImagename");
                HtmlImage imgSliderItem = (HtmlImage)e.Item.FindControl("imgSliderItem");
                HtmlImage imgSliderItemPreview = (HtmlImage)e.Item.FindControl("imgSliderItemPreview");
                HtmlGenericControl divControl = (HtmlGenericControl)e.Item.FindControl("divPreview");
                if (hdnSliderItemID != null && hdnSliderItemContentID != null && lblItemType != null && lblItemContent != null && imgSliderItem != null && divControl != null && lblImagename != null && imgSliderItemPreview != null)
                {
                    if (lblItemType.Text == "Image")
                    {
                        imgSliderItem.Visible = true;
                        lblItemContent.Visible = false;
                    }
                    else
                    {
                        imgSliderItemPreview.Visible = false;
                        imgSliderItem.Visible = false;
                        divControl.Visible = false;
                    }
                }
            }
        }
        protected void btnAddSlider_Click(object sender, EventArgs e)
        {
            Response.Redirect("SliderItemDetailsPage.aspx?SliderItemID=" + Convert.ToString(Request.QueryString["SliderItemID"]) + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("SliderItemDashboard.aspx?SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
        }
    }
}