﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class BlogDashboard : System.Web.UI.Page
    {
        public Int32 tid { get; set; }
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "licontent";
            Session["Ulname"] = "ulcontent";
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    Bind_Repeater();
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    Set_Controls();
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        //=========Bind Blogs Details============
        public void Set_Controls()
        {
            DataTable dt =ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubmoduleID"]),Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    Button1.Visible = true;
                }
                else 
                {
                    Button1.Visible = false;
                }
                foreach (RepeaterItem item in RepeaterContent.Items)
                {
                    LinkButton linktitle = (LinkButton)item.FindControl("linktitle");
                    Label lbltitle = (Label)item.FindControl("lbltitle");
                    ImageButton LinkEdit=(ImageButton)item.FindControl("LinkEdit");
                    ImageButton linkdelete = (ImageButton)item.FindControl("linkdelete");
                    HtmlGenericControl SpanDiv = (HtmlGenericControl)item.FindControl("SpanDiv");
                    Label lblActiveStatus = (Label)item.FindControl("lblActiveStatus");
                    ImageButton btnOrderUP = (ImageButton)item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)item.FindControl("btnOrderDown");
                    if (linktitle != null && lbltitle != null && LinkEdit != null && linkdelete != null && SpanDiv != null && lblActiveStatus != null && btnOrderDown != null && btnOrderUP != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            LinkEdit.Enabled = true;
                            linktitle.Visible = true;
                            lbltitle.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;
                            SpanDiv.Visible = true;
                            lblActiveStatus.Visible = false;
                        }
                        else
                        {
                            LinkEdit.Enabled = false;
                            linktitle.Visible = false;
                            lbltitle.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                            SpanDiv.Visible = false;
                            lblActiveStatus.Visible = true;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            linkdelete.Enabled = true;
                        else
                            linkdelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }

        private void Bind_Repeater()
        {
            DataTable dt = new DataTable();
            dt =   ServicesFactory.DocCMSServices.Fetch_Blog_Details();
            if (dt != null && dt.Rows.Count > 0)
            {
                RepeaterContent.DataSource = dt;
                RepeaterContent.DataBind();
            }
        }
        protected void RepeaterContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "EditLink")
            {
                Response.Redirect("~/cadmin/Blogs.aspx?BlogID=" + e.CommandArgument.ToString());
            }
            if (e.CommandName == "Delete")
            {
                ServicesFactory.DocCMSServices.Delete_Blog_By_id(Convert.ToInt32(e.CommandArgument));
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            if (e.CommandName == "OrderUp")
            {
                Label LabDisplayOrder = (Label)e.Item.FindControl("LabDisplayOrder");
                ServicesFactory.DocCMSServices.Reorder_Blog_Type(Convert.ToInt32(e.CommandArgument.ToString()), Convert.ToInt32(LabDisplayOrder.Text), "UP");
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            if (e.CommandName == "OrderDown")
            {
                Label LabDisplayOrder = (Label)e.Item.FindControl("LabDisplayOrder");
                ServicesFactory.DocCMSServices.Reorder_Blog_Type(Convert.ToInt32(e.CommandArgument.ToString()), Convert.ToInt32(LabDisplayOrder.Text), "DOWN");
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }
        protected void RepeaterContent_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

                Image ImgBlog = (Image)e.Item.FindControl("ImgBlog");
                Label lblBlogImage = (Label)e.Item.FindControl("lblBlogImage");
                string BlogImagePath = "../UploadedFiles/UploadedFiles/Blogs/";
                if (lblBlogImage != null && ImgBlog != null)
                {
                  ImgBlog.ImageUrl = BlogImagePath + lblBlogImage.Text;
                }
            }
        }
        protected void cmdInsert_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Blogs.aspx");
        }
        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            Int32 Retval = ServicesFactory.DocCMSServices.Update_Blog_Status(Convert.ToInt32(CurRestID.Value), Convert.ToBoolean(CurStatus.Value));
            Bind_Repeater();
        }

    }//=== class ends here ===
}