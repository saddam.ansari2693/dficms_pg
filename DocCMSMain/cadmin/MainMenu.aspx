﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="MainMenu.aspx.cs" Inherits="DocCMSMain.cadmin.MainMenu" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": pagesize,
                "aaSortingFixed": [[2, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Main Menu Links</a> <span class="separator"></span></li>
        <li>Manage Main Menu Links</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Main Menu Links</h5>
            <h1>
                Manage Main Menu Links</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                Main Menu Links Details
                <asp:Button ID="Button1" runat="server" CssClass="btn" Width="150" Text="Add New Entry"
                    OnCommand="ShowFormView" CommandArgument="New" ValidationGroup="grpsubmit" Style="float: right;
                    margin-top: -5px;" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" />
                    <col class="con1" />
                    <col class="con1" />
                    <col class="con1" />
                    <col class="con1" />
                    <col class="con1" style="align: center; width: 10%" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Main Menu Title
                        </th>
                        <th class="head1">
                            Main Menu Link
                        </th>
                        <th class="head1">
                            Display
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                            Creation Date
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RepeaterContent" OnItemCommand="RepeaterContent_ItemCommand"
                        OnItemDataBound="RepeaterContent_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:LinkButton ID="linktitle" CommandArgument='<%# Eval("IdNavigatgion")%>' CommandName="EditLink"
                                        runat="server" Text='<%# Eval("Title")%>' />
                                    <asp:Label runat="server" Text='<%# Eval("Title")%>' ID="lab1" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Link")%>' ID="Lablink"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("TemplatePage")%>' ID="Labtemplate"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("PolicyMemorandums")%>' ID="Labpolicy"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("PageTitle")%>' ID="Labptitle"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("Filename")%>' ID="Labfile"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("VideoLink")%>' ID="LabMedia"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Dorder")%>' ID="LabDisplayOrder"></asp:Label>
                                    <div style="display: inline; margin-left: 20px">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("IdNavigatgion")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("IdNavigatgion")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' ID="Label2"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreatedDate","{0:MM/dd/yyyy}")%>' ID="Label3"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="LinkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("IdNavigatgion")%>' CommandName="EditLink" ToolTip="Edit" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                <td>
                                    <asp:LinkButton ID="linktitle" CommandArgument='<%# Eval("IdNavigatgion")%>' CommandName="EditLink"
                                        runat="server" Text='<%# Eval("Title")%>' />
                                    <asp:Label runat="server" Text='<%# Eval("Title")%>' ID="lab1" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Link")%>' ID="Lablink"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("TemplatePage")%>' ID="Labtemplate"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("PolicyMemorandums")%>' ID="Labpolicy"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("PageTitle")%>' ID="Labptitle"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("Filename")%>' ID="Labfile"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("VideoLink")%>' ID="LabMedia"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Dorder")%>' ID="LabDisplayOrder"></asp:Label>
                                    <div style="display: inline; margin-left: 20px">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("IdNavigatgion")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("IdNavigatgion")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' ID="Label2"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreatedDate","{0:MM/dd/yyyy}")%>' ID="Label3"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="LinkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("IdNavigatgion")%>' CommandName="EditLink" ToolTip="Edit" />
                                   
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <asp:Button ID="cmdInsert" runat="server" CssClass="btn btn-primary" Width="150"
                Text="Add Main Menu Link" OnCommand="ShowFormView" CommandArgument="New" ValidationGroup="grpsubmit" />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
