﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class SurveyFullReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["PWID"] != null && Request.QueryString["RID"] != null)
                {

                    using (DataTable dtSurveyDetail = ServicesFactory.DocCMSServices.Fetch_Survey_Detail_By_id(Convert.ToString(Request.QueryString["PWID"])))
                    {
                        if (dtSurveyDetail != null && dtSurveyDetail.Rows.Count > 0)
                        {
                            LitSurveyName.Text = Convert.ToString(dtSurveyDetail.Rows[0]["SurveyName"]);
                            LitSurveyDescription.Text = Convert.ToString(dtSurveyDetail.Rows[0]["Description"]);
                        }
                    }
                    using (DataTable dtSurveyQuestion = ServicesFactory.DocCMSServices.Fetch_Survey_questions(Convert.ToString(Request.QueryString["PWID"])))
                    {
                        if (dtSurveyQuestion != null && dtSurveyQuestion.Rows.Count > 0)
                        {
                            RepeaterQuestion.DataSource = dtSurveyQuestion;
                            RepeaterQuestion.DataBind();
                        }
                    }
                }
            }
        }

        protected void btnExportReport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtExport = new DataTable();
                dtExport.Columns.Add("SurveyResultTitle", typeof(string));
                dtExport.Columns.Add("ReponsesData", typeof(string));
                DataTable dtQuestions = ServicesFactory.DocCMSServices.Fetch_Survey_questions(Convert.ToString(Request.QueryString["PWID"]));
                if (dtQuestions != null && dtQuestions.Rows.Count > 0)
                {
                   
                    DataRow drow = null;
                    drow = dtExport.NewRow();
                    drow["SurveyResultTitle"] = "Survey Name";
                    drow["ReponsesData"] = LitSurveyName.Text;
                    dtExport.Rows.Add(drow);

                    drow = dtExport.NewRow();
                    drow["SurveyResultTitle"] = "Survey Description";
                    drow["ReponsesData"] = LitSurveyDescription.Text;
                    dtExport.Rows.Add(drow);

                    DataTable dtResponse = null;
                    foreach (DataRow drQuestions in dtQuestions.Rows)
                    {
                        
                        string QuestionText = "";
                        string ResponseList = "";
                        if (drQuestions["AnswerType"] != null)
                        {
                            QuestionText = Convert.ToString(drQuestions["Question"]);
                            switch (Convert.ToString(drQuestions["AnswerType"]).ToUpper())
                            {
                                case "TEXT":
                                    ResponseList = ServicesFactory.DocCMSServices.Fetch_Response_By_questionheading(Convert.ToString(Request.QueryString["RID"]), Convert.ToString(drQuestions["AnswerType"]), Convert.ToString(drQuestions["SurveyQuestionID"]));
                                    break;
                                case "OPTION":
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_Response_By_questionList(Convert.ToString(Request.QueryString["PWID"]), Convert.ToString(drQuestions["SurveyQuestionID"]), "Option", Convert.ToString(Request.QueryString["RID"]));
                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dtResponse.Rows)
                                        {
                                            if (dr["checked"] == "checked")
                                            {
                                                ResponseList = dr["optionVal"].ToString();
                                            }
                                        }
                                    }
                                    break;
                                case "CHECK":
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_Response_By_questionList(Convert.ToString(Request.QueryString["PWID"]), Convert.ToString(drQuestions["SurveyQuestionID"]), "Check", Convert.ToString(Request.QueryString["RID"]));
                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dtResponse.Rows)
                                        {
                                            if (Convert.ToString(dr["checked"]).ToUpper() == "CHECKED")
                                            {
                                                ResponseList = ResponseList + "," + dr["optionVal"].ToString();
                                            }
                                        }
                                        if (ResponseList.Length > 0)
                                        {
                                            ResponseList = ResponseList.Substring(1);
                                        }
                                    }
                                    break;
                                case "DROPDOWN":
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_Response_By_questionList(Convert.ToString(Request.QueryString["PWID"]), Convert.ToString(drQuestions["SurveyQuestionID"]), "DropDown", Convert.ToString(Request.QueryString["RID"]));
                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dtResponse.Rows)
                                        {
                                            if (dr["checked"] == "checked")
                                            {
                                                ResponseList = dr["optionVal"].ToString();
                                            }
                                        }
                                    }
                                    break;
                            }
                            drow = dtExport.NewRow();
                            drow["SurveyResultTitle"] = QuestionText;
                            drow["ReponsesData"] = ResponseList;
                            dtExport.Rows.Add(drow);
                        }
                    }
                    if (dtExport != null && dtExport.Rows.Count > 0)
                    {
                        ServicesFactory.DocCMSServices.ExporttoExcel(dtExport, "SurveyDetailedReport.xls", "Response Data", "Response", "", "", "", "");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lnkback_Click(object sender, EventArgs e)
        {
            Response.Redirect("SurveyDetailedReport.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]));
        }


        protected void RepeaterQuestion_ItemDataBound1(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblQuestionType = (Label)e.Item.FindControl("lblQuestionType");
                Label lblQuestionID = (Label)e.Item.FindControl("lblQuestionID");
                TextBox txtAnswerText = (TextBox)e.Item.FindControl("txtAnswerText");
                Label lblFieldType = (Label)e.Item.FindControl("lblFieldType");
                Label lblSurveyID = (Label)e.Item.FindControl("lblSurveyID");
                DataTable dtResponse = null;
                if (lblQuestionType != null)
                {
                    switch (lblQuestionType.Text.Trim().ToUpper())
                    {
                        case "TEXT":
                        case "DROPDOWN":
                            if (txtAnswerText != null && lblFieldType != null)
                            {
                                txtAnswerText.Text = ServicesFactory.DocCMSServices.Fetch_Response_By_questionheading(Convert.ToString(Request.QueryString["RID"]), lblFieldType.Text.Trim(), lblQuestionID.Text.Trim());
                            }
                            break;
                        case "OPTION":
                            if (txtAnswerText != null)
                            {
                                txtAnswerText.Visible = false;
                            }
                            if (lblSurveyID != null)
                            {
                                dtResponse = ServicesFactory.DocCMSServices.Fetch_Response_By_questionList(lblSurveyID.Text.Trim(),lblQuestionID.Text.Trim(), "Option", Convert.ToString(Request.QueryString["RID"]));
                                if (dtResponse != null)
                                {
                                    Repeater RepeaterRadio = (Repeater)e.Item.FindControl("RepeaterRadio");
                                    RepeaterRadio.DataSource = dtResponse;
                                    RepeaterRadio.DataBind();
                                }
                            }
                            break;
                        case "CHECK":
                            if (txtAnswerText != null)
                            {
                                txtAnswerText.Visible = false;
                            }
                            if (txtAnswerText != null)
                            {
                                txtAnswerText.Visible = false;
                            }
                            if (lblSurveyID != null)
                            {
                                dtResponse = ServicesFactory.DocCMSServices.Fetch_Response_By_questionList(lblSurveyID.Text.Trim(),lblQuestionID.Text.Trim(), "Check", Convert.ToString(Request.QueryString["RID"]));
                                if (dtResponse != null)
                                {

                                    Repeater RepeaterCheckbox = (Repeater)e.Item.FindControl("RepeaterCheckbox");
                                    RepeaterCheckbox.DataSource = dtResponse;
                                    RepeaterCheckbox.DataBind();
                                }
                            }
                            break;
                    }
                }
            }
        }
        protected void repeateryearlysummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item != null)
            {
                HtmlControl Divgreen = (HtmlControl)e.Item.FindControl("divgreen");
                HtmlControl Divblack = (HtmlControl)e.Item.FindControl("divblack");
                HtmlControl DivTotalResponseFallYear = (HtmlControl)e.Item.FindControl("DivTotalResponseFallYear");
                Label lableft = (Label)e.Item.FindControl("lableft");
                Label labright = (Label)e.Item.FindControl("labright");
                if (Divgreen != null && lableft != null)
                {
                    Divgreen.Style.Add("width", lableft.Text.ToString());
                }
                //=============================================
                if (Divblack != null && labright != null)
                {
                    Divblack.Style.Add("width", labright.Text.ToString());
                }
            }
        }

        protected void RepeaterResponse_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
           
        }


        protected void RepeaterQuestion_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item != null)
                {
                    Label lblQuestionID = (Label)e.Item.FindControl("lblQuestionID");
                    Repeater RepeaterResponse = (Repeater)e.Item.FindControl("RepeaterResponse");
                    HiddenField hdnTotalResponse = (HiddenField)e.Item.FindControl("hdnTotalResponse");
                    Label lblTotalResponse = (Label)e.Item.FindControl("lblTotalResponse");
                    Label lblQuestionType = (Label)e.Item.FindControl("lblQuestionType");
                    Literal litQTypeMsg = (Literal)e.Item.FindControl("litQTypeMsg");
                    Label lblSurveyID = (Label)e.Item.FindControl("lblSurveyID");
                    Label lblFieldType = (Label)e.Item.FindControl("lblFieldType");
                    if (lblQuestionType != null)
                    {
                        if (litQTypeMsg != null)
                        {
                            switch (lblQuestionType.Text.ToUpper())
                            {
                                case "TEXT":
                                    litQTypeMsg.Text = "Type:&nbsp;Text&nbsp;&nbsp;-&nbsp;Descriptive";
                                    break;
                                case "OPTION":
                                    litQTypeMsg.Text = "Type:&nbsp;Single&nbsp;Choice";
                                    break;
                                case "CHECK":
                                    litQTypeMsg.Text = "Type:&nbsp;Multiple&nbsp;Choice";
                                    break;
                                case "DROPDOWN":
                                    litQTypeMsg.Text = "Type:&nbsp;Multiple&nbsp;Choice";
                                    break;
                            }
                        }
                    }
                    if (RepeaterResponse != null)
                    {
                        DataTable DtQuestionSummary = null;
                        DtQuestionSummary = ServicesFactory.DocCMSServices.Fetch_questions_Response(lblSurveyID.Text.Trim(), lblQuestionType.Text.Trim(), lblQuestionID.Text.Trim(), lblFieldType.Text.Trim(), System.DateTime.Now.Year.ToString(), System.DateTime.Now.Month.ToString());
                        if (DtQuestionSummary != null && DtQuestionSummary.Rows.Count > 0)
                        {
                            if (hdnTotalResponse != null)
                            {
                                hdnTotalResponse.Value = Convert.ToString(DtQuestionSummary.Rows[0]["TotalResponse"]);
                            }
                            if (lblTotalResponse != null)
                            {
                                lblTotalResponse.Text = hdnTotalResponse.Value;
                            }
                            RepeaterResponse.DataSource = DtQuestionSummary;
                            RepeaterResponse.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadReport(string surveyID, Int32 Year, Int32 Month)
        {

        }
        // To Get the Responses Fall Year
    }// Class Ends Here
}