﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Web.Security;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class Default : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Copywrite footer year dynamically 
                labyear.Text = DateTime.Now.Year.ToString();
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                // Check UserName and Password is Valid or Not
                bool CheckUser = false;
                bool CheckforActiveRole = false;
                int RoleID = 0;
                CheckUser = ServicesFactory.DocCMSServices.Check_User_Login_Details(username.Value, password.Value);
                if (CheckUser == true)
                {
                    //CheckforActiveRole = ServicesFactory.DocCMSServices.Check_For_Active_UserRole(username.Value, password.Value);
                    CheckforActiveRole = true;
                    if (CheckforActiveRole == true)
                    {
                        // If  user Name and Password is valid then  Store UserId, Firstname and EmailId inside Session
                        FormsAuthentication.SetAuthCookie(username.Value, chkSignin.Checked);
                        DataTable dt = new DataTable();
                        dt = ServicesFactory.DocCMSServices.Fetch_User_Profile_Details(username.Value, password.Value);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            Session["UserID"] = Convert.ToString(dt.Rows[0]["UserID"]).Replace(" ", "");
                            Session["FName"] = Convert.ToString(dt.Rows[0]["FName"]);
                            Session["EmailID"] = Convert.ToString(dt.Rows[0]["EmailID"]);
                            Session["RoleID"] = Convert.ToString(dt.Rows[0]["RoleID"]).Replace(" ","");
                            RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
                        }
                        //Get the top submodule navigation URL to redirect on  dashboard
                        DataTable dtRedirectURL = ServicesFactory.DocCMSServices.Fetch_SubModule_URL_By_Role_Id(RoleID);
                        if (dtRedirectURL != null && dtRedirectURL.Rows.Count > 0)
                        {
                            string RedirectURL = "";
                            if (Convert.ToString(dtRedirectURL.Rows[0]["NavigationUrl"]).ToUpper().Contains("PID"))
                            {   
                                string[] splitURL = Convert.ToString(dtRedirectURL.Rows[0]["NavigationUrl"]).Split('?');
                                string[] SplitPageID = splitURL[1].Split('_');
                                RedirectURL = ".." + splitURL[0] + "?PID=" + SplitPageID[1];
                                Session["SubModuleID"] = Convert.ToString(dtRedirectURL.Rows[0]["SubModuleID"]);
                                Response.Redirect(RedirectURL.Trim());
                            }
                            else
                            {
                                Session["SubModuleID"] = Convert.ToString(dtRedirectURL.Rows[0]["SubModuleID"]);
                                Response.Redirect(".."+Convert.ToString(dtRedirectURL.Rows[0]["NavigationUrl"]));
                            }
                        }
                        else
                        {
                              Response.Redirect("../cadmin/MainDashboard.aspx");
                        }
                    }
                    else
                    {
                        innermsgDiv.InnerText = "Please Contact Administrator.";
                        msgDiv.Attributes.Add("class", "Exists");
                    }
                }
                else
                {
                    // If UserName and Password is Not valid Error message will display
                    innermsgDiv.InnerText = "Invalid username or password.";
                    msgDiv.Attributes.Add("class", "Exists");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}