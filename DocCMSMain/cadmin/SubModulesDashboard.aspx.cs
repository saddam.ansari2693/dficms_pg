﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class SubModulesDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_SubModules();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
                SetControls();
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddSubModuleTop.Visible = true;
                    btnAddSubModuleBottom.Visible = true;
                }
                else
                {
                    btnAddSubModuleTop.Visible = false;
                    btnAddSubModuleBottom.Visible = false;

                }
                foreach (RepeaterItem Item in RptrSubModules.Items)
                {

                    LinkButton lnkSubModuleName = (LinkButton)Item.FindControl("lnkSubModuleName");
                    LinkButton lnkNavigationURL = (LinkButton)Item.FindControl("lnkNavigationURL");
                    Label Label1 = (Label)Item.FindControl("Label1");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");

                    if (lblTitle != null && lnkSubModuleName != null && btnOrderUP != null && btnOrderDown != null && lnkEdit != null && lnkDelete != null && Label1 != null && lnkNavigationURL !=null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkSubModuleName.Visible = true;
                            lnkNavigationURL.Visible = true;
                            lblTitle.Visible = false;
                            Label1.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;

                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkSubModuleName.Visible = false;
                            lnkNavigationURL.Visible = false;
                            lblTitle.Visible = true;
                            Label1.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                     
                        }

                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // bind SubModules inside RptrSubModules repeater
        protected void Bind_SubModules()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SubModules();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrSubModules.DataSource = dt;
                    RptrSubModules.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnAddSubModuleBottom_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/cadmin/SubModules.aspx");                
           
        }

        protected void RptrSubModules_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                Label lblModuleID = (Label)e.Item.FindControl("lblModuleID"); 
                switch (e.CommandName)
                {
                    case "EditSubModule":
                        Response.Redirect("~/cadmin/SubModules.aspx?ID=" + e.CommandArgument.ToString() + "&MID=" + Convert.ToInt32(lblModuleID.Text));
                        break;
                    case "DeleteSubModule":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_SubModules(Convert.ToInt32(e.CommandArgument));
                        Bind_SubModules();
                        Response.Redirect("~/cadmin/SubModulesDashboard.aspx");
                        break;
                    case "OrderUp":
                        if (lblModuleID != null && lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_SubModules(Convert.ToInt32(e.CommandArgument),Convert.ToInt32(lblModuleID.Text), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        Bind_SubModules();
                        break;
                    case "OrderDown":
                        if (lblModuleID != null && lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_SubModules(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblModuleID.Text), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        Bind_SubModules();
                        break;
                    default:
                        break;
                }
            }
        }

        protected void RptrSubModules_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

            }
        }
    }
}