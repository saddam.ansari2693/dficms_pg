﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using DocCMS.Core.DataTypes;

namespace DocCMSMain.cadmin
{
    public partial class QuoteComment : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;

        protected void Page_Load(object sender, EventArgs e)
        {
             Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";

            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["PurchaseOrderID"] != null)
                {
                   
                        Bind_QuoteComments();
                    
                }
            }
        }


        protected void RptrQuoteComment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                CheckBox ChkActive = (CheckBox)e.Item.FindControl("ChkActive");
                Label lblQuoteHeading = (Label)e.Item.FindControl("lblQuoteHeading");
                TextBox txtOrder = (TextBox)e.Item.FindControl("txtOrder");

                // Check comment count by Purchase order ID
                Int32 Retcount = ServicesFactory.DocCMSServices.CheckCount_For_comment_Bypurchaseorderid(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                if (Retcount > 0)
                {
                    DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_Quote_comment_Bypurchaseorderid(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));

 
                        if (dtresult != null && dtresult.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtresult.Rows.Count; i++)
                            {
                                if (lblQuoteHeading.Text.ToUpper() == Convert.ToString(dtresult.Rows[i]["QuoteName"]).ToUpper())
                                {
                                    ChkActive.Checked = true;
                                    txtOrder.Text = Convert.ToString(dtresult.Rows[i]["Dorder"]);
                                }
                               
                            }
                        }
                
                }


            }

        }
        // Function to bind Previous Quote Comment
        protected void Bind_PrevQuoteComments(Int32 PurchaseOrderID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Quote_comment_Bypurchaseorderid(PurchaseOrderID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrQuoteComment.DataSource = dt;
                    RptrQuoteComment.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Function to bind Quote Comment
        protected void Bind_QuoteComments()
        {
            DataTable dtCompany = null;
            DataTable dt=null;
            try
            {
                dtCompany = ServicesFactory.DocCMSServices.Fetch_AllQuote_comment();
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    dt = ServicesFactory.DocCMSServices.Fetch_AllQuote_comment();
                }
              
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrQuoteComment.DataSource = dt;
                    RptrQuoteComment.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Function to back click
        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/CreateQuote.aspx?PurchaseOrderID=" + Convert.ToString(Request.QueryString["PurchaseOrderID"]));
        }
        // Function to add comment 
        protected void btnAddCommenttoQuote_Click(object sender, EventArgs e)
        {
            Int32 Count = 0;
            ServicesFactory.DocCMSServices.Delete_Quotescomment_From_quotecommentTable(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
            foreach (RepeaterItem ri in RptrQuoteComment.Items)
            {
                CheckBox ChkActive = (CheckBox)ri.FindControl("ChkActive");
                Label lblQuoteHeading = (Label)ri.FindControl("lblQuoteHeading");
                Label lblQuoteComment = (Label)ri.FindControl("lblQuoteComment");
                TextBox txtOrder = (TextBox)ri.FindControl("txtOrder");
                if (ChkActive.Checked)
                {
                    Cls_Quotescomment obj =new Cls_Quotescomment();
                    obj.purchaseorderid=Convert.ToInt32(Request.QueryString["PurchaseOrderID"]);
                    obj.quotename=lblQuoteHeading.Text;
                    obj.quotecomment=lblQuoteComment.Text;
                    obj.dorder = Convert.ToInt32(txtOrder.Text);
                    Int32 RetVal = ServicesFactory.DocCMSServices.Insert_Quotescomment_to_quotecommentTable(obj);
                    if (RetVal > 0)
                        Count = 1;
                    else
                        return;
                }

                if (Count > 0)
                {
                  
                   


                }
            }
        }
    }
}