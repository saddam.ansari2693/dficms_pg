﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using DocCMS.Core;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace DocCMSMain.cadmin
{
    public partial class UserGuidePage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (Session["RoleID"] != null)
            {
                if (!IsPostBack)
                {
                    lblAuthority.Text = "http://" + Request.Url.Authority + "_###_" + Request.RawUrl;
                    BindHtml(Convert.ToInt32(Session["RoleID"]));
                    BindSideMenu(Convert.ToInt32(Session["RoleID"]));
                }
            }
        }

        protected void BindHtml(Int32 RoleID)
        {
            DataTable dtModules = ServicesFactory.DocCMSServices.Fetch_MainContent(RoleID);
            StringBuilder sb = new StringBuilder();
            if (dtModules != null && dtModules.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtModules.Rows.Count; i++)
                {
                    sb.Append("<a href='#'></a>");
                    sb.Append("<h3 id='Module_" + dtModules.Rows[i]["MainContentID"]+"'><b>" + Convert.ToString(dtModules.Rows[i]["Heading"]) + "</b></h3>");
                    sb.Append(Convert.ToString(dtModules.Rows[i]["Description"]));
                    DataTable dtSubmodule = ServicesFactory.DocCMSServices.Fetch_UG_SubContent_for_html(Convert.ToInt32(dtModules.Rows[i]["MainContentID"]));
                    if (dtSubmodule != null && dtSubmodule.Rows.Count > 0)
                    {
                        for (Int32 k = 0; k < dtSubmodule.Rows.Count; k++)
                        {
                            sb.Append("<h4 id='SubModule_" + dtSubmodule.Rows[k]["SubContentID"] + "'><b>" + Convert.ToString(dtSubmodule.Rows[k]["Heading"]) + "</b></h4>");

                            sb.Append("<h5>" + Convert.ToString(dtSubmodule.Rows[k]["Description"]) + "</h5>");
                            sb.Append("</br>");
                        }
                    }
                  UserGuideDiv.InnerHtml = sb.ToString();
                }
            }
        }

        protected void BindSideMenu(Int32 RoleID)
        {
            DataTable dtModules = ServicesFactory.DocCMSServices.Fetch_MainContent(RoleID);
            StringBuilder sb = new StringBuilder();
            if (dtModules != null && dtModules.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtModules.Rows.Count; i++)
                {
                    sb.Append("<li><a href='UserGuidePage.aspx#Module_" + dtModules.Rows[i]["MainContentID"] + "'>" + Convert.ToString(dtModules.Rows[i]["Heading"]) + "<a></li>");
                    maincontents.InnerHtml = sb.ToString();
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            string imagePath = "";
            string imageRawURL = Request.RawUrl;
            ClientScript.RegisterStartupScript(this.GetType(), "", "<script>alert('" + imagePath + "');</script>");
            string UserGuideDivecontent = UserGuideDiv.InnerHtml;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SetWidthHeight();", true);
            if (imageRawURL.ToLower().IndexOf("/cadmin") == 0)
            {
                imagePath = "http://" + Request.Url.Authority;
                UserGuideDivecontent = UserGuideDivecontent.ToString().Replace("../UploadEditorFiles/", imagePath + "/UploadEditorFiles/");
            }
            else
            {
                string[] rawArrary = imageRawURL.ToString().Split('/');
                if (rawArrary.Length > 1)
                {
                    imagePath = "http://" + Request.Url.Authority + "/" + rawArrary[1].ToString();
                }
                UserGuideDivecontent = UserGuideDivecontent.ToString().Replace("../UploadEditorFiles/", imagePath + "/UploadEditorFiles/");
            }
            UserGuideDiv.InnerHtml = UserGuideDivecontent;
            UserGuideDiv.Attributes["class"] = "myclass";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserGuide.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            UserGuideDiv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 2f, 2f, 20f, 20f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
          }
     }
}
