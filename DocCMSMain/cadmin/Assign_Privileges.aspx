﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="Assign_Privileges.aspx.cs" Inherits="DocCMSMain.cadmin.Assign_Privileges" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/LeftSideBar.ascx" TagName="LeftSidebar" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script src="../js/Pages/RolePrivileges.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#hdnSuperAdmin").val() != "SuperAdmin") {
                ReadAllRows();
                FetchHideModulesFunction();
                ModulesHide();
            }
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });

        function CheckForPrivileges() {
            var totalModules = jQuery("dyntable >tbody");
            var btnUpdate = document.getElementById("btnupdate");
            if (btnUpdate != null) {
            }
        }

        var ModuleId = "";
        function ReadAllRows() {
            jQuery.ajax({
                type: "Get",
                contentType: "application/json; charset=utf-8",
                url: "../WebService/DocCMSApi.svc/Fetch_Roles",
                data: { "RoleID": jQuery("#hdnMasterRole").val() },
                dataType: "json",
                success: function (data) {
                    if (data != null && data != "") {
                       var result = jQuery.parseJSON(data);
                        if (result != null && result.length > 0) {
                            var html = "";
                            for (var i = 0; i < result.length; i++) {
                                if (result[i].IsView != "Checked") {
                                    jQuery("#uniform-ChkModule_View_" + result[i].ModuleID).css("visibility", "hidden");
                                    jQuery("#lblModule_View_" + result[i].ModuleID).css("visibility", "hidden");
                                }
                                if (result[i].IsAdd != "Checked") {
                                    jQuery("#uniform-ChkModule_Edit_" + result[i].ModuleID).css("visibility", "hidden");
                                    jQuery("#lblModule_Edit_" + result[i].ModuleID).css("visibility", "hidden");
                                }
                                if (result[i].IsEdit != "Checked") {
                                    jQuery("#uniform-ChkModule_Add_" + result[i].ModuleID).css("visibility", "hidden");
                                    jQuery("#lblModule_Add_" + result[i].ModuleID).css("visibility", "hidden");
                                }
                                if (result[i].IsDelete != "Checked") {
                                    jQuery("#uniform-ChkModule_Delete_" + result[i].ModuleID).css("visibility", "hidden");
                                    jQuery("#lblModule_Delete_" + result[i].ModuleID).css("visibility", "hidden");
                                }

                                //SubModule Section Start
                                jQuery.ajax({
                                    type: "Get",
                                    contentType: "application/json; charset=utf-8",
                                    url: "../WebService/DocCMSApi.svc/Fetch_Privileges_By_RoleId",
                                    data: { "RoleId": jQuery("#ddlRole").val(), "ModuleId": result[i].ModuleID, "MasterRole": jQuery("#hdnMasterRole").val() },
                                    dataType: "json",
                                    async: false,
                                    success: function (data) {
                                        var resultsub = jQuery.parseJSON(data);
                                        if (resultsub != null && resultsub.length > 0) {
                                            var html = "";
                                            jQuery("#hdncheckmodulesvalue").val("true");
                                            for (var j = 0; j < resultsub.length; j++) {
                                                var CheckStatusOfAllDiv = false;
                                                if (resultsub[j].IsView != "Checked") {
                                                    jQuery("#divSubModule_View_" + resultsub[j].SubModuleID).css("visibility", "hidden");
                                                    CheckStatusOfAllDiv = true;
                                                }
                                                else {
                                                    CheckStatusOfAllDiv = false;
                                                }
                                                if (resultsub[j].IsAdd != "Checked") {
                                                    jQuery("#divSubModule_Add_" + resultsub[j].SubModuleID).css("visibility", "hidden");
                                                    CheckStatusOfAllDiv = true;
                                                } else {
                                                    CheckStatusOfAllDiv = false;
                                                }
                                                if (resultsub[j].IsEdit != "Checked") {
                                                    jQuery("#divSubModule_Edit_" + resultsub[j].SubModuleID).css("visibility", "hidden");
                                                    CheckStatusOfAllDiv = true;
                                                } else {
                                                    CheckStatusOfAllDiv = false;
                                                }
                                                if (resultsub[j].IsDelete != "Checked") {
                                                    jQuery("#divSubModule_Delete_" + resultsub[j].SubModuleID).css("visibility", "hidden");
                                                    CheckStatusOfAllDiv = true;
                                                } else {
                                                    CheckStatusOfAllDiv = false;
                                                }
                                                if (resultsub[j].IsView != "Checked" && resultsub[j].IsAdd != "Checked" && resultsub[j].IsEdit != "Checked" && resultsub[j].IsDelete != "Checked") {
                                                    jQuery("#divSubModuleList_" + resultsub[j].SubModuleID).hide();
                                                }
                                                else {
                                                    jQuery("#divSubModuleList_" + resultsub[j].SubModuleID).show();
                                                }
                                            }
                                        }
                                    },
                                    error: function (result) {
                                    }
                                });
                                var chkModuleStatus = "";
                            }
                        }
                    }
                },
                error: function (result) {
                }
            });
        }

        //Get Empty Modules Id
        function FetchHideModulesFunction() {
          var ModulesId = "";
            jQuery("#dyntable > tbody").each(function () {
                jQuery(this).find("tr").find("td").find(".submodules").each(function () {
                    var DivId = jQuery(this).attr("id");
                    if (jQuery("#" + DivId).html().indexOf("div") == -1) {
                        var Prefix = DivId.substring(DivId.indexOf("_") + 1, DivId.length);
                        jQuery("#trMian_" + Prefix).hide();
                        ModulesId += Prefix + "|";
                        jQuery("#hdnEmptyModules").val(ModulesId);
                    }
                });
            });
        }

        //Hide empty Modules using by Module Id
        function ModulesHide() {
            var SplitModule = jQuery("#hdnEmptyModules").val().split('|');
            for (var s = 0; s < SplitModule.length - 1; s++) {
                jQuery("#trMian_" + SplitModule[s]).hide();
            }
        }
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Privilege</a> <span class="separator"></span></li>
        <li>Assign Privileges</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Privileges</h5>
            <h1>
                Assign Privileges</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h3 class="widgettitle">
                <b>Create Role Privilege</b>
                <label id="Label1" runat="server" style="font-size: 12px; font-family: Times New Roman;">
                </label>
            </h3>
            <div class="widgetcontent">
                <div style="padding-top: 5px; width: 100%; display: inline;">
                    <div class="form-inline">
                        <label id="Label2" runat="server" style="font-size: 13px; width: 110px; text-align: right;">
                            <strong>Select Role Name:</strong>
                        </label>
                        <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="true" ClientIDMode="Static"
                            OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" Style="width: 430px;">
                        </asp:DropDownList>
                        <div runat="server" id="divSelectAll" style="margin-left: 30px; display: inline;
                            vertical-align: middle;">
                            <strong>Select All:</strong>&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkSelectAll" runat="server"
                                ClientIDMode="Static" CssClass="input-large" Style="opacity: 1;" onchange="SelectAll();" />
                            </label>
                        </div>
                        <asp:Button ID="btnExportPdf" runat="server" class="btn btn-primary" Text="Export"
                            Width="150px" OnClick="btnExportPdf_Click" />
                    </div>
                   </div>
                  <div class="form-inline">
                    <asp:HiddenField ID="hdnRolesPrivileges" runat="server" ClientIDMode="Static" />
                      <asp:Repeater runat="server" ID="RptrPrivileges" OnItemDataBound="RptrPrivileges_databound" ClientIDMode="Static">
                        <HeaderTemplate>
                            <table id="dyntable" class="table table-bordered table-responsive">
                                <colgroup>
                                    <col class="con1" style="width: 100%;" />
                                </colgroup>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr id='trMian_<%# Eval("ModuleId") %>'>
                                    <td style="padding: 5px;">
                                        <div id='divModule_<%# Eval("ModuleId") %>' class="modules">
                                            <asp:Label ID="lblNone" runat="server" ClientIDMode="Static" Text='<%# Eval("ModuleID") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblModuleName" runat="server" ClientIDMode="Static" Text='<%# Eval("ModuleName") %>'></asp:Label>
                                            <div class="ModuleCheckBoxes" id="allCheckbox">
                                                <input type="hidden" id='hdnModule_View_<%# Eval("ModuleId") %>' value="" />
                                                <input type="checkbox" id='ChkModule_View_<%# Eval("ModuleId") %>' class="View" value='View_Module_<%# Eval("ModuleId") %>'
                                                    <%# Eval("IsView") %> name='ChkModule_View_<%# Eval("ModuleId") %>' onclick="CheckSubmoudles(this.id);" />
                                                <label for='ChkModule_View_<%# Eval("ModuleId") %>' id='lblModule_View_<%# Eval("ModuleId") %>'>
                                                    View</label>
                                                <input type="hidden" id='hdnModule_Add<%# Eval("ModuleId") %>' value="" />
                                                <input type="checkbox" id='ChkModule_Add_<%# Eval("ModuleId") %>' class="Add" value='Add_Module_<%# Eval("ModuleId") %>'
                                                    <%# Eval("IsAdd") %> name='ChkModule_Add_<%# Eval("ModuleId") %>' onclick="CheckSubmoudles(this.id);" />
                                                <label for='ChkModule_Add_<%# Eval("ModuleId") %>' id='lblModule_Add_<%# Eval("ModuleId") %>'>
                                                    Add</label>
                                                <input type="hidden" id='hdnModule_Edit_<%# Eval("ModuleId") %>' value="" />
                                                <input type="checkbox" id='ChkModule_Edit_<%# Eval("ModuleId") %>' class="Edit" value='Edit_Module_<%# Eval("ModuleId") %>'
                                                    <%# Eval("IsEdit") %> name='ChkModule_Edit_<%# Eval("ModuleId") %>' onclick="CheckSubmoudles(this.id);" />
                                                <label for='ChkModule_Edit_<%# Eval("ModuleId") %>' id='lblModule_Edit_<%# Eval("ModuleId") %>'>
                                                    Edit</label>
                                                <input type="hidden" id='hdnModule_Delete_<%# Eval("ModuleId") %>' value="" />
                                                <input type="checkbox" id='ChkModule_Delete_<%# Eval("ModuleId") %>' class="Delete"
                                                    value='Delete_Module_<%# Eval("ModuleId") %>' name='ChkModule_Delete_<%# Eval("ModuleId") %>'
                                                    <%# Eval("IsDelete") %> onclick="CheckSubmoudles(this.id);" />
                                                <label for='ChkModule_Delete_<%# Eval("ModuleId") %>' id='lblModule_Delete_<%# Eval("ModuleId") %>'>
                                                    Delete</label>
                                            </div>
                                        </div>
                                        <!-- Sub Module Desing Start here -->
                                        <div class="submodules" id='divSubModule_<%# Eval("ModuleID") %>'>
                                            <asp:Repeater ID="rptrSubmodules" runat="server" ClientIDMode="Static">
                                                <ItemTemplate>
                                                    <div id='divSubModuleList_<%# Eval("SubModuleID") %>'>
                                                        <asp:Label ID="lblSubModuleIsview" runat="server" ClientIDMode="Static"></asp:Label>
                                                        <div class="divsubmodule clearfix">
                                                            <asp:Label ID="lblSubmoduleID" Text='<%# Eval("SubModuleID") %>' runat="server" ClientIDMode="Static"
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSubmodule" runat="server" ClientIDMode="Static" Text='<%# Eval("SubModuleName") %>'></asp:Label>
                                                            <div class="SubModuleCheckBoxes" id="SubModules">
                                                                <input type="hidden" id='hdnSubModule_View_<%# Eval("SubModuleID") %>' value="" />
                                                                <div id='divSubModule_View_<%# Eval("SubModuleID") %>' style="display: inline-block;">
                                                                    <input type="checkbox" id='ChkSubModule_View_<%# Eval("InnerModuleID") %>' class="View"
                                                                        <%# Eval("IsView") %> value='View_SubModule_<%# Eval("SubModuleID") %>' name='ChkSubModule_View_<%# Eval("InnerModuleID") %>' />
                                                                    <label for='ChkSubModule_View_<%# Eval("InnerModuleID") %>' id='lblSubModule_View_<%# Eval("InnerModuleID") %>'>
                                                                        View</label>
                                                                </div>
                                                                <input type="hidden" id='hdnSubModule_Add_<%# Eval("SubModuleID") %>' value="" />
                                                                <div id='divSubModule_Add_<%# Eval("SubModuleID") %>' style="display: inline-block;">
                                                                    <input type="checkbox" id='ChkSubModule_Add_<%# Eval("InnerModuleID") %>' class="Add"
                                                                        <%# Eval("IsAdd") %> value='Add_SubModule_<%# Eval("SubModuleID") %>' name='ChkSubModule_Add_<%# Eval("InnerModuleID") %>' />
                                                                    <label for='ChkSubModule_Add_<%# Eval("InnerModuleID") %>' id='lblSubModule_Add_<%# Eval("SubModuleID") %>'>
                                                                        Add</label>
                                                                </div>
                                                                <input type="hidden" id='hdnSubModule_Edit_<%# Eval("SubModuleID") %>' value="" />
                                                                <div id='divSubModule_Edit_<%# Eval("SubModuleID") %>' style="display: inline-block;">
                                                                    <input type="checkbox" id='ChkSubModule_Edit_<%# Eval("InnerModuleID") %>' class="Edit"
                                                                        <%# Eval("IsEdit") %> value='Edit_SubModule_<%# Eval("SubModuleID") %>' name='ChkSubModule_Edit_<%# Eval("InnerModuleID") %>' />
                                                                    <label for='ChkSubModule_Edit_<%# Eval("InnerModuleID") %>' id='lblSubModule_Edit_<%# Eval("InnerModuleID") %>'>
                                                                        Edit</label>
                                                                </div>
                                                                <input type="hidden" id='hdnSubModule_Delete_<%# Eval("SubModuleID") %>' value="" />
                                                                <div id='divSubModule_Delete_<%# Eval("SubModuleID") %>' style="display: inline-block;">
                                                                    <input type="checkbox" id='ChkSubModule_Delete_<%# Eval("InnerModuleID") %>' class="Delete"
                                                                        <%# Eval("IsDelete") %> value='Delete_SubModule_<%# Eval("SubModuleID") %>' name='ChkSubModule_Delete_<%# Eval("InnerModuleID") %>' />
                                                                    <label for='ChkSubModule_Delete_<%# Eval("InnerModuleID") %>' id='lblSubModule_Delete_<%# Eval("InnerModuleID") %>'>
                                                                        Delete</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <!-- Sub Module Desing Ends here -->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="clearfix" style="height: 10px;">
                </div>
                <div class="form-inline">
                    <p class="stdformbutton">
                        <input type="button" id="btnsubmit" runat="server" clientidmode="Static" class="btn btn-primary"
                            value="Save" style="width: 150px;" onclick="Validate_CheckBox();" />
                        <input type="button" id="btnupdate" runat="server" clientidmode="Static" class="btn btn-primary"
                            value="Update" style="width: 150px;" onclick="Validate_CheckBox();" />
                        <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                            OnClick="btnCancel_Click" />
                    </p>
                </div>
            </div>
            <!--maincontentinner-->
            <div>
                <br />
                <br />
                <uc1:Footer ID="FooterControl" runat="server" />
                <asp:HiddenField ID="hdnUserRole" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnRoleID" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnMasterRole" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnModules" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnSuperAdmin" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnEmptyModules" runat="server" ClientIDMode="Static" />
                 <asp:HiddenField ID="hdnCheckForPdf" runat="server" ClientIDMode="Static" Value="True" />
               <span id="ModuelsValues"></span>
                
            </div>
        </div>
        <!--maincontent-->
        <style type="text/css">
            .disabledbutton
            {
                pointer-events: none;
                opacity: 0.4;
            }
            #dyntable .check-center
            {
                text-align: center !important;
            }
            
            .modules
            {
                background-color: #444;
                color: #fff;
                padding: 8px 10px;
                margin-bottom: 5px;
                font-size: 14px;
                font-weight: bold;
            }
            
            .ModuleCheckBoxes, .SubModuleCheckBoxes
            {
                float: right;
                margin-right: 950px;
            }
            
            .ModuleCheckBoxes input, .SubModuleCheckBoxes input
            {
                margin-right: 0px;
            }
            
            .ModuleCheckBoxes label, .SubModuleCheckBoxes label
            {
                display: inline-block;
                font-size: 14px;
                margin-right: 5px;
                line-height: 16px;
            }
            
            .SubModuleCheckBoxes label
            {
                visibility: hidden;
            }
            
            .divsubmodule
            {
                margin: 5px 0;
                background: #eee;
                padding: 8px 12px 8px 10px;
            }
            
            div.checker
            {
                margin-right: 0px !important;
            }
            .ChkModuleView
            {
            }
        </style>
        <script type="text/javascript" language="javascript">
            jQuery(document).ready(function () {
                jQuery("#dyntable_length").hide();
                jQuery("#dyntable_filter").hide();
            });

            function CheckSubmoudles(ModuleID) {
                var ModuleArray = ModuleID.toString().split('_');
                var PrivType = ModuleArray[1];
                var Suffix = ModuleArray[2];
                var TargetBaseControl = document.getElementById("divSubModule_" + Suffix);
                var Inputs = TargetBaseControl.getElementsByTagName("input");
                for (var n = 0; n < Inputs.length; ++n) {
                    if (Inputs[n].type == 'checkbox') {
                        if (Inputs[n].id.toString().indexOf(PrivType.toString() + "_" + Suffix.toString()) > -1) {
                            var ChkID = Inputs[n].id.toString();
                            if (jQuery("#" + ModuleID).prop('checked') == true) {
                                jQuery("#uniform-ChkSubModule_" + PrivType + "_" + Suffix + " span").addClass("checked");
                                jQuery("#hdnSubModule_" + PrivType + "_" + Suffix).val("checked");
                                jQuery("#" + ChkID).attr("checked", true);
                            }
                            else {
                                jQuery("#uniform-ChkSubModule_" + PrivType + "_" + Suffix + " span").removeClass("checked");
                                jQuery("#hdnSubModule_" + PrivType + "_" + Suffix).val("");
                                jQuery("#" + ChkID).removeAttr("checked");
                            }
                        }
                    }
                }
            }
            function SelectAll() {
                var TargetBaseControl = document.getElementById("dyntable");
                var Inputs = TargetBaseControl.getElementsByTagName("input");
                for (var n = 0; n < Inputs.length; ++n) {
                    if (Inputs[n].type == 'checkbox') {
                        var ChkID = Inputs[n].id.toString();
                        if (jQuery("#chkSelectAll").prop('checked') == true) {
                            jQuery("#dyntable span").addClass("checked");
                            jQuery("#dyntable").find("input[type='hidden']").val("checked");
                        }
                        else {
                            jQuery("#dyntable span").removeClass("checked");
                            jQuery('input[type=checkbox]:checked').attr('checked', false);
                            jQuery("#dyntable").find("input[type='hidden']").val("");
                        }
                    }
                }
            }

            function Validate_CheckBox() {
                valid = true;
                if (jQuery('input[type=checkbox]:checked').length != 0) {
                  Save_Privileges('Insert');
                }
                else {
                    alert("ERROR! Please select at least one checkbox");
                    valid = false;
                }
                return valid;
            }

            function Save_Privileges(SaveMode) {
                var privListModule = "";
                var privListSubModule = "";
                var ModuleList = "";
                var enumView = jQuery("#dyntable tr");
                var curModuleID = "";
                for (var i = 0; i < enumView.length; i++) {
                    privListModule = privListModule + "," + jQuery("#hdnRoleID").val();
                    var ModuleArray = enumView[i].id.split('_');
                    curModuleID = ModuleArray[ModuleArray.length - 1];
                    privListModule = privListModule + "~" + curModuleID.toString() + "~0";
                    ModuleList = ModuleList + "," + curModuleID;
                    var moduleDiv = "divModule_" + curModuleID;
                    var enumPriv = jQuery("#" + moduleDiv + " div div span");
                    for (var j = 0; j < enumPriv.length; j++) {
                        if (jQuery(enumPriv[j]).attr('class') == "checked") {
                            privListModule = privListModule + "~" + "1";
                        }
                        else {
                            privListModule = privListModule + "~" + "0";
                        }
                    }
                }
                ModuleList = ModuleList.substring(1);
                var ModuleArrayInner = ModuleList.split(',');
                var SubModulePriv = "";
                for (var ma = 0; ma < ModuleArrayInner.length; ma++) {
                    privListSubModule = privListSubModule + "," + jQuery("#hdnRoleID").val() + "~" + ModuleArrayInner[ma];
                    var enumSubModule = jQuery("#divSubModule_" + ModuleArrayInner[ma].toString() + ">div");
                    if (enumSubModule.length > 0) {
                        for (var mlist = 0; mlist < enumSubModule.length; mlist++) {
                            var submdouleArray = enumSubModule[mlist].id.toString().split('_');
                            SubModulePriv = SubModulePriv + "," + ModuleArrayInner[ma];
                            var CurSubModuleID = submdouleArray[submdouleArray.length - 1];
                            var EnumSubmodulePriv = jQuery("#" + enumSubModule[mlist].id + " div div div span");
                            SubModulePriv = SubModulePriv + "~" + CurSubModuleID.toString();
                            for (x = 0; x < EnumSubmodulePriv.length; x++) {
                                if (jQuery(EnumSubmodulePriv[x]).attr('class') == "checked") {
                                    SubModulePriv = SubModulePriv + "~" + "1";
                                }
                                else {
                                    SubModulePriv = SubModulePriv + "~" + "0";
                                }
                            }
                        }
                    }
                }
                SubModulePriv = SubModulePriv.substring(1);
                var SubPrivArray = SubModulePriv.split(',');
                var FinalSubModulePrivList = "";
                for (var xy = 0; xy < SubPrivArray.length; xy++) {
                    var privLocal = jQuery("#hdnRoleID").val();
                    privLocal = privLocal + "~" + SubPrivArray[xy].toString();
                    FinalSubModulePrivList = FinalSubModulePrivList + "," + privLocal.toString();
                }
                FinalSubModulePrivList = FinalSubModulePrivList.substring(1);
                privListModule = privListModule.substring(1);
                privListModule = privListModule + "," + FinalSubModulePrivList;
                if (isNaN(privListModule)) {
                    Insert_Privileges(privListModule);
                }
                else {

                    alert("Please specify Privileges for the seleted role");
                    return false;
                }
            }
      </script>
</asp:Content>
