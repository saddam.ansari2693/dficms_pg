﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Web.Services;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class ContentPages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "licontent";
            Session["Ulname"] = "ulcontent";
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    hdnCurUser.Value = Convert.ToString(Session["UserID"]);
                    if (Request.QueryString["ID"] != null)
                    {
                        hdnModPageID.Value =Request.QueryString["ID"];
                    }
                }
            }
        }

        protected void lnkSubmit_Click(Object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFile PostedFile = Request.Files[i];
                    fileUpload(1, PostedFile);
                }
                Response.Redirect("~/cadmin/ContentPageDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // File Upload Function
        public void fileUpload(int Mode, HttpPostedFile postedfile)
        {
            string fname = "";
            string uploadpath = "../UploadedFiles/image/";
            try
            {
                if (postedfile != null && !string.IsNullOrEmpty(postedfile.FileName))
                {
                    fname = postedfile.FileName;
                    string serverpath = Server.MapPath(uploadpath);
                    DirectoryInfo directoryinfo = new DirectoryInfo(serverpath);
                    bool isdircreated = directoryinfo.Exists;
                    if (!isdircreated)
                    {
                        directoryinfo.Create();
                    }
                    postedfile.SaveAs(serverpath + fname);
                }
                else
                {
                }
                if (!string.IsNullOrEmpty(fname))
                {
                    string userid = "";
                    Clsfileuploader fileuploader = new Clsfileuploader();
                    fileuploader.filename = fname;
                    fileuploader.creationdate = DateTime.Now.ToString();
                    fileuploader.filepath = @"../UploadedFiles/image/";
                    fileuploader.filecategoryid = "1";
                    fileuploader.modificationdate = DateTime.Now.ToString();
                    fileuploader.alternatetext = "";
                    fileuploader.description = "";
                    //==== memeber ship user =====//
                    if(Session["UserID"]!=null)                    
                    {
                        userid = Convert.ToString(Session["UserID"]);
                    }
                    fileuploader.uploadedby = Convert.ToString(userid);
                    if (Mode == 1)
                    {
                        ServicesFactory.DocCMSServices.Insert_MediaFiles(fileuploader);
                    }
                    if (Mode == 2)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }// Class Ends here
}