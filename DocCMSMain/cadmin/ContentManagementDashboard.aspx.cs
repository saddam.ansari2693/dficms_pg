﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class ContentManagementDashboard : System.Web.UI.Page
    {

        bool btnStatus = false;
        Int32 pageID = 0;
        string ShowTableSize;
        protected static string PageName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["PID"] != null)
                {
                    
                pageID = Convert.ToInt32(Request.QueryString["PID"]);
                if (!IsPostBack)
                {
                    Bind_Dashboard_Content(pageID);
                    if(Session["showtablesize"] !=null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    SetControls();
                }
            }
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public static void SetSession(int showtablesize)
        {
            HttpContext.Current.Session["showtablesize"] = showtablesize;
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            DataTable dtContent = ServicesFactory.DocCMSServices.Fetch_CMS_Dashboard(Convert.ToInt32(Request.QueryString["PID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                    if (dtContent != null && dtContent.Rows.Count > 0)
                    {
                        if (Convert.ToString(dtContent.Rows[0]["IsMultiPage"]).ToUpper() == "TRUE")
                            btnAddContentPageTop.Visible = true;
                        else
                            btnAddContentPageTop.Visible = false;
                    }
                foreach (RepeaterItem Item in RptrContentPage.Items)
                {
                    LinkButton lblContentTitle = (LinkButton)Item.FindControl("lblContentTitle");
                    Label LinkContentTitle = (Label)Item.FindControl("LinkContentTitle");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    if (lblContentTitle != null && LinkContentTitle != null && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lblContentTitle.Visible = true;
                            LinkContentTitle.Visible = false;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            LinkContentTitle.Visible = true;
                            lblContentTitle.Visible = false;
                        }

                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }

                }

            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // Bind  Content Management Dashboad Details inside RptrContentPage Repeater Control
        protected void Bind_Dashboard_Content(Int32 PageID)
        {
            PageName = ServicesFactory.DocCMSServices.Fetch_pagename_By_pageid(pageID);
            //string LastModifiedBy = ServicesFactory.DocCMSServices.Get_ModifiedBy_
            DataTable dtContent = null;
            if (!string.IsNullOrEmpty(PageName))
            {
                headingPage.InnerText = "Manage " + PageName;
                ListPageName.InnerText = "Manage " + PageName;
                btnAddContentPageTop.Text = "Add new " + PageName.ToLower();
            }

            switch (PageName.ToUpper())
            {
                // this is for the page which consist Menu
                case "SUB MENU":
                    //Fetch the Data with Menu name order by display order
                    dtContent = ServicesFactory.DocCMSServices.Fetch_CMS_Dashboard_Fordisplayorder(PageID, "DropDown");
                    CustomHeading.InnerText = "Menu Name";
                    break;
                default:
                    //Fetch the Data without Menu name order by display order
                    dtContent = ServicesFactory.DocCMSServices.Fetch_CMS_Dashboard_Fordisplayorder(PageID, "Display");
                    break;

            }

            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                RptrContentPage.DataSource = dtContent;
                RptrContentPage.DataBind();
            }
            else
            {
                //Fetch the Data without Menu name and Display order
                dtContent = ServicesFactory.DocCMSServices.Fetch_CMS_Dashboard(PageID);
                if (dtContent != null && dtContent.Rows.Count > 0)
                {
                    RptrContentPage.DataSource = dtContent;
                    RptrContentPage.DataBind();
                }
                else
                {
                    RptrContentPage.DataSource = "";
                    RptrContentPage.DataBind();
                }
            } // ends of Check
            //Checking the page has Multiple page true or not if not btnAddContentpage will not visible 
            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                if (Convert.ToString(dtContent.Rows[0]["IsMultiPage"]).ToUpper() == "TRUE")
                    btnAddContentPageTop.Visible = true;
                else
                    btnAddContentPageTop.Visible = false;
            }
            else
                btnAddContentPageTop.Visible = true;
        }

        protected void RptrContentPage_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                switch (e.CommandName)
                {
                    case "EditPageContent":
                        Response.Redirect("~/cadmin/ContentManagement.aspx?" + e.CommandArgument);
                        break;
                    case "DeletePageContent":

                        Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                        LinkButton lblContentTitle = (LinkButton)e.Item.FindControl("lblContentTitle");
                        if (lblCMSID != null && lblContentTitle != null && lblPageID != null)
                        {
                           
                                Int32 DelVal = ServicesFactory.DocCMSServices.Delete_CMS_Data(Convert.ToInt32(lblCMSID.Text));
                                if (DelVal > 0)
                                {
                                    if (ServicesFactory.DocCMSServices.Is_Content_Page_Drafted(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text)))
                                    {
                                        string retval = ServicesFactory.DocCMSServices.Remove_CMS_Data_Draft(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToInt32(lblCMSID.Text));
                                    }
                                    Bind_Dashboard_Content(Convert.ToInt32(lblPageID.Text));
                                    ClientScript.RegisterStartupScript(this.GetType(), "Deleted", "<script>SuccessMsg(" + lblPageID.Text + ");</script>");
                                }

                                else
                                {
                                    string retval = ServicesFactory.DocCMSServices.Remove_CMS_Data_Draft(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToInt32(lblCMSID.Text));
                                    if (string.IsNullOrEmpty(retval))
                                    {
                                        Bind_Dashboard_Content(Convert.ToInt32(lblPageID.Text));
                                        ClientScript.RegisterStartupScript(this.GetType(), "Deleted", "<script>SuccessMsg(" + lblPageID.Text + ");</script>");
                                    }
                                }

                        }
                        break;

                    default:
                        break;
                }
            }
        }

        protected void RptrContentPage_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                LinkButton lblContentTitle = (LinkButton)e.Item.FindControl("lblContentTitle");
                Label lnkPageName = (Label)e.Item.FindControl("lnkPageName");
                ImageButton lnkEdit = (ImageButton)e.Item.FindControl("lnkEdit");
                Label LinkContentTitle = (Label)e.Item.FindControl("LinkContentTitle"); 
                ImageButton lnkDelete = (ImageButton)e.Item.FindControl("lnkDelete");
                string[] TempPageID = Convert.ToString(Request.QueryString["PID"]).Split('_');
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                HtmlTableCell trDisplay = (HtmlTableCell)e.Item.FindControl("trDisplay");
                HtmlAnchor ancPreview = (HtmlAnchor)e.Item.FindControl("ancPreview");
                HtmlAnchor ancPreviewDraft = (HtmlAnchor)e.Item.FindControl("ancPreviewDraft");
                Label lblActive = (Label)e.Item.FindControl("lblActive");
                HtmlTableCell Activestatusdata = (HtmlTableCell)e.Item.FindControl("Activestatusdata");
                Label lblContentState = (Label)e.Item.FindControl("lblContentState");
                Label lblCreationDate = (Label)e.Item.FindControl("lblCreationDate");
                Literal litContentStatus = (Literal)e.Item.FindControl("litContentStatus");
                if (lblCMSID != null && lblPageID != null && lblContentTitle != null && lnkPageName != null && lnkEdit != null && lnkDelete != null && !string.IsNullOrEmpty(lblDorder.Text) && litContentStatus != null && lblContentState != null && lblCreationDate != null)
                {

                    if (lnkPageName.Text.ToLower() == "home content")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/home.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Visible = false;
                    }
                    else if (lnkPageName.Text.ToLower() == "products")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/Products.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/Products.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else if (lnkPageName.Text.ToLower() == "company page")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/Company.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/Company.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else if (lnkPageName.Text.ToLower() == "graphics design")
                    {
                         ancPreview.Attributes.Add("onclick", "window.open('../dfi/GraphicsDesign.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                         ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/GraphicsDesign.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else if (lnkPageName.Text.ToLower() == "marketing and promotions")
                    {
                         ancPreview.Attributes.Add("onclick", "window.open('../dfi/Marketing.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                         ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/Marketing.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else
                    {
                        ancPreview.Visible = false;
                        ancPreviewDraft.Visible = false;
                    }
                    // bind status of content as active and inactive
                    if (lnkPageName.Text.ToUpper() != "" || lnkPageName.Text.ToUpper() != null)
                    {
                        string OriginalStatus = "";
                        DataTable dtStatus = ServicesFactory.DocCMSServices.Fetch_displayorder_Bycmsid(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text));
                        if (dtStatus != null && dtStatus.Rows.Count > 0)
                        {

                            OriginalStatus = Convert.ToString(dtStatus.Rows[0]["IsActive"]);
                            lblActive.Text = OriginalStatus;
                        }
                        else 
                        {
                            DataTable dtStatusDraft = ServicesFactory.DocCMSServices.Fetch_displayorder_Bycmsid_forDraft(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text));
                            if (dtStatusDraft != null && dtStatusDraft.Rows.Count > 0)
                            {

                                OriginalStatus = Convert.ToString(dtStatusDraft.Rows[0]["IsActive"]);
                                lblActive.Text = OriginalStatus;
                            }
                            else
                            {
                                ActiveStatusheading.Visible = false;
                                Activestatusdata.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        ActiveStatusheading.Visible = false;
                        Activestatusdata.Visible = false;
                    }
                    if (ServicesFactory.DocCMSServices.Is_Content_Page_Drafted(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text)))
                    {
                        
                        lblCreationDate.Text = ServicesFactory.DocCMSServices.Get_Single_Value_String("Convert(varchar,Creationdate,101)", "Content_Management_Master_Draft", "CMSID", lblCMSID.Text, "Number");
                        lblContentState.Text = "Draft";
                        if (Convert.ToInt32(lblCMSID.Text) >= 99999999)
                        {
                            litContentStatus.Text = "<img src='../images/status_workinprogress.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/status_draft.png' style='height:30px;'/>";                            
                        }
                        else
                        {
                            string ContentModifiedRoleID = ServicesFactory.DocCMSServices.Get_Content_modifiedby_pageidAndcmsid(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text));
                            if (ContentModifiedRoleID.ToUpper() == "EEEEEEEE-EEEE-EEEE-EEEE-EEEEEEEEEEEE")
                            {
                                litContentStatus.Text = "<img src='../images/status_published.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/status_draft.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/ApprovalPending.png' style='height:30px;width:133px;'/>";

                            }
                            else
                            litContentStatus.Text = "<img src='../images/status_published.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/status_draft.png' style='height:30px;'/>";
                        }
                    }
                    else
                    {
                        lblCreationDate.Text = ServicesFactory.DocCMSServices.Get_Single_Value_String("Convert(varchar,Creationdate,101)", "Content_Management_Master", "CMSID", lblCMSID.Text, "Number");
                        lblContentState.Text = "Published";
                        litContentStatus.Text = "<img src='../images/status_published.png' style='height:30px;'/>";
                        ancPreviewDraft.Visible = false;
                    }
                    lblContentTitle.Text = ServicesFactory.DocCMSServices.Fetch_Content_Title(Convert.ToInt32(lblCMSID.Text));
                    if (lblContentTitle.Text == null || lblContentTitle.Text == "")
                    {
                        lblContentTitle.Text = ServicesFactory.DocCMSServices.Fetch_Content_Title(Convert.ToInt32(lblCMSID.Text));
                    }
                    LinkContentTitle.Text = Convert.ToString(lblContentTitle.Text);
                    lnkEdit.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    lnkDelete.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                    lblContentTitle.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    thDisplay.Visible = true;
                    trDisplay.Visible = true;
                }
                else
                {

                    if (lnkPageName.Text.ToLower() == "home content")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/home.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Visible = false;
                    }
                    else if (lnkPageName.Text.ToLower() == "products")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/Products.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/Products.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else if (lnkPageName.Text.ToLower() == "company page")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/Company.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/Company.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else if (lnkPageName.Text.ToLower() == "graphics design")
                    {
                        ancPreview.Attributes.Add("onclick", "window.open('../dfi/GraphicsDesign.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                        ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/GraphicsDesign.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else if (lnkPageName.Text.ToLower() == "marketing and promotions")
                    {
                         ancPreview.Attributes.Add("onclick", "window.open('../dfi/Marketing.html?preview=1', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                         ancPreviewDraft.Attributes.Add("onclick", "window.open('../dfi/Marketing.html?preview=2', '_blank', 'location=yes,height=570,width=800,scrollbars=yes,status=yes')");
                    }
                    else
                    {
                        ancPreview.Visible = false;
                        ancPreviewDraft.Visible = false;
                    }
                    // bind status of content as active and inactive
                    if (lnkPageName.Text.ToUpper() != "" || lnkPageName.Text.ToUpper() != null)
                    {
                        string OriginalStatus = "";
                        DataTable dtStatus = ServicesFactory.DocCMSServices.Fetch_displayorder_Bycmsid(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text));
                        if (dtStatus != null && dtStatus.Rows.Count > 0)
                        {

                            OriginalStatus = Convert.ToString(dtStatus.Rows[0]["IsActive"]);
                            lblActive.Text = OriginalStatus;
                        }
                        else
                        {
                            ActiveStatusheading.Visible = false;
                            Activestatusdata.Visible = false;
                        }
                    }
                    else
                    {
                        string OriginalStatus = "";
                        DataTable dtStatusDraft = ServicesFactory.DocCMSServices.Fetch_displayorder_Bycmsid_forDraft(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text));
                        if (dtStatusDraft != null && dtStatusDraft.Rows.Count > 0)
                        {

                            OriginalStatus = Convert.ToString(dtStatusDraft.Rows[0]["IsActive"]);
                            lblActive.Text = OriginalStatus;
                        }
                        else
                        {
                            ActiveStatusheading.Visible = false;
                            Activestatusdata.Visible = false;
                        }
                    }
                    if (ServicesFactory.DocCMSServices.Is_Content_Page_Drafted(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text)))
                    {
                        
                        lblCreationDate.Text = ServicesFactory.DocCMSServices.Get_Single_Value_String("Convert(varchar,Creationdate,101)", "Content_Management_Master", "CMSID", lblCMSID.Text, "Number");
                        lblContentState.Text = "Draft";
                        if (Convert.ToInt32(lblCMSID.Text) >= 99999999)
                        {
                            litContentStatus.Text = "<img src='../images/status_workinprogress.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/status_draft.png' style='height:30px;'/>";                            
                        }
                        else
                        {
                             string ContentModifiedRoleID = ServicesFactory.DocCMSServices.Get_Content_modifiedby_pageidAndcmsid(Convert.ToInt32(lblPageID.Text), Convert.ToInt32(lblCMSID.Text));
                             if (ContentModifiedRoleID.ToUpper() == "EEEEEEEE-EEEE-EEEE-EEEE-EEEEEEEEEEEE")
                             {
                                 litContentStatus.Text = "<img src='../images/status_published.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/status_draft.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/ApprovalPending.png' style='height:30px;width:133px;'/>";
                             }
                             else
                             {
                                 litContentStatus.Text = "<img src='../images/status_published.png' style='height:30px;'/>&nbsp;&nbsp;<img src='../images/status_draft.png' style='height:30px;'/>";
                             }
                        }
                    }
                    else
                    {
                        lblCreationDate.Text = ServicesFactory.DocCMSServices.Get_Single_Value_String("Convert(varchar,Creationdate,101)", "Content_Management_Master", "CMSID", lblCMSID.Text, "Number");
                        lblContentState.Text = "Published";
                        litContentStatus.Text = "<img src='../images/status_published.png' style='height:30px;'/>";
                        ancPreviewDraft.Visible = false;
                    }
                    if (lnkPageName.Text.ToUpper() == "SPECIAL BUTTON")
                    {
                        lblContentTitle.Text = ServicesFactory.DocCMSServices.Fetch_Content_Title_SpecialPage(Convert.ToInt32(lblCMSID.Text));
                    }
                    else
                    {
                        lblContentTitle.Text = ServicesFactory.DocCMSServices.Fetch_Content_Title(Convert.ToInt32(lblCMSID.Text));
                    }
                    if (lblContentTitle.Text == null || lblContentTitle.Text == "")
                    {
                        lblContentTitle.Text = ServicesFactory.DocCMSServices.Fetch_Content_Title(Convert.ToInt32(lblCMSID.Text));
                    }
                    LinkContentTitle.Text = Convert.ToString(lblContentTitle.Text);
                    lnkEdit.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    lnkDelete.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                    lblContentTitle.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    thDisplay.Visible = false;
                    trDisplay.Visible = false;
                }
            }
        }

        protected void btnAddContentPageBottom_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["PID"] != null)
            {
                Response.Redirect("~/cadmin/ContentManagement.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]));
            }
        }
    }// Class Ends HEre
}