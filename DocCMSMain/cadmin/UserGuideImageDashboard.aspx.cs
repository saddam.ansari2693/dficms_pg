﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideImageDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        Int32 SourceID;
        string SourceName;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["SID"] != null && Request.QueryString["SN"] != null || Request.QueryString["MDID"] !=null)
            {
                 SourceID = Convert.ToInt32(Request.QueryString["SID"]);
                 SourceName = Convert.ToString(Request.QueryString["SN"]);
                 if (!IsPostBack)
                 {
                   
                    Bind_Images(SourceID, SourceName);
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                }
            }
        }
        protected void Bind_Images(Int32 SourceID, string SourceName)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_images_BasedOn_sourceidAndname(SourceID, SourceName);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrUserGuideImages.DataSource = dt;
                    RptrUserGuideImages.DataBind();
                }
                else
                {
                    RptrUserGuideImages.DataSource = "";
                    RptrUserGuideImages.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrUserGuideImages_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

                Label lblUserRole = (Label)e.Item.FindControl("lblUserRole");
                HtmlImage UserGuideImage = (HtmlImage)e.Item.FindControl("UserGuideImage");
                Label lblImageName = (Label)e.Item.FindControl("lblImageName");
                if (UserGuideImage != null)
                {
                    if (!string.IsNullOrEmpty(lblUserRole.Text))
                    {
                        string sourceName=Convert.ToString(Request.QueryString["SN"]);
                        string sourceid = Convert.ToString(Request.QueryString["SID"]);
                        UserGuideImage.Src = "../UploadedFiles/UserGuide/" + sourceName + "/" + sourceid + "/" + lblUserRole.Text + "/" + lblImageName.Text;
                    }
                    else
                    {
                        UserGuideImage.Src = "../UploadedFiles/No_image_available.png";
                    }
                }
            }
        }

        protected void RptrUserGuideImages_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {

                    case "EditImage":
                        Response.Redirect("~/cadmin/UserGuideImages.aspx?ID="+e.CommandArgument.ToString()+"&SID="+SourceID+"&SN="+SourceName+"");
                        break;

                    case "DeleteImage":
                        Int32 retVal = 0;
                        retVal = ServicesFactory.DocCMSServices.Delete_UserGuideimage(Convert.ToInt32(e.CommandArgument));
                        if (retVal > 0)
                        {
                            ServicesFactory.DocCMSServices.Adjust_image_dorder(SourceID, SourceName);
                            Bind_Images(SourceID,SourceName);
                            Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?SID="+SourceID+"&SN="+SourceName+"");
                        }

                        break;
                    case "OrderUp":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserGuideimage(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "UP", SourceID, SourceName);
                        }
                        Bind_Images(SourceID, SourceName);
                        break;
                    case "OrderDown":
                        if (lblDorder != null)
                        {
                              ServicesFactory.DocCMSServices.Reorder_UserGuideimage(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "DOWN", SourceID, SourceName);
                        }
                        Bind_Images(SourceID, SourceName);
                        break;
                    default:
                        break;

                }
            }
        }

        protected void btnAddUserGuideImageBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideImages.aspx?SID=" + SourceID + "&SN=" + SourceName + "");
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["SN"] == "UserGuideModules")
            {
                Response.Redirect("~/cadmin/UserGuideModuleDashboard.aspx");
            }
            if (Request.QueryString["SN"] == "UserGuideActionDescription")
            {
                Response.Redirect("~/cadmin/UserGuideActionDescriptionDashboard.aspx?MDID=" + Request.QueryString["MDID"] + "&MID=" + Request.QueryString["MID"]);
            }
            else
            {
                Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?MDID=" + Convert.ToString(Request.QueryString["MDID"]) + "&MID=" + Convert.ToString(Request.QueryString["MID"]));
            }
        }
    }
}