﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideActionDescription : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if (Request.QueryString["AID"] != null && Request.QueryString["MDID"]!=null) 
                {
                    hdnModuleDetailID.Value = Convert.ToString(Request.QueryString["MDID"]);
                    Bind_UG_Action_Data(Convert.ToInt32(Request.QueryString["AID"]));
                    Bind_DisplayOrder(Convert.ToInt32(hdnModuleDetailID.Value));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else if (Request.QueryString["MDID"] != null)
                {
                    hdnModuleDetailID.Value = Convert.ToString(Request.QueryString["MDID"]);
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_UserGuideAction_dorder(Convert.ToInt32(hdnModuleDetailID.Value)) + 1;
                    Bind_DisplayOrder(Convert.ToInt32(hdnModuleDetailID.Value));
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        protected void Bind_UG_Action_Data(Int32 ActionDetailID)
        {
            try
            {
                Cls_UGActionDesc objMainContent = ServicesFactory.DocCMSServices.Fetch_UG_ActionData_Byid(ActionDetailID);
                if (objMainContent != null)
                {
                    txtheading.Value = objMainContent.heading;
                    elm1.Value = objMainContent.description;
                    txtDisplayOrder.Value = Convert.ToString(objMainContent.displayorder);
                    ChkActiveStatus.Checked = objMainContent.isactive;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void Bind_DisplayOrder(Int32 ModuleDetailID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UG_ActionDesc_Byid(ModuleDetailID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGuideActionDescriptionDashboard.aspx?MDID=" + Convert.ToInt32(hdnModuleDetailID.Value));
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UGActionDesc objMainContent = new Cls_UGActionDesc();

                objMainContent.heading = txtheading.Value;
                objMainContent.description = elm1.Value;
                objMainContent.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                objMainContent.createdby = Convert.ToString(UserId);
                objMainContent.moduledetailid = Convert.ToInt32(hdnModuleDetailID.Value);
                if (ChkActiveStatus.Checked)
                    objMainContent.isactive = true;
                else
                    objMainContent.isactive = false;

                Int32 retVal = ServicesFactory.DocCMSServices.Insert_UG_Action(objMainContent);
                Response.Redirect("UserGuideActionDescriptionDashboard.aspx?MDID=" + Convert.ToString(Request.QueryString["MDID"]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrDisplayOrder_databound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UGActionDesc objMainContent = new Cls_UGActionDesc();
                objMainContent.heading = txtheading.Value;
                objMainContent.description = elm1.Value;

                objMainContent.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                objMainContent.modifiedby = Convert.ToString(UserId);
                objMainContent.actiondetailid = Convert.ToInt32(Request.QueryString["AID"]);
                objMainContent.moduledetailid = Convert.ToInt32(hdnModuleDetailID.Value);
                if (ChkActiveStatus.Checked)
                    objMainContent.isactive = true;
                else
                    objMainContent.isactive = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UG_Action(objMainContent);
                Response.Redirect("UserGuideActionDescriptionDashboard.aspx?MDID=" + Convert.ToString(Request.QueryString["MDID"]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UGActionDesc> lstModules = new List<Cls_UGActionDesc>();
            Int32[] Dorder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblmaincontentid = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblmaincontentid");
                Label lblText = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblText");
                if (Session["UserID"] != null && lblmaincontentid != null && lblText != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    Dorder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblText.Text + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(hdnModuleDetailID.Value));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(Dorder);

            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < Dorder.Length - 1; j++)
            {
                if (Dorder[j] < Dorder[j + 1])
                {
                    if (Dorder[j] == Dorder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnModuleDetailID.Value));
                        return;
                    }
                }
                else if (Dorder[j] == Dorder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(hdnModuleDetailID.Value));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UGActionDesc objMainContent = new Cls_UGActionDesc();
                Label lblMainContentID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblMainContentID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblMainContentID != null)
                {
                    objMainContent.actiondetailid = Convert.ToInt32(lblMainContentID.Text.Trim());
                    objMainContent.displayorder = Convert.ToInt32(txtDorder.Value);
                    objMainContent.modifiedby = Convert.ToString(UserId);
                    lstModules.Add(objMainContent);
                }
            }
            if (lstModules != null && lstModules.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UG_Action_displayorder(lstModules);
                if (retVal > 0)
                {

                    if (Request.QueryString["MID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["MID"] + ");</script>");
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
        }
    }
}