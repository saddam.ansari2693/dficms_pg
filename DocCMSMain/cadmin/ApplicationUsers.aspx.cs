﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class ApplicationUsers : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["AID"] != null)
                {
                    Bind_ApplicationUser_Data(Convert.ToInt32(Request.QueryString["AID"]));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Module Data when Module Id is alerady existing
        protected void Bind_ApplicationUser_Data(Int32 ApplicationUserID)
        {
            try
            {
                Cls_ApplicationUser obj = ServicesFactory.DocCMSServices.Fetch_ApplicationUser_Byid(ApplicationUserID);
                if (obj != null)
                {
                    txtEmail.Value = obj.email;
                    txtEmployeeName.Value = obj.name;
                    txtUsername.Value = Convert.ToString(obj.username);
                    ChkActiveStatus.Checked = obj.isactive;
                    txtPassowrd.Value = ServicesFactory.DocCMSServices.Decrypt(obj.password);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_ApplicationUser cls = new Cls_ApplicationUser();
                cls.name = txtEmployeeName.Value;
                cls.email = txtEmail.Value;
                cls.username = txtUsername.Value;
                cls.password = ServicesFactory.DocCMSServices.Encrypt(txtPassowrd.Value);
                cls.createdby = "sdsd";
                if (ChkActiveStatus.Checked)
                    cls.isactive = true;
                else
                    cls.isactive = false;

                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_Module(txtUsername.Value.Trim());
                if (RetCheck > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('User with Same Name Already Exists...');</script>");
                    return;
                }
                else
                {
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_ApplicationUsers(cls);
                    Response.Redirect("~/cadmin/ApplicationUsersDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_ApplicationUser cls = new Cls_ApplicationUser();
                cls.name = txtEmployeeName.Value;
                cls.email = txtEmail.Value;
                cls.username = txtUsername.Value;
                cls.password = ServicesFactory.DocCMSServices.Encrypt(txtPassowrd.Value);
                cls.lastmodifiedby = "sdsd";
                cls.applicationuserid = Convert.ToInt32(Request.QueryString["AID"]);
                if (ChkActiveStatus.Checked)
                    cls.isactive = true;
                else
                    cls.isactive = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_ApplicationUsers(cls);
                Response.Redirect("~/cadmin/ApplicationUsersDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/ApplicationUsersDashboard.aspx");
        }
    }
}