﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SliderItemDetailsPage.aspx.cs" Inherits="DocCMSMain.cadmin.SliderItemDetailsPage1" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="css/colopicker/js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/colopicker/css/jquery.minicolors.css" />
    <script type="text/javascript" src="css/colopicker/js/jquery.minicolors.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var colpick = $('.demo').each(function () {
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: 'lowercase',
                    opacity: false,
                    change: function (hex, opacity) {
                        if (!hex) return;
                        if (opacity) hex += ', ' + opacity;
                        try {
                            console.log(hex);
                        } catch (e) { }
                        $(this).select();
                    },
                    theme: 'bootstrap'
                });
            });

            var $inlinehex = $('#inlinecolorhex h3 small');
            $('#inlinecolors').minicolors({
                inline: true,
                theme: 'bootstrap',
                change: function (hex) {
                    if (!hex) return;
                    $inlinehex.html(hex);
                }
            });
        });

        function isNumber(evt) {
            var key;
            if (window.evt) {
                key = window.evt.keyCode;     //IE
            }
            else {
                key = evt.which;      //firefox              
            }
            if ((key >= 48 && key <= 57)) {
                return true;
            }
            else if (key == 0 || key == 8 || key == 118 || key == 45)//|| key == 9 || key == 27 || key == 32 || key == 35 || key == 46 || key == 59 || key == 64 || key == 38 || key == 39 || key == 40 || key == 41 || key == 46 || key == 91 || key == 93 || key == 123 || key == 125 || key == 127)) {
            {
                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {
            if ($("#chkIsEmphasized").is(":checked")) {
                $('#txtTextBackgroundColor').attr('disabled', false);
            }
            else if ($("#chkIsEmphasized").is(":not(:checked)")) {
                $('#txtTextBackgroundColor').attr('disabled', true);
                $('#txtTextBackgroundColor').val("");
            }
            if ($('#ddlItemType').val() == "Text") {
                $('#DivImage').hide();
                $('#DivContent').show();
                $('#DivContentPosition').addClass("Show");
                $('#DivFontSize').addClass("Show");
                $('#DivFontColor').addClass("Show");
                $('#DivBold').addClass("Show");
                $('#DivItalic').addClass("Show");
                $('#DivUnderline').addClass("Show");
                $('#DivEmphasized').addClass("Show");
                $('#DivTextBackGroundColor').addClass("Show");
            }
            else {
                $('#DivImage').show();
                $('#DivContent').hide();
                $('#DivContentPosition').addClass("Hide");
                $('#DivFontSize').addClass("Hide");
                $('#DivFontColor').addClass("Hide");
                $('#DivBold').addClass("Hide");
                $('#DivItalic').addClass("Hide");
                $('#DivUnderline').addClass("Hide");
                $('#DivEmphasized').addClass("Hide");
                $('#DivTextBackGroundColor').addClass("Hide");

            }
        });

        function ManageEmphasized() {
            if ($("#chkIsEmphasized").is(":checked")) {
                $('#txtTextBackgroundColor').attr('disabled', false);
            }
            else if ($("#chkIsEmphasized").is(":not(:checked)")) {
                $('#txtTextBackgroundColor').attr('disabled', true);
                $('#txtTextBackgroundColor').val("");
            }
        }

        function SaveMsg(Alias) {
            alert("SMTP Detail " + Alias + " Suceessfully");
            var url = "";
            url = "../cadmin/SMTPSettings.aspx";
            $(location).attr('href', url);

        }
        function ManageDimensions() {
            if ($("#chkFullWidth").is(":checked")) {
                $("#txtWidth").val("0");
                $("#txtWidth").attr("disabled", true);
            }
            else if ($("#chkFullWidth").is(":not(:checked)")) {
                $("#txtWidth").attr("disabled", false);
            }
        }

        //Start Image Preview Function
        function ShowPreview(input) {
            if (input.files && input.files[0]) {
                var ImageDir = new FileReader();
                ImageDir.onload = function (e) {
                    $('#ImgPreview').attr('src', e.target.result);
                }
                ImageDir.readAsDataURL(input.files[0]);
            }
        }

        //End Image Preview Function

        //Start DropDown change Event
        function SelectValueOnChange(e) {
            if ($("#" + e.id + " option:selected").text() == "Text") {
                $('#DivImage').hide();
                $('#DivContent').show();
                $('#DivContentPosition').addClass("Show");
                $('#DivFontSize').addClass("Show");
                $('#DivFontColor').addClass("Show");
                $('#DivBold').addClass("Show");
                $('#DivItalic').addClass("Show");
                $('#DivUnderline').addClass("Show");
                $('#DivEmphasized').addClass("Show");
                $('#DivTextBackGroundColor').addClass("Show");
                $('#DivContentPosition').removeClass("Hide");
                $('#DivFontSize').removeClass("Hide");
                $('#DivFontColor').removeClass("Hide");
                $('#DivBold').removeClass("Hide");
                $('#DivItalic').removeClass("Hide");
                $('#DivUnderline').removeClass("Hide");
                $('#DivEmphasized').removeClass("Hide");
                $('#DivTextBackGroundColor').removeClass("Hide");
            }
            else {
                $('#DivImage').show();
                $('#DivContent').hide();
                $('#DivContentPosition').addClass("Hide");
                $('#DivFontSize').addClass("Hide");
                $('#DivFontColor').addClass("Hide");
                $('#DivBold').addClass("Hide");
                $('#DivItalic').addClass("Hide");
                $('#DivUnderline').addClass("Hide");
                $('#DivEmphasized').addClass("Hide");
                $('#DivTextBackGroundColor').addClass("Hide");
                $('#DivContentPosition').removeClass("Show");
                $('#DivFontSize').removeClass("Show");
                $('#DivFontColor').removeClass("Show");
                $('#DivBold').removeClass("Show");
                $('#DivItalic').removeClass("Show");
                $('#DivUnderline').removeClass("Show");
                $('#DivEmphasized').removeClass("Show");
                $('#DivTextBackGroundColor').removeClass("Show");
            }
        }
        //End DropDown change Event
    </script>
    <style type="text/css">
        .Hide
        {
            display: none;
        }
        .Show
        {
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Manage Slider</a> <span class="separator"></span></li>
        <li>Slider Details Settings</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Slider</h5>
            <h1>
                Slider Details Settings</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent" id="DivSurveyContent" runat="server">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <asp:HiddenField ID="hdnSliderId" runat="server" />
            <asp:HiddenField ID="hdnSliderItemID" runat="server" />
            <asp:HiddenField ID="hdnImageNameForUpdate" runat="server" />
            <div class="widget">
                <h4 class="widgettitle">
                    Slider Details Settings</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform" id="DivSMTPPage" runat="server">
                        <div class="par control-group">
                            <label class="control-label" for="txtImageName">
                                Item Type</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlItemType" runat="server" ClientIDMode="Static" onchange="SelectValueOnChange(this)">
                                    <asp:ListItem Value="Image" Text="Image"></asp:ListItem>
                                    <asp:ListItem Value="Text" Text="Text"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="par control-group" id="DivImage">
                            <label class="control-label" for="txtImageName">
                                Image</label>
                            <div class="controls">
                                <asp:FileUpload ID="FileUploadSlides" runat="server" ClientIDMode="Static" onchange="ShowPreview(this)" />
                                <div style="margin-left: 5%; margin-top: -0%; width: 80px; display: inline;">
                                    <img id="ImgPreview" clientidmode="Static" style="width: 80px; height: 50px; margin-top: -12px;"
                                        runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="par control-group" id="DivContent">
                            <label class="control-label" for="txtheight">
                                Item Content</label>
                            <div class="controls">
                                <asp:TextBox ID="txtItemContent" runat="server" CssClass="input-large" ClientIDMode="Static"
                                    ValidationGroup="grpsubmit" placeholder="Enter Item Content"></asp:TextBox>
                            </div>
                        </div>
                        <div class="par control-group" id="DivContentPosition">
                            <label class="control-label" for="txtheight">
                                Content Position</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlContentPosition" runat="server">
                                    <asp:ListItem Value="Top-Left" Text="Top-Left"></asp:ListItem>
                                    <asp:ListItem Value="Bottom-Left" Text="Bottom-Left"></asp:ListItem>
                                    <asp:ListItem Value="Top-Right" Text="Top-Right"></asp:ListItem>
                                    <asp:ListItem Value="Bottom-Right" Text="Bottom-Right"></asp:ListItem>
                                    <asp:ListItem Value="Top-Center" Text="Top-Center"></asp:ListItem>
                                    <asp:ListItem Value="Bottom-Center" Text="Bottom-Center"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="par control-group" id="DivFontSize">
                            <label class="control-label" for="txtWidth">
                                Font Size
                            </label>
                            <div class="controls">
                                <asp:TextBox ID="txtFontSize" runat="server" CssClass="input-large" ClientIDMode="Static"
                                    onkeypress="javascript:return isNumber(event)" MaxLength="10" ValidationGroup="grpsubmit"
                                    placeholder="Enter Font Size"></asp:TextBox>
                            </div>
                        </div>
                        <div class="par control-group" id="DivFontColor">
                            <label class="control-label" style="margin-top: 0px;">
                                Font Color</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFontColor" runat="server" CssClass="input-large demo" ClientIDMode="Static"
                                    Style="width: 732px; height: 24px; margin-bottom: 0;" placeholder="Enter Font Color"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="par control-group" id="DivBold">
                            <label class="control-label" style="margin-top: -10px;">
                                <b>Bold</b></label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="IsBold" id="chkIsBold" class="input-large"
                                    clientidmode="Static" style="opacity: 1;" checked />
                            </div>
                        </div>
                        <div class="par control-group" id="DivItalic">
                            <label class="control-label" style="margin-top: -10px;">
                                <i>Italic</i></label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="IsItalic" id="chkIsItalic" class="input-large"
                                    clientidmode="Static" style="opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group" id="DivUnderline">
                            <label class="control-label" style="margin-top: -10px;">
                                <u>Underline</u></label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="IsUnderline" id="chkIsUnderline" class="input-large"
                                    clientidmode="Static" style="opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group" id="DivEmphasized">
                            <label class="control-label" style="margin-top: -10px;">
                                <em>Emphasized</em></label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="IsEmphasized" id="chkIsEmphasized" onchange="return ManageEmphasized();"
                                    class="input-large" clientidmode="Static" style="opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group" id="EntryStyle">
                            <label class="control-label" style="margin-top: 0px;">
                                Entry Style</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlEntryStyle" runat="server">
                                    <asp:ListItem Value="Top" Text="Top"></asp:ListItem>
                                    <asp:ListItem Value="Bottom" Text="Bottom"></asp:ListItem>
                                    <asp:ListItem Value="Left" Text="Left"></asp:ListItem>
                                    <asp:ListItem Value="Right" Text="Right"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: 0px;">
                                Delay (miliseconds)</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDelay" runat="server" CssClass="input-large" ClientIDMode="Static"
                                    Text="0" onkeypress="javascript:return isNumber(event)" MaxLength="10" ValidationGroup="grpsubmit"
                                    placeholder="Enter Delay for entry"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqdDelay" ValidationGroup="grpsubmit" ControlToValidate="txtDelay"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: 0px;">
                                Margin Top</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMarginTop" runat="server" type="number" CssClass="input-large"
                                    ClientIDMode="Static" Text="0" onkeypress="javascript:return isNumber(event)"
                                    MaxLength="10" placeholder="Enter margin from top into pixels"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqdMarginTop" ValidationGroup="grpsubmit" ControlToValidate="txtMarginTop"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: 0px;">
                                Margin Bottom</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMarginBottom" runat="server" type="number" CssClass="input-large"
                                    ClientIDMode="Static" Text="0" onkeypress="javascript:return isNumber(event)"
                                    MaxLength="10" ValidationGroup="grpsubmit" placeholder="Enter margin from bottom into pixels"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqdMarginBottom" ValidationGroup="grpsubmit" ControlToValidate="txtMarginBottom"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: 0px;">
                                Margin Right</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMarginRight" runat="server" type="number" CssClass="input-large"
                                    ClientIDMode="Static" Text="0" onkeypress="javascript:return isNumber(event)"
                                    MaxLength="10" ValidationGroup="grpsubmit" placeholder="Enter margin from right into pixels"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqdMarginRight" ValidationGroup="grpsubmit" ControlToValidate="txtMarginRight"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: 0px;">
                                Margin Left</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMarginLeft" runat="server" type="number" CssClass="input-large"
                                    ClientIDMode="Static" Text="0" onkeypress="javascript:return isNumber(event)"
                                    MaxLength="10" ValidationGroup="grpsubmit" placeholder="Enter margin from left into pixels"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqdMarginLeft" ValidationGroup="grpsubmit" ControlToValidate="txtMarginLeft"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group" id="DivTextBackGroundColor">
                            <label class="control-label" style="margin-top: 0px;">
                                Emphasized Color</label>
                            <div class="controls">
                                <asp:TextBox ID="txtTextBackgroundColor" runat="server" ClientIDMode="Static" CssClass="input-large demo"
                                    Style="width: 732px; height: 24px; margin-bottom: 0; display: inline;" ValidationGroup="grpsubmit"
                                    placeholder="Enter text background color" Text="#ff6161"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="par control-group">
                            <label class="control-label" for="txtNavigationUrl">
                                Navigation URL</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtNavigationUrl" id="txtNavigationUrl" class="input-large autocomplete"
                                    clientidmode="Static" placeholder="Enter Navigation URl" />
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtDataEffect">
                                Data Effect</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtDataEffect" id="txtDataEffect" class="input-large autocomplete"
                                    clientidmode="Static" placeholder="Data Effect" />
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: -10px;">
                                Display Order</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDorder" runat="server" Enabled="false" CssClass="input-large"
                                    ClientIDMode="Static" onkeydown="return onlyNumbers(event);" ValidationGroup="grpsubmit"
                                    placeholder="Enter Display Order"></asp:TextBox>
                                <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                    Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click" />
                                <asp:RequiredFieldValidator ID="rqdDorder" ValidationGroup="grpsubmit" ControlToValidate="txtDorder"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" style="margin-top: 0px;">
                                Active</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    clientidmode="Static" style="opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group">
                            <p class="stdformbutton">
                                <asp:Button ID="btnsubmitItemDetails" runat="server" class="btn btn-primary" Visible="false"
                                    OnClick="btnsubmitItemDetails_onclick" Text="Save" Width="150px" ValidationGroup="grpsubmit" />
                                <asp:Button ID="btnMasterItemupdateDetails" runat="server" class="btn btn-primary"
                                    OnClick="btnMasterItemupdateDetails_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit"
                                    Visible="false" />
                                <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                    OnClick="btnCancel_Click" />
                            </p>
                        </div>
                    </div>
                </div>
                <!--------------------Start display order model popup---------------------->
                <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                                <h4 id="myModalLabel" class="modal-title">
                                    Update Slider Details Display Orders
                                </h4>
                            </div>
                            <div class="modal-body" style="height: 640px;">
                                <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                    <colgroup>
                                        <col class="con1" style="align: left; width: 70%;" />
                                        <col class="con0" style="align: right; width: 30%;" />
                                    </colgroup>
                                    <thead>
                                        <tr style="width: 100%;">
                                            <th>
                                                Slider&nbsp;Info
                                            </th>
                                            <th id="ActiveStatusheading" runat="server">
                                                Status
                                            </th>
                                            <th>
                                                Display&nbsp;Order
                                                <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                    onclick="ToggleDisplay();" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="overflow-y: auto;">
                                        <asp:Repeater runat="server" ID="RptrDisplayOrder" OnItemDataBound="RptrDisplayOrder_databound">
                                            <ItemTemplate>
                                                <tr class="gradeX">
                                                    <td>
                                                        <asp:Label ID="lblSliderItemContentID" runat="server" Text='<%# Eval("SliderItemContentID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:HiddenField ID="hdnItemType" runat="server" Value='<%# Eval("ItemType") %>' />
                                                        <asp:Image ID="Imgslider" runat="server" ImageUrl='<%#"../UploadedFiles/SliderItemDetails/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>'
                                                            Style="width: 50px; height: 50px;" />
                                                        <asp:Label ID="lblText" runat="server" Text='<%# Eval("ItemContent") %>'></asp:Label>
                                                    </td>
                                                    <td id="Activestatusdata" runat="server">
                                                        <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderItemContentID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                            CommandName="EditPageContent"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                        <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)"
                                                            maxlength="5" class="DorderText" style="display: none; width: 100px; float: left;
                                                            margin-right: 25px;" value='<%# Eval("Dorder") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tr class="gradeA">
                                                    <td>
                                                        <asp:Label ID="lblSliderItemContentID" runat="server" Text='<%# Eval("SliderItemContentID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:HiddenField ID="hdnItemType" runat="server" Value='<%# Eval("ItemType") %>' />
                                                        <asp:Image ID="Imgslider" runat="server" ImageUrl='<%#"../UploadedFiles/SliderItemDetails/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/"+Eval("ImageName")%>'
                                                            Style="width: 50px; height: 50px;" />
                                                        <asp:Label ID="lblText" runat="server" Text='<%# Eval("ItemContent") %>'></asp:Label>
                                                    </td>
                                                    <td id="Activestatusdata" runat="server">
                                                        <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderItemContentID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                            CommandName="EditPageContent"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                        <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)"
                                                            maxlength="5" class="DorderText" style="display: none; width: 100px; float: left;
                                                            margin-right: 25px;" value='<%# Eval("Dorder") %>' />
                                                    </td>
                                                </tr>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btnUpdate" class="btn btn-primary" onclick="ValidateDisplayOrders();"
                                    value="Update Display Orders" />
                                <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                    style="display: none;"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                    OnClick="btnUpdateDisplayOrder_Click" />
                <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
                <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                    TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
                </asp:ModalPopupExtender>
                <!-------------------End display order model popup---------------------->
                <%-- </form>--%>
            </div>
            <!--widgetcontent-->
        </div>
        <!--widget-->
        <asp:HiddenField ID="hdnSMTPId" runat="server" />
        <asp:HiddenField ID="hdnSMTPDetailId" runat="server" />
        <uc1:CadminFooter ID="CadminFooter1" runat="server" />
        <!-- for Display Order Popup -->
    </div>
    <!--maincontentinner-->
    <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
        role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
        top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
    </ul>
    </div>
    <!--maincontent-->
    <!----Question Content ends here--->
    <script language="javascript" type="text/javascript">
       function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
        }

        function AddQues() {
            var ImgAddQuestion = document.getElementById("ImgAddQuestion");
            ImgAddQuestion.click();
        }
        function AddAns() {
            var ImgAddAnswer = document.getElementById("ImgAddAnswer");
            ImgAddAnswer.click();
        }

        function RemoveFeature(elem) {
            var id = $(elem).attr("id");
            var GetId = id.toString().split('_');
            $("#divFeature_" + GetId[1]).style.visibility = "hidden";
        }
    </script>
    <style type="text/css">
     .minicolors
     {
         margin-left: 222px;
         height: 20px;
     }
    </style>
</asp:Content>
