﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="MediaManagementPage.aspx.cs" Inherits="DocCMSMain.cadmin.MediaManagementPage"  ValidateRequest="false" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="../Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="../css/isotope.css" type="text/css" />
    <link rel="stylesheet" href="../css/bootstrap-fileupload.min.css" type="text/css" />
    <script type="text/javascript" src="../js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="../js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>        
    <script src="../js/Pages/media_manager.js" type="text/javascript"></script>
    <link href="css/colopicker/css/jquery.minicolors.css" rel="stylesheet" type="text/css" />
    <script src="css/colopicker/js/jquery.minicolors.min.js" type="text/javascript"></script>
    <link href="../js/Jcrop/Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../js/Jcrop/Jcrop.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(function () {
            var colpick = jQuery('.demo').each(function () {
                jQuery(this).minicolors({
                    control: jQuery(this).attr('data-control') || 'hue',
                    inline: jQuery(this).attr('data-inline') === 'true',
                    letterCase: 'lowercase',
                    opacity: false,
                    change: function (hex, opacity) {
                        if (!hex) return;
                        if (opacity) hex += ', ' + opacity;
                        try {
                            console.log(hex);
                        } catch (e) { }
                        jQuery(this).select();
                    },
                    theme: 'bootstrap'
                });
            });

            var $inlinehex = jQuery('#inlinecolorhex h3 small');
            jQuery('#inlinecolors').minicolors({
                inline: true,
                theme: 'bootstrap',
                change: function (hex) {
                    if (!hex) return;
                    $inlinehex.html(hex);
                }
            });
        });
        jQuery(document).ready(function () {
            OnPageLoad();
            //Replaces data-rel attribute to rel.
            //We use data-rel because of w3c validation issue
            jQuery('a[data-rel]').each(function () {
                jQuery(this).attr('rel', jQuery(this).data('rel'));
            });

            jQuery("#medialist a").colorbox();
            jQuery("#chkResizeImageCreate").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#chkCustomeImageCreate").prop("checked", false);
                    jQuery("#divCustomResizeImageCreate").show();
                    jQuery("#divCustomImageCreate").hide();
                    jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                    jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                    CheckResizeValues();
                } else {
                    jQuery("#chkCustomeImageCreate").prop("checked", true);
                    jQuery("#divCustomResizeImageCreate").hide();
                    jQuery("#divCustomImageCreate").show();
                    jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
                    jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
                }
            });


            jQuery("#chkCustomeImageCreate").click(function () {//
                if (jQuery(this).is(':checked')) {
                    jQuery("#chkResizeImageCreate").prop("checked", false);
                    jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
                    jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
                    jQuery("#divCustomImageCreate").show();
                    jQuery("#divCustomResizeImageCreate").hide();
                    var MiddleImageWidth = parseInt(jQuery("#hdnActualImageWidth").val()) / 6;
                    var MiddleImageHeight = parseInt(jQuery("#hdnActualImageHeight").val()) / 6;
                    var MarginTopAndBottom = parseInt(200 - MiddleImageHeight) / 2;
                    jQuery("#ImgCustomeImagePreviewRunning").css("margin-top", parseInt(MarginTopAndBottom));
                    jQuery("#ImgCustomeImagePreviewRunning").css("margin-bottom", parseInt(MarginTopAndBottom));
                    jQuery("#ImgCustomeImagePreviewRunning").css("width", parseInt(MiddleImageWidth));
                    jQuery("#ImgCustomeImagePreviewRunning").css("height", parseInt(MiddleImageHeight));
                } else {
                    jQuery("#chkResizeImageCreate").prop("checked", true);
                    jQuery("#divCustomImageCreate").hide();
                    jQuery("#divCustomResizeImageCreate").show();
                    jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                    jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                }
            });

            jQuery("#chkFinalCrop").click(function () {//
                if (jQuery(this).is(':checked')) {

                    Set_Cropper(1);
                    jQuery("#btnCropWithResize").show();
                } else {
                    Set_Cropper(0);
                    jQuery("#btnCropWithResize").hide();
                }
            });

            jQuery("#chkAspectRatioResize").click(function () {//
                if (jQuery(this).is(':checked')) {
                    jQuery("#hdnResizeAsepctRatio").val("True");
                    setTimeout(function () { PreviewResizedImage(); }, 200);
                    jQuery("#uniform-chkAspectRatioResize").find("span").addClass("checked");

                } else {
                    jQuery("#hdnResizeAsepctRatio").val("False");
                    jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
                    setTimeout(function () { PreviewResizedImage(); }, 200);
                }
            });

            jQuery("#chkFinalCrop").click(function () {//
                if (jQuery(this).is(':checked')) {
                    Set_Cropper(1);
                    jQuery("#btnCropWithResize").show();
                } else {
                    Set_Cropper(0);
                    jQuery("#btnCropWithResize").hide();
                }
            });
        });

        function OnPageLoad() {
            jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
            jQuery("#chkResizeImageCreate").prop("checked", true);
            jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
            jQuery("#chkCustomeImageCreate").prop("checked", false);
            jQuery("#txtCustomWidth").val("1800");
            jQuery("#txtCustomHeight").val("1200");
            jQuery("#chkAspectRatioResize").prop("checked", false);
            jQuery("#hdnResizeAsepctRatio").val("False");
            jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
        }
        jQuery(window).load(function () {
            jQuery('#medialist').isotope({
                itemSelector: 'li',
                layoutMode: 'fitRows'
            });

            // Media Filter
            jQuery('#mediafilter a').click(function () {
                var filter = (jQuery(this).attr('href') != 'all') ? '.' + jQuery(this).attr('href') : '*';
                jQuery('#medialist').isotope({ filter: filter });
                jQuery('#mediafilter li').removeClass('current');
                jQuery(this).parent().addClass('current');
                return false;
            });
        });

        function ValidateFileSize() {
            var file = "";
            var Ext = "";
            var labFn = document.getElementById('ContentPlaceHolder1_labfilename');
            if (labFn != null) {
                file = labFn.innerHTML;
            }
            var FileUploadControl = document.getElementById('ContentPlaceHolder1_FileUploadControl');
            if (FileUploadControl.value != "") {
                file = FileUploadControl.files[0];
                var size = file.size;
                if (size > 52428800) {
                    jQuery('#ContentPlaceHolder1_lblSizeError').html("File Size limit Exceeds. not more than 50 MB allowed");
                    return false;
                }
                file = FileUploadControl.value;
                Ext = FileUploadControl.value.toString().split('.');
            }
            else {
                Ext = file.toString().split('.');
            }
            var RemovedWord = "";
            var msg = "";
            var FileExtention = Ext[Ext.length - 1];
            if (FileExtention == "png" || FileExtention == "PNG" || FileExtention == "gif" || FileExtention == "GIF" || FileExtention == "jpg" || FileExtention == "JPG" || FileExtention == "jpeg" || FileExtention == "JPEG") {
                var pass = Section508(file);
                if (pass == "Blank" || pass == "MinLength") {
                    msg = "Please write your \'alt text\' so that it\'s at least 3 words long and so that it does not contain any of the words from the \'" + FileUploadControl.value + "\' filename.";
                    msg = msg + "\n\n\nPlease click the OK button to continue with your changes.";
                }
                else {
                    if (pass != "") {
                        var DupWords = pass.substring(1).split(',');
                        for (var j = 0; j < DupWords.length; j++) {
                            if (DupWords[j] != "") {
                                RemovedWord = RemovedWord + (j + 1).toString() + ".) " + DupWords[j].toString() + "\n\n      ";
                            }
                        }
                        msg = "Please write your \'alt text\' so that it\'s at least 3 words long and so that it does not contain any of the words from the \'" + FileUploadControl.value + "\' filename.";
                        msg = msg + "\n";
                        msg = msg + "\n      NOTE: Please do not include the following words in your alt text:\n\n";
                        msg = msg + "      " + RemovedWord;
                        msg = msg + "\n";
                        msg = msg + "Please click the OK button to continue with your changes.";

                    }
                }
                if (msg != "") {
                    if (confirm(msg)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            }
        }

        function Section508(FileName) {
            var file = FileName;
            if (file.toString() == "") {
                alert("Fine name Is blank");
                remWord = "Blank";
                return remWord;
            }
            var AltText = document.getElementById('ContentPlaceHolder1_txtalternatetext').value;
            var remWord = "";
            if (AltText.toString() == "") {
                remWord = "Blank";
                return remWord;
            }
            var strFile = AltText.trim().split(" ");
            if (strFile.length < 3) {
                remWord = "MinLength";
                return remWord;
            }
            var len = 0;
            for (var i = 0; i < strFile.length; i++) {
                if (strFile[i].toString().trim() != "") {
                    len = len + 1;
                    if (file.toUpperCase().indexOf(strFile[i].toString().trim().toUpperCase()) > -1) {
                        remWord = remWord + "," + strFile[i].toString();
                    }
                }
            }
            if (len < 3) {
                remWord = "MinLength";
                return remWord;
            }
            if (remWord.replace(/,/g, "") == "") {
                return "";
            }
            else {
                return remWord;
            }
        }

        function Button1_onclick() {

        }


        // function change event of color picker
        function ColorChange(Textbox) {
            var colorName = jQuery("#" + Textbox.id).val();
            var Image = jQuery("#ImgResizeImage").attr("src");
            jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
            jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
        }

        function ManageAspectRatio(ratioparam, e) {
           var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
                if (jQuery("#chkAspectRatioResize").is(':checked')) {
                    if (ratioparam == "w") {
                        var width = jQuery("#txtCustomWidth").val();
                        if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                            jQuery("#txtCustomWidth").css("color", "Red");
                            jQuery("#txtCustomHeight").val("0");
                        }
                        else {
                            var height = parseFloat(jQuery("#txtCustomWidth").val()) / 1.333;
                            jQuery("#txtCustomHeight").val(parseInt(height));
                        }
                    }
                    else {
                        var height = jQuery("#txtCustomHeight").val();
                        if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                            jQuery("#txtCustomHeight").css("color", "Red");
                            jQuery("#txtCustomWidth").val("0");
                        }
                        else {
                            var width = parseFloat(jQuery("#txtCustomHeight").val()) * 1.333;
                            jQuery("#txtCustomWidth").val(parseInt(width));
                        }
                    }
                }
                else {
                    jQuery("#ImgResizePreview").Jcrop({
                        onSelect: SelectCropArea,
                        bgFade: true

                    }, function () {
                        jcrop_api = this;
                    });
                    jcrop_api.destroy();
                    PreviewResizedImage();
                    jQuery("#ImgResizePreview").css("visibility", "visible");
                }
            }
            else {
                return false;
            }
        }
        
        function CheckResizeValues() {
            setTimeout(function () { PreviewResizedImage(); }, 200);
        }

        function PreviewResizedImage() {
            var actWidth = jQuery("#hdnActualImageWidth").val();
            var actHeight = jQuery("#hdnActualImageHeight").val();
            var resizeWidth = jQuery("#txtCustomWidth").val();
            var resizeHeight = jQuery("#txtCustomHeight").val();
            if (jQuery("#hdnResizeAsepctRatio").val() == "True") {
                var ratioX = parseInt(resizeWidth / 6) / parseInt(actWidth / 6);
                var ratioY = parseInt(resizeHeight / 6) / parseInt(actHeight / 6);
                var ratio = Math.max(ratioX, ratioY);
                var newWidth = parseInt(parseInt(resizeWidth / 6) * ratio);
                var newHeight = parseInt(parseInt(resizeHeight / 6) * ratio);
                newWidth = parseInt(resizeWidth / 6);
                newHeight = parseInt(resizeHeight / 6);
                jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                jQuery("#ImgResizePreview").css("max-width", "none");
            }
            else {
                var newWidth = parseInt(resizeWidth / 6);
                var newHeight = parseInt(resizeHeight / 6);
                jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                jQuery("#ImgResizePreview").css("max-width", "none");
            }
            CheckImageFits(newHeight, newWidth);
        }

        function CheckImageFits(newHeight, newWidth) {
            var RequiredWidth = parseInt(jQuery("#divResizePreview").css("width"));
            var Requiredheight = parseInt(jQuery("#divResizePreview").css("height"));
            if ((RequiredWidth < newWidth) || (Requiredheight < newHeight)) {
                jQuery("#lblUnfitMsg").show();
                jQuery("#spnFinalCrop").show();
                jQuery("#btnCropWithResize").show();
                jQuery("#btnResizeImage").hide();
            }
            else {
                jQuery("#lblUnfitMsg").hide();
                jQuery("#spnFinalCrop").hide();
                jQuery("#btnResizeImage").show();
                jQuery("#btnCropWithResize").hide();
            }
            return false;
        }

        //Ready Function Close

        function Set_Cropper(setVal) {
            if (setVal == 1) {
                var reqsize = jQuery("#ddlSizeResize").val().split('x');
                var orientation = jQuery("#ddlOrientationResize").val();
                var cropWidth = 0;
                var cropHeight = 0;
                var CropSize = "";
                var cropHeight = 0;
                if (orientation == "Portrait") {
                    cropWidth = (parseInt(reqsize[0]) * 300) / 6;
                    cropHeight = (parseInt(reqsize[1]) * 300) / 6
                    CropSize = reqsize[0] + ' x ' + reqsize[1];
                }
                else {
                    cropWidth = (parseInt(reqsize[1]) * 300) / 6;
                    cropHeight = (parseInt(reqsize[0]) * 300) / 6
                    CropSize = reqsize[1] + ' x ' + reqsize[0];
                }
                jQuery("#maxHeight").val(cropHeight.toString());
                jQuery("#maxWidth").val(cropWidth.toString());
                var imagewidth = parseInt(jQuery("#txtCustomWidth").val());
                var imageHeight = parseInt(jQuery("#txtCustomHeight").val());
                jQuery("#ImgResizePreview").Jcrop({
                    onSelect: SelectCropArea,
                    onChange: SelectCropArea,
                    bgFade: true                
                }, function () {
                    jcrop_api = this;
                    jcrop_api.setOptions({ allowSelect: true });
                    jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
                    jcrop_api.animateTo([10, 20, ((imagewidth - cropWidth) / 2) + cropWidth, ((imageHeight - cropHeight) / 2) + cropHeight]);
                    // Use the API to get the real image size
                    jcrop_api.focus();
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                });
            }
            else {
                jcrop_api.destroy();
                jQuery("#ImgResizePreview").css("visibility", "visible");
            }
        }


        var boundx, boundy;
        function SelectCropArea(c) {
            // Grab some information about the preview pane            
            var preview = jQuery('#preview-pane');
            var pcnt = jQuery('#preview-pane .preview-container');
            var pimg = jQuery('#preview-pane .preview-container img');
            var xsize = pcnt.width(),
            ysize = pcnt.height();
            jQuery('#XAxis').val(parseInt(c.x));
            jQuery('#YAxis').val(parseInt(c.y));
            jQuery('#Width').val(parseInt(c.w));
            jQuery('#Height').val(parseInt(c.h));
            jQuery('#x1').val(c.x);
            jQuery('#y1').val(c.y);
            jQuery('#x2').val(c.x2);
            jQuery('#y2').val(c.y2);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
            if (parseInt(c.w) > 0) {
                var rx = xsize / c.w;
                var ry = ysize / c.h;
                pimg.css({
                    width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });
            }
        }
        function SaveChanges() {
            ResizeImageUsingByCrop();
            modal.style.display = "none";
            jcrop_api.destroy();
        }


        function SetReSizeValues(paramval) {
            jQuery("#ImgResizePreview").Jcrop({
                onSelect: SelectCropArea,
                onChange: SelectCropArea,
                bgFade: true

            }, function () {
                jcrop_api = this;
            });
            jcrop_api.destroy();
            jQuery("#ImgResizePreview").css("visibility", "visible");
            var AutoSize = "";
            var reqsize = "";
            var orientation = "";
            var ResizeWidth = 0;
            var ResizeHeight = 0;
            if (paramval == "S") {
                AutoSize = jQuery("#ddlSizeResize").val();
                reqsize = jQuery("#ddlSizeResize").val().split('x');
                orientation = jQuery("#ddlOrientationResize").val();
                if (orientation == "Portrait") {
                    ResizeWidth = parseInt(reqsize[0]) * 300;
                    ResizeHeight = parseInt(reqsize[1]) * 300;
                }
                else {
                    ResizeWidth = parseInt(reqsize[1]) * 300;
                    ResizeHeight = parseInt(reqsize[0]) * 300;
                }
            }
            else {
                if (orientation == "Portrait") {
                    ResizeWidth = jQuery("#txtCustomWidth").val();
                    ResizeHeight = jQuery("#txtCustomHeight").val();
                }
                else {
                    ResizeWidth = jQuery("#txtCustomHeight").val();
                    ResizeHeight = jQuery("#txtCustomWidth").val();
                }
            }
            if (orientation == "Portrait") {
                var previewHeight = jQuery("#divResizePreview").css("height");
                var previewwidth = jQuery("#divResizePreview").css("width");
                jQuery("#divResizePreview").css("height", previewwidth);
                jQuery("#divResizePreview").css("width", previewHeight);
                jQuery("#txtCustomHeight").val(ResizeHeight);
                jQuery("#txtCustomWidth").val(ResizeWidth);
            }
            else {
                var previewHeight = jQuery("#divResizePreview").css("height");
                var previewwidth = jQuery("#divResizePreview").css("width");
                jQuery("#divResizePreview").css("height", previewwidth);
                jQuery("#divResizePreview").css("width", previewHeight);
                jQuery("#txtCustomHeight").val(ResizeHeight);
                jQuery("#txtCustomWidth").val(ResizeWidth);
            }            
            setTimeout(function () { PreviewResizedImage(); }, 200);
            setTimeout(function () {
                if (jQuery("#chkFinalCrop").prop("checked") == true) {
                    Set_Cropper(1);
                }
            }, 400);
        }

        //Crop Image With Resize Image
        function CropImageWithResize() {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/CropImageWithResize_MediaManagementPage",
                contentType: 'application/json; charset=utf-8',
                data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"),
                    'ImageHeight': jQuery("#txtCustomHeight").val(),
                    'ImageWidth': jQuery("#txtCustomWidth").val(),
                    'XAxis': document.getElementById('XAxis').value,
                    'YAxis': document.getElementById('YAxis').value,
                    'CropImageWidth': document.getElementById('Width').value,
                    'CropImageHeight': document.getElementById('Height').value.toString()
                    
                },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.FileName == null) {
                        alert("Please Crop Image");
                    }
                    else {
                        location.reload();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        function ResizeImage() {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/ResizeImage_MediaManagementPage",
                contentType: 'application/json; charset=utf-8',
                data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"),
                    'RatioStatus': document.getElementById('hdnResizeAsepctRatio').value,
                    'ImageHeight': document.getElementById('txtCustomHeight').value,
                    'ImageWidth': document.getElementById('txtCustomWidth').value,
                    "pageID": jQuery("#hdnCurPageID").val(),
                    'FileName': jQuery("#labfilename").html(),
                    'Description': jQuery("#txtdescription").val(),
                    'AlternateText': jQuery("#txtalternatetext").val(),
                    'UserID': document.getElementById('hdnUserID').value,

                },
                dataType: "json",
                success: function (data) {
                    if (data != null && data != undefined && data != "") {
                        location.reload();
                    }
                    else {
                        //Start Save Image Preview//
                        jQuery("#divFinalPreview").hide();
                        jQuery("#divCropFinalImage").hide();
                        jQuery("#imgGetHeightWidth").attr("src", "");
                        jQuery("#dZUpload").show();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }


        // Custome Image Create
        function CustomeImage() {
            var ImageNameArray = jQuery("#ImgResizeImage").attr("src").split('/');
            var ImageName = ImageNameArray[ImageNameArray.length - 1];
            var Color = jQuery('#txtFontColor').val();
            var hex = parseInt(Color.substring(1), 16);
            var r = (hex & 0xff0000) >> 16;
            var g = (hex & 0x00ff00) >> 8;
            var b = hex & 0x0000ff;
            jQuery.ajax({
                type: "GET",
                url: "../WebService/DocCMSApi.svc/Custome_Image_Create_MediaManagementPage",
                data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'R': r, 'G': g, 'B': b, 'userID': jQuery('#hdnCurUserID').val() },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.FileName != null && data.FileName != undefined) {
                        location.reload();
                    }
                    else {
                        jQuery("#ImgCustomeImagePreview").attr("src", "");
                        jQuery("#ImgCustomeImagePreview").hide();
                        jQuery("#imgFinalPreview").attr("src", "");
                        jQuery("#EditImageOnModelPopup").attr("src", "");
                        jQuery("#divFinalPreview").hide();
                        jQuery("#divCropFinalImage").hide();
                        jQuery("#dZUpload").show();
                        modalResize.style.display = "block";
                        jQuery("#imgGetHeightWidth").attr("src", "");
                        jQuery("#btnPostImage").prop("disabled", true);
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }
    </script>
    <script type = "text/javascript">
          function Confirm(value) {
              var confirm_value = document.createElement("INPUT");
              confirm_value.type = "hidden";
              confirm_value.name = "confirm_value";
              if (confirm(value)) {
                  confirm_value.value = "Yes";
              } else {
                  confirm_value.value = "No";
              }
              document.forms[0].appendChild(confirm_value);
          }
          function SetTextImageValues(cntrl) {
              jQuery("#ImgResizePreview").Jcrop({
                  onSelect: SelectCropArea,
                  onChange: SelectCropArea,
                  bgFade: true

              }, function () {
                  jcrop_api = this;
              });
              jcrop_api.destroy();
              jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
              jQuery("#ImgResizePreview").css('visibility', 'visible');
              switch (cntrl) {
                  case "Left Picture":
                      jQuery("#txtCustomWidth").val("373");
                      jQuery("#txtCustomHeight").val("280");
                      CheckResizeValues();
                      break;
                  case "Center Picture":
                      jQuery("#txtCustomWidth").val("373");
                      jQuery("#txtCustomHeight").val("280");
                      CheckResizeValues();
                      break;
                  case "Right Picture":
                      jQuery("#txtCustomWidth").val("373");
                      jQuery("#txtCustomHeight").val("280");
                      CheckResizeValues();
                      break;
                  case "Full Picture":
                      jQuery("#txtCustomWidth").val("1119");
                      jQuery("#txtCustomHeight").val("840");
                      CheckResizeValues();
                      break;
                  default:
                      jQuery("#txtCustomWidth").val("1800");
                      jQuery("#txtCustomHeight").val("1200");
                      CheckResizeValues();
                      break;
              }
          }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">
            <lable id="labheading1" runat="server"></lable>
        </a><span class="separator"></span></li>
        <li>
            <lable id="labheading2" runat="server"></lable>
        </li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5 id="h5heading1" runat="server">
                Heading1</h5>
            <h1 id="h1heading1" runat="server">
                Heading2</h1>
        </div>
    </div>
          <!---------------------Resize Section--------------------------->
   <div id="divModelResize" class="modalResizeEidt" style="height: auto% !important;max-width:auto% !important; " >
      <div class="modalResizeEidt-content" style="height: auto !important;max-width:auto !important;" >
          <span class="ResizeHeaderEdit">Resize Your Image to get a best fit result</span>
              <span class="cross" onclick="CloseImageResizeModel();">×</span>
                   <div style="width:auto !important;">
                  <img src="" id="ImgResizeImage" style="width:auto;height:auto"/>
                <div style="width: 100%;">
                        Resize Image Option  <input type="checkbox" id="chkResizeImageCreate"/>
                        Create Custom Image <input type="checkbox" id="chkCustomeImageCreate"/>
                       <div  id="divCustomResizeImageCreate">
                        <input type="checkbox" id="chkAspectRatioResize" checked="checked" />
                        Keep Aspect Ratio<br />
                        Image Width :<input type="text" id="txtCustomWidth" placeholder="Width in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('w',event);" style="width:18%"/>
                        Image Height :<input type="text" id="txtCustomHeight" placeholder="Height in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('h',event);" style="width:18%"/>
                    
                    <div id="divResizeSection">
                        Select Image Size<select id="ddlSizeResize" style="width: 90px; height: 36px;" onchange="SetReSizeValues('S');">                            
                            <option value="4x6">4x6</option>
                        </select>
                        Select Orientation<select id="ddlOrientationResize" style="width: 150px; height: 36px;"  onchange="SetReSizeValues('O');">                            
                            <option value="Landscape">Landscape</option>
                            <option value="Portrait">Portrait</option>
                        </select>
                          Page Text Image<select id="ddlPageTextImage" style="width: 150px; height: 36px;"  onchange="SetTextImageValues(this.value);">                            
                            <option value="Left Picture">Left Picture</option>
                            <option value="Center Picture">Center Picture</option>
                             <option value="Right Picture">Right Picture</option>
                            <option value="Full Picture">Full Picture</option>
                        </select>
                    </div>
                    <input type="button" id="btnResizeImage" class="btn btn-danger btn-lg" value="Resize" onclick="ResizeImage();"
                    style="margin-top:10px; margin-bottom:10px;" />
                          <span id="spnFinalCrop" style="display:none;"><span style="color:Red;font-weight:400;">The previewed image exceeds the required dimensions.You need to crop the image to get the required dimensions.</span><br /><input type="checkbox" id="chkFinalCrop"/>Crop Image
                        <input type="button" class="btn btn-danger btn-lg" style="display:none;" id="btnCropWithResize" value="Crop Image" onclick="CropImageWithResize();" />
                        </span>
                    <input type="button" id="Button1" class="btn btn-danger btn-lg" value="Preview" onclick="PreviewResizedImage();"
                    style="margin-top:10px; margin-bottom:10px;display:none;"  />
                  <asp:HiddenField ID="hdnResizeAsepctRatio" runat="server" ClientIDMode="Static" Value="True"/>
                 <div id="divResizePreviewPane" style="margin-left:20px; margin-bottom :20px;">
                <span ><b>Image Preview</b></span>
                <div class="preview-container" id="divResizePreview"  style="width:300px; height:200px;">
                    <img src="" id="ImgResizePreview" alt="Preview" />
                </div>
          
            </div>
            </div>
            </div>
             </div>
             <div id="divCustomImageCreate" style="display:none;">
             Select Background Image Color:
             <asp:TextBox ID="txtFontColor" runat="server" CssClass="input-large demo" ClientIDMode="Static" style=" width:300px; height:24px;margin-bottom: 0;" placeholder="Enter Font Color" onchange="ColorChange(this)"></asp:TextBox>
             <br />
               <input type="button" id="btncustomeImage" class="btn btn-danger btn-lg" value="Create Image" onclick="CustomeImage();"
                    style="margin-top:10px; margin-bottom:10px;"  />
        
                 <div id="divCustomImagePreviewRunning" style="width:300px; height:200px; background-color:#FFFFFF;margin-left:5%;margin-bottom:10px;">
                 <center>
                 <img src="" id="ImgCustomeImagePreviewRunning" alt="Custom Image Preview" />
                 </center>
                </div>
               </div>
             </div>
                          <div id="Div4" runat="server" clientidmode="Static"></div>
                        </div>
      <!---------------------Resize Section--------------------------->
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <div class="widget">
                <h4 class="widgettitle" id="h4heading1" runat="server">
                    Heading3</h4>
                <div class="widgetcontent">
                    <div class="stdform">
                        <asp:Label ID="labtid" runat="server" Visible="false"></asp:Label>
                        <p id="pmediatype" runat="server">
                            <label style="width: 100px;">
                                Media Type</label>
                            <span class="field" style="margin-left: 130px;">
                                <asp:DropDownList ID="drpFileType" runat="server" CssClass="uniformselect" placeholder="Select File Type">
                                </asp:DropDownList>
                            </span>
                            
                        </p>
                        <p id="ppreviousfile" runat="server" visible="false">
                            <label style="width: 100px;">
                                File Name</label>
                            <span class="field" >
                                <asp:Label ID="labfilename" ClientIDMode="Static" runat="server" Text="" style="margin-left: -71px;"></asp:Label>
                            </span>
                        </p>
                        <p>
                            <label style="width: 100px;">
                                Select File</label>
                            <span class="field" style="margin-left: 130px;">
                                <div class="par" style="margin-left: 130px;">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input span8" style=" width:774px;">
                                                <i class="iconfa-file fileupload-exists"></i><span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file"><span class="fileupload-new">Select File</span> <span
                                                class="fileupload-exists">Change</span>
                                                <input type="file" id="ImageUploadControl" size="500" runat="server" name="ImageUploadControl" clientidmode="Static" visible="false"/>
                                                  <input type="file" id="FileUploadControl" size="500" runat="server" name="FileUploadControl" clientidmode="Static" visible="false"/></span>
                                            <a class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                        <asp:Label ID="lblSizeError" CssClass="alert-error" runat="server" Text="" Style="display: inline;
                                            margin-left: 10px;"></asp:Label>
                                    </div>
                                    
                                </div>
                            </span>
                        </p>
                         <p id="PImage" runat="server" clientidmode="Static">
                            <label style="width: 100px;">
                              Preview</label>
                            <span class="field" style="margin-left: 130px;">
                                <div class="par" style="margin-left: 130px;">
                                      <asp:Image ID="imgLogo" runat="server" ClientIDMode="Static" Height="120" Width="120" Style="border:1px solid Black;" ImageUrl="~/images/image-not-found.png"/>
                                              <div style="margin-left: 5%; margin-top: %; width: 120px;">
                                      <img src="../images/Edit-image.png" style="width:24px;float:right;" title="Edit Image" onclick="EditImage(this.id);" runat="server" id="imgEdit" />
                                      </div>
                                </div>
                            </span>
                        </p>
                        <div id="divyoutube" runat="server" visible="false">
                            <p>
                                <label style="width: 100px;">
                                    Video Link</label>
                                <span class="formwrapper" style="margin-left:130px; width:56.2%;">
                                    <asp:TextBox ID="txtvideolink" runat="server" CssClass="input-block-level" placeholder="Enter Link Address" />
                                </span>
                            </p>
                        </div>
                        <p>
                            <label style="width: 100px;">
                                Alternate text</label>
                            <span class="field" style="width:56.2%;">
                                <asp:TextBox ID="txtalternatetext" runat="server" ClientIDMode="Static" CssClass="input-block-level" placeholder="Alternate Text" style="margin-left:-71px;" />
                            </span>
                        </p>
                        <p>
                            <label style="width: 100px;">
                                Description</label>
                            <span class="field" style="margin-left: 130px;">
                                <asp:TextBox ID="txtdescription" runat="server" 
                                CssClass="input-block-level" placeholder="Description" ClientIDMode="Static" Rows="5" 
                                TextMode="MultiLine" style="width: 60%; height: 108px;max-width:60%;min-width:60%; min-height:108px; max-height:108px;"></asp:TextBox>
                            </span>
                        </p>
                        <p class="stdformbutton">
                           
                            <asp:Button CssClass="btn btn-primary" ID="btnSubmit" style="height:34px" Text="Save Information" runat="server" OnClick="btnsubmit_click" OnClientClick="return ValidateFileSize();"  validationgroup="grpsubmit" visible="False" />

                            <asp:Button CssClass="btn btn-primary" ID="btnEdit" style="height:34px" Text="Update Information" runat="server" OnClick="btnEdit_click" OnClientClick="return ValidateFileSize();"  validationgroup="grpsubmit" visible="False" />

                                <asp:Button class="btn" style="width: 175px; margin-left: 20px;" ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_click" OnClientClick="return Button1_onclick()"  CausesValidation="false" />                            
                        </p>
                        <p class="stdformbutton">
                            <asp:Label ID="labfileformat" runat="server" Visible="false" Font-Bold="false" Font-Size="14"
                                ForeColor="Red"></asp:Label>
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <asp:Label ID="lblText" runat="server" />
                        <asp:HiddenField ID="hdn1" runat="server" />
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
        </div>
                        <asp:HiddenField ID="hdnUserID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="XAxis" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="YAxis" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="Width" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="Height" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="maxWidth" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="maxHeight" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDropDownListVal" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDisplayOrder" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDropDownListValLevel1" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnListBoxVal" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnCurDivID" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hndImageFileName" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnActualImageWidth" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnActualImageHeight" runat="server" ClientIDMode="Static"/>
        <!--maincontentinner-->
    </div>
    <script language="javascript" type="text/javascript">
        //Start Image Preview Function
        function ShowPreview(fileInput) {
            var fileinputidval = fileInput.id;
            var thumbnilid = "";
            var Get_FileinputID = fileinputidval.toString().split('_');
            if (Get_FileinputID.length > 3) {
                thumbnilid = Get_FileinputID[0] + "_" + Get_FileinputID[1] + "_ImgCMSFileUploader_" + Get_FileinputID[3];
            }
           
            var files = fileInput.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var ext = file.name.split('.').pop().toLowerCase();
                var imageType = /image.*/;
                var filetype = file.type
                var img = document.getElementById(thumbnilid.toString());
                img.file = file;
                var reader = new FileReader();
                if (file.type.match(imageType)) {

                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = e.target.result;
                        };
                    })(img);
                    reader.readAsDataURL(file);
                }
                else if (ext == "pdf") {
                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = '../UploadedFiles/pdf.png';
                        };
                    })(img);
                    reader.readAsDataURL(file);


                }
                else if (ext == "doc" || ext == "txt") {

                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = '../UploadedFiles/doc.png';
                        };
                    })(img);
                    reader.readAsDataURL(file);


                }

                else if (ext == "xls" || ext == "cvc") {

                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = '../UploadedFiles/xls.png';
                        };
                    })(img);
                    reader.readAsDataURL(file);
                }
            }
            
        }

        function EditImage(ctrl) {
            var ctrlArray = ctrl.split('_');
            OpenModelPopupCropAndResize(ctrlArray[ctrlArray.length - 1]);
        }
        //Open Model Popup Crop and Resize
        function OpenModelPopupCropAndResize(prefix) {
            var FilePath = jQuery("#imgLogo").attr("src");
            var modalResize = document.getElementById('divModelResize');
            modalResize.style.display = "block";
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/ProcessFiles_MediaManagementPage",
                contentType: 'application/json; charset=utf-8',
                data: { "FilePath": FilePath, "pageID": jQuery("#hdnCurPageID").val()},
                dataType: "json",
                success: ajaxSucceeded,
                error: ajaxFailed
            });
            function ajaxSucceeded(data) {
                if (data != null) {
                    setTimeout(function () { ResizeImagePopup(data); }, 200);
                }
            }
            function ajaxFailed() {
                alert("There is an error during operation.");
            }
        }

        function ResizeImagePopup(fileName) {
             var jcrop_api;
            jQuery("#ImgResizePreview").attr("src", fileName);
            jQuery("#ImgResizeImage").attr("src", fileName);
            jQuery("#ImgCustomeImagePreviewRunning").attr("src", fileName);
            setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
            CheckResizeValues();
            return false;
        }

        function Get_Actual_Uploaded_Image_Size() {
            var myImg = document.querySelector("#ImgResizeImage");
            var realWidth = myImg.naturalWidth;
            var realHeight = myImg.naturalHeight;
            jQuery("#hdnActualImageWidth").val(realWidth);
            jQuery("#hdnActualImageHeight").val(realHeight);
        }

        var boundx, boundy;
        function SelectCropArea(c) {
            // Grab some information about the preview pane            
            var preview = jQuery('#preview-pane');
            var pcnt = jQuery('#preview-pane .preview-container');
            var pimg = jQuery('#preview-pane .preview-container img');
            var xsize = pcnt.width(),
            ysize = pcnt.height();
            jQuery('#XAxis').val(parseInt(c.x));
            jQuery('#YAxis').val(parseInt(c.y));
            jQuery('#Width').val(parseInt(c.w));
            jQuery('#Height').val(parseInt(c.h));
            jQuery('#x1').val(c.x);
            jQuery('#y1').val(c.y);
            jQuery('#x2').val(c.x2);
            jQuery('#y2').val(c.y2);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
            if (parseInt(c.w) > 0) {
                var rx = xsize / c.w;
                var ry = ysize / c.h;
                pimg.css({
                    width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });
            }
        }

        function CloseImageResizeModel() {
            jQuery("#ImgResizeImage").attr("src", "");
            var modalResize = document.getElementById('divModelResize');
            modalResize.style.display = "none";
        }
    
    </script>
     <script src="../js/jscolor.js" type="text/javascript"></script>
     <link href="../css/cropfunctionality.css" rel="stylesheet" type="text/css" />
     <asp:HiddenField ID="hdnCurCMSID" runat="server" ClientIDMode="Static"/>
     <asp:HiddenField ID="hdnCurPageID" runat="server" ClientIDMode="Static"/>
    <!--maincontent-->
</asp:Content>
