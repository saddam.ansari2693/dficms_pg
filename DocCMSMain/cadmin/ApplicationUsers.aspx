﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="ApplicationUsers.aspx.cs" Inherits="DocCMSMain.cadmin.ApplicationUsers" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8) {
                return true;
            }

            if (key > 31 && (key < 48 || key > 57))
                return false;
            //             
            return true;
        }
    </script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $("#txtNavigationUrl").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../cadmin/SubModules.aspx/GetKeyWords",
                        data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }
        function SearchText_btn() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../cadmin/SubModules.aspx/GetKeyWords_btnClick",
                data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                dataType: "json",
                success: function (data) {
                    var dataArray = data.d.toString().split(',');
                    var List = "";
                    for (var i = 0; i < dataArray.length; i++) {
                        List = List + "<li class='ui-menu-item customMenu' role='menuitem' id='liAutoComplete_" + i.toString() + "'>";
                        var NavigateURL = "";
                        if (i == 0) {
                            NavigateURL = dataArray[i].toString().trim().substring(2);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        else if (i == dataArray.length - 1) {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 2);
                        }
                        else {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        if (dataArray.length == 1) {
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        List = List + "<a class='ui-corner-all' tabindex='-1' id='ancAutoComplete_" + i.toString() + "' onclick='SetValueInTextBox(this);'>" + NavigateURL + "</a>";
                        List = List + "</li>";
                    }
                    $("#ulAutocomplete").html(List);
                    $("#ulAutocomplete").show();
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }
        function SetValueInTextBox(ctrlID) {
            $("#txtNavigationUrl").val($("#" + ctrlID.id).text());
            $("#ulAutocomplete").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Application User</a> <span class="separator"></span></li>
        <li>Manage Application User</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Application User</h5>
            <h1>
                Manage Application User</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <div class="widget">
                <h4 class="widgettitle">
                    Application User Details</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform">
                        <div class="par control-group">
                            <label class="control-label" for="txtEmployeeName">
                                Employee Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtEmployeeName" id="txtEmployeeName" class="input-large"
                                    clientidmode="Static" placeholder="Enter Employee Name" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtEmployeeName" runat="server" Style="color: Red; font-size: 24px;
                                    font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                         <div class="par control-group">
                            <label class="control-label" for="txtEmail">
                                Email</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtEmail" id="txtEmail" class="input-large autocomplete"
                                    clientidmode="Static" placeholder="Enter Email ID" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtEmail" runat="server" Style="color: Red; font-size: 24px;
                                    font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtUsername">
                                Username</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtUsername" id="txtUsername" class="input-large autocomplete"
                                    clientidmode="Static" placeholder="Enter Username" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtUsername" runat="server" Style="color: Red; font-size: 24px;
                                    font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtPassowrd">
                                Password</label>
                            <div class="controls">
                                <input type="password" runat="server" name="txtPassowrd" id="txtPassowrd" class="input-large autocomplete"
                                    clientidmode="Static" placeholder="Enter Password" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtPassowrd" runat="server" Style="color: Red; font-size: 24px;
                                    font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                       <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server"  name="activestatus" id="ChkActiveStatus" class="input-large"
                                    style="opacity: 1" checked />
                            </div>
                        </div>
                        <p class="stdformbutton">
                            <asp:Button ID="btnsubmit" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnsubmit_onclick" Text="Submit" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnupdate" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnupdate_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                OnClick="btnCancel_Click" />
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            <!-- for Display Order Popup -->
        </div>
        <!--maincontentinner-->
        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
          role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
        </ul>
    </div>
    <!--maincontent-->
    <script language="javascript" type="text/javascript">
        function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function SuccessMsg(ModuleID) {
            alert("The provided display orders are successfully Updated");
            var url = "";
            if (ModuleID != null) {
                url = "../cadmin/Modules.aspx?MID=" + ModuleID;
                $(location).attr('href', url);
            }
            else {
                url = "../cadmin/Modules.aspx";
                $(location).attr('href', url);
            }
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
            jQuery("#btnUpdate").show();
            jQuery("#img1").hide();
        }
  </script>
</asp:Content>
