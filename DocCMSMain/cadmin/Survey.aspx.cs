﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class Survey : System.Web.UI.Page
    {

        dynamic UserId;
        protected static Int32 Surveyid = 0;
        protected static Int32 QuestionSurveyid = 0;
        protected static Int32 AnswerSurveyid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            Session["HiddenRow"] = null;
            Session["HiddenAnsRow"] = null;
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Request.QueryString["SurveyID"] != null)
                    {
                        Surveyid = Convert.ToInt32(Request.QueryString["SurveyID"]);
                        Bind_Data_For_Questions(Convert.ToInt32(Request.QueryString["SurveyID"]));
                        Bind_DisplayOrder();
                    }
                    else
                    {
                        Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Survey_Type_dorder() + 1;
                        txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                        btnsubmitMaster.Visible = true;
                        btnMasterupdate.Visible = false;
                        btnSaveQuestion.Visible = false;
                        btnCancelQuestion.Visible = false;
                        AddFeature.Visible = false;
                        lblQuestion.Visible = true;
                        spanAnotherQues.Visible = false;
                        DivQuestion.Visible = false;
                        Bind_DisplayOrder();
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        // Save Master data 
        protected void btnsubmitMaster_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null)
                {

                    ClsSurvey clsSurvey = new ClsSurvey();
                    clsSurvey.surveyname = txtSurveyName.Value;
                    clsSurvey.description = txtDescription.Value;
                    clsSurvey.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                    clsSurvey.createdby = Convert.ToString(UserId);
                    if (ChkActiveStatus.Checked)
                    {
                        Int32 retval = ServicesFactory.DocCMSServices.Update_Default_SurveyCheck();
                        clsSurvey.defaultactive = true;
                    }
                    else
                    {
                        clsSurvey.defaultactive = false;
                    }
                    if (ChkReport.Checked)
                    {
                        clsSurvey.showreport = true;
                    }
                    else
                    {
                        clsSurvey.showreport = false;
                    }
                    // check Survey name
                    bool ServerNameExists = ServicesFactory.DocCMSServices.ChkDuplicate_surveyname(txtSurveyName.Value.Trim());
                    if (ServerNameExists == true)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Survey with Same Name Already Exists...');</script>");
                        return;
                    }
                    else
                    {
                        Int32 retVal = ServicesFactory.DocCMSServices.Insert_Survey_Master(clsSurvey);
                        if (retVal > 0)
                        {
                            Surveyid = retVal;
                            btnAddQuestion.Visible = true;
                            btnMasterupdate.Visible = true;
                            btnsubmitMaster.Visible = false;
                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Survey has been created successfully...');</script>");
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ImgAddQuestion_Click(object sender, ImageClickEventArgs e)
        {
            trAdditionalRow.Visible = true;
            trActionQuestion.Visible = true;
            Int32 Qdorder = ServicesFactory.DocCMSServices.Get_Max_question_dorder(Surveyid) + 1;
            txtQuest.Text = "";
            ddlQType.SelectedIndex = 0;
            txtQDorder.Text = Convert.ToString(Qdorder);
            btnSaveQuestion.Visible = true;
            spanAnotherQues.Visible = true;
            chkQActive.Checked = true;
        }
        protected void btnMasterupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null)
                {

                    ClsSurvey clsSurvey = new ClsSurvey();
                    clsSurvey.surveyname = txtSurveyName.Value;
                    clsSurvey.description = txtDescription.Value;
                    clsSurvey.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                    clsSurvey.modifiedby = Convert.ToString(UserId);
                    clsSurvey.surveyid = Surveyid;
                    if (ChkActiveStatus.Checked)
                    {
                        Int32 retval = ServicesFactory.DocCMSServices.Update_Default_SurveyCheck();
                        clsSurvey.defaultactive = true;
                    }
                    else
                    {
                        clsSurvey.defaultactive = false;
                    }
                    if (ChkReport.Checked)
                    {
                        clsSurvey.showreport = true;
                    }
                    else
                    {
                        clsSurvey.showreport = false;
                    }

                    // check Survey name
                    bool ServerNameExists = ServicesFactory.DocCMSServices.ChkDuplicate_surveyname_ForUpdate(txtSurveyName.Value.Trim(), Surveyid);
                    if (ServerNameExists == true)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Survey with Same Name Already Exists...');</script>");
                        return;
                    }
                    else
                    {
                        Int32 retVal = ServicesFactory.DocCMSServices.Update_Survey_Detail_By_surveyid(clsSurvey);
                        if (retVal > 0)
                        {

                            btnAddQuestion.Visible = true;
                            btnMasterupdate.Visible = true;
                            btnsubmitMaster.Visible = false;
                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Survey has been updated successfully...');</script>");
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/SurveyDashBoard.aspx");
        }

        //Bind Data For Questions after Saving  Master Details
        protected void Bind_Data_For_Questions(Int32 SurveyID)
        {
            try
            {
                ClsSurvey objsurvey = ServicesFactory.DocCMSServices.Fetch_Survey_Master_Byid(SurveyID);
                if (objsurvey != null)
                {
                    txtSurveyName.Value = objsurvey.surveyname;
                    txtDescription.Value = objsurvey.description;
                    ChkActiveStatus.Checked = objsurvey.defaultactive;
                    ChkReport.Checked = objsurvey.showreport;
                    txtDisplayOrder.Value = Convert.ToString(objsurvey.displayorder);
                    lblSurveyName.InnerText = objsurvey.surveyname;
                    if (Request.QueryString["SurveyID"] != null)
                    {
                        // Setting Visible and inviisble 
                        lblSurveyName.Style.Add("display", "none");
                        txtSurveyName.Style.Add("display", "block");
                        txtDescription.Style.Add("display", "block");
                        txtDisplayOrder.Style.Add("display", "block");
                        btnMasterupdate.Visible = true;
                        btnsubmitMaster.Visible = false;
                        btnAddQuestion.Visible = true;
                        DivQuestion.Visible = false;
                        btnCancelQuestion.Visible = false;
                    }
                    else
                    {
                        txtSurveyName.Style.Add("display", "none");
                        lblSurveyName.Style.Add("display", "block");
                        DivDescription.Visible = false;
                        DivDisplayOrder.Visible = false;
                        DivCheckStatus.Visible = false;
                        AddFeature.Visible = true;
                        lblQuestion.Visible = false;
                        btnSaveQuestion.Visible = false;
                        btnCancelQuestion.Visible = true;
                        spanAnotherQues.Visible = false;
                        btnsubmitMaster.Visible = false;
                        btnMasterupdate.Visible = false;
                        btnCancel.Visible = false;
                    }
                    // Setting value into Control
                    txtSurveyName.Value = objsurvey.surveyname;
                    lblSurveyName.InnerText = objsurvey.surveyname;
                    txtDescription.Value = objsurvey.description;
                    txtDisplayOrder.Value = Convert.ToString(objsurvey.displayorder);
                    ChkActiveStatus.Checked = objsurvey.defaultactive;
                    ChkReport.Checked = objsurvey.showreport;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnAddQuestion_Click(object sender, EventArgs e)
        {
            Bind_Data_For_Questions(Surveyid);
            btnAddQuestion.Visible = false;
            DivQuestion.Visible = true;
            txtSurveyName.Style.Add("display", "none");
            lblSurveyName.InnerText = txtSurveyName.Value;
            lblSurveyName.Style.Add("display", "block");
            DivDescription.Visible = false;
            DivDisplayOrder.Visible = false;
            DivCheckStatus.Visible = false;
            AddFeature.Visible = true;
            lblQuestion.Visible = false;
            btnSaveQuestion.Visible = false;
            btnCancelQuestion.Visible = true;
            spanAnotherQues.Visible = false;
            btnsubmitMaster.Visible = false;
            btnMasterupdate.Visible = false;
            btnCancel.Visible = false;
            if (Request.QueryString["SurveyID"] != null)
            {
                DivQuestion.Visible = true;
                btnCancelQuestion.Visible = true;
                Bind_Question();
            }
            else
            {
            }
        }
        protected void Bind_Survey_Page()
        {
            txtSurveyName.Style.Add("display", "block");
            lblSurveyName.Style.Add("display", "none");
            DivDescription.Visible = true;
            DivDisplayOrder.Visible = true;
            DivCheckStatus.Visible = true;
            Bind_Question();
            btnMasterupdate.Visible = true;
            btnsubmitMaster.Visible = false;
            btnCancel.Visible = true;
            btnSaveQuestion.Visible = false;
            btnUpdateQuestion.Visible = false;
            spanAnotherQues.Visible = false;
            AddFeature.Visible = false;
            DivQuestionContent.Visible = false;
            DivQuestion.Visible = false;
            btnAddQuestion.Visible = true;
            btnCancelQuestion.Visible = false;
        }

        protected void btnCancelQuestion_Click(object sender, EventArgs e)
        {
            Bind_Survey_Page();
            trAdditionalRow.Visible = false;
            txtQuest.Text = "";
            txtQDorder.Text = "";
            chkQActive.Checked = true;
            ddlQType.SelectedIndex = 0;
        }

        protected void btnSaveQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    ClsSurveyquestions clsSurvey = new ClsSurveyquestions();

                    clsSurvey.question = txtQuest.Text;
                    clsSurvey.surveyid = Surveyid;
                    clsSurvey.displayorder = Convert.ToInt32(txtQDorder.Text);
                    clsSurvey.answerType = ddlQType.SelectedValue;
                    if (chkQActive.Checked)
                        clsSurvey.isactive = true;

                    else
                        clsSurvey.isactive = false;

                    if (chkShowReport.Checked)
                        clsSurvey.showreport = true;

                    else
                        clsSurvey.showreport = false;
                    // check Question Exist
                    bool QuestionExist = ServicesFactory.DocCMSServices.ChkDuplicate_question(txtQuest.Text.Trim(), Surveyid);
                    if (QuestionExist == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Question Already Exists...');", true);
                        return;
                    }
                    else
                    {
                        Int32 retVal = ServicesFactory.DocCMSServices.Insert_Survey_question(clsSurvey);
                        if (retVal > 0)
                        {

                            if (ChkAnotherQues.Checked)
                            {
                                trAdditionalRow.Visible = false;
                                txtQuest.Text = "";
                                txtQDorder.Text = "";
                                ddlQType.SelectedIndex = 0;
                                Bind_Question();
                                trAdditionalRow.Visible = true;
                                Int32 Qdorder = ServicesFactory.DocCMSServices.Get_Max_question_dorder(Surveyid) + 1;
                                txtQDorder.Text = Convert.ToString(Qdorder);
                            }
                            else
                            {
                                trAdditionalRow.Visible = false;
                                Bind_Question();
                                spanAnotherQues.Visible = false;
                                btnSaveQuestion.Visible = false;
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnUpdateQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    ClsSurveyquestions clsSurvey = new ClsSurveyquestions();

                    clsSurvey.question = txtQuest.Text;
                    clsSurvey.surveyid = Surveyid;
                    clsSurvey.displayorder = Convert.ToInt32(txtQDorder.Text);
                    clsSurvey.answerType = ddlQType.SelectedValue;
                    clsSurvey.surveyquestionid = QuestionSurveyid;
                    if (chkQActive.Checked)
                        clsSurvey.isactive = true;

                    else
                        clsSurvey.isactive = false;

                    if (chkShowReport.Checked)
                        clsSurvey.showreport = true;

                    else
                        clsSurvey.showreport = false;
                    if (ddlQType.SelectedValue.ToUpper() == "TEXT")
                    {
                        Int32 RetVal = ServicesFactory.DocCMSServices.Delete_Survey_answer_Byquestionid(QuestionSurveyid);
                    }
                    // check Survey name
                    Int32 retVal = ServicesFactory.DocCMSServices.Update_Survey_question(clsSurvey);
                    if (retVal > 0)
                    {
                        trAdditionalRow.Visible = false;
                        Bind_Question();
                        btnUpdateQuestion.Visible = false;
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Bind Data For Questions after Saving  Master Details
        protected void Bind_Question()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_question_Dashboard(Surveyid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    rptrQuestion.DataSource = dt;
                    rptrQuestion.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptrQuestion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (Session["UserID"] != null)
            {
                if (e.CommandArgument != null)
                {
                    LinkButton lnkQuestion = (LinkButton)e.Item.FindControl("lnkQuestion");
                    Label lblQuestionID = (Label)e.Item.FindControl("lblQuestionID");
                    Label lblAnswerType = (Label)e.Item.FindControl("lblAnswerType");
                    Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                    Label lblActive = (Label)e.Item.FindControl("lblActive");
                    LinkButton lnkEditAnswer = (LinkButton)e.Item.FindControl("lnkEditAnswer");
                    LinkButton lnkAddAnswer = (LinkButton)e.Item.FindControl("lnkAddAnswer");
                    if (lnkQuestion != null && lblQuestionID != null && lblAnswerType != null && lblDorder != null && lblActive != null && lnkEditAnswer != null && lnkAddAnswer != null)
                    {

                        switch (e.CommandName)
                        {
                            case "AddAnswer":

                                DivSurveyContent.Visible = false;
                                DivQuestionContent.Visible = true;
                                Bind_Answer(Convert.ToInt32(lblQuestionID.Text));
                                Bind_Answer_Dashboard(Convert.ToInt32(lblQuestionID.Text));
                                break;

                            case "EditQuestion":
                                e.Item.Visible = false;
                                trAdditionalRow.Visible = true;
                                btnUpdateQuestion.Visible = true;
                                Session["HiddenRow"] = Convert.ToString(e.Item.ItemIndex);
                                Bind_Question();
                                Bind_EditQuestion(Convert.ToInt32(lblQuestionID.Text));
                                break;
                            case "DeleteQuestion":
                                ServicesFactory.DocCMSServices.Delete_SurveyByquestionid(Convert.ToInt32(lblQuestionID.Text));
                                Bind_Question();
                                break;
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Default.aspx");

            }
        }


        protected void Bind_EditQuestion(Int32 SurveyQuestionID)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    ClsSurveyquestions objques = ServicesFactory.DocCMSServices.Fetch_question_Byid(SurveyQuestionID);
                    if (objques != null)
                    {
                        txtQuest.Text = objques.question;
                        txtQDorder.Text = Convert.ToString(objques.displayorder);
                        ddlQType.SelectedValue = Convert.ToString(objques.answerType);
                        QuestionSurveyid = Convert.ToInt32(objques.surveyquestionid);
                        btnUpdateQuestion.Visible = true;
                        trActionQuestion.Visible = true;
                        btnSaveQuestion.Visible = false;
                        spanAnotherQues.Visible = false;
                        chkQActive.Checked = objques.isactive;
                        chkShowReport.Checked = objques.showreport;

                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_Answer(Int32 SurveyQuestionID)
        {

            try
            {
                if (Session["UserID"] != null)
                {
                    ClsSurveyquestions objques = ServicesFactory.DocCMSServices.Fetch_question_Byid(SurveyQuestionID);
                    if (objques != null)
                    {
                        lblQuesName.InnerText = objques.question;
                        lblQSurveyName.InnerText = txtSurveyName.Value;
                        lblQType.InnerText = Convert.ToString(objques.answerType);
                        QuestionSurveyid = Convert.ToInt32(objques.surveyquestionid);
                        DivAddAnser.Visible = true;
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        protected void rptrQuestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblAnswerType = (Label)e.Item.FindControl("lblAnswerType");
                Label lblbar = (Label)e.Item.FindControl("lblbar");

                LinkButton lnkAddAnswer = (LinkButton)e.Item.FindControl("lnkAddAnswer");
                if (lblAnswerType != null && lnkAddAnswer != null && lblAnswerType.Text.ToUpper() == "TEXT" && lblbar != null)
                {
                    lblbar.Visible = false;
                    lnkAddAnswer.Visible = false;
                }
                if (Session["HiddenRow"] != null)
                {
                    string ItemIndex = Convert.ToString(Session["HiddenRow"]);
                    if (e.Item.ItemIndex.ToString() == ItemIndex)
                    {
                        e.Item.Visible = false;
                    }
                }
            }
        }

        protected void btnSaveAnswer_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    ClsSurveyanswers clsSurvey = new ClsSurveyanswers();

                    clsSurvey.answer = txtQAnswer.Text;
                    clsSurvey.surveyid = Surveyid;
                    clsSurvey.surveyquestionid = QuestionSurveyid;
                    clsSurvey.displayorder = Convert.ToInt32(txtAnsDorder.Text);
                    if (ChkAnsActive.Checked)
                        clsSurvey.isactive = true;

                    else
                        clsSurvey.isactive = false;

                    // check Survey name

                    bool AnswerExist = ServicesFactory.DocCMSServices.ChkDuplicate_answer(txtQAnswer.Text.Trim(), QuestionSurveyid);
                    if (AnswerExist == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Answer Already Exists...');", true);
                        return;

                    }
                    else
                    {
                        Int32 retVal = ServicesFactory.DocCMSServices.Insert_Survey_answer(clsSurvey);
                        if (retVal > 0)
                        {
                            if (ChkAnotherAnswer.Checked)
                            {
                                trAddAnsRow.Visible = false;
                                txtQAnswer.Text = "";
                                txtAnsDorder.Text = "";
                                Bind_Answer_Dashboard(QuestionSurveyid);
                                trAddAnsRow.Visible = true;
                                Int32 AnsDorder = ServicesFactory.DocCMSServices.Get_Max_answer_dorder(QuestionSurveyid) + 1;
                                txtAnsDorder.Text = Convert.ToString(AnsDorder);
                            }
                            else
                            {
                                trAddAnsRow.Visible = false;
                                Bind_Answer_Dashboard(QuestionSurveyid);
                                spanAnotherQues.Visible = true;
                                btnSaveAnswer.Visible = false;
                                spanAnotherAnswer.Visible = false;

                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Bind Data For Questions after Saving  Master Details
        protected void Bind_Answer_Dashboard(Int32 SurveyQuestionId)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_answer_Byquestionid_Dashboard(SurveyQuestionId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    rptrAnswer.DataSource = dt;
                    rptrAnswer.DataBind();
                }
                else
                {
                    rptrAnswer.DataSource = null;
                    rptrAnswer.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnUpdateAnswer_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    ClsSurveyanswers clsSurvey = new ClsSurveyanswers();

                    clsSurvey.answer = txtQAnswer.Text;
                    clsSurvey.surveyid = Surveyid;
                    clsSurvey.surveyquestionid = QuestionSurveyid;
                    clsSurvey.surveyanswerid = AnswerSurveyid;
                    clsSurvey.displayorder = Convert.ToInt32(txtAnsDorder.Text);
                    if (ChkAnsActive.Checked)
                    {
                        clsSurvey.isactive = true;
                    }

                    else
                    {
                        clsSurvey.isactive = false;
                    }

                    Int32 retVal = ServicesFactory.DocCMSServices.Update_Survey_answer(clsSurvey);
                    if (retVal > 0)
                    {
                        trAddAnsRow.Visible = false;
                        Bind_Answer_Dashboard(QuestionSurveyid);
                        btnUpdateAnswer.Visible = false;
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancelAnswer_onclick(object sender, EventArgs e)
        {
            DivQuestionContent.Visible = false;
            DivSurveyContent.Visible = true;
            btnAddQuestion.Visible = false;
            DivQuestion.Visible = true;
            txtSurveyName.Style.Add("display", "none");
            lblSurveyName.Style.Add("display", "block");
            DivDescription.Visible = false;
            DivDisplayOrder.Visible = false;
            DivCheckStatus.Visible = false;
            AddFeature.Visible = true;
            lblQuestion.Visible = false;
            btnSaveQuestion.Visible = false;
            btnCancelQuestion.Visible = true;
            spanAnotherQues.Visible = false;
            btnsubmitMaster.Visible = false;
            btnMasterupdate.Visible = false;
            btnCancel.Visible = false;
            trAddAnsRow.Visible = false;
            txtAnsDorder.Text = "";
            txtQAnswer.Text = "";
            ChkAnsActive.Checked = true;
            btnSaveAnswer.Visible = false;
            btnUpdateAnswer.Visible = false;
            spanAnotherAnswer.Visible = false;
            trAdditionalRow.Visible = false;
            if (Request.QueryString["SurveyID"] != null)
            {
                DivQuestion.Visible = true;
                btnCancelQuestion.Visible = true;
                Bind_Question();
            }
            else
            {
            }
        }

        protected void rptrAnswer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                LinkButton lnkAnswer = (LinkButton)e.Item.FindControl("lnkAnswer");
                Label lblAnswerID = (Label)e.Item.FindControl("lblAnswerID");
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                Label lblActive = (Label)e.Item.FindControl("lblActive");
                LinkButton lnkEditAnswer = (LinkButton)e.Item.FindControl("lnkEditAnswer");

                if (lnkAnswer != null && lblAnswerID != null && lblDorder != null && lblActive != null && lnkEditAnswer != null)
                {
                    switch (e.CommandName)
                    {
                        case "EditAnswer":
                            e.Item.Visible = false;
                            trActionAnswer.Visible = true;
                            trAddAnsRow.Visible = true;
                            btnUpdateAnswer.Visible = true;
                            Session["HiddenAnsRow"] = Convert.ToString(e.Item.ItemIndex);
                            Bind_Answer_Dashboard(QuestionSurveyid);
                            Bind_EditAnswer(Convert.ToInt32(lblAnswerID.Text));
                            break;

                        case "DeleteAnswer":
                            ServicesFactory.DocCMSServices.Delete_SurveyanswerByquestionid(Convert.ToInt32(lblAnswerID.Text));
                            Bind_Answer_Dashboard(QuestionSurveyid);
                            break;
                    }
                }
            }
        }
        protected void Bind_EditAnswer(Int32 SurveyAnswerID)
        {
            try
            {
                ClsSurveyanswers objAnswer = ServicesFactory.DocCMSServices.Fetch_answer_Byid(SurveyAnswerID);
                if (objAnswer != null)
                {
                    txtQAnswer.Text = objAnswer.answer;
                    txtAnsDorder.Text = Convert.ToString(objAnswer.displayorder);
                    QuestionSurveyid = Convert.ToInt32(objAnswer.surveyquestionid);
                    AnswerSurveyid = Convert.ToInt32(objAnswer.surveyanswerid);
                    ChkAnsActive.Checked = objAnswer.isactive;
                    btnUpdateAnswer.Visible = true;
                    btnSaveAnswer.Visible = false;
                    spanAnotherAnswer.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        protected void ImgAddAnswer_Click(object sender, ImageClickEventArgs e)
        {
            trAddAnsRow.Visible = true;
            Int32 AnsDorder = ServicesFactory.DocCMSServices.Get_Max_answer_dorder(QuestionSurveyid) + 1;
            txtAnsDorder.Text = Convert.ToString(AnsDorder);
            txtQAnswer.Text = "";
            trActionAnswer.Visible = true;
            btnCancelAnswer.Visible = true;
            btnSaveAnswer.Visible = true;
            spanAnotherAnswer.Visible = true;
            ChkAnsActive.Checked = true;
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {

            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");

        }

        // Bind display order Function for Toggle
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_Survey();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
        }

        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

                if (Session["HiddenAnsRow"] != null)
                {
                    string ItemIndex = Convert.ToString(Session["HiddenAnsRow"]);
                    if (e.Item.ItemIndex.ToString() == ItemIndex)
                    {
                        e.Item.Visible = false;
                    }
                }
            }
        }
    }
}