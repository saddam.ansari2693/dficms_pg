﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class BlogCategoryDashboard : System.Web.UI.Page
    {
        bool btnStatus = false;
        string ShowTableSize;
        public Int32 tid { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "licontent";
            Session["Ulname"] = "ulcontent";

            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    Bind_Repeater();
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    SetControls();
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        //=========Bind Blogs Details============
       
        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                    Button1.Visible = true;
                else
                    Button1.Visible = false;
                foreach (RepeaterItem Item in RepeaterContent.Items)
                {

                    LinkButton linktitle = (LinkButton)Item.FindControl("linktitle");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    ImageButton LinkEdit = (ImageButton)Item.FindControl("LinkEdit");
                    ImageButton linkdelete = (ImageButton)Item.FindControl("linkdelete");
                    HtmlGenericControl SpanDiv = (HtmlGenericControl)Item.FindControl("SpanDiv");
                    Label lblActiveStatus = (Label)Item.FindControl("lblActiveStatus");
                    if (lblTitle != null && linktitle != null && btnOrderUP != null && btnOrderDown != null && LinkEdit != null && linkdelete != null && SpanDiv != null && lblActiveStatus != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            LinkEdit.Enabled = true;
                            linktitle.Visible = true;
                            lblTitle.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;
                            SpanDiv.Visible = true;
                            lblActiveStatus.Visible = false;
                        }
                        else
                        {
                            LinkEdit.Enabled = false;
                            linktitle.Visible = false;
                            lblTitle.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                            SpanDiv.Visible = false;
                            lblActiveStatus.Visible = true;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            linkdelete.Enabled = true;
                        else
                            linkdelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        
        private void Bind_Repeater()
        {
            DataTable dt = new DataTable();

            dt = ServicesFactory.DocCMSServices.Fetch_category_Details();
            if (dt != null && dt.Rows.Count > 0)
            {
                RepeaterContent.DataSource = dt;
                RepeaterContent.DataBind();
            }
        }

        protected void RepeaterContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "EditLink")
            {
                Response.Redirect("~/cadmin/BlogCategory.aspx?CategoryID=" + e.CommandArgument.ToString());
            }
            if (e.CommandName == "Delete")
            {
                ServicesFactory.DocCMSServices.Delete_category_By_id(Convert.ToInt32(e.CommandArgument));
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            if (e.CommandName == "OrderUp")
            {
                Label LabDisplayOrder = (Label)e.Item.FindControl("LabDisplayOrder");
                ServicesFactory.DocCMSServices.Reorder_category_Type(Convert.ToInt32(e.CommandArgument.ToString()), Convert.ToInt32(LabDisplayOrder.Text), "UP");
                Bind_Repeater();
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            if (e.CommandName == "OrderDown")
            {
                Label LabDisplayOrder = (Label)e.Item.FindControl("LabDisplayOrder");
                ServicesFactory.DocCMSServices.Reorder_category_Type(Convert.ToInt32(e.CommandArgument.ToString()), Convert.ToInt32(LabDisplayOrder.Text), "DOWN");
                Bind_Repeater();
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }

        protected void RepeaterContent_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }
        
        protected void cmdInsert_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/BlogCategory.aspx");
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            Int32 Retval = ServicesFactory.DocCMSServices.Update_category_Status(Convert.ToInt32(CurRestID.Value), Convert.ToBoolean(CurStatus.Value));
            Bind_Repeater();
        }

    }//=== class ends here ===
}