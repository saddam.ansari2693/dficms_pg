﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.Web.UI.HtmlControls;


namespace DocCMSMain.cadmin
{
    public partial class FrontEndContentPage : System.Web.UI.Page
    {
        public Int32 tid { get; set; }
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "licontent";
            Session["Ulname"] = "ulcontent";
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    Bind_Repeater();
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    SetControls();
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
               foreach (RepeaterItem Item in RepeaterContent.Items)
                {
                    HtmlGenericControl SpanDiv = (HtmlGenericControl)Item.FindControl("SpanDiv");
                    Label IsActive = (Label)Item.FindControl("IsActive");
                    if (SpanDiv != null && IsActive != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            SpanDiv.Visible = true;
                            IsActive.Visible = false;
                        }
                        else
                        {
                            SpanDiv.Visible = false;
                            IsActive.Visible = true;
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        private void Bind_Repeater()
        {
            DataTable dt = new DataTable();

            dt = ServicesFactory.DocCMSServices.Fetch_Content_For_FrontEndContentPage();
            if (dt != null && dt.Rows.Count > 0)
            {
                RepeaterContent.DataSource = dt;
                RepeaterContent.DataBind();
            }
        }
        protected void RepeaterContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "EditLink")
            {
                Response.Redirect("~/cadmin/BlogCategory.aspx?CategoryID=" + e.CommandArgument.ToString());

            }
            if (e.CommandName == "Delete")
            {
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            if (e.CommandName == "OrderUp")
            {
                Label LabDisplayOrder = (Label)e.Item.FindControl("LabDisplayOrder");
                Bind_Repeater();
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            if (e.CommandName == "OrderDown")
            {
                Label LabDisplayOrder = (Label)e.Item.FindControl("LabDisplayOrder");
                Bind_Repeater();
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }
        protected void RepeaterContent_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            Int32 Retval = ServicesFactory.DocCMSServices.Update_isactive_ContentPage_By_pageid(Convert.ToInt32(CurRestID.Value), Convert.ToBoolean(CurStatus.Value));
            Bind_Repeater();
        }
    }
}