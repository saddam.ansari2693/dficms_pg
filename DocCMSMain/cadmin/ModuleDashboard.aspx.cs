﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.Factory;

namespace DocCMSMain.cadmin
{
    public partial class ModuleDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Modules();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
                SetControls();
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddModuleTop.Visible = true;
                    btnAddModuleBottom.Visible = true;
                }
                else
                {
                    btnAddModuleTop.Visible = false;
                    btnAddModuleBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrModules.Items)
                {
                    LinkButton lnkModuleName = (LinkButton)Item.FindControl("lnkModuleName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    ImageButton lnkNavigate = (ImageButton)Item.FindControl("lnkNavigate");
                    if (lblTitle != null && lnkModuleName != null && btnOrderUP != null && btnOrderDown != null && lnkEdit != null && lnkDelete != null && lnkNavigate != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkModuleName.Visible = true;
                            lblTitle.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;
                            lnkNavigate.Enabled = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkModuleName.Visible = false;
                            lblTitle.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                            lnkNavigate.Enabled = false;
                        }

                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }

        // Bind Module inside RptrModules Repeater
        protected void Bind_Modules()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Modules();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrModules.DataSource = dt;
                    RptrModules.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrModules_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditModule":
                        Response.Redirect("~/cadmin/Modules.aspx?MID=" + e.CommandArgument.ToString());
                        break;
                    case "AddSubModule":
                        Response.Redirect("~/cadmin/SubModules.aspx?MID=" + e.CommandArgument.ToString());
                        break;
                    case "DeleteModule":
                        Int32 DelVal = Convert.ToInt32(Service.DbFunctions.SelectCount("[spCheckDependencyBeforeDelete] 'ModuleID','Modules,RolePrivileges','" + e.CommandArgument.ToString() + "'"));
                        if (DelVal > 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>alert('Sorry!, Record Cannot be deleted, because it has been captured by some other resource(s). Please contact administrator.');</script>");
                        }
                        else
                        {
                            Int32 retVal = ServicesFactory.DocCMSServices.Delete_Modules(Convert.ToInt32(e.CommandArgument));
                            Bind_Modules();
                            Response.Redirect("~/cadmin/ModuleDashboard.aspx");
                        }
                        break;
                    case "OrderUp":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_Modules(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        Bind_Modules();
                        break;
                    case "OrderDown":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_Modules(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        Bind_Modules();
                        break;
                    default:
                        break;

                }
            }
        }

        protected void RptrModules_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void btnAddModuleBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Modules.aspx");
        }
    }
}