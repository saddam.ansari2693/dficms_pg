﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveySummaryReport.aspx.cs" Inherits="DocCMSMain.cadmin.SurveySummaryReport" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DOCFOCUS SURVEY SUMMARY REPORT</title>
    <!-- Bootstrap -->
    <link href="../js/Survey/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  
    <link href="../js/Survey/css/main.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" language="javascript">
        function PrintPage() {
            window.print();
        }
    </script>
    <!--For PIE CHART-->
    <script src="../js/Survey/js/ChartNew.js" type="text/javascript"></script>
    <script type="text/javascript">
        var charJSPersonnalDefaultOptions = { decimalSeparator: ",", thousandSeparator: ".", roundNumber: "none", graphTitleFontSize: 2 };
        var opt1;
        var mydata2
        function SetChart(v1, v2) {
            var PosRes = parseFloat(v1);
            var NegRes = parseFloat(v2);
            if (PosRes > 0 && NegRes > 0) {
                mydata2 = [
	                    {
	                        value: v2,
	                        color: "#655C5A",
	                        title: "Negative Responses"
	                    },
	                    {
	                        value: v1,
	                        color: "#0866C6",
	                        title: "Positive Responses"

	                    }

                    ];
                opt1 = {
                    animationStartWithDataset: startWithDataset,
                    animationStartWithData: startWithData,
                    animateRotate: true,
                    animateScale: false,
                    animationByData: false,
                    animationSteps: 50,
                    graphTitle: "Survey Response Summary",
                    legend: true,
                    inGraphDataShow: true,
                    animationEasing: "easeOutBounce",
                    annotateDisplay: true,
                    spaceBetweenBar: 5,
                    graphTitleFontSize: 18

                }
            }
            if (PosRes > 0 && NegRes <= 0) {
                mydata2 = [
	                    {
	                        value: v1,
	                        color: "#0866C6",
	                        title: "Positive Responses"
	                    }

                    ];
                opt1 = {
                    animationStartWithDataset: startWithDataset,
                    animationStartWithData: startWithData,
                    animateRotate: true,
                    animateScale: false,
                    animationByData: false,
                    animationSteps: 50,
                    graphTitle: "Survey Response Summary",
                    legend: true,
                    inGraphDataShow: true,
                    animationEasing: "easeOutBounce",
                    annotateDisplay: true,
                    spaceBetweenBar: 5,
                    graphTitleFontSize: 18

                }
            }
            if (PosRes <= 0 && NegRes > 0) {
                mydata2 = [
	                    {
	                        value: v2,
	                        color: "#655C5A",
	                        title: "Negative Responses"
	                    }
                    ];
                opt1 = {
                    animationStartWithDataset: startWithDataset,
                    animationStartWithData: startWithData,
                    animateRotate: true,
                    animateScale: false,
                    animationByData: false,
                    animationSteps: 50,
                    graphTitle: "Survey Response Summary",
                    legend: true,
                    inGraphDataShow: true,
                    animationEasing: "easeOutBounce",
                    annotateDisplay: true,
                    spaceBetweenBar: 5,
                    graphTitleFontSize: 18

                }
            }

            var startWithDataset = 1;
            var startWithData = 1;
          }
        function drawChart(v1, v2) {
            SetChart(v1, v2);
            var myPie = new Chart(document.getElementById("canvas_Pie1").getContext("2d")).Pie(mydata2, opt1);
        }        
    </script>
    <!--END For PIE CHART-->
</head>
<body>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="wrapper" id="MainDiv" runat="server" clientidmode="Static">
        <h2>
            Survey Results for Business Groups Survey</h2>
        <br />
        <label>
            <strong>SurveyName:</strong>
        </label>
        <asp:Literal ID="LitSurveyName" runat="server"></asp:Literal>
        <br />
        <label>
            <strong>Survey Description:</strong>
        </label>
        <asp:Literal ID="LitSurveyDescription" runat="server"></asp:Literal>
        <div class="report-box pull-left">
            <label>
                <strong>Select Report Period:</strong>
            </label>
            <asp:DropDownList ID="ddlYear" runat="server">
            </asp:DropDownList>
            &nbsp;&nbsp;<asp:DropDownList ID="ddlMonth" runat="server">
                </asp:DropDownList>
            <asp:Button ID="btnShowReport" runat="server" ClientIDMode="Static" Text="Show Report"
                CssClass="btn btn-default btn-sm" OnClick="btnShowReport_Click" />
                &nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnExport" runat="server" ClientIDMode="Static" Text="Export Report"
                CssClass="btn btn-default btn-sm" OnClick="btnExportReport_Click" Visible="false" />
            <br />
            <asp:Label ID="lblMessage" runat="server" ClientIDMode="Static" Text="Please Select Year and Month"
                Style="color: Red; font-weight: bold;" Visible="false"></asp:Label>
            <asp:Label ID="lblReportStatus" runat="server" ClientIDMode="Static" Text="No Response(s) found for the selected response period"
                Style="color: Red; font-weight: bold;" Visible="false"></asp:Label>
        </div>
        <div class="space-10">
        </div>
        <div class="report-box pull-left">
            This Survey will be used to learn more about what people are expecting from the
            program.
            <div class="pull-right">
                <asp:LinkButton ID="lnkback" runat="server" Text="Back" CssClass="btn btn-default btn-sm"
                    OnClick="lnkback_Click"></asp:LinkButton>
                <input type="button" id="btnPrint" class="btn btn-default btn-sm" value="Print Report" onclick="PrintPage();" runat="server" visible="false"/>
                 </div>
            <div class="space-10">
            </div>
            <div class="form-group clearfix" style="margin-bottom: 0px;">
                <label for="inputEmail3" class="col-sm-3 control-label no-padd-lft normal">
                    Included Response Periods:</label>
                <div class="col-sm-5 clearfix">
                    <asp:Literal runat="server" ID="litFallYear"></asp:Literal>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 no-padd-lft no-padd-lft normal	 ">
                    Compare Results By:</label>
                <div class="col-sm-5">
                    Response Periods
                </div>
            </div>
        </div>
        <h2>
            Overall Summary</h2>
        <div class="space-10">
        </div>
        <div class="report-box pull-left">
            <asp:Literal runat="server" ID="LitTotalResponse"></asp:Literal>
            <div class="space-10">
            </div>
            <asp:Repeater ID="repeateryearlysummary" runat="server" OnItemDataBound="repeateryearlysummary_ItemDataBound">
                <ItemTemplate>
               <asp:HiddenField ID="hdnResponded" runat="server" />
                <asp:HiddenField ID="hdnNonResponded" runat="server" />
                <asp:HiddenField ID="hdnTotal" runat="server" />
              
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 no-padd-lft no-padd-lft normal">
                            Response Rate for
                            <%# Eval("FallMonth") %>
                            <%# Eval("FallYear") %></label>
                        <div class="col-sm-6 no-padd-lft">
                            <div class="progress progress-bg">
                                <asp:Label ID="lableft" runat="server" Visible="false" Text='<%# Eval("perleft")%>'></asp:Label>
                                <asp:Label ID="labright" runat="server" Visible="false" Text='<%# Eval("perright")%>'></asp:Label>
                                <asp:Label ID="lblTotal" runat="server" Visible="false" Text='<%# Eval("TotalFYResponse")%>'></asp:Label>
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                    aria-valuemin="0" aria-valuemax='<%# Eval("TotalFYResponse") %>' style="background-color: #0866c6!important;"
                                    id="divgreen" runat="server">
                                    <%# Eval("NoOfReposesPart")%>
                                </div>
                                <div class="progress-bar progress-bar-fail" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax='<%# Eval("TotalFYResponse") %>' style="background-color: #655c5a!important;"
                                    id="divblack" runat="server">
                                    <%# Eval("NoOfPartLeft")%>
                                </div>
                            </div>
                        </div>
                        <div class="pull-left txt-11" style="margin-left: -10px; margin-top: 3px;" id="DivTotalResponseFallYear"
                            runat="server">
                            <%# Eval("TotalFYResponse")%>
                            Total Participants</div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    <div class="form-group">
                        <label class="col-sm-4 no-padd-lft no-padd-lft normal" for="inputEmail3">
                        </label>
                        <div class="col-sm-8">
                            <asp:Image ID="imagex" runat="server" ImageUrl="~/images/XAxis.png" Style="width: 81.7%;
                                margin-left: -23px;" />
                        </div>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <div class="space-20">
                <div class="pull-left">
                    <h3>
                    </h3>
                </div>
                <div class="pull-left space-10">
                </div>
                <div class="pull-left space-10 margin-lft-10">
                </div>
            </div>
            <div class="space-20 clearfix">
            </div>
            <div class="round-gray space-20">
                <div class="pull-left">
                    <h3>
                        SURVEY RESPONDENTS</h3>
                </div>
                <div class="pull-left space-10">
                    <input type="button" class="xs-green-btn" style="background-color: #0866c6 !important;" />
                    Responded</div>
                <div class="pull-left space-10 margin-lft-10">
                    <input type="button" class="xs-black-btn" style="background-color: #655c5a !important;" />
                    Not Responded</div>
                <div class="clearfix">
                </div>
            </div>
            <div>
                <hr />
            </div>
            <div class="col-lg-3 no-padd bolder" style="margin-top: -20px; margin-left: 30%;">
                <div id="divChart" style="width: 98%;" align="center">
                    <canvas id="canvas_Pie1" height="350" width="350" align="center"></canvas>
                </div>
                </div>
        </div>
        <div id="divDetail" runat="server">
            <h2>
                Detailed Question Results</h2>
            <asp:Repeater ID="RepeaterQuestion" runat="server" OnItemDataBound="RepeaterQuestion_OnItemDataBound">
                <ItemTemplate>
                    <asp:HiddenField ID="hdnTotalResponse" runat="server" />
                    <asp:HiddenField ID="hdnPercentage" runat="server" />
                      <asp:HiddenField ID="hdnQTypeMsg" runat="server" />
                    <div class="green-fade-bx">
                        <div class="green-fade" style="background-color: #0866c6!important;">
                            <div style="margin-left: 40px; padding-left: 40px; color: White;">
                                <asp:Label ID="lblSurveyID" runat="server" Text='<%# Eval("SurveyID")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblQuestionID" runat="server" Text='<%# Eval("SurveyQuestionID")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblFieldType" runat="server" Text='<%# Eval("AnswerType")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblQuestionType" runat="server" Text='<%# Eval("AnswerType")%>'
                                    Visible="false"></asp:Label>
                                <%# Container.ItemIndex +1 %>.
                                <%# Eval("Question")%></div>
                        </div>
                        <div class="col-lg-offset-1 col-lg-11 space-20 table-responsive">
                            <table style="width: 100%;">
                                <tr>
                                    <td colspan="5" style="width: 100%; float: left; vertical-align: middle;">
                                        <asp:Literal ID="litQTypeMsg" runat="server" ClientIDMode="Static"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="width: 15%; vertical-align: middle;">
                                    </td>
                                    <td style="width: 11%; vertical-align: middle; margin-left: 2%; padding-left: 120px;">
                                        Percentage:
                                    </td>
                                    <td style="width: 15%; text-align: right; vertical-align: middle;">
                                        Number:
                                    </td>
                                </tr>
                                <asp:Repeater runat="server" ID="RepeaterResponse" OnItemDataBound="RepeaterResponse_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td id="tdAnsText" runat="server" clientidmode="Static" tyle="width: 30%; vertical-align: middle;">
                                                <%# Eval("AnswerText")%>
                                            </td>
                                            <td style="width: 30%; vertical-align: middle;">
                                                <div class="progress short-progress" style="margin-top: 5px;">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                                        aria-valuemin="0" aria-valuemax="100" style="width: <%# Eval("Percentage")%>;
                                                        background-color: #0866c6!important;">
                                                    </div>
                                                </div>
                                            </td>
                                            <td id="tdEmpty" runat="server" clientidmode="Static">
                                            </td>
                                            <td style="width: 15%; vertical-align: middle; margin-left: 2%; padding-left: 140px;">
                                                <%# Eval("Percentage")%>
                                            </td>
                                            <td style="width: 15%; text-align: center; vertical-align: middle; padding-left: 50px;">
                                                <%# Eval("ResponseCount")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td colspan="3" style="width: 25%; vertical-align: middle;">
                                    </td>
                                    <td style="width: 15%; text-align: left; vertical-align: middle; padding-left: 140px;">
                                        Total
                                    </td>
                                    <td style="width: 15%; text-align: center; border-top: double #ddd 3px; vertical-align: middle;
                                        padding-left: 47px;">
                                        <asp:Label ID="lblTotalResponse" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div style="height: 10px;">
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    </form>
</body>
</html>

