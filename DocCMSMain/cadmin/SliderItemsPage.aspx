﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SliderItemsPage.aspx.cs" Inherits="DocCMSMain.cadmin.SliderItemsPage" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/SliderResizer.ascx" TagName="SliderResizer" TagPrefix="ucResizer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/bootstrap-fileupload.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Jcrop/Jcrop.css" rel="stylesheet" type="text/css" />
    <link href="css/colopicker/css/jquery.minicolors.css" rel="stylesheet" type="text/css" />
    <script src="css/colopicker/js/jquery.minicolors.min.js" type="text/javascript"></script>
    <script src="../Jcrop/Jcrop.js" type="text/javascript"></script>
    <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wysiwyg.js"></script>
    <script type="text/javascript" src="../js/ContentSpecialScripts.js"></script>
    <script src="../js/Pages/media_manager.js" type="text/javascript"></script>
    <script type="text/javascript">
        function isNumber(evt) {
            var key;
            if (window.evt) {
                key = window.evt.keyCode;     //IE
            }
            else {
                key = evt.which;      //firefox              
            }
            if ((key >= 48 && key <= 57)) {
                return true;
            }
            else if (key == 0 || key == 8 || key == 118 || key == 45)//|| key == 9 || key == 27 || key == 32 || key == 35 || key == 46 || key == 59 || key == 64 || key == 38 || key == 39 || key == 40 || key == 41 || key == 46 || key == 91 || key == 93 || key == 123 || key == 125 || key == 127)) {
            {
                return true;
            }
            else {
                return false;
            }
        }
        function SaveMsg(Alias) {
            alert("SMTP Detail " + Alias + " Suceessfully");
            var url = "";
            url = "../cadmin/SMTPSettings.aspx";
            $(location).attr('href', url);
        }
        //Start Image Preview Function
        function ShowPreview(input) {
            if (input.files && input.files[0]) {
                var ImageDir = new FileReader();
                ImageDir.onload = function (e) {
                    $('#imgLogo').attr('src', e.target.result);
                }
                ImageDir.readAsDataURL(input.files[0]);
            }
        }

        //End Image Preview Function
        function ChangeImage() {
            jQuery("#imgLogo").attr("src", "../UploadedFiles/No_image_available.png");
            jQuery("#imgLogo").attr("src", "../UploadedFiles/SliderImage/" + jQuery("#hdnCurPageID").val() + "/" + jQuery("#hdnCurCMSID").val() + "/" + jQuery("#ddlSliderPosition").val() + "_" + jQuery("#lblFileUploader").text());
        }

        function Show_UploadedImage(Image) {
        }
        function ManageDimensions() {
            if ($("#chkFullWidth").is(":checked")) {
                $("#txtWidth").val("0");
                $("#txtWidth").attr("disabled", true);
            }
            else if ($("#chkFullWidth").is(":not(:checked)")) {
                $("#txtWidth").attr("disabled", false);
            }
        }
    </script>
    <script type="text/javascript">
        jQuery(function () {
            var colpick = jQuery('.demo').each(function () {
                jQuery(this).minicolors({
                    control: jQuery(this).attr('data-control') || 'hue',
                    inline: jQuery(this).attr('data-inline') === 'true',
                    letterCase: 'lowercase',
                    opacity: false,
                    change: function (hex, opacity) {
                        if (!hex) return;
                        if (opacity) hex += ', ' + opacity;
                        try {
                            console.log(hex);
                        } catch (e) { }
                        jQuery(this).select();
                    },
                    theme: 'bootstrap'
                });
            });
            var $inlinehex = jQuery('#inlinecolorhex h3 small');
            jQuery('#inlinecolors').minicolors({
                inline: true,
                theme: 'bootstrap',
                change: function (hex) {
                    if (!hex) return;
                    $inlinehex.html(hex);
                }
            });
        });
        function onlyNumbers(event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        jQuery(document).ready(function () {
            var url = window.location;
            if (url.toString().indexOf("?EmailID=") > -1) {
                // Do nothing
            }
            else {
                jQuery("#txtEmailId").val("");
                jQuery("#txtNewPswd").val("");
            }
        });          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Manage Slider</a> <span class="separator"></span></li>
        <li>Slider Item Settings</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Slider</h5>
            <h1>
                Slider Item Settings</h1>
        </div>
    </div>
    <%--Resizer control Goes here--%>
    <ucResizer:SliderResizer ID="Reziser1" runat="server" />
    <%--Resizer control Ends here--%>
    <!--pageheader-->
    <div class="maincontent" id="DivSurveyContent" runat="server">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <asp:HiddenField ID="hdnSliderItemId" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="hdnSliderId" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="hdnImageName" ClientIDMode="Static" runat="server" />
            <asp:Button ID="btnPrieviewImage" runat="server" OnClick="btnPrieviewImage_Click" Style="display: none;" />
            <div class="widget">
                <h4 class="widgettitle" id="sliderWidgettitle" runat="server">
                    Slider Managment Settings &nbsp;&nbsp;&nbsp;[Note: Full Width Slider Image Dimensions
                    : (1900 x 700 pixels) , Left Slider Image Dimensions : (497 x 288 pixels) and Right
                    Slider Image Dimensions : (497 x 288 pixels)]</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform" id="DivSMTPPage" runat="server">
                        <div class="par control-group" id="divSliderPosition" runat="server" clientidmode="Static">
                            <label class="control-label" for="txtImageName">
                                Select Slider Position</label>
                            <div class="controls">
                                <div style="">
                                    <asp:DropDownList ID="ddlSliderPosition" runat="server" ClientIDMode="Static" onchange="return ChangeImage();">
                                        <asp:ListItem Value="ORG" Text="Original Image"></asp:ListItem>
                                        <asp:ListItem Value="FullWidth" Text="Full Width Slider"></asp:ListItem>
                                        <asp:ListItem Value="Left" Text="Left/Right Slider"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtImageName">
                                Slider Image</label>
                            <div class="controls">
                                <div style="">
                                    <img alt="" src="../UploadedFiles/No_image_available.png" runat="server" id="imgLogo"
                                        clientidmode="Static" style="height: 100px; width: 100px; margin-top: 0px;" />
                                    <br />
                                    <asp:FileUpload ID="ImageUploadControl" Style="margin-left: 220px" runat="server"
                                        ClientIDMode="Static" CssClass="input-upload" />
                                    <asp:Label runat="server" ID="lblFileUploader" ClientIDMode="Static" Style="display: none;"></asp:Label>
                                </div>
                                <div style="margin-bottom: 20px; margin-left: 500px;">
                                    <img src="../images/Edit-image.png" style="width: 24px;" title="Edit Image" onclick="EditImage(this.id);"
                                        runat="server" id="imgEdit" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="par control-group">
                            <label class="control-label" for="txtheight">
                                Dorder</label>
                            <div class="controls">
                                <asp:TextBox ID="txtdorder" runat="server" CssClass="input-large" ClientIDMode="Static"
                                    onkeypress="javascript:return isNumber(event)" ValidationGroup="grpsubmit" placeholder="Enter Order"
                                    Enabled="false"></asp:TextBox>
                                <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                    Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click" />
                                <asp:RequiredFieldValidator ID="rqdImageHeight" ValidationGroup="grpsubmit" ControlToValidate="txtdorder"
                                    runat="server" Style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus" style="margin-top: -10px;">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    clientidmode="Static" style="opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group">
                            <p class="stdformbutton">
                                <asp:Button ID="btnsubmitItemMaster" runat="server" class="btn btn-primary" Visible="false"
                                    OnClick="btnsubmitItemMaster_onclick" Text="Save" Width="150px" ValidationGroup="grpsubmit" />
                                <asp:Button ID="btnMasterItemupdate" runat="server" class="btn btn-primary" OnClick="btnMasterItemupdate_onclick"
                                    Text="Update" Width="150px" ValidationGroup="grpsubmit" Visible="false" />
                                <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                    OnClick="btnCancel_Click" />
                            </p>
                        </div>
                    </div>
                </div>
                <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                                <h4 id="myModalLabel" class="modal-title">
                                    Update Slider Images Display Orders
                                </h4>
                            </div>
                            <div class="modal-body" style="height: 640px;">
                                <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                    <colgroup>
                                        <col class="con1" style="align: left; width: 70%;" />
                                        <col class="con0" style="align: right; width: 30%;" />
                                    </colgroup>
                                    <thead>
                                        <tr style="width: 100%;">
                                            <th>
                                                Slider&nbsp;Image
                                            </th>
                                            <th id="ActiveStatusheading" runat="server">
                                                Status
                                            </th>
                                            <th>
                                                Display&nbsp;Order
                                                <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                    onclick="ToggleDisplay();" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="overflow-y: auto;">
                                        <asp:Repeater runat="server" ID="RptrDisplayOrder">
                                            <ItemTemplate>
                                                <tr class="gradeX">
                                                    <td>
                                                        <asp:Label ID="lblSliderItemId" runat="server" Text='<%# Eval("SliderItemID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:Image ID="Imgslider" runat="server" ImageUrl='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/ORG_"+Eval("ImageName")%>'
                                                            alt='<%# Eval("ImageName")%>' Style="width: 50px; height: 50px;" />
                                                    </td>
                                                    <td id="Activestatusdata" runat="server">
                                                        <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderItemID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                            CommandName="EditPageContent"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                        <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)"
                                                            maxlength="5" class="DorderText" style="display: none; width: 100px; float: left;
                                                            margin-right: 25px;" value='<%# Eval("Dorder") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tr class="gradeA">
                                                    <td>
                                                        <asp:Label ID="lblSliderItemId" runat="server" Text='<%# Eval("SliderItemID") %>' Visible="false"></asp:Label>
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:Image ID="Imgslider" runat="server" ImageUrl='<%#"../UploadedFiles/SliderImage/"+Eval("SliderID")+"/"+Eval("SliderItemID")+"/ORG_"+Eval("ImageName")%>'
                                                            alt='<%# Eval("ImageName")%>' Style="width: 50px; height: 50px;" />
                                                    </td>
                                                    <td id="Activestatusdata" runat="server">
                                                        <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderItemID") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                            CommandName="EditPageContent"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                        <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)"
                                                            maxlength="5" class="DorderText" style="display: none; width: 100px; float: left;
                                                               margin-right: 25px;" value='<%# Eval("Dorder") %>' />
                                                    </td>
                                                </tr>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btnUpdate" class="btn btn-primary" onclick="ValidateDisplayOrders();"
                                    value="Update Display Orders" />
                                <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                    style="display: none;"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                    OnClick="btnUpdateDisplayOrder_Click" />
                <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
                <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                    TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
                </asp:ModalPopupExtender>
            </div>
        </div>
        <!--widgetcontent-->
    </div>
    <!--widget-->
    <asp:HiddenField ID="hdnSMTPId" runat="server" />
    <asp:HiddenField ID="hdnSMTPDetailId" runat="server" />
    <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnDropDownListVal" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnListBoxVal" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCurDivID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="XAxis" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="YAxis" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="Width" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="Height" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="maxWidth" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="maxHeight" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField2" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnDisplayOrder" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnDropDownListValLevel1" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField3" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField4" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hndImageFileName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnActualImageWidth" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnActualImageHeight" runat="server" ClientIDMode="Static" />
    <uc1:CadminFooter ID="CadminFooter1" runat="server" />
    <!-- for Display Order Popup -->
    <!--maincontentinner-->
    <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
        role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
        top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
    </ul>
    <!--maincontent-->
    <!----Question Content ends here--->
    <script language="javascript" type="text/javascript">
        function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
        }

        function AddQues() {
            var ImgAddQuestion = document.getElementById("ImgAddQuestion");
            ImgAddQuestion.click();
        }
        function AddAns() {
            var ImgAddAnswer = document.getElementById("ImgAddAnswer");
            ImgAddAnswer.click();
        }

        function RemoveFeature(elem) {
            var id = $(elem).attr("id");
            var GetId = id.toString().split('_');
            $("#divFeature_" + GetId[1]).style.visibility = "hidden";
        }
    </script>
    <script language="javascript" type="text/javascript">
        //Start Image Preview Function
        function ShowPreview(fileInput) {
            var fileinputidval = fileInput.id;
            var thumbnilid = "";
            var Get_FileinputID = fileinputidval.toString().split('_');
            if (Get_FileinputID.length > 3) {
                thumbnilid = Get_FileinputID[0] + "_" + Get_FileinputID[1] + "_ImgCMSFileUploader_" + Get_FileinputID[3];
            }
            var files = fileInput.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var ext = file.name.split('.').pop().toLowerCase();
                var imageType = /image.*/;
                var filetype = file.type
                var img = document.getElementById(thumbnilid.toString());
                img.file = file;
                var reader = new FileReader();
                if (file.type.match(imageType)) {
                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = e.target.result;
                        };
                    })(img);
                    reader.readAsDataURL(file);
                }
                else if (ext == "pdf") {
                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = '../UploadedFiles/pdf.png';
                        };
                    })(img);
                    reader.readAsDataURL(file);
                }
                else if (ext == "doc" || ext == "txt") {
                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = '../UploadedFiles/doc.png';
                        };
                    })(img);
                    reader.readAsDataURL(file);
                }
                else if (ext == "xls" || ext == "cvc") {
                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.src = '../UploadedFiles/xls.png';
                        };
                    })(img);
                    reader.readAsDataURL(file);
                }
            }
        }
    </script>
    <link href="../css/cropfunctionality.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField ID="hdnCurCMSID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCurPageID" runat="server" ClientIDMode="Static" />
</asp:Content>
