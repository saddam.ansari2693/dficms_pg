﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class Blogs : System.Web.UI.Page
    {
        public Int32 BlogID { get; set; }
        public string Userid { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["linkname"] = "licontent";
                Session["Ulname"] = "ulcontent";
                Bind_DisplayOrder();
                Bind_BlogCategoryName();
                if (Request.QueryString["BlogID"] != null)
                {
                    BlogID = Convert.ToInt32(Request.QueryString["BlogID"]);
                    Set_Data(BlogID);
                    btnEdit.Visible = true;
                    btnsubmit.Visible = false;
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Blog_Type_dorder() + 1;
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnEdit.Visible = false;
                    btnsubmit.Visible = true;
                }
            }
        }

        // Bind Blog Name inside ddlBlogname dropdown
        protected void Bind_BlogCategoryName()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Blog_category_name_By_dorder();
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlBlogCategory.DataSource = dt;
                    ddlBlogCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_click(Object sender, EventArgs e)
        {
            try
            {
                bool BlogTitleExist = false;
                BlogTitleExist = ServicesFactory.DocCMSServices.ChkDuplicate_BlogTitle(txtBlogName.Value);
                if (BlogTitleExist == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Blog with Same Name Already Exists');", true);
                }
                else
                {
                    Cls_Blog objsection = new Cls_Blog();
                    objsection.blogname = txtBlogName.Value.Trim();
                    objsection.blogcategoryid = Convert.ToInt32(ddlBlogCategory.SelectedValue);
                    objsection.blogpostedby = txtPostedBy.Value.Trim();
                    objsection.navigationurl= txtNavigationUrl.Value.Trim();
                    objsection.description = elm1.Value;
                    objsection.accesstype = ddlAccessType.SelectedValue;
                    objsection.blogtype = Convert.ToInt32(drpBlogType.SelectedValue);
                    string ImageBlogExt = System.IO.Path.GetExtension(ImageUploadControl.FileName);
                    if ((ImageBlogExt == ".jpg" || ImageBlogExt == ".png" || ImageBlogExt == ".jpeg" || ImageBlogExt == ".bmp" || ImageBlogExt == ".gif"))
                    {
                        string BlogDirectory = Server.MapPath(@"../UploadedFiles/UploadedFiles/Blogs/");
                        if (!Directory.Exists(BlogDirectory))
                        {
                            Directory.CreateDirectory(BlogDirectory);
                        }
                        string FileName = System.IO.Path.GetFileName(ImageUploadControl.FileName);
                        var Title = FileName;
                        Title = Title.Replace(" ", "_");
                        ImageUploadControl.SaveAs(BlogDirectory + Title + ImageBlogExt.ToString());
                        objsection.imagename = Title + ImageBlogExt;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Please Upload the Image for Blog...');</script>");
                        return;
                    }
                    objsection.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                    objsection.createdby = Convert.ToString(Session["UserID"]);
                    if (ChkActiveStatus.Checked)
                        objsection.isactive = true;
                    else
                        objsection.isactive = false;
                    if (ChkAllowPosting.Checked)
                        objsection.allowposting = true;
                    else
                        objsection.allowposting = false;

                    ServicesFactory.DocCMSServices.Insert_BlogDetails(objsection);
                    ServicesFactory.DocCMSServices.Adjust_Blogs_dorder();
                    Response.Redirect("~/cadmin/BlogDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }

        protected void btnreset_click(Object sender, EventArgs e)
        {
            reset_form();
        }

        protected void reset_form()
        {
            
        }

        protected void btnCancel_click(Object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/BlogDashboard.aspx");
        }

        protected void btnEdit_click(Object sender, EventArgs e)
        {

            try
            {
                if (Request.QueryString["BlogID"] != null)
                {
                    Cls_Blog objsection = new Cls_Blog();
                    objsection.blogtype = Convert.ToInt32(drpBlogType.SelectedValue);
                    objsection.blogname = txtBlogName.Value.Trim();
                    objsection.blogpostedby = txtPostedBy.Value.Trim();
                    objsection.navigationurl = txtNavigationUrl.Value.Trim();
                    objsection.description = elm1.Value;
                    objsection.accesstype = ddlAccessType.SelectedValue;
                    objsection.blogcategoryid = Convert.ToInt32(ddlBlogCategory.SelectedValue);
                    string ImageBlogExt = System.IO.Path.GetExtension(ImageUploadControl.FileName).ToLower();
                    if ((ImageBlogExt == ".jpg" || ImageBlogExt == ".png" || ImageBlogExt == ".jpeg" || ImageBlogExt == ".bmp" || ImageBlogExt == ".gif"))
                    {
                        string BlogDirectory = Server.MapPath(@"../UploadedFiles/UploadedFiles/Blogs/");
                        if (!Directory.Exists(BlogDirectory))
                        {
                            Directory.CreateDirectory(BlogDirectory);
                        }
                        string FileName = System.IO.Path.GetFileName(ImageUploadControl.FileName);
                        var Title = FileName;
                        Title = Title.Replace(" ", "_");
                        ImageUploadControl.SaveAs(BlogDirectory + Title + ImageBlogExt.ToString());
                        objsection.imagename = Title + ImageBlogExt;
                    }
                    else
                    {
                       objsection.imagename= lblFileUploader.Text;
                    }
                    objsection.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                    objsection.lastmodifiedby = Convert.ToString(Session["UserID"]);
                    objsection.blogid = Convert.ToInt32(Request.QueryString["BlogID"]);
                    if (ChkActiveStatus.Checked)
                        objsection.isactive = true;
                    else
                        objsection.isactive = false;
                    if (ChkAllowPosting.Checked)
                        objsection.allowposting = true;
                    else
                        objsection.allowposting = false;
                    ServicesFactory.DocCMSServices.Update_Blog_By_id(objsection);
                    ServicesFactory.DocCMSServices.Adjust_Blogs_dorder();
                    Response.Redirect("~/cadmin/BlogDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Set_Data(Int32 BlogID)
        {
            try
            {
                if ((Request.QueryString["BlogID"] != null))
                {
                    Cls_Blog obj = new Cls_Blog();
                    obj = ServicesFactory.DocCMSServices.Fetch_Blog_By_id(BlogID);
                    if (obj != null)
                    {
                        drpBlogType.SelectedValue = Convert.ToString(obj.blogtype);
                        txtBlogName.Value= obj.blogname;
                        txtPostedBy.Value = obj.blogpostedby;
                        txtNavigationUrl.Value = obj.navigationurl;
                        txtDisplayOrder.Value = Convert.ToString(obj.dorder);
                        elm1.Value = obj.description;
                        ChkActiveStatus.Checked = obj.isactive;
                        ChkAllowPosting.Checked = obj.allowposting;
                        ChkActiveStatus.Checked = obj.isactive;
                        lblFileUploader.Text = obj.imagename;
                        ddlAccessType.SelectedValue = obj.accesstype;
                        string serverpath = @"../UploadedFiles/UploadedFiles/Blogs/" + obj.imagename;
                        imgLogo.Src = serverpath;
                        ddlBlogCategory.SelectedValue =Convert.ToString(obj.blogcategoryid);
                        DataTable dtCmsId = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(BlogID);
                        hdnCurPageID.Value = Convert.ToString(BlogID);
                        {
                            if (dtCmsId != null && dtCmsId.Rows.Count > 0)
                            {
                                hdnCurCMSID.Value = Convert.ToString(dtCmsId.Rows[0]["CMSID"]);
                                hdnCurPageID.Value = Convert.ToString(BlogID);
                            }
                        }
                    }
                }
            }
            catch
            {
                //Nothing
            }
        }

        // Bind Display order at Toggle inside RptrDisplayOrder repeater
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Blog_Order_By_dorder();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_Blog> lstBlog= new List<Cls_Blog>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblBlogID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblBlogID");
                Label lblBlogName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblBlogName");
                if (Session["UserID"] != null && lblBlogName != null && lblBlogID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblBlogName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_Blog objCategory = new Cls_Blog();
                Label lblBlogID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblBlogID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblBlogID != null)
                {
                    objCategory.blogid = Convert.ToInt32(lblBlogID.Text.Trim());
                    objCategory.dorder = Convert.ToInt32(txtDorder.Value);
                    objCategory.lastmodifiedby = Convert.ToString(Session["UserID"]);
                    lstBlog.Add(objCategory);
                }
            }
            if (lstBlog != null && lstBlog.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Blog_dorder(lstBlog);
                if (retVal > 0)
                {

                    if (Request.QueryString["BlogID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["BlogID"] + ");</script>");
                        Set_Data(Convert.ToInt32(Request.QueryString["BlogID"]));
                        Bind_DisplayOrder();
                        btnsubmit.Visible = false;
                        btnEdit.Visible = true;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                        Int32 DisplayOrder1 = ServicesFactory.DocCMSServices.Get_Max_Blog_Type_dorder() + 1;
                        Bind_DisplayOrder();
                        txtDisplayOrder.Value = Convert.ToString(DisplayOrder1);
                        btnsubmit.Visible = true;
                        btnEdit.Visible = false;
                    }
                }
            }
        }

        protected void ddlBlogCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }
     }//==== class ends here 
}