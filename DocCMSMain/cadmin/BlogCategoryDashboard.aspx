﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="BlogCategoryDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.BlogCategoryDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
        function CheckedChange(MemberID, CtrlCheck, EmailID) {
            jQuery("#CurRestID").val(MemberID);
            jQuery("#CurEmailID").val(EmailID);
            var btn = document.getElementById("btnUpdateStatus");
            if (jQuery(CtrlCheck).prop("checked") == true) {
                if (confirm('Are you sure you want to Activate?')) {
                    jQuery("#CurStatus").val("True");
                    btn.click();
                }
                else {
                    jQuery(CtrlCheck).prop("checked", false);
                    jQuery(CtrlCheck).closest("span").removeClass("checked");
                }
            }
            else {
                if (confirm('Are you sure you want to Deactivate?')) {
                    jQuery("#CurStatus").val("False");
                    btn.click();
                }
                else {
                    jQuery(CtrlCheck).prop("checked", true);
                    jQuery(CtrlCheck).closest("span").addClass("checked");
                }
            }
        }
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }

        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
        }
    </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;
            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' #IsActives').prop('checked') == false) {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' #IsActives').prop('checked') == false) {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Content Management Dashboard</a> <span class="separator"></span>
        </li>
        <li>Manage Blog category</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5 id="h5heading1" runat="server">
                Content management Dashboard</h5>
            <h1 id="h1heading1" runat="server">
                Blog Category</h1>
        </div>
    </div>
    <!--pageheader-->
    <div id="lblError" class="alert-customerror" runat="server" visible="false">
        This page is referenced somewhere.It can't be deleted
    </div>
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle" id="h4heading11" runat="server">
                <!--Filer by Status starts here--->
                <div class="par_ctrl_sec">
                    <div class="par control-group">
                        <label class="control-label" for="ddlStatus">
                            Filer by Status</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                                <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!--Filer by Status ends here--->
                <label id="h4heading1" runat="server">
                    Blog Category
                </label>
                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Width="150" Text="Add Category"
                    OnClick="cmdInsert_Click" Style="float: right; margin-top: -25px;" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" style="align: center; width: 10%" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head0">
                            Category Name
                        </th>
                        <th class="head1">
                            Display Order
                        </th>
                        <th class="head1">
                            Active Status
                        </th>
                        <th class="head1">
                            Creation Date
                        </th>
                        <th class="head1" style="text-align: center;">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RepeaterContent" OnItemCommand="RepeaterContent_ItemCommand"
                        OnItemDataBound="RepeaterContent_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:LinkButton ID="linktitle" CommandArgument='<%# Eval("BlogCategoryID")%>' CommandName="EditLink"
                                        runat="server" Text='<%# Eval("BlogCategoryName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("BlogCategoryName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("BlogCategoryID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="LabDisplayOrder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'></asp:Label>
                                    <div style="display: inline; margin-left: 20px;">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("BlogCategoryID")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("BlogCategoryID")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' Visible="false" ID="IsActive"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("ActiveStatus")%>' Visible="false" class="Status"
                                        ID="lblActiveStatus"></asp:Label>
                                    <span id="SpanDiv" runat="server">
                                    <input type="checkbox" id="IsActives" onchange="return CheckedChange(<%# Eval("BlogCategoryID")%>,this);"
                                           <%# Eval("IsActive")%> /></span>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate","{0:MM/dd/yyyy}")%>' ID="Label3"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="LinkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("BlogCategoryID")%>' CommandName="EditLink" ToolTip="Edit" />
                                    <asp:ImageButton runat="server" ID="linkdelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("BlogCategoryID")%>' CommandName="Delete" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this?');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:LinkButton ID="linktitle" CommandArgument='<%# Eval("BlogCategoryID")%>' CommandName="EditLink"
                                        runat="server" Text='<%# Eval("BlogCategoryName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("BlogCategoryName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("BlogCategoryID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="LabDisplayOrder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'></asp:Label>
                                    <div style="display: inline; margin-left: 20px;">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("BlogCategoryID")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("BlogCategoryID")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' Visible="false" ID="IsActive"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("ActiveStatus")%>' Visible="false" class="Status"
                                        ID="lblActiveStatus"></asp:Label>
                                    <span id="SpanDiv" runat="server">
                                        <input type="checkbox" id="IsActives" onchange="return CheckedChange(<%# Eval("BlogCategoryID")%>,this);"
                                            <%# Eval("IsActive")%> /></span>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate","{0:MM/dd/yyyy}")%>' ID="Label3"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="LinkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("BlogCategoryID")%>' CommandName="EditLink" ToolTip="Edit" />
                                    <asp:ImageButton runat="server" ID="linkdelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("BlogCategoryID")%>' CommandName="Delete" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this?');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <asp:Button ID="btnUpdateStatus" runat="server" ClientIDMode="Static" Style="display: none;"
                Text="Update" OnClick="btnUpdateStatus_Click" />
            <asp:HiddenField ID="CurRestID" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="CurStatus" runat="server" ClientIDMode="Static" />
            <br />
            <br />
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
</asp:Content>
