﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="JobApplication.aspx.cs" Inherits="DocCMSMain.cadmin.JobApplication" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
         jQuery(document).ready(function () {
             var showValue = '<%= Session["showtablesize"] %>';
             if (showValue == null || showValue == "") {
                 showValue = 10;
             }
             jQuery('#dyntable').dataTable({
                 "sPaginationType": "full_numbers",
                 "iDisplayLength": parseInt(showValue, 10),
                 "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                 "fnDrawCallback": function (oSettings) {
                     jQuery.uniform.update();
                 }
             });
             jQuery('#dyntable2').dataTable({
                 "bScrollInfinite": true,
                 "bScrollCollapse": true,
                 "sScrollY": "300px"
             });
         });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
              });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {

                },
                error: function () {

                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Job Applications</a> <span class="separator"></span></li>
        <li>Manage Job Applications</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Job Applications</h5>
            <h1>
              Job Applications</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              Job Applications Detail
             <label id="Label2" runat="server" style="font-size: 13px; width: 150px; margin-left:1084px; margin-top:-16px">
                            <strong>Select Export Format:</strong>
                        </label>
                        <asp:DropDownList ID="ddlExport" runat="server" ClientIDMode="Static" 
                          style="font-size: 13px; width: 170px; margin-left:1214px; margin-top:-25px;">                        
                        <asp:ListItem Value="0" Text="Excel"> </asp:ListItem>
                          <asp:ListItem Value="1" Text="CSV(Comma Separated)"> </asp:ListItem>
                          <asp:ListItem Value="2" Text="CSV(Space Separated)"> </asp:ListItem>
                          <asp:ListItem Value="3" Text="PDF"> </asp:ListItem>
                        </asp:DropDownList>
             <asp:Button ID="btnExportTop" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Export"
                    Style="margin-top:-42px; margin-left:1400px; float:left;" onclick="btnExportTop_Click" />
            </h4>
              <asp:Panel ID="PanelJob" runat="server">
            <table id="dyntable" class="table table-bordered responsive" >
                <colgroup>
                    <col class="con0" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 10%;" />
                    <col class="con1" style="width: 12%;" />  
                    <col class="con1" style="width: 12%;" />  
                    <col class="con1" style="align: center; width: 10%;" />   
                 <col class="con1" style="align: center; width: 21%;" /> 
                 <col class="con1" style="align: center; width: 5%;" />   
                    <col class="con1" style="align: center; width: 5%;" />   
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                         Job Title
                        </th>
                        <th class="head1">
                          Applied By
                          </th>
                              <th class="head1">
                          Contact No.
                        </th>
                             <th class="head1">
                          Email ID
                        </th>
                        <th class="head1">
                         Resume File
                        </th>
                        <th class="head1">
                           Description
                        </th>
                       <th class="head1">
                           Skills
                        </th>
                          <th class="head1">
                           Applied Date
                        </th>
                         <th class="head1">
                          Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrJob" OnItemCommand="RptrJob_ItemCommand"
                        OnItemDataBound="RptrJob_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                  <asp:Label ID="lblJobApplyID" runat="server" ClientIDMode="Static" Text='<%# Eval("JobApplyID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("JobTitle")%>' ID="lblJobTitle"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("Name")%>' ID="lblName"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("PhoneNo")%>' ID="Label1"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("EmailID")%>' ID="lblEmailID"></asp:Label>
                                </td>
                                <td>
                                   <asp:LinkButton runat="server"  CommandName="DownloadPDF" ID="lnkFileName" Text='<%# Eval("FileName")%>'></asp:LinkButton>
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("Filename")%>' Visible="false"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("Description")%>' ID="lblDescription"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("Skills")%>' ID="lblskills"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("ApplyDate")%>' ID="lblApplyDate"></asp:Label>
                                </td>
                           <td class="center">
                                   <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("JobApplyID")%>' CommandName="DeleteJob" ToolTip="Delete Job Application" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText=" Are you sure to delete?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                <td>
                                  <asp:Label ID="lblJobApplyID" runat="server" ClientIDMode="Static" Text='<%# Eval("JobApplyID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("JobTitle")%>' ID="lblJobTitle"></asp:Label>
                                </td>

                                      <td>
                                 <asp:Label runat="server" Text='<%# Eval("Name")%>' ID="lblName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("PhoneNo")%>' ID="Label1"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("EmailID")%>' ID="lblEmailID"></asp:Label>
                                </td>
                                <td>
                                  <asp:LinkButton runat="server"  CommandName="DownloadPDF" ID="lnkFileName" Text='<%# Eval("FileName")%>'></asp:LinkButton>
                                   <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("FileName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("Description")%>' ID="lblDescription"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("Skills")%>' ID="lblskills"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("ApplyDate")%>' ID="lblApplyDate"></asp:Label>
                                </td>
                       
                                <td class="center">
                                 <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("JobApplyID")%>' CommandName="DeleteJob" ToolTip="Delete Job Application" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText=" Are you sure to delete?"
                                        TargetControlID="lnkDelete" />
                                </td>
                           </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            </asp:Panel>
            <br />
            <br />
            <asp:Button ID="btnExportBottom" runat="server" CssClass="btn btn-primary" Width="150"
                Text="Export" OnClick="btnExportTop_Click" />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
    </div>
</asp:Content>