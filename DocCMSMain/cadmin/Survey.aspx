﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="Survey.aspx.cs" Inherits="DocCMSMain.cadmin.Survey" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../js/Pages/media_manager.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
    <link href="css/SurveyStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/Popup.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jscolor.js"></script>
    <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript">
        var iClick = 1;
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8 || key == 46) {
                return true;
            }
            if (key > 31 && (key < 48 || key > 57))
                return false;
            return true;
        }

        function ConfirmDelete() {
            if (confirm("Are you sure to delete this feature from the plan?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ValidateQuestEntry() {
            var validFlag = true;
            txtQDorder
            chkQActive

            if ($("#txtQuest").val().trim() == "") {
                validFlag = false;
            }
            else if ($("#txtQDorder").val().trim() == "") {
                validFlag = false;
            }
            else {
                return true;
            }
            if (validFlag != true) {
                alert("Please Make proper entry for the question");
                return validFlag;
            }
        }

            function ValidateAnswerEntry() {
            var validFlag = true;
            if ($("#txtQAnswer").val().trim() == "") {
                validFlag = false;
            }
            else if ($("#txtAnsDorder").val().trim() == "") {
                validFlag = false;
            }
            else {
                return true;
            }
            if (validFlag != true) {
                alert("Please Make proper entry for the answer");
                return validFlag;
            }
        }
    </script>
   
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $("#txtNavigationUrl").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../cadmin/SubModules.aspx/GetKeyWords",
                        data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }
        function SearchText_btn() {
           $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../cadmin/SubModules.aspx/GetKeyWords_btnClick",
                data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                dataType: "json",
                success: function (data) {
                    var dataArray = data.d.toString().split(',');
                    var List = "";
                    for (var i = 0; i < dataArray.length; i++) {
                        List = List + "<li class='ui-menu-item customMenu' role='menuitem' id='liAutoComplete_" + i.toString() + "'>";
                        var NavigateURL = "";
                        if (i == 0) {
                            NavigateURL = dataArray[i].toString().trim().substring(2);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        else if (i == dataArray.length - 1) {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 2);
                        }
                        else {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        if (dataArray.length == 1) {
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        List = List + "<a class='ui-corner-all' tabindex='-1' id='ancAutoComplete_" + i.toString() + "' onclick='SetValueInTextBox(this);'>" + NavigateURL + "</a>";
                        List = List + "</li>";
                    }
                    $("#ulAutocomplete").html(List);
                    $("#ulAutocomplete").show();

                },
                error: function (result) {
                    alert("No Match");
                }
            });

        }
        function SetValueInTextBox(ctrlID) {
            $("#txtNavigationUrl").val($("#" + ctrlID.id).text());
            $("#ulAutocomplete").hide();
        }
    </script>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlFruits) {
            var selectedText = ddlAnswerOption.options[ddlAnswerOption.selectedIndex].innerHTML;
            var selectedValue = ddlAnswerOption.value;
            if (selectedValue == "Description Type") {
                $("#ddlItems").css("display", "none");
            }
            else if (selectedValue == "Single Choice Type") {
                $("#ddlItems").css("display", "block");
            }
            else if (selectedValue == "Option Type") {
                $("#ddlItems").css("display", "block");
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Survey</a> <span class="separator"></span></li>
        <li>Manage Survey</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Survey</h5>
            <h1>
                Manage Survey</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent" id="DivSurveyContent" runat="server">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <div class="widget">
                <h4 class="widgettitle">
                    Survey Details</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform" id="DivSurveyPage" runat="server">
                        <div class="par control-group">
                            <label class="control-label" for="txtSurveyName">
                                Survey Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtSurveyName" id="txtSurveyName" class="input-large"
                                    clientidmode="Static" placeholder="Enter Survey Name" />
                                <label runat="server" name="lblSurveyName" id="lblSurveyName" class="input-large"
                                    clientidmode="Static" style="display: none; text-align: left;width:80%;">
                                </label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtSurveyName" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group" id="DivDescription" runat="server">
                            <label class="control-label" for="txtDescription">
                                Survey Description</label>
                            <div class="controls">
                                <textarea id="txtDescription" name="txtDescription" rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="height: 100px; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" placeholder="Enter Survey Description"></textarea>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtDescription" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group" id="DivDisplayOrder" runat="server">
                            <label class="control-label" for="txtDisplayOrder">
                                Display Order</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtDisplayOrder" id="txtDisplayOrder" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Display Order" maxlength="5"  disabled/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtDisplayOrder" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                              </div>
                        </div>
                        <div class="par control-group" id="DivCheckStatus" runat="server">
                            <label class="control-label" for="ChkActiveStatus">
                                Set as Default</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    style=" margin-top:10px; opacity: 1" checked  />
                            </div>
                        </div>
                            <div class="par control-group" id="DivReport" runat="server">
                            <label class="control-label" for="ChkReport">
                                Show Survey Screen Results</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="ChkReport" id="ChkReport" class="input-large"
                                    style=" margin-top:10px; opacity: 1" checked  />
                            </div>
                        </div>
                        <div class="par control-group" id="DivQuestion" runat="server">
                            <label class="control-label" for="tblFeature">
                                Questions</label>
                            <div class="controls">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ClientIDMode="Static">
                                    <ContentTemplate>
                                        <table id="tblQuestion" class="table table-bordered responsive " style="margin-left: 150px;
                                            width: 80%;">
                                            <colgroup>
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 10%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                            </colgroup>
                                            <thead id="tblQuestionhead" runat="server">
                                                <tr>
                                                    <th class="head1" style="height: 20px;">
                                                        <div style="display: inline; cursor: pointer;" class="AddFeature" id="AddFeature"
                                                            runat="server" onclick="return AddQues();">
                                                            <asp:ImageButton ID="ImgAddQuestion" runat="server" ClientIDMode="Static" ImageUrl="../images/plus-icon.png"
                                                                ToolTip="Add Question" OnClick="ImgAddQuestion_Click" />
                                                            <span style="margin-top:10px; "> Add Question</span></div>
                                                        <label id="lblQuestion" runat="server" clientidmode="Static" visible="false" style="text-align: left;">
                                                            Question</label>
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Question Type
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Display Order
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Status
                                                    </th>
                                                     <th class="head1" style="height: 10px;">
                                                       Show Results
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Operations
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="tblQuestionBody" runat="server">
                                                <asp:Repeater ID="rptrQuestion" runat="server" ClientIDMode="Static" OnItemCommand="rptrQuestion_ItemCommand"
                                                    OnItemDataBound="rptrQuestion_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr class="gradeX">
                                                            <td>
                                                                <asp:LinkButton ID="lnkQuestion" CommandName="EditQuestion" CommandArgument='<% Eval("Question") %>'
                                                                    Text='<%# Eval("Question") %>' runat="server"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAnswerType" runat="server" Text='<%# Eval("AnswerType") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblQuestionID" runat="server" Text='<%# Eval("SurveyQuestionID") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                                <td>
                                                                <asp:Label ID="lblShowReport" runat="server" Text='<%# Eval("ShowReport") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkEditAnswer" CommandName="EditQuestion" CommandArgument='<% Eval("SurveyQuestionID") %>'
                                                                    runat="server" Style="cursor: pointer">Edit</asp:LinkButton>
                                                                <asp:Label ID="lblbar" runat="server" Text="|"></asp:Label>
                                                                <asp:LinkButton ID="lnkAddAnswer" CommandName="AddAnswer" CommandArgument='<% Eval("SurveyQuestionID") %>'
                                                                    runat="server" Style="cursor: pointer">Add Answer</asp:LinkButton>
                                                                     <asp:Label ID="Label1" runat="server" Text="|"></asp:Label>
                                                                      <asp:LinkButton ID="lnkDeleteQuestion" CommandName="DeleteQuestion" CommandArgument='<% Eval("SurveyQuestionID") %>'
                                                                    runat="server" Style="cursor: pointer">Delete Question</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr class="gradeA">
                                                            <td>
                                                                <asp:LinkButton ID="lnkQuestion" CommandName="EditQuestion" CommandArgument='<% Eval("Question") %>'
                                                                    Text='<%# Eval("Question") %>' runat="server"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAnswerType" runat="server" Text='<%# Eval("AnswerType") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblQuestionID" runat="server" Text='<%# Eval("SurveyQuestionID") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                                <td>
                                                                <asp:Label ID="lblShowReport" runat="server" Text='<%# Eval("ShowReport") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkEditAnswer" CommandName="EditQuestion" CommandArgument='<% Eval("SurveyQuestionID") %>'
                                                                    runat="server" Style="cursor: pointer">Edit</asp:LinkButton>
                                                                <asp:Label ID="lblbar" runat="server" Text="|"></asp:Label>
                                                                <asp:LinkButton ID="lnkAddAnswer" CommandName="AddAnswer" CommandArgument='<% Eval("SurveyQuestionID") %>'
                                                                    runat="server" Style="cursor: pointer">Add Answer</asp:LinkButton>
                                                                    <asp:Label ID="Label1" runat="server" Text="|"></asp:Label>
                                                                       <asp:LinkButton ID="lnkDeleteQuestion" CommandName="DeleteQuestion" CommandArgument='<% Eval("SurveyQuestionID") %>'
                                                                    runat="server" Style="cursor: pointer">Delete Question</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                </asp:Repeater>
                                                <tr class="gradeX" id="trAdditionalRow" runat="server" clientidmode="Static" visible="false">
                                                    <td>
                                                        <asp:TextBox ID="txtQuest" runat="server" placeholder="Enter Question" ClientIDMode="Static"
                                                            class="input-ques"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlQType" runat="server" ClientIDMode="Static">
                                                            <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                                            <asp:ListItem Text="Single Choice" Value="Option"></asp:ListItem>
                                                            <asp:ListItem Text="Multiple Choice" Value="Check"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtQDorder" placeholder="Enter Display Order" runat="server" onkeypress="return onlyNumbers(event)"
                                                            ClientIDMode="Static"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkQActive" runat="server" ClientIDMode="Static" Checked/>
                                                        Active
                                                    </td>
                                                     <td>
                                                        <asp:CheckBox ID="chkShowReport" runat="server" ClientIDMode="Static" Checked/>
                                                   Show Survey Screen Results
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr id="trActionQuestion" runat="server" clientidmode="Static" visible="false">
                                                    <td colspan="5">
                                                        <asp:Button ID="btnSaveQuestion" runat="server" class="btn btn-primary" Visible="false" Width="150px"
                                                            ClientIDMode="Static" Text="Save Question" ValidationGroup="ValidateQuestEntry"
                                                            OnClientClick="return ValidateQuestEntry();" OnClick="btnSaveQuestion_Click" />
                                                        <asp:Button ID="btnUpdateQuestion" runat="server" class="btn btn-primary" Visible="false" OnClientClick="return ValidateQuestEntry();" Width="150px"
                                                            ClientIDMode="Static" Text="Update Question" ValidationGroup="ValidateQuestEntry"
                                                            OnClick="btnUpdateQuestion_Click" />
                                                      
                                                        <span id="spanAnotherQues" runat="server" visible="false">
                                                            <input type="checkbox" runat="server" name="activestatus" id="ChkAnotherQues" class="input-large"
                                                                style="opacity: 1" checked />
                                                            Create Another Question</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                              
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <br />
                        <p class="stdformbutton">
                            <asp:Button ID="btnsubmitMaster" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnsubmitMaster_onclick" Text="Save" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnMasterupdate" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnMasterupdate_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit" />
                                   <asp:Button ID="btnAddQuestion" runat="server" class="btn btn-info" Text="Add Question" Width="150px"
                                OnClick="btnAddQuestion_Click" Visible="false" />
                            <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                OnClick="btnCancel_Click" />
                          <asp:Button ID="btnCancelQuestion" runat="server" class="btn" ClientIDMode="Static"
                                                  Width="150px" Text="Return" CausesValidation="false" OnClick="btnCancelQuestion_Click" />
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            <!-- for Display Order Popup -->
        </div>
        <!--maincontentinner-->
        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
            role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
        </ul>
    </div>
    <!--maincontent-->
    <!---- Question content Start here---->
    <div class="maincontent" id="DivQuestionContent" runat="server" visible="false">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <div class="widget">
                <h4 class="widgettitle">
                    Add Survey Answer</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="Div2" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="Label2" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform" id="DivQuestionPart" runat="server">
                        <div class="par control-group">
                            <label class="control-label" for="lblQSurveyName">
                                Survey name</label>
                            <div class="controls">
                                <label runat="server" name="lblQSurveyName" id="lblQSurveyName" class="input-large" 
                                    clientidmode="Static" style="text-align: left;width:80%;">
                                </label>
                            </div>
                        </div>
                        <br />
                        <div class="par control-group">
                            <label class="control-label" for="lblQuesName">
                                Question</label>
                            <div class="controls">
                                <label runat="server" name="lblQuesName" id="lblQuesName" class="input-large" style="text-align: left;width:80%;"
                                    clientidmode="Static">
                                </label>
                            </div>
                        </div>
                        <br />
                        <div class="par control-group">
                            <label class="control-label" for="lblQType">
                                Question Type</label>
                            <div class="controls">
                                <label runat="server" name="lblQType" id="lblQType" class="input-large" style="text-align: left; width:80%;"
                                    clientidmode="Static">
                                </label>
                            </div>
                        </div>
                        <br />
                        <div class="par control-group" id="Div7" runat="server">
                            <label class="control-label" for="tblFeature">
                                Answer</label>
                            <div class="controls">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                                    <ContentTemplate>
                                        <table id="tblAnswer" class="table table-bordered responsive " style="margin-left: 150px;
                                            width: 80%;">
                                            <colgroup>
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                                <col class="con1" style="align: center; width: 20%;" />
                                            </colgroup>
                                            <thead id="tblHeadAnswer" runat="server">
                                                <tr>
                                                    <th class="head1" style="height: 20px;">
                                                        <div style="display: inline; cursor: pointer;" class="AddFeature" id="DivAddAnser" onclick="return AddAns();"
                                                            runat="server">
                                                            <asp:ImageButton ID="ImgAddAnswer" runat="server" ClientIDMode="Static" ImageUrl="../images/plus-icon.png"
                                                                ToolTip="Add Answer" OnClick="ImgAddAnswer_Click" />
                                                            Add Answer</div>
                                                        <label id="lblAddAnswer" runat="server" clientidmode="Static" visible="false" style="text-align: left;">
                                                            Answer</label>
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Display Order
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Active
                                                    </th>
                                                    <th class="head1" style="height: 20px;">
                                                        Operations
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="Tbody1" runat="server">
                                                <asp:Repeater ID="rptrAnswer" runat="server" ClientIDMode="Static" 
                                                    OnItemCommand="rptrAnswer_ItemCommand" 
                                                    onitemdatabound="rptrAnswer_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr class="gradeX">
                                                            <td>
                                                                <asp:LinkButton ID="lnkAnswer" CommandName="EditAnswer" CommandArgument='<% Eval("SurveyAnswerID") %>'
                                                                    Text='<%# Eval("Answer") %>' runat="server"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAnswerID" runat="server" Text='<%# Eval("SurveyAnswerID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkEditAnswer" CommandName="EditAnswer" CommandArgument='<% Eval("SurveyAnswerID") %>'
                                                                    runat="server">Edit</asp:LinkButton>
                                                                <asp:Label ID="Label1" runat="server" Text="|"></asp:Label>
                                                                <asp:LinkButton ID="linkDeleteAnswer" CommandName="DeleteAnswer" CommandArgument='<% Eval("SurveyAnswerID") %>'
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr class="gradeA">
                                                            <td>
                                                                <asp:LinkButton ID="lnkAnswer" CommandName="EditAnswer" CommandArgument='<% Eval("SurveyAnswerID") %>'
                                                                    Text='<%# Eval("Answer") %>' runat="server"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAnswerID" runat="server" Text='<%# Eval("SurveyAnswerID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkEditAnswer" CommandName="EditAnswer" CommandArgument='<% Eval("SurveyAnswerID") %>'
                                                                    runat="server">Edit</asp:LinkButton>
                                                                <asp:Label ID="Label1" runat="server" Text="|"></asp:Label>
                                                                <asp:LinkButton ID="linkDeleteAnswer" CommandName="DeleteAnswer" CommandArgument='<% Eval("SurveyAnswerID") %>'
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                </asp:Repeater>
                                                <tr class="gradeX" id="trAddAnsRow" runat="server" clientidmode="Static" visible="false">
                                                    <td>
                                                        <asp:TextBox ID="txtQAnswer" runat="server" placeholder="Enter Answer" ClientIDMode="Static"
                                                            class="input-ques" ></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAnsDorder" placeholder="Enter Display Order" runat="server" onkeypress="return onlyNumbers(event)"
                                                             ClientIDMode="Static" ></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="ChkAnsActive" runat="server" ClientIDMode="Static" Checked/>
                                                        Active
                                                    </td>
                                                </tr>
                                                <tr id="trActionAnswer" runat="server" clientidmode="Static" visible="false">
                                                    <td colspan="5">
                                                        <asp:Button ID="btnSaveAnswer" runat="server" class="btn btn-primary" Visible="false"
                                                            ClientIDMode="Static" OnClientClick="return ValidateAnswerEntry();" OnClick="btnSaveAnswer_onclick" Text="Save" Width="150px" 
                                                            ValidationGroup="grpsubmit" />
                                                        <asp:Button ID="btnUpdateAnswer" runat="server" class="btn btn-primary" Visible="false" OnClientClick="return ValidateAnswerEntry();"
                                                            OnClick="btnUpdateAnswer_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit"
                                                            ClientIDMode="Static" />
                                                        <span id="spanAnotherAnswer" runat="server">
                                                            <input type="checkbox" runat="server" name="activestatus" id="ChkAnotherAnswer" class="input-large"
                                                                style="opacity: 1" checked />
                                                            Create Another Answer</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <p class="stdformbutton">
                        <asp:Button ID="btnCancelAnswer" runat="server" class="btn" ClientIDMode="Static" OnClick="btnCancelAnswer_onclick" Text="Return" Width="150px" />
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <uc1:CadminFooter ID="CadminFooter2" runat="server" />
            <!-- for Display Order Popup -->
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                Update Survey Display Orders
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 70%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Survey&nbsp;Name
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                            <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                onclick="ToggleDisplay();" />
                                        </th>
                                        </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrDisplayOrder">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("SurveyID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("SurveyName") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("DisplayOrder") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                <<td>
                                                    <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("SurveyID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("SurveyName") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("DisplayOrder") %>' />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnUpdateDisplay" class="btn btn-primary" onclick="ValidateDisplayOrders();"
                                value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
    <!----Question Content ends here--->
    <script language="javascript" type="text/javascript">
       function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function SuccessMsg(PlanID) {
            alert("The provided display orders are successfully Updated");
            var url = "";
            if (PlanID != null) {
                url = "../cadmin/Plans.aspx?PID=" + PlanID;
                $(location).attr('href', url);
            }
            else {
                url = "../cadmin/Plans.aspx";
                $(location).attr('href', url);
            }
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
        }

        function AddQues() {
            var ImgAddQuestion = document.getElementById("ImgAddQuestion");
            ImgAddQuestion.click();
        }

        function AddAns() {
            var ImgAddAnswer = document.getElementById("ImgAddAnswer");
            ImgAddAnswer.click();
        }

        function RemoveFeature(elem) {
            var id = $(elem).attr("id");
            var GetId = id.toString().split('_');
            $("#divFeature_" + GetId[1]).style.visibility = "hidden";
        }
    </script>
</asp:Content>
