﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserRoles : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["RID"] != null)
                {
                    Bind_Role_Data(Convert.ToInt32(Request.QueryString["RID"]));
                    Bind_DisplayOrder();
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_User_Role_Dorder() + 1;
                    Bind_DisplayOrder();
                    TxtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Role data when page with RoleId is existing
        protected void Bind_Role_Data(Int32 RoleID)
        {
            Cls_UserRoles objRoles = ServicesFactory.DocCMSServices.Fetch_UserRole_ById(RoleID);
            if (objRoles != null)
            {
                TxtUserRole.Value = objRoles.rolename;
                txtDescription.Value = objRoles.description;
                TxtDisplayOrder.Value = Convert.ToString(objRoles.displayorder);
                ChkActiveStatus.Checked = objRoles.active;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UserRoles clsUser = new Cls_UserRoles();
                clsUser.rolename = TxtUserRole.Value;
                clsUser.description = txtDescription.Value;
                clsUser.displayorder = Convert.ToInt32(TxtDisplayOrder.Value);
                clsUser.parentid = Convert.ToInt32(Session["RoleID"]);
                clsUser.createby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsUser.active = true;
                else
                    clsUser.active = false;

                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_Role(TxtUserRole.Value.Trim());
                if (RetCheck > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Role with similar Name Already Exists...');</script>");
                    return;
                }
                else
                {
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_User_Roles(clsUser);
                    if (retVal > 0)
                    {
                        ServicesFactory.DocCMSServices.Adjust_User_Role_Dorder();
                    }
                    Response.Redirect("~/cadmin/UserRolesDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UserRoles clsUser = new Cls_UserRoles();
                clsUser.rolename = TxtUserRole.Value;
                clsUser.description = txtDescription.Value;
                clsUser.displayorder = Convert.ToInt32(TxtDisplayOrder.Value);
                clsUser.lastmodifiedby = Guid.Parse(UserId);
                clsUser.parentid = Convert.ToInt32(Session["RoleID"]);
                clsUser.roleid = Convert.ToInt32(Request.QueryString["RID"]);
                if (ChkActiveStatus.Checked)
                    clsUser.active = true;
                else
                    clsUser.active = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UserRoles(clsUser);
                if (retVal > 0)
                {
                    ServicesFactory.DocCMSServices.Adjust_User_Role_Dorder();
                }
                Response.Redirect("~/cadmin/UserRolesDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind display order Function for Toggle
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Roles(Convert.ToInt32(Session["RoleID"]));
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserRolesDashboard.aspx");
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UserRoles> lstRoles = new List<Cls_UserRoles>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblRoleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblRoleID");
                Label lblRoleName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblRoleName");
                if (Session["UserID"] != null && lblRoleName != null && lblRoleID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblRoleName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }

                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }

            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UserRoles objRole = new Cls_UserRoles();
                Label lblRoleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblRoleID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblRoleID != null)
                {
                    objRole.roleid = Convert.ToInt32(lblRoleID.Text.Trim());
                    objRole.displayorder = Convert.ToInt32(txtDorder.Value);
                    objRole.lastmodifiedby = Guid.Parse(UserId);
                    lstRoles.Add(objRole);
                }
            }

            if (lstRoles != null && lstRoles.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Role_Dorder(lstRoles);
                if (retVal > 0)
                {

                    if (Request.QueryString["RID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["RID"] + ");</script>");
                        Bind_Role_Data(Convert.ToInt32(Request.QueryString["RID"]));
                        Bind_DisplayOrder();
                        btnsubmit.Visible = false;
                        btnupdate.Visible = true;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                        Int32 DisplayOrder1 = ServicesFactory.DocCMSServices.Get_Max_User_Role_Dorder() + 1;
                        Bind_DisplayOrder();
                        TxtDisplayOrder.Value = Convert.ToString(DisplayOrder1);
                        btnsubmit.Visible = true;
                        btnupdate.Visible = false;
                    }
                }
            }
        }
    }
}//=== CLASS ENDS HERE ======
