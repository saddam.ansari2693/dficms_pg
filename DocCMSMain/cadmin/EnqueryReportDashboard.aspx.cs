﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.IO;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DocCMSMain.cadmin
{
    public partial class EnqueryReportDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Enquiry();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
            }
        }


        // Bind Enquiry Details into RptrEnquiry Repeater
        protected void Bind_Enquiry()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_Enquiry();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrEnquiry.DataSource = dt;
                    RptrEnquiry.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrEnquiry_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
            }
        }

        protected void RptrEnquiry_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

            }
        }

        // TO Export the search result into Excel File Start Here
        // TO Create the DataTable for Exporting into Excel
        private DataTable CreateDataTable()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Name");
                dt.Columns.Add("EmailID");
                dt.Columns.Add("Subject");
                dt.Columns.Add("Message");
                dt.Columns.Add("EnquiryDate");                
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get the data from the Dashboard and Set into Export DataTable
        protected void btnExportTop_Click(object sender, EventArgs e)
        {
            DataTable dt;
            try
            {
                dt = CreateDataTable();
                foreach (RepeaterItem item in RptrEnquiry.Items)
                {
                    DataRow drow = dt.NewRow();//create a row instance
                    Label lblName = (Label)item.FindControl("lblName");
                    Label lblEmailId = (Label)item.FindControl("lblEmailId");
                    Label lblSubject = (Label)item.FindControl("lblSubject");
                    Label lblMessage = (Label)item.FindControl("lblMessage");
                    Label lblCreationDate = (Label)item.FindControl("lblCreationDate");
                    //----------------------------------------------------------------------//
                    if (lblName != null && lblEmailId != null && lblSubject != null && lblMessage != null && lblCreationDate != null)
                    {
                        dt.Rows.Add(lblName.Text.Trim(), lblEmailId.Text.Trim(), lblSubject.Text.Trim(), lblMessage.Text.Trim(), lblCreationDate.Text.Trim());
                    }
                }
                if (dt.Rows.Count > 0 && dt.Rows != null)
                {
                    if (ddlExport.SelectedValue == "0")
                    {
                        ServicesFactory.DocCMSServices.ExporttoExcel(dt, "EnqueryReport.xls", "Name", "Email ID", "Subject", "Message", "Enquiry date", "");
                    }
                    // for comma Separated
                    else if (ddlExport.SelectedValue == "1")
                    {
                        ServicesFactory.DocCMSServices.ExportToCSVFile(dt, "Detail", ",");
                    }
                    // fopr space seaparated
                    else if (ddlExport.SelectedValue == "2")
                    {
                        ServicesFactory.DocCMSServices.ExportToCSVFile(dt, "Detail", ",");
                    }
                    // For PDF
                    else
                    {
                        Update_Pdf(true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        string PDFName = "";
        public float Update_Pdf(bool IsDownload)
        {
            string JobApplicationDirectoryPath = "";
            float TotalLoggedHours = 0.0f;
            try
            {
                PDFName = Convert.ToString("EnquiryReport");
                JobApplicationDirectoryPath = "/PDFs/";
                string appRootDir = Server.MapPath(JobApplicationDirectoryPath);
                // Step 1: Creating System.IO.FileStream object
                using (FileStream fs = new FileStream(appRootDir + "/" + PDFName.ToString() + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                // Step 2: Creating iTextSharp.text.Document object
                using (Document doc = new Document(PageSize.A4))
                // Step 3: Creating iTextSharp.text.pdf.PdfWriter object
                // It helps to write the Document to the Specified FileStream
                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    // Step 4: Openning the Document
                    doc.Open();

                    // Step 5: Adding a paragraph
                    // NOTE: When we want to insert text, then we've to do it through creating paragraph
                    // TO Add Header Rectangle and text
                    PdfContentByte cbHeader = writer.DirectContent;
                    // select the font properties
                    BaseFont bfHeader = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbHeader.SaveState();
                    cbHeader.SetColorFill(BaseColor.LIGHT_GRAY);
                    cbHeader.Rectangle(35, 800, 530, 20);
                    cbHeader.Fill();
                    cbHeader.RestoreState();
                    cbHeader.BeginText();
                    cbHeader.SetColorFill(BaseColor.WHITE);
                    cbHeader.SetFontAndSize(bfHeader, 11);
                    cbHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, " EnquiryReport", 550, 805, 0);
                    cbHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "DOCFOCUS", 120, 805, 0);
                    cbHeader.EndText();
                    doc.Add(new Paragraph(20, "\u00a0"));
                    // TO TIMESHEET HEADING
                    PdfContentByte cbHeading = writer.DirectContent;
                    // select the font properties
                    BaseFont bfcbHeading = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_Enquiry();

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        PdfPTable table1 = new PdfPTable(new float[] { 8f, 20f, 20f, 15f, 14f, 34f, 15f });
                        table1.WidthPercentage = 100;
                        BaseColor FontColor = new BaseColor(0, 0, 0);
                        var MyFont = FontFactory.GetFont("Arial", 7, FontColor);

                        // Main Coulm binding starts here................
                        PdfPCell cell2 = new PdfPCell(new Phrase("S.No."));
                        cell2.BackgroundColor = new BaseColor(82, 142, 212);
                        cell2.BorderColor = new BaseColor(82, 142, 212);
                        cell2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                        PdfPCell cell3 = new PdfPCell(new Phrase("Customer Name"));
                        cell3.BackgroundColor = new BaseColor(82, 142, 212);
                        cell3.BorderColor = new BaseColor(82, 142, 212);
                        cell3.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell cell4 = new PdfPCell(new Phrase("Email"));
                        cell4.BackgroundColor = new BaseColor(82, 142, 212);
                        cell4.BorderColor = new BaseColor(82, 142, 212);
                        cell4.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell cell5 = new PdfPCell(new Phrase("PhoneNo"));
                        cell5.BackgroundColor = new BaseColor(82, 142, 212);
                        cell5.BorderColor = new BaseColor(82, 142, 212);
                        cell5.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell cell6 = new PdfPCell(new Phrase("Subject"));
                        cell6.BackgroundColor = new BaseColor(82, 142, 212);
                        cell6.BorderColor = new BaseColor(82, 142, 212);
                        cell6.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell cell7 = new PdfPCell(new Phrase("Message"));
                        cell7.BackgroundColor = new BaseColor(82, 142, 212);
                        cell7.BorderColor = new BaseColor(82, 142, 212);
                        cell7.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell cell8 = new PdfPCell(new Phrase("CreationDate"));
                        cell8.BackgroundColor = new BaseColor(82, 142, 212);
                        cell8.BorderColor = new BaseColor(82, 142, 212);
                        cell8.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        // Main Coulm binding ends here...............
                        table1.AddCell(cell2);
                        table1.AddCell(cell3);
                        table1.AddCell(cell4);
                        table1.AddCell(cell5);
                        table1.AddCell(cell6);
                        table1.AddCell(cell7);
                        table1.AddCell(cell8);
                        for (Int32 i = 0; i < dt.Rows.Count; i++)
                        {
                            // setting font color and size for cell 
                            var FontColour = new BaseColor(0, 0, 0);
                            var newFont = FontFactory.GetFont("Arial", 8, FontColour);

                            // declaring varible for result
                            Int32 Sno = i + 1;
                            string CustomerName = Convert.ToString(dt.Rows[i]["Name"]);
                            string Email = Convert.ToString(dt.Rows[i]["EmailId"]);
                            string PhoneNo = Convert.ToString(dt.Rows[i]["PhoneNo"]);
                            string Subject = Convert.ToString(dt.Rows[i]["Subject"]);
                            string Message = Convert.ToString(dt.Rows[i]["Message"]);
                            string CreationDate = Convert.ToString(dt.Rows[i]["CreationDate"]);

                            table1.AddCell(new Paragraph(new Paragraph(Convert.ToString(Sno), newFont)));
                            table1.AddCell(new Paragraph(new Paragraph(CustomerName, newFont)));
                            table1.AddCell(new Paragraph(new Paragraph(Email, newFont)));
                            table1.AddCell(new Paragraph(new Paragraph(PhoneNo, newFont)));
                            table1.AddCell(new Paragraph(new Paragraph(Subject, newFont)));
                            table1.AddCell(new Paragraph(new Paragraph(Message, newFont)));
                            table1.AddCell(new Paragraph(new Paragraph(CreationDate, newFont)));
                        }
                        doc.Add(table1);
                    }
                    // Step 6: Closing the Document
                    doc.Close();
                }
                if (IsDownload)
                {
                    ProcessRequest(appRootDir + "/" + PDFName.ToString() + ".pdf");
                }
                return TotalLoggedHours;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessRequest(string fileRelativePath)
        {
            try
            {
                string contentType = "";
                //Get the physical path to the file.
                string FilePath = fileRelativePath;
                contentType = "application/pdf";
                //Set the appropriate ContentType.
                HttpContext.Current.Response.ContentType = contentType;
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(fileRelativePath)).Name);

                //Write the file directly to the HTTP content output stream.
                HttpContext.Current.Response.WriteFile(FilePath);
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                //To Do
                throw ex;
            }
        }
        protected string GetImageUrl(string imagepath)
        {
            string[] splits = Request.Url.AbsoluteUri.Split('/');
            if (splits.Length >= 2)
            {
                string url = splits[0] + "//";
                url += splits[1];
                url += "/";
                for (int i = 2; i < splits.Length - 1; i++)
                {
                    url += splits[i];
                    url += "/";
                }
                return url + imagepath;
            }
            return imagepath;
        }
        // TO Export the search result into Excel File End Here
    }

    class PDFWriterEventss : IPdfPageEvent
    {
        string watermarkTexts;
        float fontSizes = 80f;
        float xPositions = 300f;
        float yPositions = 800f;
        float angles = 45f;

        public PDFWriterEventss(string watermarkText, float fontSize = 80f, float xPosition = 300f, float yPosition = 400f, float angle = 45f)
        {
            this.watermarkTexts = watermarkText;
            this.xPositions = xPosition;
            this.yPositions = yPosition;
            this.angles = angle;
        }

        public void OnOpenDocument(PdfWriter writer, Document document) { }
        public void OnCloseDocument(PdfWriter writer, Document document) { }
        
        public void OnStartPage(PdfWriter writer, Document document)
        {
            try
            {
                PdfContentByte cb = writer.DirectContentUnder;
                BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
                cb.BeginText();
                cb.SetColorFill(BaseColor.LIGHT_GRAY);
                cb.SetFontAndSize(baseFont, fontSizes);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, watermarkTexts, xPositions, yPositions, angles);
                cb.EndText();
            }
            catch (DocumentException docEx)
            {
                throw docEx;
            }
        }
        public void OnEndPage(PdfWriter writer, Document document) { }
        public void OnParagraph(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title) { }
        public void OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title) { }
        public void OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnGenericTag(PdfWriter writer, Document document, iTextSharp.text.Rectangle rect, String text) { }
    }
}