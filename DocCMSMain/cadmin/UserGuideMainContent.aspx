﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="UserGuideMainContent.aspx.cs" Inherits="DocCMSMain.cadmin.UserGuideMainContent" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-fileupload.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wysiwyg.js"></script>
    <link href="css/colopicker/css/jquery.minicolors.css" rel="stylesheet" type="text/css" />
    <script src="css/colopicker/js/jquery.minicolors.min.js" type="text/javascript"></script>
    <link href="../js/Jcrop/Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../js/Jcrop/Jcrop.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ContentSpecialScripts.js"></script>
    <script src="../js/Pages/media_manager.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8) {
                return true;
            }
            if (key > 31 && (key < 48 || key > 57))
                return false;
            //             
            return true;
        }
    </script>
     <script type="text/javascript">
         //For the Text editor
         jQuery('#txtdescription').jqte();
    </script>
    <script language="javascript" type="text/javascript">
        function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
        }

        function AddQues() {
            var ImgAddQuestion = document.getElementById("ImgAddQuestion");
            ImgAddQuestion.click();
        }
        function AddAns() {
            var ImgAddAnswer = document.getElementById("ImgAddAnswer");
            ImgAddAnswer.click();
        }

        function RemoveFeature(elem) {
            var id = $(elem).attr("id");
            var GetId = id.toString().split('_');
            $("#divFeature_" + GetId[1]).style.visibility = "hidden";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">User Guide</a> <span class="separator"></span></li>
        <li>Manage User Guide</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
  
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                User Guide</h5>
            <h1>
                Manage User Guide</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner" style="padding:20px 20px 100px;">
            <div class="widget">
                <h4 class="widgettitle">
                    User Guide Details</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform">
                        <div class="par control-group">
                            <label class="control-label" for="txtmainheading">
                                Main Heading</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtMainheading" id="txtmainheading" class="input-large"
                                    clientidmode="Static" placeholder="Enter Main Heading Name" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtmainheading" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group" id="DivTextEditor" runat="server" clientidmode="Static">
                               <label class="control-label">
                                   Description
                              </label>
                              <div class="controls">
                               <textarea id="elm1" name="elm1" rows="15" class="tinymce" validationgroup="grpsubmit" 
                                   runat="server"   style="height: 140px; width: 100px; max-height: 200px; min-height: 200px; max-width: 200px;
                                    min-width: 100px;" onclick="return elm1_onclick()"></textarea> 
                             </div>
                           </div>
                        <div style="margin-top:-40px;margin-left:1010px; ">              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="grpsubmit" ClientIDMode="Static"
                                    ControlToValidate="elm1" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;" >*</asp:RequiredFieldValidator></div>
                         <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                                Display Order</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtDisplayOrder" id="txtDisplayOrder" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Order Number" disabled />
                                <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                    Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click" />
                            </div>
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    style="opacity: 1" checked />
                                <br />
                                <br />
                            </div>
                        </div>
                        <p class="stdformbutton">
                            <asp:Button ID="btnsubmit" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnsubmit_onclick" Text="Submit" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnupdate" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnupdate_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                OnClick="btnCancel_Click" />
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
       <!-- for Display Order Popup -->
      <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                Update Slider Details Display Orders
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 70%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                           Slider&nbsp;Info
                                        </th>
                                            <th id="ActiveStatusheading" runat="server">
                                        Status
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                            <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                onclick="ToggleDisplay();" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrDisplayOrder"  OnItemDataBound="RptrDisplayOrder_databound">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblmaincontentid" runat="server" Text='<%# Eval("MainContentID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>' Visible="false" ></asp:Label>
                                                    <asp:Label ID="lblText" runat="server" Text='<%# Eval("Heading") %>' ></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                    <asp:Label ID="Label2" runat="server" ClientIDMode="Static" Text='<%# Eval("MainContentID") %>'
                                                      Visible="false"></asp:Label>
                                                    <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                      CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("Dorder") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">                
                                                <td>
                                                    <asp:Label ID="lblmaincontentid" runat="server" Text='<%# Eval("MainContentID") %>' Visible="false"></asp:Label>
                                                     <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>' Visible="false" ></asp:Label>
                                                     <asp:Label ID="lblText" runat="server" Text='<%# Eval("Heading") %>' ></asp:Label>
                                                </td>
                                                 <td id="Activestatusdata" runat="server">
                                                     <asp:Label ID="Label2" runat="server" ClientIDMode="Static" Text='<%# Eval("MainContentID") %>'
                                                        Visible="false"></asp:Label>
                                                     <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                        CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("Dorder") %>' />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="Button1" class="btn btn-primary" onclick="ValidateDisplayOrders();"
                                value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button2" runat="server" ClientIDMode="Static" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="Button3" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Text="Update" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
        </div>
        <!--maincontentinner-->
        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
            role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
        </ul>
    </div>
    <!--maincontent-->
<script type="text/javascript">
    //For the Text editor
    jQuery('#txtdescription').jqte();
</script>
</asp:Content>
