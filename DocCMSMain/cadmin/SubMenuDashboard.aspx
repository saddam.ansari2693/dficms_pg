﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="SubMenuDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.SubMenuDashboard" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Content Management Dashboard</a> <span class="separator"></span>
        </li>
        <li>Manage Content </li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Content Management Dashboard</h5>
            <h1>
                Manage Content
            </h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                Content Pages Details
                <asp:Button ID="btnAddContentPageTop" runat="server" CssClass="btn btn-primary" Width="150" Text="Add Content Page"
                    Style="float: right; margin-top: -5px;" OnClick="btnAddContentPageBottom_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 20%;" />
                    <col class="con1" style="width: 35%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="align: center; width: 15%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Page Name
                        </th>
                        <th class="head1">
                            Content Title
                        </th>
                        <th class="head1">
                            Expiration Date
                        </th>
                        <th class="head1">
                            Creation Date
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrContentPage" OnItemCommand="RptrContentPage_ItemCommand"
                        OnItemDataBound="RptrContentPage_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("PageName")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lblContentTitle" CommandName="EditPageContent"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblExpirationDate" Text='<%# Eval("ExpirationDate")%>'
                                        CommandName="EditPageContent"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandName="EditPageContent" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandName="DeletePageContent" ToolTip="Delete Content Page" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("PageName")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lblContentTitle" CommandName="EditPageContent"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblExpirationDate" Text='<%# Eval("ExpirationDate")%>'
                                        CommandName="EditPageContent"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandName="EditPageContent" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandName="DeletePageContent" ToolTip="Delete Content Page" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <uc1:Footer ID="Footer1" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
