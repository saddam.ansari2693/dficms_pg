﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class UserAccounts : System.Web.UI.Page
    {
        public string EmailID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Request.QueryString["EmailID"] != null)
                    {
                        EmailID = Convert.ToString(Request.QueryString["EmailID"]);
                        Bind_Role();
                        InsertButton.Visible = false;
                        UpdateButton.Visible = true;
                        show_data(EmailID);
                        txtEmailId.Enabled = false;
                        divchangepassword.Visible = true;
                        divShowPass.Visible = true;
                    }
                    if (Request.QueryString["CloneEmailID"] != null)
                    {
                        EmailID = Convert.ToString(Request.QueryString["CloneEmailID"]);
                        Bind_Role();
                        Cls_UserProfiles objUser = ServicesFactory.DocCMSServices.Fetch_User_Details_Byid(EmailID);
                        if (objUser != null)
                        {
                            txtEmailId.Text = "";
                            txtFirstName.Value = "";
                            txtLastName.Value = "";
                            txtAddress.Value = "";
                            txtCity.Value = "";
                            txtState.Value = "";
                            txtZipcode.Value = "";
                            txtPhoneno.Value = "";
                            ChkActiveStatus.Checked = objUser.active;
                            ddlRole.Enabled = false;
                            UpdateButton.Visible = false;
                        }
                    }
                    else
                    {
                        Bind_Role();
                        InsertButton.Visible = true;
                        UpdateButton.Visible = false;
                        divShowPass.Visible = false;
                        txtEmailId.Text = "";
                        txtNewPswd.Attributes.Add("value", "");
                    }
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        protected void Cancel_click(Object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserDashboard.aspx");
        }

        protected void resetform()
        {
            txtFirstName.Value = null;
            txtLastName.Value = "";
            txtAddress.Value = "";
            txtCity.Value = "";
            txtState.Value = "";
            txtZipcode.Value = "";
            txtPhoneno.Value = "";
            txtEmailId.Text = "";
            txtNewPswd.Value = "";
            txtCnfPswd.Value = "";
        }

        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Cls_UserProfiles obj = new Cls_UserProfiles();
                    if (!string.IsNullOrEmpty(txtNewPswd.Value))
                    {
                        if (txtNewPswd.Value.Equals(txtCnfPswd.Value))
                        {
                            bool CheckForOldpswd = ServicesFactory.DocCMSServices.Compare_password_with_Old_password(txtEmailId.Text,txtNewPswd.Value);
                            if (CheckForOldpswd == true)
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Old password is same as new password');</script>");
                                return;
                            }
                            else
                            {
                                obj.password = txtNewPswd.Value;
                            }
                        }
                    }

                    divMsg.Visible = false;
                    obj.emailid = txtEmailId.Text;
                    obj.fname = txtFirstName.Value;
                    obj.lname = txtLastName.Value;
                    obj.roleid = ddlRole.SelectedValue;
                    obj.address = txtAddress.Value;
                    obj.state = txtState.Value;
                    obj.city = txtCity.Value;
                    obj.zipcode = txtZipcode.Value;
                    obj.phonenumber = txtPhoneno.Value;
                    obj.modifiedby = Convert.ToString(Session["UserID"]);
                    obj.modifieddate = Convert.ToString(DateTime.Now);
                    if (ChkActiveStatus.Checked)
                        obj.active = true;
                    else
                        obj.active = false;
                

                    if (ImageUploadControl.HasFile)
                    {
                        string fimagename = ImageUploadControl.PostedFile.FileName;
                        string FolderName = "UserProfile";
                        string serverpath = Server.MapPath("../UploadedFiles/" + FolderName + "/");
                        DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                        bool isDirCreated = directoryInfo.Exists;
                        if (!isDirCreated)
                        {
                            directoryInfo.Create();
                        }
                        obj.imagename = fimagename;
                        ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fimagename);
                    }
                    else
                    {
                        obj.imagename = lblFileUploader.Text;
                    }
                        
                    ServicesFactory.DocCMSServices.Update_User_Profile(obj);
                    Response.Redirect("~/cadmin/UserDashboard.aspx");
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        // show data function when User with Email Id is existing
        public void show_data(string EmailID)
        {
            try
            {

                Cls_UserProfiles objUser = ServicesFactory.DocCMSServices.Fetch_User_Details_Byid(EmailID);
                if (objUser != null)
                {
                    txtEmailId.Text= objUser.emailid;
                    txtFirstName.Value = objUser.fname;
                    txtLastName.Value = objUser.lname;
                    txtAddress.Value = objUser.address;
                    txtCity.Value = objUser.city;
                    txtState.Value = objUser.state;
                    txtZipcode.Value = objUser.zipcode;
                    txtPhoneno.Value = objUser.phonenumber;
                    ChkActiveStatus.Checked = objUser.active;
                    string serverpath="";
                    if (objUser.imagename != null && objUser.imagename!="")
                    {
                        serverpath = @"../UploadedFiles/UserProfile/" + objUser.imagename;
                        lblFileUploader.Text = objUser.imagename;
                    }
                    else
                    {
                         serverpath = @"../UploadedFiles/No_image_available.png";
                         lblFileUploader.Text = objUser.imagename;
                    }
                    imgLogo.Src = serverpath;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind Role inside ddlRole dropdown
        protected void Bind_Role()
        {
            try
            {
                hdnCurPageID.Value = Convert.ToString((Convert.ToInt32(Request.QueryString["CID"])));
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Roles(Convert.ToInt32(Session["RoleID"]));
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlRole.DataSource = dt;
                    ddlRole.DataBind();
                }

                if (Request.QueryString["RoleId"] != null)
                {
                    ddlRole.SelectedValue = Request.QueryString["RoleId"];

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    bool UserExist = false;
                    UserExist = ServicesFactory.DocCMSServices.User_Available(txtEmailId.Text);
                    if (UserExist == true)
                    {
                        divMsg.Visible = true;
                        LblText.Text = "User Already Exists";
                    }
                    else
                    {
                        Cls_UserProfiles obj = new Cls_UserProfiles();
                        divMsg.Visible = false;
                        obj.userid = Convert.ToString(Guid.NewGuid());
                        obj.emailid = txtEmailId.Text;
                        obj.password = txtNewPswd.Value;
                        obj.fname = txtFirstName.Value;
                        obj.lname = txtLastName.Value;
                        obj.roleid = ddlRole.SelectedValue;
                        obj.address = txtAddress.Value;
                        obj.state = txtState.Value;
                        obj.city = txtCity.Value;
                        obj.zipcode = txtZipcode.Value;
                        obj.phonenumber = txtPhoneno.Value;
                        obj.createdby = Convert.ToString(Session["UserID"]);
                        obj.creationdate = Convert.ToString(DateTime.Now);
                        if (ChkActiveStatus.Checked)
                            obj.active = true;
                        else
                            obj.active = false;


                        if (ImageUploadControl.HasFile)
                        {
                            string fimagename = ImageUploadControl.PostedFile.FileName;
                            string FolderName = "UserProfile";
                            string serverpath = Server.MapPath("../UploadedFiles/" + FolderName + "/");
                            DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                            bool isDirCreated = directoryInfo.Exists;
                            if (!isDirCreated)
                            {
                                directoryInfo.Create();
                            }
                            obj.imagename = fimagename;
                            ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fimagename);
                        }
                        else
                        {
                            obj.imagename = lblFileUploader.Text;
                        }
                        
                        ServicesFactory.DocCMSServices.Insert_User_Profile(obj);
                        Response.Redirect("~/cadmin/UserDashboard.aspx");
                    }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        protected void lnkShowPass_Click(object sender, EventArgs e)
        {
            if (divchangepassword.Visible == true)
            {
                divchangepassword.Visible = false;
            }
            else {
                divchangepassword.Visible = true;
            }
        }

    }
}