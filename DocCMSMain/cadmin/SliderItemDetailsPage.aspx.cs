﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class SliderItemDetailsPage1 : System.Web.UI.Page
    {
        dynamic UserId;
        protected static Int32 SMTPDetailID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnsubmitItemDetails.Visible = true;
                if (Request.QueryString["SliderItemContentID"] == null && Request.QueryString["SliderItemContentID"] != "" && Request.QueryString["SliderItemID"] != null && Request.QueryString["SliderItemID"] != "" && Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    hdnSliderId.Value = Convert.ToString(Request.QueryString["SliderID"]);
                    hdnSliderItemID.Value = Convert.ToString(Request.QueryString["SliderItemID"]);
                    btnsubmitItemDetails.Visible = true;
                    btnMasterItemupdateDetails.Visible = false;
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_SliderItemDetails_displayorder(Convert.ToInt16(Request.QueryString["SliderItemID"])) + 1;
                    txtDorder.Text = Convert.ToString(DisplayOrder);
                    Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                }
                else if (Request.QueryString["SliderItemContentID"] != null && Request.QueryString["SliderItemContentID"] != "" && Request.QueryString["SliderItemID"] != null && Request.QueryString["SliderItemID"] != "" && Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    Bind_Master_Details_Slider(Convert.ToInt16(Request.QueryString["SliderItemContentID"]));
                    hdnSliderItemID.Value = Convert.ToString(Request.QueryString["SliderItemID"]);
                    btnMasterItemupdateDetails.Visible = true;
                    btnsubmitItemDetails.Visible = false;
                    Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                }
                else
                {
                    btnsubmitItemDetails.Visible = false;
                    btnMasterItemupdateDetails.Visible = true;
                }
            }
        }

        protected void btnsubmitItemDetails_onclick(object sender, EventArgs e)
        {
            int RetValue = 0;
            string Filename = "";
            string FileExtention = "";
            string ImageName = "";
            Int32 SliderID = 0;
            string SliderItemId = "";
            if (FileUploadSlides.PostedFile != null && !string.IsNullOrEmpty(FileUploadSlides.PostedFile.FileName) && FileUploadSlides.HasFile)
            {
                Filename = FileUploadSlides.PostedFile.FileName;
                FileExtention = Path.GetExtension(Filename);
                string NewId = Guid.NewGuid().ToString();
                ImageName = NewId + FileExtention;
            }
            else
            {
                ImageName = "";
            }
            string FilePath = "~/UploadedFiles/SliderItemDetails/" + Convert.ToString(Request.QueryString["SliderID"]) + "/" + Convert.ToString(Request.QueryString["SliderItemID"]) + "/";
            if (!Directory.Exists(Server.MapPath(FilePath)))
            {
                Directory.CreateDirectory(Server.MapPath(FilePath));
            }
            if (ImageName != "")
            {
                FileUploadSlides.SaveAs(Server.MapPath(FilePath + "/" + ImageName));
            }
            ClsSliderDetails objClsSliderDetails = new ClsSliderDetails();
            objClsSliderDetails.slideritemid = Convert.ToInt32(Request.QueryString["SliderItemID"]);
            objClsSliderDetails.itemtype = Convert.ToString(ddlItemType.SelectedValue);
            objClsSliderDetails.image = ImageName;
            objClsSliderDetails.itemcontent = Convert.ToString(txtItemContent.Text != "" ? txtItemContent.Text : "");
            objClsSliderDetails.contentposition = Convert.ToString(ddlContentPosition.SelectedValue);
            objClsSliderDetails.fontsize = Convert.ToString(txtFontSize.Text != "" ? txtFontSize.Text : "");
            objClsSliderDetails.fontcolor = Convert.ToString(txtFontColor.Text != "" ? txtFontColor.Text : "");
            objClsSliderDetails.isbold = Convert.ToString(chkIsBold.Checked ? "True" : "False");
            objClsSliderDetails.isitalic = Convert.ToString(chkIsItalic.Checked ? "True" : "False");
            objClsSliderDetails.isunderline = Convert.ToString(chkIsUnderline.Checked ? "True" : "False");
            objClsSliderDetails.isemphasized = Convert.ToString(chkIsEmphasized.Checked ? "True" : "False");
            objClsSliderDetails.entrystyle = Convert.ToString(ddlEntryStyle.SelectedValue);
            objClsSliderDetails.entrydelay = Convert.ToString(txtDelay.Text != "" ? txtDelay.Text : "");
            objClsSliderDetails.margintop = Convert.ToString(txtMarginTop.Text != "" ? txtMarginTop.Text : "");
            objClsSliderDetails.marginbottom = Convert.ToString(txtMarginBottom.Text != "" ? txtMarginBottom.Text : "");
            objClsSliderDetails.marginright = Convert.ToString(txtMarginRight.Text != "" ? txtMarginRight.Text : "");
            objClsSliderDetails.marginleft = Convert.ToString(txtMarginLeft.Text != "" ? txtMarginLeft.Text : "");
            objClsSliderDetails.textbackgroundcolor = Convert.ToString(chkIsEmphasized.Checked ? txtTextBackgroundColor.Text : "");
            objClsSliderDetails.navigationurl = Convert.ToString(txtNavigationUrl.Value != "" ? txtNavigationUrl.Value : "");
            objClsSliderDetails.dataeffect = Convert.ToString(txtDataEffect.Value != "" ? txtDataEffect.Value : "");
            objClsSliderDetails.dorder = Convert.ToInt32(txtDorder.Text != "" ? txtDorder.Text : "");
            objClsSliderDetails.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
            RetValue = ServicesFactory.DocCMSServices.Insert_Slider_Details(objClsSliderDetails);
            if (RetValue > 0)
            {
                Response.Redirect("SliderItemDetailsDashboard.aspx?SliderItemID=" + Convert.ToString(Request.QueryString["SliderItemID"]) + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
            }
        }
        protected void btnMasterItemupdateDetails_onclick(object sender, EventArgs e)
        {
            int RetValue = 0;
            string Filename = "";
            string FileExtention = "";
            string ImageName = "";
            if (Convert.ToString(ddlItemType.SelectedValue) == "Image")
            {
                if (FileUploadSlides.PostedFile != null && !string.IsNullOrEmpty(FileUploadSlides.PostedFile.FileName) && FileUploadSlides.HasFile)
                {
                    Filename = FileUploadSlides.PostedFile.FileName;
                    FileExtention = Path.GetExtension(Filename);
                    string NewId = Guid.NewGuid().ToString();
                    ImageName = NewId + FileExtention;
                    string FilePath = "~/UploadedFiles/SliderItemDetails/" + Convert.ToString(Request.QueryString["SliderID"]) + "/" + Convert.ToString(Request.QueryString["SliderItemID"]) + "/";
                    if (!Directory.Exists(Server.MapPath(FilePath)))
                    {
                        Directory.CreateDirectory(Server.MapPath(FilePath));
                    }
                    if (ImageName != "")
                    {
                        FileUploadSlides.SaveAs(Server.MapPath(FilePath + "/" + ImageName));
                    }
                }
                else if (hdnImageNameForUpdate.Value != null && hdnImageNameForUpdate.Value != "")
                {
                    ImageName = hdnImageNameForUpdate.Value;
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Please Select Image First');</script>");
                    return;
                }
            }
            else
            {
            }
            ClsSliderDetails objClsSliderDetails = new ClsSliderDetails();
            objClsSliderDetails.slideritemcontentid = Convert.ToInt32(Request.QueryString["SliderItemContentID"]);
            objClsSliderDetails.slideritemid = Convert.ToInt32(Request.QueryString["SliderItemID"]);
            objClsSliderDetails.itemtype = Convert.ToString(ddlItemType.SelectedValue);
            objClsSliderDetails.image = ImageName;
            objClsSliderDetails.itemcontent = Convert.ToString(txtItemContent.Text != "" ? txtItemContent.Text : "");
            objClsSliderDetails.contentposition = Convert.ToString(ddlContentPosition.SelectedValue);
            objClsSliderDetails.fontsize = Convert.ToString(txtFontSize.Text != "" ? txtFontSize.Text : "");
            objClsSliderDetails.fontcolor = Convert.ToString(txtFontColor.Text != "" ? txtFontColor.Text : "");
            objClsSliderDetails.isbold = Convert.ToString(chkIsBold.Checked ? "True" : "False");
            objClsSliderDetails.isitalic = Convert.ToString(chkIsItalic.Checked ? "True" : "False");
            objClsSliderDetails.isunderline = Convert.ToString(chkIsUnderline.Checked ? "True" : "False");
            objClsSliderDetails.isemphasized = Convert.ToString(chkIsEmphasized.Checked ? "True" : "False");
            objClsSliderDetails.entrystyle = Convert.ToString(ddlEntryStyle.SelectedValue);
            objClsSliderDetails.entrydelay = Convert.ToString(txtDelay.Text != "" ? txtDelay.Text : "");
            objClsSliderDetails.margintop = Convert.ToString(txtMarginTop.Text != "" ? txtMarginTop.Text : "");
            objClsSliderDetails.marginbottom = Convert.ToString(txtMarginBottom.Text != "" ? txtMarginBottom.Text : "");
            objClsSliderDetails.marginright = Convert.ToString(txtMarginRight.Text != "" ? txtMarginRight.Text : "");
            objClsSliderDetails.marginleft = Convert.ToString(txtMarginLeft.Text != "" ? txtMarginLeft.Text : "");
            objClsSliderDetails.textbackgroundcolor = Convert.ToString(chkIsEmphasized.Checked ? txtTextBackgroundColor.Text : "");
            objClsSliderDetails.navigationurl = Convert.ToString(txtNavigationUrl.Value != "" ? txtNavigationUrl.Value : "");
            objClsSliderDetails.dataeffect = Convert.ToString(txtDataEffect.Value != "" ? txtDataEffect.Value : "");
            objClsSliderDetails.dorder = Convert.ToInt32(txtDorder.Text != "" ? txtDorder.Text : "");
            objClsSliderDetails.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
            RetValue = ServicesFactory.DocCMSServices.Update_Slider_Item_Details(objClsSliderDetails);
            if (RetValue > 0)
            {
                hdnImageNameForUpdate.Value = "";
                Response.Redirect("SliderItemDetailsDashboard.aspx?SliderItemID=" + Convert.ToString(Request.QueryString["SliderItemID"]) + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
            }
        }


        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<ClsSliderDetails> lstClsSliderDetails = new List<ClsSliderDetails>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblSliderItemContentID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSliderItemContentID");
                HiddenField hdnImageName = (HiddenField)RptrDisplayOrder.Items[ItemCount].FindControl("hdnImageName");
                if (Session["UserID"] != null && lblSliderItemContentID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + hdnImageName.Value + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                ClsSliderDetails objClsSliderDetails = new ClsSliderDetails();
                Label lblSliderItemContentID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSliderItemContentID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblSliderItemContentID != null)
                {
                    objClsSliderDetails.slideritemcontentid = Convert.ToInt32(lblSliderItemContentID.Text.Trim());
                    objClsSliderDetails.dorder = Convert.ToInt32(txtDorder.Value);
                    lstClsSliderDetails.Add(objClsSliderDetails);
                }
            }
            if (lstClsSliderDetails != null && lstClsSliderDetails.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_SliderItemMaster_dorder(lstClsSliderDetails);
                if (retVal > 0)
                {
                    if (Request.QueryString["SliderItemID"] != null && Request.QueryString["SliderItemContentID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["SliderItemID"] + ");</script>");
                        Bind_Master_Details_Slider(Convert.ToInt32(Request.QueryString["SliderItemContentID"]));
                        Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                        btnsubmitItemDetails.Visible = false;
                        btnMasterItemupdateDetails.Visible = true;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                        Int32 DisplayOrder1 = ServicesFactory.DocCMSServices.Get_Max_SliderItemDetails_displayorder(Convert.ToInt16(Request.QueryString["SliderItemID"])) + 1;
                        Bind_DisplayOrder(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                        txtDorder.Text = Convert.ToString(DisplayOrder1);
                        btnsubmitItemDetails.Visible = true;
                        btnMasterItemupdateDetails.Visible = false;
                    }
                }
            }
            //Update On 20160803
            Bind_Master_Details_Slider(Convert.ToInt32(Request.QueryString["SliderItemContentID"]));
        }


        protected void RptrDisplayOrder_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                HiddenField hdnItemType = (HiddenField)e.Item.FindControl("hdnItemType");
                Image Imgslider = (Image)e.Item.FindControl("Imgslider");
                Label lblText = (Label)e.Item.FindControl("lblText");
                if (hdnItemType.Value == "Text")
                {
                    lblText.Visible = true;
                    Imgslider.Visible = false;
                }
                else
                {
                    lblText.Visible = false;
                    Imgslider.Visible = true;
                }
            }
        }


        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "RMA", "<script>return FormatTable();</script>");
        }


        protected void Bind_DisplayOrder(int SliderItemID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Slider_Details_For_Display_Order(SliderItemID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SliderItemDetailsDashboard.aspx?SliderItemID=" + Convert.ToString(Request.QueryString["SliderItemID"]) + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
        }

        public void ClearControls()
        {
            txtItemContent.Text = "";
            txtFontSize.Text = "";
            txtFontColor.Text = "";
            chkIsBold.Checked = false;
            chkIsItalic.Checked = false;
            chkIsUnderline.Checked = false;
            chkIsEmphasized.Checked = false;
            txtDelay.Text = "";
            txtDorder.Text = "";
            ChkActiveStatus.Checked = false;
        }


        //Bind All Master Slider
        private void Bind_Master_Details_Slider(int SliderItemContentID)
        {
            DataTable DtGetMasterSliderDetails = ServicesFactory.DocCMSServices.Fetch_Slider_Details_Data_By_slideritemcontentid(SliderItemContentID);
            if (DtGetMasterSliderDetails != null && DtGetMasterSliderDetails.Rows.Count > 0)
            {
                ddlItemType.SelectedValue = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ItemType"]);
                if (Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ItemType"]) != "Text")
                {
                    ImgPreview.Src = "../UploadedFiles/SliderItemDetails/" + Convert.ToInt16(DtGetMasterSliderDetails.Rows[0]["SliderID"]) + "/" + Convert.ToInt16(DtGetMasterSliderDetails.Rows[0]["SliderItemID"]) + "/" + Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ImageName"]);
                }
                hdnImageNameForUpdate.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ImageName"]);
                txtItemContent.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ItemContent"]);
                ddlContentPosition.SelectedValue = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["ContentPosition"]);
                txtFontSize.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["FontSize"]);
                txtFontColor.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["FontColor"]);
                chkIsBold.Checked = Convert.ToBoolean(DtGetMasterSliderDetails.Rows[0]["IsBold"]);
                chkIsItalic.Checked = Convert.ToBoolean(DtGetMasterSliderDetails.Rows[0]["IsItalic"]);
                chkIsUnderline.Checked = Convert.ToBoolean(DtGetMasterSliderDetails.Rows[0]["IsUnderLine"]);
                chkIsEmphasized.Checked = Convert.ToBoolean(DtGetMasterSliderDetails.Rows[0]["IsEmphasized"]);
                ddlEntryStyle.SelectedValue = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["EntryStyle"]);
                txtDelay.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["EntryDelay"]);
                txtMarginTop.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["MarginTop"]);
                txtMarginBottom.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["MarginBottom"]);
                txtMarginLeft.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["MarginLeft"]);
                txtMarginRight.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["MarginRight"]);
                txtTextBackgroundColor.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["TextBackgroundColor"]);
                txtNavigationUrl.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["NavigationUrl"]);
                txtDataEffect.Value = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["DataEffect"]);
                txtDorder.Text = Convert.ToString(DtGetMasterSliderDetails.Rows[0]["Dorder"]);
                ChkActiveStatus.Checked = Convert.ToBoolean(DtGetMasterSliderDetails.Rows[0]["IsActive"]);
            }
        }
    }// Class Ends here
}