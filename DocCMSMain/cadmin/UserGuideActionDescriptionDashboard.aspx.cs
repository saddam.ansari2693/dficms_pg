﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideActionDescriptionDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["MDID"] != null || Request.QueryString["MID"] !=null)
                {
                    hdModuleDetailID.Value = Convert.ToString(Request.QueryString["MID"]);
                    Bind_UserGuideAction_Data(Convert.ToInt32(Request.QueryString["MDID"]));
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                }
            }
        }

        // Bind User Guid Action Description data
        protected void Bind_UserGuideAction_Data(Int32 ModelDetailId)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UG_ActionDesc_Byid(ModelDetailId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrUGActionDescription.DataSource = dt;
                    RptrUGActionDescription.DataBind();
                }
                else
                {
                    RptrUGActionDescription.DataSource = "";
                    RptrUGActionDescription.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrUGActionDescription_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "AddContentImage":
                        Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?SID=" + e.CommandArgument.ToString() + "&SN=UserGuideActionDescription" + "&MDID=" + Convert.ToString(Request.QueryString["MDID"]) + "&MID=" + Convert.ToString(Request.QueryString["MID"]));
                        break;
                    case "EditUserGuideAction":
                        Response.Redirect("UserGuideActionDescription.aspx?AID=" + e.CommandArgument.ToString() + "&MDID=" + Convert.ToString(Request.QueryString["MDID"]));

                        break;
                    case "DeleteUGAction":
                        Int32 retval = ServicesFactory.DocCMSServices.Delete_UserGuideActiondescription_By_id(Convert.ToInt32(e.CommandArgument)); 
                        Bind_UserGuideAction_Data(Convert.ToInt16(Request.QueryString["MDID"]));
                        break;

                    case "OrderUp":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserGuideAction(Convert.ToInt32(e.CommandArgument), Convert.ToInt16(Request.QueryString["MDID"]), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        Bind_UserGuideAction_Data(Convert.ToInt16(Request.QueryString["MDID"]));
                        break;
                    case "OrderDown":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserGuideAction(Convert.ToInt32(e.CommandArgument), Convert.ToInt16(Request.QueryString["MDID"]), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        Bind_UserGuideAction_Data(Convert.ToInt16(Request.QueryString["MDID"]));
                        break;
                    default:
                        break;
                }
            }
        }

        protected void RptrUGActionDescription_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

            }
        }

        protected void btnAddUserGuideActionDescription_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGuideActionDescription.aspx?MDID=" + Convert.ToInt32(Request.QueryString["MDID"]) + "&MID=" + Convert.ToInt32(Request.QueryString["MID"]));
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGuideModuloeDetailDashboard.aspx?MID=" + Convert.ToString(hdModuleDetailID.Value));
        }
    }
}