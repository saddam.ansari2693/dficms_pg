﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="ContentPages.aspx.cs" Inherits="DocCMSMain.cadmin.ContentPages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>    
    <link href="../css/Popup.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jscolor.js"></script>
     <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <script src="../js/Pages/ContentPages.js" type="text/javascript"></script>
    <script type="text/javascript" >
        $(function () {
            $("#datepicker").datepicker();
        });
        function CancelPage() {
            window.location.href = "ContentPageDashboard.aspx";
        }
        function validateKey(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if ((key >= 48 && key <= 57)) {
                return true;
            }
            else if ((key == 0 || key == 8 || key == 9 || key == 13 || key == 27 || key == 32 || key == 127)) {
                return true;
            }
            else {
                return false;
            }
        }

        function showBackgroundImage(fileInput) {
            var fileinputidval = fileInput.id;
            var thumbnilid = "";
            var Get_FileinputID = fileinputidval.toString().split('_');
            if (Get_FileinputID.length > 0) {
                thumbnilid = "imgBackground";
            }
            var files = fileInput.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    continue;
                }
                var img = document.getElementById(thumbnilid.toString());
                img.file = file;
                var reader = new FileReader();
                reader.onload = (function (aImg) {
                    return function (e) {
                        aImg.src = e.target.result;
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        } //==== Function Closed here
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input type="hidden" value="" id="hid1" />
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Add New Content Page</a> <span class="separator"></span></li>
        <li>Add New Content Page</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Add New Content Page</h5>
            <h1>
                Add New Content Page</h1>
        </div>
    </div>
    <div class="maincontent" style="border-color:transparent;">
        <div class="maincontentinner" style="border-color:transparent;">
    <div class="main-container" >
<div class="top-header">
 <div id="divLoader">
 <img src="../images/loading.gif" />
 </div>
    <div class="pull-left normal-fnt space5 align-right" style="width:150px;" >Page Name</div>
    <div class="pull-left span8"><input type="text" id="txtPageName" placeholder="Page Name" class="form-cont"  style="width:100%; margin-bottom:15px;border-radius: 3.01px;border:1px solid #ccc;"></div><span id="imgPageTitleReq" style="float:right;color:Red;font:bold; font-size: 24px; font-weight: bold; margin-right:-2px;margin-top:5px;display:none;">*</span>
    <br clear="all" />
    <div class="pull-left normal-fnt space5 align-right" style="width:150px;">Page Description</div>
    <div class="pull-left span8"><textarea id="txtPageDescription" placeholder="Page Description" class="txt-area"  rows="2" style="border-radius: 3.01px;border:1px solid #ccc; max-height:70px; max-width:877px; min-height:70px; min-width:877px;"></textarea></div>
    <div id="DivTime" runat="server" visible="false">
    <div class="pull-left normal-fnt space5 align-right"  style="width:150px; margin-top:15px;">Expiration Date</div>
    <div class="pull-left span3"><input type="text" id="datepicker" placeholder="Expiration Date" class="form-cont" style="width:55%; margin-top:15px;border-radius: 3.01px;border:1px solid #ccc;">
    <img id="image_datepicker" src="../images/icons/bg_icon_calendar.png" style="width: 5%; height: 5%; margin-top:20px;" />
    </div>    
     </div>
         <br clear="all" />
       <div class="pull-left normal-fnt space5 align-right" style="width:150px;">Allow Multiple entry</div>
     <div class="pull-left span8" style="margin:10px 0 10px 50px;">   
          <input type="checkbox" runat="server" name="activestatus" id="ChkMultiPage" class="input-large"  clientidmode="Static" style="opacity: 1" checked />
    </div> 
       <div class="pull-left normal-fnt space5 align-right" style="width:150px;">Is Active</div>
     <div class="pull-left span8" style="margin:10px 0px 50px 50px;">   
          <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"  clientidmode="Static" style="opacity: 1" checked />
    </div>
    </div>
   &nbsp;
        <div class="left-side" id="divLeftSide" style="width:350px;">
            <div>
                Standard Elements
            </div>
            <div class="btn-section" id="dvSource">
             <div class="btn-size" id="Divtitle" title="Title" style="display:none;">
                <img src="images/ContentImages/SE_Title.png">
                </div>
                <div class="btn-size" style="display:none;" id="Content" title="Content"><img src="images/ContentImages/SE_Content.png"></div>
                <div class="btn-size"  id="Dvupload-img"><img src="images/ContentImages/SE_upload-img.png"></div>
                <div class="btn-size" id="Dvsection-break"><img src="images/ContentImages/SE_section-break.png"></div>
                <div class="btn-size" id="Dvempty-space"><img src="images/ContentImages/SE_empty-space.png"></div>
                <div class="btn-size" id="Dvparagraph-text"><img src="images/ContentImages/SE_paragraph-text.png"></div>
                <div class="btn-size" id="Dvsingle-line"><img src="images/ContentImages/SE_single-line.png"></div>                 
                <div class="btn-size" id="DivRadioButton" title="Title"><img src="images/ContentImages/SE_Single-choice.png"></div>
                <div class="btn-size" id="Divcheckbox" title="Content"><img src="images/ContentImages/SE_multiple-choice.png"></div>
                <div class="btn-size" id="DivDropDown" title="Content"><img src="images/ContentImages/SE_dropdown.png"></div>
                <!-- New Controls added on 20151116 -->
                <div class="btn-size" id="DivMultiLine" title="Content"><img src="images/ContentImages/SE_MultiLine.png"></div>
                <div class="btn-size" id="DivListBox" title="Content"><img src="images/ContentImages/SE_ListBox.png"></div>
                <div class="btn-size" id="DivTextEditor" title="Content"><img src="images/ContentImages/SE_TextEditor.png"></div>
                <!-- New Controls added on 20151116 -->
                <div class="sapce-20">
                    &nbsp;</div>
                    </div>
              <div class="btn-section" id="dvSource1" style="visibility:hidden;">
              <div class="sapce-20">
                    <br />
                    <br />
                    Form Fields</div>
                <div class="btn-size" id="Dvfull-name"><img src="images/ContentImages/FF_full-name.png"></div>
                <div class="btn-size" id="Dvfirst-name"><img src="images/ContentImages/FF_first-name.png"></div>
                <div class="btn-size" id="Dvlast-name"><img src="images/ContentImages/FF_last-name.png"></div>
                <div class="btn-size" id="Dvemail-address"><img src="images/ContentImages/FF_email-address.png"></div>
                <div class="btn-size" id="Dvcompany"><img src="images/ContentImages/FF_company.png"></div>
                <div class="btn-size" id="Dvphone-number"><img src="images/ContentImages/FF_phone-number.png"></div>
                <div class="btn-size" id="Dvadress-1"><img src="images/ContentImages/FF_address-1.png"></div>
                <div class="btn-size" id="Dvaddress-2"><img src="images/ContentImages/FF_address-2.png"></div>
                <div class="btn-size" id="Dvcity"><img src="images/ContentImages/FF_city.png"></div>
                <div class="btn-size" id="Dvstate"><img src="images/ContentImages/FF_state.png"></div>
                <div class="btn-size" id="Dvzip"><img src="images/ContentImages/FF_zip.png"></div>
                <div class="btn-size" id="Dvcustom-1"><img src="images/ContentImages/FF_custom-1.png"></div>
                <div class="btn-size" id="Dvcustom-2"><img src="images/ContentImages/FF_custom-2.png"></div>
                <div class="btn-size" id="Dvcustom-3"><img src="images/ContentImages/FF_custom-3.png"></div>
            </div>
        </div>
       <%--  Main Area where User can Drop the Elementes position: fixed --%>
        <div class="right-side" id="divMain" style="height:600px;overflow:auto;" >   
        </div>
      <%--  Main Area where User can Drop the Elementes Closed Here--%>
      <%--  Div for buttons to Save and Cancel the Survey popup--%>
      <div id="divActual" class="right-side" style="float: right;">           
           <asp:Button id="btnSubmitMain" runat="server" onclick="lnkSubmit_Click" ClientIDMode="Static" style="display:none;"/>
              <img src="../images/Submit1.png" id="btnSubmit" onclick="return Save_Content_Page();" alt="Submit Button" tooltip="Click to save the content"/>
                <img src="../images/Cancel1.png" id="btnCancel" onclick="return CancelPage();" alt="Cancel Button" tooltip="Click to get back to the Dashboard"/>
                 <img src="../images/plus-icon.png" id="btnAddControl" onclick="adddiv();" alt="Add Div" tooltip="Click to Add an empty container."/>
        </div>
        <div id="divDummy" class="right-side" style="float: right;display:none;">                                       
               <span id="spnIndicator" style="color:black;float: left;font-size: 14px;font-weight: normal !important;text-decoration: none;display:none;">Please wait... while application is processing the data.</span>
               <br />
              <img src="../images/Submit1.png" alt="" id="Img1" />
               <img src="../images/Cancel1.png" alt="" id="Img2" />
                <img src="../images/plus-icon.png" alt="" id="Img3"/>               
        </div>
    </div>    
    <asp:HiddenField ID="hdnCurUser" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCurPageID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnModPageID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMode" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCaseName" runat="server" ClientIDMode="Static" />
    </div>
    </div>  
</asp:Content>

