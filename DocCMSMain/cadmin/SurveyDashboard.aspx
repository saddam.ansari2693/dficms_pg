﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="SurveyDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.SurveyDashboard" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
     <script type="text/javascript">
         jQuery(document).ready(function () {
             var showValue = '<%= Session["showtablesize"] %>';
             if (showValue == null || showValue == "") {
                 showValue = 10;
             }
             jQuery('#dyntable').dataTable({
                 "sPaginationType": "full_numbers",
                 "iDisplayLength": parseInt(showValue, 10),
                 "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                 "fnDrawCallback": function (oSettings) {
                     jQuery.uniform.update();
                 }
             });

             jQuery('#dyntable2').dataTable({
                 "bScrollInfinite": true,
                 "bScrollCollapse": true,
                 "sScrollY": "300px"
             });
         });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
      }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Survey</a> <span class="separator"></span></li>
        <li>Manage Survey</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Survey
            </h5>
            <h1>
                Manage Survey
           </h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
               Survey Details
                <asp:Button ID="btnAddSurveyTop" runat="server" CssClass="btn btn-primary" Width="150" Text="Add New Survey"
                    Style="float: right; margin-top: -5px;" OnClick="btnAddSurveyBottom_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 15%;" />
                       <col class="con1" style="width: 40%;" />
                    <col class="con1" style="width: 10%;" />                    
                    <col class="con1" style="align: center; width: 20%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Display Order
                        </th>
                        <th class="head1">
                            Survey Name
                        </th>
                          <th class="head1">
                            Survey Description
                        </th>
                        <th class="head1">
                            Set as Default
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrSurvey" OnItemCommand="RptrSurvey_ItemCommand"
                        OnItemDataBound="RptrSurvey_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                    <td>
                                    <asp:Label ID="lblSurveyID" runat="server" ClientIDMode="Static" Text='<%# Eval("SurveyID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                    <div style="display: inline; margin-left: 20px;">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("SurveyID")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("SurveyID")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkSurveyName" CommandArgument='<%# Eval("SurveyID")%>' CommandName="EditSurvey"
                                        runat="server" Text='<%# Eval("SurveyName")%>' />
                                        <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("SurveyName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Description")%>' ID="lblDescription"></asp:Label>
                                </td> 
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("DefaultActive")%>' ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                      <asp:LinkButton ID="lnkEdit" CommandArgument='<%# Eval("SurveyID")%>' CommandName="EditSurvey" runat="server" style="cursor:pointer;">Edit</asp:LinkButton>
                                      |   <asp:LinkButton ID="lnkReport"  CommandArgument='<%# Eval("SurveyID")%>' CommandName="ViewReport" runat="server" style="cursor:pointer;">Reports</asp:LinkButton>
                                      |   <asp:LinkButton ID="lnkWelcome"   CommandArgument='<%# Eval("SurveyID")%>' CommandName="Welcome" runat="server" style=" cursor:pointer;">Welcome</asp:LinkButton>
                                      |   <asp:LinkButton ID="lnkDelete"  CommandArgument='<%# Eval("SurveyID")%>' CommandName="DeleteSurvey" runat="server" style="cursor:pointer;">Delete</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                               <td>
                                    <asp:Label ID="lblSurveyID" runat="server" ClientIDMode="Static" Text='<%# Eval("SurveyID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                    <div style="display: inline; margin-left: 20px;">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("SurveyID")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("SurveyID")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkSurveyName" CommandArgument='<%# Eval("SurveyID")%>' CommandName="EditSurvey"
                                        runat="server" Text='<%# Eval("SurveyName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("SurveyName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Description")%>' ID="lblDescription"></asp:Label>
                                </td>  
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("DefaultActive")%>' ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                   <asp:LinkButton ID="lnkEdit" CommandArgument='<%# Eval("SurveyID")%>' CommandName="EditSurvey" runat="server" style="cursor:pointer;">Edit</asp:LinkButton>
                                      |   <asp:LinkButton ID="lnkReport"  CommandArgument='<%# Eval("SurveyID")%>' CommandName="ViewReport" runat="server" style="cursor:pointer;">Reports</asp:LinkButton>
                                      |   <asp:LinkButton ID="lnkWelcome"   CommandArgument='<%# Eval("SurveyID")%>' CommandName="Welcome" runat="server" style=" cursor:pointer;">Welcome</asp:LinkButton>
                                      |   <asp:LinkButton ID="lnkDelete"  CommandArgument='<%# Eval("SurveyID")%>' CommandName="DeleteSurvey" runat="server" style="cursor:pointer;">Delete</asp:LinkButton>
                                             
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <asp:Button ID="btnAddSurveyBottom" runat="server" CssClass="btn btn-primary" Width="150"
                Text="Add New Survey" OnClick="btnAddSurveyBottom_Click" />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
