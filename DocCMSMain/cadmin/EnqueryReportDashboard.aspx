﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="EnqueryReportDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.EnqueryReportDashboard" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
     <script type="text/javascript">
         jQuery(document).ready(function () {
             var showValue = '<%= Session["showtablesize"] %>';
             if (showValue == null || showValue == "") {
                 showValue = 10;
             }
             jQuery('#dyntable').dataTable({
                 "sPaginationType": "full_numbers",
                 "iDisplayLength": parseInt(showValue, 10),
                 "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                 "fnDrawCallback": function (oSettings) {
                     jQuery.uniform.update();
                 }
             });

             jQuery('#dyntable2').dataTable({
                 "bScrollInfinite": true,
                 "bScrollCollapse": true,
                 "sScrollY": "300px"
             });
         });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Enquiry Report</a> <span class="separator"></span></li>
        <li>Manage Enquiry Report</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Enquiry Report</h5>
            <h1>
                Manage Enquiry Report</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
               Enquiry Report Details
                <label id="Label2" runat="server" style="font-size: 13px; width: 150px; margin-left:1084px; margin-top:-19px">
                            <strong>Select Export Format:</strong>
                        </label> 
                    <asp:DropDownList ID="ddlExport" runat="server" ClientIDMode="Static" 
                    style="font-size: 13px; width: 170px; margin-left:1214px; margin-top:-25px;">                        
                          <asp:ListItem Value="0" Text="Excel"> </asp:ListItem>
                          <asp:ListItem Value="1" Text="CSV(Comma Separated)"> </asp:ListItem>
                          <asp:ListItem Value="2" Text="CSV(Space Separated)"> </asp:ListItem>
                          <asp:ListItem Value="3" Text="PDF"> </asp:ListItem>
                    </asp:DropDownList>
                <asp:Button ID="btnExportTop" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Export"
                    Style="float: right; margin-top: -25px;" onclick="btnExportTop_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 25%;" />
                    <col class="con1" style="width: 35%;" />  
                    <col class="con1" style="align: center; width: 10%;" />   
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                          Name
                        </th>
                        <th class="head1">
                           Email Id
                        </th>
                        <th class="head1">
                            Subject
                        </th>
                        <th class="head1">
                           Message
                        </th>
                          <th class="head1">
                            Enquiry Date
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrEnquiry" OnItemCommand="RptrEnquiry_ItemCommand"
                        OnItemDataBound="RptrEnquiry_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:Label ID="lblID" runat="server" ClientIDMode="Static" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("Name")%>' ID="lblName"></asp:Label>
                                </td>
                                 <td>
                                   <asp:Label runat="server" Text='<%# Eval("EmailId")%>' ID="lblEmailId"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Subject")%>' ID="lblSubject"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("Message")%>' ID="lblMessage"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                       
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                 <td>
                                    <asp:Label ID="lblID" runat="server" ClientIDMode="Static" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("Name")%>' ID="lblName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("EmailId")%>' ID="lblEmailId"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Subject")%>' ID="lblSubject"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("Message")%>' ID="lblMessage"></asp:Label>
                                </td>
                                  <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>