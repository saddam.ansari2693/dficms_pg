﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;
using DocCMS.Core.DataTypes;
using System.IO;
using System.Configuration;
using System.Data.OleDb;

namespace DocCMSMain.cadmin
{
    public partial class CatalogueDashboard : System.Web.UI.Page
    {

        Int32 pageID = 0;
        protected static string PageName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["PID"] != null)
            {
                if (!IsPostBack)
                {
                    Bind_Dashboard_Content(66);
                    Set_Controls();
                }
            }
        }

        public void Set_Controls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubmoduleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddContentPageTop.Visible = true;
                }
                else
                {
                    btnAddContentPageTop.Visible = false;
                }
                foreach (RepeaterItem item in RptrContentPage.Items)
                {
                    LinkButton lblContentTitle = (LinkButton)item.FindControl("lblContentTitle");
                    Label lblTitle = (Label)item.FindControl("lblTitle");
                    ImageButton lnkEdit = (ImageButton)item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)item.FindControl("lnkDelete");
                    HtmlGenericControl chkbox = (HtmlGenericControl)item.FindControl("chkbox");
                    HtmlTableCell trCheckbox = (HtmlTableCell)item.FindControl("trCheckbox");
                    if (lblContentTitle != null && lblTitle != null && lnkEdit != null && lnkDelete != null && chkbox != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lblContentTitle.Visible = true;
                            lblTitle.Visible = false;
                            chkall.Visible = true;
                            chkbox.Visible = true;
                            FileUploadControl.Visible = true;
                            btnUpload.Visible = true;
                            btnDelete.Visible = true;
                            th1.Visible = true;
                            trCheckbox.Visible = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lblContentTitle.Visible = false;
                            lblTitle.Visible = true;
                            chkall.Visible = false;
                            chkbox.Visible = false;
                            FileUploadControl.Visible = false;
                            btnUpload.Visible = false;
                            btnDelete.Visible = false;
                            th1.Visible = false;
                            trCheckbox.Visible = false;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;

                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }

        // Bind  Content Management Dashboad Details inside RptrContentPage Repeater Control
        protected void Bind_Dashboard_Content(Int32 PageID)
        {
            PageName = ServicesFactory.DocCMSServices.Fetch_pagename_By_pageid(pageID);
            if (!string.IsNullOrEmpty(PageName))
            {
                headingPage.InnerText = "Manage " + PageName;
                ListPageName.InnerText = "Manage " + PageName;
                btnAddContentPageTop.Text = "Add new " + PageName.ToLower();
            }
            //Checking  if the page has Display order or not if not then it will not show display order column
            DataTable dtContent = ServicesFactory.DocCMSServices.Fetch_Catalouge_Dashboard(PageID);
            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                RptrContentPage.DataSource = dtContent;
                RptrContentPage.DataBind();
                ChkSelectAll.Visible = true;
                btnDelete.Visible = true;
            }
            else
            {
                ChkSelectAll.Visible = false;
                btnDelete.Visible = false;
            } // ends of Check
        }

        protected void RptrContentPage_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lbldorder = (Label)e.Item.FindControl("lbldorder");
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                switch (e.CommandName)
                {
                    case "EditPageContent":
                        Response.Redirect("~/cadmin/ContentManagement.aspx?" + e.CommandArgument);
                        break;
                    case "DeletePageContent":

                        Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                        LinkButton lblContentTitle = (LinkButton)e.Item.FindControl("lblContentTitle");
                        Label lnkPageName = (Label)e.Item.FindControl("lnkPageName");
                        if (lblCMSID != null && lblContentTitle != null && lblPageID != null && lnkPageName != null)
                        {
                            Int32 DelVal = ServicesFactory.DocCMSServices.Delete_CMS_Data(Convert.ToInt32(lblCMSID.Text));
                            if (DelVal > 0)
                            {
                                Bind_Dashboard_Content(Convert.ToInt32(lblPageID.Text));
                                Response.Redirect("~/cadmin/CatalogueDashboard.aspx?PID=" + lblPageID.Text + "");
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        protected void RptrContentPage_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                LinkButton lblContentTitle = (LinkButton)e.Item.FindControl("lblContentTitle");
                Label lnkPageName = (Label)e.Item.FindControl("lnkPageName");
                ImageButton lnkEdit = (ImageButton)e.Item.FindControl("lnkEdit");
                ImageButton lnkDelete = (ImageButton)e.Item.FindControl("lnkDelete");
                string[] TempPageID = Convert.ToString(Request.QueryString["PID"]).Split('_');
                Label lbldorder = (Label)e.Item.FindControl("lbldorder");
                HtmlTableCell trDisplay = (HtmlTableCell)e.Item.FindControl("trDisplay");
                Image ImgCatalog = (Image)e.Item.FindControl("ImgCatalog");
                Label lblCatalogImageName = (Label)e.Item.FindControl("lblCatalogImageName");
                Label lblTitle=(Label)e.Item.FindControl("lblTitle");
                string ImagePath = "../UploadedFiles/ContentImages/Catalogue/";
                if (lblCatalogImageName != null && ImgCatalog != null)
                {
                    if (!string.IsNullOrEmpty(lblCatalogImageName.Text))
                        ImgCatalog.ImageUrl = ImagePath + lblCatalogImageName.Text;
                    else
                        ImgCatalog.ImageUrl = "~/UploadedFiles/No_image_available.png";
                }
                if (lblCMSID != null && lblPageID != null && lblContentTitle != null && lnkPageName != null && lnkEdit != null && lnkDelete != null && !string.IsNullOrEmpty(lbldorder.Text))
                {
                    lnkEdit.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    lnkDelete.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                    lblContentTitle.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    thDisplay.Visible = true;
                    trDisplay.Visible = true;
                }
                else
                {
                    lnkEdit.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    lnkDelete.CommandArgument = "PID=" + lnkPageName.Text + "&CMSID=" + lblCMSID.Text;
                    lblContentTitle.CommandArgument = "PID=" + lblPageID.Text + "&CMSID=" + lblCMSID.Text;
                    thDisplay.Visible = false;
                    trDisplay.Visible = false;
                    ColDisplay.Style.Add("width", "40%");
                 }
            }
        }

        protected void btnAddContentPageBottom_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["PID"] != null)
            {
                Response.Redirect("~/cadmin/ContentManagement.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]));
            }
        }
        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            Int32 Count = 0;
            foreach (RepeaterItem ri in RptrContentPage.Items)
            {
                CheckBox ChkActive = (CheckBox)ri.FindControl("ChkActive");
                if (ChkSelectAll.Checked)
                {
                    ChkActive.Checked = true;
                    Count++;
                }
                else
                {
                    ChkActive.Checked = false;
                }
            }
            if (Count > 0)
                btnDelete.Enabled = true;
            else
                btnDelete.Enabled = false;
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Int32 Count = 0;
            Int32 PageID = 0;
            foreach (RepeaterItem ri in RptrContentPage.Items)
            {
                CheckBox ChkActive = (CheckBox)ri.FindControl("ChkActive");
                Label lblCMSID = (Label)ri.FindControl("lblCMSID");
                Label lblPageID = (Label)ri.FindControl("lblPageID");
                LinkButton lblContentTitle = (LinkButton)ri.FindControl("lblContentTitle");
                Label lnkPageName = (Label)ri.FindControl("lnkPageName");
                if (lblCMSID != null && lblContentTitle != null && lblPageID != null && lnkPageName != null && ChkActive != null)
                {
                    if (ChkActive.Checked)
                    {
                        PageID = Convert.ToInt32(lblPageID.Text);
                        Int32 DelVal = ServicesFactory.DocCMSServices.Delete_CMS_Data(Convert.ToInt32(lblCMSID.Text));
                        if (DelVal > 0)
                            Count = 1;
                        else
                            return;
                    }
                }
            }
            if (Count > 0)
            {
                Bind_Dashboard_Content(PageID);
                Response.Redirect("~/cadmin/CatalogueDashboard.aspx?PID=" + PageID + "");
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "NMAIIF", "<script>alert('Operation Failed');</script>");

        }
        private void ReadCSV(Int32 CategoryPageID, string FilePath)
        {
            List<string> Head = null;
            List<List<string>> Rows = null;
            List<Cls_ContentManagement> lstContent = new List<Cls_ContentManagement>();
            Cls_ContentManagement clscontent = new Cls_ContentManagement();
            string CSVFile = Server.MapPath(FilePath);
            bool HeadRow = true;
            int RowCount = 0;
            Head = new List<string>();
            Rows = new List<List<string>>();
            StreamReader reader = null;
            string RetVal = "";
            try
            {
                reader = new StreamReader(File.OpenRead(CSVFile));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (HeadRow)
                    {
                        Head.AddRange(line.Split(','));
                        if (string.IsNullOrEmpty(Head[Head.Count - 1]))
                            Head.RemoveAt(Head.Count - 1);
                        HeadRow = false;
                    }
                    else
                    {
                        Rows.Add(new List<string>());
                        Rows[RowCount].AddRange(line.Split(','));
                        RowCount++;
                    }
                }
                for (Int32 iRowCount = 0; iRowCount < RowCount; iRowCount++)
                {
                    //Insert the part category
                    lstContent = new List<Cls_ContentManagement>();
                    clscontent.pageid = CategoryPageID;
                    clscontent.pagename = "Product Category";
                    if (Session["UserID"] != null)
                    {
                        clscontent.createdby = Guid.Parse(Convert.ToString(Session["UserID"]));
                        clscontent.lastmodifiedby = Guid.Parse(Convert.ToString(Session["UserID"]));
                    }
                    lstContent.Add(clscontent);
                    bool CheckExistPartCategory = true;
                    for (Int32 jCategoryRow = 0; jCategoryRow < 4; jCategoryRow++)
                    {
                        clscontent = new Cls_ContentManagement();
                        if (CheckExistPartCategory)
                        {
                            Int32 Count = jCategoryRow + 1;
                            switch (jCategoryRow)
                            {
                                case 0:
                                    bool CheckExist = ServicesFactory.DocCMSServices.Check_Existing_Menu_For_Save(Convert.ToString(Rows[iRowCount][1]), CategoryPageID);
                                    if (CheckExist)
                                    {
                                        CheckExistPartCategory = false;
                                    }
                                    else
                                    {
                                        clscontent.dorder = Count;
                                        clscontent.fieldtext = Convert.ToString(Head[1]);
                                        clscontent.fieldtype = "Single Line Text";
                                        clscontent.fieldvalue = Convert.ToString(Rows[iRowCount][1]);
                                    }
                                    break;
                                case 1:

                                    clscontent.dorder = Count;
                                    clscontent.fieldtype = "Multi Line Text";
                                    clscontent.fieldtext = Convert.ToString(Head[2]);
                                    clscontent.fieldvalue = Convert.ToString(Rows[iRowCount][1]);
                                    break;
                                case 2:
                                    clscontent.dorder = Count;
                                    clscontent.fieldtype = "Single Line Text";
                                    clscontent.fieldvalue = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_Content_dorder(CategoryPageID) + 1);
                                    clscontent.fieldtext = "Display Order";
                                    break;
                                case 3:
                                    clscontent.dorder = Count;
                                    clscontent.fieldtype = "Checkbox";
                                    clscontent.fieldtext = "Is Active";
                                    clscontent.fieldvalue = "TRUE";
                                    break;
                            }

                        }
                        if (CheckExistPartCategory)
                        {
                            lstContent.Add(clscontent);
                        }
                    }
                    if (lstContent.Count == 5)
                    {
                        RetVal = ServicesFactory.DocCMSServices.Insert_CMS_Data(lstContent, Convert.ToString(Session["UserID"]));
                        if (RetVal == "")
                        {
                        }
                    }

                    if (RetVal.Equals(""))
                    {
                        // Now fill the catalouge value with respect to part category
                        lstContent = new List<Cls_ContentManagement>();
                        clscontent = new Cls_ContentManagement();
                        clscontent.pageid = pageID;
                        clscontent.pagename = "Catalogue";
                        if (Session["UserID"] != null)
                        {
                            clscontent.createdby = Guid.Parse(Convert.ToString(Session["UserID"]));
                            clscontent.lastmodifiedby = Guid.Parse(Convert.ToString(Session["UserID"]));
                        }
                        lstContent.Add(clscontent);
                        bool CkhExisting = true;
                        for (Int32 i = 0; i < 8; i++)
                        {
                            clscontent = new Cls_ContentManagement();
                            if (CkhExisting)
                            {
                                Int32 CountRow = i + 1;
                                switch (i)
                                {
                                    case 0:
                                        bool CheckExisting = ServicesFactory.DocCMSServices.Check_Existing_Menu_For_Save(Convert.ToString(Rows[iRowCount][0]).ToUpper(), pageID);
                                        if (CheckExisting)
                                        {
                                            CkhExisting = false;
                                        }
                                        else
                                        {
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Single Line Text";
                                            clscontent.fieldtext = Convert.ToString(Head[0]);
                                            clscontent.fieldvalue = Convert.ToString(Rows[iRowCount][0]);
                                         }

                                        break;
                                    case 1:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Multi Line Text";
                                        clscontent.fieldtext = Convert.ToString(Head[2]);
                                        clscontent.fieldvalue = Convert.ToString(Rows[iRowCount][2]);
                                        break;
                                    case 2:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Dropdown";
                                        clscontent.fieldvalue = Convert.ToString(Rows[iRowCount][1]);
                                        clscontent.fieldtext = Convert.ToString(Rows[iRowCount][1]);
                                        break;
                                    case 3:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Upload Image";
                                        clscontent.fieldtext = "Upload Image";
                                        clscontent.fieldvalue = "";

                                        break;
                                    case 4:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Single Line Text";
                                        clscontent.fieldtext = "Price";
                                        clscontent.fieldvalue = Convert.ToString(Rows[iRowCount][3]);

                                        break;
                                    case 5:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Checkbox";
                                        clscontent.fieldtext = "Show Price";
                                        clscontent.fieldvalue = "TRUE";

                                        break;
                                    case 6:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Single Line Text";
                                        Int32 Count = iRowCount + 1;
                                        clscontent.fieldvalue = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_Content_dorder_ByMenuname(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToString(Rows[iRowCount][1])) + 1);
                                        clscontent.fieldtext = "Display Order";

                                        break;
                                    case 7:
                                        clscontent.dorder = CountRow;
                                        clscontent.fieldtype = "Checkbox";
                                        clscontent.fieldtext = "Is Active";
                                        clscontent.fieldvalue = "TRUE";
                                        break;
                                 }
                            }
                            if (CkhExisting)
                            {
                                lstContent.Add(clscontent);

                            }
                        }
                        if (lstContent.Count == 9)
                        {
                            RetVal = ServicesFactory.DocCMSServices.Insert_CMS_Data(lstContent, Convert.ToString(Session["UserID"]));
                            if (RetVal == "")
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Data inserted successfully');</script>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }

        private void ReadExcel(Int32 CategoryPageID, string FilePath, string ext)
        {
            string conString = string.Empty;
            string RetVal = "";
            List<Cls_ContentManagement> lstContent = new List<Cls_ContentManagement>();
            Cls_ContentManagement clscontent = new Cls_ContentManagement();
            switch (ext)
            {
                case ".xls": //Excel 97-03
                    conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx": //Excel 07 or higher
                    conString = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    break;
            }
            conString = string.Format(conString, Server.MapPath(FilePath));
            using (OleDbConnection excel_con = new OleDbConnection(conString))
            {
                excel_con.Open();
                string sheet1 = excel_con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                DataSet dsExcelData = new DataSet();
                DataTable dt = null;
                ////[OPTIONAL]: It is recommended as otherwise the data will be considered as String by default.
                using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_con))
                {
                    oda.Fill(dsExcelData);
                }
                excel_con.Close();
                if (dsExcelData != null && dsExcelData.Tables.Count > 0)
                {
                    dt = dsExcelData.Tables[0];
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (Int32 iRowCount = 0; iRowCount < dt.Rows.Count; iRowCount++)
                    {
                        //Insert the part category
                        lstContent = new List<Cls_ContentManagement>();
                        clscontent.pageid = CategoryPageID;
                        clscontent.pagename = "Product Category";
                        if (Session["UserID"] != null)
                        {
                            clscontent.createdby = Guid.Parse(Convert.ToString(Session["UserID"]));
                            clscontent.lastmodifiedby = Guid.Parse(Convert.ToString(Session["UserID"]));
                        }
                        lstContent.Add(clscontent);
                        bool CheckExistPartCategory = true;
                        for (Int32 jCategoryRow = 0; jCategoryRow < 4; jCategoryRow++)
                        {
                            clscontent = new Cls_ContentManagement();
                            if (CheckExistPartCategory)
                            {
                                Int32 Count = jCategoryRow + 1;
                                switch (jCategoryRow)
                                {

                                    case 0:
                                        bool CheckExist = ServicesFactory.DocCMSServices.Check_Existing_Menu_For_Save(Convert.ToString(dt.Rows[iRowCount]["Product Category Name"]), CategoryPageID);
                                        if (CheckExist)
                                        {
                                             CheckExistPartCategory = false;
                                        }
                                        else
                                        {
                                            clscontent.dorder = Count;
                                            clscontent.fieldtext = "Product Category Name ";
                                            clscontent.fieldtype = "Single Line Text";
                                            clscontent.fieldvalue = Convert.ToString(dt.Rows[iRowCount]["Product Category Name"]);
                                         }
                                        break;
                                    case 1:

                                        clscontent.dorder = Count;
                                        clscontent.fieldtype = "Multi Line Text";
                                        clscontent.fieldtext = "Description";
                                        clscontent.fieldvalue = Convert.ToString(dt.Rows[iRowCount]["Product Category Name"]);
                                        break;
                                    case 2:
                                        clscontent.dorder = Count;
                                        clscontent.fieldtype = "Single Line Text";
                                        clscontent.fieldvalue = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_Content_dorder(CategoryPageID) + 1);
                                        clscontent.fieldtext = "Display Order";
                                        break;
                                    case 3:
                                        clscontent.dorder = Count;
                                        clscontent.fieldtype = "Checkbox";
                                        clscontent.fieldtext = "Is Active";
                                        clscontent.fieldvalue = "TRUE";
                                        break;
                                }
                            }
                            if (CheckExistPartCategory)
                            {
                                lstContent.Add(clscontent);
                            }
                        }
                        if (lstContent.Count == 5)
                        {
                            RetVal = ServicesFactory.DocCMSServices.Insert_CMS_Data(lstContent, Convert.ToString(Session["UserID"]));
                        }
                        if (RetVal == "")
                        {
                            // Now fill the catalouge value with respect to part category
                            lstContent = new List<Cls_ContentManagement>();
                            clscontent = new Cls_ContentManagement();
                            clscontent.pageid = pageID;
                            clscontent.pagename = "Catalogue";
                            if (Session["UserID"] != null)
                            {
                                clscontent.createdby = Guid.Parse(Convert.ToString(Session["UserID"]));
                                clscontent.lastmodifiedby = Guid.Parse(Convert.ToString(Session["UserID"]));
                            }
                            lstContent.Add(clscontent);
                            bool CkhExisting = true;
                            for (Int32 i = 0; i < 8; i++)
                            {
                                clscontent = new Cls_ContentManagement();
                                if (CkhExisting)
                                {
                                    Int32 CountRow = i + 1;
                                    switch (i)
                                    {
                                        case 0:
                                            bool CheckExisting = ServicesFactory.DocCMSServices.Check_Existing_Menu_For_Save(Convert.ToString(dt.Rows[iRowCount]["Product Name"]).ToUpper(), pageID);
                                            if (CheckExisting)
                                            {
                                                CkhExisting = false;
                                            }
                                            else
                                            {
                                                clscontent.dorder = CountRow;
                                                clscontent.fieldtype = "Single Line Text";
                                                clscontent.fieldtext = "Product Name";
                                                clscontent.fieldvalue = Convert.ToString(dt.Rows[iRowCount]["Product Name"]);
                                            }
                                            break;
                                        case 1:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Multi Line Text";
                                            clscontent.fieldtext = "Description";
                                            clscontent.fieldvalue = Convert.ToString(dt.Rows[iRowCount]["Description"]);
                                            break;
                                        case 2:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Dropdown";
                                            clscontent.fieldvalue = Convert.ToString(dt.Rows[iRowCount]["Product Category Name"]);
                                            clscontent.fieldtext = Convert.ToString(dt.Rows[iRowCount]["Product Category Name"]);
                                            break;
                                        case 3:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Upload Image";
                                            clscontent.fieldtext = "Upload Image";
                                            clscontent.fieldvalue = "";

                                            break;
                                        case 4:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Single Line Text";
                                            clscontent.fieldtext = "Price";
                                            clscontent.fieldvalue = Convert.ToString(dt.Rows[iRowCount]["Price"]);

                                            break;
                                        case 5:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Checkbox";
                                            clscontent.fieldtext = "Show Price";
                                            clscontent.fieldvalue = "TRUE";

                                            break;
                                        case 6:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Single Line Text";
                                            Int32 Count = iRowCount + 1;
                                            clscontent.fieldvalue = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_Content_dorder_ByMenuname(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToString(dt.Rows[iRowCount]["Product Category Name"])) + 1);
                                            clscontent.fieldtext = "Display Order";

                                            break;
                                        case 7:
                                            clscontent.dorder = CountRow;
                                            clscontent.fieldtype = "Checkbox";
                                            clscontent.fieldtext = "Is Active";
                                            clscontent.fieldvalue = "TRUE";
                                            break;
                                       }
                                }
                                if (CkhExisting)
                                {
                                    lstContent.Add(clscontent);
                                }
                             }
                            if (lstContent.Count == 9)
                            {
                                RetVal = ServicesFactory.DocCMSServices.Insert_CMS_Data(lstContent, Convert.ToString(Session["UserID"]));
                                if (RetVal == "")
                                {
                                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Data inserted successfully');</script>");
                                }
                            }
                        }
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('There is no data to import');</script>");
                    return;
                }
            }
        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                String ext = "";
                if (FileUploadControl.PostedFile != null && !string.IsNullOrEmpty(FileUploadControl.PostedFile.FileName))
                {
                    ext = System.IO.Path.GetExtension(FileUploadControl.PostedFile.FileName);
                    if (ext.ToLower() == ".csv" || ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx")
                    {
                        string serverpath = Server.MapPath("~/UploadedFiles/Catalogue");
                        DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                        bool isDirCreated = directoryInfo.Exists;
                        if (!isDirCreated)
                        {
                            directoryInfo.Create();
                        }
                        FileUploadControl.PostedFile.SaveAs(serverpath + @"\\" + FileUploadControl.PostedFile.FileName);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Only csv and xls file are allowed');</script>");
                        return;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Please Select a file to import');</script>");
                    return;
                }
                string Uploadpath = "~/UploadedFiles/Catalogue/" + FileUploadControl.PostedFile.FileName.ToString();
                if (Session["UserID"] != null)
                {
                    if (ext.ToLower() == ".csv")
                    {
                        ReadCSV(68, Uploadpath);
                    }
                    else
                    {
                        ReadExcel(68, Uploadpath, ext);
                    }
                    Bind_Dashboard_Content(pageID);
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
             }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert(' Error in File Insertion !Please try again');", true);
                return;
            }
        }
    }// Class Ends HEre
}