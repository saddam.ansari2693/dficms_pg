﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.IO;
using DocCMS.Core.DataTypes;
using System.Drawing;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideImages : System.Web.UI.Page
    {
        dynamic UserId;
       

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if (Request.QueryString["ID"] != null && Request.QueryString["SID"] != null && Request.QueryString["SN"] != null)
                {
                    hdnsourceID.Value = Convert.ToString(Request.QueryString["SID"]);
                    hdnsourceName.Value = Convert.ToString(Request.QueryString["SN"]);
                    Bind_UserGuideImage_Data(Convert.ToInt32(Request.QueryString["ID"]));
                    Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName.Value));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else if (Request.QueryString["SID"] != null && Request.QueryString["SN"] != null)
                {
                    hdnsourceID.Value = Convert.ToString(Request.QueryString["SID"]);
                    hdnsourceName.Value = Convert.ToString(Request.QueryString["SN"]);
                    Bind_RoleName();
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_UserGuideimage_dorder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName.Value)) + 1;
                    Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value),Convert.ToString(hdnsourceName.Value));
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Role Name
        protected void Bind_RoleName()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Rolename();
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlRoleName.DataSource = dt;
                    ddlRoleName.DataTextField = "RoleName";
                    ddlRoleName.DataValueField = "RoleID";
                    ddlRoleName.DataBind();
                }
                if (Request.QueryString["ID"] != null)
                {
                    ddlRoleName.SelectedValue = Request.QueryString["SID"];

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Bind_DisplayOrder(Int32 SourceID,string SourceName)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_images_BasedOn_sourceidAndname(SourceID, SourceName);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Bind user guide image data
        protected void Bind_UserGuideImage_Data(Int32 ImageID)
        {
            try
            {
                Cls_UserGuideimage objUserGuideImage = ServicesFactory.DocCMSServices.Fetch_UserGuideimage_Byid(ImageID);
                if (objUserGuideImage != null)
                {
                   
                    Bind_RoleName();
                    ddlRoleName.SelectedValue = Convert.ToString(objUserGuideImage.roleid);
                    txtImageHeight.Value=  Convert.ToString(objUserGuideImage.imageheight);
                    txtImageWidth.Value = Convert.ToString(objUserGuideImage.imagewidth);
                    txtDisplayOrder.Value = Convert.ToString(objUserGuideImage.displayorder);
                    ChkActiveStatus.Checked = objUserGuideImage.isactive;
                    string serverpath = "";
                    if (objUserGuideImage.imagename != null && objUserGuideImage.imagename != "")
                    {
                        serverpath = @"../UploadedFiles/UserGuide/" + hdnsourceName.Value + "/" + hdnsourceID.Value + "/" + objUserGuideImage.roleid + "/" + objUserGuideImage.imagename;
                        lblFileUploader.Text = objUserGuideImage.imagename;
                    }
                    else
                    {
                        serverpath = @"../UploadedFiles/No_image_available.png";
                        lblFileUploader.Text = objUserGuideImage.imagename;
                    }
                    imgNoImage.Src = Convert.ToString(serverpath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            string fimagename = null;
            try
            {

                Cls_UserGuideimage clsUserGuideImages = new Cls_UserGuideimage();
                foreach (ListItem item in ddlRoleName.Items)
                {
                    if (item.Selected)
                    {
                        clsUserGuideImages.roleid = Convert.ToInt32(item.Value);
                        clsUserGuideImages.sourceid = Convert.ToInt32(hdnsourceID.Value);
                        clsUserGuideImages.source = Convert.ToString(hdnsourceName.Value);
                        clsUserGuideImages.imageheight = Convert.ToInt32(txtImageHeight.Value);
                        clsUserGuideImages.imagewidth = Convert.ToInt32(txtImageWidth.Value);
                        Int32 displayorder = ServicesFactory.DocCMSServices.Get_Max_UserGuideimage_dorder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName.Value)) + 1;
                        clsUserGuideImages.displayorder = Convert.ToInt32(displayorder);
                        clsUserGuideImages.createdby = Guid.Parse(UserId);
                        if (ChkActiveStatus.Checked)
                            clsUserGuideImages.isactive = true;
                        else
                            clsUserGuideImages.isactive = false;
                        if (ImageUploadControl.HasFile)
                        {
                            fimagename = ImageUploadControl.PostedFile.FileName;
                            string serverpath = Server.MapPath("../UploadedFiles/UserGuide/" + hdnsourceName.Value + "/" + hdnsourceID.Value + "/" + item.Value + "/");
                            DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                            bool isDirCreated = directoryInfo.Exists;
                            if (!isDirCreated)
                            {
                                directoryInfo.Create();
                            }
                            clsUserGuideImages.imagename = fimagename;
                            ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fimagename);
                        }
                        else
                        {
                            clsUserGuideImages.imagename = lblFileUploader.Text;
                        }
                            Int32 retVal = ServicesFactory.DocCMSServices.Insert_UserGuideimages(clsUserGuideImages);
                    }
                }
                Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?SID=" + hdnsourceID.Value + "&SN=" + hdnsourceName.Value + "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["ID"] != null)
                {
                    foreach (ListItem item in ddlRoleName.Items)
                    {
                        if (item.Selected)
                        {
                            Cls_UserGuideimage clsUserGuideImages = new Cls_UserGuideimage();
                            clsUserGuideImages.roleid = Convert.ToInt32(item.Value);
                            clsUserGuideImages.sourceid = Convert.ToInt32(hdnsourceID.Value);
                            clsUserGuideImages.imageid = Convert.ToInt32(Request.QueryString["ID"]);
                            clsUserGuideImages.source = Convert.ToString(hdnsourceName.Value);
                            clsUserGuideImages.imageheight = Convert.ToInt32(txtImageHeight.Value);
                            clsUserGuideImages.imagewidth = Convert.ToInt32(txtImageWidth.Value);
                            clsUserGuideImages.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                            clsUserGuideImages.createdby = Guid.Parse(UserId);
                            if (ChkActiveStatus.Checked)
                                clsUserGuideImages.isactive = true;
                            else
                                clsUserGuideImages.isactive = false;
                            if (ImageUploadControl.HasFile)
                            {
                                string fimagename = ImageUploadControl.PostedFile.FileName;
                                string serverpath = Server.MapPath("../UploadedFiles/UserGuide/" + hdnsourceName.Value + "/" + hdnsourceID.Value + "/" + Convert.ToInt32(item.Value) + "/");
                                DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                                bool isDirCreated = directoryInfo.Exists;
                                if (!isDirCreated)
                                {
                                    directoryInfo.Create();
                                }
                                clsUserGuideImages.imagename = fimagename;
                                ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + fimagename);
                            }
                            else
                            {
                                clsUserGuideImages.imagename = lblFileUploader.Text;
                            }
                            Int32 retVal = ServicesFactory.DocCMSServices.Update_UserGuideimages(clsUserGuideImages);
                        }
                    }
                    Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?SID=" + hdnsourceID.Value + "&SN=" + hdnsourceName.Value + "");
                }
                else
                {
                    // Do nothing
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?SID=" + hdnsourceID.Value + "&SN=" + hdnsourceName.Value + "");
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UserGuideimage> lstUserGuideImages = new List<Cls_UserGuideimage>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblUserGuideImageID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblUserGuideImageID");
                Label lblImageName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblImageName");
                if (Session["UserID"] != null && lblImageName != null && lblUserGuideImageID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblImageName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName));
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UserGuideimage objUserGuideImages = new Cls_UserGuideimage();
                Label lblUserGuideImageID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblUserGuideImageID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblUserGuideImageID != null)
                {
                    objUserGuideImages.imageid = Convert.ToInt32(lblUserGuideImageID.Text.Trim());
                    objUserGuideImages.displayorder = Convert.ToInt32(txtDorder.Value);
                    objUserGuideImages.modifiedby = Guid.Parse(UserId);
                    lstUserGuideImages.Add(objUserGuideImages);
                }
            }
            if (lstUserGuideImages != null && lstUserGuideImages.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UserGuideimages_dorder(lstUserGuideImages);
                if (retVal > 0)
                {
                    if (Request.QueryString["ID"] != null && Request.QueryString["MID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["ID"] + "," + Request.QueryString["MID"] + ");</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName));
                    }
                    else
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg1();</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnsourceID.Value), Convert.ToString(hdnsourceName));
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
        }
    }
}