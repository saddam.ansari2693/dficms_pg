﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using System.Data;
using DocCMS.Core;
using System.Web.Services;
using System.Web.Hosting;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class Modules : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["MID"] != null)
                {
                    Bind_Module_Data(Convert.ToInt32(Request.QueryString["MID"]));
                    Bind_DisplayOrder();
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Modules_Dorder() + 1;
                    Bind_DisplayOrder();
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Module Data when Module Id is alerady existing
        protected void Bind_Module_Data(Int32 ModuleId)
        {
            try
            {
                Clsmodules objModule = ServicesFactory.DocCMSServices.Fetch_Modules_ById(ModuleId);
                if (objModule != null)
                {
                    txtModuleName.Value = objModule.modulename;
                    txtDescription.Value = objModule.description;
                    txtDisplayOrder.Value = Convert.ToString(objModule.displayorder);
                    ChkActiveStatus.Checked = objModule.active;
                    txtNavigationUrl.Value = objModule.navigationurl;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Clsmodules clsModules = new Clsmodules();
                clsModules.modulename = txtModuleName.Value;
                clsModules.description = txtDescription.Value;
                clsModules.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsModules.navigationurl = txtNavigationUrl.Value;
                clsModules.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsModules.active = true;
                else
                    clsModules.active = false;

                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_Module(txtModuleName.Value.Trim());
                if (RetCheck > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Module with Same Name Already Exists...');</script>");
                    return;
                }
                else
                {
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_Modules(clsModules);
                    if (retVal > 0)
                    {
                        ServicesFactory.DocCMSServices.Adjust_Modules_Dorder();
                    }
                    Response.Redirect("~/cadmin/ModuleDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Clsmodules clsModules = new Clsmodules();
                clsModules.modulename = txtModuleName.Value;
                clsModules.description = txtDescription.Value;
                clsModules.navigationurl = txtNavigationUrl.Value;
                clsModules.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsModules.lastmodifiedby = Guid.Parse(UserId);
                clsModules.moduleid = Convert.ToInt32(Request.QueryString["MID"]);
                if (ChkActiveStatus.Checked)
                    clsModules.active = true;
                else
                    clsModules.active = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Modules(clsModules);
                if (retVal > 0)
                {
                    ServicesFactory.DocCMSServices.Adjust_Modules_Dorder();
                }
                Response.Redirect("~/cadmin/ModuleDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind Display order at Toggle inside RptrDisplayOrder repeater
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Modules();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/ModuleDashboard.aspx");
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }
        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Clsmodules> lstModules = new List<Clsmodules>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblModuleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblModuleID");
                Label lblModuleName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblModuleName");
                if (Session["UserID"] != null && lblModuleName != null && lblModuleID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblModuleName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                 }
             }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Clsmodules objModules = new Clsmodules();
                Label lblModuleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblModuleID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblModuleID != null)
                {
                    objModules.moduleid = Convert.ToInt32(lblModuleID.Text.Trim());
                    objModules.displayorder = Convert.ToInt32(txtDorder.Value);
                    objModules.lastmodifiedby = Guid.Parse(UserId);
                    lstModules.Add(objModules);
                }
            }
            if (lstModules != null && lstModules.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Module_Dorder(lstModules);
                if (retVal > 0)
                {
                    if (Request.QueryString["MID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["MID"] + ");</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                    }
                }
            }
        }
    }// Class Ends Here
}






