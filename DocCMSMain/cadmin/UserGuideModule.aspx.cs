﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideModule : System.Web.UI.Page
    {
        dynamic UserId;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if ( Request.QueryString["MID"] != null)
                {
                    Bind_ModuleName();
                    Bind_UserGuideModule_Data(Convert.ToInt32(Request.QueryString["MID"]));
                    Bind_DisplayOrder();
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    Bind_ModuleName();
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_UserGuidemodules_dorder() + 1;
                    Bind_DisplayOrder();
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind user guide module data
        protected void Bind_UserGuideModule_Data(Int32 MainContentID)
        {
            try
            {
                Cls_UserGuidemodules objUserGuideModule = ServicesFactory.DocCMSServices.Fetch_UserGuidemodules_Byid(MainContentID);
                if (objUserGuideModule != null)
                {
                    ddlModuleName.SelectedValue = Convert.ToString(objUserGuideModule.moduleid);
                    Bind_SubModuleName(Convert.ToInt32(objUserGuideModule.moduleid));
                    ddlSubModuleName.SelectedValue = Convert.ToString(objUserGuideModule.submoduleid);
                    elm1.Value = objUserGuideModule.description;
                    txtDisplayOrder.Value = Convert.ToString(objUserGuideModule.displayorder);
                    ChkActiveStatus.Checked = objUserGuideModule.isactive;
                    ChkIsExternalStatus.Checked = objUserGuideModule.isexternalcontent;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Bind_SubModuleName(Int32 ModuleID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SubModules_ByModuleId(ModuleID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlSubModuleName.DataSource = dt;
                    ddlSubModuleName.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Bind_ModuleName()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_ModulesName_UserGuide();
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlModuleName.DataSource = dt;
                    ddlModuleName.DataTextField = "ModuleName";
                    ddlModuleName.DataValueField = "ModuleId";
                    ddlModuleName.DataBind();
                }
                if (Request.QueryString["MID"] != null)
                {
                    ddlModuleName.SelectedValue = Request.QueryString["MID"];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlModuleName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ModuleID = Convert.ToInt32(ddlModuleName.SelectedValue);
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SubModulesname_UserGuide(ModuleID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlSubModuleName.DataSource = dt;
                    ddlSubModuleName.DataTextField = "SubModuleName";
                    ddlSubModuleName.DataValueField = "SubModuleId";
                    ddlSubModuleName.DataBind();
                }
                if (Request.QueryString["MID"] != null)
                {
                    ddlSubModuleName.SelectedValue = Request.QueryString["MID"];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UserGuidemodules();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UserGuidemodules clsUserGuideModules = new Cls_UserGuidemodules();
                clsUserGuideModules.moduleid = Convert.ToInt32(ddlModuleName.SelectedValue);
                clsUserGuideModules.submoduleid = Convert.ToInt32(ddlSubModuleName.SelectedValue);
                clsUserGuideModules.description = elm1.Value;
                clsUserGuideModules.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsUserGuideModules.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsUserGuideModules.isactive = true;
                else
                    clsUserGuideModules.isactive = false;
                if (ChkIsExternalStatus.Checked)
                    clsUserGuideModules.isexternalcontent = true;
                else
                    clsUserGuideModules.isexternalcontent = false;
                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_UserGuideModule(Convert.ToInt32(ddlModuleName.SelectedValue), Convert.ToInt32(ddlSubModuleName.SelectedValue), 0, 1);
                if (RetCheck > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('UserGuide with Same Name Already Exists...');</script>");
                    return;
                }
                else
                {
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_UserGuidemodules(clsUserGuideModules);
                    Response.Redirect("~/cadmin/UserGuideModuleDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["MID"] != null)
                {
                    Cls_UserGuidemodules clsUserGuideModules = new Cls_UserGuidemodules();
                    clsUserGuideModules.moduleid = Convert.ToInt32(ddlModuleName.SelectedValue);
                    clsUserGuideModules.submoduleid = Convert.ToInt32(ddlSubModuleName.SelectedValue);
                    clsUserGuideModules.description = elm1.Value;
                    clsUserGuideModules.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                    clsUserGuideModules.maincontentid = Convert.ToInt32(Request.QueryString["MID"]);
                    clsUserGuideModules.modifiedby = Guid.Parse(UserId);
                    if (ChkActiveStatus.Checked)
                        clsUserGuideModules.isactive = true;
                    else
                        clsUserGuideModules.isactive = false;
                    if (ChkIsExternalStatus.Checked)
                        clsUserGuideModules.isexternalcontent = true;
                    else
                        clsUserGuideModules.isexternalcontent = false;

                    Int32 RetCheck = 0;
                    RetCheck = ServicesFactory.DocCMSServices.Check_Existing_UserGuideModule(Convert.ToInt32(ddlModuleName.SelectedValue), Convert.ToInt32(ddlSubModuleName.SelectedValue), Convert.ToInt32(Request.QueryString["MID"]), 2);
                    if (RetCheck > 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('UserGuide with Same Name Already Exists...');</script>");
                        return;
                    }
                    else
                    {
                        Int32 retVal = ServicesFactory.DocCMSServices.Update_UserGuidemodules(clsUserGuideModules);
                        Response.Redirect("~/cadmin/UserGuideModuleDashboard.aspx");
                    }
                }
                else
                {
                    // Do nothing
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideModuleDashboard.aspx");
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UserGuidemodules> lstUserGuideModules = new List<Cls_UserGuidemodules>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblUserGuideModuleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblUserGuideModuleID");
                Label lblSubModuleName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSubModuleName");
                if (Session["UserID"] != null && lblSubModuleName != null && lblUserGuideModuleID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblSubModuleName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);

            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
           }

            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UserGuidemodules objUserGuideModules = new Cls_UserGuidemodules();
                Label lblUserGuideModuleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblUserGuideModuleID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblUserGuideModuleID != null)
                {
                    objUserGuideModules.maincontentid = Convert.ToInt32(lblUserGuideModuleID.Text.Trim());
                    objUserGuideModules.displayorder = Convert.ToInt32(txtDorder.Value);
                    objUserGuideModules.modifiedby = Guid.Parse(UserId);
                    lstUserGuideModules.Add(objUserGuideModules);
                }
            }

            if (lstUserGuideModules != null && lstUserGuideModules.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UserGuideModule_dorder(lstUserGuideModules);
                if (retVal > 0)
                {
                    if (Request.QueryString["ID"] != null && Request.QueryString["MID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["ID"] + "," + Request.QueryString["MID"] + ");</script>");
                        Bind_DisplayOrder();
                    }
                    else
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg1();</script>");
                        Bind_DisplayOrder();
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
        }
    }
}