﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="QuoteDetailDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.QuoteDetailDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[2, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
         });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        function ViewDetails(Price) {
            alert(Price);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage Detail Quote</a> <span class="separator"></span></li>
        <li>Manage Detail Quote</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Detail Quote</h5>
            <h1>
              Manage Detail Quote</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              Manage Detail Quote
                  <label id="Label2" runat="server" style="font-size: 13px; width: 150px; margin-left:1084px; margin-top:-16px">
                     <strong>Select Export Format:</strong>
                        </label>                        
                          <asp:DropDownList ID="ddlExport" runat="server" ClientIDMode="Static" 
                            style="font-size: 13px; width: 170px; margin-left:1214px; margin-top:-25px;">                        
                          <asp:ListItem Value="0" Text="Excel"> </asp:ListItem>
                          <asp:ListItem Value="1" Text="CSV(Comma Separated)"> </asp:ListItem>
                          <asp:ListItem Value="2" Text="CSV(Space Separated)"> </asp:ListItem>
                          <asp:ListItem Value="3" Text="PDF"> </asp:ListItem>
                         </asp:DropDownList>
                <asp:Button ID="btnExportTop" runat="server" CssClass="btn btn-primary" Width="150" Text="Export"
                    Style="margin-top:-42px; margin-left:1400px; float:left;" onclick="btnExportTop_Click" />
              </h4>
              <asp:Panel ID="PanelJob" runat="server">
            <table id="dyntable" class="table table-bordered responsive" >
                <colgroup>
                    <col class="con0" style="align: center; width: 20%;" />
                    <col class="con1" style="width: 20%;" />
                    <col class="con1" style="width: 20%;" />
                    <col class="con1" style="width: 20%;" />
                    <col class="con1" style="width: 20%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                       Category
                        </th>
                        <th class="head1">
                       Product Number
                          </th>
                        <th class="head1">
                       Product Name
                          </th>
                        <th class="head1">
                       Quantity
                       </th>
                        <th class="head1">
                      Unit Price
                        </th>
                        <th class="head1">
                       Amount
                        </th>
                 </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrOrder" OnItemCommand="RptrOrder_ItemCommand"
                        OnItemDataBound="RptrOrder_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductNumber")%>' ID="Label3"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductName")%>' ID="Label1"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("Quantity")%>' ID="lblOrderType"></asp:Label>
                                </td>
                               <td>
                                 $<asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("RunningPrice")%>' ID="lblRunningPrice"></asp:Label>
                                </td>
                             </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                                 <tr class="gradeX">
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductNumber")%>' ID="Label3"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductName")%>' ID="Label1"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("Quantity")%>' ID="lblOrderType"></asp:Label>
                                </td>
                               <td>
                                 $ <asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("RunningPrice")%>' ID="lblRunningPrice"></asp:Label>
                                </td>
                             </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            </asp:Panel>
             <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                Update Modules Display Orders
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table1" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 70%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Module&nbsp;Name
                                        </th>
                                        <th id="ActiveStatusheading" runat="server">
                                            Status
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnUpdate" class="btn btn-primary" onclick="ValidateDisplayOrders();"
                                style="display: none;" value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div  style="width:200px; float:right; text-align:center; background-color:#0F5B96; color:#fff;font-size:17px; padding:5px;">
              <span ID="divTotal" runat="server"></span>
              </div>
            <br />
            <br />
            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Width="200"
                Text="Back To Report Dashboard" OnClick="btnback_Click" />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
