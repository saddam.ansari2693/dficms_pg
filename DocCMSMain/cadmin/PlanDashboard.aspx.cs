﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;

namespace DocCMSMain.cadmin
{
    public partial class PlanDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Plans();
                SetControls();
            }
        }


        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddPlansTop.Visible = true;
                    btnAddPlansBottom.Visible = true;
                }
                else
                {
                    btnAddPlansTop.Visible = false;
                    btnAddPlansBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrPlans.Items)
                {
                    LinkButton lnkPlanName = (LinkButton)Item.FindControl("lnkPlanName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    if (lblTitle != null && lnkPlanName != null  && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkPlanName.Visible = true;
                            lblTitle.Visible = false;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkPlanName.Visible = false;
                            lblTitle.Visible = true;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }

        // Bind all Plans inside RptrPlans Repeater
        protected void Bind_Plans()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Plan_For_displayorder();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrPlans.DataSource = dt;
                    RptrPlans.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrPlans_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditPlan":
                        Response.Redirect("~/cadmin/Plans.aspx?PID=" + e.CommandArgument.ToString());
                        break;
                    case "DeletePlan":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_Plans(Convert.ToInt32(e.CommandArgument));
                        Bind_Plans();
                        Response.Redirect("~/cadmin/PlanDashboard.aspx");
                        break;
                   
                    default:
                        break;

                }
            }
        }

        protected void RptrPlans_databound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void btnAddPlansBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Plans.aspx");
        }
    }
}