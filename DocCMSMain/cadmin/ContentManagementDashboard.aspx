﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="ContentManagementDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.ContentManagementDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
         jQuery(document).ready(function () {
             jQuery('select[name="dyntable_length"]').on('change', function () {
                 var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
               });
           });
           // Set Session for showing data in data table
         function setSession(ShowTableSize) {
             var args = {
                 showtablesize: ShowTableSize
             }
             // service of session
             jQuery.ajax({
                 type: "POST",
                 url: "ContentManagementDashboard.aspx/SetSession",
                 data: JSON.stringify(args),
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function () {
                 },
                 error: function () {
                 }
             });
         }
     </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;
            if (selectedValue == 'Show Active') {
                  for (var i = 1; i <= TotalCount; i++) {
                  if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                          jQuery('#tableTR' + i).hide();
                }
                else {
                    jQuery('#tableTR' + i).show();
                }
              }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
      }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Content Management Dashboard</a> <span class="separator"></span>
        </li>
        <li id="ListPageName" runat="server">Manage Content </li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Content Management Dashboard</h5>
            <h1 id="headingPage" runat="server">
                Manage Content
            </h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
            <!--Filer by Status starts here--->
              <div class="par_ctrl_sec">
                     <div class="par control-group" >
                            <label class="control-label" for="ddlStatus">
                              Filer by Status</label>
                            <div class="controls">
                               <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                               <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                               </asp:DropDownList>
                            </div>
                        </div>
                     </div> 
            <!--Filer by Status ends here--->
                Content Pages Details
               <asp:Button ID="btnAddContentPageTop" runat="server" CssClass="btn btn-primary" 
                    Style="float: right; margin-top: -5px;" OnClick="btnAddContentPageBottom_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
            <div style="float:left">abc</div>
                <colgroup>
                   <col class="con1" style="align: center; width: 8%;"   />
                    <col class="con1" style="width: 16%;" />
                    <col class="con1" style="width: 17%;"  runat="server" id="ColDisplay"/>
                    <col class="con1" style="width: 16%;" />    
                    <col class="con1" style="width: 25%;" />
                    <col class="con1" style="align: center; width: 100%;" />                
                    <col class="con1" style="width: 28%" />
                </colgroup>
                <thead>
                    <tr >
                      <th class="head1" id="thDisplay" runat="server">
                            Display Order
                        </th>
                        <th class="head1"  id="CustomHeading" runat="server" clientidmode="Static">
                            Page Name
                        </th>
                        <th class="head1">
                            Content Title
                        </th>
                        <th class="head1" id="ActiveStatusheading" runat="server">
                            Active Status
                        </th>
                        <th class="head1">
                            Content Status
                        </th>
                        <th class="head1">
                            Creation Date
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrContentPage" OnItemCommand="RptrContentPage_ItemCommand"
                        OnItemDataBound="RptrContentPage_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                               <td id="trDisplay" runat="server"  visible="false">
                                    <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                </td>
                                <td>
                                    <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("PageName")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lblContentTitle" CommandName="EditPageContent"></asp:LinkButton>
                                 <asp:Label ID="LinkContentTitle" runat="server" Visible="false"></asp:Label>
                                </td>
                                <td id="Activestatusdata" runat="server" >
                                    <asp:Label runat="server" ID="lblExpirationDate" Text='<%# Eval("ExpirationDate")%>'
                                        CommandName="EditPageContent" Visible="false"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                        CommandName="EditPageContent"></asp:Label>
                                </td>
                                <td style="text-align:center;">
                                    <asp:Label runat="server" ID="lblContentState" Visible="false"></asp:Label>
                                    <asp:Literal runat="server" ID="litContentStatus"></asp:Literal>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCreationDate" Text=""></asp:Label>
                                </td>
                                <td class="">
                                <a runat="server" id="ancPreview">
                                    <img alt="" src="../images/green_prev.png" title="Preview Publish"  style="margin-bottom: -8px;height:23px" /></a>
                                <a runat="server" id="ancPreviewDraft">
                                       <img alt="" src="../images/orange_prev.png" title="Preview Draft" style="margin-bottom: -8px;height:23px" /></a>
                                     <asp:ImageButton runat="server"  ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandName="EditPageContent" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete"  ImageUrl="../images/table_icon_2.gif"
                                        CommandName="DeletePageContent" ToolTip="Delete Content Page" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td id="trDisplay" runat="server"  visible="false">
                                    <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                </td>
                                <td>
                                    <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("PageName")%>'></asp:Label>
                                </td>
                                <td>
                                 <asp:LinkButton runat="server" ID="lblContentTitle" CommandName="EditPageContent"></asp:LinkButton>
                                 <asp:Label ID="LinkContentTitle" runat="server" Visible="false"></asp:Label>
                                </td>
                                <td id="Activestatusdata" runat="server" >
                                    <asp:Label runat="server" ID="lblExpirationDate" Text='<%# Eval("ExpirationDate")%>'
                                        CommandName="EditPageContent" Visible="false"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                        CommandName="EditPageContent"></asp:Label>
                                </td>
                                <td style="text-align:center;">
                                    <asp:Label runat="server" ID="lblContentState" Visible="false"></asp:Label>
                                    <asp:Literal runat="server" ID="litContentStatus"></asp:Literal>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCreationDate" Text=""></asp:Label>
                                </td>
                                <td class="">
                                <a runat="server" id="ancPreview">
                                     <img alt="" src="../images/green_prev.png" title="Preview Publish" style="margin-bottom: -8px;height:23px ;padding-top: 5px;" /></a>
                                <a runat="server" id="ancPreviewDraft">
                                    <img alt="" src="../images/orange_prev.png" title="Preview Draft" style="margin-bottom: -8px;height:23px;padding-top: 5px; " /></a>
                                    <asp:ImageButton runat="server" style="padding-top: 5px;" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandName="EditPageContent" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" style="padding-top: 5px;" ImageUrl="../images/table_icon_2.gif"
                                        CommandName="DeletePageContent" ToolTip="Delete Content Page" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <uc1:Footer ID="Footer1" runat="server" />
            </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
      <script language="javascript" type="text/javascript">
          function SuccessMsg(PageID) {
              alert("Record is deleted successfully!!!");
              var url = "";
              if (PageID != null) {
                  url = "../cadmin/ContentManagementDashboard.aspx?PID=" + PageID;
                  $(location).attr('href', url);
              }
              else {
                  url = "../cadmin/ContentManagementDashboard.aspx";
                  $(location).attr('href', url);
              }
          }
        </script>
</asp:Content>
