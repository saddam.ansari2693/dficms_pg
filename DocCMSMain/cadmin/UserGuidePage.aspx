﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="UserGuidePage.aspx.cs" Inherits="DocCMSMain.cadmin.UserGuidePage" %>

<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var modal = document.getElementById('myModal');
            jQuery(".userGuideDiv img").click(function () {
                var modalImg = document.getElementById("img01");
                modalImg.src = jQuery(this).attr("src");
                modal.style.display = "block";

            });
        });
        function SetWidthHeight() {
            jQuery("#ContentPlaceHolder1_UserGuideDiv img").css("width", "70%");
            jQuery("#ContentPlaceHolder1_UserGuideDiv img").css("height", "100%");
        }
        function DisplayPreview(ctrl) {
            alert(ctrl);
        }
        function Close() {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: "Lato" , sans-serif;
        }
        .sidenav
        {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            right: 18px;
            background-color: #111;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
            margin-left: 700px;
        }
        
        .sidenav li
        {
            padding: 8px 8px 8px 32px;
            text-decoration: none;
            font-size: 15px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }
        
        .sidenav a:hover, .offcanvas a:focus
        {
            color: #f1f1f1;
            text-decoration: none !important;
        }
        
        .sidenav .closebtn
        {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }
        
        @media screen and (max-height: 450px)
        {
            .sidenav
            {
                padding-top: 15px;
            }
            .sidenav a
            {
                font-size: 18px;
            }
        }
        .sidenav .a
        {
        }
    </style>
    <style type="text/css">
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}
.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
<script type="text/javascript">
        // Get the modal
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('ctrl');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function () {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
</script>
<script type="text/javascript">
        function openNav() {
            if (document.getElementById("mySidenav").style.width == "0px") {
                document.getElementById("mySidenav").style.width = "250px";
            }
            else {
                document.getElementById("mySidenav").style.width = "0px";
            }
        }
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0px";
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="margin-top: 7px">
        <div id="myModal" class="modal" style="margin-left: 10px">
            <span class="close" onclick="Close();">&times;</span>
            <img class="modal-content" id="img01" style="padding-left: 265px; width: 96%; max-width: 62%;
                height: 82%;">
            <div id="caption">
            </div>
        </div>
        <div id="mySidenav" class="sidenav" style="margin-left: 1651px; margin-top: 150px;">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <ul id="maincontents" runat="server" style="padding-bottom: 100%">
            </ul>
        </div>
        <asp:Button ID="btnExport" Style="margin-left: 30px" class="btn btn-primary" runat="server"
            Text="Export Pdf" OnClick="btnExport_Click" Height="32px" />
        <asp:Label ID="lblAuthority" runat="server" ClientIDMode="Static" Style="display: none;"></asp:Label></div>
    <span style="font-size: 30px; cursor: pointer; margin-left: 1367px;" onclick="openNav()">
        GoTo&#9776;</span>
    <div runat="server" id="UserGuideDiv" style="margin-left: 50px; padding-top: 8px;
        padding-left: 20px; margin-top: 10px; overflow-y: auto; max-height: 830px; border: 1px solid #000;"
        class="userGuideDiv">
        <uc1:Footer ID="FooterControl" runat="server" />
    </div>
</asp:Content>
