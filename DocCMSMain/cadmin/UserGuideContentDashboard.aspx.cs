﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideContentDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";

            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["MID"] != null && Request.QueryString["MID"] != "")
                {
                    hndUGMainID.Value = Convert.ToString(Request.QueryString["MID"]);
                    Bind_SubContentItems(Convert.ToInt16(Request.QueryString["MID"]));
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                }
            }
        }
        protected void Bind_SubContentItems(int  MainContentID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UG_SubContent(MainContentID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrSubContentItem.DataSource = dt;
                    RptrSubContentItem.DataBind();
                }
                else
                {
                    RptrSubContentItem.DataSource = "";
                    RptrSubContentItem.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void RptrSubContentItem_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "AddImages":
                        Response.Redirect("UserGuideImageDashboard.aspx?SubContentID=" + e.CommandArgument.ToString() + "&MID=" + Convert.ToString(Request.QueryString["MID"]));
                        break;
                    case "EditSubContent":
                         Response.Redirect("UserGuideSubContent.aspx?SubContentID=" + e.CommandArgument.ToString() + "&MID=" + Convert.ToString(Request.QueryString["MID"]));
                       
                        break;
                    case "DeleteSubContent":
                        Int32 retval = ServicesFactory.DocCMSServices.Delete_UGSubContent_By_id(Convert.ToInt32(e.CommandArgument));
                        int SliderId = Convert.ToInt16(hndUGMainID.Value);
                        Bind_SubContentItems(Convert.ToInt16(Request.QueryString["MID"]));
                        
                        break;

                    default:
                        break;
                }
            }
        }

        protected void RptrSubContentItem_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGuideModuleDashboard.aspx");
        }

       

        protected void btnSubContent_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGuideSubContent.aspx?MID=" + hndUGMainID.Value);
        }
        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            Int32 Retval = ServicesFactory.DocCMSServices.Update_isactive_Content(Convert.ToInt32(SubContentID.Value), Convert.ToBoolean(CurStatus.Value));
            Bind_SubContentItems(Convert.ToInt16(Request.QueryString["MID"]));
        }
    }
}