﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DocCMS.Core;
using System.IO;
using System.Drawing;

namespace DocCMSMain.cadmin
{
    public partial class SurveyReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "licontent";
            Session["Ulname"] = "ulcontent";
            if (!IsPostBack)
            {
                if (Request.QueryString["PWID"] != null)
                {
                    // Do nothing
                }
                else
                {
                    Response.Redirect("SurveyResponseDashboard.aspx");
                }
            }
        }
        protected void lnkViewSummaryReport_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("SurveySummaryReport.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]) + "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lnkViewDetailedReport_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("SurveyDetailedReport.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]) + "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void lnkExport_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SurveyResponseDashboard.aspx");
        }

        protected void rbtnSummary_CheckedChanged(object sender, EventArgs e)
        {
            return;
        }

        protected void rbtnDetail_CheckedChanged1(object sender, EventArgs e)
        {
            return;
        }
    }// Class Ends here
}