﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideSubContent : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if (Request.QueryString["SubContentID"] != null && Request.QueryString["MID"] != null)
                {
                    Bind_UG_Content_Data(Convert.ToInt32(Request.QueryString["SubContentID"]));
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["MID"]));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_UG_SubContent_dorder(Convert.ToInt32(Request.QueryString["MID"])) + 1;
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["MID"]));
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        protected void Bind_UG_Content_Data(Int32 SubContentID)
        {
            try
            {
                Cls_UG_SubContent objMainContent = ServicesFactory.DocCMSServices.Fetch_UG_SubContent_Byid(SubContentID);
                if (objMainContent != null)
                {
                    txtSubHeading.Value = objMainContent.heading;
                    elm1.Value = objMainContent.description;
                    txtDisplayOrder.Value = Convert.ToString(objMainContent.dorder);
                    ChkActiveStatus.Checked = objMainContent.isactive;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_DisplayOrder(Int32 MainContentID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UG_SubContent(MainContentID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UG_SubContent objCLSSubContent = new Cls_UG_SubContent();
                objCLSSubContent.maincontentid = Convert.ToInt32(Request.QueryString["MID"]);
                objCLSSubContent.heading = txtSubHeading.Value;
                objCLSSubContent.description = elm1.Value;
                objCLSSubContent.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                objCLSSubContent.createdby = Convert.ToString(UserId);
                if (ChkActiveStatus.Checked)
                    objCLSSubContent.isactive = true;
                else
                    objCLSSubContent.isactive = false;

                Int32 retVal = ServicesFactory.DocCMSServices.Insert_UG_SubContent(objCLSSubContent);
                Response.Redirect("~/cadmin/UserGuideContentDashboard.aspx?MID=" + Convert.ToString(Request.QueryString["MID"]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideContentDashboard.aspx?MID=" + Convert.ToString(Request.QueryString["MID"]));
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UG_SubContent objCLSSubContent = new Cls_UG_SubContent();
                objCLSSubContent.subcontentid = Convert.ToInt32(Request.QueryString["SubContentID"]);
                objCLSSubContent.maincontentid = Convert.ToInt32(Request.QueryString["MID"]);
                objCLSSubContent.heading = txtSubHeading.Value;
                objCLSSubContent.description = elm1.Value;
                objCLSSubContent.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                objCLSSubContent.lastmodifiedby = Convert.ToString(UserId);
                if (ChkActiveStatus.Checked)
                    objCLSSubContent.isactive = true;
                else
                    objCLSSubContent.isactive = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UG_SubContent(objCLSSubContent);
                Response.Redirect("~/cadmin/UserGuideContentDashboard.aspx?MID=" + Convert.ToString(Request.QueryString["MID"]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }


        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UG_SubContent> lstModules = new List<Cls_UG_SubContent>();
            Int32[] Dorder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblSubContentID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSubContentID");
                Label lblheading = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblheading");
                if (Session["UserID"] != null && lblSubContentID != null && lblheading != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    Dorder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblheading.Text + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["MID"]));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(Dorder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < Dorder.Length - 1; j++)
            {
                if (Dorder[j] < Dorder[j + 1])
                {
                    if (Dorder[j] == Dorder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["MID"]));
                        return;
                    }
                }
                else if (Dorder[j] == Dorder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["MID"]));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UG_SubContent objModules = new Cls_UG_SubContent();
                Label lblSubContentID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSubContentID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblSubContentID != null)
                {
                    objModules.subcontentid = Convert.ToInt32(lblSubContentID.Text.Trim());
                    objModules.dorder = Convert.ToInt32(txtDorder.Value);
                    objModules.lastmodifiedby = Convert.ToString(UserId);
                    lstModules.Add(objModules);
                }
            }
            if (lstModules != null && lstModules.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UG_SubContent_dorder(lstModules);
                if (retVal > 0)
                {

                    if (Request.QueryString["MID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["MID"] + ");</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                    }
                }
            }
        }
    }
}