﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Services;

namespace DocCMSMain.cadmin
{
    public partial class Plans : System.Web.UI.Page
    {
        dynamic UserId;
        protected static Int32 FeatureIndex;
        protected void Page_Load(object sender, EventArgs e)
        {   
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            txtDisplayOrder.Attributes.Add("disabled", "disabled");
            if (!IsPostBack)
            {
                FeatureIndex = 1;
                if (Request.QueryString["PID"] != null)
                {
                    Bind_Plan_Data(Convert.ToInt32(Request.QueryString["PID"]));
                    DataTable dtCmsId = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(Convert.ToInt32(Request.QueryString["PID"]));
                    hdnCurPageID.Value = Convert.ToString((Convert.ToInt32(Request.QueryString["PID"])));
                    {
                        if (dtCmsId != null && dtCmsId.Rows.Count > 0)
                        {
                            hdnCurCMSID.Value = Convert.ToString(dtCmsId.Rows[0]["CMSID"]);
                            hdnCurPageID.Value = Convert.ToString((Convert.ToInt32(Request.QueryString["PID"])));
                        }
                    }
                    Bind_DisplayOrder();
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                   
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Plans_dorder() + 1;
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    Bind_DisplayOrder();
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }
        // Bind function for  Existing Plan 
        protected void Bind_Plan_Data(Int32 PlanID)
        {
            try
            {
                Cls_Plans objPlan = ServicesFactory.DocCMSServices.Fetch_Plan_Master_Byid(PlanID);
                if (objPlan != null)
                {
                    txtPlanName.Value = objPlan.planname;
                    txtDescription.Value = objPlan.plandescription;
                    txtDisplayOrder.Value = Convert.ToString(objPlan.dorder);
                    ChkActiveStatus.Checked = objPlan.active;
                    txtPrice.Value = objPlan.price;
                    lblFileUploader.Text = objPlan.imagename;
                    string serverpath = @"../UploadedFiles/Plans/" + objPlan.imagename;
                    imgLogo.Src = serverpath;

                }
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Plan_Detail_Data_By_planid(PlanID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    HtmlGenericControl[] aryDiv = new HtmlGenericControl[] { divFeature_1, divFeature_2, divFeature_3, divFeature_4, divFeature_5, divFeature_6, divFeature_7, divFeature_8, divFeature_9, divFeature_10 };
                    HtmlInputCheckBox[] aryActiveStatus = new HtmlInputCheckBox[] { chkActive1, chkActive2, chkActive3, chkActive4, chkActive5, chkActive6, chkActive7, chkActive8, chkActive9, chkActive10 };
                    HtmlInputText[] aryFeatureName = new HtmlInputText[] { txtFeatureName1, txtFeatureName2, txtFeatureName3, txtFeatureName4, txtFeatureName5, txtFeatureName6, txtFeatureName7, txtFeatureName8, txtFeatureName9, txtFeatureName10 };
                    HtmlInputText[] aryDisplayNumber = new HtmlInputText[] { txtFeatureDisplayOrder1, txtFeatureDisplayOrder2, txtFeatureDisplayOrder3, txtFeatureDisplayOrder4, txtFeatureDisplayOrder5, txtFeatureDisplayOrder6, txtFeatureDisplayOrder7, txtFeatureDisplayOrder8, txtFeatureDisplayOrder9, txtFeatureDisplayOrder10 };
                    FeatureIndex = FeatureIndex + dt.Rows.Count-1;
                    for (Int32 ItemCount = 0; ItemCount < dt.Rows.Count; ItemCount++)
                    {
                        string FeatureName = Convert.ToString(dt.Rows[ItemCount]["FeatureName"]);
                        string DisplayOrder = Convert.ToString(dt.Rows[ItemCount]["DisplayOrder"]);
                        string ActiveStatus = Convert.ToString(Convert.ToString(dt.Rows[ItemCount]["Active"]));
                        if (!string.IsNullOrEmpty(FeatureName) && !string.IsNullOrEmpty(DisplayOrder) && !string.IsNullOrEmpty(ActiveStatus))
                        {
                            aryDiv[ItemCount].Visible = true;
                            aryFeatureName[ItemCount].Value = FeatureName;
                            aryDisplayNumber[ItemCount].Value = DisplayOrder;
                            if (ActiveStatus.ToUpper() == "TRUE")
                            {
                                aryActiveStatus[ItemCount].Checked = true;
                            }
                            else
                            {
                                aryActiveStatus[ItemCount].Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       // Save Plan Detail
        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                bool FeatureFlag = true;
                List<Cls_Plans> objPlanList = new List<Cls_Plans>();
                Cls_Plans clsPlans = new Cls_Plans();
                clsPlans.planname = txtPlanName.Value;
                clsPlans.plandescription = txtDescription.Value;
                clsPlans.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsPlans.price = txtPrice.Value;
                clsPlans.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsPlans.isactive = true;
                else
                    clsPlans.isactive = false;

                if (ImageUploadControl.HasFile)
                {
                    string serverpath = Server.MapPath("../UploadedFiles/Plans/");
                    DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                    bool isDirCreated = directoryInfo.Exists;
                    if (!isDirCreated)
                    {
                        directoryInfo.Create();
                    }

                    ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + ImageUploadControl.FileName);
                    clsPlans.imagename = ImageUploadControl.FileName;
                }
                else
                {
                    clsPlans.imagename = "";

                }
                objPlanList.Add(clsPlans);
                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_planname(txtPlanName.Value.Trim());
                if (RetCheck > 0)
                {
                    FeatureFlag = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Plan with Same Name Already Exists...');</script>");
                    return;
                }
                else
                {

                    HtmlGenericControl[] aryDiv = new HtmlGenericControl[] { divFeature_1, divFeature_2, divFeature_3, divFeature_4, divFeature_5, divFeature_6, divFeature_7, divFeature_8, divFeature_9, divFeature_10 };
                    for (Int32 i = 0; i < aryDiv.Length; i++)
                    {
                        if (aryDiv[i].Visible == true)
                        {
                            clsPlans = new Cls_Plans();
                            switch (i)
                            {
                                case 0:
                                    if (ValidateFeature(chkActive1, txtFeatureName1, txtFeatureDisplayOrder1) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName1.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder1.Value);
                                        clsPlans.active = chkActive1.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 1:
                                    if (ValidateFeature(chkActive2, txtFeatureName2, txtFeatureDisplayOrder2) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName2.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder2.Value);
                                        clsPlans.active = chkActive2.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 2:
                                    if (ValidateFeature(chkActive3, txtFeatureName3, txtFeatureDisplayOrder3) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName3.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder3.Value);
                                        clsPlans.active = chkActive3.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 3:
                                    if (ValidateFeature(chkActive4, txtFeatureName4, txtFeatureDisplayOrder4) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName4.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder4.Value);
                                        clsPlans.active = chkActive4.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 4:
                                    if (ValidateFeature(chkActive5, txtFeatureName5, txtFeatureDisplayOrder5) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName5.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder5.Value);
                                        clsPlans.active = chkActive5.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 5:
                                    if (ValidateFeature(chkActive6, txtFeatureName6, txtFeatureDisplayOrder6) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName6.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder6.Value);
                                        clsPlans.active = chkActive6.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 6:
                                    if (ValidateFeature(chkActive7, txtFeatureName7, txtFeatureDisplayOrder7) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName7.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder7.Value);
                                        clsPlans.active = chkActive7.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 7:
                                    if (ValidateFeature(chkActive8, txtFeatureName8, txtFeatureDisplayOrder8) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName8.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder8.Value);
                                        clsPlans.active = chkActive8.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 8:
                                    if (ValidateFeature(chkActive9, txtFeatureName9, txtFeatureDisplayOrder9) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName9.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder9.Value);
                                        clsPlans.active = chkActive9.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 9:
                                    if (ValidateFeature(chkActive10, txtFeatureName10, txtFeatureDisplayOrder10) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName10.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder10.Value);
                                        clsPlans.active = chkActive10.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    string RetVal = "";
                    if (FeatureFlag == true)
                    {
                        if (objPlanList != null && objPlanList.Count > 1)
                        {
                            RetVal = ServicesFactory.DocCMSServices.Insert_Plan_Detail(objPlanList);
                            Response.Redirect("~/cadmin/PlanDashboard.aspx");
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Please Provide the Features Details...');</script>");
                            return;
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Please Provide the Features Details...');</script>");
                        return;
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Update Existing Plan Detail
        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                bool FeatureFlag = true;
                List<Cls_Plans> objPlanList = new List<Cls_Plans>();
                Cls_Plans clsPlans = new Cls_Plans();
                clsPlans.planid =Convert.ToInt32( Request.QueryString["PID"]);
                clsPlans.plandescription = txtDescription.Value;
                clsPlans.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsPlans.price = txtPrice.Value;
                clsPlans.lastmodifiedby= Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsPlans.isactive = true;
                else
                    clsPlans.isactive = false;

                if (ImageUploadControl.HasFile)
                {
                    string serverpath = Server.MapPath("../UploadedFiles/Plans/");
                    DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                    bool isDirCreated = directoryInfo.Exists;
                    if (!isDirCreated)
                    {
                        directoryInfo.Create();
                    }
                    ImageUploadControl.PostedFile.SaveAs(serverpath + @"\\" + ImageUploadControl.FileName);
                    clsPlans.imagename = ImageUploadControl.FileName;
                }
                else
                {
                    clsPlans.imagename = lblFileUploader.Text;
                }
                objPlanList.Add(clsPlans);
                HtmlGenericControl[] aryDiv = new HtmlGenericControl[] { divFeature_1, divFeature_2, divFeature_3, divFeature_4, divFeature_5, divFeature_6, divFeature_7, divFeature_8, divFeature_9, divFeature_10 };
                    for (Int32 i = 0; i < aryDiv.Length; i++)
                    {
                        if (aryDiv[i].Visible == true)
                        {
                            clsPlans = new Cls_Plans();
                            switch (i)
                            {
                                case 0:
                                    if (ValidateFeature(chkActive1, txtFeatureName1, txtFeatureDisplayOrder1) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName1.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder1.Value);
                                        clsPlans.active = chkActive1.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 1:
                                    if (ValidateFeature(chkActive2, txtFeatureName2, txtFeatureDisplayOrder2) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName2.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder2.Value);
                                        clsPlans.active = chkActive2.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 2:
                                    if (ValidateFeature(chkActive3, txtFeatureName3, txtFeatureDisplayOrder3) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName3.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder3.Value);
                                        clsPlans.active = chkActive3.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 3:
                                    if (ValidateFeature(chkActive4, txtFeatureName4, txtFeatureDisplayOrder4) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName4.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder4.Value);
                                        clsPlans.active = chkActive4.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 4:
                                    if (ValidateFeature(chkActive5, txtFeatureName5, txtFeatureDisplayOrder5) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName5.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder5.Value);
                                        clsPlans.active = chkActive5.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 5:
                                    if (ValidateFeature(chkActive6, txtFeatureName6, txtFeatureDisplayOrder6) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName6.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder6.Value);
                                        clsPlans.active = chkActive6.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 6:
                                    if (ValidateFeature(chkActive7, txtFeatureName7, txtFeatureDisplayOrder7) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName7.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder7.Value);
                                        clsPlans.active = chkActive7.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 7:
                                    if (ValidateFeature(chkActive8, txtFeatureName8, txtFeatureDisplayOrder8) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName8.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder8.Value);
                                        clsPlans.active = chkActive8.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 8:
                                    if (ValidateFeature(chkActive9, txtFeatureName9, txtFeatureDisplayOrder9) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName9.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder9.Value);
                                        clsPlans.active = chkActive9.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                case 9:
                                    if (ValidateFeature(chkActive10, txtFeatureName10, txtFeatureDisplayOrder10) == true)
                                    {
                                        clsPlans.featurename = txtFeatureName10.Value;
                                        clsPlans.displayorder = Convert.ToInt32(txtFeatureDisplayOrder10.Value);
                                        clsPlans.active = chkActive10.Checked;
                                        objPlanList.Add(clsPlans);
                                    }
                                    else
                                    {
                                        FeatureFlag = false;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                     }
                    string RetVal = "";
                    if (FeatureFlag == true)
                    {
                        if (objPlanList != null && objPlanList.Count > 1)
                        {
                            RetVal = ServicesFactory.DocCMSServices.Update_Plan_Page_Byid(objPlanList);
                        }
                    }
                   Response.Redirect("~/cadmin/PlanDashboard.aspx");
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }
        // Bind Display order at Toggle inside RptrDisplayOrder repeater
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Plan_For_displayorder();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/PlanDashboard.aspx");
        }

    
        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_Plans> lstPlans = new List<Cls_Plans>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblPlanID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblPlanID");
                Label lblPlanName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblPlanName");
                if (Session["UserID"] != null && lblPlanName != null && lblPlanID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblPlanName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);

            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                }

            }

            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_Plans objPlans = new Cls_Plans();
                Label lblPlanID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblPlanID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblPlanID != null)
                {
                    objPlans.planid = Convert.ToInt32(lblPlanID.Text.Trim());
                    objPlans.dorder= Convert.ToInt32(txtDorder.Value);
                    objPlans.lastmodifiedby = Guid.Parse(UserId);
                    lstPlans.Add(objPlans);
                }
            }

            if (lstPlans != null && lstPlans.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Plans_dorder(lstPlans);
                if (retVal > 0)
                {

                    if (Request.QueryString["PID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["PID"] + ");</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");

                    }
                }
            }
        }
        
        // To Check Whether the Fearture has complete Entry or not
        protected bool ValidateFeature(HtmlInputCheckBox chkCurActive, HtmlInputText txtCurFeature, HtmlInputText txtCurDorder)
        {
            bool flag = true;
            try
            {
                if(chkCurActive!=null && txtCurFeature!=null && txtCurDorder!=null)
                {
                    if (string.IsNullOrEmpty(txtCurFeature.Value) || string.IsNullOrEmpty(txtCurDorder.Value))
                    {
                        flag = false;
                    }
                }
                else
                {
                    flag=false;
                }
                return flag;
            }
            catch
            {
                return false;
            }
        }

        protected void imgAddfeature_Click(object sender, ImageClickEventArgs e)
        {
            
                    if (FeatureIndex <= 10)
                    {
                        FeatureIndex++;
                        switch (FeatureIndex)
                        {
                            case 2:
                                divFeature_2.Visible = true;
                                break;
                            case 3:
                                divFeature_3.Visible = true;
                                break;
                            case 4:
                                divFeature_4.Visible = true;
                                break;
                            case 5:
                                divFeature_5.Visible = true;
                                break;
                            case 6:
                                divFeature_6.Visible = true;
                                break;
                            case 7:
                                divFeature_7.Visible = true;
                                break;
                            case 8:
                                divFeature_8.Visible = true;
                                break;
                            case 9:
                                divFeature_9.Visible = true;
                                break;
                            case 10:
                                divFeature_10.Visible = true;
                                break;
                        }
                    }
              }
        // Hide DivFeature on Cancel button Click
        protected void ImgCancel1_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_1.Visible = false;
        }

        protected void ImgCancel2_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_2.Visible = false;
        }
        protected void ImgCancel3_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_3.Visible = false;
        }
        protected void ImgCancel4_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_4.Visible = false;
        }
        protected void ImgCancel5_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_5.Visible = false;
        }
        protected void ImgCancel6_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_6.Visible = false;
        }
        protected void ImgCancel7_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_7.Visible = false;
        }
        protected void ImgCancel8_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_8.Visible = false;
        }
        protected void ImgCancel9_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_9.Visible = false;
        }
        protected void ImgCancel10_Click(object sender, ImageClickEventArgs e)
        {
            divFeature_10.Visible = false;
        }
     
       
    }// Class Ends Here
}