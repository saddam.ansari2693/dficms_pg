﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class QuoteCommentDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Quotes();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
              
            }
        }
        // Bind Quotes inside Repeater
        protected void Bind_Quotes()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_AllQuote_comment();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrQuote.DataSource = dt;
                    RptrQuote.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrQuote_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditQuote":
                        Response.Redirect("~/cadmin/QuoteCommentEntry.aspx?QID=" + e.CommandArgument.ToString());
                        break;

                    case "DeleteQuote":
                         Int32 retVal = ServicesFactory.DocCMSServices.Delete_Quote(Convert.ToInt32(e.CommandArgument));
                         Bind_Quotes();
                         Response.Redirect("~/cadmin/QuoteCommentDashboard.aspx");
                        
                        break;
                  
                    default:
                        break;
                }
            }
        }

        protected void RptrQuote_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }
        //Function When add quote is clicked
        protected void btnAddQuote_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/QuoteCommentEntry.aspx");
        }
    }
}