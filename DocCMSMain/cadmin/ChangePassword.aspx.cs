﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (!IsPostBack)
            {
                if (Session["UserID"] == null || string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    string Password = "";
                    if (!string.IsNullOrEmpty(txtNewPswd.Value))
                    {
                        if (txtNewPswd.Value.Equals(txtCnfPswd.Value))
                        {
                            Password = txtNewPswd.Value;
                        }
                    }
                    if (Session["UserID"] != null)
                    {
                        ServicesFactory.DocCMSServices.Change_User_password_ById(Guid.Parse(Convert.ToString(Session["UserID"])), Password);
                        divalert.Visible = true;
                    }
                    else
                    {
                        Response.Redirect("~/cadmin/Default.aspx");
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        protected void Cancel_click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Default.aspx");
        }
    }
}