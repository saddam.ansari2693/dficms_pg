﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SurveyReports.aspx.cs" Inherits="DocCMSMain.cadmin.SurveyReports" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Survey Response Dashboard</a> <span class="separator"></span></li>
        <li>Manage Survey Response Dashboard</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Survey Response Dashboard</h5>
            <h1>
                Survey Response Dashboard</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                Survey Response Dashboard
            </h4>
            <asp:Panel ID="PanelJob" runat="server">
                <table id="dyntable" class="table table-bordered responsive">
                    <colgroup>
                        <col class="con0" style="align: right; width: 2%; display: none;" />
                        <col class="con0" />
                        <col class="con0" />
                        <col class="con1" style="align: right; width: 40%;" />
                        <col class="con1" style="align: center; width: 10%;" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="head1" style="display: none;">
                                Select
                            </th>
                            <th class="head1">
                                Report Type
                            </th>
                            <th class="head0">
                                Repondents
                            </th>
                            <th class="head0">
                                Q&A Format
                            </th>
                            <th class="head1">
                                Functions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="gradeX">
                            <td style="display: none;">
                                <center>
                                    <asp:RadioButton ID="rbtnSummary" runat="server" GroupName="Report" Style="vertical-align: middle;"
                                        OnCheckedChanged="rbtnSummary_CheckedChanged" AutoPostBack="true" Checked="true" />
                                </center>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkSummaryReport" ToolTip="View Report" runat="server" Text="Summary Report"
                                    CssClass="linktitle" OnClick="lnkViewSummaryReport_Click"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='Summarized'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text='Summarized'></asp:Label>
                            </td>
                            <td class="center">
                                <asp:LinkButton runat="server" ID="lnkViewSummaryReport" CssClass="btn btn-primary"
                                    ToolTip="View Summary Report" Text="View Report" OnClick="lnkViewSummaryReport_Click"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr class="gradeA">
                            <td style="display: none;">
                                <center>
                                    <asp:RadioButton ID="rbtnDetail" runat="server" GroupName="Report" Style="vertical-align: middle;"
                                        AutoPostBack="true" OnCheckedChanged="rbtnDetail_CheckedChanged1" />
                                </center>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDetailedReport" ToolTip="View Report" runat="server" Text="Detailed Report"
                                    CssClass="linktitle" OnClick="lnkViewDetailedReport_Click"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label runat="server" Text='1 Per Row' ID="lblTotalResponses"></asp:Label>
                            </td>
                            <td>
                                <asp:Label runat="server" Text='1 Question per Column, Exact Text Answer Displayed'
                                    ID="Label4"></asp:Label>
                            </td>
                            <td class="center">
                                <asp:LinkButton runat="server" ID="lnkViewDetailedReport" CssClass="btn btn-primary"
                                    ToolTip="View Detailed Report" Text="View Report" OnClick="lnkViewDetailedReport_Click"></asp:LinkButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
