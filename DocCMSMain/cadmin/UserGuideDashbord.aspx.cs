﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.Factory;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text.RegularExpressions;
using System.Text;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.HtmlControls;


namespace DocCMSMain.cadmin
{
    public partial class UserGuideDashbord : System.Web.UI.Page
    {
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (!IsPostBack)
            {
                if (Session["RoleID"] != null)
                {
                   
                    Bind_UG_Content();
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    SetControls();
                }
                else
                {
                }
            }
        }

        protected void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnaddmainHeading.Visible = true;
                }
                else
                {
                    btnaddmainHeading.Visible = false;
                }
                foreach (RepeaterItem Item in RptrUGMainContent.Items)
                {

                    LinkButton lnkheading = (LinkButton)Item.FindControl("lnkheading");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton lnkNavigate = (ImageButton)Item.FindControl("lnkNavigate");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    Label IsActive=(Label)Item.FindControl("IsActive");
                    if (lblTitle != null && lnkheading != null && lnkEdit != null && lnkDelete != null && lnkNavigate != null  && IsActive !=null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkheading.Visible = true;
                            lblTitle.Visible = false;
                            lnkNavigate.Enabled = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkheading.Visible = false;
                            lblTitle.Visible = true;
                            lnkNavigate.Enabled = false;
                            IsActive.Visible = true;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }



        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        protected void Bind_UG_Content()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UG_MainContent();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrUGMainContent.DataSource = dt;
                    RptrUGMainContent.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnaddmainHeading_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideMainContent.aspx");
        }

        protected void RptrUGMainContent_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {

            }
        }

        protected void RptrUGMainContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditUGContent":
                        Response.Redirect("~/cadmin/UserGuideMainContent.aspx?MID=" + e.CommandArgument.ToString());
                        break;
                    case "AddUGContent":
                        Response.Redirect("~/cadmin/UserGuideContentDashboard.aspx?MID=" + e.CommandArgument.ToString());
                        break;
                    case "DeleteUGContent":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_UGMainContent_By_id(Convert.ToInt32(e.CommandArgument));
                        Bind_UG_Content();
                        Response.Redirect("~/cadmin/UserGuideDashbord.aspx");
                        break;
                      default:
                        break;

                }
            }
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            Int32 Retval = ServicesFactory.DocCMSServices.Update_isactive(Convert.ToInt32(MainContentID.Value), Convert.ToBoolean(CurStatus.Value));
            Bind_UG_Content();
        }
    }
}