﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="UserRoles.aspx.cs" Inherits="DocCMSMain.cadmin.UserRoles" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8) {
                return true;
            }
            if (key > 31 && (key < 48 || key > 57))
                return false;
            return true;
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">User Roles</a> <span class="separator"></span></li>
        <li>Manage User Roles</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                User Roles</h5>
            <h1>
                Manage User Roles</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <div class="widget">
                <h4 class="widgettitle">
                    User Roles Details</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform">
                        <div class="par control-group">
                            <label class="control-label" for="TxtUserRole">
                                Role Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="TxtUserRole" id="TxtUserRole" class="input-large"
                                    clientidmode="Static" placeholder="Enter Role Name" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="TxtUserRole" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtModuleName">
                                Role Description</label>
                            <div class="controls">
                                <textarea id="txtDescription" name="txtDescription" rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="height: 100px; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" placeholder="Enter Sub Module Description"></textarea>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtDescription" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="TxtDisplayOrder">
                                Display Order</label>
                            <div class="controls">
                                <input type="text" runat="server" name="TxtDisplayOrder" id="TxtDisplayOrder" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Order Number" maxlength="5" disabled />
                                <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                    Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click" />
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    style="opacity: 1" checked />
                            </div>
                        </div>
                        <br />
                        <p class="stdformbutton">
                            <asp:Button ID="btnsubmit" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnsubmit_onclick" Text="Submit" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnupdate" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnupdate_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                OnClick="btnCancel_Click" />
                            </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            <!-- for Display Order Popup -->
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                Update Role Display Orders
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 70%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                    
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Role&nbsp;Name
                                        </th>
                                          <th id="ActiveStatusheading" runat="server">
                                        Status
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                            <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                onclick="ToggleDisplay();" />
                                        </th>
                                        </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrDisplayOrder">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("RoleId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("RoleName") %>'></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("RoleId") %>'
                                                        Visible="false"></asp:Label>
                                                    <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("Active")%>'
                                                        CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" maxlength="5" onkeypress="return onlyNumbers(event)"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("DisplayOrder") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                <td>
                                                    <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("RoleId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("RoleName") %>'></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                   <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("RoleId") %>'
                                                        Visible="false"></asp:Label>
                                                   <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("Active")%>'
                                                        CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" maxlength="5" onkeypress="return onlyNumbers(event)"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("DisplayOrder") %>' />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnUpdateDisplay" class="btn btn-primary" onclick="ValidateDisplayOrders();" Style="display: none;"
                                value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->

  <script language="javascript" type="text/javascript">
      function ValidateDisplayOrders() {
          var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
          btnUpdateDisplayOrder.click();
      }
      function SuccessMsg(RoleID) {
          alert("The provided display orders are successfully Updated");
          var url = "";
          if (RoleID != null) {
              url = "../cadmin/UserRoles.aspx?RID=" + RoleID;
              $(location).attr('href', url);
          }
          else {
              url = "../cadmin/UserRoles.aspx";
              $(location).attr('href', url);
          }
      }

      function ToggleDisplay(ItemIndex) {
          jQuery(".DorderLabel").toggle();
          jQuery(".DorderText").toggle();
          jQuery("#btnUpdateDisplay").show();
          jQuery("#img1").hide();
      }
 </script>
</asp:Content>
