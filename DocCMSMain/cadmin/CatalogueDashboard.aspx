﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="CatalogueDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.CatalogueDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }

        function Validate_CheckBox() {
            var btn = document.getElementById("btnDelete");
            if (jQuery('input[type=checkbox]:checked').length != 0) {
               btn.disabled = false;
            }
            else {
                   btn.disabled = true;
                }
        }
        function Validate_CheckBox_All() {
            var btn = document.getElementById("btnSelectAll");
            btn.click();
        }

        function SelectAll() {
            var btn = document.getElementById("btnDelete");
            var TargetBaseControl = document.getElementById("dyntable");
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n) {
                if (Inputs[n].type == 'checkbox') {
                    var ChkID = Inputs[n].id.toString();
                    if (jQuery("#ChkSelectAll").prop('checked') == true) {
                        jQuery("#dyntable span").addClass("checked");
                        jQuery("#dyntable").find("input[type='hidden']").val("checked");
                        btn.disabled = false;
                    }
                    else {
                        jQuery("#dyntable span").removeClass("checked");
                        jQuery('input[type=checkbox]:checked').attr('checked', false);
                        jQuery("#dyntable").find("input[type='hidden']").val("");
                        btn.disabled = true;
                    }
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Content Management Dashboard</a> <span class="separator"></span>
        </li>
        <li id="ListPageName" runat="server">Manage Catalogue </li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Content Management Dashboard</h5>
            <h1 id="headingPage" runat="server">
                Manage Catalogue
            </h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                Content Pages Details
                 <asp:Button ID="btnAddContentPageTop" runat="server" Text="Add new store item Catalogue" CssClass="btn btn-primary" 
                    Style="float: right; margin-top: -5px;" OnClick="btnAddContentPageBottom_Click" />
                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" 
                    Style="float: right; margin-top: -5px;" onclick="btnDelete_Click" Text="Delete" Enabled="false" ClientIDMode="Static" />
               <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="btnDelete" />
                      </h4>
             <h4 class="widgettitle">
                 <asp:FileUpload ID="FileUploadControl" runat="server" style="display:none;" />
                 <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-primary"  Text="Click here to import"
                    Style="margin-left:-56px;display:none;" OnClick="btnUpload_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                     <col class="con1" style="align: center; width: 10%;"   />
                     <col class="con1" style="align: center; width: 10%;"   />
                     <col class="con1" style="width: 15%;" />
                     <col class="con1" style="width: 11%;" />
                     <col class="con1" style="width: 11%;" />
                     <col class="con1" style="width: 8%;"  runat="server" id="ColDisplay"/>
                     <col class="con1" style="width: 8%;" />  
                     <col class="con1" style="width: 10%;" />      
                     <col class="con1" style="width: 10%;"  runat="server" id="ColCheck"/>             
                     <col class="con1" style="align: center; width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1" id="thImage" runat="server">
                            Image
                        </th>
                         <th class="head1">
                           Product Number
                        </th>
                        <th class="head1">
                           Product Name
                        </th>
                          <th class="head1">
                         Category
                        </th>
                        <th class="head1">
                           Description
                        </th>
                         <th class="head1" id="thDisplay" runat="server">
                            Display Order
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                            Creation Date
                        </th>
                         <th class="head1" id="th1" runat="server" style="text-align:center">
                         <asp:Button ID="btnSelectAll" runat="server" Text="Button"  ClientIDMode="Static"
                       onclick="btnSelectAll_Click" style=" display:none;"  />
                    <span id="chkall" runat="server"><input type="checkbox" runat="server" style="align: center;" id="ChkSelectAll" onchange="return Validate_CheckBox_All();"  /></span>
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrContentPage" OnItemCommand="RptrContentPage_ItemCommand"
                        OnItemDataBound="RptrContentPage_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("ImageName")%>' Visible="false" ID="lblCatalogImageName"></asp:Label>
                                    <asp:Image ID="ImgCatalog" runat="server" Width="60px" Height="60px" ImageUrl="">
                                    </asp:Image>
                                </td>
                                 <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("ProductNumber")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("PageName")%>' Visible="false"></asp:Label>
                                     <asp:LinkButton runat="server" Text='<%# Eval("ProductName")%>' ID="lblContentTitle" CommandName="EditPageContent" ></asp:LinkButton>
                                     <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("ProductName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Category")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description")%>'></asp:Label>
                                </td>
                                   <td id="trDisplay" runat="server"  >
                                    <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                </td>
                                <td>
                                    <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                                <td id="trCheckbox" runat="server"  style="text-align:center">
                                  <span id="chkbox" runat="server"><asp:CheckBox ID="ChkActive" runat="server" onchange="return Validate_CheckBox();" /></span>
                               </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandName="EditPageContent" ToolTip="Edit Content Page" />
                                     <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif" CommandName="DeletePageContent"  CommandArgument='<%# Eval("CMSID")%>' ToolTip="Delete Content Page" OnClientClick="return confirm('Are you sure you want to delete this?');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                              <tr class="gradeX">
                              <td>
                                    <asp:Label runat="server" Text='<%# Eval("ImageName")%>' Visible="false" ID="lblCatalogImageName"></asp:Label>
                                    <asp:Image ID="ImgCatalog" runat="server" Width="60px" Height="60px" ImageUrl="">
                                    </asp:Image>
                                </td>
                                 <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("ProductNumber")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lnkPageName" runat="server" Text='<%# Eval("PageName")%>' Visible="false"></asp:Label>
                                    <asp:LinkButton runat="server" Text='<%# Eval("ProductName")%>' ID="lblContentTitle" CommandName="EditPageContent" ></asp:LinkButton>
                                    <asp:Label ID="lblTitle"  runat="server" ClientIDMode="Static" Text='<%# Eval("ProductName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Category")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description")%>'></asp:Label>
                                </td>
                                 <td id="trDisplay" runat="server"  visible="false">
                                    <asp:Label ID="lblModuleID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                </td>
                                <td>
                                    <asp:Label ID="lblActive" runat="server" Text='<%# Eval("IsActive")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                                <td id="trCheckbox" runat="server"  style="text-align:center">
                                 <span id="chkbox" runat="server"><asp:CheckBox ID="ChkActive" runat="server"  onchange="return Validate_CheckBox();"  /></span>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandName="EditPageContent" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif" CommandName="DeletePageContent"  CommandArgument='<%# Eval("CMSID")%>' ToolTip="Delete Content Page" OnClientClick="return confirm('Are you sure you want to delete this?');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <uc1:Footer ID="Footer1" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
