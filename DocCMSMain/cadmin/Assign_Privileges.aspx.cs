﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.Services;
using System.Web.Hosting;
using System.IO;
using DocCMS.Core.DataTypes;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace DocCMSMain.cadmin
{
    public partial class Assign_Privileges : System.Web.UI.Page
    {
        string SuperAdminName = "";
        string SuperAdminId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (!IsPostBack)
            {
                if (Session["UserID"] != null && Session["RoleID"] != null)
                {
                    hdnUserRole.Value = Convert.ToString(Session["RoleID"]);
                    SuperAdminName = ServicesFactory.DocCMSServices.Get_Single_Value_String("RoleName", "UserRoles", "RoleId", hdnUserRole.Value, "Number");
                    hdnSuperAdmin.Value = SuperAdminName;
                    SuperAdminId = ServicesFactory.DocCMSServices.Get_Single_Value_String("RoleId", "UserRoles", "RoleName", "SuperAdmin", "Text");
                    Bind_Role(Convert.ToInt32(hdnUserRole.Value));
                    Bind_RolePrivilege(Convert.ToInt32(hdnMasterRole.Value));
                }
                else
                {
                    Response.Redirect("~/cadmin/Default.aspx");
                }
            }
        }

        // Bind Role inside ddlRole Dropdown control 
        protected void Bind_Role(Int32 RoleID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Roles(RoleID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlRole.DataSource = dt;
                    ddlRole.DataTextField = "RoleName";
                    ddlRole.DataValueField = "RoleId";
                    ddlRole.DataBind();
                    hdnRoleID.Value = Convert.ToString(dt.Rows[0]["RoleId"]);
                    hdnMasterRole.Value = Convert.ToString(dt.Rows[0]["RoleId"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Hide and show Export and SelectAll checkbox
        public void HideAndShow()
        {
        }

        //  Update and Submit button Functionality on the basis of All Checked
        protected void Bind_RolePrivilege(Int32 RoleID)
        {
            try
            {
                DataTable dtModules = ServicesFactory.DocCMSServices.Fetch_Modules_For_Grid(RoleID);
                if (dtModules != null && dtModules.Rows.Count > 0)
                {
                    RptrPrivileges.DataSource = dtModules;
                    RptrPrivileges.DataBind();
                    Int32 CheckCount = ServicesFactory.DocCMSServices.Fetch_Checked_modules_By_roleid(RoleID);
                    if (SuperAdminName.ToUpper() == "SUPERADMIN")
                    {
                        if (CheckCount > 0)
                        {
                            btnupdate.Visible = true;
                            btnsubmit.Visible = false;
                        }
                        else
                        {
                            btnsubmit.Visible = true;
                            btnupdate.Visible = false;
                        }
                    }
                    else
                    {
                        if (CheckCount > 0)
                        {
                            if (Convert.ToString(hdnRoleID.Value).ToUpper() == Convert.ToString(hdnUserRole.Value).ToUpper())
                            {
                                btnupdate.Visible = false;
                                btnCancel.Visible = false;
                                btnsubmit.Visible = false;
                            }
                            else
                            {
                                btnupdate.Visible = true;
                                btnsubmit.Visible = false;
                                btnCancel.Visible = true;
                            }
                        }
                        else
                        {
                            if (Convert.ToString(hdnRoleID.Value).ToUpper() == Convert.ToString(hdnUserRole.Value).ToUpper())
                            {
                                btnsubmit.Visible = false;
                                btnCancel.Visible = false;
                                btnsubmit.Visible = false;
                            }
                            else
                            {
                                btnsubmit.Visible = true;
                                btnupdate.Visible = false;
                                btnCancel.Visible = true;
                            }
                        }
                    }
                    Int32 CountAll = ServicesFactory.DocCMSServices.Fetch_CheckedAll_modules_By_roleid(RoleID);
                    if (CountAll > 0)
                    {
                        chkSelectAll.Checked = false;
                    }
                    else
                    {
                        chkSelectAll.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrPrivileges_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void RptrPrivileges_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                DataTable dtSubmodule = null;
                Repeater rptrSubmodules = (Repeater)e.Item.FindControl("rptrSubmodules");
                Label lblNone = (Label)e.Item.FindControl("lblNone");
                if (lblNone != null && rptrSubmodules != null)
                {
                    if (SuperAdminName.ToUpper() == "SUPERADMIN")
                    {
                        dtSubmodule = ServicesFactory.DocCMSServices.Fetch_privileges_By_roleid(Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt32(lblNone.Text));
                    }
                    else if (Convert.ToString(hdnMasterRole.Value).ToUpper() == Convert.ToString(ddlRole.SelectedValue).ToUpper())
                    {
                        dtSubmodule = ServicesFactory.DocCMSServices.Fetch_privileges_By_RoleId_For_Other_Admin(Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt32(lblNone.Text));
                    }
                    else
                    {
                        dtSubmodule = ServicesFactory.DocCMSServices.Fetch_privileges_By_roleid(Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt32(lblNone.Text));
                    }
                    // Bind the SubModule inside rptrSubmodules Repeater
                    if (dtSubmodule != null && dtSubmodule.Rows.Count > 0)
                    {
                        rptrSubmodules.DataSource = dtSubmodule;
                        rptrSubmodules.DataBind();
                    }
                }
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRoleID.Value = ddlRole.SelectedValue;
            SuperAdminName = ServicesFactory.DocCMSServices.Get_Single_Value_String("RoleName", "UserRoles", "RoleId", hdnUserRole.Value, "Number");
            SuperAdminId = ServicesFactory.DocCMSServices.Get_Single_Value_String("RoleId", "UserRoles", "RoleName", "SuperAdmin", "Text");
            Bind_RolePrivilege(Convert.ToInt32(hdnRoleID.Value));
            HideAndShow();
            return;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Assign_Privileges.aspx");
        }
        protected void btnExportPdf_Click(object sender, EventArgs e)
        {
            Export_Roles(true);
        }
        public float Export_Roles(bool IsDownload)
        {

            string PDFName = "";
            string JobApplicationDirectoryPath = "";
            float TotalLoggedHours = 0.0f;
            try
            {

                PDFName = "RolesAndPrivileges";
                JobApplicationDirectoryPath = "/PDFs/";
                string appRootDir = Server.MapPath(JobApplicationDirectoryPath);

                // Step 1: Creating System.IO.FileStream object
                using (FileStream fs = new FileStream(appRootDir + "/" + PDFName.ToString() + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                // Step 2: Creating iTextSharp.text.Document object
                using (Document doc = new Document(PageSize.LETTER))
                // Step 3: Creating iTextSharp.text.pdf.PdfWriter object
                // It helps to write the Document to the Specified FileStream
                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    // Step 4: Openning the Document
                    doc.Open();
                    // Step 5: Adding a paragraph
                    // NOTE: When we want to insert text, then we've to do it through creating paragraph
                    // TO Add Header Rectangle and text
                    PdfContentByte cbHeader = writer.DirectContent;
                    // select the font properties
                    BaseFont bfHeader = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    cbHeader.SaveState();
                    cbHeader.SetColorFill(BaseColor.LIGHT_GRAY);
                    cbHeader.Rectangle(35, 800, 530, 20);
                    cbHeader.Fill();
                    cbHeader.RestoreState();
                    cbHeader.BeginText();
                    cbHeader.SetColorFill(BaseColor.BLACK);
                    cbHeader.SetFontAndSize(bfHeader, 14);
                    cbHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Roles & Privileges", 550, 805, 0);
                    cbHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "DocFocus", 120, 805, 0);
                    cbHeader.EndText();
                    doc.Add(new Paragraph(20, "\u00a0"));
                    // TO TIMESHEET HEADING
                    PdfContentByte cbHeading = writer.DirectContent;
                    // select the font properties
                    BaseFont bfcbHeading = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    DataTable dt = ServicesFactory.DocCMSServices.Fetch_Selected_Roles(Convert.ToInt32(ddlRole.SelectedValue));
                    BaseColor FontColor = new BaseColor(255, 255, 255);
                    var MyFont = FontFactory.GetFont("Arial", 10, FontColor);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            PdfPTable table1 = new PdfPTable(new float[] { 100f });
                            table1.WidthPercentage = 100;
                            // Main Coulm binding starts here................
                            var WhiteColor = FontFactory.GetFont("Arial");
                            WhiteColor.SetColor(255, 255, 255);
                            var RolesTitle = new Chunk("Role Name : " + Convert.ToString(dt.Rows[i]["RoleName"]), WhiteColor);
                            //PdfPCell cellModuleName = new PdfPCell(new Phrase("Module: " + Convert.ToString(dtModules.Rows[j]["ModuleName"])));

                            PdfPCell cell1 = new PdfPCell(new Phrase(RolesTitle));
                            cell1.BackgroundColor = new BaseColor(90, 90, 90);
                            cell1.BorderColor = new BaseColor(255, 255, 255);
                            cell1.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            // Main Coulm binding ends here...............
                            table1.AddCell(cell1);
                            doc.Add(table1);
                            doc.Add(new Paragraph(5, "\u00a0"));
                            DataTable dtModules = ServicesFactory.DocCMSServices.Fetch_Modules_For_Grid(Convert.ToInt32(dt.Rows[i]["RoleId"]));
                            if (dtModules != null && dtModules.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtModules.Rows.Count; j++)
                                {
                                    PdfPTable tableModule = new PdfPTable(new float[] { 60f, 10f, 10f, 10f, 10f });
                                    var ModuleWhiteColor = FontFactory.GetFont("Arial");
                                    WhiteColor.SetColor(255, 255, 255);
                                    var ModuleRolesTitle = new Chunk("Module: " + Convert.ToString(dtModules.Rows[j]["ModuleName"]), WhiteColor);

                                    PdfPCell cellModuleName = new PdfPCell(new Phrase(ModuleRolesTitle));
                                    cellModuleName.BackgroundColor = new BaseColor(51, 153, 255);
                                    cellModuleName.BorderColor = new BaseColor(255, 255, 255);
                                    cellModuleName.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                    tableModule.AddCell(cellModuleName);

                                    var ModuleRolesView = new Chunk("View", WhiteColor);
                                    PdfPCell cellModuleView = new PdfPCell(new Phrase(ModuleRolesView));
                                    cellModuleView.BackgroundColor = new BaseColor(51, 153, 255);
                                    cellModuleView.BorderColor = new BaseColor(255, 255, 255);
                                    cellModuleView.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                    tableModule.AddCell(cellModuleView);

                                    var ModuleRolesAdd = new Chunk("Add", WhiteColor);
                                    PdfPCell cellModuleAdd = new PdfPCell(new Phrase(ModuleRolesAdd));
                                    cellModuleAdd.BackgroundColor = new BaseColor(51, 153, 255);
                                    cellModuleAdd.BorderColor = new BaseColor(255, 255, 255);
                                    cellModuleAdd.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                    tableModule.AddCell(cellModuleAdd);
                                    

                                    var ModuleRolesEdit = new Chunk("Edit", WhiteColor);
                                    PdfPCell cellModuleEdit = new PdfPCell(new Phrase(ModuleRolesEdit));
                                    cellModuleEdit.BackgroundColor = new BaseColor(51, 153, 255);
                                    cellModuleEdit.BorderColor = new BaseColor(255, 255, 255);
                                    cellModuleEdit.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                    tableModule.AddCell(cellModuleEdit);
                                    
                                    var ModuleRolesDelete = new Chunk("Delete", WhiteColor);
                                    PdfPCell cellModuleDelete = new PdfPCell(new Phrase(ModuleRolesDelete));
                                    cellModuleDelete.BackgroundColor = new BaseColor(51, 153, 255);
                                    cellModuleDelete.BorderColor = new BaseColor(255, 255, 255);
                                    cellModuleDelete.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                    tableModule.AddCell(cellModuleDelete);

                                    // Main column binding ends here...............
                                    if (dtModules.Rows[j]["IsView"].ToString().ToLower() == "checked" || dtModules.Rows[j]["IsAdd"].ToString().ToLower() == "checked" || dtModules.Rows[j]["IsEdit"].ToString().ToLower() == "checked" || dtModules.Rows[j]["IsDelete"].ToString().ToLower() == "checked")
                                    {
                                        doc.Add(tableModule);
                                    }
                                    doc.Add(new Paragraph(5, "\u00a0"));
                                    DataTable dtSubmodule = null;
                                    string SuperAdminName = ServicesFactory.DocCMSServices.Fetch_Roles_name(Convert.ToInt32(hdnRoleID.Value));
                                    dtSubmodule = ServicesFactory.DocCMSServices.Fetch_privileges_By_roleid(Convert.ToInt32(dt.Rows[i]["RoleId"]), Convert.ToInt32(dtModules.Rows[j]["ModuleID"]));
                                    if (dtSubmodule != null && dtSubmodule.Rows.Count > 0)
                                    {
                                        for (int k = 0; k < dtSubmodule.Rows.Count; k++)
                                        {
                                            PdfPTable tableSubModule = new PdfPTable(new float[] { 60f, 10f, 10f, 10f, 10f });
                                            // Main Coulm binding starts here................
                                            PdfPCell cellSubModuleName = new PdfPCell(new Phrase(Convert.ToString(dtSubmodule.Rows[k]["SubModuleName"])));
                                            cellSubModuleName.BackgroundColor = new BaseColor(200, 200, 200);
                                            cellSubModuleName.BorderColor = new BaseColor(255, 255, 255);
                                            cellSubModuleName.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                            string viewText = "";
                                            string AddText = "";
                                            string EditText = "";
                                            string DeleteText = "";
                                            if (Convert.ToString(dtSubmodule.Rows[k]["IsView"]).ToLower() == "checked")
                                                viewText = "Yes";
                                            else
                                                viewText = "No";

                                            if (Convert.ToString(dtSubmodule.Rows[k]["IsAdd"]).ToLower() == "checked")
                                                AddText = "Yes";
                                            else
                                                AddText = "No";
                                            if (Convert.ToString(dtSubmodule.Rows[k]["IsEdit"]).ToLower() == "checked")
                                                EditText = "Yes";
                                            else
                                                EditText = "No";
                                            if (Convert.ToString(dtSubmodule.Rows[k]["IsDelete"]).ToLower() == "checked")
                                                DeleteText = "Yes";
                                            else
                                                DeleteText = "No";

                                            PdfPCell cellSubModuleView = new PdfPCell(new Phrase(viewText));
                                            cellSubModuleView.BackgroundColor = new BaseColor(200, 200, 200);
                                            cellSubModuleView.BorderColor = new BaseColor(255, 255, 255);
                                            cellSubModuleView.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                            PdfPCell cellSubModuleAdd = new PdfPCell(new Phrase(AddText));
                                            cellSubModuleAdd.BackgroundColor = new BaseColor(200, 200, 200);
                                            cellSubModuleAdd.BorderColor = new BaseColor(255, 255, 255);
                                            cellSubModuleAdd.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                            PdfPCell cellSubModuleEdit = new PdfPCell(new Phrase(EditText));
                                            cellSubModuleEdit.BackgroundColor = new BaseColor(200, 200, 200);
                                            cellSubModuleEdit.BorderColor = new BaseColor(255, 255, 255);
                                            cellSubModuleEdit.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                            PdfPCell cellSubModuleDelete = new PdfPCell(new Phrase(DeleteText));
                                            cellSubModuleDelete.BackgroundColor = new BaseColor(200, 200, 200);
                                            cellSubModuleDelete.BorderColor = new BaseColor(255, 255, 255);
                                            cellSubModuleDelete.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                            // Main Coulm binding ends here...............
                                            if (Convert.ToString(dtSubmodule.Rows[k]["IsView"]).ToLower() == "checked" || Convert.ToString(dtSubmodule.Rows[k]["IsAdd"]).ToLower() == "checked" || Convert.ToString(dtSubmodule.Rows[k]["IsAdd"]).ToLower() == "checked" || Convert.ToString(dtSubmodule.Rows[k]["IsDelete"]).ToLower() == "checked")
                                            {
                                                tableSubModule.AddCell(cellSubModuleName);
                                                tableSubModule.AddCell(cellSubModuleView);
                                                tableSubModule.AddCell(cellSubModuleAdd);
                                                tableSubModule.AddCell(cellSubModuleEdit);
                                                tableSubModule.AddCell(cellSubModuleDelete);
                                            }
                                            else
                                            {
                                                tableSubModule.AddCell(cellSubModuleName);
                                                if (Convert.ToString(dtSubmodule.Rows[k]["IsView"]).ToLower() == "checked")
                                                {
                                                    tableSubModule.AddCell(cellSubModuleView);
                                                }
                                                if (Convert.ToString(dtSubmodule.Rows[k]["IsAdd"]).ToLower() == "checked")
                                                {
                                                    tableSubModule.AddCell(cellSubModuleAdd);
                                                }
                                                if (Convert.ToString(dtSubmodule.Rows[k]["IsEdit"]).ToLower() == "checked")
                                                {
                                                    tableSubModule.AddCell(cellSubModuleEdit);
                                                }
                                                if (Convert.ToString(dtSubmodule.Rows[k]["IsDelete"]).ToLower() == "checked")
                                                {
                                                    tableSubModule.AddCell(cellSubModuleDelete);
                                                }
                                            }
                                            doc.Add(tableSubModule);
                                            doc.Add(new Paragraph(5, "\u00a0"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // Step 6: Closing the Document
                    doc.Close();
                }
                if (IsDownload)
                {
                    ProcessRequest(appRootDir + "/" + PDFName.ToString() + ".pdf");
                }

                return TotalLoggedHours;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ProcessRequest(string fileRelativePath)
        {
            try
            {
                
                string contentType = "";
                //Get the physical path to the file.
                string FilePath = fileRelativePath;
                contentType = "application/pdf";
                //Set the appropriate ContentType.
                HttpContext.Current.Response.ContentType = contentType;
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(fileRelativePath)).Name);
                //Write the file directly to the HTTP content output stream.
                HttpContext.Current.Response.WriteFile(FilePath);
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                //To Do
                throw ex;
            }
        }

        //Show checkboxes Based on Roles and privileges
        public void Show_Checkbox_Based_On_Roles()
        {
            foreach (RepeaterItem MainRepItem in RptrPrivileges.Items)
            {
                Repeater rptrSubmodules = (Repeater)MainRepItem.FindControl("rptrSubmodules");
                Label lblNone = (Label)MainRepItem.FindControl("lblNone");
                DataTable dtSubmodule = ServicesFactory.DocCMSServices.Fetch_privileges_By_roleid(Convert.ToInt32(hdnMasterRole.Value), Convert.ToInt32(lblNone.Text));
                hdnRolesPrivileges.Value += Convert.ToString(lblNone.Text) + ":";
                foreach (DataRow dr in dtSubmodule.Rows)
                {
                    if (Convert.ToString(dr["IsView"]) == "Checked" && Convert.ToString(dr["IsAdd"]) == "Checked" && Convert.ToString(dr["IsEdit"]) == "Checked" && Convert.ToString(dr["IsDelete"]) == "Checked")
                    {
                        hdnRolesPrivileges.Value += dr["SubModuleID"] + "|";
                    }
                }
                hdnRolesPrivileges.Value += ",";
            }
        }
    }// Class Ends Here
}