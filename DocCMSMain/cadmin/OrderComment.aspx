﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="OrderComment.aspx.cs" Inherits="DocCMSMain.cadmin.OrderComment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
         }

        function onlyNumbers(event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
      <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;
            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
         }

        function Validate_CheckBox() {
            var btn = document.getElementById("btnDelete");
            if (jQuery('input[type=checkbox]:checked').length != 0) {
                btn.disabled = false;
            }
            else {
                btn.disabled = true;
              }
           }

        function AddNewComment() {
            var QuoteName = jQuery("#txtCommentHeading").val();
            var QuoteComment = jQuery("#txtComment").val();
            jQuery.ajax({
                url: "/WebService/DocCMSApi.svc/Add_NewQuote_to_QuotesTable",
                contentType: 'application/json; charset=utf-8',
                data: { "QuoteName": QuoteName.toString(), "QuoteComment": QuoteComment.toString() },
                dataType: "json",
                success: ajaxSucceeded,
                error: ajaxFailed
            });
            function ajaxSucceeded(data) {
                location.reload();
            }
            function ajaxFailed() {
              alert("There is an error during operation.");
            }
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Quote Comment</a> <span class="separator"></span></li>
        <li>Manage Quote Comment</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Order Description</h5>
            <h1>
                Manage Order Description</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              <!--Filer by Status starts here--->
              <div class="par_ctrl_sec">
                     <div class="par control-group" >
                            <label class="control-label" for="ddlStatus">
                              Filer by Status</label>
                            <div class="controls">
                               <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                                <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                               </asp:DropDownList>
                            </div>
                        </div>
                     </div> 
            <!--Filer by Status ends here--->
               Order Description
                <asp:Button ID="btnAddCustomOrderDescription" ClientIDMode="Static" runat="server" data-toggle="modal" data-target="#myModelComment" CssClass="btn btn-primary" Width="150" Text="Add New Description"
                    Style="float: right; margin-top: -5px;"  />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 55%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 5%;" />                    
                    <col class="con1" style="align: center; width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            OrderDescriptionID
                        </th>
                        <th class="head1">
                            Order Description
                        </th>
                        <th class="head1">
                            Insert Order
                        </th>
                        <th class="head1">
                            Select Description
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrOrderDescription"  OnItemDataBound="RptrOrderDescription_ItemDataBound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:Label ID="lblOrderDescriptionID" runat="server" ClientIDMode="Static" Text='<%# Eval("OrderDescriptionID") %>'
                                       ></asp:Label>
                               </td>
                                <td>
                                   <asp:Label ID="lblOrderDescription" runat="server" ClientIDMode="Static" Text='<%# Eval("OrderDescription")%>'
                                        ></asp:Label>
                                </td>
                                <td>
                                  <asp:TextBox runat="server" ID="txtOrder"  onkeypress="return onlyNumbers(event)" ClientIDMode="Static"></asp:TextBox>
                                </td>
                                <td>
                                 <span id="chkbox" runat="server"><asp:CheckBox ID="ChkActive" runat="server" onchange="return Validate_CheckBox();" /></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:Label ID="lblOrderDescriptionID" runat="server" ClientIDMode="Static" Text='<%# Eval("OrderDescriptionID") %>'
                                       ></asp:Label>
                               </td>
                                <td>
                                   <asp:Label ID="lblOrderDescription" runat="server" ClientIDMode="Static" Text='<%# Eval("OrderDescription")%>'
                                        ></asp:Label>
                                </td>
                                <td>
                                  <asp:TextBox runat="server" ID="txtOrder"  onkeypress="return onlyNumbers(event)" ClientIDMode="Static"></asp:TextBox>
                                </td>
                                <td>
                                  <span id="chkbox" runat="server"><asp:CheckBox ID="ChkActive" runat="server" onchange="return Validate_CheckBox();" /></span>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
              <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Width="200"
                Text="Back To Order detail" OnClick="btnback_Click" />
              <asp:Button ID="btnAddDescriptionToOrder" runat="server" 
                CssClass="btn btn-primary" Width="200" style="float: right;"
                Text="Add Comments to Quote" onclick="btnAddDescriptionToOrder_Click"  />
            <br />
            <br />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
  <div class="modal fade" id="myModelComment" role="dialog" style="width:40%">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enter Order Description</h4>
        </div>
        <div class="modal-body">
           <div id="divNewComment" >Order Description:<textarea id="txtComment" name="txtComment"  rows="8" clientidmode="Static" validationgroup="grpsubmit" style="height: 100px;margin-left: 3%; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" ></textarea>
   </div> 
    <hr />    
      <input type="button" id="btnAddNewComment"  onclick="AddNewDescription()"  value="Add Description" /> <br /><br /> 
        </div>
        <div class="modal-footer">
          <button type="button" id="Button2" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     </div>
  </div>
</asp:Content>

