﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="MediaManagementDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.MediaManagementDashboard" %>


<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx"  TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
           
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }

        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
              });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {

                },
                error: function () {

                }
            });

        }

    </script>
     <%--Filter by Status ie show all,show active,show inactive--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">
            <lable id="labheading1" runat="server"></lable>
        </a><span class="separator"></span></li>
        <li>
            <lable id="labheading2" runat="server"></lable>
        </li>

    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5 id="h5heading1" runat="server">
                Heading1</h5>
            <h1 id="h1heading1" runat="server">
                Heading2</h1>
        </div>
    </div>
    <!--pageheader-->
    <div id="lblError" class="alert-customerror" runat="server" visible="false">
      This page is referenced somewhere.It can't be deleted
    </div>
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle" id="h4heading11" runat="server">
        
                <label id="h4heading1" runat="server">
                </label>
                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Width="150" Text="Add New Entry"
                    OnClick="cmdInsert_Click" Style="float: right; margin-top: -25px;" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con1" style="width: 10%;"/>   
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" style="align: center; width: 10%" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head0">
                            File Name
                        </th>
                        <th class="head0">
                            Type
                        </th>
                          <th class="head1"  clientidmode="Static">
                            Original File Name
                        </th>
                        <th class="head1">
                            Last Modified by
                        </th>
                        <th class="head1">
                            Last Modified Date
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RepeaterContent" OnItemCommand="RepeaterContent_ItemCommand">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:LinkButton ID="linktitle" CommandArgument='<%# Eval("id")%>' CommandName="EditLink"
                                        runat="server" Text='<%# Eval("Filename")%>' />
                                           <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("Filename")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="Lablink"></asp:Label>
                                </td>
                                <td id="tdColVideoNameheading" >
                                     <asp:Label ID="lblVideoName" runat="server" Text='<%#Eval("OrignalFileName")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Uploadedby")%>' ID="Label4"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("ModificationDate","{0:MM/dd/yyyy}")%>'
                                        ID="Label3"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="LinkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("Id")%>' CommandName="EditLink" ToolTip="Edit" />
                                    <asp:ImageButton runat="server" ID="linkdelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("Id")%>' CommandName="Delete" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this?');"/>
                                    </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                <td>
                                    <asp:LinkButton ID="linktitle" CommandArgument='<%# Eval("id")%>' CommandName="EditLink"
                                        runat="server" Text='<%# Eval("Filename")%>' />
                                           <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("Filename")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="Lablink"></asp:Label>
                                </td>
                                <td id="tdColVideoNameheading" >
                                     <asp:Label ID="lblVideoName" runat="server" Text='<%#Eval("OrignalFileName")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Uploadedby")%>' ID="Label4"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("ModificationDate","{0:MM/dd/yyyy}")%>'
                                        ID="Label3"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="LinkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("Id")%>' CommandName="EditLink" ToolTip="Edit" />
                                    <asp:ImageButton runat="server" ID="linkdelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("Id")%>' CommandName="Delete" ToolTip="Delete" OnClientClick="return confirm('Are you sure to delete this record ?');"/>
                                     </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <asp:Button ID="cmdInsert" runat="server" CssClass="btn btn-primary" Width="250"
                Text="Add Banner Images Links" CommandArgument="New" ValidationGroup="grpsubmit"
                OnClick="cmdInsert_Click" />
            <br />
            <br />
            
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
  
</asp:Content>

