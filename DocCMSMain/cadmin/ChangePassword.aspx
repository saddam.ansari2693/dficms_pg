﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="DocCMSMain.cadmin.ChangePassword" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="CadminFooter" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onlyNumbers(event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        jQuery(document).ready(function () {
            var url = window.location;
            if (url.toString().indexOf("?EmailID=") > -1) {
                // Do nothing
            }
            else {
                jQuery("#txtEmailId").val("");
                jQuery("#txtNewPswd").val("");
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Change Password</a> <span class="separator"></span></li>
        <li>Change Your Account Password</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
               Change Password</h5>
            <h1>
                Change Your Account Password</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <div class="widget">
                <h4 class="widgettitle">
                    Password Details</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                   <div class="stdform">
                       <div id="divchangepassword" runat="server" clientidmode="Static">
                            <div class="par control-group">
                                <label class="control-label" for="txtNewPswd">
                                    New Password</label>
                                <div class="controls">
                                    <input type="password" runat="server" name="txtNewPswd" id="txtNewPswd" class="input-large" style="width:40%;" placeholder="New Password"
                                        clientidmode="Static" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="grpsubmit"
                                        ControlToValidate="txtNewPswd" ForeColor="Red">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNewPswd"
                                        ID="RegularExpressionValidator1" ValidationExpression="^[\s\S]{7,}$" ValidationGroup="grpsubmit"
                                        runat="server" ErrorMessage="Password must be 7 characters long." ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>
                                      <div class="par control-group">
                                <label class="control-label" for="txtCnfPswd">
                                    Confirm Password</label>
                                <div class="controls">
                                    <input type="password" runat="server" name="txtCnfPswd" id="txtCnfPswd" style="width:40%;" placeholder="Confirm Password"
                                        class="input-large" clientidmode="Static" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="grpsubmit"
                                        ControlToValidate="txtCnfPswd" ForeColor="Red">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ValidationGroup="grpsubmit"
                                        ErrorMessage="Password must be match" ControlToCompare="txtNewPswd" ControlToValidate="txtCnfPswd"
                                        ForeColor="Red"></asp:CompareValidator>
                                </div>
                            </div>
                            </div>                   
                        </div>
                        <div id="divalert" runat="server" visible="false" style="margin-left:13%">
                        <label runat="server" id="lblMessage" style="color:Red">Password Changed successfully</label>
                        </div>
                        <br />
                        <p class="stdformbutton">
                            <asp:Button ID="btnChangePassword" runat="server" CausesValidation="True" CssClass="btn btn-primary"
                                Text="Change Password" ValidationGroup="grpsubmit" Width="140" OnClick="btnChangePassword_Click" />
                            <asp:Button ID="btnCancelButton" runat="server" CausesValidation="False" CssClass="btn"
                                Text="Cancel" ValidationGroup="grpsubmit" Width="140" OnClick="Cancel_click" />
                        </p>
                    </div>
                 </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
           <uc1:CadminFooter ID="CadminFooter1" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>

