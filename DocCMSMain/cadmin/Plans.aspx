﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="Plans.aspx.cs" Inherits="DocCMSMain.cadmin.Plans" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../js/Pages/media_manager.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../css/Popup.css" rel="stylesheet" type="text/css" />
    <link href="css/colopicker/css/jquery.minicolors.css" rel="stylesheet" type="text/css" />
    <script src="css/colopicker/js/jquery.minicolors.min.js" type="text/javascript"></script>
    <link href="../js/Jcrop/Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../js/Jcrop/Jcrop.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jscolor.js"></script>
    <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <style type="text/css"> 
    .HideDiv 
    {border:1px solid Gray;width:94%;height:30px;margin-top:5px; display:none;
    }
    .ShowDiv 
    {
        border:1px solid Gray;width:94%;height:30px;margin-top:5px; display:block;
    }
   .DivBorder
    {
        border:1px solid Gray;width:94%;height:30px;margin-top:5px;
    }
    </style> 
    <script type="text/javascript">
      jQuery(function () {
            var colpick = jQuery('.demo').each(function () {
                jQuery(this).minicolors({
                    control: jQuery(this).attr('data-control') || 'hue',
                    inline: jQuery(this).attr('data-inline') === 'true',
                    letterCase: 'lowercase',
                    opacity: false,
                    change: function (hex, opacity) {
                        if (!hex) return;
                        if (opacity) hex += ', ' + opacity;
                        try {
                            console.log(hex);
                        } catch (e) { }
                        jQuery(this).select();
                    },
                    theme: 'bootstrap'
                });
            });
            var $inlinehex = jQuery('#inlinecolorhex h3 small');
            jQuery('#inlinecolors').minicolors({
                inline: true,
                theme: 'bootstrap',
                change: function (hex) {
                    if (!hex) return;
                    $inlinehex.html(hex);
                }
            });
        });
        var iClick = 1;
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8 || key==46) {
                return true;
            }
            if (key > 31 && (key < 48 || key > 57))
                return false;
            //             
            return true;
        }
        function ConfirmDelete() {
            if (confirm("Are you sure to delete this feature from the plan?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
       $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $("#txtNavigationUrl").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../cadmin/SubModules.aspx/GetKeyWords",
                        data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }
        function SearchText_btn() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../cadmin/SubModules.aspx/GetKeyWords_btnClick",
                data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                dataType: "json",
                success: function (data) {
                    var dataArray = data.d.toString().split(',');
                    var List = "";
                    for (var i = 0; i < dataArray.length; i++) {
                        List = List + "<li class='ui-menu-item customMenu' role='menuitem' id='liAutoComplete_" + i.toString() + "'>";
                        var NavigateURL = "";
                        if (i == 0) {
                            NavigateURL = dataArray[i].toString().trim().substring(2);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        else if (i == dataArray.length - 1) {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 2);
                        }
                        else {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        if (dataArray.length == 1) {
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        List = List + "<a class='ui-corner-all' tabindex='-1' id='ancAutoComplete_" + i.toString() + "' onclick='SetValueInTextBox(this);'>" + NavigateURL + "</a>";
                        List = List + "</li>";
                    }
                    $("#ulAutocomplete").html(List);
                    $("#ulAutocomplete").show();

                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        function SetValueInTextBox(ctrlID) {
            $("#txtNavigationUrl").val($("#" + ctrlID.id).text());
            $("#ulAutocomplete").hide();
        }
    </script>

          <script type="text/javascript">
               jQuery(document).ready(function () {
                  var jcrop_api;
                  var url = window.location;
                  if (url.toString().indexOf("?EmailID=") > -1) {
                      // Do nothing
                  }
                  else {
                      jQuery("#txtEmailId").val("");
                      jQuery("#txtNewPswd").val("");
                  }
                  OnPageLoad();
                  jQuery("#chkResizeImageCreate").click(function () {
                      if (jQuery(this).is(':checked')) {
                          jQuery("#chkCustomeImageCreate").prop("checked", false);
                          jQuery("#divCustomResizeImageCreate").show();
                          jQuery("#divCustomImageCreate").hide();
                          jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                          jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                          CheckResizeValues();
                      } else {
                          jQuery("#chkCustomeImageCreate").prop("checked", true);
                          jQuery("#divCustomResizeImageCreate").hide();
                          jQuery("#divCustomImageCreate").show();
                          jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
                          jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
                      }
                  });

                  jQuery("#chkCustomeImageCreate").click(function () {//
                      if (jQuery(this).is(':checked')) {
                          jQuery("#chkResizeImageCreate").prop("checked", false);
                          jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
                          jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
                          jQuery("#divCustomImageCreate").show();
                          jQuery("#divCustomResizeImageCreate").hide();
                          var MiddleImageWidth = parseInt(jQuery("#hdnActualImageWidth").val()) / 6;
                          var MiddleImageHeight = parseInt(jQuery("#hdnActualImageHeight").val()) / 6;
                          var MarginTopAndBottom = parseInt(200 - MiddleImageHeight) / 2;
                          jQuery("#ImgCustomeImagePreviewRunning").css("margin-top", parseInt(MarginTopAndBottom));
                          jQuery("#ImgCustomeImagePreviewRunning").css("margin-bottom", parseInt(MarginTopAndBottom));
                          jQuery("#ImgCustomeImagePreviewRunning").css("width", parseInt(MiddleImageWidth));
                          jQuery("#ImgCustomeImagePreviewRunning").css("height", parseInt(MiddleImageHeight));
                      } else {
                          jQuery("#chkResizeImageCreate").prop("checked", true);
                          jQuery("#divCustomImageCreate").hide();
                          jQuery("#divCustomResizeImageCreate").show();
                          jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                          jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                      }
                  });

                  jQuery("#chkFinalCrop").click(function () {//
                      if (jQuery(this).is(':checked')) {
                          Set_Cropper(1);
                          jQuery("#btnCropWithResize").show();
                      } else {
                          Set_Cropper(0);
                          jQuery("#btnCropWithResize").hide();
                      }
                  });

                  jQuery("#chkAspectRatioResize").click(function () {//
                      if (jQuery(this).is(':checked')) {
                          jQuery("#hdnResizeAsepctRatio").val("True");
                          setTimeout(function () { PreviewResizedImage(); }, 200);
                          jQuery("#uniform-chkAspectRatioResize").find("span").addClass("checked");

                      } else {
                          jQuery("#hdnResizeAsepctRatio").val("False");
                          jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
                          setTimeout(function () { PreviewResizedImage(); }, 200);
                      }
                  });

                  jQuery("#chkFinalCrop").click(function () {//
                      if (jQuery(this).is(':checked')) {
                          Set_Cropper(1);
                          jQuery("#btnCropWithResize").show();
                      } else {
                          Set_Cropper(0);
                          jQuery("#btnCropWithResize").hide();
                      }
                  });
              });

              function OnPageLoad() {
                  jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                  jQuery("#chkResizeImageCreate").prop("checked", true);
                  jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                  jQuery("#chkCustomeImageCreate").prop("checked", false);
                  jQuery("#txtCustomWidth").val("1800");
                  jQuery("#txtCustomHeight").val("1200");
                  jQuery("#chkAspectRatioResize").prop("checked", false);
                  jQuery("#hdnResizeAsepctRatio").val("False");
                  jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
              }

              //-------image------
              function ColorChange(Textbox) {
                  var colorName = jQuery("#" + Textbox.id).val();
                  var Image = jQuery("#ImgResizeImage").attr("src");
                  jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
                  jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
              }

              function ManageAspectRatio(ratioparam, e) {
                  var key;
                  if (window.event) {
                      key = window.event.keyCode;     //IE
                  }
                  else {
                      key = e.which;      //firefox              
                  }
                  if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
                      if (jQuery("#chkAspectRatioResize").is(':checked')) {
                          if (ratioparam == "w") {
                              var width = jQuery("#txtCustomWidth").val();
                              if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                                  jQuery("#txtCustomWidth").css("color", "Red");
                                  jQuery("#txtCustomHeight").val("0");
                              }
                              else {
                                  var height = parseFloat(jQuery("#txtCustomWidth").val()) / 1.333;
                                  jQuery("#txtCustomHeight").val(parseInt(height));
                              }
                          }
                          else {
                              var height = jQuery("#txtCustomHeight").val();
                              if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                                  jQuery("#txtCustomHeight").css("color", "Red");
                                  jQuery("#txtCustomWidth").val("0");
                              }
                              else {
                                  var width = parseFloat(jQuery("#txtCustomHeight").val()) * 1.333;
                                  jQuery("#txtCustomWidth").val(parseInt(width));
                              }
                          }
                      }
                      else {
                          jQuery("#ImgResizePreview").Jcrop({
                              onSelect: SelectCropArea,
                              bgFade: true

                          }, function () {
                              jcrop_api = this;
                          });
                          jcrop_api.destroy();
                          PreviewResizedImage();
                          jQuery("#ImgResizePreview").css("visibility", "visible");
                      }
                  }
                  else {
                      return false;
                  }
              }
             function CheckResizeValues() {
                  setTimeout(function () { PreviewResizedImage(); }, 200);
              }

              function PreviewResizedImage() {
                  var actWidth = jQuery("#hdnActualImageWidth").val();
                  var actHeight = jQuery("#hdnActualImageHeight").val();
                  var resizeWidth = jQuery("#txtCustomWidth").val();
                  var resizeHeight = jQuery("#txtCustomHeight").val();
                  if (jQuery("#hdnResizeAsepctRatio").val() == "True") {
                      var ratioX = parseInt(resizeWidth / 6) / parseInt(actWidth / 6);
                      var ratioY = parseInt(resizeHeight / 6) / parseInt(actHeight / 6);
                      var ratio = Math.max(ratioX, ratioY);
                      var newWidth = parseInt(parseInt(resizeWidth / 6) * ratio);
                      var newHeight = parseInt(parseInt(resizeHeight / 6) * ratio);
                      newWidth = parseInt(resizeWidth / 6);
                      newHeight = parseInt(resizeHeight / 6);
                      jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                      jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                      jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                      jQuery("#ImgResizePreview").css("max-width", "none");
                  }
                  else {
                      var newWidth = parseInt(resizeWidth / 6);
                      var newHeight = parseInt(resizeHeight / 6);
                      jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                      jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                      jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                      jQuery("#ImgResizePreview").css("max-width", "none");
                  }
                  CheckImageFits(newHeight, newWidth);
              }

              function CheckImageFits(newHeight, newWidth) {
                  var RequiredWidth = parseInt(jQuery("#divResizePreview").css("width"));
                  var Requiredheight = parseInt(jQuery("#divResizePreview").css("height"));
                  if ((RequiredWidth < newWidth) || (Requiredheight < newHeight)) {
                      jQuery("#lblUnfitMsg").show();
                      jQuery("#spnFinalCrop").show();
                      jQuery("#btnCropWithResize").show();
                      jQuery("#btnResizeImage").hide();
                  }
                  else {
                      jQuery("#lblUnfitMsg").hide();
                      jQuery("#spnFinalCrop").hide();
                      jQuery("#btnResizeImage").show();
                      jQuery("#btnCropWithResize").hide();
                  }
                  return false;
              }

              //Ready Function Close

              function Set_Cropper(setVal) {
                  if (setVal == 1) {
                      var reqsize = jQuery("#ddlSizeResize").val().split('x');
                      var orientation = jQuery("#ddlOrientationResize").val();
                      var cropWidth = 0;
                      var cropHeight = 0;
                      var CropSize = "";
                      var cropHeight = 0;
                      if (orientation == "Portrait") {
                          cropWidth = (parseInt(reqsize[0]) * 300) / 6;
                          cropHeight = (parseInt(reqsize[1]) * 300) / 6
                          CropSize = reqsize[0] + ' x ' + reqsize[1];
                      }
                      else {
                          cropWidth = (parseInt(reqsize[1]) * 300) / 6;
                          cropHeight = (parseInt(reqsize[0]) * 300) / 6
                          CropSize = reqsize[1] + ' x ' + reqsize[0];
                      }
                      jQuery("#maxHeight").val(cropHeight.toString());
                      jQuery("#maxWidth").val(cropWidth.toString());
                      var imagewidth = parseInt(jQuery("#txtCustomWidth").val());
                      var imageHeight = parseInt(jQuery("#txtCustomHeight").val());
                      jQuery("#ImgResizePreview").Jcrop({
                          onSelect: SelectCropArea,
                          onChange: SelectCropArea,
                          bgFade: true
                          }, function () {
                          jcrop_api = this;
                          jcrop_api.setOptions({ allowSelect: true });
                          jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
                          jcrop_api.animateTo([10, 20, ((imagewidth - cropWidth) / 2) + cropWidth, ((imageHeight - cropHeight) / 2) + cropHeight]);
                          // Use the API to get the real image size
                          jcrop_api.focus();
                          var bounds = this.getBounds();
                          boundx = bounds[0];
                          boundy = bounds[1];
                      });
                  }
                  else {
                      jcrop_api.destroy();
                      jQuery("#ImgResizePreview").css("visibility", "visible");
                  }
              }


              var boundx, boundy;
              function SelectCropArea(c) {
                  // Grab some information about the preview pane            
                  var preview = jQuery('#preview-pane');
                  var pcnt = jQuery('#preview-pane .preview-container');
                  var pimg = jQuery('#preview-pane .preview-container img');
                  var xsize = pcnt.width(),
                  ysize = pcnt.height();
                  jQuery('#XAxis').val(parseInt(c.x));
                  jQuery('#YAxis').val(parseInt(c.y));
                  jQuery('#Width').val(parseInt(c.w));
                  jQuery('#Height').val(parseInt(c.h));
                  jQuery('#x1').val(c.x);
                  jQuery('#y1').val(c.y);
                  jQuery('#x2').val(c.x2);
                  jQuery('#y2').val(c.y2);
                  jQuery('#w').val(c.w);
                  jQuery('#h').val(c.h);
                  if (parseInt(c.w) > 0) {
                      var rx = xsize / c.w;
                      var ry = ysize / c.h;
                      pimg.css({
                          width: Math.round(rx * boundx) + 'px',
                          height: Math.round(ry * boundy) + 'px',
                          marginLeft: '-' + Math.round(rx * c.x) + 'px',
                          marginTop: '-' + Math.round(ry * c.y) + 'px'
                      });
                  }
              }
              function SaveChanges() {
                  ResizeImageUsingByCrop();
                  modal.style.display = "none";
                  jcrop_api.destroy();
              }


              function SetReSizeValues(paramval) {
                  jQuery("#ImgResizePreview").Jcrop({
                      onSelect: SelectCropArea,
                      onChange: SelectCropArea,
                      bgFade: true

                  }, function () {
                      jcrop_api = this;
                  });
                  jcrop_api.destroy();
                  jQuery("#ImgResizePreview").css("visibility", "visible");

                  var AutoSize = "";
                  var reqsize = "";
                  var orientation = "";
                  var ResizeWidth = 0;
                  var ResizeHeight = 0;
                  if (paramval == "S") {
                      AutoSize = jQuery("#ddlSizeResize").val();
                      reqsize = jQuery("#ddlSizeResize").val().split('x');
                      orientation = jQuery("#ddlOrientationResize").val();
                      if (orientation == "Portrait") {
                          ResizeWidth = parseInt(reqsize[0]) * 300;
                          ResizeHeight = parseInt(reqsize[1]) * 300;
                      }
                      else {
                          ResizeWidth = parseInt(reqsize[1]) * 300;
                          ResizeHeight = parseInt(reqsize[0]) * 300;
                      }
                  }
                  else {
                      if (orientation == "Portrait") {
                          ResizeWidth = jQuery("#txtCustomWidth").val();
                          ResizeHeight = jQuery("#txtCustomHeight").val();
                      }
                      else {
                          ResizeWidth = jQuery("#txtCustomHeight").val();
                          ResizeHeight = jQuery("#txtCustomWidth").val();
                      }
                  }
                  if (orientation == "Portrait") {
                      var previewHeight = jQuery("#divResizePreview").css("height");
                      var previewwidth = jQuery("#divResizePreview").css("width");
                      jQuery("#divResizePreview").css("height", previewwidth);
                      jQuery("#divResizePreview").css("width", previewHeight);
                      jQuery("#txtCustomHeight").val(ResizeHeight);
                      jQuery("#txtCustomWidth").val(ResizeWidth);
                  }
                  else {
                      var previewHeight = jQuery("#divResizePreview").css("height");
                      var previewwidth = jQuery("#divResizePreview").css("width");
                      jQuery("#divResizePreview").css("height", previewwidth);
                      jQuery("#divResizePreview").css("width", previewHeight);
                      jQuery("#txtCustomHeight").val(ResizeHeight);
                      jQuery("#txtCustomWidth").val(ResizeWidth);
                  }
                  setTimeout(function () { PreviewResizedImage(); }, 200);
                  setTimeout(function () {
                      if (jQuery("#chkFinalCrop").prop("checked") == true) {
                          Set_Cropper(1);
                      }
                  }, 400);
              }

              //Crop Image With Resize Image
              function CropImageWithResize() {
                  debugger;
                  jQuery.ajax({
                      url: "../WebService/DocCMSApi.svc/CropImageWithResize",
                      contentType: 'application/json; charset=utf-8',
                      data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'ImageHeight': jQuery("#txtCustomHeight").val(), 'ImageWidth': jQuery("#txtCustomWidth").val(), 'XAxis': document.getElementById('XAxis').value, 'YAxis': document.getElementById('YAxis').value, 'CropImageWidth': document.getElementById('Width').value, 'CropImageHeight': document.getElementById('Height').value, 'BlogId': jQuery('#hndBlogId').val(), 'userID': jQuery('#hdnCurUserID').val() },
                      dataType: "json",
                      async: false,
                      success: function (data) {
                          if (data.FileName == null) {
                              alert("Please Crop Image");
                          }
                          else {
                              location.reload();
                          }
                      },
                      error: function (result) {
                          alert("No Match");
                      }
                  });
              }

              function ResizeImage() {
                  debugger;
                  jQuery.ajax({
                      url: "../WebService/DocCMSApi.svc/ResizeImage",
                      contentType: 'application/json; charset=utf-8',
                      data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'RatioStatus': document.getElementById('hdnResizeAsepctRatio').value, 'ImageHeight': document.getElementById('txtCustomHeight').value, 'ImageWidth': document.getElementById('txtCustomWidth').value, "pageID": jQuery("#hdnCurPageID").val(), "CMSID": jQuery("#hdnCurCMSID").val() },
                      dataType: "json",
                      success: function (data) {
                          if (data != null && data != undefined && data != "") {
                              location.reload();
                          }
                          else {
                              //Start Save Image Preview//
                              jQuery("#divFinalPreview").hide();
                              jQuery("#divCropFinalImage").hide();
                              jQuery("#imgGetHeightWidth").attr("src", "");
                              jQuery("#dZUpload").show();
                          }
                      },
                      error: function (result) {
                          alert("No Match");
                      }
                  });
              }


              // Custome Image Create
              function CustomeImage() {
                  var ImageNameArray = jQuery("#ImgResizeImage").attr("src").split('/');
                  var ImageName = ImageNameArray[ImageNameArray.length - 1];
                  var Color = jQuery('#txtFontColor').val();
                  var hex = parseInt(Color.substring(1), 16);
                  var r = (hex & 0xff0000) >> 16;
                  var g = (hex & 0x00ff00) >> 8;
                  var b = hex & 0x0000ff;
                  jQuery.ajax({
                      type: "GET",
                      url: "../WebService/DocCMSApi.svc/Custome_Image_Create",
                      data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'R': r, 'G': g, 'B': b, 'BlogID': jQuery('#hndBlogId').val(), 'userID': jQuery('#hdnCurUserID').val() },
                      dataType: "json",
                      async: false,
                      success: function (data) {
                          if (data.FileName != null && data.FileName != undefined) {
                              location.reload();
                              }
                          else {
                              jQuery("#ImgCustomeImagePreview").attr("src", "");
                              jQuery("#ImgCustomeImagePreview").hide();
                              jQuery("#imgFinalPreview").attr("src", "");
                              jQuery("#EditImageOnModelPopup").attr("src", "");
                              jQuery("#divFinalPreview").hide();
                              jQuery("#divCropFinalImage").hide();
                              jQuery("#dZUpload").show();
                              modalResize.style.display = "block";
                              jQuery("#imgGetHeightWidth").attr("src", "");
                              jQuery("#btnPostImage").prop("disabled", true);
                          }
                      },
                      error: function (result) {
                          alert("No Match");
                      }
                  });
              }
              //-------image------

              function ColorChange(Textbox) {
                  var colorName = jQuery("#" + Textbox.id).val();
                  var Image = jQuery("#ImgResizeImage").attr("src");
                  jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
                  jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
              }

              function ManageAspectRatio(ratioparam, e) {
                  var key;
                  if (window.event) {
                      key = window.event.keyCode;     //IE
                  }
                  else {
                      key = e.which;      //firefox              
                  }
                  if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
                      if (jQuery("#chkAspectRatioResize").is(':checked')) {
                          if (ratioparam == "w") {
                              var width = jQuery("#txtCustomWidth").val();
                              if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                                  jQuery("#txtCustomWidth").css("color", "Red");
                                  jQuery("#txtCustomHeight").val("0");
                              }
                              else {
                                  var height = parseFloat(jQuery("#txtCustomWidth").val()) / 1.333;
                                  jQuery("#txtCustomHeight").val(parseInt(height));
                              }
                          }
                          else {
                              var height = jQuery("#txtCustomHeight").val();
                              if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                                  jQuery("#txtCustomHeight").css("color", "Red");
                                  jQuery("#txtCustomWidth").val("0");
                              }
                              else {
                                  var width = parseFloat(jQuery("#txtCustomHeight").val()) * 1.333;
                                  jQuery("#txtCustomWidth").val(parseInt(width));
                              }
                          }
                      }
                      else {
                          jQuery("#ImgResizePreview").Jcrop({
                              onSelect: SelectCropArea,
                              bgFade: true

                          }, function () {
                              jcrop_api = this;
                          });
                          jcrop_api.destroy();
                          PreviewResizedImage();
                          jQuery("#ImgResizePreview").css("visibility", "visible");
                      }
                  }
                  else {
                      return false;
                  }
              }

              function CheckResizeValues() {
                  setTimeout(function () { PreviewResizedImage(); }, 200);
              }

              function PreviewResizedImage() {
                  var actWidth = jQuery("#hdnActualImageWidth").val();
                  var actHeight = jQuery("#hdnActualImageHeight").val();
                  var resizeWidth = jQuery("#txtCustomWidth").val();
                  var resizeHeight = jQuery("#txtCustomHeight").val();
                  if (jQuery("#hdnResizeAsepctRatio").val() == "True") {
                      var ratioX = parseInt(resizeWidth / 6) / parseInt(actWidth / 6);
                      var ratioY = parseInt(resizeHeight / 6) / parseInt(actHeight / 6);
                      var ratio = Math.max(ratioX, ratioY);
                      var newWidth = parseInt(parseInt(resizeWidth / 6) * ratio);
                      var newHeight = parseInt(parseInt(resizeHeight / 6) * ratio);
                      newWidth = parseInt(resizeWidth / 6);
                      newHeight = parseInt(resizeHeight / 6);
                      jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                      jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                      jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                      jQuery("#ImgResizePreview").css("max-width", "none");
                  }
                  else {
                      var newWidth = parseInt(resizeWidth / 6);
                      var newHeight = parseInt(resizeHeight / 6);
                      jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                      jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                      jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                      jQuery("#ImgResizePreview").css("max-width", "none");
                  }
                  CheckImageFits(newHeight, newWidth);
              }

              function CheckImageFits(newHeight, newWidth) {
                  var RequiredWidth = parseInt(jQuery("#divResizePreview").css("width"));
                  var Requiredheight = parseInt(jQuery("#divResizePreview").css("height"));
                  if ((RequiredWidth < newWidth) || (Requiredheight < newHeight)) {
                      jQuery("#lblUnfitMsg").show();
                      jQuery("#spnFinalCrop").show();
                      jQuery("#btnCropWithResize").show();
                      jQuery("#btnResizeImage").hide();
                  }
                  else {
                      jQuery("#lblUnfitMsg").hide();
                      jQuery("#spnFinalCrop").hide();
                      jQuery("#btnResizeImage").show();
                      jQuery("#btnCropWithResize").hide();
                  }
                  return false;
              }

              //Ready Function Close
              function Set_Cropper(setVal) {
                  if (setVal == 1) {
                      var reqsize = jQuery("#ddlSizeResize").val().split('x');
                      var orientation = jQuery("#ddlOrientationResize").val();
                      var cropWidth = 0;
                      var cropHeight = 0;
                      var CropSize = "";
                      var cropHeight = 0;
                      if (orientation == "Portrait") {
                          cropWidth = (parseInt(reqsize[0]) * 300) / 6;
                          cropHeight = (parseInt(reqsize[1]) * 300) / 6
                          CropSize = reqsize[0] + ' x ' + reqsize[1];
                      }
                      else {
                          cropWidth = (parseInt(reqsize[1]) * 300) / 6;
                          cropHeight = (parseInt(reqsize[0]) * 300) / 6
                          CropSize = reqsize[1] + ' x ' + reqsize[0];
                      }
                      jQuery("#maxHeight").val(cropHeight.toString());
                      jQuery("#maxWidth").val(cropWidth.toString());
                      var imagewidth = parseInt(jQuery("#txtCustomWidth").val());
                      var imageHeight = parseInt(jQuery("#txtCustomHeight").val());
                      jQuery("#ImgResizePreview").Jcrop({
                          onSelect: SelectCropArea,
                          onChange: SelectCropArea,
                          bgFade: true
                      
                      }, function () {
                          jcrop_api = this;
                          jcrop_api.setOptions({ allowSelect: true });
                          jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
                          jcrop_api.animateTo([10, 20, ((imagewidth - cropWidth) / 2) + cropWidth, ((imageHeight - cropHeight) / 2) + cropHeight]);
                          // Use the API to get the real image size
                          jcrop_api.focus();
                          var bounds = this.getBounds();
                          boundx = bounds[0];
                          boundy = bounds[1];
                      });
                  }
                  else {
                      jcrop_api.destroy();
                      jQuery("#ImgResizePreview").css("visibility", "visible");
                  }
              }


              var boundx, boundy;
              function SelectCropArea(c) {
                  // Grab some information about the preview pane            
                  var preview = jQuery('#preview-pane');
                  var pcnt = jQuery('#preview-pane .preview-container');
                  var pimg = jQuery('#preview-pane .preview-container img');
                  var xsize = pcnt.width(),
                  ysize = pcnt.height();
                  jQuery('#XAxis').val(parseInt(c.x));
                  jQuery('#YAxis').val(parseInt(c.y));
                  jQuery('#Width').val(parseInt(c.w));
                  jQuery('#Height').val(parseInt(c.h));
                  jQuery('#x1').val(c.x);
                  jQuery('#y1').val(c.y);
                  jQuery('#x2').val(c.x2);
                  jQuery('#y2').val(c.y2);
                  jQuery('#w').val(c.w);
                  jQuery('#h').val(c.h);
                  if (parseInt(c.w) > 0) {
                      var rx = xsize / c.w;
                      var ry = ysize / c.h;

                      pimg.css({
                          width: Math.round(rx * boundx) + 'px',
                          height: Math.round(ry * boundy) + 'px',
                          marginLeft: '-' + Math.round(rx * c.x) + 'px',
                          marginTop: '-' + Math.round(ry * c.y) + 'px'
                      });
                  }
              }
              function SaveChanges() {
                  ResizeImageUsingByCrop();
                  modal.style.display = "none";
                  jcrop_api.destroy();
              }


              function SetReSizeValues(paramval) {
                  jQuery("#ImgResizePreview").Jcrop({
                      onSelect: SelectCropArea,
                      onChange: SelectCropArea,
                      bgFade: true

                  }, function () {
                      jcrop_api = this;
                  });
                  jcrop_api.destroy();
                  jQuery("#ImgResizePreview").css("visibility", "visible");
                  var AutoSize = "";
                  var reqsize = "";
                  var orientation = "";
                  var ResizeWidth = 0;
                  var ResizeHeight = 0;
                  if (paramval == "S") {
                      AutoSize = jQuery("#ddlSizeResize").val();
                      reqsize = jQuery("#ddlSizeResize").val().split('x');
                      orientation = jQuery("#ddlOrientationResize").val();
                      if (orientation == "Portrait") {
                          ResizeWidth = parseInt(reqsize[0]) * 300;
                          ResizeHeight = parseInt(reqsize[1]) * 300;
                      }
                      else {
                          ResizeWidth = parseInt(reqsize[1]) * 300;
                          ResizeHeight = parseInt(reqsize[0]) * 300;
                      }
                  }
                  else {
                      if (orientation == "Portrait") {
                          ResizeWidth = jQuery("#txtCustomWidth").val();
                          ResizeHeight = jQuery("#txtCustomHeight").val();
                      }
                      else {
                          ResizeWidth = jQuery("#txtCustomHeight").val();
                          ResizeHeight = jQuery("#txtCustomWidth").val();
                      }
                  }
                  if (orientation == "Portrait") {
                      var previewHeight = jQuery("#divResizePreview").css("height");
                      var previewwidth = jQuery("#divResizePreview").css("width");
                      jQuery("#divResizePreview").css("height", previewwidth);
                      jQuery("#divResizePreview").css("width", previewHeight);
                      jQuery("#txtCustomHeight").val(ResizeHeight);
                      jQuery("#txtCustomWidth").val(ResizeWidth);
                  }
                  else {
                      var previewHeight = jQuery("#divResizePreview").css("height");
                      var previewwidth = jQuery("#divResizePreview").css("width");
                      jQuery("#divResizePreview").css("height", previewwidth);
                      jQuery("#divResizePreview").css("width", previewHeight);
                      jQuery("#txtCustomHeight").val(ResizeHeight);
                      jQuery("#txtCustomWidth").val(ResizeWidth);
                  }
                  setTimeout(function () { PreviewResizedImage(); }, 200);
                  setTimeout(function () {
                      if (jQuery("#chkFinalCrop").prop("checked") == true) {
                          Set_Cropper(1);
                      }
                  }, 400);
              }

              //Crop Image With Resize Image
              function CropImageWithResize() {
                  debugger;
                  jQuery.ajax({
                      url: "../WebService/DocCMSApi.svc/CropImageWithResize",
                      contentType: 'application/json; charset=utf-8',
                      data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'ImageHeight': jQuery("#txtCustomHeight").val(), 'ImageWidth': jQuery("#txtCustomWidth").val(), 'XAxis': document.getElementById('XAxis').value, 'YAxis': document.getElementById('YAxis').value, 'CropImageWidth': document.getElementById('Width').value, 'CropImageHeight': document.getElementById('Height').value, 'BlogId': jQuery('#hndBlogId').val(), 'userID': jQuery('#hdnCurUserID').val() },
                      dataType: "json",
                      async: false,
                      success: function (data) {
                          if (data.FileName == null) {
                              alert("Please Crop Image");
                          }
                          else {
                              location.reload();
                          }
                      },
                      error: function (result) {
                          alert("No Match");
                      }
                  });
              }

              function ResizeImage() {
                  debugger;
                  jQuery.ajax({
                      url: "../WebService/DocCMSApi.svc/ResizeImage",
                      contentType: 'application/json; charset=utf-8',
                      data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'RatioStatus': document.getElementById('hdnResizeAsepctRatio').value, 'ImageHeight': document.getElementById('txtCustomHeight').value, 'ImageWidth': document.getElementById('txtCustomWidth').value, "pageID": jQuery("#hdnCurPageID").val(), "CMSID": jQuery("#hdnCurCMSID").val() },
                      dataType: "json",
                      success: function (data) {
                          if (data != null && data != undefined && data != "") {
                              location.reload();
                          }
                          else {
                              //Start Save Image Preview//
                              jQuery("#divFinalPreview").hide();
                              jQuery("#divCropFinalImage").hide();
                              jQuery("#imgGetHeightWidth").attr("src", "");
                              jQuery("#dZUpload").show();
                          }
                      },
                      error: function (result) {
                          alert("No Match");
                      }
                  });
              }


              // Custome Image Create
              function CustomeImage() {
                  var ImageNameArray = jQuery("#ImgResizeImage").attr("src").split('/');
                  var ImageName = ImageNameArray[ImageNameArray.length - 1];
                  var Color = jQuery('#txtFontColor').val();
                  var hex = parseInt(Color.substring(1), 16);
                  var r = (hex & 0xff0000) >> 16;
                  var g = (hex & 0x00ff00) >> 8;
                  var b = hex & 0x0000ff;
                  jQuery.ajax({
                      type: "GET",
                      url: "../WebService/DocCMSApi.svc/Custome_Image_Create",
                      data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'R': r, 'G': g, 'B': b, 'BlogID': jQuery('#hndBlogId').val(), 'userID': jQuery('#hdnCurUserID').val() },
                      dataType: "json",
                      async: false,
                      success: function (data) {
                          if (data.FileName != null && data.FileName != undefined) {
                              location.reload();
                          }
                          else {
                              jQuery("#ImgCustomeImagePreview").attr("src", "");
                              jQuery("#ImgCustomeImagePreview").hide();
                              jQuery("#imgFinalPreview").attr("src", "");
                              jQuery("#EditImageOnModelPopup").attr("src", "");
                              jQuery("#divFinalPreview").hide();
                              jQuery("#divCropFinalImage").hide();
                              jQuery("#dZUpload").show();
                              modalResize.style.display = "block";
                              jQuery("#imgGetHeightWidth").attr("src", "");
                              jQuery("#btnPostImage").prop("disabled", true);
                          }
                      },
                      error: function (result) {
                          alert("No Match");
                      }
                  });
              }

              function SetTextImageValues(cntrl) {
                  jQuery("#ImgResizePreview").Jcrop({
                      onSelect: SelectCropArea,
                      onChange: SelectCropArea,
                      bgFade: true

                  }, function () {
                      jcrop_api = this;
                  });
                  jcrop_api.destroy();
                  jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                  jQuery("#ImgResizePreview").css('visibility', 'visible');
                  switch (cntrl) {
                      case "Left Picture":
                          jQuery("#txtCustomWidth").val("373");
                          jQuery("#txtCustomHeight").val("280");
                          CheckResizeValues();
                          break;
                      case "Center Picture":
                          jQuery("#txtCustomWidth").val("373");
                          jQuery("#txtCustomHeight").val("280");
                          CheckResizeValues();
                          break;
                      case "Right Picture":
                          jQuery("#txtCustomWidth").val("373");
                          jQuery("#txtCustomHeight").val("280");
                          CheckResizeValues();
                          break;
                      case "Full Picture":
                          jQuery("#txtCustomWidth").val("1119");
                          jQuery("#txtCustomHeight").val("840");
                          CheckResizeValues();
                          break;
                      default:
                          jQuery("#txtCustomWidth").val("1800");
                          jQuery("#txtCustomHeight").val("1200");
                          CheckResizeValues();
                          break;
                  }
              }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Plans</a> <span class="separator"></span></li>
        <li>Manage Plans</li>

    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Plans</h5>
            <h1>
                Manage Plans</h1>
        </div>
    </div>
      <!---------------------Resize Section--------------------------->
      <div id="divModelResize" class="modal" style="height: auto% !important;max-width:auto% !important; " >
          <div class="modal-content" style="height: auto !important;max-width:auto !important;" >
               <span class="ResizeHeader">Resize Your Image to get a best fit result</span>
                   <span class="cross" onclick="CloseImageResizeModel();">×</span>
                       <div style="width:auto !important;">
                <img src="" id="ImgResizeImage" style="width:auto;height:auto"/>
                <div style="width: 100%;">
                        Resize Image Option  <input type="checkbox" id="chkResizeImageCreate"/>
                        Create Custom Image <input type="checkbox" id="chkCustomeImageCreate"/>
                       <div  id="divCustomResizeImageCreate">
                        <input type="checkbox" id="chkAspectRatioResize" checked="checked" />
                        Keep Aspect Ratio<br />
                        Image Width :<input type="text" id="txtCustomWidth" placeholder="Width in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('w',event);" style="width:18%"/>
                        Image Height :<input type="text" id="txtCustomHeight" placeholder="Height in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('h',event);" style="width:18%"/>
                    <div id="divResizeSection">
                        Select Image Size<select id="ddlSizeResize" style="width: 90px; height: 36px;" onchange="SetReSizeValues('S');">                            
                            <option value="4x6">4x6</option>
                        </select>
                        Select Orientation<select id="ddlOrientationResize" style="width: 150px; height: 36px;"  onchange="SetReSizeValues('O');">                            
                            <option value="Landscape">Landscape</option>
                            <option value="Portrait">Portrait</option>
                        </select>
                          Page Text Image<select id="ddlPageTextImage" style="width: 150px; height: 36px;"  onchange="SetTextImageValues(this.value);">                            
                            <option value="Left Picture">Left Picture</option>
                            <option value="Center Picture">Center Picture</option>
                             <option value="Right Picture">Right Picture</option>
                            <option value="Full Picture">Full Picture</option>
                        </select>
                    </div>
                   <input type="button" id="btnResizeImage" class="btn btn-danger btn-lg" value="Resize" onclick="ResizeImage();"
                    style="margin-top:10px; margin-bottom:10px;" />
                          <span id="spnFinalCrop" style="display:none;"><span style="color:Red;font-weight:400;">The previewed image exceeds the required dimensions.You need to crop the image to get the required dimensions.</span><br /><input type="checkbox" id="chkFinalCrop"/>Crop Image
                        <input type="button" class="btn btn-danger btn-lg" style="display:none;" id="btnCropWithResize" value="Crop Image" onclick="CropImageWithResize();" />
                        </span>
               <input type="button" id="Button1" class="btn btn-danger btn-lg" value="Preview" onclick="PreviewResizedImage();"
                    style="margin-top:10px; margin-bottom:10px;display:none;"  />
                <asp:HiddenField ID="hdnResizeAsepctRatio" runat="server" ClientIDMode="Static" Value="True"/>
                 <div id="divResizePreviewPane" style="margin-left:20px; margin-bottom :20px;">
                <span ><b>Image Preview</b></span>
                <div class="preview-container" id="divResizePreview"  style="width:300px; height:200px;">
                    <img src="" id="ImgResizePreview" alt="Preview" />
                </div>
            </div>
            </div>
            </div>
             </div>
             <div id="divCustomImageCreate" style="display:none;">
             Select Background Image Color:
             <asp:TextBox ID="txtFontColor" runat="server" CssClass="input-large demo" ClientIDMode="Static" style=" width:300px; height:24px;margin-bottom: 0;"
                                     placeholder="Enter Font Color" onchange="ColorChange(this)"></asp:TextBox>
             <br />
               <input type="button" id="btncustomeImage" class="btn btn-danger btn-lg" value="Create Image" onclick="CustomeImage();"
                    style="margin-top:10px; margin-bottom:10px;"  />
                 <div id="divCustomImagePreviewRunning" style="width:300px; height:200px; background-color:#FFFFFF;margin-left:5%;margin-bottom:10px;">
                 <center>
                 <img src="" id="ImgCustomeImagePreviewRunning" />
                 </center>
                </div>
               </div>
             </div>
            <div id="Div4" runat="server" clientidmode="Static"></div>
                        </div>
      <!---------------------Resize Section--------------------------->
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner" style="padding: 20px 20px 100px;">
            <div class="widget">
                <h4 class="widgettitle">
                    Plans Details</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform">
                        <div class="par control-group">
                            <label class="control-label" for="txtPlanName">
                                Plan Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtPlanName" id="txtPlanName" class="input-large"
                                    clientidmode="Static" placeholder="Enter Plan Name" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtPlanName" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtDescription">
                                Plane Description</label>
                            <div class="controls">
                                <textarea id="txtDescription" name="txtDescription" rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="height: 100px; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" placeholder="Enter Plan Description"></textarea>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtDescription" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtPrice">
                                Price</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtPrice" id="txtPrice" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Price" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtPrice" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group" id="DivUpload" runat="server" clientidmode="Static">
                            <label class="control-label">
                                Select Image
                            </label>
                            <div class="controls">
                                <asp:FileUpload ID="ImageUploadControl" runat="server" ClientIDMode="Static" CssClass="input-upload" />
                                <div style="margin-left: 35%; margin-top: -2%; width: 80px;">
                                    <img alt="" src="../UploadedFiles/No_image_available.png" runat="server" id="imgLogo"
                                        style="width: 80px; height: 50px; margin-top: -20px;" clientidmode="Static" />
                                </div>
                                <div style="margin-left: 41%">
                                   <img src="../images/Edit-image.png" style="width:24px" title="Edit Image" onclick="EditImage(this.id);" runat="server" id="imgEdit" />
                                </div>
                                <asp:Label runat="server" ID="lblFileUploader" ClientIDMode="Static" Style="display: none;"></asp:Label>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                                Display Order</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtDisplayOrder" id="txtDisplayOrder" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Display Order Number" />
                                <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                    Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click" />
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    style="margin-top:5px;opacity: 1" checked />
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="tblFeature">
                                Features</label>
                            <div class="controls">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static" >
                             <ContentTemplate>
                                <table id="Table1" class="table table-bordered responsive " style="margin-left: 150px;
                                    width: 55%;">
                                    <colgroup>
                                        <col class="con1" style="align: center; width: 50%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="head1" style="height: 15px;">
                                                <div style="display: inline; cursor: pointer;" class="AddFeature">
                                                <asp:ImageButton ID="imgAddfeature" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/plus-icon.png" ToolTip="Add Features" 
                                                        onclick="imgAddfeature_Click"/>
                                                         Add Feature</div>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="gradeX">
                                            <td>
                                            <div style="min-height:250px;overflow-y:auto;" id="divMain" runat="server" clientidmode="Static">
                                               <div id="divFeature_1" class="right-side DivBorder"  runat="server" clientidmode="Static" >
                                                    <input type="checkbox" id="chkActive1" runat="server" />
                                                    <input type="text" id="txtFeatureName1"  runat="server"/>
                                                    <input type="text" id="txtFeatureDisplayOrder1" runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                    <asp:ImageButton id="ImgCancel1" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel1_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                  
                                            <div id="divFeature_2" class="right-side DivBorder"  runat="server" clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive2" runat="server" />
                                                    <input type="text" id="txtFeatureName2" runat="server" />
                                                    <input type="text" id="txtFeatureDisplayOrder2" runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                    <asp:ImageButton id="ImgCancel2" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel2_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                <div class="right-side  DivBorder" id="divFeature_3" runat="server" clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive3" runat="server" />
                                                    <input type="text" id="txtFeatureName3"  runat="server"/>
                                                    <input type="text" id="txtFeatureDisplayOrder3"  runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                     <asp:ImageButton id="ImgCancel3" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel3_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                <div class="right-side  DivBorder" id="divFeature_4" runat="server" clientidmode="Static" visible="false" >
                                                    <input type="checkbox" id="chkActive4" runat="server" />
                                                    <input type="text" id="txtFeatureName4"  runat="server"/>
                                                    <input type="text" id="txtFeatureDisplayOrder4"  runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                    <asp:ImageButton id="ImgCancel4" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel4_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                <div class="right-side  DivBorder" id="divFeature_5" runat="server" clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive5" runat="server"/>
                                                    <input type="text" id="txtFeatureName5" runat="server" />
                                                    <input type="text" id="txtFeatureDisplayOrder5"  runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                    <asp:ImageButton id="ImgCancel5" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel5_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                <div class="right-side  DivBorder" id="divFeature_6" runat="server" clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive6" runat="server"/>
                                                    <input type="text" id="txtFeatureName6" runat="server" />
                                                    <input type="text" id="txtFeatureDisplayOrder6"  runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                    <asp:ImageButton id="ImgCancel6" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel6_Click" OnClientClick="return ConfirmDelete();"/>
                                                         </span>
                                                </div>

                                                <div class="right-side  DivBorder" id="divFeature_7" runat="server"  clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive7" runat="server"/>
                                                    <input type="text" id="txtFeatureName7" runat="server"/>
                                                    <input type="text" id="txtFeatureDisplayOrder7" runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                     <asp:ImageButton id="ImgCancel7" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel7_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                 <div class="right-side  DivBorder" id="divFeature_8" runat="server" clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive8" runat="server"/>
                                                    <input type="text" id="txtFeatureName8" runat="server" />
                                                    <input type="text" id="txtFeatureDisplayOrder8" runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                     <asp:ImageButton id="ImgCancel8" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel8_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                 <div class="right-side  DivBorder" id="divFeature_9" runat="server" clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive9" runat="server" />
                                                    <input type="text" id="txtFeatureName9" runat="server" />
                                                    <input type="text" id="txtFeatureDisplayOrder9" runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                     <asp:ImageButton id="ImgCancel9" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel9_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                                 <div class="right-side  DivBorder" id="divFeature_10" runat="server"  clientidmode="Static" visible="false">
                                                    <input type="checkbox" id="chkActive10" runat="server" />
                                                    <input type="text" id="txtFeatureName10"  runat="server"/>
                                                    <input type="text" id="txtFeatureDisplayOrder10" runat="server" onkeypress="return onlyNumbers(event)"/>                                                    
                                                    <span style="display:inline;float:right;" runat="server">
                                                      <asp:ImageButton id="ImageButton10" runat="server" ClientIDMode="Static" 
                                                        ImageUrl="../images/table_icon_2.gif" onclick="ImgCancel10_Click" OnClientClick="return ConfirmDelete();"/>
                                                    </span>
                                                </div>
                                               </div>
                                            </td>
                                        </tr>
                                     </tbody>
                                </table>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <br />
                        <p class="stdformbutton">
                            <asp:Button ID="btnsubmit" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnsubmit_onclick" Text="Submit" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnupdate" runat="server" class="btn btn-primary" Visible="false"
                                OnClick="btnupdate_onclick" Text="Update" Width="150px" ValidationGroup="grpsubmit" />
                            <asp:Button ID="btnCancel" runat="server" class="btn" Text="Cancel" Width="150px"
                                OnClick="btnCancel_Click" />
                        </p>
                    </div>
                        <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnDropDownListVal" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnListBoxVal" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnCurDivID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="XAxis" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="YAxis" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="Width" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="Height" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="maxWidth" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="maxHeight" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="HiddenField2" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDisplayOrder" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDropDownListValLevel1" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="HiddenField3" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="HiddenField4" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hndImageFileName" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnActualImageWidth" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnActualImageHeight" runat="server" ClientIDMode="Static"/>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <uc1:CadminFooter ID="CadminFooter1" runat="server" />
            <!-- for Display Order Popup -->
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                Update Plan Display Orders
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 70%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Plan&nbsp;Name
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                            <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                onclick="ToggleDisplay();" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrDisplayOrder">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblPlanID" runat="server" Text='<%# Eval("PlanId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PlanName") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("Dorder") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                <td>
                                                    <asp:Label ID="lblPlanID" runat="server" Text='<%# Eval("PlanId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PlanName") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("Dorder") %>' />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnUpdate" class="btn btn-primary" onclick="ValidateDisplayOrders();" Style="display: none;"
                                value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
        </div>
        <!--maincontentinner-->
        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
            role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
        </ul>
    </div>
    <!--maincontent-->
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
        });

        function ValidateDisplayOrders() {
           var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }

        function SuccessMsg(PlanID) {
            alert("The provided display orders are successfully Updated");
            var url = "";
            if (PlanID != null) {
                url = "../cadmin/Plans.aspx?PID=" + PlanID;
                $(location).attr('href', url);
            }
            else {
                url = "../cadmin/Plans.aspx";
                $(location).attr('href', url);
            }
        }

        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
            jQuery("#btnUpdate").show();
            jQuery("#img1").hide();
        }

        function AddNewFeature() {
            iClick = iClick + 1;
            var divLength = $("#divMain div");
            $("#divFeature_" + iClick).removeClass('HideDiv');
            $("#divFeature_" + iClick).addClass('ShowDiv');
         }
         function RemoveFeature(elem) {
             var id = $(elem).attr("id");
             var GetId = id.toString().split('_');
             $("#divFeature_" + GetId[1]).style.visibility = "hidden";
         }
       </script>
        <script language="javascript" type="text/javascript">
            //Start Image Preview Function
            function ShowPreview(fileInput) {
                var fileinputidval = fileInput.id;
                var thumbnilid = "";
                var Get_FileinputID = fileinputidval.toString().split('_');
                if (Get_FileinputID.length > 3) {
                    thumbnilid = Get_FileinputID[0] + "_" + Get_FileinputID[1] + "_ImgCMSFileUploader_" + Get_FileinputID[3];
                }
                var files = fileInput.files;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var ext = file.name.split('.').pop().toLowerCase();
                    var imageType = /image.*/;
                    var filetype = file.type
                    var img = document.getElementById(thumbnilid.toString());
                    img.file = file;
                    var reader = new FileReader();
                    if (file.type.match(imageType)) {
                        reader.onload = (function (aImg) {
                            return function (e) {
                                aImg.src = e.target.result;
                            };
                        })(img);
                        reader.readAsDataURL(file);
                    }
                    else if (ext == "pdf") {
                        reader.onload = (function (aImg) {
                            return function (e) {
                                aImg.src = '../UploadedFiles/pdf.png';
                            };
                        })(img);
                        reader.readAsDataURL(file);
                    }
                    else if (ext == "doc" || ext == "txt") {
                          reader.onload = (function (aImg) {
                            return function (e) {
                                aImg.src = '../UploadedFiles/doc.png';
                            };
                        })(img);
                        reader.readAsDataURL(file);
                    }

                    else if (ext == "xls" || ext == "cvc") {
                         reader.onload = (function (aImg) {
                            return function (e) {
                                aImg.src = '../UploadedFiles/xls.png';
                            };
                        })(img);
                        reader.readAsDataURL(file);
                    }
                }
            }

            function EditImage(ctrl) {
                 var ctrlArray = ctrl.split('_');
                OpenModelPopupCropAndResize(ctrlArray[ctrlArray.length - 1]);
                return false;
            }
            //Open Model Popup Crop and Resize
            function OpenModelPopupCropAndResize(prefix) {
                var FilePath = jQuery("#imgLogo").attr("src");
                var modalResize = document.getElementById('divModelResize');
                modalResize.style.display = "block";
                jQuery.ajax({
                    url: "../WebService/DocCMSApi.svc/ProcessFiles",
                    contentType: 'application/json; charset=utf-8',
                    data: { "FilePath": FilePath, "pageID": jQuery("#hdnCurPageID").val(), "CMSID": jQuery("#hdnCurCMSID").val() },
                    dataType: "json",
                    success: ajaxSucceeded,
                    error: ajaxFailed
                });
                function ajaxSucceeded(data) {
                    if (data != null) {
                        setTimeout(function () { ResizeImagePopup(data); }, 200);
                    }
                }
                function ajaxFailed() {
                    alert("There is an error during operation.");
                }
            }

            function ResizeImagePopup(fileName) {
                var jcrop_api;
                jQuery("#ImgResizePreview").attr("src", fileName);
                jQuery("#ImgResizeImage").attr("src", fileName);
                jQuery("#ImgCustomeImagePreviewRunning").attr("src", fileName);
                setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
                CheckResizeValues();
                return false;
            }

            function Get_Actual_Uploaded_Image_Size() {
                var myImg = document.querySelector("#ImgResizeImage");
                var realWidth = myImg.naturalWidth;
                var realHeight = myImg.naturalHeight;
                jQuery("#hdnActualImageWidth").val(realWidth);
                jQuery("#hdnActualImageHeight").val(realHeight);
            }

            var boundx, boundy;
            function SelectCropArea(c) {
                // Grab some information about the preview pane            
                var preview = jQuery('#preview-pane');
                var pcnt = jQuery('#preview-pane .preview-container');
                var pimg = jQuery('#preview-pane .preview-container img');
                var xsize = pcnt.width(),
                ysize = pcnt.height();
                jQuery('#XAxis').val(parseInt(c.x));
                jQuery('#YAxis').val(parseInt(c.y));
                jQuery('#Width').val(parseInt(c.w));
                jQuery('#Height').val(parseInt(c.h));
                jQuery('#x1').val(c.x);
                jQuery('#y1').val(c.y);
                jQuery('#x2').val(c.x2);
                jQuery('#y2').val(c.y2);
                jQuery('#w').val(c.w);
                jQuery('#h').val(c.h);
                if (parseInt(c.w) > 0) {
                    var rx = xsize / c.w;
                    var ry = ysize / c.h;
                    pimg.css({
                        width: Math.round(rx * boundx) + 'px',
                        height: Math.round(ry * boundy) + 'px',
                        marginLeft: '-' + Math.round(rx * c.x) + 'px',
                        marginTop: '-' + Math.round(ry * c.y) + 'px'
                    });
                }
            }

            function CloseImageResizeModel() {
                jQuery("#ImgResizeImage").attr("src", "");
                var modalResize = document.getElementById('divModelResize');
                modalResize.style.display = "none";
            }
    </script>
     <link href="../css/cropfunctionality.css" rel="stylesheet" type="text/css" />
     <asp:HiddenField ID="hdnCurCMSID" runat="server" ClientIDMode="Static"/>
     <asp:HiddenField ID="hdnCurPageID" runat="server" ClientIDMode="Static"/>
</asp:Content>
