﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class LiveChatManager : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                BindCheckboxStatus();
            }
        }
        protected void ChkActiveStatus_CheckedChanged(object sender, EventArgs e)
        {
            Int32 retval = 0;
            string CheckStatus = "";
            CheckStatus = Convert.ToString(ChkActiveStatus.Checked);
            ClsChatManager ObjClsChatManager = new ClsChatManager();
            ObjClsChatManager.isactive = CheckStatus;
            ObjClsChatManager.modifiedby = UserId;
            retval = ServicesFactory.DocCMSServices.UpdateChatManager(ObjClsChatManager);
            if (retval > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Chating Option Successfully Updated');</script>");
            }
            BindCheckboxStatus();
        }


        //Bind Checkbox
        public void BindCheckboxStatus()
        {
            string Status = "";
            Status = ServicesFactory.DocCMSServices.BindChatStatus();
            if (!string.IsNullOrEmpty(Status))
            {
                ChkActiveStatus.Checked = Convert.ToBoolean(Status);
            }
        }
    }
}