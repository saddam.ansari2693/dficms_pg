﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="DocCMSMain.cadmin.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>FAQs</h1>
                </div>
                <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="Home.htm">Home</a></li>
                        <li class="active" id="liRightHeading" runat="server" clientidmode="Static">Error</li>                         
                    </ul>
                </div>
            </div>
        </div>
    </section>
   <div id="Div1" class="container" style="min-height:400px;" runat="server">
        <section id="error" class="container" visible="false" runat="server" style=" font-size:16px;">
        <h1>No Result Found</h1>
        <p>The Page you are looking for doesn't exist...</p>
        <a class="btn btn-success" href="Home.htm">GO BACK TO THE HOME PAGE</a>
    </section>
</asp:Content>
