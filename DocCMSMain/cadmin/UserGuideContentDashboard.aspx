﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="UserGuideContentDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.UserGuideContentDashboard" %>

<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
     <script language="javascript" type="text/javascript">
         function CheckedChange(MemberID, CtrlCheck, EmailID) {
             jQuery("#SubContentID").val(MemberID);
             jQuery("#CurEmailID").val(EmailID);
             var btn = document.getElementById("btnUpdateStatus");
             if (jQuery(CtrlCheck).prop("checked") == true) {
                 if (confirm('Are you sure you want to Activate?')) {
                     jQuery("#CurStatus").val("True");
                     btn.click();
                 }
                 else {
                     jQuery(CtrlCheck).prop("checked", false);
                     jQuery(CtrlCheck).closest("span").removeClass("checked");
                 }
             }
             else {
                 if (confirm('Are you sure you want to Deactivate?')) {
                     jQuery("#CurStatus").val("False");
                     btn.click();
                 }
                 else {
                     jQuery(CtrlCheck).prop("checked", true);
                     jQuery(CtrlCheck).closest("span").addClass("checked");
                 }
             }
         }
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        function PreviewImage(ctrlID) {
            var ctrlNew = ctrlID.replace('imgSliderItem', 'divPreview');
            jQuery("#" + ctrlID).css("display", "none");
            jQuery("#" + ctrlNew).css("display", "");
            ctrlID.replace('divPreview', 'imgSliderItems');
        }

        function ClosePreviewImage(ctrlID) {
            var ctrlNew = ctrlID.replace('lblClosePreview', 'divPreview');
            var ctrlSmallImage = ctrlID.replace('lblClosePreview', 'imgSliderItem');
            jQuery("#" + ctrlSmallImage).css("display", "");
            jQuery("#" + ctrlNew).css("display", "none");
            ctrlID.replace('imgSliderItem', 'lblClosePreview');
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
        }
    </script>
       <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;
            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage Sub Heading</a> <span class="separator"></span></li>
        <li>Sub Heading Item Dashboard</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Sub Heading</h5>
            <h1>
                Sub Heading Item Dashboard</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                       <!--Filer by Status starts here--->
              <div class="par_ctrl_sec">
                     <div class="par control-group" >
                            <label class="control-label" for="ddlStatus">
                              Filer by Status</label>
                            <div class="controls">
                               <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                               <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                               </asp:DropDownList>
                            </div>
                        </div>
                     </div> 
              <!--Filer by Status ends here--->
              Sub Heading Item Dashboard
                <asp:HiddenField ID="hndUGMainID" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnBack" runat="server" Width="100" Text="Back" CssClass="btn" 
                    Style="float: right; margin-top: -5px;" OnClick="btnBack_Click" />
                <asp:Button ID="btnAddSliderImage" runat="server" CssClass="btn btn-primary" Width="190" Text="Add New Sub Heading Item "
                    Style="float: right; margin-top: -5px;" OnClick="btnSubContent_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style=" width: 30%;" />
                    <col class="con1" style="width: 5%;" />
                    
                    <col class="con1" style="width:  5%;" />   
                    <col class="con1" style="width:  10%;"/>    
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1" >
                           Sub Heading
                        </th>
                       <th class="head1">
                           Display Order
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                        <center>
                            Functions</center>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrSubContentItem" OnItemCommand="RptrSubContentItem_ItemCommand"
                        OnItemDataBound="RptrSubContentItem_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td style="">
                                <asp:LinkButton ID="lnkImageName" runat="server" ClientIDMode="Static" Text='<%# Eval("Heading") %>' CommandArgument='<%# Eval("SubContentID")%>' CommandName="EditSubContent"></asp:LinkButton>
                                   <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("Heading")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("Dorder")%>' ID="lblDorder"></asp:Label>
                                </td>
                                 <td>
                                  <asp:Label ID="IsActive" runat="server" Text='<%#Eval("IsActive") %>' class="Status" ></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SubContentID")%>' CommandName="EditSubContent" ToolTip="Edit " />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SubContentID")%>' CommandName="DeleteSubContent" ToolTip="Delete "  OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td style="">
                                <asp:LinkButton ID="lnkImageName" runat="server" ClientIDMode="Static" Text='<%# Eval("Heading") %>' CommandArgument='<%# Eval("SubContentID")%>'  CommandName="EditSubContent"></asp:LinkButton>
                                 <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("Heading")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("Dorder")%>' ID="lblheight"></asp:Label>
                                </td>
                                 <td>
                                  <asp:Label ID="IsActive" runat="server" Text='<%#Eval("IsActive") %>' class="Status" ></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SubContentID")%>' CommandName="EditSubContent" ToolTip="Edit " />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SubContentID")%>' CommandName="DeleteSubContent" ToolTip="Delete "  OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
        <asp:Button ID="btnUpdateStatus" runat="server" ClientIDMode="Static" style=" display:none;" Text="Update" onclick="btnUpdateStatus_Click"/>
         <asp:HiddenField ID="SubContentID" runat="server" ClientIDMode="Static" />
         <asp:HiddenField ID="CurStatus" runat="server" ClientIDMode="Static" />
    </div>
    <style type="text/css">
        #divPreview
        {
            max-width:400px;max-height:355px;z-index:10000000;
        }
        .closePreview
        {
    border:1px solid Green;width:20px;color:#000;background:#fff;position:absolute;font-weight:400;border-radius:4px;
        }
        .closePreview:hover
        {
    border:1px solid Green;width:20px;color:#fff;background:Red;position:absolute;font-weight:400;border-radius:4px;
        }
    </style>
    <!--maincontent-->
</asp:Content>