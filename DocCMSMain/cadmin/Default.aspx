﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DocCMSMain.cadmin.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>DOCFOCUS- Admin panel</title>
<link rel="shortcut icon" href="../images/ico/docfocus.ico" />
    <link href="../css/login.style.default.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-migrate-1.1.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
    <script src="../js/modernizr.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/jquery.cookie.js" type="text/javascript"></script>
    <script src="../js/custom.js" type="text/javascript"></script>
    <script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#login').submit(function () {
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if (u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>
<body class="loginpage">
<div class="loginpanel">
    <div class="loginpanelinner">
        <div class="logo animate0 bounceIn"><img src="../images/Logos/DocLogo2.png" height="120" width="260" alt="DocFocus Logo" /></div>
        <form id="login" action="" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
            <div class="inputwrapper login-alert" runat="server" id="msgDiv">
                <div class="alert alert-error" align="center" runat="server" id="innermsgDiv">Invalid username or password</div>
            </div>
            <div class="inputwrapper animate1 bounceIn">
                <input type="text" runat="server" name="username" id="username" placeholder="Enter your username" required />
                <label for="username" id="Label1" runat="server" visible="false"></label>
            </div>
            <div class="inputwrapper animate2 bounceIn">
                <input type="password" runat="server" name="password" id="password" placeholder="Enter your password"  required/>
                <label id="Label2" for="password" runat="server" visible="false"></label>
            </div>
            <div class="inputwrapper animate3 bounceIn">               
                 <asp:Button ID="btnsubmit" runat="server" Text="SIGN IN" onclick="submit_Click" Width="270" Height="40"  BackColor="#0972DD" style="border: 1px solid #0C57A3;color: #FFFFFF;display: block;"/> 
            </div>
            <div class="inputwrapper animate4 bounceIn">              
                <label style="color:#000;"><input type="checkbox" id="chkSignin" runat="server" class="remember" name="signin" /> Keep me sign in</label>
                  <label><a href="../cadmin/ForgetPassword.aspx" title="" style="color:#000;float:right;margin-left:57px;font-size:14px;">Forgot Your Password</a>  </label>
            </div>
        </form>
    </div><!--loginpanelinner-->
</div><!--loginpanel-->
<div class="loginfooter">
    <p>&copy; DOCFOCUS INC.<asp:Label id="labyear" runat="server" Text=""></asp:Label> All Rights Reserved.</p>
    <p style="float:right;margin-right:25px;">&copy;     
</div>
</body>
</html>
