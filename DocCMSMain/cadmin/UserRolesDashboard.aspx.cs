﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;

namespace DocCMSMain.cadmin
{
    public partial class UserRolesDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Session["RoleID"] != null && Convert.ToString(Session["RoleId"]) != "")
                {
                    Bind_User_Roles(Convert.ToInt32(Session["RoleID"]));
                    Set_Assign_Roles();
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                    SetControls();
                }
            }
        }
     
        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddRoleTop.Visible = true;
                    btnAddRoleBottom.Visible = true;
                }
                else
                {
                    btnAddRoleTop.Visible = false;
                    btnAddRoleBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrRoles.Items)
                {
                    LinkButton lnkRoleName = (LinkButton)Item.FindControl("lnkRoleName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    if (lblTitle != null && lnkRoleName != null && btnOrderUP != null && btnOrderDown != null && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkRoleName.Visible = true;
                            lblTitle.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkRoleName.Visible = false;
                            lblTitle.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }


        // Bind User Role Function
        protected void Bind_User_Roles(Int32 RoleID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_User_Roles(RoleID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrRoles.DataSource = dt;
                    RptrRoles.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrRoles_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void RptrRoles_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditRole":
                        Response.Redirect("~/cadmin/UserRoles.aspx?RID=" + e.CommandArgument.ToString());
                        break;
                    case "DeleteRole":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_userroles(Convert.ToInt32(e.CommandArgument));
                        Bind_User_Roles(Convert.ToInt32(Session["RoleID"]));
                        break;
                    case "OrderUp":
                        if(lblDorder!=null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserRoles(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        Bind_User_Roles(Convert.ToInt32(Session["RoleID"]));
                        break;
                    case "OrderDown":                        
                        if(lblDorder!=null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserRoles(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        Bind_User_Roles(Convert.ToInt32(Session["RoleID"]));
                        break;
                    default:
                        break;

                }
            }
        }
        

        protected void btnAddRoleBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserRoles.aspx");
        }

        protected void Set_Assign_Roles()
        {
            foreach (RepeaterItem item in RptrRoles.Items)
            {
                Label lblRoleID = (Label)item.FindControl("lblRoleID");
                ImageButton btnOrderUP = (ImageButton)item.FindControl("btnOrderUP");
                ImageButton btnOrderDown = (ImageButton)item.FindControl("btnOrderDown");
                ImageButton lnkEdit = (ImageButton)item.FindControl("lnkEdit");
                LinkButton lnkRoleName = (LinkButton)item.FindControl("lnkRoleName");
                ImageButton lnkDelete = (ImageButton)item.FindControl("lnkDelete");
                string SuperAdminName = ServicesFactory.DocCMSServices.Fetch_Roles_name(Convert.ToInt32(Session["RoleID"]));
                if (lblRoleID.Text == Convert.ToString(Session["RoleID"]))
                {
                    lnkEdit.Visible = false;
                    lnkDelete.Visible = false;
                    lnkRoleName.Enabled = false;
                }
                else
                {
                    lnkRoleName.Enabled = true;
                }
                if (SuperAdminName.ToUpper() == "SuperAdmin".ToUpper())
                {
                    lnkEdit.Visible = true;
                    lnkDelete.Visible = true;
                    lnkRoleName.Enabled = true;
                }
                else
                {
                    btnOrderDown.Visible = false;
                    btnOrderUP.Visible = false;
                }
            }
        }
    }
}