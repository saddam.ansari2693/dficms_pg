﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideMainContent : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);

            if (!IsPostBack)
            {

                if (Request.QueryString["MID"] != null)
                {
                    Bind_UG_Content_Data(Convert.ToInt32(Request.QueryString["MID"]));
                    Bind_DisplayOrder();
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_UG_MainContent_dorder() + 1;
                    Bind_DisplayOrder();
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        protected void Bind_UG_Content_Data(Int32 MainContentID)
        {
            try
            {
                Cls_UG_MainContent objMainContent = ServicesFactory.DocCMSServices.Fetch_UG_MainContent_Byid(MainContentID);
                if (objMainContent != null)
                {
                    txtmainheading.Value = objMainContent.heading;
                    elm1.Value = objMainContent.description;
                    txtDisplayOrder.Value = Convert.ToString(objMainContent.dorder);
                    ChkActiveStatus.Checked = objMainContent.isactive;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UG_MainContent();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideDashbord.aspx");
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UG_MainContent objMainContent = new Cls_UG_MainContent();

                objMainContent.heading = txtmainheading.Value;
                objMainContent.description = elm1.Value;
                objMainContent.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                objMainContent.createdby =Convert.ToString(UserId);
                if (ChkActiveStatus.Checked)
                    objMainContent.isactive = true;
                else
                    objMainContent.isactive = false;

                Int32 retVal = ServicesFactory.DocCMSServices.Insert_UGMainContent(objMainContent);
                Response.Redirect("~/cadmin/UserGuideDashbord.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void RptrDisplayOrder_databound(object sender, RepeaterItemEventArgs e)
        {
            
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UG_MainContent objMainContent = new Cls_UG_MainContent();
                objMainContent.heading = txtmainheading.Value;
                objMainContent.description = elm1.Value;
                objMainContent.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                objMainContent.lastmodifiedby =Convert.ToString(UserId);
                objMainContent.maincontentid = Convert.ToInt32(Request.QueryString["MID"]);
                if (ChkActiveStatus.Checked)
                    objMainContent.isactive = true;
                else
                    objMainContent.isactive = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UGMainContent(objMainContent);
               
                Response.Redirect("~/cadmin/UserGuideDashbord.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UG_MainContent> lstModules = new List<Cls_UG_MainContent>();
            Int32[] Dorder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblmaincontentid = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblmaincontentid");
                Label lblText = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblText");
                if (Session["UserID"] != null && lblmaincontentid != null && lblText != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    Dorder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblText.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(Dorder);

            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < Dorder.Length - 1; j++)
            {
                if (Dorder[j] < Dorder[j + 1])
                {
                    if (Dorder[j] == Dorder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }
                }
                else if (Dorder[j] == Dorder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UG_MainContent objMainContent = new Cls_UG_MainContent();
                Label lblMainContentID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblMainContentID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblMainContentID != null)
                {
                    objMainContent.maincontentid = Convert.ToInt32(lblMainContentID.Text.Trim());
                    objMainContent.dorder = Convert.ToInt32(txtDorder.Value);
                    objMainContent.lastmodifiedby = Convert.ToString(UserId);
                    lstModules.Add(objMainContent);
                }
            }
            if (lstModules != null && lstModules.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UG_Content_dorder(lstModules);
                if (retVal > 0)
                {

                    if (Request.QueryString["MID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["MID"] + ");</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                    }
                }
            }
        }
    }
}