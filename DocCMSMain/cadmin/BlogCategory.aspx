﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="BlogCategory.aspx.cs" Inherits="DocCMSMain.cadmin.BlogCategory" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="../Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="../css/isotope.css" type="text/css" />
    <link rel="stylesheet" href="../css/bootstrap-fileupload.min.css" type="text/css" />
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="../js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onlyNumbers(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if (key == 0 || key == 8) {
                return true;
            }
            if (key > 31 && (key < 48 || key > 57))
                return false;
            //             
            return true;
        }
    </script>
   <script language="javascript" type="text/javascript">
      $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $("#txtNavigationUrl").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../cadmin/SubModules.aspx/GetKeyWords",
                        data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }
        function SearchText_btn() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../cadmin/SubModules.aspx/GetKeyWords_btnClick",
                data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                dataType: "json",
                success: function (data) {
                    var dataArray = data.d.toString().split(',');
                    var List = "";
                    for (var i = 0; i < dataArray.length; i++) {
                        List = List + "<li class='ui-menu-item customMenu' role='menuitem' id='liAutoComplete_" + i.toString() + "'>";
                        var NavigateURL = "";
                        if (i == 0) {
                            NavigateURL = dataArray[i].toString().trim().substring(2);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        else if (i == dataArray.length - 1) {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 2);
                        }
                        else {
                            NavigateURL = dataArray[i].toString().trim().substring(1);
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        if (dataArray.length == 1) {
                            NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                        }
                        List = List + "<a class='ui-corner-all' tabindex='-1' id='ancAutoComplete_" + i.toString() + "' onclick='SetValueInTextBox(this);'>" + NavigateURL + "</a>";
                        List = List + "</li>";
                    }
                    $("#ulAutocomplete").html(List);
                    $("#ulAutocomplete").show();
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }
        function SetValueInTextBox(ctrlID) {
            $("#txtNavigationUrl").val($("#" + ctrlID.id).text());
            $("#ulAutocomplete").hide();
        }
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
             //Replaces data-rel attribute to rel.
            //We use data-rel because of w3c validation issue
            jQuery('a[data-rel]').each(function () {
                jQuery(this).attr('rel', jQuery(this).data('rel'));
            });
            jQuery("#medialist a").colorbox();
        });
        jQuery(window).load(function () {
            jQuery('#medialist').isotope({
                itemSelector: 'li',
                layoutMode: 'fitRows'
            });
            // Media Filter
            jQuery('#mediafilter a').click(function () {
                var filter = (jQuery(this).attr('href') != 'all') ? '.' + jQuery(this).attr('href') : '*';
                jQuery('#medialist').isotope({ filter: filter });
                jQuery('#mediafilter li').removeClass('current');
                jQuery(this).parent().addClass('current');
                return false;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Content Management</a> <span class="separator"></span></li>
        <li>Manage Blog Category</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5 id="h5heading1" runat="server">
             Content Management</h5>
            <h1 id="h1heading1" runat="server">
                 Blog Category</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <div class="widget">
                <h4 class="widgettitle" id="h4heading1" runat="server">
                     Blog Category</h4>
                  <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform">
                        <div class="par control-group">
                            <label class="control-label" for="txtCategoryName">
                                Category Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtCategoryName" id="txtCategoryName" class="input-large"
                                    clientidmode="Static" placeholder="Enter Category Name" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtCategoryName" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 7px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtDescription">
                               Category Description</label>
                            <div class="controls">
                                <textarea id="txtDescription" name="txtDescription" rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="height: 100px; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" placeholder="Enter Category Description"></textarea>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtDescription" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 7px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                                Display Order</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtDisplayOrder" id="txtDisplayOrder" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Order Number" disabled/>
                                <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                    Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click" />
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtNavigationUrl">
                                Navigation URL</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtNavigationUrl" id="txtNavigationUrl" class="input-large autocomplete"
                                    clientidmode="Static" placeholder="Enter Navigation URl" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="grpsubmit"
                                    ControlToValidate="txtNavigationUrl" runat="server" style="color: Red; font-size: 24px; font-weight: bold; padding: 7px;">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus">
                                Active Status</label>
                            <div class="controls">
                                <input type="checkbox" runat="server" name="activestatus" id="ChkActiveStatus" class="input-large"
                                    style="opacity: 1" checked />
                            </div>
                        </div>
                        <p class="stdformbutton">
                          <button class="btn btn-primary" id="btnsubmit" validationgroup="grpsubmit" runat="server"
                              onserverclick="btnsubmit_click" onclick="return btnsubmit_onclick()">
                                  Save Information</button>
                          <asp:Button ID="btnEdits" class="btn btn-primary" runat="server" Text="Update Information"  OnClick="btnEdit_click"></asp:Button>
                          <button class="btn btn-primary" id="Button1" runat="server" onserverclick="btnCancel_click"
                              visible="True"> Cancel</button>
                        </p>
                    </div>
                    </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
            <!-- for Display Order Popup -->
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                Update Modules Display Orders
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 70%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                           Blog&nbsp;Name
                                        </th>
                                          <th id="ActiveStatusheading" runat="server">
                                        Status
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                            <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                onclick="ToggleDisplay();" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrDisplayOrder">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# Eval("BlogCategoryID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("BlogCategoryName") %>'></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                   <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("BlogCategoryID") %>'
                                                       Visible="false"></asp:Label>
                                                   <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                       CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("Dorder") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">                
                                                <td>
                                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# Eval("BlogCategoryID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("BlogCategoryName") %>'></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("BlogCategoryID") %>'
                                                        Visible="false"></asp:Label>
                                                    <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                        CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("Dorder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        class="DorderText" style="display: none; width: 100px; float: left; margin-right: 25px;"
                                                        value='<%# Eval("Dorder") %>' />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnUpdate" class="btn btn-primary" onclick="ValidateDisplayOrders();"
                                value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
            <asp:Label ID="lblText" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <script type="text/javascript">
        //For the Text editor
        jQuery('#txtdescription').jqte();
    </script>
    <!--maincontent-->
    <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
       role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
    </ul>
   <!--maincontent-->
    <script language="javascript" type="text/javascript">
        function ValidateDisplayOrders() {
            var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
            btnUpdateDisplayOrder.click();
        }
        function SuccessMsg(CategoryID) {
            alert("The provided display orders are successfully Updated");
            var url = "";
            if (CategoryID != null) {
                url = "../cadmin/BlogCategory.aspx?CategoryID=" + CategoryID;
                $(location).attr('href', url);
            }
            else {
                url = "../cadmin/BlogCategory.aspx";
                $(location).attr('href', url);
            }
        }
        function ToggleDisplay(ItemIndex) {
            jQuery(".DorderLabel").toggle();
            jQuery(".DorderText").toggle();
       }
    </script>
</asp:Content>