﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="PrivilegesDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.PrivilegesDashboard" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Privileges</a> <span class="separator"></span></li>
        <li>Manage Privileges</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Privileges</h5>
            <h1>
                Manage Privileges</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
               Privileges Details
                <asp:Button ID="btnAddPrivilegeTop" runat="server" CssClass="btn btn-primary" Width="150" Text="Add New Privilege"
                    Style="float: right; margin-top: -5px;" OnClick="btnAddPrivilegeBottom_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                  <colgroup>
                    <col class="con1" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 70%;" />
                    <col class="con1" style="width: 10%;" />                    
                    <col class="con1" style="align: center; width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            Display Order
                        </th>
                        <th class="head1">
                            Privileges
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrPrivileges" OnItemCommand="RptrPrivileges_ItemCommand"
                        OnItemDataBound="RptrPrivileges_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                    <asp:Label ID="lblPrivilegeID" runat="server" ClientIDMode="Static" Text='<%# Eval("PrivilgeId") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                
                                    <div style="display: inline; margin-left: 20px;">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("PrivilgeId")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("PrivilgeId")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrivilegeName" CommandArgument='<%# Eval("PrivilgeId")%>' CommandName="EditPrivilege"
                                        runat="server" Text='<%# Eval("PrivilegeName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("PrivilegeName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Active")%>' ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("PrivilgeId")%>' CommandName="EditPrivilege" ToolTip="Edit Privilege" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("PrivilgeId")%>' CommandName="DeletePrivilege" ToolTip="Delete Privilege" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to delete?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                <td>
                                    <asp:Label ID="lblPrivilegeID" runat="server" ClientIDMode="Static" Text='<%# Eval("PrivilgeId") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("DisplayOrder") %>'></asp:Label>                                    
                                    <div style="display: inline; margin-left: 20px;">
                                        <asp:ImageButton ID="btnOrderUP" runat="server" CommandArgument='<%# Eval("PrivilgeId")%>'
                                            ImageUrl="~/images/icon_display-order_up.png" CommandName="OrderUp" />
                                        <asp:ImageButton ID="btnOrderDown" runat="server" CommandArgument='<%# Eval("PrivilgeId")%>'
                                            ImageUrl="~/images/icon_display-order_dn.png" CommandName="OrderDown" />
                                    </div>
                                </td>
                                <td>
                                 <asp:LinkButton ID="lnkPrivilegeName" CommandArgument='<%# Eval("PrivilgeId")%>' CommandName="EditPrivilege"
                                        runat="server" Text='<%# Eval("PrivilegeName")%>' />
                                 <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("PrivilegeName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Active")%>' ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("PrivilgeId")%>' CommandName="EditPrivilege" ToolTip="Edit Privilege" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("PrivilgeId")%>' CommandName="DeletePrivilege" ToolTip="Delete Privilege" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText=" Are you sure to delete?" OnClientCancel="CancelClick"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <asp:Button ID="btnAddPrivilegeBottom" runat="server" CssClass="btn btn-primary" Width="150"
                Text="Add New Privilege" OnClick="btnAddPrivilegeBottom_Click" />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
