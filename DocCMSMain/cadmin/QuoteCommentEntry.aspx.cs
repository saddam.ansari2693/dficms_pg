﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;

namespace DocCMSMain.cadmin
{
    public partial class QuoteCommentEntry : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if (Request.QueryString["QID"] != null)
                {
                    Bind_Quote_Data(Convert.ToInt32(Request.QueryString["QID"]));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Quote Data when Quote Id is alerady existing
        protected void Bind_Quote_Data(Int32 QuoteID)
        {
            try
            {
                Cls_Quotes objQuote = ServicesFactory.DocCMSServices.Fetch_Quotes_Byid(QuoteID);
                if (objQuote != null)
                {
                    txtQuoteName.Value = objQuote.quotename;
                    txtQuoteComment.Value = objQuote.quotecomment;
                    ChkActiveStatus.Checked = objQuote.active;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Function When submit button is clicked
        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_Quotes ClsQuote = new Cls_Quotes();
                ClsQuote.quotename = txtQuoteName.Value;
                ClsQuote.quotecomment = txtQuoteComment.Value;
                
                if (ChkActiveStatus.Checked)
                    ClsQuote.active = true;
                else
                    ClsQuote.active = false;

                 Int32 retVal = ServicesFactory.DocCMSServices.Insert_Quotes(ClsQuote);
                 Response.Redirect("~/cadmin/QuoteCommentDashboard.aspx");
                }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Function When update button is clicked
        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_Quotes ClsQuote = new Cls_Quotes();
                ClsQuote.quotename = txtQuoteName.Value;
                ClsQuote.quotecomment = txtQuoteComment.Value;
                ClsQuote.quoteid = Convert.ToInt32(Request.QueryString["QID"]);
                if (ChkActiveStatus.Checked)
                    ClsQuote.active = true;
                else
                    ClsQuote.active = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Quotes(ClsQuote);
                Response.Redirect("~/cadmin/QuoteCommentDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Function When cancel button is clicked
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/QuoteCommentDashboard.aspx");
        }
    }
}