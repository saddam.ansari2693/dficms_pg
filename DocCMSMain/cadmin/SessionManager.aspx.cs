﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Web.UI.HtmlControls;


namespace DocCMSMain.cadmin
{
    public partial class SessionManager : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                BindTimeout();
                SetControls();
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
           
                    if (SpanDiv != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            SpanDiv.Visible = true;
                            btnsubmit.Visible = true;
                            btnCancel.Visible = true;
                        }
                        else
                        {
                            SpanDiv.Visible = false;
                            btnsubmit.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }
                }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        public void BindTimeout()
        {
            DataTable dtTime = ServicesFactory.DocCMSServices.Fetchtimeout();
            if (dtTime != null && dtTime.Rows.Count > 0)
            {
                txtTimeout.Value = Convert.ToString(dtTime.Rows[0]["Timeout"]);
                ChkActiveStatus.Checked = Convert.ToBoolean(dtTime.Rows[0]["IsActive"]);
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] != null && Convert.ToString(Session["UserID"]) != "")
                {
                    UserId = Convert.ToString(Session["UserID"]);
                }
                else
                {
                    UserId = "";
                }
                Clstimeout Obj_ClsTimeout = new Clstimeout();
                Obj_ClsTimeout.timeout = Convert.ToInt16(txtTimeout.Value);
                Obj_ClsTimeout.isactive = Convert.ToBoolean(ChkActiveStatus.Checked);
                Obj_ClsTimeout.modifiedby = UserId;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_timeout(Obj_ClsTimeout);
                if (retVal > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Timeout Successfully Updated');</script>");
                }
                Response.Redirect("~/cadmin/SessionManager.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/SessionManager.aspx");
        }
    }
}