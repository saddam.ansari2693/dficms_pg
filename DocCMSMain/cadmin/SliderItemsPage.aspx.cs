﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Drawing;

namespace DocCMSMain.cadmin
{
    public partial class SliderItemsPage : System.Web.UI.Page
    {
        dynamic UserId;
        protected static Int32 SMTPDetailID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                btnsubmitItemMaster.Visible = true;
                if (Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    hdnSliderId.Value = Convert.ToString(Request.QueryString["SliderID"]);
                    btnsubmitItemMaster.Visible = true;
                    btnMasterItemupdate.Visible = false;
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_SliderItem_displayorder(Convert.ToInt32(Request.QueryString["SliderID"])) + 1;
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderItemID"]));
                    txtdorder.Text = Convert.ToString(DisplayOrder);
                }
                else
                {
                    divSliderPosition.Visible = true;
                    btnsubmitItemMaster.Visible = false;
                    btnMasterItemupdate.Visible = true;
                }
                if (Request.QueryString["SliderItemID"] != null && Request.QueryString["SliderItemID"] != "")
                {
                    hdnSliderItemId.Value = Convert.ToString(Request.QueryString["SliderItemID"]);
                    Bind_Master_Slider(Convert.ToInt16(Request.QueryString["SliderItemID"]));
                    btnsubmitItemMaster.Visible = false;
                    btnMasterItemupdate.Visible = true;
                    divSliderPosition.Visible = true;
                    hdnCurPageID.Value = Convert.ToString(Request.QueryString["SliderID"]);
                    hdnCurCMSID.Value = Convert.ToString(Request.QueryString["SliderItemID"]);
                }
                else
                {
                    divSliderPosition.Visible = false;
                    btnsubmitItemMaster.Visible = true;
                    btnMasterItemupdate.Visible = false;
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_SliderItem_displayorder(Convert.ToInt32(Request.QueryString["SliderID"])) + 1;
                    txtdorder.Text = Convert.ToString(DisplayOrder);
                }
            }
        }


        //Image Resize funtion
        public bool ResizeImageAndUpload(System.IO.FileStream newFile, string folderPathAndFilenameNoExtension, string Extention, int maxHeight, int maxWidth)
        {
            try
            {
                // Create variable to hold the image
                System.Drawing.Image thisImage = System.Drawing.Image.FromStream(newFile);
                int width = (int)maxWidth;
                int height = (int)maxHeight;
                // Create "blank" image for drawing new image
                Bitmap outImage = new Bitmap(width, height);
                Graphics outGraphics = Graphics.FromImage(outImage);
                SolidBrush sb = new SolidBrush(System.Drawing.Color.White);
                // Fill "blank" with new sized image
                outGraphics.FillRectangle(sb, 0, 0, outImage.Width, outImage.Height);
                outGraphics.DrawImage(thisImage, 0, 0, outImage.Width, outImage.Height);
                sb.Dispose();
                outGraphics.Dispose();
                thisImage.Dispose();
                // Save new image as jpg
                outImage.Save(Server.MapPath(folderPathAndFilenameNoExtension + Extention));
                outImage.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // FileUploader Click Button Event
        protected void btnPrieviewImage_Click(object sender, EventArgs e)
        {
            imgLogo.Src = ImageUploadControl.FileName;
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderID"]));
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }


        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<ClsSliderItemMaster> lstSliderItemMaster = new List<ClsSliderItemMaster>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblSliderItemId = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSliderItemId");
                HiddenField hdnImageName = (HiddenField)RptrDisplayOrder.Items[ItemCount].FindControl("hdnImageName");
                if (Session["UserID"] != null && lblSliderItemId != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + hdnImageName.Value + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderID"]));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);

            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderID"]));
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderID"]));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                ClsSliderItemMaster objClsSliderItemMaster = new ClsSliderItemMaster();
                Label lblSliderItemId = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSliderItemId");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblSliderItemId != null)
                {
                    objClsSliderItemMaster.slideritemid = Convert.ToInt32(lblSliderItemId.Text.Trim());
                    objClsSliderItemMaster.dorder = Convert.ToInt32(txtDorder.Value);
                    lstSliderItemMaster.Add(objClsSliderItemMaster);
                }
            }

            if (lstSliderItemMaster != null && lstSliderItemMaster.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_SliderItemMaster_dorder(lstSliderItemMaster);
                if (retVal > 0)
                {

                    if (Request.QueryString["SliderID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["SliderID"] + ");</script>");
                        Bind_Master_Slider(Convert.ToInt32(Request.QueryString["SliderItemID"]));
                        Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderItemID"]));
                        btnsubmitItemMaster.Visible = false;
                        btnMasterItemupdate.Visible = true;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                        Int32 DisplayOrder1 = ServicesFactory.DocCMSServices.Get_Max_SliderItem_displayorder(Convert.ToInt32(Request.QueryString["SliderID"])) + 1;
                        Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["SliderItemID"]));
                        txtdorder.Text = Convert.ToString(DisplayOrder1);
                        btnsubmitItemMaster.Visible = true;
                        btnMasterItemupdate.Visible = false;
                    }
                }
            }
            //Update On 20160803
            Bind_Master_Slider(Convert.ToInt32(Request.QueryString["SliderItemID"]));
        }

        protected void Bind_DisplayOrder(int SliderID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SliderItemMaster_Order_By_dorder(SliderID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmitItemMaster_onclick(object sender, EventArgs e)
        {
            try
            {
                string Filename = "";
                string SliderName = "";
                string SliderId = "";
                int ImageHeight = 0;
                int ImageWidth = 0;                
                if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName) && ImageUploadControl.HasFile)
                {
                    string NewId = Guid.NewGuid().ToString();
                    string Extention = Path.GetExtension(ImageUploadControl.PostedFile.FileName);
                    string FileName=NewId.ToString()+Extention.ToString();
                    ClsSliderItemMaster objClsSliderItemMaster = new ClsSliderItemMaster();
                    objClsSliderItemMaster.sliderid = Convert.ToInt16(hdnSliderId.Value);
                    objClsSliderItemMaster.imagename = FileName;
                    objClsSliderItemMaster.dorder = Convert.ToInt16(txtdorder.Text);
                    objClsSliderItemMaster.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
                    Int32 Retval = ServicesFactory.DocCMSServices.Insert_SliderItem_Master_Item(objClsSliderItemMaster);
                    if (Retval > 0)
                    {
                        string FilePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + Retval + "/";
                        string ResizeImagePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + Retval + "/";

                        if (!Directory.Exists(Server.MapPath(FilePath)))
                        {
                            Directory.CreateDirectory(Server.MapPath(FilePath));
                        }
                        // Uploading Original File                        
                        ImageUploadControl.SaveAs(Server.MapPath(FilePath + "ORG_" + NewId.ToString() + Extention));
                        //Resize Image Code
                        HttpPostedFile myFile = ImageUploadControl.PostedFile;
                        FilePath = FilePath + NewId.ToString();
                        Filename = NewId + Extention;
                        int nFileLen = myFile.ContentLength;
                        if ((nFileLen > 0))
                        {
                            // Read file into a data stream
                            byte[] FileData = new Byte[nFileLen];
                            myFile.InputStream.Read(FileData, 0, nFileLen);
                            myFile.InputStream.Dispose();

                            // Save the stream to disk as temporary file. make sure the path is unique!
                            System.IO.FileStream newFile= new System.IO.FileStream(Server.MapPath(FilePath), System.IO.FileMode.Create);
                            newFile.Write(FileData, 0, FileData.Length);

                            // run ALL the image optimisations you want here..... make sure your paths are unique
                            // you can use these booleans later if you need the results for your own labels or so.
                            // dont call the function after the file has been closed.
                            for (int i = 1; i < 3; i++)
                            {
                                if (i == 1)
                                {
                                    //set height and width for left and right slider
                                    ImageWidth = 497;
                                    ImageHeight = 288;
                                    ResizeImagePath = "";
                                    ResizeImagePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + Retval.ToString() + "/";
                                    ResizeImagePath = ResizeImagePath + "Left_" + NewId.ToString();
                                }
                                else
                                {
                                    //set height and width for full width slider
                                    ImageWidth = 1900;
                                    ImageHeight = 700;
                                    ResizeImagePath = "";
                                    ResizeImagePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + Retval.ToString() + "/";
                                    ResizeImagePath = ResizeImagePath + "Fullwidth_" + NewId.ToString();
                                }
                                bool success = ResizeImageAndUpload(newFile, ResizeImagePath, Extention, ImageHeight, ImageWidth);
                            }
                            // tidy up and delete the temp file.
                            newFile.Close();
                            System.IO.File.Delete(Server.MapPath(FilePath));
                        }
                        Response.Redirect("SliderItemDashboard.aspx?SliderID=" + Request.QueryString["SliderID"]);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "RMA", "<script>alert('Please choose Image');</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnMasterItemupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                string FilenameWidthExtention = "";
                string Filename = "";
                string SliderId = "";
                int ImageHeight = 0;
                int ImageWidth = 0; 

                if (ImageUploadControl.PostedFile != null && !string.IsNullOrEmpty(ImageUploadControl.PostedFile.FileName) && ImageUploadControl.HasFile)
                {
                    FilenameWidthExtention = lblFileUploader.Text.Trim();
                    Filename = lblFileUploader.Text.Trim().Split('.')[0];
                    string Extention = Path.GetExtension(ImageUploadControl.PostedFile.FileName);
                    Filename = Filename + Extention.ToString();
                    ClsSliderItemMaster objClsSliderItemMaster = new ClsSliderItemMaster();
                    objClsSliderItemMaster.slideritemid = Convert.ToInt16(hdnSliderItemId.Value);
                    objClsSliderItemMaster.sliderid = Convert.ToInt16(hdnSliderId.Value);
                    objClsSliderItemMaster.imagename = Filename;
                    objClsSliderItemMaster.dorder = Convert.ToInt16(txtdorder.Text);
                    objClsSliderItemMaster.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
                    Int32 retval  = ServicesFactory.DocCMSServices.Update_Slider_Item_Master_Data(objClsSliderItemMaster);
                    if (retval > 0)
                    {

                        string FilePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + hdnSliderItemId.Value + "/";
                        string ResizeImagePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + hdnSliderItemId.Value + "/";
                        if (!Directory.Exists(Server.MapPath(FilePath)))
                        {
                            Directory.CreateDirectory(Server.MapPath(FilePath));
                        }
                        if (File.Exists(Server.MapPath(FilePath + "ORG_" + FilenameWidthExtention)))
                        {
                            File.Delete(Server.MapPath(FilePath + "ORG_" + FilenameWidthExtention));
                        }
                        if (File.Exists(Server.MapPath(FilePath + "LEFT_" + FilenameWidthExtention)))
                        {
                            File.Delete(Server.MapPath(FilePath + "LEFT_" + FilenameWidthExtention));
                        }
                        if (File.Exists(Server.MapPath(FilePath + "FULLWIDTH_" + FilenameWidthExtention)))
                        {
                            File.Delete(Server.MapPath(FilePath + "FULLWIDTH_" + FilenameWidthExtention));
                        }
                        // Uploading Original File                        
                        ImageUploadControl.SaveAs(Server.MapPath(FilePath + "ORG_" + Filename));
                        // Resizing Image
                        HttpPostedFile myFile = ImageUploadControl.PostedFile;                        
                        int nFileLen = myFile.ContentLength;
                        if ((nFileLen > 0))
                        {
                            // Read file into a data stream
                            byte[] FileData = new Byte[nFileLen];
                            myFile.InputStream.Read(FileData, 0, nFileLen);
                            myFile.InputStream.Dispose();

                            // Save the stream to disk as temporary file. make sure the path is unique!
                            FilePath = FilePath + Filename.Split('.')[0];
                            System.IO.FileStream newFile
                                    = new System.IO.FileStream(Server.MapPath(FilePath),
                                                               System.IO.FileMode.Create);
                            newFile.Write(FileData, 0, FileData.Length);

                            // run ALL the image optimisations you want here..... make sure your paths are unique
                            // you can use these booleans later if you need the results for your own labels or so.
                            // dont call the function after the file has been closed.
                            for (int i = 1; i < 3; i++)
                            {
                                if (i == 1)
                                {
                                    //set height and width for left and right slider
                                    ImageWidth = 497;
                                    ImageHeight = 288;
                                    ResizeImagePath = "";
                                    ResizeImagePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + hdnSliderItemId.Value.ToString() + "/";
                                    ResizeImagePath = ResizeImagePath + "Left_" + Filename.ToString().Trim().Split('.')[0];
                                }
                                else
                                {
                                    //set height and width for full width slider
                                    ImageWidth = 1900;
                                    ImageHeight = 700;
                                    ResizeImagePath = "";
                                    ResizeImagePath = "~/UploadedFiles/SliderImage/" + hdnSliderId.Value + "/" + hdnSliderItemId.Value.ToString() + "/";
                                    ResizeImagePath = ResizeImagePath + "Fullwidth_" + Filename.ToString().Trim().Split('.')[0];
                                }
                                bool success = ResizeImageAndUpload(newFile, ResizeImagePath, Extention, ImageHeight, ImageWidth);
                            }
                            // tidy up and delete the temp file.
                            newFile.Close();
                            System.IO.File.Delete(Server.MapPath(FilePath));
                            Response.Redirect("SliderItemDashboard.aspx?SliderID=" + Convert.ToInt16(hdnSliderId.Value));
                            hdnImageName.Value = "";
                        }
                     //Resize Image Code
                    }
                }
                else
                {
                    FilenameWidthExtention = lblFileUploader.Text.Trim();
                    Filename = lblFileUploader.Text.Trim().Split('.')[0];
                    string Extention = Path.GetExtension(ImageUploadControl.PostedFile.FileName);
                    Filename = Filename + Extention.ToString();
                    ClsSliderItemMaster objClsSliderItemMaster = new ClsSliderItemMaster();
                    objClsSliderItemMaster.slideritemid = Convert.ToInt16(hdnSliderItemId.Value);
                    objClsSliderItemMaster.sliderid = Convert.ToInt16(hdnSliderId.Value);
                    objClsSliderItemMaster.imagename = FilenameWidthExtention;
                    objClsSliderItemMaster.dorder = Convert.ToInt16(txtdorder.Text);
                    objClsSliderItemMaster.isactive = Convert.ToString(ChkActiveStatus.Checked ? "True" : "False");
                    Int32 retval = ServicesFactory.DocCMSServices.Update_Slider_Item_Master_Data(objClsSliderItemMaster);
                    Response.Redirect("~/cadmin/SliderItemDashboard.aspx?SliderID=" + Request.QueryString["SliderID"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/SliderItemDashboard.aspx?SliderID=" + Request.QueryString["SliderID"]);
        }


        //Bind All Master Slider
        private void Bind_Master_Slider(int SliderItemID)
        {
            try
            {
                string ImageName = "";
                DataTable DtGetSliderItemMaster = ServicesFactory.DocCMSServices.Fetch_SliderItem_For_Edit(SliderItemID);
                if (DtGetSliderItemMaster != null && DtGetSliderItemMaster.Rows.Count > 0)
                {
                    imgLogo.Src = "../UploadedFiles/SliderImage/" + Convert.ToString(DtGetSliderItemMaster.Rows[0]["SliderID"]) + "/" + Convert.ToString(SliderItemID) + "/ORG_" + Convert.ToString(DtGetSliderItemMaster.Rows[0]["ImageName"]);                    
                    lblFileUploader.Text = Convert.ToString(DtGetSliderItemMaster.Rows[0]["ImageName"]);
                    txtdorder.Text = Convert.ToString(DtGetSliderItemMaster.Rows[0]["Dorder"]);
                    hdnSliderId.Value = Convert.ToString(DtGetSliderItemMaster.Rows[0]["SliderID"]);
                    ChkActiveStatus.Checked = Convert.ToBoolean(DtGetSliderItemMaster.Rows[0]["IsActive"]);
                    ImageName = Convert.ToString(DtGetSliderItemMaster.Rows[0]["ImageName"]).Split('_').Last();
                    hdnImageName.Value = ImageName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //End Image Resize Function
    }// Class Ends here
}