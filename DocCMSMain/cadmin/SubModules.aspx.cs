﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Web.Services;
using System.IO;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class SubModules : System.Web.UI.Page
    {
        dynamic UserId;
        string ModuleID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if (Request.QueryString["ID"] != null && Request.QueryString["MID"] != null)
                {
                    Bind_ModuleName();
                    Bind_SubModuleModule_Data(Convert.ToInt32(Request.QueryString["ID"]));
                    Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    Bind_ModuleName();
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_SubModules_Dorder(Convert.ToInt32(ddlModuleName.SelectedValue)) + 1;
                    Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Module Name inside ddlModulename dropdown
        protected void Bind_ModuleName()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_ModulesName();
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlModuleName.DataSource = dt;
                    ddlModuleName.DataBind();
                }
                if (Request.QueryString["MID"] != null)
                {
                    ddlModuleName.SelectedValue = Request.QueryString["MID"];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind SubModule data when SubModule already existing
        protected void Bind_SubModuleModule_Data(Int32 SubmoduleID)
        {
            try
            {

                Clssubmodules objSubModule = ServicesFactory.DocCMSServices.Fetch_SubModules_ById(SubmoduleID);
                if (objSubModule != null)
                {
                    txtSubModuleName.Value = objSubModule.submodulename;
                    txtDescription.Value = objSubModule.description;
                    txtDisplayOrder.Value = Convert.ToString(objSubModule.displayorder);
                    ChkActiveStatus.Checked = objSubModule.active;
                    txtNavigationUrl.Value = objSubModule.navigationurl;
                    ddlModuleName.SelectedValue = Convert.ToString(objSubModule.moduleid);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Clssubmodules clsSubModules = new Clssubmodules();
                clsSubModules.submodulename = txtSubModuleName.Value;
                clsSubModules.description = txtDescription.Value;
                clsSubModules.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsSubModules.navigationurl = txtNavigationUrl.Value;
                clsSubModules.moduleid = Convert.ToInt32(ddlModuleName.SelectedValue);
                clsSubModules.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsSubModules.active = true;
                else
                    clsSubModules.active = false;

                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_SubModule(txtSubModuleName.Value.Trim());
                if (RetCheck > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('SubModule with Same Name Already Exists...');</script>");
                    return;
                }
                else
                {
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_SubModules(clsSubModules);
                    if (retVal > 0)
                    {
                        ServicesFactory.DocCMSServices.Adjust_Submodules_Dorder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    }
                    Response.Redirect("~/cadmin/SubModulesDashboard.aspx?MID=" + Convert.ToInt32(ddlModuleName.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["ID"] != null)
                {
                    Clssubmodules clsSubModules = new Clssubmodules();
                    clsSubModules.submodulename = txtSubModuleName.Value;
                    clsSubModules.description = txtDescription.Value;
                    clsSubModules.navigationurl = txtNavigationUrl.Value;
                    clsSubModules.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                    clsSubModules.lastmodifiedby = Guid.Parse(UserId);
                    clsSubModules.moduleid = Convert.ToInt32(ddlModuleName.SelectedValue);
                    clsSubModules.submoduleid = Convert.ToInt32(Request.QueryString["ID"]);
                    if (ChkActiveStatus.Checked)
                        clsSubModules.active = true;
                    else
                        clsSubModules.active = false;
                    Int32 retVal = ServicesFactory.DocCMSServices.Update_SubModules(clsSubModules);
                    if (retVal > 0)
                    {
                        ServicesFactory.DocCMSServices.Adjust_Submodules_Dorder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    }
                    Response.Redirect("~/cadmin/SubModulesDashboard.aspx");
                }
                else
                {
                    // Do nothing
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/SubModulesDashboard.aspx");
        }

        //For AutoComplete function for SubModule navigation
        [WebMethod]
        public static List<string> GetKeyWords(string SearchName)
        {
            string extension = SearchName;
            List<string> SearchResult = new List<string>();
            List<string> FilterredList = new List<string>();
            var rootPath = HostingEnvironment.ApplicationPhysicalPath;
            string[] DirectoryList = Directory.GetDirectories(rootPath);
            FileInfo[] RootfileInfo = GetFilesFromFolder(rootPath, (extension == "") ? "aspx" : extension);
            if (RootfileInfo.Length > 0)
            {
                foreach (FileInfo fileInfoTemp in RootfileInfo)
                {
                    string DirPath = rootPath.Replace("\\", "~");
                    string[] CurrentDirectory = DirPath.Split('~');
                    SearchResult.Add("../" + fileInfoTemp.ToString());
                }
            }

            for (Int32 i = 0; i < DirectoryList.Length; i++)
            {
                FileInfo[] fileInfo = GetFilesFromFolder(DirectoryList[i], (extension == "") ? "aspx" : extension);
                if (fileInfo.Length > 0)
                {
                    foreach (FileInfo fileInfoTemp in fileInfo)
                    {
                        string DirPath = DirectoryList[i].Replace("\\", "~");
                        string[] CurrentDirectory = DirPath.Split('~');
                        if (fileInfoTemp.ToString().Trim().ToUpper().Contains("CONTENTMANAGEMENTDASHBOARD.ASPX"))
                        {
                            // Find the pages and add here
                            DataTable ContentData = ServicesFactory.DocCMSServices.Fetch_Page_name();
                            if (ContentData != null && ContentData.Rows.Count > 0)
                            {
                                foreach (DataRow PageName in ContentData.Rows)
                                {
                                    SearchResult.Add("/" + Convert.ToString(CurrentDirectory[CurrentDirectory.Length - 1]) + "/" + fileInfoTemp.ToString() + "?PID=" + PageName["PageName"] + "_" + PageName["PageID"]);
                                }
                            }
                        }
                        else
                        {
                            SearchResult.Add("/" + Convert.ToString(CurrentDirectory[CurrentDirectory.Length - 1]) + "/" + fileInfoTemp.ToString());
                        }
                    }
                }
            }
            foreach (string str in SearchResult)
            {
                if (str.ToUpper().Contains(SearchName.ToUpper()))
                {
                    FilterredList.Add(str);
                }
            }
            return FilterredList;
        }

        static FileInfo[] GetFilesFromFolder(string folderName, string SearchName)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(folderName);
            FileInfo[] fileInfo = directoryInfo.GetFiles("*.aspx");
            return fileInfo;

        }

        protected void ddlModuleName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_SubModules_Dorder(Convert.ToInt32(ddlModuleName.SelectedValue)) + 1;
            txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
            Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
            ModuleID = ddlModuleName.SelectedValue;
        }

        //For AutoComplete function for SubModule navigation
        [WebMethod]
        public static string GetKeyWords_btnClick(string SearchName)
        {
            string extension = SearchName;
            List<string> SearchResult = new List<string>();
            List<string> FilterredList = new List<string>();
            var rootPath = HostingEnvironment.ApplicationPhysicalPath;
            string[] DirectoryList = Directory.GetDirectories(rootPath);
            FileInfo[] RootfileInfo = GetFilesFromFolder(rootPath, (extension == "") ? "aspx" : extension);
            if (RootfileInfo.Length > 0)
            {
                foreach (FileInfo fileInfoTemp in RootfileInfo)
                {
                    string DirPath = rootPath.Replace("\\", "~");
                    string[] CurrentDirectory = DirPath.Split('~');
                    SearchResult.Add("../" + fileInfoTemp.ToString());
                }
            }

            for (Int32 i = 0; i < DirectoryList.Length; i++)
            {
                FileInfo[] fileInfo = GetFilesFromFolder(DirectoryList[i], (extension == "") ? "aspx" : extension);
                if (fileInfo.Length > 0)
                {
                    foreach (FileInfo fileInfoTemp in fileInfo)
                    {
                        string DirPath = DirectoryList[i].Replace("\\", "~");
                        string[] CurrentDirectory = DirPath.Split('~');
                        if (fileInfoTemp.ToString().Trim().ToUpper().Contains("CONTENTMANAGEMENTDASHBOARD.ASPX"))
                        {
                            // Find the pages and add here
                            DataTable ContentData = ServicesFactory.DocCMSServices.Fetch_Page_name();
                            if (ContentData != null && ContentData.Rows.Count > 0)
                            {
                                foreach (DataRow PageName in ContentData.Rows)
                                {
                                    SearchResult.Add("/" + Convert.ToString(CurrentDirectory[CurrentDirectory.Length - 1]) + "/" + fileInfoTemp.ToString() + "?PID=" + PageName["PageName"] + "_" + PageName["PageID"]);
                                }
                            }
                        }
                        else
                        {
                            SearchResult.Add("/" + Convert.ToString(CurrentDirectory[CurrentDirectory.Length - 1]) + "/" + fileInfoTemp.ToString());
                        }
                    }
                }
            }
            foreach (string str in SearchResult)
            {
                if (str.ToUpper().Contains(SearchName.ToUpper()))
                {
                    FilterredList.Add(str);
                }
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string output = jss.Serialize(FilterredList);
            return output;
        }
        // Bind Display order at toggle inside RptrDisplayOrder Repeater
        protected void Bind_DisplayOrder(Int32 ModuleId)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SubModules_ByModuleId(ModuleId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }
        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Clssubmodules> lstSubModules = new List<Clssubmodules>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblSubModuleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSubModuleID");
                Label lblSubModuleName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSubModuleName");
                if (Session["UserID"] != null && lblSubModuleName != null && lblSubModuleID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblSubModuleName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);

            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                        return;
                    }

                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    return;
                }



            }

            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Clssubmodules objSubModules = new Clssubmodules();
                Label lblSubModuleID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblSubModuleID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblSubModuleID != null)
                {
                    objSubModules.submoduleid = Convert.ToInt32(lblSubModuleID.Text.Trim());
                    objSubModules.displayorder = Convert.ToInt32(txtDorder.Value);
                    objSubModules.lastmodifiedby = Guid.Parse(UserId);
                    lstSubModules.Add(objSubModules);
                }
            }
            if (lstSubModules != null && lstSubModules.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_SubModule_Dorder(lstSubModules);
                if (retVal > 0)
                {
                    if (Request.QueryString["ID"] != null && Request.QueryString["MID"] != null)
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["ID"] + "," + Request.QueryString["MID"] + ");</script>");
                        Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    }
                    else
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg1();</script>");
                        Bind_DisplayOrder(Convert.ToInt32(ddlModuleName.SelectedValue));
                    }
                }
            }
        }
    }
}
