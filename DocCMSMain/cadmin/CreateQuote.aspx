﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="CreateQuote.aspx.cs" Inherits="DocCMSMain.cadmin.CreateQuote" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[2, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        //Function to view detail of user 
        function ViewDetails(QuoteDetailID) {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Fetch_PartDetail_ByOrderDetalID",
                contentType: 'application/json; charset=utf-8',
                data: { "QuoteDetailID": QuoteDetailID.toString() },
                dataType: "json",
                success: ajaxSucceeded,
                error: ajaxFailed
            });
            function ajaxSucceeded(data) {
                if (data != null) {
                    var result = jQuery.parseJSON(data);
                    if (result.length > 0) {
                        jQuery("#txtProductName").val(result[0].ProductName);
                        jQuery("#txtInternalDescription").val(result[0].InternalDescription);
                        jQuery("#txtUnitPrice").val(result[0].Price);
                        var PCost = result[0].Cost;
                        if (PCost == "0.00" || PCost == "" || PCost == "0") {
                            jQuery("#txtCost").val('N/A');
                        }
                        else {
                            jQuery("#txtCost").val(result[0].Cost);
                        }
                        jQuery("#txtQuantity").val(result[0].Quantity);
                        jQuery("#txtAmount").val(result[0].RunningPrice);
                        jQuery("#hdnOrderDetailID").val(result[0].QuoteDetailID);
                        jQuery("#txtdiscountPrice").val(result[0].DiscountedPrice);
                        jQuery("#txtdiscountAmount").val(result[0].DiscountedAmount);
                        jQuery("#txtDiscountPercentage").val(result[0].DiscountedPercentage);
                    }
                }
                else {
                }
            }
            function ajaxFailed() {
              alert("There is an error during operation.");
            }
        }
    </script>
  <script type="text/javascript">
    //Function to check weather input is number or not
    function checkDecimal(obj, objStr) {
        var objNumber;
        if (isNaN(objStr) && objStr != '') {
            alert('Value entered is not numeric');
            objNumber = '0.00';
        }
        else if (objStr == '') {
            objNumber = '';
        }
        else if (objStr.indexOf('.') != -1) {
            if (((objStr.length) - (objStr.indexOf('.'))) > 3) {
                objStr = objStr.substr(0, ((objStr.indexOf('.')) + 3));
            }
            if (objStr.indexOf('.') == 0) {
                objStr = '0' + objStr;
            }
            var sLen = objStr.length;
            var TChar = objStr.substr(sLen - 3, 3);
            if (TChar.indexOf('.') == 0) {
                objNumber = objStr;
            }
            else if (TChar.indexOf('.') == 1) {
                objNumber = objStr + '0';
            }
            else if (TChar.indexOf('.') == 2) {
                objNumber = objStr + '00';
            }
        }
        else {
            objNumber = objStr + '.00';
        }
        obj.value = objNumber;
        if (obj.value != '') {
            var UnitPrice = jQuery("#txtUnitPrice").val();
            var DiscountedPrice = jQuery("#txtdiscountPrice").val();
            var ItemQuantity = jQuery("#txtQuantity").val();
            var DiscountedAmount = DiscountedPrice * ItemQuantity;
            var rounded = DiscountedAmount.toFixed(2);
            jQuery("#txtdiscountAmount").val(rounded);
            var TotalDiscount = UnitPrice - DiscountedPrice;
            var DiscountPercentage = (TotalDiscount / UnitPrice) * 100;
            jQuery("#txtDiscountPercentage").val(DiscountPercentage.toFixed(2));
        }
    }
 </script>
 <script type="text/javascript">
    function ShowCommentDiv() {
       var x = document.getElementById("divNewComment");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>
<style type="text/css">
.input-large 
{
   width: 64%;
}
</style>
 <script type="text/jscript">
        //Enter Discounted percentage function
         function EnterDiscoutedPercentage() {
            if (jQuery("#ChkDiscountedPercentage").is(':checked')) {
                jQuery("#txtDiscountPercentage").attr('readonly', false);
            }
            else {
                jQuery("#txtDiscountPercentage").attr('readonly', true);
            }
        }
        //Enter Discounted price function
        function EnterDiscoutedPrice() {
            if (jQuery("#ChkDiscountedPrice").is(':checked')) {
                jQuery("#txtdiscountPrice").attr('readonly', false);
           }
            else {
                jQuery("#txtdiscountPrice").attr('readonly', true);

            }
        }
        //Enter order Quantity function
        function EnterOrderQuatity() {
            if (jQuery("#ChkOrderQuantity").is(':checked')) {
                jQuery("#txtQuantity").attr('readonly', false);
            }
            else {
                jQuery("#txtQuantity").attr('readonly', true);
            }
        }

        //function on change of Order Quantity
        function ChangeQuantity(objStr) {
            var UnitPrice = jQuery("#txtUnitPrice").val();
            var TotalAmount = objStr * UnitPrice;
            jQuery("#txtAmount").val(TotalAmount.toFixed(2));

        if (jQuery("#txtdiscountPrice").val() != "") {
                var DiscountedPrice = jQuery("#txtdiscountPrice").val();
                var DiscountedAmount = objStr * DiscountedPrice;
                jQuery("#txtdiscountAmount").val(DiscountedAmount);
                var rounded = DiscountedAmount.toFixed(2);
                jQuery("#txtdiscountAmount").val(rounded);
                var TotalDiscount = UnitPrice - DiscountedPrice;
                var DiscountPercentage = (TotalDiscount / UnitPrice) * 100;
                jQuery("#txtDiscountPercentage").val(DiscountPercentage.toFixed(2));
            }
        }
        // change percentage function
        function ChangePercentage(objStr) {
            var TotalQuantity = jQuery("#txtQuantity").val();
            var DiscountedPercentage = objStr;
            var UnitPrice = jQuery("#txtUnitPrice").val();
            var DiscountOnPrice = (DiscountedPercentage * UnitPrice) / 100;
            var DiscountPrice = UnitPrice - DiscountOnPrice;
            jQuery("#txtdiscountPrice").val(DiscountPrice.toFixed(2));
            var TotalDiscountedAmount = DiscountPrice * TotalQuantity;
            jQuery("#txtdiscountAmount").val(TotalDiscountedAmount.toFixed(2));
        }
</script>
  <%--function to add new item to data base--%>
  <script type="text/javascript">
        //function to add new product
        function AddNewProduct(ImgCtrl, CMSID) {
        var categoryname = jQuery(ImgCtrl).closest("tr").find("#hdnProductCategoryName").val();
        var ProductName = jQuery(ImgCtrl).closest("tr").find("#lblPartNo").html();
        var ProductNumber = jQuery(ImgCtrl).closest("tr").find("#lblProductNumber").val();
        var InternalDescription = jQuery(ImgCtrl).closest("tr").find("#hdnInternalDescription").val();
        var description = jQuery(ImgCtrl).closest("tr").find("#lblDescription").html();
        var Price = jQuery(ImgCtrl).closest("tr").find("#lblProductPrice").html();
        var Cost = jQuery(ImgCtrl).closest("tr").find("#hdnCost").html();
        if (Cost == "" || Cost == null) {
            Cost = 0;
        }
        var quantity = jQuery(ImgCtrl).closest("tr").find("#txtEnterOrderQuantity").val();
        var amount = Price * quantity;
        var purchaseorderid = jQuery("#hdnPurchaseOrderID").val();
        var Cls_OrderDetail =
    {
        ProductName: ProductName,
        ProductNumber: ProductNumber,
        InternalDescription: InternalDescription,
        CategoryName: categoryname,
        Quantity: quantity,
        Description: description,
        Cost: Cost,
        Price: Price,
        RunningPrice: amount,
        PurchaseOrderID: purchaseorderid
    };

      var objOrderDetail = JSON.stringify(Cls_OrderDetail);
      jQuery.ajax({
        url: "../WebService/DocCMSApi.svc/Insert_New_Product_fromCMS",
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(Cls_OrderDetail),
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null && data > 0) {
            alert("Item Added Sucessfully");
            location.reload(true);
        }
        else {
            alert("This item is already added");
        }
    }
    function ajaxFailed() {
          alert("There is an error during operation.");
        }
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnPurchaseOrderID" runat="server" ClientIDMode="Static" />
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage Detail Quote</a> <span class="separator"></span></li>
        <li>Manage Detail Quote</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Detail Quote</h5>
            <h1>
              Manage Detail Quote</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              Manage Detail Quote
               <asp:Button ID="btnShowCustomerDetail" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Show Customer Detail" 
                    OnClick="btnShowCustomerDetail_Click" />
               <asp:Button ID="btnDownloadQuote" runat="server" CssClass="btn btn-primary" 
                    Text="Download Quote" 
                    style="font-size: 13px; width: 149px; margin-left:935px; margin-top:2px"
                    OnClick="btnDownloadQuote_Click" />
               <asp:Button ID="btnSendQuote" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Send Quote" 
                    Style="margin-top:-35px; margin-left:1400px; float:left;" 
                    OnClick="btnSendQuote_Click" />
               <asp:Button ID="btnAddNewProduct" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Add New Product" 
                    Style="margin-top:-35px; margin-left:1061px; float:left;" 
                    OnClick="btnAddNewProduct_Click" />
            </h4>
              <asp:Panel ID="PanelJob" runat="server">
            <table id="dyntable" class="table table-bordered responsive" >
               <colgroup>
                 <col class="con0" style="align: center; width: 15%;" />
                  <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                      <col class="con1" style="width: 10%;" />
                        <col class="con1" style="width: 10%;" />
                          <col class="con1" style="width: 10%;" />
                           <col class="con1" style="width: 10%;" />
                             <col class="con1" style="width: 10%;" />
                               <col class="con1" style="width: 10%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                          Category
                        </th>
                         <th class="head1">
                           Product Number
                          </th>
                        <th class="head1">
                            Product Name
                          </th>
                         <th class="head1">
                            Quantity Order
                          </th>
                           <th class="head1">
                            Unit Cost
                           </th>
                           <th class="head1">
                          Unit Price
                      </th>
                      <th class="head1">
                       Amount
                       </th>
                      <th class="head1">
                       Discounted Price
                      </th>
                      <th class="head1">
                       Discounted Amount
                      </th>
                      <th class="head1">
                       Discounted Percentage
                      </th>
                       <th class="head1">
                       Edit Quote
                       </th>
                 </tr>
                </thead>
                <tbody>
                   <asp:Repeater runat="server" ID="RptrOrder" OnItemCommand="RptrOrder_ItemCommand"
                        OnItemDataBound="RptrOrder_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductNumber")%>' ID="Label3"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductName")%>' ID="Label1"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("Quantity")%>' ID="lblOrderType"></asp:Label>
                                </td>
                                 <td>
                                 <span id="spanCostDollar" runat="server">$</span><asp:Label runat="server" Text='<%# Eval("Cost")%>' ID="lblCost"></asp:Label>
                                </td>
                               <td>
                                 $<asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("RunningPrice")%>' ID="lblRunningPrice"></asp:Label>
                                </td>
                                    <td>
                                 <span id="spanDiscountPriceDollar" runat="server">$</span><asp:Label runat="server" Text='<%# Eval("DiscountedPrice")%>' ID="lblDiscountPrice"></asp:Label>
                                </td>
                                <td>
                                 <span id="spanDiscountAmountDollar" runat="server">$</span><asp:Label runat="server" Text='<%# Eval("DiscountedAmount")%>' ID="lblDiscountAmount"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("DiscountedPercentage")%>' ID="lblDiscountPercentage"></asp:Label><span  id="spanDiscountPercentage" runat="server" >%</span>
                                </td>
                                <td>
                                <input type="image"  id="lblShowTimings" title="Edit Module" data-toggle="modal" data-target="#myModal" src="../images/table_icon_1.gif" onclick='return ViewDetails(<%# Eval("QuoteDetailID") %>)' />
                               </td>
                             </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                                 <tr class="gradeX">
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductNumber")%>' ID="Label3"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductName")%>' ID="Label1"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("Quantity")%>' ID="lblOrderType"></asp:Label>
                                </td>
                                  <td>
                                  <span id="spanCostDollar" runat="server">$</span><asp:Label runat="server" Text='<%# Eval("Cost")%>' ID="lblCost"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("RunningPrice")%>' ID="lblRunningPrice"></asp:Label>
                                </td>
                                <td>
                                 <span id="spanDiscountPriceDollar" runat="server">$</span><asp:Label runat="server" Text='<%# Eval("DiscountedPrice")%>' ID="lblDiscountPrice"></asp:Label>
                                </td>
                                <td>
                                 <span id="spanDiscountAmountDollar" runat="server">$</span><asp:Label runat="server" Text='<%# Eval("DiscountedAmount")%>' ID="lblDiscountAmount"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("DiscountedPercentage")%>' ID="lblDiscountPercentage"></asp:Label><span  id="spanDiscountPercentage" runat="server" >%</span>
                                </td>
                                <td>
                                <input type="image"  id="lblShowTimings" title="Edit Module" data-toggle="modal" data-target="#myModal" src="../images/table_icon_1.gif" onclick='return ViewDetails(<%# Eval("QuoteDetailID") %>)' />
                                </td>
                             </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
             <%--Customer Detail  strat here --%>  
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                 Quote Detail
                                     <a href="" id="btnCloseDisplayOrder" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table1" class="table table-bordered responsive" style="width: 100%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 30%;" />
                                    <col class="con0" style="align: right; width: 70%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Content
                                        </th>
                                       <th>
                                            Detail
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrCompleteDetail">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblContent" Text='<%# Eval("Quote")%>' runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                     <asp:Label ID="Label1" Text='<%# Eval("QuoteDetail")%>' runat="server" ></asp:Label>
                                                </td>
                                             </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                <td>
                                                    <asp:Label ID="lblContent" Text='<%# Eval("Quote")%>' runat="server"></asp:Label>
                                               </td>
                                                <td>
                                                     <asp:Label ID="Label1" Text='<%# Eval("QuoteDetail")%>' runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                       </div>
                    </div>
                </div>
            </div>
            <%--Customer Detail  ends here --%> 
             <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
            <%--Customer Detail  ends here --%>  
             <%--Add new Product strat here --%>  
              <div id="pnlAddNewProduct" class="modal fade in" style="display: none;width: 50%;left:650 !important;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="btnClosepnl" class="modal-title">
                                 Product List
                                     <a href="" id="A1" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table2" class="table table-bordered responsive" style="width: 100%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 25%;" />
                                    <col class="con0" style="align: right; width: 15%;" />
                                    <col class="con1" style="align: left; width: 15%;" />
                                    <col class="con0" style="align: right; width: 15%;" />
                                    <col class="con0" style="align: right; width: 5%;" />
                                    <col class="con0" style="align: right; width: 15%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Product Image
                                        </th>
                                       <th>
                                            Product Number
                                        </th>
                                         <th>
                                            Product Name
                                        </th>
                                        <th>
                                            Description
                                        </th>
                                          <th>
                                            Price
                                        </th>
                                        <th>
                                           Enter Quantity
                                        </th>
                                        <th>
                                           Add
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                   <asp:Repeater runat="server" ID="RptrParts" OnItemDataBound="RptrParts_ItemDataBound" >
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                   <asp:HiddenField ID="hdnProductCategoryName" runat="server" Value='<%#Eval("CategoryName")%>' ClientIDMode="Static"/>                                                
                                                    <asp:Label ID="lblImageName" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                                                     <img id="myImg"   clientidmode="Static" runat="server"  style="width:120px; Height:120px;" src='<%# "../UploadedFiles/ContentImages/Catalogue/" + Eval("ImageName") %>'  title='<%# Eval("Description") %>' alt='<%# Eval("Description") %>' width="300" height="200">
                                                </td>
                                                 <td>
                                                    <asp:Label ID="lblProductNumbera" runat="server" Text='<%# Eval("ProductNumber") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' CssClass="DorderLabel" ClientIDMode="Static" Visible="false"></asp:Label>
                                                    <asp:HiddenField ID="lblProductNumber"  runat="server" Value='<%# Eval("ProductNumber") %>' ClientIDMode="Static" ></asp:HiddenField>
                                                    <asp:HiddenField ID="hdnInternalDescription"  runat="server" Value='<%# Eval("InternalDescription") %>' ClientIDMode="Static"></asp:HiddenField>
                                                    <asp:Label ID="lblPartNo" runat="server" Text='<%# Eval("ProductName") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                                <td>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                              </td>
                                              <td>
                                                <asp:Label ID="lblProductPrice" runat="server" Text='<%# Eval("Price") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                <asp:HiddenField ID="hdnCost"  runat="server" Value='<%# Eval("Cost") %>' ClientIDMode="Static" ></asp:HiddenField>

                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtEnterOrderQuantity" ClientIDMode="Static" runat="server"></asp:TextBox>
                                              </td>
                                              <td>
                                               <img id="imgAdd" alt="Add To Cart" src="../DFI/images/AddToCart1.png" title="Click here to add this item" style="margin: 0; height: 25px; width: 25px;
                                                            padding: 3px; cursor: pointer;"  onclick="AddNewProduct(this,'<%#Eval("CMSID")%>')" />
                                              </td>
                                            </tr>
                                        </ItemTemplate>
                                   </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                       </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="MpeFakeProductList" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="AddProductList" runat="server" PopupControlID="pnlAddNewProduct"
                TargetControlID="MpeFakeProductList" CancelControlID="btnClosepnl" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
          <%--Add new Product ends here --%>  
          </asp:Panel>
          <div  style="width:200px; float:right; text-align:center; background-color:#0F5B96; color:#fff;font-size:17px; padding:5px;">
              <span ID="divTotal" runat="server"></span><br />
          </div>
             <br />
             <br />
              <div  style="width:200px; float:right; text-align:center; background-color:#5cb85c; color:#fff;font-size:17px; padding:5px;">
             <span ID="divDiscountedTotal" runat="server" clientidmode="Static"></span>
              </div>
            <br />
            <br />
            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Width="200"
                Text="Back To Report Dashboard" OnClick="btnback_Click" />
            <asp:Button ID="btnComment" runat="server" CssClass="btn btn-primary" Width="200"
                Text="Insert Comment" onclick="btnComment_Click"    />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--pageheader-->
    <!-- Modal edit start-->
    <div class="modal fade" id="myModal" role="dialog" style="width:40%">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item Detail</h4>
        </div>
        <div class="modal-body">
                      <div class="stdformQuote">
                        <div class="par control-group">
                            <label class="control-label" for="txtModuleName">
                                Product Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtProductName" id="txtProductName" class="input-large"
                                    clientidmode="Static"  readonly />
                                <input type="hidden" runat="server" name="hdnOrderDetailID" id="hdnOrderDetailID"
                                    clientidmode="Static"  />
                            </div>
                        </div>
                       <div class="par control-group">
                            <label class="control-label" for="txtInternalDescription">
                               Internal Description</label>
                            <div class="controls">
                                <textarea id="txtInternalDescription" name="txtInternalDescription" readonly rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="height: 100px; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" ></textarea>
                             </div>
                        </div>
                            <div class="par control-group">
                            <label class="control-label" for="txtCost">
                               Unit Cost </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtCost" readonly id="txtCost"
                                    class="input-large"   clientidmode="Static" />$
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtUnitPrice">
                                Unit Price </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtUnitPrice" readonly id="txtUnitPrice" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Your Price"  clientidmode="Static" />$
                           </div>
                        </div>
                      <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                                Quantity </label>
                            <div class="controls">
                                <input type="text" runat="server" clientidmode="Static" name="txtQuantity" onblur="ChangeQuantity(this.value)" readonly id="txtQuantity" 
                                    class="input-large"  />
                              <input type="checkbox" runat="server" onchange="EnterOrderQuatity()"  name="activestatus"  id="ChkOrderQuantity" clientidmode="Static" class="input-large"
                                    style="opacity: 1"  />  
                            </div>
                        </div>
                         <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                               Total Amount </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtAmount" clientidmode="Static" readonly id="txtAmount" onkeypress="return onlyNumbers(event)"
                                    class="input-large"   />$
                             </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtdiscountPrice">
                               Enter Discount Price </label>
                            <div class="controls">
                                <input type="text" onFocus="this.focus();" onBlur="checkDecimal(this,this.value)" runat="server" readonly name="txtdiscountPrice"  id="txtdiscountPrice" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Discount Price"  clientidmode="Static" />$
                                    &nbsp;<input type="checkbox" runat="server" onchange="EnterDiscoutedPrice()"  name="activestatus"  id="ChkDiscountedPrice" clientidmode="Static" class="input-large"
                                    style="opacity: 1"  /> 
                            </div>
                        </div>
                          <div class="par control-group">
                            <label class="control-label" for="txtDiscountPercentage">
                               Discount Percentage</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtDiscountPercentage" onblur="ChangePercentage(this.value)" readonly id="txtDiscountPercentage" onkeypress="return onlyNumbers(event)"
                                    class="input-large"  clientidmode="Static" />%
                              &nbsp;<input type="checkbox" runat="server" onchange="EnterDiscoutedPercentage()" name="activestatus"  id="ChkDiscountedPercentage" clientidmode="Static" class="input-large"
                                    style="opacity: 1"  /> 
                            </div>
                        </div>
                            <div class="par control-group">
                            <label class="control-label" for="txtdiscountAmount">
                              Discount Amount </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtdiscountAmount" clientidmode="Static" readonly id="txtdiscountAmount" 
                                    class="input-large"   />$
                            </div>
                        </div>
                        <p class="stdformbutton">
                         <asp:Button ID="btnUpdateQuote" runat="server" class="btn btn-primary" Text="Update" Width="150px" ValidationGroup="grpsubmit" OnClick="btnUpdateQuote_Click" />
                        </p>
                    </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btncancel" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     </div>
   </div>
    <!-- Modal edit ends-->
    <!--maincontent-->
</asp:Content>
