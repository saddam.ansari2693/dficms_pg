﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;

namespace DocCMSMain.cadmin
{
    public partial class Privileges : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);

            if (!IsPostBack)
            {
                if (Request.QueryString["PID"] != null)
                {
                    Bind_Privilege_Data(Convert.ToInt32(Request.QueryString["PID"]));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else
                {
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        // Bind Privilege function data when Privilege id is already existing
        protected void Bind_Privilege_Data(Int32 PrivilgeId)
        {
            try
            {
                Clsprivileges objPrivilege = ServicesFactory.DocCMSServices.Fetch_privileges_Byid(PrivilgeId);
                if (objPrivilege != null)
                {
                    txtPrivilegeName.Value = objPrivilege.privilegename;
                    txtDisplayOrder.Value = Convert.ToString(objPrivilege.displayorder);
                    ChkActiveStatus.Checked = objPrivilege.active;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Clsprivileges clsPrivileges= new Clsprivileges();
                clsPrivileges.privilegename = txtPrivilegeName.Value;
                clsPrivileges.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsPrivileges.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsPrivileges.active = true;
                else
                    clsPrivileges.active = false;

                Int32 retVal = ServicesFactory.DocCMSServices.Insert_privileges(clsPrivileges);
                if (retVal > 0)
                {
                    ServicesFactory.DocCMSServices.Adjust_privileges_dorder();
                }
                Response.Redirect("~/cadmin/PrivilegesDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Clsprivileges clsPrivileges = new Clsprivileges();
                clsPrivileges.privilegename = txtPrivilegeName.Value;
                clsPrivileges.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsPrivileges.lastmodifiedby = Guid.Parse(UserId);
                clsPrivileges.privilgeid= Convert.ToInt32(Request.QueryString["PID"]);
                if (ChkActiveStatus.Checked)
                    clsPrivileges.active = true;
                else
                    clsPrivileges.active = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_privileges(clsPrivileges);
                if (retVal > 0)
                {
                    ServicesFactory.DocCMSServices.Adjust_privileges_dorder();
                }
                Response.Redirect("~/cadmin/PrivilegesDashboard.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/PrivilegesDashboard.aspx");
        }
    }
}