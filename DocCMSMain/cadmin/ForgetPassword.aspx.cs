﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Web.Security;
using System.Net.Mail;
using DocCMS.Core.DataTypes;
using System.Net;
using System.IO;

namespace DocCMSMain.cadmin
{
    public partial class ForgetPassword : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                labyear.Text = DateTime.Now.Year.ToString();
            }
        }


        protected void BtnemailContinue_Click(object sender, EventArgs e)
        {
            bool MesssageSucess = SendMail();
            if (MesssageSucess == true)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>FireMessage(1);</script>");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>FireMessage(2);</script>");
            }
        }


        //Create Method for Send Mail
        public bool SendMail()
        {
            bool MessageSucess = true;
            try
            {
                Cls_UserProfiles clsUser = ServicesFactory.DocCMSServices.Fetch_User_Details_Byid(txtusername.Value);
                if (clsUser != null)
                {
                    Cls_SMTP objSMTP = ServicesFactory.DocCMSServices.Fetch_SMTP_Detail_By_department("Support");
                    if (objSMTP != null)
                    {
                        string[] ToMuliId = null;
                        string UserPassword = ServicesFactory.DocCMSServices.Decrypt(Convert.ToString(clsUser.password));
                        string subject = "Forgot Password";
                        MailMessage mail = new MailMessage();
                        mail.IsBodyHtml = true;
                        mail.From = new MailAddress(objSMTP.senderemailid);
                        mail.Subject = subject;
                        mail.To.Add(new MailAddress(txtusername.Value.Trim()));
                        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ForgotPassword.htm"));
                        string readFile = reader.ReadToEnd();
                        string StrContent = "";
                        StrContent = readFile;
                        mail.Body = readFile;
                        if (mail.Body != null)
                        {
                             mail.Body = mail.Body.ToString()
                            .Replace("<%UserName%>", Convert.ToString(clsUser.fname + " " + clsUser.lname))
                            .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                            .Replace("<%EmailId%>", Convert.ToString(txtusername.Value))
                            .Replace("<%Password%>", Convert.ToString(UserPassword));
                        }
                        MessageSucess = ServicesFactory.DocCMSServices.Send_Mail(mail, objSMTP);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Internal error occcured');</script>");
                }
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Invalid user email id');</script>");
            }
            catch (Exception ex)
            {
                MessageSucess = false;
            }
            return MessageSucess;
        }
    }
}