﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Net.Mail;

namespace DocCMSMain.cadmin
{
    public partial class CreateQuote : System.Web.UI.Page
    {
        dynamic UserId;
        string GrandTotal;
        public bool IsValid = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            Cls_Order clsOrder = null;
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["PurchaseOrderID"] != null)
                {
                    hdnPurchaseOrderID.Value = Request.QueryString["PurchaseOrderID"];
                    Bind_Order();
                    clsOrder = ServicesFactory.DocCMSServices.Fetch_Order_List_By_id(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                    if (clsOrder != null)
                    {
                         GrandTotal = ServicesFactory.DocCMSServices.Fetch_Grand_Total_from_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                         divTotal.InnerText = "Total: $" + GrandTotal;
                        string DiscountedAmount=ServicesFactory.DocCMSServices.Fetch_Discounted_Total(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        if (DiscountedAmount == "" || DiscountedAmount == "0")
                        {
                            divDiscountedTotal.InnerText = "Discounted Total: N/A";
                        }
                        else
                        divDiscountedTotal.InnerText = "Discounted Total: $" + DiscountedAmount;
                    }
                }
            }
            GrandTotal = ServicesFactory.DocCMSServices.Fetch_Grand_Total_from_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
            divTotal.InnerText = "Total: $" + GrandTotal;
            Bind_Order();
        }
        // Bind Enquiry Details into RptrEnquiry Repeater
        protected void Bind_Order()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrOrder.DataSource = dt;
                    RptrOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void RptrOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }


        protected void RptrOrder_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblDiscountPrice = (Label)e.Item.FindControl("lblDiscountPrice");
                Label lblDiscountAmount = (Label)e.Item.FindControl("lblDiscountAmount");
                Label lblDiscountPercentage = (Label)e.Item.FindControl("lblDiscountPercentage");
                Label lblCost = (Label)e.Item.FindControl("lblCost");
                HtmlGenericControl spanDiscountPriceDollar = (HtmlGenericControl)e.Item.FindControl("spanDiscountPriceDollar");
                HtmlGenericControl spanCostDollar = (HtmlGenericControl)e.Item.FindControl("spanCostDollar");
                HtmlGenericControl spanDiscountAmountDollar = (HtmlGenericControl)e.Item.FindControl("spanDiscountAmountDollar");
                HtmlGenericControl spanDiscountPercentage = (HtmlGenericControl)e.Item.FindControl("spanDiscountPercentage");
                if (lblDiscountPrice.Text == "" || lblDiscountPrice.Text == null)
                {
                    lblDiscountPrice.Text = "N/A";
                    spanDiscountPriceDollar.Visible = false;
                }
                if (lblDiscountAmount.Text == "" || lblDiscountAmount.Text == null)
                {
                    lblDiscountAmount.Text = "N/A";
                    spanDiscountAmountDollar.Visible = false;
                }
                if (lblDiscountPercentage.Text == "" || lblDiscountPercentage.Text == null)
                {
                    lblDiscountPercentage.Text = "N/A";
                    spanDiscountPercentage.Visible = false;
                }
                if (lblCost.Text == "" || lblCost.Text == null ||lblCost.Text=="0.00" ||lblCost.Text=="0")
                {
                    lblCost.Text = "N/A";
                    spanCostDollar.Visible = false;
                }
            }
        }

        // Button Back 
        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/QuoteDashboard.aspx");
        }

        // Quote Update function
        protected void btnUpdateQuote_Click(object sender, EventArgs e)
        {
            Cls_QuoteDetail cls = new Cls_QuoteDetail();
            Int32 Retvalueint=0;
            DataTable dtresult = ServicesFactory.DocCMSServices.Fetch_Order_Detail_By_orderdetailid(Convert.ToInt32(hdnOrderDetailID.Value));
            if (dtresult != null && dtresult.Rows.Count > 0)
            {
                cls.quotedetailid = Convert.ToInt32(dtresult.Rows[0]["QuoteDetailID"]);
                cls.purchaseorderid = Convert.ToInt32(dtresult.Rows[0]["PurchaseOrderID"]);
                cls.categoryname = Convert.ToString(dtresult.Rows[0]["CategoryName"]);
                cls.productnumber = Convert.ToString(dtresult.Rows[0]["ProductNumber"]);
                cls.internaldescription = Convert.ToString(dtresult.Rows[0]["InternalDescription"]);
                cls.productname = Convert.ToString(dtresult.Rows[0]["ProductName"]);
                cls.quantity = Convert.ToInt32(txtQuantity.Value);
                cls.description = Convert.ToString(dtresult.Rows[0]["Description"]);
                cls.price = Convert.ToString(dtresult.Rows[0]["Price"]);
                cls.size = Convert.ToString(dtresult.Rows[0]["Size"]);
                cls.runningprice = txtAmount.Value;
                cls.color = Convert.ToString(dtresult.Rows[0]["Color"]);
                cls.discountedprice = txtdiscountPrice.Value;
                cls.discountedamount = txtdiscountAmount.Value;
                cls.discountedpercentage = txtDiscountPercentage.Value;
                Retvalueint = ServicesFactory.DocCMSServices.Update_CustomQuote_Detail_ByOrder_Detailid(cls);
                if (Retvalueint > 0)
                {
                    Response.Redirect("~/cadmin/CreateQuote.aspx?PurchaseOrderID=" + Request.QueryString["PurchaseOrderID"] + "");
                }
            }
        }

        //Comment function
        protected void btnComment_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/QuoteComment.aspx?PurchaseOrderID=" + Request.QueryString["PurchaseOrderID"] + "");
        }
        
        // Send quote to user through email
        protected void btnSendQuote_Click(object sender, EventArgs e)
        {
            Cls_Order clsOrder = new Cls_Order();
            clsOrder = ServicesFactory.DocCMSServices.Fetch_Order_List_By_id(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
            clsOrder.requesttype = "Quote Request";
            Create_PDF_For_Order(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]), clsOrder.requesttype);
            IsValid = SendMail(clsOrder, Convert.ToInt32(Request.QueryString["PurchaseOrderID"]), clsOrder.requesttype);
        }

        // Function to download quote
        protected void btnDownloadQuote_Click(object sender, EventArgs e)
        {
            Update_Pdf(true);//Bind_Pdf();
        }

        //Send Mail
        public bool SendMail(Cls_Order clsOrder, Int32 PurchaseOrderID, string RequestType)
        {

            try
            {
                string subject = "";
                string File = "";
                Cls_SMTP objSMTP = ServicesFactory.DocCMSServices.Fetch_SMTP_Detail_By_department("Sales");
                if (objSMTP != null)
                {
                    string[] ToMuliId = null;
                    if (RequestType == "Order Request")
                    {
                        subject = "Request for Order";
                    }
                    else
                    {
                        subject = "Request for Quote";
                    }
                    //**************Mail To Admin starts from here********************
                    MailMessage mailToAdmin = new MailMessage();
                    mailToAdmin.From = new MailAddress(objSMTP.senderemailid.Trim());
                    mailToAdmin.IsBodyHtml = true;
                    if (!string.IsNullOrEmpty(objSMTP.toemail))
                    {
                        ToMuliId = objSMTP.toemail.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ToEMailId in ToMuliId)
                        {
                            mailToAdmin.To.Add(new MailAddress(ToEMailId)); //adding multiple TO Email Id
                        }
                    }
                    if (!string.IsNullOrEmpty(objSMTP.bccemail))
                    {
                        ToMuliId = objSMTP.bccemail.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ToBcc in ToMuliId)
                        {
                            mailToAdmin.Bcc.Add(new MailAddress(ToBcc)); //adding multiple TO Email Id
                        }
                    }
                    if (!string.IsNullOrEmpty(objSMTP.ccemail))
                    {
                        ToMuliId = objSMTP.ccemail.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ToCC in ToMuliId)
                        {
                            mailToAdmin.CC.Add(new MailAddress(ToCC)); //adding multiple TO Email Id
                        }
                    }
                    string IsBackAllowed = "";
                    if (clsOrder.isbackorderallowed)
                        IsBackAllowed = "YES";
                    else
                        IsBackAllowed = "NO";

                    mailToAdmin.Subject = subject;
                    string HTMLPath = "";
                    if (RequestType == "Order Request")
                    {
                        HTMLPath = "~/EmailTemplate/OrderTemplate.htm";
                    }
                    else
                    {
                        HTMLPath = "~/EmailTemplate/QuoteTemplate.htm";
                    }
                    StreamReader reader = new StreamReader(Server.MapPath(HTMLPath));
                    string readFile = reader.ReadToEnd();
                    string StrContent = "";
                    StrContent = readFile;
                    mailToAdmin.Body = readFile;
                    if (RequestType == "Order Request")
                        File = "Order_" + PurchaseOrderID + ".pdf";
                    else
                        File = "Quote_" + PurchaseOrderID + ".pdf";
                    string URL = Request.Url.AbsoluteUri.Replace(Request.RawUrl, "");
                    string Dlink = "href='" + URL + "/UploadedFiles/Order/" + File + "'";
                    // Create  the file attachment for this e-mail message.
                    mailToAdmin.Attachments.Add(new Attachment(Server.MapPath("~/UploadedFiles/Order/" + File)));
                    if (mailToAdmin.Body != null)
                    {
                        mailToAdmin.Body = mailToAdmin.Body.ToString()
                        .Replace("<%Name%>", Convert.ToString("Admin"))
                        .Replace("<%CustomerName%>", Convert.ToString(clsOrder.purchasername))
                        .Replace("<%OrderNumber%>", Convert.ToString(clsOrder.orderno))
                        .Replace("<%OrderDate%>", Convert.ToString(DateTime.Now.ToString("MM/dd/yyyy")))
                        .Replace("<%PaymentType%>", Convert.ToString(clsOrder.paymentmethod))
                        .Replace("<%BackOrder%>", Convert.ToString(IsBackAllowed))
                        .Replace("<%CustomerEmailID%>", Convert.ToString(clsOrder.email))
                        .Replace("<%PhoneNumber%>", Convert.ToString(clsOrder.phone))
                        .Replace("<%Address%>", Convert.ToString(clsOrder.billingaddress1))
                        .Replace("<%City%>", Convert.ToString(clsOrder.billingcity))
                        .Replace("<%Province%>", Convert.ToString(clsOrder.billingprovience))
                        .Replace("<%Pincode%>", Convert.ToString(clsOrder.billingpostalcode))
                        .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                        .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year))
                        .Replace("<%Downloadlink%>", Convert.ToString(Dlink));
                    }
                    IsValid = ServicesFactory.DocCMSServices.Send_Mail(mailToAdmin, objSMTP);
                    //*****Mail To admin ends here*****************
                    //***Mail to Customer starts from here***
                    MailMessage mailToCustomer = new MailMessage();
                    mailToCustomer.IsBodyHtml = true;
                    mailToCustomer.To.Add(clsOrder.email);
                    mailToCustomer.From = new MailAddress(objSMTP.senderemailid);
                    mailToCustomer.Subject = subject;
                    if (RequestType == "Order Request")
                        reader = new StreamReader(Server.MapPath("~/EmailTemplate/OrderTemplate.htm"));
                    else
                        reader = new StreamReader(Server.MapPath("~/EmailTemplate/QuoteTemplate.htm"));
                    readFile = reader.ReadToEnd();
                    StrContent = "";
                    StrContent = readFile;
                    mailToCustomer.Body = readFile;
                    //Attaching File with Mail
                    mailToCustomer.Attachments.Add(new Attachment(Server.MapPath("~/UploadedFiles/Order/" + File)));
                    if (mailToCustomer.Body != null)
                    {
                        mailToCustomer.Body = mailToCustomer.Body.ToString()
                       .Replace("<%Name%>", Convert.ToString(clsOrder.purchasername))
                       .Replace("<%CustomerName%>", Convert.ToString("you"))
                       .Replace("<%OrderNumber%>", Convert.ToString(clsOrder.orderno))
                       .Replace("<%OrderDate%>", Convert.ToString(DateTime.Now.ToString("MM/dd/yyyy")))
                       .Replace("<%PaymentType%>", Convert.ToString(clsOrder.paymentmethod))
                       .Replace("<%BackOrder%>", Convert.ToString(IsBackAllowed))
                       .Replace("<%CustomerEmailID%>", Convert.ToString(clsOrder.email))
                       .Replace("<%PhoneNumber%>", Convert.ToString(clsOrder.phone))
                       .Replace("<%Address%>", Convert.ToString(clsOrder.billingaddress1))
                       .Replace("<%City%>", Convert.ToString(clsOrder.billingcity))
                       .Replace("<%Province%>", Convert.ToString(clsOrder.billingprovience))
                       .Replace("<%Pincode%>", Convert.ToString(clsOrder.billingpostalcode))
                       .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                       .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year))
                       .Replace("<%Downloadlink%>", Convert.ToString(Dlink));
                    }
                    IsValid = ServicesFactory.DocCMSServices.Send_Mail(mailToCustomer, objSMTP);
                    //***Mail to Customer ends here
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Internal error occcured');</script>");
                }
            }
            catch (Exception ex)
            {
                IsValid = false;
                Response.Write(ex);
            }
            return IsValid;
        }

        //Create PDF
        public void Create_PDF_For_Order(Int32 PurchaseOrderID, string RequestType)
        {
            string OrderDirectoryPath = "";
            string PDFFilePath = "";
            try
            {
                if (RequestType == "Order Request")
                    PDFName = "Order_" + PurchaseOrderID;
                else
                    PDFName = "Quote_" + PurchaseOrderID;
                string InvoiceImagepath = Server.MapPath("~/images/Logos/DocLogo2-Copy.png");
                OrderDirectoryPath = "~/UploadedFiles/Order/";
                if (!Directory.Exists(Server.MapPath(OrderDirectoryPath)))
                {
                    Directory.CreateDirectory(Server.MapPath(OrderDirectoryPath));
                }
                string appRootDir = Server.MapPath(OrderDirectoryPath);
                // Step 1: Creating System.IO.FileStream object
                using (FileStream fs = new FileStream(appRootDir + "/" + PDFName.ToString() + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                // Step 2: Creating iTextSharp.text.Document object
                using (Document doc = new Document(PageSize.A4))
                // Step 3: Creating iTextSharp.text.pdf.PdfWriter object
                // It helps to write the Document to the Specified FileStream
                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {// Step 4: Openning the Document
                    writer.CloseStream = false;
                    doc.Open();
                    // Step 5: Adding a paragraph
                    // NOTE: When we want to insert text, then we've to do it through creating paragraph
                    //// TO Add Header Rectangle and text
                    //// select the font properties
                    BaseFont bfHeader = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    // TO SET INVOICE HEADING
                    PdfContentByte cbHeading = writer.DirectContent;
                    // select the font properties
                    BaseFont bfcbHeading = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbHeading.BeginText();
                    cbHeading.SetColorFill(new BaseColor(84, 141, 212));
                    cbHeading.SetFontAndSize(bfHeader, 28);
                    Cls_Order clsOrder = ServicesFactory.DocCMSServices.Fetch_Order_List_By_id(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                    if (clsOrder != null)
                    {
                        cbHeading.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "QUOTE REQUEST", 35, 770, 0);
                        cbHeading.EndText();
                        cbHeading.SetColorFill(new BaseColor(84, 141, 212));
                        cbHeading.Rectangle(35, 760, 530, 2);
                        cbHeading.Fill();
                        //Adding Image to Right Header
                        iTextSharp.text.Image imagecmp = null;
                        using (FileStream fsimage = new FileStream(InvoiceImagepath, FileMode.Open))
                        {
                            imagecmp = iTextSharp.text.Image.GetInstance(fsimage);
                        }
                        imagecmp.SetAbsolutePosition(409, 765);
                        doc.Add(imagecmp);
                        // Adding Image to Right Header Ends
                        PdfContentByte cbCompany = writer.DirectContent;
                        // select the font properties
                        BaseFont bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 8);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Quote Request:"), 40, 745, 0);
                        cbCompany.EndText();

                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.SetFontAndSize(bfcbCompany, 12);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.purchasername), 40, 730, 0);
                        cbCompany.EndText();

                        //Billing address
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        doc.Add(new Paragraph(5, "\u00a0"));
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.Rectangle(40, 677, 170, 40);
                        cbCompany.Fill();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Billing Address"), 40, 720, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.billingaddress1).Trim() + ", " + Convert.ToString(clsOrder.billingaddress2).Trim(), 40, 710, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.billingcity).Trim() + ", " + Convert.ToString(clsOrder.billingpostalcode).Trim(), 40, 700, 0);
                        cbCompany.EndText();

                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.email).Trim(), 40, 690, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Phone :" + Convert.ToString(clsOrder.phone).Trim(), 40, 680, 0);
                        cbCompany.EndText();

                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.Rectangle(425, 695, 140, 40);
                        cbCompany.Fill();
                        // Billing Address Ends

                        //Shipping address
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        doc.Add(new Paragraph(5, "\u00a0"));
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.Rectangle(190, 677, 200, 40);
                        cbCompany.Fill();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Shipping Address"), 240, 720, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.shippingaddress1).Trim() + ", " + Convert.ToString(clsOrder.shippingaddress2).Trim(), 240, 710, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.shippingcity).Trim() + ", " + Convert.ToString(clsOrder.shippingpostalcode).Trim(), 240, 700, 0);
                        cbCompany.EndText();


                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.email).Trim(), 240, 690, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Phone :" + Convert.ToString(clsOrder.phone).Trim(), 240, 680, 0);
                        cbCompany.EndText();

                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.Rectangle(425, 692, 140, 40);
                        cbCompany.Fill();


                        //Shipping address ends here
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Quote Date", 485, 718, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.SetFontAndSize(bfcbCompany, 14);
                        //30 November 2016
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToDateTime(clsOrder.orderdate).ToString("MM/dd/yyyy"), 433, 703, 0);
                        cbCompany.EndText();


                        // TO Set the Light Gray Header with Company Name & Invoice No.
                        PdfContentByte cbDetailHeader = writer.DirectContent;
                        BaseFont bfDetailHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDetailHeader.SaveState();
                        cbDetailHeader.SetColorFill(new BaseColor(204, 204, 204));
                        cbDetailHeader.Rectangle(425, 735, 140, 18);
                        cbDetailHeader.Fill();
                        cbDetailHeader.RestoreState();
                        cbDetailHeader.BeginText();
                        cbDetailHeader.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader.SetFontAndSize(bfHeader, 10);
                        cbDetailHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Order No: " + clsOrder.orderno, 560, 740, 0);
                        cbDetailHeader.EndText();

                        if (Convert.ToString(clsOrder.ponumber) != "")
                        {
                            // TO Set the Light Gray Header with PO Number
                            PdfContentByte cbDetailHeader3 = writer.DirectContent;
                            BaseFont bfDetailHeader3 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cbDetailHeader.SaveState();
                            cbDetailHeader3.SetColorFill(new BaseColor(204, 204, 204));
                            cbDetailHeader3.Rectangle(425, 675, 140, 18);
                            cbDetailHeader3.Fill();
                            cbDetailHeader3.RestoreState();
                            cbDetailHeader3.BeginText();
                            cbDetailHeader3.SetColorFill(BaseColor.WHITE);
                            cbDetailHeader3.SetFontAndSize(bfHeader, 10);
                            cbDetailHeader3.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Purchase #: " + clsOrder.ponumber.Trim(), 525, 680, 0);
                            cbDetailHeader3.EndText();

                        }
                        //New Grey Box
                        PdfContentByte cbDetailHeader2 = writer.DirectContent;
                        BaseFont bfDetailHeader2 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDetailHeader.SaveState();
                        cbDetailHeader.SetColorFill(new BaseColor(204, 204, 204));
                        cbDetailHeader.Rectangle(35, 655, 530, 18);
                        cbDetailHeader2.Fill();
                        cbDetailHeader2.RestoreState();
                        cbDetailHeader2.BeginText();
                        cbDetailHeader2.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader2.SetFontAndSize(bfHeader, 10);
                        cbDetailHeader2.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, " Payment method: " + clsOrder.paymentmethod, 420, 660, 0);
                        cbDetailHeader2.EndText();


                        // TO Set the Light Gray Header with Company Name & Invoice No.
                        PdfContentByte cbDescHeader = writer.DirectContent;
                        BaseFont bfDescHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDescHeader.SaveState();
                        cbDescHeader.SetColorFill(new BaseColor(84, 141, 212));
                        cbDescHeader.Rectangle(35, 635, 530, 18);
                        cbDescHeader.Fill();
                        cbDescHeader.RestoreState();
                        cbDescHeader.BeginText();
                        cbDescHeader.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader.SetFontAndSize(bfHeader, 7);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Description", 40, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Product Name", 140, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Quantity", 220, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Unit Price", 270, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Amount ", 320, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Discounted Price", 385, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Discounted Amount ", 460, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Discounted % ", 540, 640, 0);
                        cbDescHeader.EndText();

                        // To Add the Invoice Details
                        doc.Add(new Paragraph(175, "\u00a0"));
                        float TotalDues = 0.0f;
                        PdfPTable tblInvoiceDetails = null;
                        PdfPCell cell1 = null;
                        DataTable dt = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (Int32 i = 0; i < dt.Rows.Count; i++)
                            {
                                string Categoryname = Convert.ToString(dt.Rows[i]["CategoryName"]).Trim() + "\n";
                                string ProductName = Convert.ToString(dt.Rows[i]["ProductName"]).Trim();
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Color"]).Trim()))
                                {
                                    ProductName = ProductName + "- Color: " + Convert.ToString(dt.Rows[i]["Color"]).Trim();
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Size"]).Trim()))
                                {
                                    ProductName = ProductName + "- Size: " + Convert.ToString(dt.Rows[i]["Size"]).Trim();
                                }
                                string Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                                string Qty = Convert.ToString(dt.Rows[i]["Quantity"]).Trim();
                                string UnitPrice = "$" + Convert.ToString(dt.Rows[i]["Price"]).Trim();
                                string Amount = "$" + Convert.ToString(dt.Rows[i]["RunningPrice"]).Trim();
                                string DiscountedPrice = "$" + Convert.ToString(dt.Rows[i]["DiscountedPrice"]).Trim();
                                string DiscountedAmount = "$" + Convert.ToString(dt.Rows[i]["DiscountedAmount"]).Trim();
                                string DiscountedPercentage = Convert.ToString(dt.Rows[i]["DiscountedPercentage"]).Trim() + " % ";
                                tblInvoiceDetails = new PdfPTable(new float[] { 2f, 40f, 25f, 20f, 20f, 20f, 25f, 30f, 25f });
                                tblInvoiceDetails.WidthPercentage = 100;
                                var MyFont = FontFactory.GetFont("Arial", 7, new BaseColor(105, 109, 108));

                                cell1 = new PdfPCell();
                                cell1.BackgroundColor = new BaseColor(255, 255, 255);
                                cell1.UseVariableBorders = true;
                                cell1.BorderColorLeft = BaseColor.WHITE;
                                cell1.BorderColorRight = BaseColor.WHITE;
                                cell1.BorderColorTop = BaseColor.WHITE;
                                cell1.BorderColorBottom = BaseColor.WHITE;
                                string blueDotPath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/icons/blue-dot.jpg");
                                iTextSharp.text.Image image = null;
                                using (FileStream fsimage = new FileStream(blueDotPath, FileMode.Open))
                                {
                                    image = iTextSharp.text.Image.GetInstance(fsimage);
                                }
                                image.ScaleAbsolute(2f, 2f);
                                image.SpacingBefore = 2f;

                                cell1.AddElement(image);
                                var DescHeadingFont = FontFactory.GetFont("Arial", 5, 1, new BaseColor(105, 109, 108));
                                var DescSubTitleFont = FontFactory.GetFont("Arial", 5, new BaseColor(105, 109, 108));

                                var titleChunk = new Chunk(Categoryname, DescHeadingFont);
                                var descriptionChunk = new Chunk(Description, DescSubTitleFont);

                                var phrase = new Phrase(titleChunk);
                                phrase.Add(descriptionChunk);

                                PdfPCell cell2 = new PdfPCell(phrase);
                                cell2.BackgroundColor = new BaseColor(255, 255, 255);
                                cell2.UseVariableBorders = true;
                                cell2.BorderColorLeft = BaseColor.WHITE;
                                cell2.BorderColorRight = BaseColor.WHITE;
                                cell2.BorderColorTop = BaseColor.WHITE;
                                cell2.BorderColorBottom = BaseColor.WHITE;
                                cell2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                PdfPCell cell3 = new PdfPCell(new Phrase(ProductName, MyFont));
                                cell3.BackgroundColor = new BaseColor(255, 255, 255);
                                cell3.UseVariableBorders = true;
                                cell3.BorderColorLeft = BaseColor.WHITE;
                                cell3.BorderColorRight = BaseColor.WHITE;
                                cell3.BorderColorTop = BaseColor.WHITE;
                                cell3.BorderColorBottom = BaseColor.WHITE;
                                cell3.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                PdfPCell cell4 = new PdfPCell(new Phrase(Qty, MyFont));
                                cell4.BackgroundColor = new BaseColor(255, 255, 255);
                                cell4.UseVariableBorders = true;
                                cell4.BorderColorLeft = BaseColor.WHITE;
                                cell4.BorderColorRight = BaseColor.WHITE;
                                cell4.BorderColorTop = BaseColor.WHITE;
                                cell4.BorderColorBottom = BaseColor.WHITE;
                                cell4.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                PdfPCell cell5 = new PdfPCell(new Phrase(UnitPrice, MyFont));
                                cell5.BackgroundColor = new BaseColor(255, 255, 255);
                                cell5.UseVariableBorders = true;
                                cell5.BorderColorLeft = BaseColor.WHITE;
                                cell5.BorderColorRight = BaseColor.WHITE;
                                cell5.BorderColorTop = BaseColor.WHITE;
                                cell5.BorderColorBottom = BaseColor.WHITE;
                                cell5.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                PdfPCell cell6 = new PdfPCell(new Phrase(Amount, MyFont));
                                cell6.BackgroundColor = new BaseColor(255, 255, 255);
                                cell6.UseVariableBorders = true;
                                cell6.BorderColorLeft = BaseColor.WHITE;
                                cell6.BorderColorRight = BaseColor.WHITE;
                                cell6.BorderColorTop = BaseColor.WHITE;
                                cell6.BorderColorBottom = BaseColor.WHITE;
                                cell6.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                PdfPCell cell7 = new PdfPCell(new Phrase(DiscountedPrice, MyFont));
                                cell7.BackgroundColor = new BaseColor(255, 255, 255);
                                cell7.UseVariableBorders = true;
                                cell7.BorderColorLeft = BaseColor.WHITE;
                                cell7.BorderColorRight = BaseColor.WHITE;
                                cell7.BorderColorTop = BaseColor.WHITE;
                                cell7.BorderColorBottom = BaseColor.WHITE;
                                cell7.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                PdfPCell cell8 = new PdfPCell(new Phrase(DiscountedAmount, MyFont));
                                cell8.BackgroundColor = new BaseColor(255, 255, 255);
                                cell8.UseVariableBorders = true;
                                cell8.BorderColorLeft = BaseColor.WHITE;
                                cell8.BorderColorRight = BaseColor.WHITE;
                                cell8.BorderColorTop = BaseColor.WHITE;
                                cell8.BorderColorBottom = BaseColor.WHITE;
                                cell8.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                                PdfPCell cell9 = new PdfPCell(new Phrase(DiscountedPercentage, MyFont));
                                cell9.BackgroundColor = new BaseColor(255, 255, 255);
                                cell9.UseVariableBorders = true;
                                cell9.BorderColorLeft = BaseColor.WHITE;
                                cell9.BorderColorRight = BaseColor.WHITE;
                                cell9.BorderColorTop = BaseColor.WHITE;
                                cell9.BorderColorBottom = BaseColor.WHITE;
                                cell9.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                                tblInvoiceDetails.AddCell(cell1);
                                tblInvoiceDetails.AddCell(cell2);
                                tblInvoiceDetails.AddCell(cell3);
                                tblInvoiceDetails.AddCell(cell4);
                                tblInvoiceDetails.AddCell(cell5);
                                tblInvoiceDetails.AddCell(cell6);
                                tblInvoiceDetails.AddCell(cell7);
                                tblInvoiceDetails.AddCell(cell8);
                                tblInvoiceDetails.AddCell(cell9);
                                doc.Add(tblInvoiceDetails);
                                doc.Add(new Paragraph(5, "\u00a0"));

                                // Add Horizontal Line under table Row
                                PdfPTable tblHorizontal = new PdfPTable(1);
                                tblHorizontal.HorizontalAlignment = Element.ALIGN_LEFT;
                                tblHorizontal.WidthPercentage = 100;
                                tblHorizontal.DefaultCell.FixedHeight = 1f;

                                var HorizontalFont = FontFactory.GetFont("Arial", 1, new BaseColor(221, 221, 221));
                                PdfPCell cellHorizontal = new PdfPCell(new Phrase("", HorizontalFont));
                                cellHorizontal.BackgroundColor = new BaseColor(221, 221, 221);
                                cellHorizontal.UseVariableBorders = true;
                                cellHorizontal.BorderColorLeft = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorTop = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorRight = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorBottom = new BaseColor(221, 221, 221);
                                tblHorizontal.AddCell(cellHorizontal);
                                doc.Add(tblHorizontal);
                                doc.Add(new Paragraph(10, "\u00a0"));
                            }
                        }
                        //Add Total
                        doc.Add(new Paragraph(5, "\u00a0"));
                        string DiscuontedTotal = ServicesFactory.DocCMSServices.Fetch_Discounted_Total(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        string GrandTotal = ServicesFactory.DocCMSServices.Fetch_Grand_Total_from_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        var DiscountedTotal = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));
                        var urlFont2 = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));
                        var Total = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));
                        var urlFont1 = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));

                        PdfPTable tblTotal = new PdfPTable(new float[] { 70f, 15f, 30f, 15f });
                        tblTotal.WidthPercentage = 100;

                        PdfPCell cell1Total = new PdfPCell(new Phrase("Total:", Total));
                        PdfPCell cell2Total = new PdfPCell(new Phrase("$" + Convert.ToString(GrandTotal), urlFont1));
                        cell1Total.PaddingBottom = 5f;
                        cell1Total.UseVariableBorders = true;
                        cell2Total.UseVariableBorders = true;

                        cell1Total.BorderColorBottom = BaseColor.WHITE;
                        cell1Total.BorderColorTop = BaseColor.WHITE;
                        cell1Total.BorderColorRight = BaseColor.WHITE;
                        cell1Total.BorderColorLeft = BaseColor.WHITE;

                        cell2Total.BorderColorBottom = BaseColor.WHITE;
                        cell2Total.BorderColorTop = BaseColor.WHITE;
                        cell2Total.BorderColorRight = BaseColor.WHITE;
                        cell2Total.BorderColorLeft = BaseColor.WHITE;

                        PdfPCell cell1DiscountedTotal = new PdfPCell(new Phrase("Grand Total:", Total));
                        PdfPCell cell2DiscountedTotal = new PdfPCell(new Phrase("$" + Convert.ToString(DiscuontedTotal), urlFont2));
                        cell1DiscountedTotal.PaddingBottom = 5f;
                        cell1DiscountedTotal.UseVariableBorders = true;
                        cell2DiscountedTotal.UseVariableBorders = true;

                        cell1DiscountedTotal.BorderColorBottom = BaseColor.WHITE;
                        cell1DiscountedTotal.BorderColorTop = BaseColor.WHITE;
                        cell1DiscountedTotal.BorderColorRight = BaseColor.WHITE;
                        cell1DiscountedTotal.BorderColorLeft = BaseColor.WHITE;

                        cell2DiscountedTotal.BorderColorBottom = BaseColor.WHITE;
                        cell2DiscountedTotal.BorderColorTop = BaseColor.WHITE;
                        cell2DiscountedTotal.BorderColorRight = BaseColor.WHITE;
                        cell2DiscountedTotal.BorderColorLeft = BaseColor.WHITE;

                        cell1DiscountedTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2DiscountedTotal.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        cell1Total.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2Total.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tblTotal.AddCell(cell1Total);
                        tblTotal.AddCell(cell2Total);
                        tblTotal.AddCell(cell1DiscountedTotal);
                        tblTotal.AddCell(cell2DiscountedTotal);
                        doc.Add(tblTotal);


                        //Add Other Detal
                        doc.Add(new Paragraph(20, "\u00a0"));
                        doc.Add(new Paragraph(5, "\u00a0"));
                        PdfPTable tblComment2 = new PdfPTable(new float[] { 100f });
                        tblComment2.WidthPercentage = 100;
                        var fontComment2 = FontFactory.GetFont("Arial", 10, new BaseColor(138, 140, 158));
                        PdfPCell cellComment2 = new PdfPCell(new Phrase("Other Detail: " + clsOrder.otherdetail, fontComment2));
                        cellComment2.BackgroundColor = new BaseColor(255, 255, 255);
                        cellComment2.UseVariableBorders = true;
                        cellComment2.PaddingTop = 5f;
                        cellComment2.Colspan = 2;
                        cellComment2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellComment2.BorderColorLeft = BaseColor.WHITE;
                        cellComment2.BorderColorRight = BaseColor.WHITE;
                        cellComment2.BorderColorTop = BaseColor.WHITE;
                        cellComment2.BorderColorBottom = BaseColor.WHITE;
                        tblComment2.AddCell(cellComment2);
                        doc.Add(tblComment2);


                        //Add Comments
                        doc.Add(new Paragraph(30, "\u00a0"));
                        DataTable dtComment = ServicesFactory.DocCMSServices.Fetch_Quote_comment_Bypurchaseorderid(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        if (dtComment != null && dtComment.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtComment.Rows.Count; i++)
                            {

                                doc.Add(new Paragraph(5, "\u00a0"));
                                PdfPTable tblComment = new PdfPTable(new float[] { 100f });
                                tblComment.WidthPercentage = 100;
                                var fontComment = FontFactory.GetFont("Arial", 10, new BaseColor(138, 140, 158));
                                PdfPCell cellComment = new PdfPCell(new Phrase(Convert.ToString(dtComment.Rows[i]["QuoteComment"]), fontComment));
                                cellComment.BackgroundColor = new BaseColor(255, 255, 255);
                                cellComment.UseVariableBorders = true;
                                cellComment.PaddingTop = 5f;
                                cellComment.Colspan = 2;
                                cellComment.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                cellComment.BorderColorLeft = BaseColor.WHITE;
                                cellComment.BorderColorRight = BaseColor.WHITE;
                                cellComment.BorderColorTop = BaseColor.WHITE;
                                cellComment.BorderColorBottom = BaseColor.WHITE;
                                tblComment.AddCell(cellComment);
                                doc.Add(tblComment);
                            }
                        }

                        // Adding Footer Section
                        doc.Add(new Paragraph(50, "\u00a0"));
                        var ThankyouFont = FontFactory.GetFont("Arial", 16, 1, new BaseColor(83, 142, 212));
                        var urlFont = FontFactory.GetFont("Arial", 8, 1, new BaseColor(83, 142, 212));
                        PdfPTable tblThankYou = new PdfPTable(new float[] { 50f, 50f });
                        tblThankYou.WidthPercentage = 100;


                        PdfPCell cell1ThankYou = new PdfPCell(new Phrase("THANK YOU", ThankyouFont));
                        PdfPCell cell2ThankYou = new PdfPCell(new Phrase("www.docfocus.ca", urlFont));
                        cell1ThankYou.PaddingBottom = 5f;
                        cell1ThankYou.UseVariableBorders = true;
                        cell2ThankYou.UseVariableBorders = true;

                        cell1ThankYou.BorderColorBottom = BaseColor.WHITE;
                        cell1ThankYou.BorderColorTop = BaseColor.WHITE;
                        cell1ThankYou.BorderColorRight = BaseColor.WHITE;
                        cell1ThankYou.BorderColorLeft = BaseColor.WHITE;

                        cell2ThankYou.BorderColorBottom = BaseColor.WHITE;
                        cell2ThankYou.BorderColorTop = BaseColor.WHITE;
                        cell2ThankYou.BorderColorRight = BaseColor.WHITE;
                        cell2ThankYou.BorderColorLeft = BaseColor.WHITE;

                        cell1ThankYou.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cell2ThankYou.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2ThankYou.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        tblThankYou.AddCell(cell1ThankYou);
                        tblThankYou.AddCell(cell2ThankYou);

                        var urlHr = FontFactory.GetFont("Arial", 16, new BaseColor(83, 142, 212));
                        PdfPCell cellThankHR = new PdfPCell(new Phrase("", urlHr));
                        cellThankHR.BackgroundColor = new BaseColor(83, 142, 212);
                        cellThankHR.UseVariableBorders = true;

                        var fontInvoiceNoBottom = FontFactory.GetFont("Arial", 9, new BaseColor(138, 140, 158));
                        PdfPCell cellInvoiceNoBottom = new PdfPCell(new Phrase("Quote No: " + clsOrder.orderno, fontInvoiceNoBottom));
                        cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                        cellInvoiceNoBottom.UseVariableBorders = true;
                        cellInvoiceNoBottom.PaddingTop = 2f;
                        cellInvoiceNoBottom.Colspan = 2;
                        cellInvoiceNoBottom.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                        cellInvoiceNoBottom.BorderColorLeft = BaseColor.WHITE;
                        cellInvoiceNoBottom.BorderColorRight = BaseColor.WHITE;
                        cellInvoiceNoBottom.BorderColorBottom = BaseColor.WHITE;
                        tblThankYou.AddCell(cellInvoiceNoBottom);

                        var fontBottomMsg = FontFactory.GetFont("Arial", 6, new BaseColor(138, 140, 158));
                        PdfPCell cellBottomMsg = new PdfPCell(new Phrase("All payments can be made to DOCFOCUS INC.If you have any questions, need another copy of the work, estimate, or invoice? Please contact me to address any concerns!", fontBottomMsg));
                        cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                        cellBottomMsg.UseVariableBorders = true;
                        cellBottomMsg.PaddingTop = 5f;
                        cellBottomMsg.Colspan = 2;
                        cellBottomMsg.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellBottomMsg.BorderColorLeft = BaseColor.WHITE;
                        cellBottomMsg.BorderColorRight = BaseColor.WHITE;
                        cellBottomMsg.BorderColorTop = BaseColor.WHITE;
                        cellBottomMsg.BorderColorBottom = BaseColor.WHITE;
                        tblThankYou.AddCell(cellBottomMsg);
                        doc.Add(tblThankYou);
                    }
                    // Step 6: Closing the Document
                    doc.Close();
                }
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        string PDFName = "";
        public float Update_Pdf(bool IsDownload)
        {
            string OrderPDFDirectoryPath = "";
            float TotalLoggedHours = 0.0f;
            try
            {

                PDFName = "Quote";
                string InvoiceImagepath = Server.MapPath("~/images/Logos/DocLogo2-Copy.png");
                OrderPDFDirectoryPath = "~/PDFs/";
                if (!Directory.Exists(Server.MapPath(OrderPDFDirectoryPath)))
                {
                    Directory.CreateDirectory(Server.MapPath(OrderPDFDirectoryPath));
                }
                string appRootDir = Server.MapPath(OrderPDFDirectoryPath);
                // Step 1: Creating System.IO.FileStream object
                using (FileStream fs = new FileStream(appRootDir + "/" + PDFName.ToString() + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                // Step 2: Creating iTextSharp.text.Document object
                using (Document doc = new Document(PageSize.A4))
                // Step 3: Creating iTextSharp.text.pdf.PdfWriter object
                // It helps to write the Document to the Specified FileStream
                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {// Step 4: Openning the Document
                    writer.CloseStream = false;
                    doc.Open();

                    //// select the font properties
                    BaseFont bfHeader = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    // TO SET INVOICE HEADING
                    PdfContentByte cbHeading = writer.DirectContent;
                    // select the font properties
                    BaseFont bfcbHeading = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbHeading.BeginText();
                    cbHeading.SetColorFill(new BaseColor(84, 141, 212));
                    cbHeading.SetFontAndSize(bfHeader, 28);
                    Cls_Order clsOrder = ServicesFactory.DocCMSServices.Fetch_Order_List_By_id(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                    if (clsOrder != null)
                    {
                        cbHeading.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "QUOTE REQUEST", 35, 770, 0);
                        cbHeading.EndText();
                        cbHeading.SetColorFill(new BaseColor(84, 141, 212));
                        cbHeading.Rectangle(35, 760, 530, 2);
                        cbHeading.Fill();

                        //Adding Image to Right Header
                        iTextSharp.text.Image imagecmp = null;
                        using (FileStream fsimage = new FileStream(InvoiceImagepath, FileMode.Open))
                        {
                            imagecmp = iTextSharp.text.Image.GetInstance(fsimage);
                        }
                        imagecmp.SetAbsolutePosition(409, 765);
                        doc.Add(imagecmp);
                        // Adding Image to Right Header Ends
                        PdfContentByte cbCompany = writer.DirectContent;
                        // select the font properties
                        BaseFont bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 8);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Quote Request:"), 40, 745, 0);
                        cbCompany.EndText();

                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.SetFontAndSize(bfcbCompany, 12);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.purchasername), 40, 730, 0);
                        cbCompany.EndText();

                        //Billing address
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        doc.Add(new Paragraph(5, "\u00a0"));
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.Rectangle(40, 677, 170, 40);
                        cbCompany.Fill();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Billing Address"), 40, 720, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.billingaddress1).Trim() + ", " + Convert.ToString(clsOrder.billingaddress2).Trim(), 40, 710, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.billingcity).Trim() + ", " + Convert.ToString(clsOrder.billingpostalcode).Trim(), 40, 700, 0);
                        cbCompany.EndText();


                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.email).Trim(), 40, 690, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Phone :" + Convert.ToString(clsOrder.phone).Trim(), 40, 680, 0);
                        cbCompany.EndText();

                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.Rectangle(425, 695, 140, 40);
                        cbCompany.Fill();
                        // Billing Address Ends

                        //Shipping address
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        doc.Add(new Paragraph(5, "\u00a0"));
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.Rectangle(190, 677, 200, 40);
                        cbCompany.Fill();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Shipping Address"), 240, 720, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.shippingaddress1).Trim() + ", " + Convert.ToString(clsOrder.shippingaddress2).Trim(), 240, 710, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.shippingcity).Trim() + ", " + Convert.ToString(clsOrder.shippingpostalcode).Trim(), 240, 700, 0);
                        cbCompany.EndText();


                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.email).Trim(), 240, 690, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Phone :" + Convert.ToString(clsOrder.phone).Trim(), 240, 680, 0);
                        cbCompany.EndText();

                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.Rectangle(425, 692, 140, 40);
                        cbCompany.Fill();

                        //Shipping address ends here
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Quote Date", 485, 718, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.SetFontAndSize(bfcbCompany, 14);
                        //30 November 2016
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToDateTime(clsOrder.orderdate).ToString("MM/dd/yyyy"), 433, 703, 0);
                        cbCompany.EndText();


                        // TO Set the Light Gray Header with Company Name & Invoice No.
                        PdfContentByte cbDetailHeader = writer.DirectContent;
                        BaseFont bfDetailHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDetailHeader.SaveState();
                        cbDetailHeader.SetColorFill(new BaseColor(204, 204, 204));
                        cbDetailHeader.Rectangle(425, 735, 140, 18);
                        cbDetailHeader.Fill();
                        cbDetailHeader.RestoreState();
                        cbDetailHeader.BeginText();
                        cbDetailHeader.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader.SetFontAndSize(bfHeader, 10);
                        cbDetailHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Order No: " + clsOrder.orderno, 560, 740, 0);
                        cbDetailHeader.EndText();
                        if (Convert.ToString(clsOrder.ponumber) != "")
                        {
                            // TO Set the Light Gray Header with PO Number
                            PdfContentByte cbDetailHeader3 = writer.DirectContent;
                            BaseFont bfDetailHeader3 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cbDetailHeader.SaveState();
                            cbDetailHeader3.SetColorFill(new BaseColor(204, 204, 204));
                            cbDetailHeader3.Rectangle(425, 675, 140, 18);
                            cbDetailHeader3.Fill();
                            cbDetailHeader3.RestoreState();
                            cbDetailHeader3.BeginText();
                            cbDetailHeader3.SetColorFill(BaseColor.WHITE);
                            cbDetailHeader3.SetFontAndSize(bfHeader, 10);
                            cbDetailHeader3.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Purchase #: " + clsOrder.ponumber.Trim(), 525, 680, 0);
                            cbDetailHeader3.EndText();

                        }
                        //New Grey Box

                        PdfContentByte cbDetailHeader2 = writer.DirectContent;
                        BaseFont bfDetailHeader2 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDetailHeader.SaveState();
                        cbDetailHeader.SetColorFill(new BaseColor(204, 204, 204));
                        cbDetailHeader.Rectangle(35, 655, 530, 18);
                        cbDetailHeader2.Fill();
                        cbDetailHeader2.RestoreState();
                        cbDetailHeader2.BeginText();
                        cbDetailHeader2.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader2.SetFontAndSize(bfHeader, 10);
                        cbDetailHeader2.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, " Payment method: " + clsOrder.paymentmethod, 420, 660, 0);
                        cbDetailHeader2.EndText();


                        // TO Set the Light Gray Header with Company Name & Invoice No.
                        PdfContentByte cbDescHeader = writer.DirectContent;
                        BaseFont bfDescHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDescHeader.SaveState();
                        cbDescHeader.SetColorFill(new BaseColor(84, 141, 212));
                        cbDescHeader.Rectangle(35, 635, 530, 18);
                        cbDescHeader.Fill();
                        cbDescHeader.RestoreState();
                        cbDescHeader.BeginText();
                        cbDescHeader.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader.SetFontAndSize(bfHeader, 7);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Description", 40, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Product Name", 140, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Quantity", 220, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Unit Price", 270, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Amount ", 320, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Discounted Price", 385, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Discounted Amount ", 460, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Discounted % ", 540, 640, 0);
                        cbDescHeader.EndText();

                        // To Add the Invoice Details
                        doc.Add(new Paragraph(175, "\u00a0"));
                        float TotalDues = 0.0f;
                        PdfPTable tblInvoiceDetails = null;
                        PdfPCell cell1 = null;

                        DataTable dt = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid_From_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (Int32 i = 0; i < dt.Rows.Count; i++)
                            {
                                string Categoryname = Convert.ToString(dt.Rows[i]["CategoryName"]).Trim() + "\n";
                                string ProductName = Convert.ToString(dt.Rows[i]["ProductName"]).Trim();
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Color"]).Trim()))
                                {
                                    ProductName = ProductName + "- Color: " + Convert.ToString(dt.Rows[i]["Color"]).Trim();
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Size"]).Trim()))
                                {
                                    ProductName = ProductName + "- Size: " + Convert.ToString(dt.Rows[i]["Size"]).Trim();
                                }
                                string Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                                string Qty = Convert.ToString(dt.Rows[i]["Quantity"]).Trim();
                                string UnitPrice = "$" + Convert.ToString(dt.Rows[i]["Price"]).Trim();
                                string Amount = "$" + Convert.ToString(dt.Rows[i]["RunningPrice"]).Trim();
                                string DiscountedPrice = "$" + Convert.ToString(dt.Rows[i]["DiscountedPrice"]).Trim();
                                string DiscountedAmount = "$" + Convert.ToString(dt.Rows[i]["DiscountedAmount"]).Trim();
                                string DiscountedPercentage =  Convert.ToString(dt.Rows[i]["DiscountedPercentage"]).Trim()+" % ";
                                tblInvoiceDetails = new PdfPTable(new float[] { 2f, 40f,25f, 20f, 20f, 20f,25f,30f,25f });
                                tblInvoiceDetails.WidthPercentage = 100;
                                var MyFont = FontFactory.GetFont("Arial", 7, new BaseColor(105, 109, 108));

                                cell1 = new PdfPCell();
                                cell1.BackgroundColor = new BaseColor(255, 255, 255);
                                cell1.UseVariableBorders = true;
                                cell1.BorderColorLeft = BaseColor.WHITE;
                                cell1.BorderColorRight = BaseColor.WHITE;
                                cell1.BorderColorTop = BaseColor.WHITE;
                                cell1.BorderColorBottom = BaseColor.WHITE;
                                string blueDotPath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/icons/blue-dot.jpg");
                                iTextSharp.text.Image image = null;
                                using (FileStream fsimage = new FileStream(blueDotPath, FileMode.Open))
                                {
                                    image = iTextSharp.text.Image.GetInstance(fsimage);
                                }
                                image.ScaleAbsolute(2f, 2f);
                                image.SpacingBefore = 2f;

                                cell1.AddElement(image);
                                var DescHeadingFont = FontFactory.GetFont("Arial", 5, 1, new BaseColor(105, 109, 108));
                                var DescSubTitleFont = FontFactory.GetFont("Arial", 5, new BaseColor(105, 109, 108));

                                var titleChunk = new Chunk(Categoryname, DescHeadingFont);
                                var descriptionChunk = new Chunk(Description, DescSubTitleFont);

                                var phrase = new Phrase(titleChunk);
                                phrase.Add(descriptionChunk);

                                PdfPCell cell2 = new PdfPCell(phrase);
                                cell2.BackgroundColor = new BaseColor(255, 255, 255);
                                cell2.UseVariableBorders = true;
                                cell2.BorderColorLeft = BaseColor.WHITE;
                                cell2.BorderColorRight = BaseColor.WHITE;
                                cell2.BorderColorTop = BaseColor.WHITE;
                                cell2.BorderColorBottom = BaseColor.WHITE;
                                cell2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                              
                                
                                PdfPCell cell3 = new PdfPCell(new Phrase(ProductName, MyFont));
                                cell3.BackgroundColor = new BaseColor(255, 255, 255);
                                cell3.UseVariableBorders = true;
                                cell3.BorderColorLeft = BaseColor.WHITE;
                                cell3.BorderColorRight = BaseColor.WHITE;
                                cell3.BorderColorTop = BaseColor.WHITE;
                                cell3.BorderColorBottom = BaseColor.WHITE;
                                cell3.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                PdfPCell cell4 = new PdfPCell(new Phrase(Qty, MyFont));
                                cell4.BackgroundColor = new BaseColor(255, 255, 255);
                                cell4.UseVariableBorders = true;
                                cell4.BorderColorLeft = BaseColor.WHITE;
                                cell4.BorderColorRight = BaseColor.WHITE;
                                cell4.BorderColorTop = BaseColor.WHITE;
                                cell4.BorderColorBottom = BaseColor.WHITE;
                                cell4.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                

                                PdfPCell cell5 = new PdfPCell(new Phrase(UnitPrice, MyFont));
                                cell5.BackgroundColor = new BaseColor(255, 255, 255);
                                cell5.UseVariableBorders = true;
                                cell5.BorderColorLeft = BaseColor.WHITE;
                                cell5.BorderColorRight = BaseColor.WHITE;
                                cell5.BorderColorTop = BaseColor.WHITE;
                                cell5.BorderColorBottom = BaseColor.WHITE;
                                cell5.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                PdfPCell cell6 = new PdfPCell(new Phrase(Amount, MyFont));
                                cell6.BackgroundColor = new BaseColor(255, 255, 255);
                                cell6.UseVariableBorders = true;
                                cell6.BorderColorLeft = BaseColor.WHITE;
                                cell6.BorderColorRight = BaseColor.WHITE;
                                cell6.BorderColorTop = BaseColor.WHITE;
                                cell6.BorderColorBottom = BaseColor.WHITE;
                                cell6.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                PdfPCell cell7 = new PdfPCell(new Phrase(DiscountedPrice, MyFont));
                                cell7.BackgroundColor = new BaseColor(255, 255, 255);
                                cell7.UseVariableBorders = true;
                                cell7.BorderColorLeft = BaseColor.WHITE;
                                cell7.BorderColorRight = BaseColor.WHITE;
                                cell7.BorderColorTop = BaseColor.WHITE;
                                cell7.BorderColorBottom = BaseColor.WHITE;
                                cell7.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                                PdfPCell cell8 = new PdfPCell(new Phrase(DiscountedAmount, MyFont));
                                cell8.BackgroundColor = new BaseColor(255, 255, 255);
                                cell8.UseVariableBorders = true;
                                cell8.BorderColorLeft = BaseColor.WHITE;
                                cell8.BorderColorRight = BaseColor.WHITE;
                                cell8.BorderColorTop = BaseColor.WHITE;
                                cell8.BorderColorBottom = BaseColor.WHITE;
                                cell8.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                                PdfPCell cell9 = new PdfPCell(new Phrase(DiscountedPercentage, MyFont));
                                cell9.BackgroundColor = new BaseColor(255, 255, 255);
                                cell9.UseVariableBorders = true;
                                cell9.BorderColorLeft = BaseColor.WHITE;
                                cell9.BorderColorRight = BaseColor.WHITE;
                                cell9.BorderColorTop = BaseColor.WHITE;
                                cell9.BorderColorBottom = BaseColor.WHITE;
                                cell9.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                                tblInvoiceDetails.AddCell(cell1);
                                tblInvoiceDetails.AddCell(cell2);
                                tblInvoiceDetails.AddCell(cell3);
                                tblInvoiceDetails.AddCell(cell4);
                                tblInvoiceDetails.AddCell(cell5);
                                tblInvoiceDetails.AddCell(cell6);
                                tblInvoiceDetails.AddCell(cell7);
                                tblInvoiceDetails.AddCell(cell8);
                                tblInvoiceDetails.AddCell(cell9);
                                doc.Add(tblInvoiceDetails);
                                doc.Add(new Paragraph(5, "\u00a0"));

                                // Add Horizontal Line under table Row

                                PdfPTable tblHorizontal = new PdfPTable(1);
                                tblHorizontal.HorizontalAlignment = Element.ALIGN_LEFT;
                                tblHorizontal.WidthPercentage = 100;
                                tblHorizontal.DefaultCell.FixedHeight = 1f;

                                var HorizontalFont = FontFactory.GetFont("Arial", 1, new BaseColor(221, 221, 221));
                                PdfPCell cellHorizontal = new PdfPCell(new Phrase("", HorizontalFont));
                                cellHorizontal.BackgroundColor = new BaseColor(221, 221, 221);
                                cellHorizontal.UseVariableBorders = true;
                                cellHorizontal.BorderColorLeft = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorTop = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorRight = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorBottom = new BaseColor(221, 221, 221);
                                tblHorizontal.AddCell(cellHorizontal);
                                doc.Add(tblHorizontal);
                                doc.Add(new Paragraph(10, "\u00a0"));

                            }
                        }
                        //Add Total
                        doc.Add(new Paragraph(5, "\u00a0"));
                        string DiscuontedTotal = ServicesFactory.DocCMSServices.Fetch_Discounted_Total(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        string GrandTotal = ServicesFactory.DocCMSServices.Fetch_Grand_Total_from_QuoteDetail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));

                        var DiscountedTotal = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));
                        var urlFont2 = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));
                        var Total = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));
                        var urlFont1 = FontFactory.GetFont("Arial", 10, 1, new BaseColor(83, 142, 212));

                        PdfPTable tblTotal = new PdfPTable(new float[] { 70f, 15f,30f,15f });
                        tblTotal.WidthPercentage = 100;

                        PdfPCell cell1Total = new PdfPCell(new Phrase("Total:", Total));
                        PdfPCell cell2Total = new PdfPCell(new Phrase("$" + Convert.ToString(GrandTotal), urlFont1));
                        cell1Total.PaddingBottom = 5f;
                        cell1Total.UseVariableBorders = true;
                        cell2Total.UseVariableBorders = true;

                        cell1Total.BorderColorBottom = BaseColor.WHITE;
                        cell1Total.BorderColorTop = BaseColor.WHITE;
                        cell1Total.BorderColorRight = BaseColor.WHITE;
                        cell1Total.BorderColorLeft = BaseColor.WHITE;

                        cell2Total.BorderColorBottom = BaseColor.WHITE;
                        cell2Total.BorderColorTop = BaseColor.WHITE;
                        cell2Total.BorderColorRight = BaseColor.WHITE;
                        cell2Total.BorderColorLeft = BaseColor.WHITE;

                        PdfPCell cell1DiscountedTotal = new PdfPCell(new Phrase("Grand Total:", Total));
                        PdfPCell cell2DiscountedTotal = new PdfPCell(new Phrase("$" + Convert.ToString(DiscuontedTotal), urlFont2));
                        cell1DiscountedTotal.PaddingBottom = 5f;
                        cell1DiscountedTotal.UseVariableBorders = true;
                        cell2DiscountedTotal.UseVariableBorders = true;

                        cell1DiscountedTotal.BorderColorBottom = BaseColor.WHITE;
                        cell1DiscountedTotal.BorderColorTop = BaseColor.WHITE;
                        cell1DiscountedTotal.BorderColorRight = BaseColor.WHITE;
                        cell1DiscountedTotal.BorderColorLeft = BaseColor.WHITE;

                        cell2DiscountedTotal.BorderColorBottom = BaseColor.WHITE;
                        cell2DiscountedTotal.BorderColorTop = BaseColor.WHITE;
                        cell2DiscountedTotal.BorderColorRight = BaseColor.WHITE;
                        cell2DiscountedTotal.BorderColorLeft = BaseColor.WHITE;

                        cell1DiscountedTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2DiscountedTotal.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        cell1Total.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2Total.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tblTotal.AddCell(cell1Total);
                        tblTotal.AddCell(cell2Total);
                        tblTotal.AddCell(cell1DiscountedTotal);
                        tblTotal.AddCell(cell2DiscountedTotal);
                        doc.Add(tblTotal);


                        //Add Other Detal
                        doc.Add(new Paragraph(20, "\u00a0"));
                        doc.Add(new Paragraph(5, "\u00a0"));
                        PdfPTable tblComment2 = new PdfPTable(new float[] { 100f });
                        tblComment2.WidthPercentage = 100;
                        var fontComment2 = FontFactory.GetFont("Arial", 10, new BaseColor(138, 140, 158));
                        PdfPCell cellComment2 = new PdfPCell(new Phrase("Other Detail: " + clsOrder.otherdetail, fontComment2));
                        cellComment2.BackgroundColor = new BaseColor(255, 255, 255);
                        cellComment2.UseVariableBorders = true;
                        cellComment2.PaddingTop = 5f;
                        cellComment2.Colspan = 2;
                        cellComment2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellComment2.BorderColorLeft = BaseColor.WHITE;
                        cellComment2.BorderColorRight = BaseColor.WHITE;
                        cellComment2.BorderColorTop = BaseColor.WHITE;
                        cellComment2.BorderColorBottom = BaseColor.WHITE;
                        tblComment2.AddCell(cellComment2);
                        doc.Add(tblComment2);

                        //Add Comments
                        doc.Add(new Paragraph(30, "\u00a0"));
                        DataTable dtComment = ServicesFactory.DocCMSServices.Fetch_Quote_comment_Bypurchaseorderid(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
                        if (dtComment != null && dtComment.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtComment.Rows.Count; i++)
                        { 
                        doc.Add(new Paragraph(5, "\u00a0"));
                        PdfPTable tblComment = new PdfPTable(new float[] { 100f });
                        tblComment.WidthPercentage = 100;
                        var fontComment = FontFactory.GetFont("Arial", 10, new BaseColor(138, 140, 158));
                        PdfPCell cellComment = new PdfPCell(new Phrase(Convert.ToString(dtComment.Rows[i]["QuoteComment"]), fontComment));
                        cellComment.BackgroundColor = new BaseColor(255, 255, 255);
                        cellComment.UseVariableBorders = true;
                        cellComment.PaddingTop = 5f;
                        cellComment.Colspan = 2;
                        cellComment.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellComment.BorderColorLeft = BaseColor.WHITE;
                        cellComment.BorderColorRight = BaseColor.WHITE;
                        cellComment.BorderColorTop = BaseColor.WHITE;
                        cellComment.BorderColorBottom = BaseColor.WHITE;
                        tblComment.AddCell(cellComment);
                        doc.Add(tblComment);
                          }
                        }

                        // Adding Footer Section
                        doc.Add(new Paragraph(50, "\u00a0"));

                        var ThankyouFont = FontFactory.GetFont("Arial", 16, 1, new BaseColor(83, 142, 212));
                        var urlFont = FontFactory.GetFont("Arial", 8, 1, new BaseColor(83, 142, 212));
                        PdfPTable tblThankYou = new PdfPTable(new float[] { 50f, 50f });
                        tblThankYou.WidthPercentage = 100;


                        PdfPCell cell1ThankYou = new PdfPCell(new Phrase("THANK YOU", ThankyouFont));
                        PdfPCell cell2ThankYou = new PdfPCell(new Phrase("www.docfocus.ca", urlFont));
                        cell1ThankYou.PaddingBottom = 5f;
                        cell1ThankYou.UseVariableBorders = true;
                        cell2ThankYou.UseVariableBorders = true;

                        cell1ThankYou.BorderColorBottom = BaseColor.WHITE;
                        cell1ThankYou.BorderColorTop = BaseColor.WHITE;
                        cell1ThankYou.BorderColorRight = BaseColor.WHITE;
                        cell1ThankYou.BorderColorLeft = BaseColor.WHITE;

                        cell2ThankYou.BorderColorBottom = BaseColor.WHITE;
                        cell2ThankYou.BorderColorTop = BaseColor.WHITE;
                        cell2ThankYou.BorderColorRight = BaseColor.WHITE;
                        cell2ThankYou.BorderColorLeft = BaseColor.WHITE;

                        cell1ThankYou.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cell2ThankYou.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2ThankYou.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        tblThankYou.AddCell(cell1ThankYou);
                        tblThankYou.AddCell(cell2ThankYou);

                        var urlHr = FontFactory.GetFont("Arial", 16, new BaseColor(83, 142, 212));
                        PdfPCell cellThankHR = new PdfPCell(new Phrase("", urlHr));
                        cellThankHR.BackgroundColor = new BaseColor(83, 142, 212);
                        cellThankHR.UseVariableBorders = true;

                        var fontInvoiceNoBottom = FontFactory.GetFont("Arial", 9, new BaseColor(138, 140, 158));
                        PdfPCell cellInvoiceNoBottom = new PdfPCell(new Phrase("Quote No: " + clsOrder.orderno, fontInvoiceNoBottom));
                        cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                        cellInvoiceNoBottom.UseVariableBorders = true;
                        cellInvoiceNoBottom.PaddingTop = 2f;
                        cellInvoiceNoBottom.Colspan = 2;
                        cellInvoiceNoBottom.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                        cellInvoiceNoBottom.BorderColorLeft = BaseColor.WHITE;
                        cellInvoiceNoBottom.BorderColorRight = BaseColor.WHITE;
                        cellInvoiceNoBottom.BorderColorBottom = BaseColor.WHITE;
                        tblThankYou.AddCell(cellInvoiceNoBottom);

                        var fontBottomMsg = FontFactory.GetFont("Arial", 6, new BaseColor(138, 140, 158));
                        PdfPCell cellBottomMsg = new PdfPCell(new Phrase("All payments can be made to DOCFOCUS INC.If you have any questions, need another copy of the work, estimate, or invoice? Please contact me to address any concerns!", fontBottomMsg));
                        cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                        cellBottomMsg.UseVariableBorders = true;
                        cellBottomMsg.PaddingTop = 5f;
                        cellBottomMsg.Colspan = 2;
                        cellBottomMsg.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellBottomMsg.BorderColorLeft = BaseColor.WHITE;
                        cellBottomMsg.BorderColorRight = BaseColor.WHITE;
                        cellBottomMsg.BorderColorTop = BaseColor.WHITE;
                        cellBottomMsg.BorderColorBottom = BaseColor.WHITE;
                        tblThankYou.AddCell(cellBottomMsg);
                        doc.Add(tblThankYou);
                    }
                    // Step 6: Closing the Document
                    doc.Close();
                }
                if (IsDownload)
                {
                    ProcessRequest(appRootDir + "/" + PDFName.ToString() + ".pdf");
                }
                return TotalLoggedHours;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessRequest(string fileRelativePath)
        {
            try
            {
                string contentType = "";
                string FilePath = fileRelativePath;
                contentType = "application/pdf";
                //Set the appropriate ContentType.
                HttpContext.Current.Response.ContentType = contentType;
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(fileRelativePath)).Name);
                //Write the file directly to the HTTP content output stream.
                HttpContext.Current.Response.WriteFile(FilePath);
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                //To Do
                throw ex;
            }
        }

        protected string GetImageUrl(string imagepath)
        {

            string[] splits = Request.Url.AbsoluteUri.Split('/');
            if (splits.Length >= 2)
            {

                string url = splits[0] + "//";
                url += splits[1];
                url += "/";
                for (int i = 2; i < splits.Length - 1; i++)
                {
                    url += splits[i];
                    url += "/";
                }
                return url + imagepath;
            }
            return imagepath;
        }

        //Bind Customer Detail
        public void Get_Complete_Order_Detail(Int32 PurchaseOrderID)
        {
            DataTable dtExport = new DataTable();
            dtExport.Columns.Add("Quote", typeof(string));
            dtExport.Columns.Add("QuoteDetail", typeof(string));
            Cls_Order clsOrder = ServicesFactory.DocCMSServices.Fetch_Order_List_By_id(PurchaseOrderID);
            if (clsOrder != null)
            {

                DataRow drow = null;
                drow = dtExport.NewRow();
                drow["Quote"] = "Quote No";
                drow["QuoteDetail"] = Convert.ToString(clsOrder.orderno);


                dtExport.Rows.Add(drow);
                drow = dtExport.NewRow();
                drow["Quote"] = "Company Name";
                drow["QuoteDetail"] = clsOrder.companyname;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Purchaser Name";
                drow["QuoteDetail"] = clsOrder.purchasername;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Phone";
                drow["QuoteDetail"] = clsOrder.phone;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Email ID";
                drow["QuoteDetail"] = clsOrder.email;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Fax";
                drow["QuoteDetail"] = clsOrder.fax;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Payment method";
                drow["QuoteDetail"] = clsOrder.paymentmethod;
                dtExport.Rows.Add(drow);


                drow = dtExport.NewRow();
                drow["Quote"] = "Billing Address";
                drow["QuoteDetail"] = clsOrder.billingaddress1 + " " + clsOrder.billingaddress2 + "" + clsOrder.billingcity + ", " + clsOrder.billingprovience + " Postal Code- " + Convert.ToString(clsOrder.billingpostalcode);
                dtExport.Rows.Add(drow);


                drow = dtExport.NewRow();
                drow["Quote"] = "Shipping Address";
                drow["QuoteDetail"] = clsOrder.shippingaddress1 + " " + clsOrder.shippingaddress2 + "" + clsOrder.shippingcity + ", " + clsOrder.shippingprovience + " Postal Code- " + Convert.ToString(clsOrder.shippingpostalcode);
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "PO #";
                drow["QuoteDetail"] = clsOrder.ponumber;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Order Status";
                drow["QuoteDetail"] = clsOrder.orderstatus;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Other Detail";
                drow["QuoteDetail"] = clsOrder.otherdetail;
                dtExport.Rows.Add(drow);

                drow = dtExport.NewRow();
                drow["Quote"] = "Grand Total";
                string GrandTotal = ServicesFactory.DocCMSServices.Fetch_Grand_Total_from_OrderDetail(PurchaseOrderID);
                drow["QuoteDetail"] = "$" + GrandTotal;
                dtExport.Rows.Add(drow);

                RptrCompleteDetail.DataSource = dtExport;
                RptrCompleteDetail.DataBind();
            }
        }

        //Show Customer Detail
        protected void btnShowCustomerDetail_Click(object sender, EventArgs e)
        {
            Get_Complete_Order_Detail(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "NMAIIF", "<script>return FormatTable();</script>");
        }

        //Show Product to add
        protected void btnAddNewProduct_Click(object sender, EventArgs e)
        {
            Bind_Part_Category(62);
            AddProductList.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "NMAIIF", "<script>return FormatTable();</script>");
        }
        // Bind Part Category
        private void Bind_Part_Category(Int32 PageID)
        {
            try
            {
                DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Catalouge_category_cmsid(PageID);
                if (dtDetail != null && dtDetail.Rows.Count > 0)
                {
                    Bind_Parts_Content(Convert.ToString(dtDetail.Rows[0]["CategoryName"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Bind Product
        protected void Bind_Parts_Content(string CategoryName)
        {
            DataTable dtPartsBind = new DataTable();
            dtPartsBind.Columns.Add("CMSID");
            dtPartsBind.Columns.Add("ProductName");
            dtPartsBind.Columns.Add("ProductNumber");
            dtPartsBind.Columns.Add("Description");
            dtPartsBind.Columns.Add("InternalDescription");
            dtPartsBind.Columns.Add("ImageName");
            dtPartsBind.Columns.Add("CategoryName");
            dtPartsBind.Columns.Add("Cost");
            dtPartsBind.Columns.Add("Price");
            dtPartsBind.Columns.Add("Color");
            dtPartsBind.Columns.Add("Size");
            //Checking  if the page has Display order or not if not then it will not show display order column
            DataTable dtContent = null;
            if (Session["MemberID"] != null)
            {
                dtContent = ServicesFactory.DocCMSServices.Fetch_Catlouge_cmsid_By_category_name_FromCMS(CategoryName, 66, "All");
            }
            else
            {
                dtContent = ServicesFactory.DocCMSServices.Fetch_Catlouge_cmsid_By_category_name_FromCMS(CategoryName, 66, "Public");
            }
            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtContent.Rows.Count; i++)
                {
                    DataTable dtPartInfo = ServicesFactory.DocCMSServices.Fetch_Catalouge_By_SubType_id(Convert.ToInt32(dtContent.Rows[i]["CMSID"]));
                    if (dtPartInfo != null && dtPartInfo.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtPartInfo.Rows)
                        {
                            if (Convert.ToString(rw["ShowPrice"]).ToUpper().Equals("TRUE"))
                            {

                            }
                            else
                                rw["Price"] = "0.00";

                            string Color = "";
                            string Size = "";
                            DataTable dtSizeColor = ServicesFactory.DocCMSServices.Fetch_Product_Default_size_color(Convert.ToString(rw["CMSID"]));
                            if (dtSizeColor != null && dtSizeColor.Rows.Count > 0)
                            {
                                Color = Convert.ToString(dtSizeColor.Rows[0]["Color"]);
                                Size = Convert.ToString(dtSizeColor.Rows[0]["Size"]);
                            }
                            else
                            {
                                Color = "";
                                Size = "";
                            }
                            dtPartsBind.Rows.Add(Convert.ToString(rw["CMSID"]), Convert.ToString(rw["ProductName"]), Convert.ToString(rw["ProductNumber"]), Convert.ToString(rw["Description"]), Convert.ToString(rw["InternalDescription"]), Convert.ToString(rw["ImageName"]), Convert.ToString(rw["CategoryName"]), Convert.ToString(rw["Cost"]), Convert.ToString(rw["Price"]), Color, Size);
                        }
                    }
                }
                if (dtPartsBind != null && dtPartsBind.Rows.Count > 0)
                {
                    RptrParts.DataSource = dtPartsBind;
                    RptrParts.DataBind();
                }
                else
                {
                    RptrParts.DataSource = null;
                    RptrParts.DataBind();
                }
            }
            else
            {
                RptrParts.DataSource = null;
                RptrParts.DataBind();
            }
        }

        protected void RptrParts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            if (lblPrice != null && !string.IsNullOrEmpty(lblPrice.Text))
            {
                if (lblPrice.Text != "0")
                    lblPrice.Text = "$" + lblPrice.Text;
                else
                    lblPrice.Text = "$0";
            }
            Label lblImageName = (Label)e.Item.FindControl("lblImageName");
            HtmlImage myImg = (HtmlImage)e.Item.FindControl("myImg");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            LinkButton lnkViewMore = (LinkButton)e.Item.FindControl("lnkViewMore");
            if (lblImageName != null && myImg != null)
            {
                if (!string.IsNullOrEmpty(lblImageName.Text))
                {
                }
                else
                    myImg.Src = "~/UploadedFiles/No_image_available.png";
            }
            string Description = "";
            if (lblDescription.Text.Length > 30)
            {
                Description = lblDescription.Text.ToString();
                lblDescription.Text = Description.Substring(0, 30) + "...";
            }
            else
            {
            }
        }
    }
}