﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideModuleDetail : System.Web.UI.Page
    {
        dynamic UserId;
    
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] != null && Request.QueryString["MID"] != null)
                {
                    hdnMainContentID.Value = Convert.ToString(Request.QueryString["MID"]);
                    Bind_UserGuideModuleDetail_ById_Data(Convert.ToInt32(Request.QueryString["ID"]));
                    Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                    btnsubmit.Visible = false;
                    btnupdate.Visible = true;
                }
                else if (Request.QueryString["MID"] != null)
                {
                    hdnMainContentID.Value = Convert.ToString(Request.QueryString["MID"]);
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_ModuleDetailContent_dorder(Convert.ToInt32(hdnMainContentID.Value)) + 1;
                    Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    btnsubmit.Visible = true;
                    btnupdate.Visible = false;
                }
            }
        }

        protected void Bind_UserGuideModuleDetail_ById_Data(Int32 ModuleDetailID)
        {
            try
            {
                Cls_UserGuideModuleDetail objUserGuideModule = ServicesFactory.DocCMSServices.Fetch_UserGuideModuleDetail_Byid(ModuleDetailID);
                if (objUserGuideModule != null)
                {
                    txtContentheading.Value = objUserGuideModule.contentheading;
                    elm1.Value = objUserGuideModule.description;
                    txtDisplayOrder.Value = Convert.ToString(objUserGuideModule.displayorder);
                    ChkActiveStatus.Checked = objUserGuideModule.isactive;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_DisplayOrder(Int32 MainContentID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_ModuleDetailContent_Byid(MainContentID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_UserGuideModuleDetail clsUserGuideModuleDetail = new Cls_UserGuideModuleDetail();
                clsUserGuideModuleDetail.contentheading = txtContentheading.Value;
                clsUserGuideModuleDetail.description = elm1.Value;
                clsUserGuideModuleDetail.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                clsUserGuideModuleDetail.maincontentid = Convert.ToInt32(hdnMainContentID.Value);
                clsUserGuideModuleDetail.createdby = Guid.Parse(UserId);
                if (ChkActiveStatus.Checked)
                    clsUserGuideModuleDetail.isactive = true;
                else
                    clsUserGuideModuleDetail.isactive = false;

                Int32 RetCheck = 0;
                RetCheck = ServicesFactory.DocCMSServices.Check_Existing_USerGuideContentDetail(txtContentheading.Value.Trim());
                if (RetCheck > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Content heading with Same Name Already Exists...');</script>");
                    return;
                }
                else
                {
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_UserGuideContentDetail(clsUserGuideModuleDetail);
                    Response.Redirect("~/cadmin/UserGuideModuloeDetailDashboard.aspx?MID=" + Convert.ToInt32(hdnMainContentID.Value));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["ID"] != null)
                {
                    Cls_UserGuideModuleDetail clsUserGuideModuleDetail = new Cls_UserGuideModuleDetail();
                    clsUserGuideModuleDetail.contentheading = txtContentheading.Value;
                    clsUserGuideModuleDetail.description = elm1.Value;
                    clsUserGuideModuleDetail.displayorder = Convert.ToInt32(txtDisplayOrder.Value);
                    clsUserGuideModuleDetail.maincontentid = Convert.ToInt32(hdnMainContentID.Value);
                    clsUserGuideModuleDetail.createdby = Guid.Parse(UserId);
                    clsUserGuideModuleDetail.moduledetailid = Convert.ToInt32(Request.QueryString["ID"]);
                    if (ChkActiveStatus.Checked)
                        clsUserGuideModuleDetail.isactive = true;
                    else
                        clsUserGuideModuleDetail.isactive = false;
                    Int32 retVal = ServicesFactory.DocCMSServices.Update_UserGuideContentDetail(clsUserGuideModuleDetail);
                    if (retVal > 0)
                    {
                        ServicesFactory.DocCMSServices.Adjust_MuduleDetailContent_dorder(Convert.ToInt32(hdnMainContentID.Value));
                    }
                    Response.Redirect("~/cadmin/UserGuideModuloeDetailDashboard.aspx?MID=" + Convert.ToInt32(hdnMainContentID.Value));
                }
                else
                {
                    // Do nothing
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideModuloeDetailDashboard.aspx?MID=" + Convert.ToInt32(hdnMainContentID.Value));
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTkable();</script>");
        }
        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_UserGuideModuleDetail> lstUserGuideDetail = new List<Cls_UserGuideModuleDetail>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblModuleDetailID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblModuleDetailID");
                Label lblContentHeading = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblContentHeading");
                if (Session["UserID"] != null && lblContentHeading != null && lblModuleDetailID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblContentHeading.Text + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_UserGuideModuleDetail objUserGuideModules = new Cls_UserGuideModuleDetail();
                Label lblModuleDetailID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblModuleDetailID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblModuleDetailID != null)
                {
                    objUserGuideModules.moduledetailid = Convert.ToInt32(lblModuleDetailID.Text.Trim());
                    objUserGuideModules.displayorder = Convert.ToInt32(txtDorder.Value);
                    objUserGuideModules.modifiedby = Guid.Parse(UserId);
                    lstUserGuideDetail.Add(objUserGuideModules);
                }
            }
            if (lstUserGuideDetail != null && lstUserGuideDetail.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_UserGuideDetail_dorder(lstUserGuideDetail);
                if (retVal > 0)
                {
                    if (Request.QueryString["ID"] != null && Request.QueryString["MID"] != null)
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["ID"] + "," + Request.QueryString["MID"] + ");</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg1();</script>");
                        Bind_DisplayOrder(Convert.ToInt32(hdnMainContentID.Value));
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
        }
    }
}