﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class SMTPSettings : System.Web.UI.Page
    {
        dynamic UserId;
        protected static Int32 SMTPDetailID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            Session["HiddenRow"] = null;
            if (!IsPostBack)
            {

                Bind_SMTP();
                if (!string.IsNullOrEmpty(hdnSMTPId.Value))
                {
                    btnsubmitMaster.Visible = false;
                    btnMasterupdate.Visible = true;
                    Bind_Emails();
                    Set_Controls();
                }
                else
                {

                    btnsubmitMaster.Visible = true;
                    btnMasterupdate.Visible = false;
                }
            }
        }

        public void Set_Controls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubmoduleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (RepeaterItem item in rptrSMTP.Items)
                {
                    LinkButton LnkDepatment = (LinkButton)item.FindControl("LnkDepatment");
                    Label lnkPageName = (Label)item.FindControl("lnkPageName");
                    ImageButton lnkEdit = (ImageButton)item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)item.FindControl("lnkDelete");
                    if (LnkDepatment != null && lnkPageName != null && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            LnkDepatment.Visible = true;
                            lnkPageName.Visible = false;
                            ImgAddSMTPDetail.Visible = true;
                            btnMasterupdate.Visible = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            LnkDepatment.Visible = false;
                            lnkPageName.Visible = true;
                            ImgAddSMTPDetail.Visible = false;
                            btnMasterupdate.Visible = false;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        protected void btnsubmitMaster_onclick(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPswd.Value))
                {
                    lblReqPass.Visible = true;
                    return;
                }
                else
                {
                    lblReqPass.Visible = false;
                }
                Cls_SMTP clsSMTP = new Cls_SMTP();
                clsSMTP.smtphostserver = txthost.Text.Trim();
                clsSMTP.smtpport = txtPort.Value;
                clsSMTP.senderemailid = txtSenderEmail.Text;
                clsSMTP.senderpassword = txtPswd.Value;
                if (ChkActiveStatus.Checked)
                    clsSMTP.smtpsslserver = true;
                else
                    clsSMTP.smtpsslserver = false;

                //Now After Insert new Value
                Int32 retVal = ServicesFactory.DocCMSServices.Insert_SMTP(clsSMTP);
                if (retVal > 0)
                {
                    hdnSMTPId.Value = Convert.ToString(retVal);
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('SMTP Settings saved successfully');</script>");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Error occur in Saving...');</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnMasterupdate_onclick(object sender, EventArgs e)
        {
            try
            {
                Cls_SMTP clsSMTP = new Cls_SMTP();
                clsSMTP.smtphostserver = txthost.Text.Trim();
                clsSMTP.smtpport = txtPort.Value;
                clsSMTP.senderemailid = txtSenderEmail.Text;
                clsSMTP.smtpid = Convert.ToInt32(hdnSMTPId.Value);
                if (string.IsNullOrEmpty(txtPswd.Value) && string.IsNullOrEmpty(lblPassword.Text))
                {
                    lblReqPass.Visible = true;
                    return;
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtPswd.Value))
                    {
                        clsSMTP.senderpassword = txtPswd.Value;
                    }
                    else
                    {
                        clsSMTP.senderpassword = lblPassword.Text;
                    }
                }

                if (ChkActiveStatus.Checked)
                    clsSMTP.smtpsslserver = true;
                else
                    clsSMTP.smtpsslserver = false;

                //First Delete The existing SMPID
                //Now After deleting Insert new Value
                Int32 retVal = ServicesFactory.DocCMSServices.Update_SMTP(clsSMTP);
                if (retVal > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('SMTP Settings updated successfully');</script>");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Error occur in Updating...');</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/ContentPageDashboard.aspx");
        }
        // Bind SMTP function
        private void Bind_SMTP()
        {
            try
            {
                Cls_SMTP objSMTP = ServicesFactory.DocCMSServices.Fetch_SMTP();
                if (objSMTP != null)
                {
                    hdnSMTPId.Value = Convert.ToString(objSMTP.smtpid);
                    txthost.Text = Convert.ToString(objSMTP.smtphostserver);
                    txtPort.Value = Convert.ToString(objSMTP.smtpport);
                    txtSenderEmail.Text = Convert.ToString(objSMTP.senderemailid);
                    lblPassword.Text = Convert.ToString(objSMTP.senderpassword);
                    txtPswd.Attributes["value"] = lblPassword.Text;
                    ChkActiveStatus.Checked = objSMTP.smtpsslserver;
                    Bind_Recipient_Emails(hdnSMTPId.Value);
                }
                else
                {
                    hdnSMTPId.Value = "";
                    txtPort.Value = "";
                    txtSenderEmail.Text = "";
                    txtPswd.Value = "";
                    txthost.Text = "";
                    ChkActiveStatus.Checked = true;
                    btnsubmitMaster.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_Recipient_Emails(string smtpID)
        {

        }

        protected void btnSaveSMTPDetail_Click(object sender, ImageClickEventArgs e)
        {
            Int32 CheckExistDepatment = ServicesFactory.DocCMSServices.Check_Existing_department(drpDepartment.SelectedValue, Convert.ToInt32(hdnSMTPId.Value));
            if (CheckExistDepatment == 0)
            {
                Cls_SMTP clsSMTP = new Cls_SMTP();
                clsSMTP.toemail = txtToEmail.Text.Trim();
                clsSMTP.bccemail = txtBcc.Text.Trim();
                clsSMTP.ccemail = txtCC.Text.Trim();
                clsSMTP.department = drpDepartment.SelectedValue;
                clsSMTP.smtpid = Convert.ToInt32(hdnSMTPId.Value);
                if (chkEmailActive.Checked)
                    clsSMTP.isactive = true;
                else
                    clsSMTP.isactive = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Insert_SMTP_email_Detail(clsSMTP);
                if (retVal > 0)
                {
                    trAdditionalRow.Visible = false;
                    Bind_Emails();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "javascript:alert('Email has already been setup for this deparment.');", true);
                return;
            }
        }


        protected void Bind_Emails()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SMTP_email_Detail(Convert.ToInt32(hdnSMTPId.Value));
                if (dt != null && dt.Rows.Count > 0)
                {
                    rptrSMTP.DataSource = dt;
                    rptrSMTP.DataBind();
                }
                else
                {

                    rptrSMTP.DataSource = null;
                    rptrSMTP.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnUpdateSMTPDetail_Click(object sender, ImageClickEventArgs e)
        {
            Int32 CheckExistDepatment = ServicesFactory.DocCMSServices.Check_Existing_department_For_Update(drpDepartment.SelectedValue, Convert.ToInt32(hdnSMTPId.Value), SMTPDetailID);
            if (CheckExistDepatment == 0)
            {
                Cls_SMTP clsSMTP = new Cls_SMTP();
                clsSMTP.toemail = txtToEmail.Text.Trim();
                clsSMTP.bccemail = txtBcc.Text.Trim();
                clsSMTP.ccemail = txtCC.Text.Trim();
                clsSMTP.department = drpDepartment.SelectedValue;
                clsSMTP.smtpid = Convert.ToInt32(hdnSMTPId.Value);
                clsSMTP.id = SMTPDetailID;
                if (chkEmailActive.Checked)
                    clsSMTP.isactive = true;
                else
                    clsSMTP.isactive = false;
                Int32 retVal = ServicesFactory.DocCMSServices.Update_SMTP_email_Detail(clsSMTP);
                if (retVal > 0)
                {
                    trAdditionalRow.Visible = false;
                    Bind_Emails();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "javascript:alert('Email has already been setup for this deparment.');", true);
                return;
            }
        }


        protected void ImgAddSMTPDetail_Click(object sender, ImageClickEventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnSMTPId.Value))
            {
                trAdditionalRow.Visible = true;
                drpDepartment.SelectedIndex = 0;
                txtToEmail.Text = "";
                txtBcc.Text = "";
                txtCC.Text = "";
                chkEmailActive.Checked = true;
                txtEmailDorder.Text = Convert.ToString(ServicesFactory.DocCMSServices.Get_Max_SMTP_email_dorder(Convert.ToInt32(hdnSMTPId.Value)) + 1);
                btnUpdateSMTPDetail.Visible = false;
                btnSaveSMTPDetail.Visible = true;
                Bind_Emails();
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "javascript:alert('First create a SMTP settings for email then add SMTP email detail');", true);
                return;
            }
        }
        protected void rptrSMTP_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandArgument != null)
            {
                switch (e.CommandName)
                {
                    case "DeleteSMTPDetail":
                        Int32 retDel = ServicesFactory.DocCMSServices.Delete_SMTP(Convert.ToInt32(e.CommandArgument));
                        if (retDel > 0)
                        {
                            Bind_Emails();
                        }
                        break;
                    case "EditEmail":

                        e.Item.Visible = false;
                        Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                        if (lblDorder != null)
                        {
                            Edit_SMTP_Email_ById(Convert.ToInt32(e.CommandArgument), lblDorder.Text);
                            Session["HiddenRow"] = Convert.ToString(e.Item.ItemIndex);
                            Bind_Emails();
                        }

                        break;
                }
            }
        }
        protected void Edit_SMTP_Email_ById(Int32 ID, string SNo)
        {
            Cls_SMTP clsSmtp = new Cls_SMTP();
            clsSmtp = ServicesFactory.DocCMSServices.Fetch_SMTP_email_Detail_By_id(ID);
            if (clsSmtp != null)
            {
                trAdditionalRow.Visible = true;
                btnUpdateSMTPDetail.Visible = true;
                btnSaveSMTPDetail.Visible = false;
                SMTPDetailID = clsSmtp.id;
                txtToEmail.Text = clsSmtp.toemail;
                txtEmailDorder.Text = SNo;
                txtBcc.Text = clsSmtp.bccemail;
                txtCC.Text = clsSmtp.ccemail;
                drpDepartment.SelectedValue = clsSmtp.department;
                chkEmailActive.Checked = clsSmtp.isactive;

            }

        }
        protected void rptrSMTP_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                if (Session["HiddenRow"] != null)
                {
                    string ItemIndex = Convert.ToString(Session["HiddenRow"]);
                    if (e.Item.ItemIndex.ToString() == ItemIndex)
                    {
                        e.Item.Visible = false;
                    }
                }
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                if (lblDorder != null)
                {
                    lblDorder.Text = Convert.ToString(e.Item.ItemIndex + 1);
                }
            }
        }

        protected void btnCancelSMTP_Click(object sender, EventArgs e)
        {

        }



        protected void btnCancelSMTPDetail_Click(object sender, ImageClickEventArgs e)
        {
            trAdditionalRow.Visible = false;
            Bind_Emails();
        }
    }
}