﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="ContentManagement.aspx.cs" Inherits="DocCMSMain.cadmin.ContentManagement" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/bootstrap-fileupload.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <script src="../js/tinymce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wysiwyg.js"></script>
    <script type="text/javascript" src="../js/ContentSpecialScripts.js"></script>
    <link href="../js/Jcrop/Jcrop.css" rel="stylesheet" type="text/css" />
    <link href="css/colopicker/css/jquery.minicolors.css" rel="stylesheet" type="text/css" />
    <script src="css/colopicker/js/jquery.minicolors.min.js" type="text/javascript"></script>
    <script src="../js/Jcrop/Jcrop.js" type="text/javascript"></script>
    <script src="../js/Pages/media_manager.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     //======================== Save Data if system is Idle start
        var IDLE_TIMEOUT = 300; // 300 seconds i.e. 5 minutes
        var _idleSecondsCounter = 0;
        document.onclick = function () {
            _idleSecondsCounter = 0;
        };
        document.onmousemove = function () {
            _idleSecondsCounter = 0;
        };
        document.onkeypress = function () {
            _idleSecondsCounter = 0;
        };
        window.setInterval(CheckIdleTime, 1000);
        function CheckIdleTime() {
            _idleSecondsCounter++;
            var oPanel = document.getElementById("SecondsUntilExpire");
            if (oPanel)
                oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                document.getElementById("btncontinue").click();
            }
        }
        //======================== Save Data if system is Idle End
        //== Auto save data on browser close start
        //== Auto save data on browser close end
            jQuery(function () {
                var colpick = jQuery('.demo').each(function () {
                    jQuery(this).minicolors({
                        control: jQuery(this).attr('data-control') || 'hue',
                        inline: jQuery(this).attr('data-inline') === 'true',
                        letterCase: 'lowercase',
                        opacity: false,
                        change: function (hex, opacity) {
                            if (!hex) return;
                            if (opacity) hex += ', ' + opacity;
                            try {
                                console.log(hex);
                            } catch (e) { }
                            jQuery(this).select();
                        },
                        theme: 'bootstrap'
                    });
                });
                var $inlinehex = jQuery('#inlinecolorhex h3 small');
                jQuery('#inlinecolors').minicolors({
                    inline: true,
                    theme: 'bootstrap',
                    change: function (hex) {
                        if (!hex) return;
                        $inlinehex.html(hex);
                    }
                });
            });
            jQuery(document).ready(function () {
                SearchText();
                if (jQuery("#DivTextBox .control-label").val() == "Heading") {
                    alert("dd");
                }
            });

           function SearchText() {
                jQuery("#txtNavigationUrl").autocomplete({
                    source: function (request, response) {
                        jQuery.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "../cadmin/SubModules.aspx/GetKeyWords",
                            data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                            dataType: "json",
                            success: function (data) {
                                response(data.d);
                            },
                            error: function (result) {
                                alert("No Match");
                            }
                        });
                    }
                });
            }

            function SearchText_btn() {
                jQuery.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "../cadmin/SubModules.aspx/GetKeyWords_btnClick",
                    data: "{'SearchName':'" + document.getElementById('txtNavigationUrl').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        var dataArray = data.d.toString().split(',');
                        var List = "";
                        for (var i = 0; i < dataArray.length; i++) {
                            List = List + "<li class='ui-menu-item customMenu' role='menuitem' id='liAutoComplete_" + i.toString() + "'>";
                            var NavigateURL = "";
                            if (i == 0) {
                                NavigateURL = dataArray[i].toString().trim().substring(2);
                                NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                            }
                            else if (i == dataArray.length - 1) {
                                NavigateURL = dataArray[i].toString().trim().substring(1);
                                NavigateURL = NavigateURL.substring(0, NavigateURL.length - 2);
                            }
                            else {
                                NavigateURL = dataArray[i].toString().trim().substring(1);
                                NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                            }
                            if (dataArray.length == 1) {
                                NavigateURL = NavigateURL.substring(0, NavigateURL.length - 1);
                            }
                            List = List + "<a class='ui-corner-all' tabindex='-1' id='ancAutoComplete_" + i.toString() + "' onclick='SetValueInTextBox(this);'>" + NavigateURL + "</a>";
                            List = List + "</li>";
                        }
                        jQuery("#ulAutocomplete").html(List);
                        jQuery("#ulAutocomplete").show();
                    },
                    error: function (result) {
                        alert("No Match");
                    }
                });
            }

            function SetValueInTextBox(ctrlID) {
                jQuery("#txtNavigationUrl").val(jQuery("#" + ctrlID.id).text());
                jQuery("#ulAutocomplete").hide();
            }
    </script>
    <script type="text/javascript" language="javascript">
        //Start Image Preview Function
        function ShowPreviewImage(input) {
            if (input.files && input.files[0]) {
                var ImageDir = new FileReader();
                ImageDir.onload = function (e) {
                    jQuery('#imgPrieview').attr('src', e.target.result);
                }
                ImageDir.readAsDataURL(input.files[0]);
            }
        }   

        //Check Size Checkbox
        function CheckSizeFunctionImage(Checkbox) {
            if (jQuery("#" + Checkbox.id).is(":checked")) {
                jQuery("#btnSize").show();
            }
            else if (jQuery("#" + Checkbox.id).is(":not(:checked)")) {
                jQuery("#btnSize").hide();
            }
        }

        //Check Color Checkbox
        function CheckColorFunctionImage(Checkbox) {
            if (jQuery("#" + Checkbox.id).is(":checked")) {
                jQuery("#btnColor").show();
            }
            else if (jQuery("#" + Checkbox.id).is(":not(:checked)")) {
                jQuery("#btnColor").hide();
            }
        }

        // Open Size Model popup
        function OpenSizeModelImage() {
            jQuery("#DivSizeModel").toggle();
        }

        // Toggel Size Model popup
        function CancelSizeModelImage() {
            jQuery("#DivSizeModel").toggle();
        }

        // Open Color Model popup
        function OpenColorModelImage() {
            jQuery("#DivColorModel").toggle();
        }
        // Close Color Model popup
        function CancelColorModelImage() {
            jQuery("#DivColorModel").hide();
        }

        //Bind Size On Grid
        function InsertSizeImage() {
            if (jQuery("#txtsize").val() == "") {
                alert("Please enter size");
                return false;
            }
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Insert_Product_Size",
                data: { "Size": document.getElementById('txtsize').value, "CMSID": document.getElementById('hdnCMSID').value },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null && data != "") {
                        document.getElementById('txtsize').value = "";
                        BindSizeOnGridImage();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }


        //Bind Size On Grid
        function BindSizeOnGridImage() {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Fetch_Product_Size",
                data: { "CMSID": document.getElementById('hdnCMSID').value },
                dataType: "json",
                async: false,
                success: function (data) {
                    var htmlUser = "";
                    if (data != null && data != "") {
                        jQuery("#tblSize").show();
                        var result = jQuery.parseJSON(data);
                        if (result != null && result.length > 0) {
                            var html = "";
                            for (var i = 0; i < result.length; i++) {
                                if (i % 2 == 0) {
                                    html += "<tr class='gradeX'>";
                                    html += "<td>";
                                    html += "" + result[i].Size + "";
                                    html += "</td>";
                                    html += "<td>";
                                    html += "<input type='button' id='btnRemoveSize1' value='Remove' class='btn btn-primary' onclick='RemovesizeImage(" + result[i].SizeId + ")' />";
                                    html += "</td>";
                                    html += "</tr>";
                                }
                                else {
                                    html += "<tr class='gradeA'>";
                                    html += "<td>";
                                    html += "" + result[i].Size + "";
                                    html += "</td>";
                                    html += "<td>";
                                    html += "<input type='button' id='btnRemoveSize2' value='Remove' class='btn btn-primary' onclick='RemovesizeImage(" + result[i].SizeId + ")' />";
                                    html += "</td>";
                                    html += "</tr>";
                                }
                             }
                            jQuery("#BindSize").html(html);
                        }
                    }
                    else {
                        html = "";
                        jQuery("#BindSize").html(html);
                        jQuery("#tblSize").hide();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        //Remove Size
        function RemovesizeImage(SizeId) {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Remove_Product_Size",
                data: { "SizeId": SizeId },
                dataType: "json",
                async: false,
                success: function (data) {
                    var htmlUser = "";
                    if (data != null && data != "") {
                        if (data == "Success") {
                            BindSizeOnGridImage();
                        } 
                        else {
                        }
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }


        //Insert Color using Jquery
        function InsertColorIntoDatabaseImage(ImageName) {
            if (jQuery("#txtcolor").val() == "") {
                alert("Please enter Color");
                return false;
            }
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Insert_Color_Details",
                data: { "CMSID": document.getElementById('hdnCMSID').value, "Color": document.getElementById('txtcolor').value, "ImageName": ImageName },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null && data != "") {
                        document.getElementById('txtcolor').value = "";
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        //Insert Color Image
        function InsertColorImage() {
            var fileUpload = jQuery("#UploadImage").get(0);
            var files = fileUpload.files;
            var test = new FormData();
            for (var i = 0; i < files.length; i++) {
                test.append(files[i].name, files[i]);
            }
            jQuery.ajax({
                url: "../Uploadhandler/InsertColorImage.ashx",
                type: "POST",
                contentType: false,
                processData: false,
                data: test,
                success: function (result) {
                    InsertColorIntoDatabaseImage(result);
                    BindColorDetailsImage();
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }

        //Retrieve colorData
        function Fetch_Color_DetailImage(ColorId) {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Fetch_Color_Details_By_ColorId",
                data: { "ColorId": ColorId },
                dataType: "json",
                async: false,
                success: function (data) {
                    var htmlUser = "";
                    if (data != null && data != "") {
                        var result = jQuery.parseJSON(data);
                        if (result != null && result.length > 0) {
                            var html = "";
                            for (var i = 0; i < result.length; i++) {
                                jQuery("#hdnColorId").val(result[0].ColorId);
                                jQuery("#txtcolor").val(result[0].Color);
                                jQuery("#imgPrieview").attr("src", "../UploadedFiles/ColorImages/" + result[0].Image);
                                jQuery("#btnColorUpdate").show();
                                jQuery("#btnSubmitColor").hide();
                            }
                        }
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }


        //Update Color
        function UploadColorImage() {
            var fileUpload = jQuery("#UploadImage").get(0);
            var files = fileUpload.files;
            var test = new FormData();
            for (var i = 0; i < files.length; i++) {
                test.append(files[i].name, files[i]);
            }
            jQuery.ajax({
                url: "../Uploadhandler/InsertColorImage.ashx",
                type: "POST",
                contentType: false,
                processData: false,
                data: test,
                success: function (result) {
                    UpdateColorIntoDatabaseImage(result);
                    BindColorDetailsImage();
                    jQuery("#btnColorUpdate").hide();
                    jQuery("#btnSubmitColor").show();
                    jQuery("#imgPrieview").attr("src", "../DFI/images/NoImage.png");
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }

        function UpdateColorIntoDatabaseImage(ImageName) {
            if (jQuery("#txtcolor").val() == "") {
                alert("Please enter Color");
                return false;
            }
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Update_Color_Image",
                data: { "ColorId": document.getElementById('hdnColorId').value, "CMSID": document.getElementById('hdnCMSID').value, "Color": document.getElementById('txtcolor').value, "ImageName": ImageName },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null && data != "") {
                        document.getElementById('txtcolor').value = "";
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }


        // Bind Color Details
        function BindColorDetailsImage() {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Fetch_Color_Details",
                data: { "CMSID": document.getElementById('hdnCMSID').value },
                dataType: "json",
                async: false,
                success: function (data) {
                    var htmlUser = "";
                    if (data != null && data != "") {
                        jQuery("#TblColor").show();
                        var result = jQuery.parseJSON(data);
                        if (result != null && result.length > 0) {
                            var html = "";
                            for (var i = 0; i < result.length; i++) {
                                if (i % 2 == 0) {
                                    html += "<tr class='gradeX'>";
                                    html += "<td>";
                                    html += "" + result[i].Color + "";
                                    html += "</td>";
                                    html += "<td>";
                                    html += " <img src='../UploadedFiles/ColorImages/" + result[i].Image + "' style='width:200px;height :100px;' />";
                                    html += "</td>";
                                    html += "<td>";
                                    html += "<input type='button' id='btnRemoveColor' class='btn btn-primary' value='Remove' onclick='Removecolor(" + result[i].ColorId + ")' />";
                                    html += "<input type='button' id='btnUpdate' class='btn btn-primary' value='Update' onclick='Fetch_Color_DetailImage(" + result[i].ColorId + ");' />"
                                    html += "</td>";
                                    html += "</tr>";
                                }
                                else {
                                    html += "<tr class='gradeA'>";
                                    html += "<td>";
                                    html += "" + result[i].Color + "";
                                    html += "</td>";
                                    html += "<td>";
                                    html += " <img src='../UploadedFiles/ColorImages/" + result[i].Image + "' style='width:200px;height :100px;' />";
                                    html += "</td>";
                                    html += "<td>";
                                    html += "<input type='button' id='btnRemoveColor' class='btn btn-primary' value='Remove' onclick='Removecolor(" + result[i].ColorId + ")' />";
                                    html += "<input type='button' id='btnUpdate' class='btn btn-primary' value='Update' onclick='Fetch_Color_DetailImage(" + result[i].ColorId + ");' />"
                                    html += "</td>";
                                    html += "</tr>";
                                }
                            }
                            jQuery("#TbodyColor").html(html);
                        }
                    }
                    else {
                        html = "";
                        jQuery("#TbodyColor").html(html);
                        jQuery("#TblColor").hide();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        // Remove Image
        function Removecolor(ColorId) {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Remove_Product_Color",
                data: { "ColorId": ColorId },
                dataType: "json",
                async: false,
                success: function (data) {
                    var htmlUser = "";
                    if (data != null && data != "") {
                        if (data == "Success") {
                            BindColorDetailsImage();
                        } else {

                        }
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        // function change event of color picker
        function ColorChange(Textbox) {
            var colorName = jQuery("#" + Textbox.id).val();
            var Image = jQuery("#ImgResizeImage").attr("src");
            jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
            jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
        }
    </script>
   <script type="text/javascript">
       jQuery(document).ready(function () {
             var jcrop_api;
             var url = window.location;
             if (url.toString().indexOf("?EmailID=") > -1) {
                 // Do nothing
             }
             else {
                 jQuery("#txtEmailId").val("");
                 jQuery("#txtNewPswd").val("");
             }
             OnPageLoad();
             jQuery("#chkResizeImageCreate").click(function () {
                 if (jQuery(this).is(':checked')) {
                     jQuery("#chkCustomeImageCreate").prop("checked", false);
                     jQuery("#divCustomResizeImageCreate").show();
                     jQuery("#divCustomImageCreate").hide();
                     jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                     jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                     CheckResizeValues();
                 } else {
                     jQuery("#chkCustomeImageCreate").prop("checked", true);
                     jQuery("#divCustomResizeImageCreate").hide();
                     jQuery("#divCustomImageCreate").show();
                     jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
                     jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
                 }
             });
             jQuery("#chkCustomeImageCreate").click(function () {//
                 if (jQuery(this).is(':checked')) {
                     jQuery("#chkResizeImageCreate").prop("checked", false);
                     jQuery("#uniform-chkResizeImageCreate").find("span").removeClass("checked");
                     jQuery("#uniform-chkCustomeImageCreate").find("span").addClass("checked");
                     jQuery("#divCustomImageCreate").show();
                     jQuery("#divCustomResizeImageCreate").hide();
                     var MiddleImageWidth = parseInt(jQuery("#hdnActualImageWidth").val()) / 6;
                     var MiddleImageHeight = parseInt(jQuery("#hdnActualImageHeight").val()) / 6;
                     var MarginTopAndBottom = parseInt(200 - MiddleImageHeight) / 2;
                     jQuery("#ImgCustomeImagePreviewRunning").css("margin-top", parseInt(MarginTopAndBottom));
                     jQuery("#ImgCustomeImagePreviewRunning").css("margin-bottom", parseInt(MarginTopAndBottom));
                     jQuery("#ImgCustomeImagePreviewRunning").css("width", parseInt(MiddleImageWidth));
                     jQuery("#ImgCustomeImagePreviewRunning").css("height", parseInt(MiddleImageHeight));
                 } else {
                     jQuery("#chkResizeImageCreate").prop("checked", true);
                     jQuery("#divCustomImageCreate").hide();
                     jQuery("#divCustomResizeImageCreate").show();
                     jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
                     jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
                 }
             });
             jQuery("#chkFinalCrop").click(function () {//
                 if (jQuery(this).is(':checked')) {

                     Set_Cropper(1);
                     jQuery("#btnCropWithResize").show();
                 } else {
                     Set_Cropper(0);
                     jQuery("#btnCropWithResize").hide();
                 }
             });
             jQuery("#chkAspectRatioResize").click(function () {//
                 if (jQuery(this).is(':checked')) {
                     jQuery("#hdnResizeAsepctRatio").val("True");
                     setTimeout(function () { PreviewResizedImage(); }, 200);
                     jQuery("#uniform-chkAspectRatioResize").find("span").addClass("checked");

                 } else {
                     jQuery("#hdnResizeAsepctRatio").val("False");
                     jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
                     setTimeout(function () { PreviewResizedImage(); }, 200);
                 }
             });
             jQuery("#chkFinalCrop").click(function () {//
                 if (jQuery(this).is(':checked')) {
                     Set_Cropper(1);
                     jQuery("#btnCropWithResize").show();
                 } else {
                     Set_Cropper(0);
                     jQuery("#btnCropWithResize").hide();
                 }
             });
         });
         function OnPageLoad() {
             jQuery("#uniform-chkResizeImageCreate").find("span").addClass("checked");
             jQuery("#chkResizeImageCreate").prop("checked", true);
             jQuery("#uniform-chkCustomeImageCreate").find("span").removeClass("checked");
             jQuery("#chkCustomeImageCreate").prop("checked", false);
             jQuery("#txtCustomWidth").val("1800");
             jQuery("#txtCustomHeight").val("1200");
             jQuery("#chkAspectRatioResize").prop("checked", false);
             jQuery("#hdnResizeAsepctRatio").val("False");
             jQuery("#uniform-chkAspectRatioResize").find("span").removeClass("checked");
         }
    </script>
    <script type="text/javascript">
       jQuery(document).ready(function () {
           var url = window.location;
           if (url.toString().indexOf("?EmailID=") > -1) {
               // Do nothing
           }
           else {
               jQuery("#txtEmailId").val("");
               jQuery("#txtNewPswd").val("");
           }
       });
    </script>
    <script type="text/javascript" language="javascript">
        function clearText(ctrl, defaultText) {
            if (ctrl.value == defaultText)
                ctrl.value = ""
            ctrl.style.color = "#000000";
        }
        function resetText(ctrl, defaultText) {
            if (ctrl.value == "") {
                ctrl.value = defaultText
                ctrl.style.color = "#C0C0C0";
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        function UploadFile(fileUpload) {
            if (fileUploader.value != '') {
                document.getElementById("<%=Upload.ClientID %>").click();
            }
        }
        function validateKey(e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if ((key >= 48 && key <= 57)) {
                return true;
            }
            else if ((key == 0 || key == 8 || key == 9 || key == 13 || key == 27 || key == 32 || key == 127)) {
                return true;
            }
            else {
                return false;
            }
        }
        function datepicker(ctrl) {
            jQuery("#" + ctrl.id).datepicker();
        }
        function showAutoComlete(ctrl) {
            jQuery("#" + ctrl.id).autocomplete({
                source: function (request, response) {
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../cadmin/SubModules.aspx/GetKeyWords",
                        data: "{'SearchName':'" + document.getElementById(ctrl.id).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        //-------image------
        function ColorChange(Textbox) {
            var colorName = jQuery("#" + Textbox.id).val();
            var Image = jQuery("#ImgResizeImage").attr("src");
            jQuery("#ImgCustomeImagePreviewRunning").attr("src", Image);
            jQuery("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
        }
        function ManageAspectRatio(ratioparam, e) {
            var key;
            if (window.event) {
                key = window.event.keyCode;     //IE
            }
            else {
                key = e.which;      //firefox              
            }
            if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
                if (jQuery("#chkAspectRatioResize").is(':checked')) {
                    if (ratioparam == "w") {
                        var width = jQuery("#txtCustomWidth").val();
                        if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                            jQuery("#txtCustomWidth").css("color", "Red");
                            jQuery("#txtCustomHeight").val("0");
                        }
                        else {
                            var height = parseFloat(jQuery("#txtCustomWidth").val()) / 1.333;
                            jQuery("#txtCustomHeight").val(parseInt(height));
                        }
                    }
                    else {
                        var height = jQuery("#txtCustomHeight").val();
                        if (parseInt(jQuery("#txtCustomWidth").val()) > 3200) {
                            jQuery("#txtCustomHeight").css("color", "Red");
                            jQuery("#txtCustomWidth").val("0");
                        }
                        else {
                            var width = parseFloat(jQuery("#txtCustomHeight").val()) * 1.333;
                            jQuery("#txtCustomWidth").val(parseInt(width));
                        }
                    }
                }
                else {
                    jQuery("#ImgResizePreview").Jcrop({
                        onSelect: SelectCropArea,
                        bgFade: true

                    }, function () {
                        jcrop_api = this;
                    });
                    jcrop_api.destroy();
                    PreviewResizedImage();
                    jQuery("#ImgResizePreview").css("visibility", "visible");
                }
            }
            else {
                return false;
            }
        }
        function CheckResizeValues() {
            setTimeout(function () { PreviewResizedImage(); }, 200);
        }
        function PreviewResizedImage() {
            var actWidth = jQuery("#hdnActualImageWidth").val();
            var actHeight = jQuery("#hdnActualImageHeight").val();
            var resizeWidth = jQuery("#txtCustomWidth").val();
            var resizeHeight = jQuery("#txtCustomHeight").val();
            if (jQuery("#hdnResizeAsepctRatio").val() == "True") {
                var ratioX = parseInt(resizeWidth / 6) / parseInt(actWidth / 6);
                var ratioY = parseInt(resizeHeight / 6) / parseInt(actHeight / 6);
                var ratio = Math.max(ratioX, ratioY);
                var newWidth = parseInt(parseInt(resizeWidth / 6) * ratio);
                var newHeight = parseInt(parseInt(resizeHeight / 6) * ratio);
                newWidth = parseInt(resizeWidth / 6);
                newHeight = parseInt(resizeHeight / 6);
                jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                jQuery("#ImgResizePreview").css("max-width", "none");
            }
            else {
                var newWidth = parseInt(resizeWidth / 6);
                var newHeight = parseInt(resizeHeight / 6);
                jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
                jQuery("#ImgResizePreview").css("width", newWidth.toString() + "px");
                jQuery("#ImgResizePreview").css("height", newHeight.toString() + "px");
                jQuery("#ImgResizePreview").css("max-width", "none");
            }
            CheckImageFits(newHeight, newWidth);
        }
        function CheckImageFits(newHeight, newWidth) {
            var RequiredWidth = parseInt(jQuery("#divResizePreview").css("width"));
            var Requiredheight = parseInt(jQuery("#divResizePreview").css("height"));
            if ((RequiredWidth < newWidth) || (Requiredheight < newHeight)) {
                jQuery("#lblUnfitMsg").show();
                jQuery("#spnFinalCrop").show();
                jQuery("#btnCropWithResize").show();
                jQuery("#btnResizeImage").hide();
            }
            else {
                jQuery("#lblUnfitMsg").hide();
                jQuery("#spnFinalCrop").hide();
                jQuery("#btnResizeImage").show();
                jQuery("#btnCropWithResize").hide();
            }
            return false;
        }
        //Ready Function Close
        function Set_Cropper(setVal) {
            if (setVal == 1) {
                var reqsize = jQuery("#ddlSizeResize").val().split('x');
                var orientation = jQuery("#ddlOrientationResize").val();
                var cropWidth = 0;
                var cropHeight = 0;
                var CropSize = "";
                var cropHeight = 0;
                if (orientation == "Portrait") {
                    cropWidth = (parseInt(reqsize[0]) * 300) / 6;
                    cropHeight = (parseInt(reqsize[1]) * 300) / 6
                    CropSize = reqsize[0] + ' x ' + reqsize[1];
                }
                else {
                    cropWidth = (parseInt(reqsize[1]) * 300) / 6;
                    cropHeight = (parseInt(reqsize[0]) * 300) / 6
                    CropSize = reqsize[1] + ' x ' + reqsize[0];
                }
                jQuery("#maxHeight").val(cropHeight.toString());
                jQuery("#maxWidth").val(cropWidth.toString());
                var imagewidth = parseInt(jQuery("#txtCustomWidth").val());
                var imageHeight = parseInt(jQuery("#txtCustomHeight").val());
                jQuery("#ImgResizePreview").Jcrop({
                    onSelect: SelectCropArea,
                    onChange: SelectCropArea,
                    bgFade: true
                    
                }, function () {
                    jcrop_api = this;
                    jcrop_api.setOptions({ allowSelect: true });
                    jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
                    jcrop_api.animateTo([10, 20, ((imagewidth - cropWidth) / 2) + cropWidth, ((imageHeight - cropHeight) / 2) + cropHeight]);
                    // Use the API to get the real image size
                    jcrop_api.focus();
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                });
            }
            else {
                jcrop_api.destroy();
                jQuery("#ImgResizePreview").css("visibility", "visible");
            }
        }
        var boundx, boundy;
        function SelectCropArea(c) {
            // Grab some information about the preview pane            
            var preview = jQuery('#preview-pane');
            var pcnt = jQuery('#preview-pane .preview-container');
            var pimg = jQuery('#preview-pane .preview-container img');
            var xsize = pcnt.width(),
            ysize = pcnt.height();
            jQuery('#XAxis').val(parseInt(c.x));
            jQuery('#YAxis').val(parseInt(c.y));
            jQuery('#Width').val(parseInt(c.w));
            jQuery('#Height').val(parseInt(c.h));
            jQuery('#x1').val(c.x);
            jQuery('#y1').val(c.y);
            jQuery('#x2').val(c.x2);
            jQuery('#y2').val(c.y2);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
            if (parseInt(c.w) > 0) {
                var rx = xsize / c.w;
                var ry = ysize / c.h;
                pimg.css({
                    width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });
            }
        }
        function SaveChanges() {
            ResizeImageUsingByCrop();
            modal.style.display = "none";
            jcrop_api.destroy();
        }
        function SetReSizeValues(paramval) {
            jQuery("#ImgResizePreview").Jcrop({
                onSelect: SelectCropArea,
                onChange: SelectCropArea,
                bgFade: true

            }, function () {
                jcrop_api = this;
            });
            jcrop_api.destroy();
            jQuery("#ImgResizePreview").css("visibility", "visible");
            var AutoSize = "";
            var reqsize = "";
            var orientation = "";
            var ResizeWidth = 0;
            var ResizeHeight = 0;
            if (paramval == "S") {
                AutoSize = jQuery("#ddlSizeResize").val();
                reqsize = jQuery("#ddlSizeResize").val().split('x');
                orientation = jQuery("#ddlOrientationResize").val();
                if (orientation == "Portrait") {
                    ResizeWidth = parseInt(reqsize[0]) * 300;
                    ResizeHeight = parseInt(reqsize[1]) * 300;
                }
                else {
                    ResizeWidth = parseInt(reqsize[1]) * 300;
                    ResizeHeight = parseInt(reqsize[0]) * 300;
                }
            }
            else {
                if (orientation == "Portrait") {
                    ResizeWidth = jQuery("#txtCustomWidth").val();
                    ResizeHeight = jQuery("#txtCustomHeight").val();
                }
                else {
                    ResizeWidth = jQuery("#txtCustomHeight").val();
                    ResizeHeight = jQuery("#txtCustomWidth").val();
                }
            }
            if (orientation == "Portrait") {
                var previewHeight = jQuery("#divResizePreview").css("height");
                var previewwidth = jQuery("#divResizePreview").css("width");
                jQuery("#divResizePreview").css("height", previewwidth);
                jQuery("#divResizePreview").css("width", previewHeight);
                jQuery("#txtCustomHeight").val(ResizeHeight);
                  jQuery("#txtCustomWidth").val(ResizeWidth);
            }
            else {
                var previewHeight = jQuery("#divResizePreview").css("height");
                var previewwidth = jQuery("#divResizePreview").css("width");
                jQuery("#divResizePreview").css("height", previewwidth);
                jQuery("#divResizePreview").css("width", previewHeight);
                jQuery("#txtCustomHeight").val(ResizeHeight);
                jQuery("#txtCustomWidth").val(ResizeWidth);
            }
            setTimeout(function () { PreviewResizedImage(); }, 200);
            setTimeout(function () {
                if (jQuery("#chkFinalCrop").prop("checked") == true) {
                    Set_Cropper(1);
                }
            }, 400);
        }

        //Crop Image With Resize Image
        function CropImageWithResize() {
            debugger;
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/CropImageWithResize",
                contentType: 'application/json; charset=utf-8',
                data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'ImageHeight': jQuery("#txtCustomHeight").val(), 'ImageWidth': jQuery("#txtCustomWidth").val(), 'XAxis': document.getElementById('XAxis').value, 'YAxis': document.getElementById('YAxis').value, 'CropImageWidth': document.getElementById('Width').value, 'CropImageHeight': document.getElementById('Height').value, 'BlogId': jQuery('#hndBlogId').val(), 'userID': jQuery('#hdnCurUserID').val() },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.FileName == null) {
                        alert("Please Crop Image");
                    }
                    else {
                        location.reload();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        function ResizeImage() {
            debugger;
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/ResizeImage",
                contentType: 'application/json; charset=utf-8',
                data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'RatioStatus': document.getElementById('hdnResizeAsepctRatio').value, 'ImageHeight': document.getElementById('txtCustomHeight').value, 'ImageWidth': document.getElementById('txtCustomWidth').value, "pageID": jQuery("#hdnCurPageID").val(), "CMSID": jQuery("#hdnCurCMSID").val() },
                dataType: "json",
                success: function (data) {
                    if (data != null && data != undefined && data != "") {
                        location.reload();
                    }
                    else {
                        //Start Save Image Preview//
                        jQuery("#divFinalPreview").hide();
                        jQuery("#divCropFinalImage").hide();
                        jQuery("#imgGetHeightWidth").attr("src", "");
                        jQuery("#dZUpload").show();
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        // Custome Image Create
        function CustomeImage() {
            var ImageNameArray = jQuery("#ImgResizeImage").attr("src").split('/');
            var ImageName = ImageNameArray[ImageNameArray.length - 1];
            var Color = jQuery('#txtFontColor').val();
            var hex = parseInt(Color.substring(1), 16);
            var r = (hex & 0xff0000) >> 16;
            var g = (hex & 0x00ff00) >> 8;
            var b = hex & 0x0000ff;
            jQuery.ajax({
                type: "GET",
                url: "../WebService/DocCMSApi.svc/Custome_Image_Create",
                data: { 'FilePath': jQuery("#ImgResizeImage").attr("src"), 'R': r, 'G': g, 'B': b, 'BlogID': jQuery('#hndBlogId').val(), 'userID': jQuery('#hdnCurUserID').val() },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.FileName != null && data.FileName != undefined) {
                        location.reload();
                    }
                    else {
                        jQuery("#ImgCustomeImagePreview").attr("src", "");
                        jQuery("#ImgCustomeImagePreview").hide();
                        jQuery("#imgFinalPreview").attr("src", "");
                        jQuery("#EditImageOnModelPopup").attr("src", "");
                        jQuery("#divFinalPreview").hide();
                        jQuery("#divCropFinalImage").hide();
                        jQuery("#dZUpload").show();
                        modalResize.style.display = "block";
                        jQuery("#imgGetHeightWidth").attr("src", "");
                        jQuery("#btnPostImage").prop("disabled", true);
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        function SetTextImageValues(cntrl) {
            jQuery("#ImgResizePreview").Jcrop({
                onSelect: SelectCropArea,
                onChange: SelectCropArea,
                bgFade: true

            }, function () {
                jcrop_api = this;
            });
            jcrop_api.destroy();
            jQuery("#ImgResizePreview").attr("src", jQuery("#ImgResizeImage").attr("src"));
            jQuery("#ImgResizePreview").css('visibility', 'visible');
            switch (cntrl) {
                case "Left Picture":
                    jQuery("#txtCustomWidth").val("373");
                    jQuery("#txtCustomHeight").val("280");
                    CheckResizeValues();
                    break;
                case "Center Picture":
                    jQuery("#txtCustomWidth").val("373");
                    jQuery("#txtCustomHeight").val("280");
                    CheckResizeValues();
                    break;
                case "Right Picture":
                    jQuery("#txtCustomWidth").val("373");
                    jQuery("#txtCustomHeight").val("280");
                    CheckResizeValues();
                    break;
                case "Full Picture":
                    jQuery("#txtCustomWidth").val("1119");
                    jQuery("#txtCustomHeight").val("840");
                    CheckResizeValues();
                    break;
                default:
                    jQuery("#txtCustomWidth").val("1800");
                    jQuery("#txtCustomHeight").val("1200");
                    CheckResizeValues();
                    break;
            }
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Content Management</a> <span class="separator"></span></li>
        <li>Manage Content Management</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Content Management</h5>
            <h1>
                Content Management [
                <asp:Literal ID="LitpageTitle" runat="server" ClientIDMode="Static"></asp:Literal>
                ]</h1>
        </div>
    </div>
              <!---------------------Resize Section--------------------------->
                <div id="divModelResize" class="modalResizeEidt" style="height: auto% !important;max-width:auto% !important; " >
                  <div class="modalResizeEidt-content" style="height: auto !important;max-width:auto !important;" >
                   <span class="ResizeHeaderEdit">Resize Your Image to get a best fit result</span>
                    <span class="cross" onclick="CloseImageResizeModel();">×</span>
                     <div style="width:auto !important;">
                      <img src="" id="ImgResizeImage" style="width:auto;height:auto"/>
                      <div style="width: 100%;">
                         Resize Image Option  <input type="checkbox" id="chkResizeImageCreate"/>
                         Create Custom Image <input type="checkbox" id="chkCustomeImageCreate"/>
                         <div  id="divCustomResizeImageCreate">
                        <input type="checkbox" id="chkAspectRatioResize" checked="checked" />
                        Keep Aspect Ratio<br />
                        Image Width :<input type="text" id="txtCustomWidth" placeholder="Width in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('w',event);" style="width:18%"/>
                        Image Height :<input type="text" id="txtCustomHeight" placeholder="Height in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('h',event);" style="width:18%"/>
                     <div id="divResizeSection">
                        Select Image Size<select id="ddlSizeResize" style="width: 90px; height: 36px;" onchange="SetReSizeValues('S');">                            
                            <option value="4x6">4x6</option>
                        </select>
                        Select Orientation<select id="ddlOrientationResize" style="width: 150px; height: 36px;"  onchange="SetReSizeValues('O');">                            
                            <option value="Landscape">Landscape</option>
                            <option value="Portrait">Portrait</option>
                        </select>
                          Page Text Image<select id="ddlPageTextImage" style="width: 150px; height: 36px;"  onchange="SetTextImageValues(this.value);">                            
                            <option value="Left Picture">Left Picture</option>
                            <option value="Center Picture">Center Picture</option>
                             <option value="Right Picture">Right Picture</option>
                            <option value="Full Picture">Full Picture</option>
                        </select>
                    </div>
                    <input type="button" id="btnResizeImage" class="btn btn-danger btn-lg" value="Resize" onclick="ResizeImage();"
                      style="margin-top:10px; margin-bottom:10px;" />
                          <span id="spnFinalCrop" style="display:none;"><span style="color:Red;font-weight:400;">The previewed image exceeds the required dimensions.You need to crop the image to get the required dimensions.</span><br /><input type="checkbox" id="chkFinalCrop"/>Crop Image
                        <input type="button" class="btn btn-danger btn-lg" style="display:none;" id="btnCropWithResize" value="Crop Image" onclick="CropImageWithResize();" />
                        </span>
                 <input type="button" id="Button1" class="btn btn-danger btn-lg" value="Preview" onclick="PreviewResizedImage();"
                    style="margin-top:10px; margin-bottom:10px;display:none;"  />
                <asp:HiddenField ID="hdnResizeAsepctRatio" runat="server" ClientIDMode="Static" Value="True"/>
                 <div id="divResizePreviewPane" style="margin-left:20px; margin-bottom :20px;">
                <span ><b>Image Preview</b></span>
                <div class="preview-container" id="divResizePreview"  style="width:300px; height:200px;">
                    <img src="" id="ImgResizePreview" alt="Preview" />
                </div>
            </div>
            </div>
            </div>
             </div>
             <div id="divCustomImageCreate" style="display:none;">
             Select Background Image Color:
             <asp:TextBox ID="txtFontColor" runat="server" CssClass="input-large demo" ClientIDMode="Static" style=" width:300px; height:24px;margin-bottom: 0;" placeholder="Enter Font Color" onchange="ColorChange(this)"></asp:TextBox>
             <br />
               <input type="button" id="btncustomeImage" class="btn btn-danger btn-lg" value="Create Image" onclick="CustomeImage();"
                    style="margin-top:10px; margin-bottom:10px;"  />
                 <div id="divCustomImagePreviewRunning" style="width:300px; height:200px; background-color:#FFFFFF;margin-left:5%;margin-bottom:10px;">
                 <center>
                 <img src="" id="ImgCustomeImagePreviewRunning" />
                 </center>
                </div>
               </div>
             </div>
              <div id="Div4" runat="server" clientidmode="Static"></div>
        </div>
    <!---------------------Resize Section--------------------------->
   <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <div class="widget">
                <h4 class="widgettitle">
                    <asp:Literal ID="LitContentTitle" runat="server" ClientIDMode="Static"></asp:Literal>
                    <asp:HiddenField ID="hdnCMSID" runat="server" ClientIDMode="Static" />
                     Details
                </h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                        </div>
                    </div>
                    <div class="stdform">
                        <div id="divMainPage" class="mainPage" runat="server">
                            <div class="Controls" id="divMainControlContainer" style="height: 100%;">
                                <div id="divPageDesign" align="left" style="overflow-y: auto; overflow-x: hidden;
                                    height: 100%;">
                                    <div id="DivRequired" runat="server" clientidmode="Static" style="width: auto; height: auto;
                                        background-color: transparent; color: Red; font-size: 14px; font-weight: bold;
                                        margin: 10px; margin-left: 18px;" visible="false">
                                    </div>
                                    <!-- Main Content management Controls Start here -->
                                    <asp:Repeater runat="server" ID="RptrContentMgmt" OnItemDataBound="RptrContentMgmt_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblFieldType" runat="server" ClientIDMode="Static" Text='<%# Eval("FiledType") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblDorder" runat="server" ClientIDMode="Static" Text='<%# Eval("Dorder") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblRequired" runat="server" ClientIDMode="Static" Visible="false"></asp:Label>
                                            <asp:Label ID="lblDefaultText" runat="server" ClientIDMode="Static" Visible="false"></asp:Label>
                                            <!-- Title will display here-->
                                            <div class="par control-group" id="DivTitle" runat="server" clientidmode="Static"
                                                visible="false">
                                                <span runat="server" clientidmode="Static" id="LitTitle"></span>
                                            </div>
                                            <div class="par control-group" id="DivTextBox" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LitLabelText"></asp:Literal></label>
                                                <div class="controls">
                                                    <input type="text" name="txtContainer" id="txtContainer" runat="server" class="input-large"/><span id="spnReqTexBox" style="color: Red; font-size: 24px;
                                                            font-weight: bold; padding: 5px;" runat="server" clientidmode="Static" visible="false">*</span>
                                                    <input type="button" id="btnShowNavigationURL" class="btn btn-primary" value="..."
                                                       style="margin-top: -10px;" clientidmode="Static" onclick="return SearchText_btn();" runat="server" visible="false"/>
                                                    <asp:Button ID="btnShowDisplayOrders" CssClass="btn btn-primary" runat="server" Text="..."
                                                       Style="margin-top: -10px;" ClientIDMode="Static" OnClick="btnShowDisplayOrders_Click"  visible="false"/>
                                                </div>
                                            </div>
                                            <!--Start Text Editor--->
                                            <div class="par control-group" id="DivTextEditor" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LitTextEditor"></asp:Literal></label>
                                                <div class="controls">
                                                    <textarea id="elm1" name="elm1" rows="15" class="tinymce" validationgroup="grpsubmit" 
                                                        runat="server"   style="height: 140px; width: 100px; max-height: 200px; min-height: 200px; max-width: 200px;
                                                        min-width: 100px;" ></textarea> <span id="spnReqtextarea" style="color: Red; font-size: 24px; font-weight: bold;
                                                        padding: 5px;" runat="server" clientidmode="Static" visible="false">*</span>
                                                </div>
                                            </div>
                                            <!--End Text Editor--->
                                            <!--Start  CheckBoxes -->
                                            <div class="par control-group" id="divCheckBox" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="lblSingleChoice"></asp:Literal></label>
                                                <asp:CheckBoxList ID="chkList" runat="server" ClientIDMode="Static">
                                                </asp:CheckBoxList>
                                            </div>
                                            <!--end CheckBoxes-->
                                            <!-- Start of Radio Buttons -->
                                            <div class="par control-group" id="DivRadioButton" runat="server" clientidmode="Static"
                                                visible="false">
                                                <!--Radio Section -->
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LiRadioButton"></asp:Literal></label>
                                                <div class="clearfix" style="margin: 5px 0;">
                                                    <asp:RadioButtonList ID="rbtnList" runat="server" ClientIDMode="Static">
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <!--End of Radio Button-->
                                            <!--Start Drop Down List -->
                                            <div class="par control-group" id="DivDropDown" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LitDropdown"></asp:Literal></label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlSelection" runat="server" ClientIDMode="Static" style="width:49.8%;"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSelection_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <span id="spnddl" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;"
                                                        runat="server" clientidmode="Static" visible="false">*</span>
                                                </div>
                                            </div>
                                            <!--End Drop Down List -->
                                            <!--Start List box -->
                                            <div class="par control-group" id="DivListBox" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LitlstBox"></asp:Literal></label>
                                                <div class="controls">
                                                    <asp:ListBox ID="LstSelection" runat="server" style="width:49.8%;"></asp:ListBox>
                                                    <span id="spnlst" style="color: Red; font-size: 24px; font-weight: bold; padding: 5px;"
                                                        runat="server" clientidmode="Static" visible="false">*</span>
                                                </div>
                                            </div>
                                            <!--End List box -->
                                            <!--Start Multiline --->
                                            <div class="par control-group" id="DivMultiLine" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LitMultiLine"></asp:Literal></label>
                                                <div class="controls">
                                                    <textarea id="txtMultiLine" name="txtMultiLine" rows="15" clientidmode="Static" validationgroup="grpsubmit"
                                                        style="height: 140px; width: 771px; max-height: 140px; min-height: 140px; max-width: 771px;
                                                        min-width: 771px;" runat="server"></textarea>
                                                    <span id="spnReqtextMulti" style="color: Red; font-size: 24px; font-weight: bold;
                                                        padding: 5px;" runat="server" clientidmode="Static" visible="false">*</span>
                                                </div>
                                            </div>
                                            <!--End Multiline --->
                                            <div class="par control-group" id="DivUpload" runat="server" clientidmode="Static"
                                                visible="false">
                                                <label class="control-label">
                                                    <asp:Literal runat="server" ClientIDMode="Static" ID="LitUploadLabel"></asp:Literal>
                                                </label>
                                                <div class="controls">
                                                    <asp:FileUpload ID="CMSFileUploader" runat="server" onchange="Show_UploadedImage(this)" CssClass="input-upload" />
                                                    <div style="margin-left: 36%; margin-top: -2%; width: 80px;">
                                                        <img alt="" src="../UploadedFiles/No_image_available.png" runat="server" id="ImgCMSFileUploader" style="width: 80px;height:50px; margin-top:-20px;" />
                                                    </div>
                                                      <div style="margin-left: 40%;  width: 80px;">
                                                    <img src="../images/Edit-image.png" style="width:24px;float:right;" title="Edit Image" onclick="EditImage(this.id);" runat="server" id="imgEdit" />
                                                    </div>
                                                     <asp:Label runat="server"  ID="lblCMSFileUploader" ClientIDMode="Static" style="display:none;"></asp:Label>
                                                </div>
                                             </div>
                                            <!-- SectionBreak will display here-->
                                            <div class="par control-group" id="DivSectionBreak" runat="server" clientidmode="Static"
                                                align="center" visible="false">
                                                <div id="DivSectionBreakData" style="width: 93.5%; height: 2px; background-color: White;"
                                                    runat="server" align="center">
                                                </div>
                                            </div>
                                            <!-- DivEmptySpace will display here-->
                                            <div class="par control-group" id="DivEmptySpace" runat="server" clientidmode="Static"
                                                align="center" visible="false">
                                                <div id="DivEmptySpaceData" style="width: 90%; background-color: transparent;" runat="server"
                                                    align="center">
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <!-- Main Content management Controls will end here -->
                                     <div class="par control-group" id="DivSize" runat="server" clientidmode="Static" visible="false">
                                        <label class="control-label">
                                            Size:
                                        </label>
                                        <div class="controls">
                                            <asp:CheckBox ID="CheckSize" runat="server" ClientIDMode="Static" onclick="CheckSizeFunctionImage(this)" />
                                        </div>
                                        <div class="controls" runat="server" id="DivButtonSize" >&nbsp;<input type="button" id="btnSize" value="Add Size" runat="server" clientidmode="Static"  style="margin-left:100px; display:none;" onclick="OpenSizeModelImage();" /></div>
                                    </div>
                                     <div class="par control-group" id="DivColor" runat="server" clientidmode="Static" visible="false">
                                        <label class="control-label">
                                            Color:
                                        </label>
                                        <div class="controls">
                                            <asp:CheckBox ID="CheckColor" runat="server" ClientIDMode="Static" onclick="CheckColorFunctionImage(this)" />
                                        </div>
                                        <div class="controls" id="DivButtonColor" >&nbsp;<input type="button" value="Add Color" id="btnColor" style="margin-left:100px; display:none;" onclick="OpenColorModelImage();" runat="server" clientidmode="Static"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnDropDownListVal" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnListBoxVal" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnCurDivID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="XAxis" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="YAxis" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="Width" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="Height" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="maxWidth" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="maxHeight" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="HiddenField2" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDisplayOrder" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnDropDownListValLevel1" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="HiddenField3" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="HiddenField4" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hndImageFileName" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnActualImageWidth" runat="server" ClientIDMode="Static"/>
                        <asp:HiddenField ID="hdnActualImageHeight" runat="server" ClientIDMode="Static"/>
                        <br />
                         <p class="stdformbutton" style="width: 100%; margin-left: 80px;margin-top:30px;">
                            <asp:Button class="btn btn-primary" id="btncontinue" style="width: 175px" runat="server" OnClick="btncontinue_click" ClientIDMode="Static" Text="Update Draft & Continue" />
                            <asp:Button class="btn" style="width: 175px; margin-left: 20px;" id="btnupdatedraft" runat="server" OnClick="btnupdatedraft_click" ClientIDMode="Static" Text="Update Draft" />
                            <asp:Button class="btn" style="width: 300px; margin-left: 20px;" id="btnremovedraft" ClientIDMode="Static" runat="server" OnClick="btnremovedraft_click" Text="Don't Save Changes & Delete Draft"/>
                            <asp:Button class="btn" style="width: 175px; margin-left: 20px;margin-right: 20px; height: 32px; background-color: Maroon; color: #ffffff;" 
                              id="btnpublishdraft" runat="server" OnClick="btnpublishdraft_click" ClientIDMode="Static" Text="Publish Draft Copy" />
                            <asp:Button class="btn" style="width: 175px; margin-left: 20px;" Visible="false" id="btnSendForApproval" runat="server" OnClick="btnSendForApproval_click" ClientIDMode="Static" Text="Send For Approval" />
                            <asp:Button class="btn" style="width: 175px; margin-left: 20px;" id="btncancel" runat="server" OnClick="btnCancel_Click" Text="Cancel"/>
                            <asp:Button ID="Upload" runat="server" Text="Upload" ClientIDMode="Static" Visible="false"
                                onclick="Upload_Click" />
                        </p>
                    </div>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->
       <uc1:CadminFooter ID="CadminFooter1" runat="server" />
      <!------------Modelpopup for Size---------------->
      <div id="DivSizeModel" class="modal fade in" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                    <h4 id="UpdateSize" class="modal-title" runat="server">
                    Add Size
                    </h4>
                </div>
                <div class="modal-body" style="height: 640px;">
                    <table id="tblSize" class="table table-bordered responsive" style="width: 95%;">
                        <colgroup>
                            <col class="con1" style="align: left; width: 70%;" />
                            <col class="con0" style="align: right; width: 30%;" />
                        </colgroup>
                        <thead>
                            <tr style="width: 100%;">
                                <th>
                                   Size
                                </th>
                                <th>
                                   Remove
                                </th>
                            </tr>
                        </thead>
                        <tbody style="overflow-y: auto;" id="BindSize">
                        </tbody>
                    </table>
                    <table>
                   <tr>
                    <td>Size</td>
                    <td> <input type="text" id="txtsize"/></td>
                    </tr>
                     <tr>
                    <td><input type="button" id="btnSubmitSize" class="btn btn-primary" value="Add Size" onclick="InsertSizeImage();" /></td>
                   </tr>
                    </table>
               </div>
                <div class="modal-footer">
                  <input type="button" id="btnCancelSizeModel" class="btn btn-primary" style="margin-left:88% !important;" onclick="CancelSizeModelImage();"
                      value="Cancel" />
                </div>
                <!--maincontentinner-->
                <ul id="ul1" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
                    role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
                    top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
                </ul>
            </div>
            </div>
           </div>
    <!------------Modelpopup for Size---------------->
    <!------------Modelpopup for color---------------->
     <input type="hidden" id="hdnColorId" />
      <div id="DivColorModel" class="modal fade in" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                  <h4 id="H1" class="modal-title" runat="server">
                    Add Color
                    </h4>
                </div>
                <div class="modal-body" style="height: 640px;">
                    <table id="TblColor" class="table table-bordered responsive" style="width: 95%;">
                        <colgroup>
                            <col class="con1" style="align: left; width: 70%;" />
                            <col class="con0" style="align: right; width: 30%;" />
                        </colgroup>
                        <thead>
                            <tr style="width: 100%;">
                                <th>
                                   Color
                                </th>
                                <th>
                                   Image
                                </th>
                                <th>
                                   Functions
                                </th>
                            </tr>
                        </thead>
                        <tbody style="overflow-y: auto;" id="TbodyColor">
                        </tbody>
                    </table>
                    <table>
                    <tr>
                    <td>Color</td>
                     <td><input type="text" id="txtcolor"/></td>
                    </tr>
                    <tr>  
                    <td>Image</td>
                     <td><asp:FileUpload ID="UploadImage" runat="server" ClientIDMode="Static"  onchange="ShowPreviewImage(this)" /></td> <td><img src="../DFI/images/NoImage.png" id="imgPrieview" style="width:100px; height:50px;" /> </td>
                     </tr>
                     <tr>
                     <td><input type="button" value="Submit" class="btn btn-primary" id="btnSubmitColor" onclick="InsertColorImage();" />
                     <input type="button" value="Update" class="btn btn-primary" id="btnColorUpdate" onclick="UploadColorImage();" style="display:none;" />
                     </td>
                     </tr>
                    </table>
                </div>
                <div class="modal-footer">
                 <input type="button" id="btnColorCancel" class="btn btn-primary" style="margin-left:88% !important;" onclick="CancelColorModelImage();"
                         value="Cancel" />
                </div>
                <!--maincontentinner-->
                <ul id="ul2" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
                    role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
                    top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
                </ul>
            </div>
            </div>
           </div>
                <!------------Modelpopup for Color---------------->
                <!-- for Display Order Popup -->
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title" runat="server">
                          </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 50%;" />
                                    <col class="con0" style="align: right; width: 20%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                           Name
                                        </th>
                                        <th id="ActiveStatusheading" runat="server">
                                        Status
                                        </th>
                                        <th>
                                            Display&nbsp;Order
                                            <img id="img1" src="../images/table_icon_1.gif" alt="Edit" style="float: right;"
                                                onclick="ToggleDisplay();" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrDisplayOrder"  OnItemDataBound="RptrDisplayOrder_databound">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                 <td>
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblContentName" runat="server" Text='<%# Eval("ContentName") %>'></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                   <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                                       Visible="false"></asp:Label>
                                                   <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                       CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        clientidmode="Static" class="DorderText" style="display: none; width: 100px;
                                                        float: left; margin-right: 25px;" value='<%# Eval("DisplayOrder") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                 <td>
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblContentName" runat="server" Text='<%# Eval("ContentName") %>'></asp:Label>
                                                </td>
                                                <td id="Activestatusdata" runat="server">
                                                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>'
                                                       Visible="false"></asp:Label>
                                                    <asp:Label runat="server" ID="lblActive" class="Status" Text='<%# Eval("IsActive")%>'
                                                       CommandName="EditPageContent"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDorder" runat="server" Text='<%# Eval("DisplayOrder") %>' CssClass="DorderLabel"></asp:Label>
                                                    <input type="text" runat="server" name="txtDorder" id="txtDorder" onkeypress="return onlyNumbers(event)" maxlength="5"
                                                        clientidmode="Static" class="DorderText" style="display: none; width: 100px;
                                                        float: left; margin-right: 25px;" value='<%# Eval("DisplayOrder") %>' />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="btnUpdate" class="btn btn-primary" onclick="ValidateDisplayOrders();" Style="display: none;"
                                value="Update Display Orders" />
                            <a href="" id="btnCloseDisplayOrder" onclick="return false;" class="btn">Cancel</a><span
                                style="display: none;"></span>
                           <asp:Button ID="btnUpdateDisplayOrder" runat="server" ClientIDMode="Static" Style="display: none;"
                OnClick="btnUpdateDisplayOrder_Click" />
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
        </div>
        <!--maincontentinner-->
        <ul id="ulAutocomplete" class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"
            role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1;
            top: 647px; left: 514px; display: none; width: 772px; height: 150px; overflow-y: scroll;">
        </ul>
    </div>
    <!--maincontent-->
     <!--maincontent-->
    <script language="javascript" type="text/javascript">
           function ValidateDisplayOrders() {
               var btnUpdateDisplayOrder = document.getElementById("btnUpdateDisplayOrder");
               btnUpdateDisplayOrder.click();
           }
           function SuccessMsg(PageID, CMSID) {
               alert("The provided display orders are successfully Updated");
               window.location = "../cadmin/ContentManagement.aspx?PID=" + PageID + "&CMSID=" + CMSID;
           }
           function SuccessMsg1(PageID) {
               alert("The provided display orders are successfully Updated");
               window.location = "../cadmin/ContentManagement.aspx?PID=" + PageID;
           }
           function ToggleDisplay(ItemIndex) {
               jQuery(".DorderLabel").toggle();
               jQuery(".DorderText").toggle();
               jQuery("#btnUpdate").show();
               jQuery("#img1").hide();
           }

           //Start Image Preview Function
           function ShowPreview(fileInput) {
               var fileinputidval = fileInput.id;
               var thumbnilid = "";
               var Get_FileinputID = fileinputidval.toString().split('_');
               if (Get_FileinputID.length > 3) {
                   thumbnilid = Get_FileinputID[0] + "_" + Get_FileinputID[1] + "_ImgCMSFileUploader_" + Get_FileinputID[3];
               }
               var files = fileInput.files;
               for (var i = 0; i < files.length; i++) {
                   var file = files[i];
                   var ext = file.name.split('.').pop().toLowerCase();
                   var imageType = /image.*/;
                   var filetype = file.type
                   var img = document.getElementById(thumbnilid.toString());
                   img.file = file;
                   var reader = new FileReader();
                   if (file.type.match(imageType)) {
                       reader.onload = (function (aImg) {
                           return function (e) {
                               aImg.src = e.target.result;
                           };
                       })(img);
                       reader.readAsDataURL(file);
                   }
                   else if (ext == "pdf") {
                       reader.onload = (function (aImg) {
                           return function (e) {
                               aImg.src = '../UploadedFiles/pdf.png';
                           };
                       })(img);
                       reader.readAsDataURL(file);
                   }
                   else if (ext == "doc" || ext == "txt") {
                       reader.onload = (function (aImg) {
                           return function (e) {
                               aImg.src = '../UploadedFiles/doc.png';
                           };
                       })(img);
                       reader.readAsDataURL(file);
                   }
                   else if (ext == "xls" || ext == "cvc") {
                       reader.onload = (function (aImg) {
                           return function (e) {
                               aImg.src = '../UploadedFiles/xls.png';
                           };
                       })(img);
                       reader.readAsDataURL(file);
                   }
               }
           }

           function EditImage(ctrl) {
               var ctrlArray = ctrl.split('_');
               OpenModelPopupCropAndResize(ctrlArray[ctrlArray.length - 1]);
               return false;
           }
           //Open Model Popup Crop and Resize
           function OpenModelPopupCropAndResize(prefix) {
               var FilePath = jQuery("#ContentPlaceHolder1_RptrContentMgmt_ImgCMSFileUploader_" + prefix).attr("src");
               var modalResize = document.getElementById('divModelResize');
               modalResize.style.display = "block";
               jQuery.ajax({
                   url: "../WebService/DocCMSApi.svc/ProcessFiles",
                   contentType: 'application/json; charset=utf-8',
                   data: { "FilePath": FilePath, "pageID": jQuery("#hdnCurPageID").val(), "CMSID": jQuery("#hdnCurCMSID").val() },
                   dataType: "json",
                   success: ajaxSucceeded,
                   error: ajaxFailed
               });
               function ajaxSucceeded(data) {
                   if (data != null) {
                       setTimeout(function () { ResizeImagePopup(data); }, 200);
                   }
               }
               function ajaxFailed() {
                   alert("There is an error during operation.");
               }
           }

           function ResizeImagePopup(fileName) {
               var jcrop_api;
               jQuery("#ImgResizePreview").attr("src", fileName);
               jQuery("#ImgResizeImage").attr("src", fileName);
               jQuery("#ImgCustomeImagePreviewRunning").attr("src", fileName);
               setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
               CheckResizeValues();
               return false;
           }

           function Get_Actual_Uploaded_Image_Size() {
               var myImg = document.querySelector("#ImgResizeImage");
               var realWidth = myImg.naturalWidth;
               var realHeight = myImg.naturalHeight;
               jQuery("#hdnActualImageWidth").val(realWidth);
               jQuery("#hdnActualImageHeight").val(realHeight);
           }

           var boundx, boundy;
           function SelectCropArea(c) {
               // Grab some information about the preview pane            
               var preview = jQuery('#preview-pane');
               var pcnt = jQuery('#preview-pane .preview-container');
               var pimg = jQuery('#preview-pane .preview-container img');
               var xsize = pcnt.width(),
               ysize = pcnt.height();
               jQuery('#XAxis').val(parseInt(c.x));
               jQuery('#YAxis').val(parseInt(c.y));
               jQuery('#Width').val(parseInt(c.w));
               jQuery('#Height').val(parseInt(c.h));
               jQuery('#x1').val(c.x);
               jQuery('#y1').val(c.y);
               jQuery('#x2').val(c.x2);
               jQuery('#y2').val(c.y2);
               jQuery('#w').val(c.w);
               jQuery('#h').val(c.h);
               if (parseInt(c.w) > 0) {
                   var rx = xsize / c.w;
                   var ry = ysize / c.h;
                   pimg.css({
                       width: Math.round(rx * boundx) + 'px',
                       height: Math.round(ry * boundy) + 'px',
                       marginLeft: '-' + Math.round(rx * c.x) + 'px',
                       marginTop: '-' + Math.round(ry * c.y) + 'px'
                   });
               }
           }

           function CloseImageResizeModel() {
               jQuery("#ImgResizeImage").attr("src", "");
               var modalResize = document.getElementById('divModelResize');
               modalResize.style.display = "none";
           }
    </script>
  <link href="../css/cropfunctionality.css" rel="stylesheet" type="text/css" />
   <asp:HiddenField ID="hdnCurCMSID" runat="server" ClientIDMode="Static"/>
     <asp:HiddenField ID="hdnCurPageID" runat="server" ClientIDMode="Static" />
</asp:Content>
