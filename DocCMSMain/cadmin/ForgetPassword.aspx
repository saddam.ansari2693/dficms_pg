﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs"
    Inherits="DocCMSMain.cadmin.ForgetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DOCFOCUS-Admin panel</title>
    <link rel="shortcut icon" href="../images/ico/docfocus.ico" />
    <link rel="stylesheet" href="../css/style.default.css" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript" src="../js/modernizr.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery.cookie.js"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
    <script type="text/javascript">
        function Validate_Data() {
            var result = clientvalidation();
            return result;
        }

        function SuccessMsg() {
            alert("Your password has been sent successfully");
            window.location = "../cadmin/Default.aspx";
        }

        function FireMessage(Status) {
            if (Status == 1) {
                alert("Your password has been sent successfully");
                window.location = "../cadmin/Default.aspx";
            }
            else {
                alert("Unable to send email, message: 'An error occurred while sending mail");
                window.location = "../cadmin/ForgetPassword.aspx";
            }
        }

        function clientvalidation() {
            var username = jQuery('#username').val();
            var rcode = jQuery('#registrationcode').val();
            if (username == '') {
                jQuery('#labmsg').html("Email Id is required.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (jQuery('#password').val() == '') {
                jQuery('#labmsg').html("Password is required.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (jQuery('#password').val().length < 7) {
                jQuery('#labmsg').html("Password must be 7 characters long.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (jQuery('#confirmpassword').val() == '') {
                jQuery('#labmsg').html("Confirm Password is required.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (jQuery('#password').val() != jQuery('#confirmpassword').val()) {
                jQuery('#labmsg').html("The Password must be match.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (jQuery('#firstname').val() == '') {
                jQuery('#labmsg').html("First Name is required.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (jQuery('#lastname').val() == '') {
                jQuery('#labmsg').html("Last Name is required.");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            else if (rcode != 'Code247') {
                jQuery('#labmsg').html("Invalid Registration Code!");
                jQuery('.login-alert').fadeIn();
                return false;
            }
            return true;
        }

        function Displaylabel() {
            jQuery('.login-alert').fadeIn();
            return false;
        }
   </script>
</head>
<body class="loginpage" style="background: #ddebf9 none repeat scroll 0 0;">
    <div class="regpanel">
        <div class="regpanelinner">
            <div class="pageheader">
                <div class="pageicon" style="border-color: #0c57a3; color: #0c57a3;">
                    <span class="iconfa-hand-down"></span>
                </div>
                <div class="pagetitle">
                    <h5 style="color: #0c57a3;">
                        Reset Password</h5>
                    <h1 style="width: 800px; color: #000;">
                        Confirm Your Identity</h1>
                </div>
            </div>
            <div class="regcontent">
                <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                    <div class="alert alert-error" style="height: 25px;" align="center">
                        <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                        </label>
                    </div>
                </div>
                <form action="" id="registrationform" runat="server" class="stdform" autocomplete="off">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <h3 style="color: #000; font-size: 11px;">
                    Please enter your email id in the text field below, and we will send your password
                    on the provided email if it is valid.</h3>
                <h3 class="subtitle" style="color: #0c57a3;">
                    Email ID / Username</h3>
                <p>
                    <input type="text" name="txtusername" id="txtusername" runat="server" class="input-block-level"
                        placeholder="enter your Email ID" required />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Please enter valid email Id"
                        ControlToValidate="txtusername" ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </p>
                <asp:Button runat="server" ID="btnemailsubmit" OnClick="BtnemailContinue_Click" CssClass="btn btn-primary"
                    Text="Continue" Width="380" Height="40" BackColor="#0972DD" Style="border: 1px solid #0C57A3;
                    color: #FFFFFF; display: block;" Visible="true" />
                <p>
                    <div class="inputwrapper animate4 bounceIn">
                        <div style="color: #000;">
                            Already a member? <a href="default.aspx" style="color: #0c57a3;">Sign in</a></div>
                    </div>
                </p>
                </form>
            </div>
            <!--regcontent-->
        </div>
        <!--regpanelinner-->
    </div>
    <!--regpanel-->
    <br />
    <div class="regfooter">
        <p style="color: #000;">
            &copy; DOCFOCUS INC.
            <asp:Label ID="labyear" runat="server" Text="" />. All Rights Reserved.</p>
    </div>
</body>
</html>
