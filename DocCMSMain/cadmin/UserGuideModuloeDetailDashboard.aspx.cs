﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideModuloeDetailDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {

                if (Request.QueryString["MID"] != null)
                {
                    BindUserGuideDetail(Convert.ToInt32(Request.QueryString["MID"]));
                    hdnmodulecontentId.Value=Convert.ToString(Request.QueryString["MID"]);
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                }
            }
        }

        // Bind user guide moduledetail
        protected void BindUserGuideDetail(Int32 MainContentID)
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_ModuleDetailContent_Byid(MainContentID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrUserGuideDetail.DataSource = dt;
                    RptrUserGuideDetail.DataBind();
                }
                else
                {
                    RptrUserGuideDetail.DataSource = "";
                    RptrUserGuideDetail.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnAddUserGuideModuleDetailBottom_Click(object sender, EventArgs e)
        {
           Response.Redirect("~/cadmin/UserGuideModuleDetail.aspx?MID=" + Convert.ToInt32(hdnmodulecontentId.Value));
        }

        protected void RptrUserGuideDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                Label lblMuduleContentID = (Label)e.Item.FindControl("lblMuduleContentID");

                switch (e.CommandName)
                {
                    case "EditModuleDetail":
                        Response.Redirect("~/cadmin/UserGuideModuleDetail.aspx?ID=" + e.CommandArgument.ToString() + "&MID=" + Convert.ToInt32(Request.QueryString["MID"]));
                        break;
                    case "AddUserGuideActionDescription":
                        Response.Redirect("~/cadmin/UserGuideActionDescriptionDashboard.aspx?MDID=" + e.CommandArgument.ToString() + "&MID=" + Convert.ToInt32(Request.QueryString["MID"]));
                        break;
                    case "DeleteModuleDetail":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_ModuleDetailContent(Convert.ToInt32(e.CommandArgument));
                        BindUserGuideDetail(Convert.ToInt32(Request.QueryString["MID"]));
                        Response.Redirect("~/cadmin/UserGuideModuloeDetailDashboard.aspx?MID=" + Convert.ToInt32(lblMuduleContentID.Text));
                        break;
                    case "OrderUp":
                        if (lblMuduleContentID != null && lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_ModuleDetailContent(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblMuduleContentID.Text), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        BindUserGuideDetail(Convert.ToInt32(Request.QueryString["MID"]));
                        break;
                    case "OrderDown":
                        if (lblMuduleContentID != null && lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_ModuleDetailContent(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblMuduleContentID.Text), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        BindUserGuideDetail(Convert.ToInt32(Request.QueryString["MID"]));
                        break;
                    default:
                        break;
                }
            }
        }

        protected void RptrUserGuideDetail_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideModuleDashboard.aspx");
        }
    }
}