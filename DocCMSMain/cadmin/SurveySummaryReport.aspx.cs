﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace DocCMSMain.cadmin
{
    public partial class SurveySummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["PWID"] != null)
                {
                    FillDDL(Convert.ToString(Request.QueryString["PWID"]));
                    if (ddlYear.Items.Contains(new ListItem(Convert.ToString(System.DateTime.Now.Year))))
                    {
                        ddlYear.SelectedValue = Convert.ToString(System.DateTime.Now.Year);
                        ddlMonth.SelectedValue = Convert.ToString(DateTime.Now.Month);
                        btnShowReport.Enabled = true;

                    }

                    using (DataTable dtSurveyDetail = ServicesFactory.DocCMSServices.Fetch_Survey_Detail_By_id(Convert.ToString(Request.QueryString["PWID"])))
                    {
                        if (dtSurveyDetail != null && dtSurveyDetail.Rows.Count > 0)
                        {
                            LitSurveyName.Text = Convert.ToString(dtSurveyDetail.Rows[0]["SurveyName"]);
                            LitSurveyDescription.Text = Convert.ToString(dtSurveyDetail.Rows[0]["Description"]);
                        }
                    }

                }
            }
        }

        protected void FillDDL(string surveyID)
        {
            if (!string.IsNullOrEmpty(surveyID))
            {
                DataTable dtYear = ServicesFactory.DocCMSServices.Fetch_Survey_Response_Year(surveyID);
                if (dtYear != null && dtYear.Rows.Count > 0)
                {
                    ddlYear.DataSource = dtYear;
                    ddlYear.DataValueField = "ResponseYearID";
                    ddlYear.DataTextField = "ReponseYear";
                    ddlYear.DataBind();
                }

                DataTable dtMonth = ServicesFactory.DocCMSServices.Fetch_Survey_Response_Month(surveyID);
                if (dtMonth != null && dtMonth.Rows.Count > 0)
                {
                    ddlMonth.DataSource = dtMonth;
                    ddlMonth.DataValueField = "ReponseMonthID";
                    ddlMonth.DataTextField = "ResponseMonth";
                    ddlMonth.DataBind();
                }
            }
        }



        protected void btnShowReport_Click(object sender, EventArgs e)
        {
            if (ddlYear.SelectedIndex <= 0 || ddlMonth.SelectedIndex <= 0)
            {
                lblMessage.Visible = true;
                btnExport.Visible = false;
                btnPrint.Visible = false;
                return;
            }
            lblMessage.Visible = false;
            btnExport.Visible = true;
            btnPrint.Visible = true;
            LoadReport(Convert.ToString(Request.QueryString["PWID"]), Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue));
        }

        // ////////////////////////
        protected void btnExportReport_Click(object sender, EventArgs e)
        {
            if (ddlYear.SelectedIndex <= 0 || ddlMonth.SelectedIndex <= 0)
            {
                lblMessage.Visible = true;
                return;
            }
            lblMessage.Visible = false;

            DataTable dt = null;
            try
            {
                dt = CreateDataTable();
                DataRow drow = null;
                drow = dt.NewRow();
                drow["SurveyResultTitle"] = "Survey Name";
                drow["ReponsesData"] = LitSurveyName.Text;
                drow["ReponsesDetails"] = "";
                drow["TotalReponses"] = "";
                dt.Rows.Add(drow);

                drow = dt.NewRow();
                drow["SurveyResultTitle"] = "Survey Description";
                drow["ReponsesData"] = LitSurveyDescription.Text;
                drow["ReponsesDetails"] = "";
                drow["TotalReponses"] = "";
                dt.Rows.Add(drow);

                drow = dt.NewRow();
                drow["SurveyResultTitle"] = "Included Response Periods";
                DateTimeFormatInfo mfi = new DateTimeFormatInfo();
                string strResponsePeriod = mfi.GetMonthName(Convert.ToInt32(ddlMonth.SelectedValue)).ToString();
                strResponsePeriod = strResponsePeriod + "--" + ddlYear.SelectedValue.ToString();
                drow["ReponsesData"] = strResponsePeriod;
                drow["ReponsesDetails"] = "";
                drow["TotalReponses"] = "";
                dt.Rows.Add(drow);

                drow = dt.NewRow();
                drow["SurveyResultTitle"] = "Compare Results By";
                drow["ReponsesData"] = "Response Periods";
                drow["ReponsesDetails"] = "";
                drow["TotalReponses"] = "";
                dt.Rows.Add(drow);

                drow = dt.NewRow();
                drow["SurveyResultTitle"] = "Total Survey Recipients";
                drow["ReponsesData"] = LitTotalResponse.Text.Trim().Replace("Total Survey Recipients", "");
                drow["ReponsesDetails"] = "";
                drow["TotalReponses"] = "";
                dt.Rows.Add(drow);



                foreach (RepeaterItem item in repeateryearlysummary.Items)
                {

                    drow = dt.NewRow();

                    drow["SurveyResultTitle"] = "";
                    drow["ReponsesData"] = (HiddenField)FindControl("hdnResponded");
                    drow["ReponsesDetails"] = (HiddenField)FindControl("hdnNonResponded");
                    drow["TotalReponses"] = (HiddenField)FindControl("hdnTotal");
                    dt.Rows.Add(drow);

                    DataTable dtQuestions = ServicesFactory.DocCMSServices.Fetch_Survey_questions(Convert.ToString(Request.QueryString["PWID"]));
                    Label lblSurveyID = (Label)FindControl("lblSurveyID");
                    Label lblQuestionID = (Label)FindControl("lblQuestionID");
                    Label lblQuestionType = (Label)FindControl("lblQuestionType");
                    Label lblFieldType = (Label)FindControl("lblFieldType");
                    DataTable dtResponse = null;
                    if (dtQuestions != null && dtQuestions.Rows.Count > 0)
                    {
                        string QuestionText = "";
                        string AnswerList = "";
                        string TextFieldHeading1 = "";
                        foreach (DataRow drQuestions in dtQuestions.Rows)
                        {
                            TextFieldHeading1 = Convert.ToString(drQuestions["Question"]);

                            drow = dt.NewRow();
                            drow["SurveyResultTitle"] = "";
                            drow["ReponsesData"] = "";
                            drow["ReponsesDetails"] = "";
                            drow["TotalReponses"] = "";
                            switch (Convert.ToString(drQuestions["AnswerType"]).ToUpper())
                            {
                                case "TEXT":
                                    AnswerList = "Type:&nbsp;Text&nbsp;&nbsp;-&nbsp;Descriptive";
                                    QuestionText = "";
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_questions_Response(Convert.ToString(Request.QueryString["PWID"]), "Text", lblQuestionID.Text, TextFieldHeading1, ddlYear.SelectedValue.Trim(), ddlMonth.SelectedValue.Trim());

                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dtResponse.Rows.Count; i++)
                                        {
                                            drow = dt.NewRow();
                                            if (i == 0)
                                            {
                                                drow["SurveyResultTitle"] = TextFieldHeading1;
                                            }
                                            if (dtResponse.Rows[i]["AnswerText"].ToString() == "")
                                            {
                                                drow["ReponsesData"] = "Respondent";
                                            }
                                            else
                                            {
                                                drow["ReponsesData"] = dtResponse.Rows[i]["AnswerText"];
                                            }
                                            drow["ReponsesDetails"] = dtResponse.Rows[i]["Percentage"];
                                            drow["TotalReponses"] = dtResponse.Rows[i]["ResponseCount"];
                                            dt.Rows.Add(drow);
                                        }

                                    }

                                    break;
                                case "OPTION":
                                    AnswerList = "Type:&nbsp;Single&nbsp;Choice";
                                    drow["ReponsesDetails"] = "";
                                    drow["TotalReponses"] = "";
                                    dt.Rows.Add(drow);
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_questions_Response(Convert.ToString(Request.QueryString["PWID"]), "Option", Convert.ToString(drQuestions["SurveyQuestionID"]), "MultipleChoice", ddlYear.SelectedValue.Trim(), ddlMonth.SelectedValue.Trim());
                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dtResponse.Rows.Count; i++)
                                        {
                                            drow = dt.NewRow();
                                            if (i == 0)
                                            {
                                                drow["SurveyResultTitle"] = TextFieldHeading1;
                                            }
                                            drow["ReponsesData"] = dtResponse.Rows[i]["AnswerText"].ToString();
                                            drow["ReponsesDetails"] = dtResponse.Rows[i]["Percentage"].ToString();
                                            drow["TotalReponses"] = dtResponse.Rows[i]["ResponseCount"].ToString();
                                            dt.Rows.Add(drow);
                                        }
                                    }
                                    break;
                                case "CHECK":
                                    AnswerList = "Type:&nbsp;Multiple&nbsp;Choice";
                                    AnswerList = AnswerList.Replace("~", ",");
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_questions_Response(Convert.ToString(Request.QueryString["PWID"]), "Check", Convert.ToString(drQuestions["SurveyQuestionID"]), "Checkbox", ddlYear.SelectedValue.Trim(), ddlMonth.SelectedValue.Trim());
                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtResponse.Rows.Count; i++)
                                        {
                                            drow = dt.NewRow();
                                            if (i == 0)
                                            {
                                                drow["SurveyResultTitle"] = TextFieldHeading1;
                                            }
                                            drow["ReponsesData"] = dtResponse.Rows[i]["AnswerText"].ToString();
                                            drow["ReponsesDetails"] = dtResponse.Rows[i]["Percentage"];
                                            drow["TotalReponses"] = dtResponse.Rows[i]["ResponseCount"];
                                            dt.Rows.Add(drow);
                                        }
                                    }
                                    break;
                                case "DROPDOWN":
                                    AnswerList = "Type:&nbsp;Dropdown";
                                    dtResponse = ServicesFactory.DocCMSServices.Fetch_questions_Response(Convert.ToString(Request.QueryString["PWID"]), "DropDown", lblQuestionID.Text, "DropDown", ddlYear.SelectedValue.Trim(), ddlMonth.SelectedValue.Trim());
                                    if (dtResponse != null && dtResponse.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dtResponse.Rows.Count; i++)
                                        {
                                            drow = dt.NewRow();
                                            if (i == 0)
                                            {
                                                drow["SurveyResultTitle"] = TextFieldHeading1;
                                            }
                                            drow["ReponsesData"] = dtResponse.Rows[i]["AnswerText"].ToString();
                                            drow["ReponsesDetails"] = dtResponse.Rows[i]["Percentage"];
                                            drow["TotalReponses"] = dtResponse.Rows[i]["ResponseCount"];
                                            dt.Rows.Add(drow);
                                        }
                                    }
                                    break;
                            }

                        }
                    }


                }
                if (dt.Rows.Count > 0 && dt.Rows != null)
                {
                    ServicesFactory.DocCMSServices.ExporttoExcel(dt, "ExportSurveyResponseReport.xls", "Survey Result Title", "Reponses Data", "Total Responses", "Percentage", "", "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        private DataTable CreateDataTable()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SurveyResultTitle");
                dt.Columns.Add("ReponsesData");
                dt.Columns.Add("TotalReponses");
                dt.Columns.Add("ReponsesDetails");
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        ///////////////////////

        protected void lnkback_Click(object sender, EventArgs e)
        {
            Response.Redirect("SurveyReports.aspx?PWID=" + Convert.ToString(Request.QueryString["PWID"]));
        }

        protected void repeateryearlysummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item != null)
            {

                HtmlControl Divgreen = (HtmlControl)e.Item.FindControl("divgreen");
                HtmlControl Divblack = (HtmlControl)e.Item.FindControl("divblack");
                HtmlControl DivTotalResponseFallYear = (HtmlControl)e.Item.FindControl("DivTotalResponseFallYear");

                Label lableft = (Label)e.Item.FindControl("lableft");
                Label labright = (Label)e.Item.FindControl("labright");


                if (Divgreen != null && lableft != null)
                {
                    Divgreen.Style.Add("width", lableft.Text.ToString());
                }
                //=============================================
                if (Divblack != null && labright != null)
                {
                    Divblack.Style.Add("width", labright.Text.ToString());
                }
            }
        }

        protected void RepeaterResponse_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }


        protected void RepeaterQuestion_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item != null)
                {
                    Label lblQuestionID = (Label)e.Item.FindControl("lblQuestionID");
                    Repeater RepeaterResponse = (Repeater)e.Item.FindControl("RepeaterResponse");
                    HiddenField hdnTotalResponse = (HiddenField)e.Item.FindControl("hdnTotalResponse");

                    Label lblTotalResponse = (Label)e.Item.FindControl("lblTotalResponse");
                    Label lblQuestionType = (Label)e.Item.FindControl("lblQuestionType");
                    Literal litQTypeMsg = (Literal)e.Item.FindControl("litQTypeMsg");
                    Label lblSurveyID = (Label)e.Item.FindControl("lblSurveyID");
                    Label lblFieldType = (Label)e.Item.FindControl("lblFieldType");
                    HiddenField hdnPercentage = (HiddenField)e.Item.FindControl("hdnPercentage");
                    HiddenField hdnQTypeMsg = (HiddenField)e.Item.FindControl("hdnQTypeMsg");

                    if (lblQuestionType != null)
                    {
                        if (litQTypeMsg != null)
                        {
                            switch (lblQuestionType.Text.ToUpper())
                            {
                                case "TEXT":
                                    litQTypeMsg.Text = "Type:&nbsp;Text&nbsp;&nbsp;-&nbsp;Descriptive";
                                    break;
                                case "OPTION":
                                    litQTypeMsg.Text = "Type:&nbsp;Single&nbsp;Choice";
                                    break;
                                case "CHECK":
                                    litQTypeMsg.Text = "Type:&nbsp;Multiple&nbsp;Choice";
                                    break;
                                case "DROPDOWN":
                                    litQTypeMsg.Text = "Type:&nbsp;Select&nbsp;List";
                                    break;
                            }
                        }
                    }

                    if (RepeaterResponse != null)
                    {
                        DataTable DtQuestionSummary = null;
                        if (ddlYear.SelectedIndex > 0 && ddlMonth.SelectedIndex > 0)
                        {
                            DtQuestionSummary = ServicesFactory.DocCMSServices.Fetch_questions_Response(lblSurveyID.Text.Trim(), lblQuestionType.Text.Trim(), lblQuestionID.Text.Trim(), lblFieldType.Text.Trim(), ddlYear.SelectedValue.Trim(), ddlMonth.SelectedValue.Trim());

                        }
                        else
                        {
                            DtQuestionSummary = ServicesFactory.DocCMSServices.Fetch_questions_Response(lblSurveyID.Text.Trim(), lblQuestionType.Text.Trim(), lblQuestionID.Text.Trim(), lblFieldType.Text.Trim(), System.DateTime.Now.Year.ToString(), System.DateTime.Now.Month.ToString());
                        }


                        if (DtQuestionSummary != null && DtQuestionSummary.Rows.Count > 0)
                        {
                            if (hdnTotalResponse != null)
                            {
                                hdnTotalResponse.Value = Convert.ToString(DtQuestionSummary.Rows[0]["TotalResponse"]);
                                hdnPercentage.Value = Convert.ToString(DtQuestionSummary.Rows[0]["Percentage"]);

                            }
                            if (lblTotalResponse != null)
                            {
                                lblTotalResponse.Text = hdnTotalResponse.Value;
                            }
                            RepeaterResponse.DataSource = DtQuestionSummary;
                            RepeaterResponse.DataBind();
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void LoadReport(string surveyID, Int32 Year, Int32 Month)
        {
            try
            {
                HiddenField hdnResponded = (HiddenField)FindControl("hdnResponded");
                HiddenField hdnNonResponded = (HiddenField)FindControl("hdnNonResponded");
                HiddenField hdnTotal = (HiddenField)FindControl("hdnTotal");

                StringBuilder ProgressValue = new StringBuilder();
                DataTable DtFallYearSummary = ServicesFactory.DocCMSServices.Get_Survey_Summary_Fall_Year(surveyID, Year, Month);
                if (DtFallYearSummary != null && DtFallYearSummary.Rows.Count > 0)
                {
                    lblReportStatus.Visible = false;
                    DataColumn Dc = new DataColumn("perleft", typeof(string));
                    DtFallYearSummary.Columns.Add(Dc);
                    Dc = new DataColumn("perright", typeof(string));
                    DtFallYearSummary.Columns.Add(Dc);
                    float PosRes = 0;
                    foreach (DataRow dr in DtFallYearSummary.Rows)
                    {
                        float part = Convert.ToSingle(dr["NoOfReposesPart"]);
                        PosRes = part;
                        float left = Convert.ToSingle(dr["NoOfPartLeft"]);
                        float Total = Convert.ToSingle(dr["TotalFYResponse"]);
                        float perleft = (part * 100) / Total;
                        float perright = (left * 100) / Total;
                        dr["perleft"] = Math.Round(perleft, 0, MidpointRounding.AwayFromZero).ToString() + "%";
                        dr["perright"] = Math.Round(perright, 0, MidpointRounding.AwayFromZero).ToString() + "%";
                        litFallYear.Text = Convert.ToString(dr["FallMonth"]) + " " + Convert.ToString(dr["FallYear"]);
                        ClientScript.RegisterStartupScript(this.GetType(), "SummaryReport", "drawChart(" + part + "," + left + ")", true);
                    }

                    DtFallYearSummary.AcceptChanges();
                    litFallYear.Text = litFallYear.Text;
                    LitTotalResponse.Text = "Total Survey Recipients " + Convert.ToString(DtFallYearSummary.Rows[0]["TotalResponses"]);
                    // to Show the Data according to fall year
                    repeateryearlysummary.DataSource = DtFallYearSummary;
                    repeateryearlysummary.DataBind();
                    // To Show the Detailed Questin
                    if (PosRes > 0)
                    {
                        RepeaterQuestion.DataSource = ServicesFactory.DocCMSServices.Fetch_Survey_questions(surveyID);
                        RepeaterQuestion.DataBind();
                        divDetail.Visible = true;
                    }
                    else
                    {
                        RepeaterQuestion.DataSource = "";
                        RepeaterQuestion.DataBind();
                        divDetail.Visible = false;
                    }
                }
                else
                {
                    lblReportStatus.Visible = true;
                    return;
                    // Do nothing
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // To Get the Responses Fall Year
        protected DataTable Get_Fall_Year_Response(string surveyID)
        {
            try
            {
                return new DataTable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected DataTable Get_Fall_Year_Response(string surveyID, Int32 Year, Int32 Month)
        {
            try
            {
                return new DataTable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }// Class Ends Here
}