﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="ContentPageDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.ContentPageDashboard" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
           jQuery(document).ready(function () {
               var showValue = '<%= Session["showtablesize"] %>';
               if (showValue == null || showValue == "") {
                   showValue = 10;
               }
               jQuery('#dyntable').dataTable({
                   "sPaginationType": "full_numbers",
                   "iDisplayLength": parseInt(showValue, 10),
                   "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                   "fnDrawCallback": function (oSettings) {
                       jQuery.uniform.update();
                   }
               });
               jQuery('#dyntable2').dataTable({
                   "bScrollInfinite": true,
                   "bScrollCollapse": true,
                   "sScrollY": "300px"
               });
           });
    </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
         });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
                }
            });
        }
    </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;
            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                  }
             }
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Content Page</a> <span class="separator"></span></li>
        <li>Manage Content Page</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Content Page</h5>
            <h1>
               Manage Content Page</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              <!--Filer by Status starts here--->
               <div class="par_ctrl_sec">
                     <div class="par control-group" >
                            <label class="control-label" for="ddlStatus">
                              Filer by Status</label>
                            <div class="controls">
                               <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                               <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                               </asp:DropDownList>
                            </div>
                        </div>
                     </div> 
            <!--Filer by Status ends here--->
              Content Page Details
                <asp:Button ID="btnAddContentPageTop" runat="server" CssClass="btn btn-primary" Width="150" Text="Add Content Page"
                    Style="float: right; margin-top: -5px;" OnClick="btnAddContentPageBottom_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 50%;" />
                    <col class="con1" style="width: 20%;" />
                    <col class="con1" style="align: center; width: 20%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                           Page Name
                        </th>
                       <th class="head1">
                           Page Description
                        </th>
                        <th class="head1">
                            Active
                        </th>
                        <th class="head1">
                           Creation Date
                        </th>
                        <th class="head1">
                            Functions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrContentPage" OnItemCommand="RptrContentPage_ItemCommand"
                        OnItemDataBound="RptrContentPage_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lnkPageName" CommandArgument='<%# Eval("PageID")%>'
                                        CommandName="EditPage" runat="server" Text='<%# Eval("PageName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("PageName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                  <td>
                                  <asp:Label ID="lblPageDescription" runat="server" ClientIDMode="Static" Text='<%# Eval("PageDescription") %>'></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label runat="server" Text='<%# Eval("ExpirationDate")%>' ID="lblExpirationDate" Visible="false"></asp:Label>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblActive"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("PageID")%>' CommandName="EditPage" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("PageID")%>' CommandName="DeletePage" ToolTip="Delete Content Page" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                  <td>
                                         <asp:Label ID="lblPageID" runat="server" ClientIDMode="Static" Text='<%# Eval("PageID") %>'
                                           Visible="false"></asp:Label>
                                         <asp:LinkButton ID="lnkPageName" CommandArgument='<%# Eval("PageID")%>'
                                           CommandName="EditPage" runat="server" Text='<%# Eval("PageName")%>' />
                                         <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("PageName")%>'
                                           Visible="false"></asp:Label>
                                  </td>
                                  <td>
                                  <asp:Label ID="lblPageDescription" runat="server" ClientIDMode="Static" Text='<%# Eval("PageDescription") %>'></asp:Label>
                                </td>
                                 <td>
                                  <asp:Label runat="server" Text='<%# Eval("ExpirationDate")%>' ID="lblExpirationDate" Visible="false"></asp:Label>
                                     <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblActive" ></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("CreationDate")%>' ID="lblCreationDate"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("PageID")%>' CommandName="EditPage" ToolTip="Edit Content Page" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("PageID")%>' CommandName="DeletePage" ToolTip="Delete Content Page" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this?"
                                        TargetControlID="lnkDelete" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <asp:Button ID="btnAddContentPageBottom" runat="server" CssClass="btn btn-primary"
                Width="150" Text="Add Content Page" OnClick="btnAddContentPageBottom_Click" />
            <br />
            <br />
            <uc1:Footer ID="Footer1" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
