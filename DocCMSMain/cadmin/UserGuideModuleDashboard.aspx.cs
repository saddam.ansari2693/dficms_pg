﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.Factory;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class UserGuideModuleDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_UserModules();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
            }
        }

        protected void Bind_UserModules()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_UserGuidemodules_ForDashboard();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrUserGuideModules.DataSource = dt;
                    RptrUserGuideModules.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrUserGuideModules_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "AddContentImage":
                        Response.Redirect("~/cadmin/UserGuideImageDashboard.aspx?SID="+ e.CommandArgument.ToString()+"&SN=UserGuideModules");
                        
                        break;
                    case "AddContentDetail":
                        Response.Redirect("~/cadmin/UserGuideModuloeDetailDashboard.aspx?MID=" + e.CommandArgument.ToString());
                        break;
                    case "EditUserGuideModule":
                        Response.Redirect("~/cadmin/UserGuideModule.aspx?MID=" + e.CommandArgument.ToString());
                        break;
                    case "OrderUp":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserGuidemodules(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        Bind_UserModules();
                        break;
                    case "OrderDown":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_UserGuidemodules(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        Bind_UserModules();
                        break;
                    default:
                        break;

                }
            }
        }

        protected void RptrUserGuideModules_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void btnAddUserGuideModuleBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/UserGuideModule.aspx");
        }
    }
}