﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;
using System.IO;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DocCMSMain.cadmin
{
    public partial class New_UserGuide : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (Session["RoleID"] != null)
            {
                if (!IsPostBack)
                {
                    lblAuthority.Text = "http://" + Request.Url.Authority + "_###_" + Request.RawUrl;                 
                    Bind_User_Guide(Convert.ToInt32(Session["RoleID"]));
                    BindSideMenu(Convert.ToInt32(Session["RoleID"]));
                }
            }
        }

        protected void Bind_User_Guide(Int32 RoleID)
        {
            StringBuilder strUserGuideData = new StringBuilder();
            DataTable dtModulesID = ServicesFactory.DocCMSServices.Fetch_LeftSide_modules_By_roleid(RoleID);
            if (dtModulesID != null && dtModulesID.Rows.Count > 0)
            {

                for (Int32 i = 0; i < dtModulesID.Rows.Count; i++)
                {
                    strUserGuideData.Append("<a href='#'></a>");
                    strUserGuideData.Append("<h3 id='Module_" + dtModulesID.Rows[i]["ModuleId"] + "'><b>" + Convert.ToString(dtModulesID.Rows[i]["ModuleName"]) + "</b></h3></br>");
                    DataTable dtSubModule = ServicesFactory.DocCMSServices.Fetch_LeftSide_submodules_By_roleid_UserGuide(RoleID, Convert.ToInt32(dtModulesID.Rows[i]["ModuleID"]));
                    if (dtSubModule != null && dtSubModule.Rows.Count > 0)
                    {

                        for (Int32 j = 0; j < dtSubModule.Rows.Count; j++)
                        {
                            strUserGuideData.Append("<h3 id='SubModule_" + dtSubModule.Rows[j]["SubModuleID"] + "'><b>" + Convert.ToString(dtSubModule.Rows[j]["SubModuleName"]) + "</b></h3>");
                            // Step 1.
                            DataTable dtUserGuideModule = ServicesFactory.DocCMSServices.Get_Main_Module_Data(Convert.ToInt32(dtSubModule.Rows[j]["ModuleID"]), Convert.ToInt32(dtSubModule.Rows[j]["SubModuleID"]));
                            if (dtUserGuideModule != null && dtUserGuideModule.Rows.Count > 0)
                            {
                                for (Int32 UserGuideModuleIndex = 0; UserGuideModuleIndex < dtUserGuideModule.Rows.Count; UserGuideModuleIndex++)
                                {
                                    // Bind The Description Here
                                    strUserGuideData.Append("<p style='margin-bottom: 0in; margin-top:20px;' align='LEFT'>" + Convert.ToString(dtUserGuideModule.Rows[UserGuideModuleIndex]["Description"]) + "</p>");
                                    // Step 2. // Get the Main Module Image
                                    DataTable dtMainModuleImage = ServicesFactory.DocCMSServices.Get_User_Guide_images(Convert.ToInt32(dtUserGuideModule.Rows[UserGuideModuleIndex]["MainContentID"]), "UserGuideModules", RoleID);
                                    if (dtMainModuleImage != null && dtMainModuleImage.Rows.Count > 0)
                                    {
                                        // Bind The Images for Main Module Here
                                        for (Int32 UserGuideImageIndex = 0; UserGuideImageIndex < dtMainModuleImage.Rows.Count; UserGuideImageIndex++)//style="vertical-align: baseline; height: 400px; display: block; margin-left: auto; margin-right: auto;"
                                        {
                                            strUserGuideData.Append("<p style='margin-bottom: 0in; margin-top:20px;' align='LEFT'>" + "<img id='imgMainModuleImages_UserGuideModules_" + Convert.ToString(dtUserGuideModule.Rows[UserGuideModuleIndex]["MainContentID"]) + "' style='vertical-align: baseline; height: 400px; display: block; margin-left: auto; margin-right: auto;'  width='550px' height='400px'  title=" + Convert.ToString(dtMainModuleImage.Rows[UserGuideImageIndex]["ImageName"]) + " src='../UploadedFiles/UserGuide/" + dtMainModuleImage.Rows[UserGuideImageIndex]["Source"] + "/" + dtMainModuleImage.Rows[UserGuideImageIndex]["SourceID"] + "/" + dtMainModuleImage.Rows[UserGuideImageIndex]["RoleID"] + "/" + dtMainModuleImage.Rows[UserGuideImageIndex]["ImageName"] + "'/>" + "</p>");
                                        }
                                    }// Step2 Ends Here
                                    // Step 3. // Get the User Guide Module Detail
                                    DataTable dtUserGudieDetail = ServicesFactory.DocCMSServices.Get_UserGuide_Module_Detail(Convert.ToInt32(dtUserGuideModule.Rows[UserGuideModuleIndex]["MainContentID"]));
                                    if (dtUserGudieDetail != null && dtUserGudieDetail.Rows.Count > 0)
                                    {
                                        for (Int32 UserGuideDetainIndex = 0; UserGuideDetainIndex < dtUserGudieDetail.Rows.Count; UserGuideDetainIndex++)
                                        {
                                            // Step 4. // Get the User Guide Action Description
                                            DataTable dtUserGudieActionDescription = ServicesFactory.DocCMSServices.Get_UserGuide_Module_Action_description(Convert.ToInt32(dtUserGudieDetail.Rows[UserGuideDetainIndex]["ModuleDetailID"]));
                                            if (dtUserGudieActionDescription != null && dtUserGudieActionDescription.Rows.Count > 0)
                                            {
                                                for (Int32 UserGuideActionDescriptionIndex = 0; UserGuideActionDescriptionIndex < dtUserGudieActionDescription.Rows.Count; UserGuideActionDescriptionIndex++)
                                                {
                                                    // Step 5. Bind the Action Description
                                                    strUserGuideData.Append("</br><h3 id='ModuleDetail_" + dtUserGudieActionDescription.Rows[UserGuideActionDescriptionIndex]["ModuleDetailID"] + "'><b>" + Convert.ToString(dtUserGudieActionDescription.Rows[UserGuideActionDescriptionIndex]["Heading"]) + "</b></h3>");
                                                    strUserGuideData.Append(Convert.ToString(dtUserGudieActionDescription.Rows[UserGuideActionDescriptionIndex]["Description"]));
                                                    // Step 6. Get Action Description Images                                                   
                                                    DataTable dtActionDescriptionImages = ServicesFactory.DocCMSServices.Get_User_Guide_images(Convert.ToInt32(dtUserGudieActionDescription.Rows[UserGuideActionDescriptionIndex]["ActionDetailID"]), "UserGuideActionDescription",RoleID);
                                                    if (dtActionDescriptionImages != null && dtActionDescriptionImages.Rows.Count > 0)
                                                    {
                                                        // Bind The Images for Main Module Here
                                                        for (Int32 DescImageIndex = 0; DescImageIndex < dtActionDescriptionImages.Rows.Count; DescImageIndex++)
                                                        {
                                                            strUserGuideData.Append("<p style='margin-bottom: 0in; margin-top:20px;' align='LEFT'>" + "<img id='imgActionDescription_UserGuideActionDescription" + Convert.ToString(dtUserGudieActionDescription.Rows[UserGuideActionDescriptionIndex]["ModuleDetailID"]) + "' style='vertical-align: baseline; height: 400px; display: block; margin-left: auto; margin-right: auto;'  width='550px' height='400px'   title=" + Convert.ToString(dtActionDescriptionImages.Rows[DescImageIndex]["ImageName"]) + " src='../UploadedFiles/UserGuide/" + dtActionDescriptionImages.Rows[DescImageIndex]["Source"] + "/" + dtActionDescriptionImages.Rows[DescImageIndex]["SourceID"] + "/" + dtActionDescriptionImages.Rows[DescImageIndex]["RoleID"] + "/" + dtActionDescriptionImages.Rows[DescImageIndex]["ImageName"] + "'/>" + "</p>");
                                                        }
                                                    }// Step 6 Ends Here
                                                }
                                            }// Step 4 Ends Here
                                        }
                                    }// Step 3 Ends Here
                                }
                            }// Step 1 Ends here
                        }
                    }// Submodule scope Ends here
                }
                 UserGuideDiv.InnerHtml = strUserGuideData.ToString();
            }// Main Module Scope Ends Here
           
        }

        protected void BindSideMenu(Int32 RoleID)
        {
            DataTable dtModules = ServicesFactory.DocCMSServices.Fetch_SubModules_By_roleid_UserGuide(RoleID);
            StringBuilder sb = new StringBuilder();
            if (dtModules != null && dtModules.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtModules.Rows.Count; i++)
                {
                    sb.Append("<li><a href='New_UserGuide.aspx#SubModule_" + dtModules.Rows[i]["SubModuleId"] + "'>" + Convert.ToString(dtModules.Rows[i]["SubModuleName"]) + "<a></li>");
                    maincontents.InnerHtml = sb.ToString();
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            string imagePath = "";
            string imageRawURL = Request.RawUrl;
            ClientScript.RegisterStartupScript(this.GetType(), "", "<script>alert('" + imagePath + "');</script>");
            string UserGuideDivecontent = UserGuideDiv.InnerHtml;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SetWidthHeight();", true);
            if (imageRawURL.ToLower().IndexOf("/cadmin") == 0)
            {
                imagePath = "http://" + Request.Url.Authority;
                UserGuideDivecontent = UserGuideDivecontent.ToString().Replace("../UploadedFiles/UserGuide/", imagePath + "/UploadedFiles/UserGuide/");
            }
            else
            {
                string[] rawArrary = imageRawURL.ToString().Split('/');
                if (rawArrary.Length > 1)
                {
                    imagePath = "http://" + Request.Url.Authority + "/" + rawArrary[1].ToString();
                }
                UserGuideDivecontent = UserGuideDivecontent.ToString().Replace("../UploadedFiles/UserGuide/", imagePath + "/UploadedFiles/UserGuide/");
            }
            UserGuideDiv.InnerHtml = UserGuideDivecontent;
            UserGuideDiv.Attributes["class"] = "myclass";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserGuide.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            UserGuideDiv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 2f, 2f, 20f, 20f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
         }
    }
}