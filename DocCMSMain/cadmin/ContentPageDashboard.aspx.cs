﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class ContentPageDashboard : System.Web.UI.Page
    {
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            if (!IsPostBack)
            {
                Bind_ContentPage();
                if (Session["showtablesize"] != null)
                ShowTableSize = Convert.ToString(Session["showtablesize"]);
                SetControls();
            }
        }

        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddContentPageTop.Visible = true;
                    btnAddContentPageBottom.Visible = true;
                }
                else
                {
                    btnAddContentPageTop.Visible = false;
                    btnAddContentPageBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrContentPage.Items)
                {

                    LinkButton lnkPageName = (LinkButton)Item.FindControl("lnkPageName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");

                    if (lblTitle != null && lnkPageName != null && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkPageName.Visible = true;
                            lblTitle.Visible = false;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkPageName.Visible = false;
                            lblTitle.Visible = true;
                        }

                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }

                }

            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        protected void Bind_ContentPage()
        {
            DataTable contentPage = ServicesFactory.DocCMSServices.Fetch_ContentPage();
            if (contentPage != null && contentPage.Rows.Count > 0)
            {
                RptrContentPage.DataSource = contentPage;
                RptrContentPage.DataBind();
            }
        }



        protected void RptrContentPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandArgument != null)
            {
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                switch (e.CommandName)
                {
                    case "EditPage":
                        Response.Redirect("~/cadmin/ContentPages.aspx?ID=" + e.CommandArgument.ToString() + "&Mode=Edit");
                        break;
                    case "DeletePage":
                         ServicesFactory.DocCMSServices.Delete_Content_Page_Byid(Convert.ToString(e.CommandArgument));
                        Bind_ContentPage();
                        break;
                    default:
                        break;
                }
            }
        }

        protected void RptrContentPage_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblExpirationDate = (Label)e.Item.FindControl("lblExpirationDate");
                if (lblExpirationDate != null)
                {
                    if (lblExpirationDate.Text == "01/01/1900")
                    {
                        lblExpirationDate.Text = "";
                    }
                }
            }
        }

        protected void btnAddContentPageBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/ContentPages.aspx");
        }
    }
}