﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true"
    CodeBehind="SliderManagementDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.SliderManagementDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var showValue = '<%= Session["showtablesize"] %>';
            if (showValue == null || showValue == "") {
                showValue = 10;
            }
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": parseInt(showValue, 10),
                "aaSortingFixed": [[1, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
            });
       });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {

                },
                error: function () {

                }
            });

        }       
    </script>
    <%--Filter by Status ie show all,show active,show inactive--%>
    <script type="text/javascript">
        function GetSelectedTextValue(ddlStatus) {
            var selectedText = ddlStatus.options[ddlStatus.selectedIndex].innerHTML;
            var selectedValue = ddlStatus.value;
            var TotalCount = jQuery('#dyntable tbody tr').length;

            if (selectedValue == 'Show Active') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).hide();
                    }
                    else {
                        jQuery('#tableTR' + i).show();
                    }
                }
            }
            else if (selectedValue == 'Show InActive') {
                for (var i = 1; i <= TotalCount; i++) {
                    if (jQuery('#tableTR' + i + ' .Status').html() == 'False') {
                        jQuery('#tableTR' + i).show();
                    }
                    else {
                        jQuery('#tableTR' + i).hide();
                    }
                }
            }
            else {
                for (var i = 1; i <= TotalCount; i++) {
                    jQuery('#tableTR' + i).show();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage slider</a> <span class="separator"></span></li>
        <li>Slider Management Dashboard</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage slider</h5>
            <h1>
                Slider Management Dashboard</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
                <!--Filer by Status starts here--->
                <div class="par_ctrl_sec">
                    <div class="par control-group">
                        <label class="control-label" for="ddlStatus">
                            Filer by Status</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlStatus" runat="server" onchange="GetSelectedTextValue(this)">
                                <asp:ListItem>Show All</asp:ListItem>
                                <asp:ListItem>Show Active</asp:ListItem>
                                <asp:ListItem>Show InActive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!--Filer by Status ends here--->
                Slider Management Dashboard
                <asp:Button ID="btnAddSlider" runat="server" CssClass="btn btn-primary" Width="150"
                    Text="Add New Slider" Style="float: right; margin-top: -5px;" OnClick="btnAddSlider_Click" />
            </h4>
            <table id="dyntable" class="table table-bordered responsive">
                <colgroup>
                    <col class="con1" style="align: center; width: 15%;" />
                    <col class="con1" style="width: 20%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="align: center; width: 15%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                            S_No
                        </th>
                        <th class="head1">
                            Slider Name
                        </th>
                        <th class="head1">
                            Height
                        </th>
                        <th class="head1">
                            Width
                        </th>
                        <th class="head1">
                            IsActive
                        </th>
                        <th class="head1">
                            <center>
                                Functions</center>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrSlider" OnItemCommand="RptrSlider_ItemCommand"
                        OnItemDataBound="RptrSlider_databound">
                        <ItemTemplate>
                            <tr class="gradeX" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:Label ID="lblSliderID" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderID") %>'
                                        Visible="false"></asp:Label>
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkSliderID" CommandArgument='<%# Eval("SliderID")%>' CommandName="EditSlider"
                                        runat="server" Text='<%# Eval("SliderName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Height")%>' ID="lblheight"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Width")%>' ID="lblwidth"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkNavigate" ImageUrl="~/images/icon-add.png"
                                        CommandArgument='<%# Eval("SliderID")%>' CommandName="AddSlide" ToolTip="Add Slides" />
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SliderID")%>' CommandName="EditSlider" ToolTip="Edit Slider" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SliderID")%>' CommandName="DeleteSlider" ToolTip="Delete Slider"
                                        OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA" id="tableTR<%#Container.ItemIndex+1 %>">
                                <td>
                                    <asp:Label ID="lblSliderID" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderID") %>'
                                        Visible="false"></asp:Label>
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkSliderID" CommandArgument='<%# Eval("SliderID")%>' CommandName="EditSlider"
                                        runat="server" Text='<%# Eval("SliderName")%>' />
                                    <asp:Label ID="lblTitle" runat="server" ClientIDMode="Static" Text='<%# Eval("SliderName")%>'
                                        Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Height")%>' ID="lblheight"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("Width")%>' ID="lblwidth"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Eval("IsActive")%>' class="Status" ID="lblActive"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:ImageButton runat="server" ID="lnkNavigate" ImageUrl="~/images/icon-add.png"
                                        CommandArgument='<%# Eval("SliderID")%>' CommandName="AddSlide" ToolTip="Add Slides" />
                                    <asp:ImageButton runat="server" ID="lnkEdit" ImageUrl="../images/table_icon_1.gif"
                                        CommandArgument='<%# Eval("SliderID")%>' CommandName="EditSlider" ToolTip="Edit Slider" />
                                    <asp:ImageButton runat="server" ID="lnkDelete" ImageUrl="../images/table_icon_2.gif"
                                        CommandArgument='<%# Eval("SliderID")%>' CommandName="DeleteSlider" ToolTip="Delete Slider"
                                        OnClientClick="return confirm('Are you sure to delete?');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
