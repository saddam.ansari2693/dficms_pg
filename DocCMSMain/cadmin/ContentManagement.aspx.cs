﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using DocCMS.Core;
using System.Data;
using System.Net;
using System.Web.UI.HtmlControls;
using System.Text;
using DocCMS.Core.DataTypes;
using System.IO;
using DocCMS.Core.Implementation;

namespace DocCMSMain.cadmin
{
    public partial class ContentManagement : System.Web.UI.Page
    {
        public static string SMTPServer { get; set; }
        public static string SSL_Certificate { get; set; }
        public static Int32 SurveyItemCount { get; set; }
        public static Int32 RowItemVal { get; set; }
        public static Regex ValidEmailRegex = CreateValidEmailRegex();
        public static string MainMenuName { get; set; }
        public static string SubMenuName { get; set; }
        public static Int32 CurDisplayOrder { get; set; }
        public bool ReqFlag = false;
        public Int32 WindowWidth = 0;
        public Int32 WindowHeight = 0;
        static bool IsPageAlive = true;
        dynamic UserId;
        string PageName = "";
        bool pageDrafted = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            UserId = (Session["UserID"]);
            RowItemVal = 0;
            if (!IsPostBack)
            {
                try
                {
                    if (Request.QueryString["PID"] != null && Request.QueryString["CMSID"] != null)
                    {
                        pageDrafted = ServicesFactory.DocCMSServices.Is_Content_Page_Drafted(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToInt32(Request.QueryString["CMSID"]));
                    }

                    if (Request.QueryString["PID"] != null)
                    {
                        string PageId = Convert.ToString(Request.QueryString["PID"]);
                        Bind_DisplayOrder(Convert.ToInt32(PageId));
                        DataTable dtMainPage = ServicesFactory.DocCMSServices.Fetch_Page_Master_Data_By_pageid(PageId);
                        if (dtMainPage != null && dtMainPage.Rows.Count > 0)
                        {
                            LitpageTitle.Text = Convert.ToString(dtMainPage.Rows[0]["PageName"]);
                            LitContentTitle.Text = Convert.ToString(dtMainPage.Rows[0]["PageName"]);
                            myModalLabel.InnerText = "Update " + LitContentTitle.Text + " Display Orders";
                            string ExpiryDate = Convert.ToString(dtMainPage.Rows[0]["ExpirationDate"]);
                            if (!ExpiryDate.ToString().Trim().EndsWith("1900"))
                            {
                                if (Convert.ToDateTime(ExpiryDate) < System.DateTime.Now)
                                {
                                    // do the code for expiration message                                    
                                    IsPageAlive = false;
                                }
                                else
                                {
                                    IsPageAlive = true;
                                }
                            }
                            else
                            {
                                IsPageAlive = true;
                            }
                            if (IsPageAlive == true)
                            {
                                DataTable dtContent = null;
                                dtContent = ServicesFactory.DocCMSServices.Fetch_Page_Detail_Data_By_pageid(PageId);
                                if (dtContent != null && dtContent.Rows.Count > 0)
                                {
                                    RptrContentMgmt.DataSource = dtContent;
                                    RptrContentMgmt.DataBind();
                                }
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>alert('Page has been expired... Please Contact the administrator.');</script>");
                                return;
                            }
                        }

                    }

                    if (Request.QueryString["CMSID"] != null)
                    {
                        hdnCurPageID.Value = Convert.ToString(Request.QueryString["PID"]);
                        hdnCurCMSID.Value = Convert.ToString(Request.QueryString["CMSID"]);
                        hdnCMSID.Value = Request.QueryString["CMSID"];
                        if (Convert.ToString(LitpageTitle.Text).ToUpper() == "CATALOGUE")
                        {
                            DataTable dtSize = ServicesFactory.DocCMSServices.Fetch_size(Request.QueryString["CMSID"]);
                            DataTable dtColor = ServicesFactory.DocCMSServices.Fetch_color_Details(Request.QueryString["CMSID"]);
                            DivSize.Visible = true;
                            DivColor.Visible = true;
                            if (dtSize != null && dtSize.Rows.Count > 0)
                            {
                                CheckSize.Checked = true;
                                btnSize.Style.Add("display", "");
                            }
                            else
                            {
                                CheckSize.Checked = false;
                                btnSize.Style.Add("display", "none");
                            }
                            if (dtColor != null && dtColor.Rows.Count > 0)
                            {
                                CheckColor.Checked = true;
                                btnColor.Style.Add("display", "");
                            }
                            else
                            {
                                CheckColor.Checked = false;
                                btnColor.Style.Add("display", "none");
                            }
                        }
                        else
                        {
                            DivSize.Visible = false;
                            DivColor.Visible = false;
                        }
                    }
                    else
                    {
                      
                    }
                    CheckSetDraft();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        protected void RptrContentMgmt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }

        // Function for Valide Email Regular Expression
        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }

        // Check Email id is Valid or not
        protected static bool EmailIsValid(string emailAddress)
        {
            bool isValid = ValidEmailRegex.IsMatch(emailAddress);

            return isValid;
        }

        // Function For Get Data for Selection
        protected DataTable Get_Data_For_Section(string pageID, string FieldType, string Dorder)
        {
            try
            {
                return ServicesFactory.DocCMSServices.Fetch_Page_Data_By_fieldtype(pageID, FieldType, Dorder);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrContentMgmt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                if (RowItemVal == SurveyItemCount - 1)
                    RowItemVal++;
                Label lblPageID = (Label)e.Item.FindControl("lblPageID");
                Label lblFieldType = (Label)e.Item.FindControl("lblFieldType");
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                Label lblRequired = (Label)e.Item.FindControl("lblRequired");
                Label lblDefaultText = (Label)e.Item.FindControl("lblDefaultText");

                HtmlGenericControl DivTitle = (HtmlGenericControl)e.Item.FindControl("DivTitle");
                HtmlGenericControl DivUpload = (HtmlGenericControl)e.Item.FindControl("DivUpload");
                HtmlGenericControl DivTextBox = (HtmlGenericControl)e.Item.FindControl("DivTextBox");
                HtmlGenericControl divCheckBox = (HtmlGenericControl)e.Item.FindControl("divCheckBox");
                HtmlGenericControl DivDropDown = (HtmlGenericControl)e.Item.FindControl("DivDropDown");
                HtmlGenericControl DivRadioButton = (HtmlGenericControl)e.Item.FindControl("DivRadioButton");
                HtmlGenericControl DivMultiLine = (HtmlGenericControl)e.Item.FindControl("DivMultiLine");
                HtmlGenericControl DivTextEditor = (HtmlGenericControl)e.Item.FindControl("DivTextEditor");
                HtmlGenericControl DivSectionBreak = (HtmlGenericControl)e.Item.FindControl("DivSectionBreak");
                HtmlGenericControl DivEmptySpace = (HtmlGenericControl)e.Item.FindControl("DivEmptySpace");
                HtmlGenericControl DivParagraphText = (HtmlGenericControl)e.Item.FindControl("DivParagraphText");
                HtmlGenericControl DivSingleLineText = (HtmlGenericControl)e.Item.FindControl("DivSingleLineText");
                HtmlGenericControl DivCheckBox = (HtmlGenericControl)e.Item.FindControl("DivCheckBox");
                HtmlGenericControl DivListBox = (HtmlGenericControl)e.Item.FindControl("DivListBox");
                HtmlImage btnResize = (HtmlImage)e.Item.FindControl("imgEdit");

                if (lblFieldType != null && lblPageID != null && lblDorder != null && lblRequired != null && lblDefaultText != null)
                {
                    switch (lblFieldType.Text.Trim())
                    {
                        case "Title":
                        case "Content":
                            DataTable dtTitle = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (dtTitle != null && dtTitle.Rows.Count > 0)
                            {
                                HtmlGenericControl LitTitle = (HtmlGenericControl)e.Item.FindControl("LitTitle");

                                if (LitTitle != null)
                                {
                                    LitTitle.InnerText = Convert.ToString(dtTitle.Rows[0]["TextFieldHeadingValue1"]);
                                    LitTitle.Style.Add("font-size", Convert.ToString(dtTitle.Rows[0]["TextFieldHeadingValue2"]) + "px");
                                    LitTitle.Style.Add("text-align", Convert.ToString(dtTitle.Rows[0]["TextFieldHeadingValue3"]));
                                    LitTitle.Style.Add("color", "#" + Convert.ToString(dtTitle.Rows[0]["TextFieldHeadingValue4"]));

                                }
                            }
                            DivTitle.Visible = true;
                            break;

                        case "Section Break":
                            DivSectionBreak.Visible = true;
                            DataTable DtSectionBreak = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (DtSectionBreak != null && DtSectionBreak.Rows.Count > 0)
                            {
                                HtmlGenericControl DivSectionBreakData = (HtmlGenericControl)e.Item.FindControl("DivSectionBreakData");
                                DivSectionBreakData.Style.Add(HtmlTextWriterStyle.Width, Convert.ToString(DtSectionBreak.Rows[0]["TextFieldHeadingValue2"]) + "px");
                                DivSectionBreakData.Style.Add(HtmlTextWriterStyle.Height, Convert.ToString(DtSectionBreak.Rows[0]["TextFieldHeadingValue3"]) + "px");
                                DivSectionBreakData.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + Convert.ToString(DtSectionBreak.Rows[0]["TextFieldHeadingValue4"]));
                                // DivSectionBreakData.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");

                            }
                            break;

                        case "Single Line Text":
                        case "Full Name":
                        case "First Name":
                        case "Last Name":
                        case "Email Address":
                        case "Company":
                        case "Phone Number":
                        case "Address 1":
                        case "Address 2":
                        case "City":
                        case "State":
                        case "Zip":
                        case "Custom 1":
                        case "Custom 2":
                        case "Custom 3":

                            DataTable dtTextBox = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (dtTextBox != null && dtTextBox.Rows.Count > 0)
                            {
                                Literal LitLabelText = (Literal)e.Item.FindControl("LitLabelText");
                                HtmlInputText txtContainer = (HtmlInputText)e.Item.FindControl("txtContainer");
                                HtmlInputButton btnShowNavigationURL = (HtmlInputButton)e.Item.FindControl("btnShowNavigationURL");
                                Button btnShowDisplayOrders = (Button)e.Item.FindControl("btnShowDisplayOrders");
                                if (LitLabelText != null && txtContainer != null && btnShowNavigationURL != null && btnShowDisplayOrders != null)
                                {
                                    LitLabelText.Text = Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue2"]);
                                    lblRequired.Text = Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue4"]);
                                    txtContainer.Attributes.Add("onfocus", "clearText(this,'" + Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue3"]) + "')");
                                    txtContainer.Attributes.Add("onblur", "resetText(this,'" + Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue3"]) + "')");
                                    if (txtContainer.Value == "")
                                    {
                                        lblDefaultText.Text = Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue3"]);
                                        txtContainer.Value = Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue3"]);
                                        txtContainer.Style.Add("color", "Silver");
                                    }
                                    else if (txtContainer.Value == Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue3"]))
                                    {
                                        txtContainer.Style.Add("color", "Silver");
                                    }
                                    else
                                    {
                                        txtContainer.Style.Add("color", "Black");
                                    }
                                    if (Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue5"]) == "TRUE")
                                    {
                                        txtContainer.Attributes.Add("onkeypress", "return showAutoComlete(this);");
                                    }
                                    else
                                    {
                                        btnShowNavigationURL.Visible = false;
                                    }
                                    if (Convert.ToString(dtTextBox.Rows[0]["TextFieldHeading6"]).ToUpper() == "MAX LENGTH")
                                    {
                                        txtContainer.Attributes.Add("maxlength", Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue6"]));
                                    }

                                    if (Convert.ToString(dtTextBox.Rows[0]["TextFieldHeading8"]).ToUpper() == "DATEPICKER" && Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue8"]).ToUpper() == "TRUE")
                                    {
                                        txtContainer.Attributes.Add("onfocus", "return datepicker(this);");
                                    }
                                    if (Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue7"]) == "TRUE")
                                    {
                                        txtContainer.Attributes.Add("onkeypress", "return validateKey(event);");
                                        txtContainer.Attributes.Add("autocomplete", "off");
                                        if (Convert.ToString(dtTextBox.Rows[0]["TextFieldHeadingValue2"]).ToUpper() == "DISPLAY ORDER")
                                        {
                                            btnShowDisplayOrders.Visible = true;
                                            Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Content_dorder(Convert.ToInt32(Request.QueryString["PID"])) + 1;
                                            if (DisplayOrder != 0)
                                            {
                                                txtContainer.Value = Convert.ToString(DisplayOrder);
                                                txtContainer.Attributes.Add("disabled", "disabled");

                                            }
                                        }
                                    }
                                    else
                                    {
                                        btnShowDisplayOrders.Visible = false;
                                    }
                                }

                                if (Request.QueryString["CMSID"] != null)
                                {
                                    string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                    if (!string.IsNullOrEmpty(Fieldvalue))
                                    {
                                        txtContainer.Value = Fieldvalue.Trim();
                                        txtContainer.Style.Add("color", "Black");
                                    }
                                }
                            }
                            DivTextBox.Visible = true;
                            break;
                        case "Multi Line Text":
                        case "Paragraph Text":
                            DataTable dtMultiText = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (dtMultiText != null && dtMultiText.Rows.Count > 0)
                            {
                                Literal LitMultiLine = (Literal)e.Item.FindControl("LitMultiLine");
                                HtmlTextArea txtMultiLine = (HtmlTextArea)e.Item.FindControl("txtMultiLine");
                                if (LitMultiLine != null && txtMultiLine != null)
                                {
                                    lblRequired.Text = Convert.ToString(dtMultiText.Rows[0]["TextFieldHeadingValue4"]);
                                    LitMultiLine.Text = Convert.ToString(dtMultiText.Rows[0]["TextFieldHeadingValue2"]);
                                    if (txtMultiLine.Value == "")
                                    {
                                        //txtMultiLine.Value = Convert.ToString(dtMultiText.Rows[0]["TextFieldHeadingValue3"]);
                                        txtMultiLine.Attributes.Add("placeholder", Convert.ToString(dtMultiText.Rows[0]["TextFieldHeadingValue3"]));
                                        lblDefaultText.Text = Convert.ToString(dtMultiText.Rows[0]["TextFieldHeadingValue3"]);
                                    }
                                }
                                if (Request.QueryString["CMSID"] != null)
                                {
                                    string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                    if (!string.IsNullOrEmpty(Fieldvalue))
                                    {
                                        txtMultiLine.Value = Fieldvalue.Trim();
                                        txtMultiLine.Style.Add("color", "Black");
                                    }
                                }
                            }
                            DivMultiLine.Visible = true;
                            break;
                        case "TextEditor":
                            DataTable dtText = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (dtText != null && dtText.Rows.Count > 0)
                            {
                                Literal LitTextEditor = (Literal)e.Item.FindControl("LitTextEditor");
                                HtmlTextArea elm1 = (HtmlTextArea)e.Item.FindControl("elm1");
                                if (LitTextEditor != null && elm1 != null)
                                {
                                    lblRequired.Text = Convert.ToString(dtText.Rows[0]["TextFieldHeadingValue4"]);
                                    LitTextEditor.Text = Convert.ToString(dtText.Rows[0]["TextFieldHeadingValue2"]);
                                    if (elm1.Value == "")
                                    {
                                        elm1.Value = Convert.ToString(dtText.Rows[0]["TextFieldHeadingValue3"]);
                                        lblDefaultText.Text = Convert.ToString(dtText.Rows[0]["TextFieldHeadingValue3"]);
                                        elm1.Style.Add("color", "Black");
                                        //elm1.Attributes.Add("class", "tinymce");
                                    }
                                }
                                if (Request.QueryString["CMSID"] != null)
                                {
                                    string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                    if (!string.IsNullOrEmpty(Fieldvalue))
                                    {
                                        elm1.Value = Fieldvalue.Trim();
                                        elm1.Style.Add("color", "Black");
                                    }
                                    else
                                    {
                                        elm1.Value = Convert.ToString(dtText.Rows[0]["TextFieldHeadingValue3"]);
                                        elm1.Style.Add("color", "Black");
                                    }
                                }
                            }
                            DivTextEditor.Visible = true;
                            break;


                        case "MultipleChoice":

                            DataTable DtMultipleChoice = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (DtMultipleChoice != null && DtMultipleChoice.Rows.Count > 0)
                            {
                                Literal LiRadioButton = (Literal)e.Item.FindControl("LiRadioButton");
                                RadioButtonList rbtnList = (RadioButtonList)e.Item.FindControl("rbtnList");
                                if (LiRadioButton != null && rbtnList != null)
                                {
                                    LiRadioButton.Text = Convert.ToString(DtMultipleChoice.Rows[0]["TextFieldHeadingValue1"]);
                                    StringBuilder strRadio = new StringBuilder();
                                    string ItemCount = Convert.ToString(DtMultipleChoice.Rows[0]["TextFieldHeadingValue2"]);
                                    string Itemval = Convert.ToString(DtMultipleChoice.Rows[0]["TextFieldHeadingValue3"]);

                                    if (!string.IsNullOrEmpty(Itemval))
                                    {
                                        string[] ItemData = Itemval.Split('~');
                                        if (ItemData.Length > 0)
                                        {
                                            ListItem rbtnLi = null;

                                            for (Int32 i = 0; i < ItemData.Length; i++)
                                            {
                                                if (!string.IsNullOrEmpty(ItemData[i]))
                                                {
                                                    rbtnLi = new ListItem(ItemData[i], ItemData[i]);
                                                    rbtnList.Items.Add(rbtnLi);
                                                    if (Request.QueryString["CMSID"] != null)
                                                    {
                                                        string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                                        if (!string.IsNullOrEmpty(Fieldvalue))
                                                        {
                                                            if (Fieldvalue == rbtnLi.Value)
                                                            {
                                                                rbtnList.Items[i].Selected = true;
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                            DivRadioButton.Visible = true;
                            break;
                        case "Checkbox":
                            ListItem ckkLi = null;
                            CheckBoxList chkList = null;
                            DataTable dtCheckedArea = null;
                            DataTable DtCheckOption = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (DtCheckOption != null && DtCheckOption.Rows.Count > 0)
                            {
                                Literal lblSingleChoice = (Literal)e.Item.FindControl("lblSingleChoice");
                                chkList = (CheckBoxList)e.Item.FindControl("chkList");
                                if (lblSingleChoice != null && chkList != null)
                                {
                                    lblSingleChoice.Text = Convert.ToString(DtCheckOption.Rows[0]["TextFieldHeadingValue1"]);
                                    StringBuilder strCheck = new StringBuilder();
                                    string ItemCount = Convert.ToString(DtCheckOption.Rows[0]["TextFieldHeadingValue2"]);
                                    string Itemval = Convert.ToString(DtCheckOption.Rows[0]["TextFieldHeadingValue3"]);

                                    if (ItemCount.ToUpper() != "SELECT")
                                    {

                                        if (!string.IsNullOrEmpty(Itemval))
                                        {

                                            string[] ItemData = Itemval.Split('~');
                                            if (ItemData.Length > 0)
                                            {
                                                for (Int32 i = 0; i < ItemData.Length; i++)
                                                {
                                                    if (!string.IsNullOrEmpty(ItemData[i]))
                                                    {

                                                        if (Request.QueryString["CMSID"] != null)
                                                        {

                                                            DataTable Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_Checked_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                                            if (Fieldvalue != null && Fieldvalue.Rows.Count > 0)
                                                            {
                                                                ckkLi = new ListItem(ItemData[i], Convert.ToString(Fieldvalue.Rows[i]["FieldValue"]));
                                                                chkList.Items.Add(ckkLi);

                                                                if (Convert.ToString(Fieldvalue.Rows[i]["FieldValue"]) == "True")
                                                                {
                                                                    chkList.Items[i].Selected = true;
                                                                }
                                                                else
                                                                {
                                                                    chkList.Items[i].Selected = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ckkLi = new ListItem(ItemData[i], ItemData[i]);
                                                                chkList.Items.Add(ckkLi);
                                                            }

                                                        }
                                                        else
                                                        {
                                                            ckkLi = new ListItem(ItemData[i], ItemData[i]);
                                                            chkList.Items.Add(ckkLi);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (Int32 j = 0; j < Convert.ToInt32(ItemCount); j++)
                                            {
                                                ckkLi = new ListItem();
                                                chkList.Items.Add(ckkLi);
                                                if (Request.QueryString["CMSID"] != null)
                                                {
                                                    string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                                    if (!string.IsNullOrEmpty(Fieldvalue))
                                                    {
                                                        if (Fieldvalue.Trim().ToUpper() == "TRUE")
                                                        {
                                                            chkList.Items[j].Selected = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    else
                                    {
                                        switch (Convert.ToString(LitpageTitle.Text.Trim().ToUpper()))
                                        {
                                            case "HOME CONTENT":
                                                PageName = "Home Section";
                                                break;
                                            case "EVENT":
                                                PageName = "Area Master";
                                                break;
                                            default:
                                                break;
                                        }
                                        DataTable dtMenus = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown(PageName);
                                        if (dtMenus != null && dtMenus.Rows.Count > 0)
                                        {
                                            for (Int32 i = 0; i < dtMenus.Rows.Count; i++)
                                            {
                                                if (Request.QueryString["CMSID"] != null)
                                                {

                                                    DataTable Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_Checked_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                                    if (LitpageTitle.Text.ToUpper() == "EVENT")
                                                    {
                                                        if (Fieldvalue != null && Fieldvalue.Rows.Count > 0)
                                                        {
                                                            dtCheckedArea = Fieldvalue;
                                                        }
                                                        ckkLi = new ListItem(Convert.ToString(dtMenus.Rows[i]["Fieldvalue"]), Convert.ToString(dtMenus.Rows[i]["FieldValue"]));
                                                        chkList.Items.Add(ckkLi);
                                                    }
                                                    else
                                                    {
                                                        if (Fieldvalue != null && Fieldvalue.Rows.Count > 0)
                                                        {
                                                            ckkLi = new ListItem(Convert.ToString(dtMenus.Rows[i]["Fieldvalue"]), Convert.ToString(Fieldvalue.Rows[i]["FieldValue"]));
                                                            chkList.Items.Add(ckkLi);

                                                            if (Convert.ToString(Fieldvalue.Rows[i]["FieldValue"]) == "True")
                                                            {
                                                                chkList.Items[i].Selected = true;
                                                            }
                                                            else
                                                            {
                                                                chkList.Items[i].Selected = false;
                                                            }


                                                        }
                                                        else
                                                        {
                                                            ckkLi = new ListItem(Convert.ToString(dtMenus.Rows[i]["Fieldvalue"]), Convert.ToString(dtMenus.Rows[i]["Fieldvalue"]));
                                                            chkList.Items.Add(ckkLi);
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    ckkLi = new ListItem(Convert.ToString(dtMenus.Rows[i]["Fieldvalue"]), Convert.ToString(dtMenus.Rows[i]["Fieldvalue"]));
                                                    chkList.Items.Add(ckkLi);
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                            if (dtCheckedArea != null && dtCheckedArea.Rows.Count > 0)
                            {
                                string FieldAreas = Convert.ToString(dtCheckedArea.Rows[0]["Fieldvalue"]).Trim();
                                string[] FieldAreasArray = FieldAreas.Trim().Split(',');
                                for (Int32 x = 0; x < FieldAreasArray.Length; x++)
                                {
                                    for (Int32 y = 0; y < chkList.Items.Count; y++)
                                    {
                                        if (chkList.Items[y].Text.Trim().ToLower() == FieldAreasArray[x].Trim().ToLower())
                                        {
                                            chkList.Items[y].Selected = true;
                                        }
                                    }
                                }
                            }
                            divCheckBox.Visible = true;

                            break;
                        case "ListBox":
                            if (LitpageTitle != null)
                            {
                                switch (Convert.ToString(LitpageTitle.Text.ToUpper()))
                                {
                                    case "SUB MENU":
                                        PageName = "Main Menu";
                                        break;
                                    default:
                                        break;
                                }
                            }

                            DataTable dtListBox = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));

                            if (dtListBox != null && dtListBox.Rows.Count > 0)
                            {
                                Literal LitlstBox = (Literal)e.Item.FindControl("LitlstBox");
                                if (LitlstBox != null)
                                {
                                    LitlstBox.Text = Convert.ToString(dtListBox.Rows[0]["TextFieldHeadingValue1"]);
                                    string ItemCount = Convert.ToString(dtListBox.Rows[0]["TextFieldHeadingValue2"]);
                                    string Itemval = Convert.ToString(dtListBox.Rows[0]["TextFieldHeadingValue3"]);
                                    ListBox LstSelection = (ListBox)e.Item.FindControl("LstSelection");

                                    if (ItemCount.Trim().ToUpper() == "SELECT")
                                    {
                                        switch (LitpageTitle.Text)
                                        {
                                            case "Sub Menu(Level-1)":
                                                PageName = "Main Menu";
                                                break;
                                            case "Sub Menu(Level-2)":
                                                PageName = "Sub Menu(Level-1)";
                                                break;
                                            default:
                                                break;
                                        }
                                        DataTable dtMenus = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown(PageName);
                                        if (dtMenus != null && dtMenus.Rows.Count > 0)
                                        {

                                            LstSelection.DataSource = dtMenus;
                                            LstSelection.DataValueField = "FieldValue";
                                            LstSelection.DataTextField = "FieldValue";
                                            LstSelection.DataBind();
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(Itemval))
                                        {
                                            string[] ItemData = Itemval.Split('~');
                                            if (ItemData.Length > 0)
                                            {

                                                for (Int32 i = 0; i < ItemData.Length; i++)
                                                {
                                                    LstSelection.Items.Add(new ListItem(ItemData[i].ToString()));
                                                }
                                                hdnListBoxVal.Value = ItemData[0].ToString();



                                            }


                                        }
                                    }

                                    if (Request.QueryString["CMSID"] != null)
                                    {
                                        string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                        if (!string.IsNullOrEmpty(Fieldvalue))
                                        {
                                            LstSelection.SelectedValue = Fieldvalue.Trim();

                                        }
                                    }
                                }
                            }

                            DivListBox.Visible = true;

                            break;
                        case "Dropdown":
                            DataTable dtDropDown = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));

                            if (dtDropDown != null && dtDropDown.Rows.Count > 0)
                            {
                                Literal LitDropdown = (Literal)e.Item.FindControl("LitDropdown");
                                if (LitDropdown != null)
                                {
                                    LitDropdown.Text = Convert.ToString(dtDropDown.Rows[0]["TextFieldHeadingValue1"]);
                                    string ItemCount = Convert.ToString(dtDropDown.Rows[0]["TextFieldHeadingValue2"]);
                                    string Itemval = Convert.ToString(dtDropDown.Rows[0]["TextFieldHeadingValue3"]);
                                    DropDownList ddlSelection = (DropDownList)e.Item.FindControl("ddlSelection");
                                    if (LitDropdown.Text.ToUpper() == "SELECT ACCESS TYPE")// || LitDropdown.Text.ToUpper() == "SUB MENU(LEVEL-1)")
                                    {
                                        ddlSelection.AutoPostBack = false;
                                    }
                                    else if (LitDropdown.Text.ToUpper() == "SELECT MAIN MENU")
                                    {
                                        ddlSelection.AutoPostBack = true;
                                    }
                                    else
                                    {
                                        ddlSelection.AutoPostBack = true;
                                    }


                                    //****************Off the autopostback for specific page
                                    if (LitpageTitle != null)
                                    {
                                        switch (Convert.ToString(LitpageTitle.Text.ToUpper()))
                                        {
                                            case "NEWS FEED":
                                                ddlSelection.AutoPostBack = false;
                                                break;

                                            default:
                                                break;
                                        }
                                    }



                                    //****************Select list starts here***/
                                    if (ItemCount.Trim().ToUpper() == "SELECT")
                                    {
                                        switch (LitpageTitle.Text)
                                        {
                                            case "Sub Menu(Level-1) Name":
                                                //PageName = "Main Menu";
                                                PageName = "Main Menu";
                                                break;
                                            case "Sub Menu(Level-2)":
                                                if (LitDropdown.Text.ToLower().IndexOf("main menu") > -1)
                                                {
                                                    PageName = "Main Menu";
                                                }
                                                else if (LitDropdown.Text.ToLower().IndexOf("sub menu(level-1)") > -1)
                                                {
                                                    PageName = "Sub Menu(Level-1) Name";
                                                }

                                                break;
                                            case "Manage Images":
                                            case "Manage Videos":
                                                PageName = "Portfolio Category";
                                                break;
                                            case "FAQ":
                                                PageName = "FAQ Category";
                                                break;
                                            case "Catalogue":
                                                PageName = "Store Item Category";
                                                break;
                                            case "Skill Content":
                                                PageName = "Project Category";
                                                break;
                                            case "New Job Application":
                                                PageName = "Project Category";
                                                break;
                                            case "Job":
                                                PageName = "Project Category";
                                                break;
                                            case "Project Category Detail":
                                                PageName = "Project Category";
                                                break;
                                            case "Special Page":
                                                PageName = "Special Button";
                                                break;
                                            default:
                                                break;
                                        }
                                        DataTable dtMenus = null;
                                        if (PageName.ToUpper() == "SUB MENU(LEVEL-1) NAME")
                                        {
                                            dtMenus = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown_SubMenu_Leve1(PageName, MainMenuName);
                                        }
                                        else if (PageName.ToUpper() == "SPECIAL BUTTON")
                                        {
                                            dtMenus = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown_forSpecialPage(PageName);
                                        }
                                        else
                                        {
                                            dtMenus = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown(PageName);
                                        }

                                        if (dtMenus != null && dtMenus.Rows.Count > 0)
                                        {
                                            ddlSelection.DataSource = dtMenus;
                                            ddlSelection.DataValueField = "FieldValue";
                                            ddlSelection.DataTextField = "FieldValue";
                                            ddlSelection.DataBind();
                                            if (LitDropdown.Text.ToUpper() == "SELECT ACCESS TYPE")
                                            {

                                            }
                                            else if (LitDropdown.Text.ToUpper() == "SELECT MAIN MENU")
                                            {
                                                hdnDropDownListVal.Value = Convert.ToString(ddlSelection.SelectedValue);
                                                MainMenuName = Convert.ToString(dtMenus.Rows[0]["FieldValue"]);
                                            }
                                            else
                                            {
                                                hdnDropDownListVal.Value = Convert.ToString(ddlSelection.SelectedValue);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(Itemval))
                                        {
                                            string[] ItemData = Itemval.Split('~');
                                            if (ItemData.Length > 0)
                                            {
                                                for (Int32 i = 0; i < ItemData.Length; i++)
                                                {
                                                    ddlSelection.Items.Add(new ListItem(ItemData[i].ToString()));
                                                }
                                                if (LitDropdown.Text.ToUpper() == "SELECT ACCESS TYPE")
                                                {

                                                }
                                                else
                                                {
                                                    hdnDropDownListVal.Value = ItemData[0].ToString();
                                                }
                                            }
                                        }
                                    }

                                    if (Request.QueryString["CMSID"] != null)
                                    {
                                        string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                        if (!string.IsNullOrEmpty(Fieldvalue))
                                        {
                                            ddlSelection.SelectedValue = Fieldvalue.Trim();
                                            if (LitDropdown.Text.ToUpper() == "SELECT ACCESS TYPE")
                                            {

                                            }
                                            else if (LitDropdown.Text == "Select Main Menu")
                                            {
                                                hdnDropDownListVal.Value = Fieldvalue.Trim();
                                                MainMenuName = Fieldvalue.Trim();
                                            }
                                            else if (LitDropdown.Text == "Sub Menu(Level-1)")
                                            {
                                                Int32 DisplayOrder = 0;
                                                hdnDropDownListValLevel1.Value = Fieldvalue.Trim();
                                                if (!string.IsNullOrEmpty(hdnDropDownListValLevel1.Value) && !(hdnDropDownListValLevel1.Value.ToUpper().Equals("INTERNAL") || hdnDropDownListValLevel1.Value.ToUpper().Equals("PUBLIC")))
                                                {
                                                    DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Content_dorder_ByMenuname_Level2(Convert.ToInt32(Request.QueryString["PID"]), MainMenuName, hdnDropDownListValLevel1.Value) + 1;
                                                    if (CurDisplayOrder == 0)
                                                    {
                                                        CurDisplayOrder = DisplayOrder;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                hdnDropDownListVal.Value = Fieldvalue.Trim();
                                            }
                                        }
                                    }
                                }
                            }

                            DivDropDown.Visible = true;
                            break;


                        case "Empty Space":
                            DivEmptySpace.Visible = true;
                            DivEmptySpace.Style.Add(HtmlTextWriterStyle.Height, "10px");
                            break;

                        case "Upload Image":
                            DivUpload.Visible = true;

                            DataTable dtImage = Get_Data_For_Section(Convert.ToString(lblPageID.Text), Convert.ToString(lblFieldType.Text), Convert.ToString(lblDorder.Text));
                            if (dtImage != null && dtImage.Rows.Count > 0)
                            {
                                HtmlImage ImgCMSFileUploader = (HtmlImage)e.Item.FindControl("ImgCMSFileUploader");
                                Label lblCMSFileUploader = (Label)e.Item.FindControl("lblCMSFileUploader");
                                Literal LitUploadLabel = (Literal)e.Item.FindControl("LitUploadLabel");
                                if (ImgCMSFileUploader != null && lblCMSFileUploader != null && LitUploadLabel != null)
                                {
                                    LitUploadLabel.Text = Convert.ToString(dtImage.Rows[0]["TextFieldHeadingValue3"]);
                                    if (Request.QueryString["CMSID"] != null && Request.QueryString["PID"] != null && LitpageTitle != null)
                                    {
                                        string Fieldvalue = ServicesFactory.DocCMSServices.Fetch_ContentValue_By_cmsid(Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(lblDorder.Text));
                                        if (!string.IsNullOrEmpty(Fieldvalue))
                                        {
                                            string FolderName = "";
                                            if (pageDrafted)
                                                FolderName = LitpageTitle.Text + "_Draft";
                                            else
                                                FolderName = LitpageTitle.Text;

                                            if (FolderName.Contains(" "))
                                            {
                                                FolderName = FolderName.Replace(" ", "");
                                            }

                                            string[] ext = Fieldvalue.Split('.');
                                            string FileFormat = ext[1].ToLower();
                                            if (FileFormat == "pdf")
                                            {

                                                ImgCMSFileUploader.Src = "../UploadedFiles/pdf.png";

                                            }
                                            else if (FileFormat == "doc" || FileFormat == "txt" || FileFormat == "ods")
                                            {
                                                ImgCMSFileUploader.Src = "../UploadedFiles/doc.png";
                                            }
                                            else if (FileFormat == "xls" || FileFormat == "cvc")
                                            {
                                                ImgCMSFileUploader.Src = "../UploadedFiles/xls.png";
                                            }
                                            else if (FileFormat == "swf")
                                            {
                                                ImgCMSFileUploader.Src = "../UploadedFiles/swf.png";
                                            }
                                            else
                                            {
                                                string serverpath = @"../UploadedFiles/ContentImages/" + FolderName + "/" + Fieldvalue;
                                                ImgCMSFileUploader.Src = serverpath;

                                            }
                                            ImgCMSFileUploader.Style.Add("margin-top", "-12px");
                                            lblCMSFileUploader.Text = Fieldvalue;

                                        }
                                        else
                                        {

                                            ImgCMSFileUploader.Src = "";
                                        }
                                    }
                                    
                                }

                            }
                            break;

                        default:
                            break;
                    }
                    //}
                }
            }

        }

        private void CheckSetDraft()
        {
            bool IsDrafted = false;
            if (Request.QueryString["CMSID"] != null && Request.QueryString["PID"] != null)
            {
                IsDrafted = ServicesFactory.DocCMSServices.Is_Content_Page_Drafted(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToInt32(Request.QueryString["CMSID"]));
                ViewState["NewDraft"] = false;
            }
            else
            {
                ViewState["NewDraft"] = true;
            }
            if (IsDrafted)
            {
                btncontinue.Text = "Update Draft & Continue";
                btnupdatedraft.Text = "Update Draft";
                btnpublishdraft.Visible = true;
                btnremovedraft.Visible = true;
            }
            else
            {
                btncontinue.Text = "Save Draft & Continue";
                btnupdatedraft.Text = "Save Draft";
                btnpublishdraft.Visible = false;
                btnremovedraft.Visible = false;
            }
            if (Convert.ToInt32(Session["RoleID"]) == 7 || Convert.ToInt32(Session["RoleID"])==17)
            {
                btnpublishdraft.Visible = false;
                btnSendForApproval.Visible = true;
            }
        }

        protected void Bind_DisplayOrder(Int32 PageID)
        {
            try
            {
                DataTable dtDummyDorder = new DataTable();
                dtDummyDorder.Columns.Add("CMSID", typeof(string));
                dtDummyDorder.Columns.Add("ContentName", typeof(string));
                dtDummyDorder.Columns.Add("DisplayOrder", typeof(string));
                dtDummyDorder.Columns.Add("IsActive", typeof(string));
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Main_Menu_Dashboard_Bypagename(PageID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        DataTable dtContent = ServicesFactory.DocCMSServices.Fetch_Contentdisplayorder_By_cmsid(Convert.ToInt32(dt.Rows[i]["CMSID"]));
                        if (dtContent != null && dtContent.Rows.Count > 0)
                        {
                            dtDummyDorder.Rows.Add(Convert.ToString(dtContent.Rows[0]["CMSID"]), Convert.ToString(dtContent.Rows[0]["ContentValue"]), Convert.ToString(dtContent.Rows[0]["DisplayOrder"]), Convert.ToString(dtContent.Rows[0]["IsActive"])); ;
                        }
                    }
                }
                RptrDisplayOrder.DataSource = dtDummyDorder;
                RptrDisplayOrder.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrDisplayOrder_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                string lnkPageName = LitpageTitle.Text;
                string PageId = Convert.ToString(Request.QueryString["PID"]);
                Label lblActive = (Label)e.Item.FindControl("lblActive");
                HtmlTableCell Activestatusdata = (HtmlTableCell)e.Item.FindControl("Activestatusdata");
                if (lnkPageName.ToUpper() != "" || lnkPageName.ToUpper() != null)
                {
                    string OriginalStatus = "";
                    DataTable dtStatus = ServicesFactory.DocCMSServices.Fetch_displayorder_Bycmsid(Convert.ToInt32(PageId), Convert.ToInt32(lblCMSID.Text));
                    if (dtStatus != null && dtStatus.Rows.Count > 0)
                    {

                        OriginalStatus = Convert.ToString(dtStatus.Rows[0]["IsActive"]);
                    }
                    lblActive.Text = OriginalStatus;
                }
                else
                {
                    ActiveStatusheading.Visible = false;
                    Activestatusdata.Visible = false;
                }
            }
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_ContentManagement> lstContent = new List<Cls_ContentManagement>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblCMSID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblCMSID");
                Label lblContentName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblContentName");
                if (Session["UserID"] != null && lblContentName != null && lblCMSID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblContentName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["PID"]));
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["PID"]));
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["PID"]));
                    return;
                }
            }
            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_ContentManagement objContent = new Cls_ContentManagement();
                Label lblCMSID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblCMSID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblCMSID != null)
                {
                    objContent.cmsid = Convert.ToInt32(lblCMSID.Text.Trim());
                    objContent.displayorder = Convert.ToString(txtDorder.Value);
                    objContent.lastmodifiedby = Guid.Parse(UserId);
                    lstContent.Add(objContent);
                }
            }
            if (lstContent != null && lstContent.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_Content_displayorder(lstContent);
                if (retVal > 0)
                {

                    if (Request.QueryString["PID"] != null && Request.QueryString["CMSID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["PID"] + "," + Request.QueryString["CMSID"] + ");</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg1(" + Request.QueryString["PID"] + ");</script>");
                        Bind_DisplayOrder(Convert.ToInt32(Request.QueryString["PID"]));
                    }
                }
            }
        }

        protected void ddlSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool runEventCode = true;
            foreach (string ctl in Page.Request.Form)
            {
                Control c = Page.FindControl(ctl);
                if (c is Literal)
                {
                    runEventCode = false;
                    break;
                }
                else if (c is Button)
                {
                    runEventCode = false;
                    break;
                }
            }
            if (runEventCode)
            {
                //Event handler code here
                DropDownList ddlSelection = (DropDownList)sender;
                DropDownList ddlSelectionSubMenu = null;
                string selectedValue = ddlSelection.SelectedValue;
                Int32 DisplayOrder = 0;
                if (LitpageTitle.Text == "Sub Menu(Level-2)")
                {
                    if (((Literal)(ddlSelection.Parent).FindControl("LitDropDown")).Text == "Select Main Menu")
                    {
                        MainMenuName = selectedValue;
                        ddlSelectionSubMenu = (DropDownList)RptrContentMgmt.Items[1].FindControl("ddlSelection");
                        if (ddlSelectionSubMenu != null)
                        {
                            ddlSelectionSubMenu.DataSource = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown_SubMenu_Leve1("Sub Menu(Level-1) Name", MainMenuName);
                            ddlSelectionSubMenu.DataValueField = "FieldValue";
                            ddlSelectionSubMenu.DataTextField = "FieldValue";
                            ddlSelectionSubMenu.DataBind();
                        }
                        if (!string.IsNullOrEmpty(selectedValue) && !(selectedValue.ToUpper().Equals("INTERNAL") || selectedValue.ToUpper().Equals("PUBLIC")))
                        {
                            DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Content_dorder_ByMenuname_Level2(Convert.ToInt32(Request.QueryString["PID"]), ddlSelection.SelectedValue, ddlSelectionSubMenu.SelectedValue) + 1;
                        }
                        for (Int32 i = 0; i < RptrContentMgmt.Items.Count; i++)
                        {
                            Label lblDorderType = (Label)RptrContentMgmt.Items[i].FindControl("lblDorderType");
                            Literal LitDropdown = (Literal)RptrContentMgmt.Items[i].FindControl("LitDropdown");
                            if (lblDorderType != null && lblDorderType.Text == "DisplayOrder" && LitDropdown != null)
                            {

                                HtmlInputText txtContainer = (HtmlInputText)RptrContentMgmt.Items[i].FindControl("txtContainer");
                                if (DisplayOrder != 0)
                                    txtContainer.Value = Convert.ToString(DisplayOrder);
                            }
                        }
                    }
                    else if (((Literal)(ddlSelection.Parent).FindControl("LitDropDown")).Text == "Sub Menu(Level-1)")
                    {
                        if (!string.IsNullOrEmpty(selectedValue) && !(selectedValue.ToUpper().Equals("INTERNAL") || selectedValue.ToUpper().Equals("PUBLIC")))
                        {
                            ddlSelectionSubMenu = (DropDownList)RptrContentMgmt.Items[0].FindControl("ddlSelection");
                            MainMenuName = ddlSelectionSubMenu.SelectedValue;
                            DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Content_dorder_ByMenuname_Level2(Convert.ToInt32(Request.QueryString["PID"]), MainMenuName, ddlSelection.SelectedValue) + 1;
                        }
                        for (Int32 i = 0; i < RptrContentMgmt.Items.Count; i++)
                        {
                            Label lblDorderType = (Label)RptrContentMgmt.Items[i].FindControl("lblDorderType");
                            Literal LitDropdown = (Literal)RptrContentMgmt.Items[i].FindControl("LitDropdown");
                            if (lblDorderType != null && lblDorderType.Text == "DisplayOrder" && LitDropdown != null)
                            {

                                HtmlInputText txtContainer = (HtmlInputText)RptrContentMgmt.Items[i].FindControl("txtContainer");
                                if (DisplayOrder != 0)
                                    txtContainer.Value = Convert.ToString(DisplayOrder);
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(selectedValue) && !(selectedValue.ToUpper().Equals("INTERNAL") || selectedValue.ToUpper().Equals("PUBLIC")))
                    {
                        DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_Content_dorder_ByMenuname(Convert.ToInt32(Request.QueryString["PID"]), selectedValue) + 1;
                    }
                    for (Int32 i = 0; i < RptrContentMgmt.Items.Count; i++)
                    {
                        Label lblDorderType = (Label)RptrContentMgmt.Items[i].FindControl("lblDorderType");
                        Literal LitDropdown = (Literal)RptrContentMgmt.Items[i].FindControl("LitDropdown");
                        if (lblDorderType != null && lblDorderType.Text == "DisplayOrder" && LitDropdown != null)
                        {

                            HtmlInputText txtContainer = (HtmlInputText)RptrContentMgmt.Items[i].FindControl("txtContainer");
                            if (DisplayOrder != 0)
                                txtContainer.Value = Convert.ToString(DisplayOrder);
                        }
                    }
                }
            }
        }

        protected void btncontinue_click(object sender, EventArgs e)
        {
            string ModifyBY = Convert.ToString(Session["UserID"]);
            string retval = Save_Draft_Data(ModifyBY);
            int RetvalNum;
            bool Is_ResultNumric = int.TryParse(retval, out RetvalNum);
            if (Is_ResultNumric == true)
            {
                Response.Redirect("../cadmin/ContentManagement.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]) + "&CMSID=" + retval);
            }
        }

        protected void btnSendForApproval_click(object sender, EventArgs e)
        {

            string ModifyBY = "EEEEEEEE-EEEE-EEEE-EEEE-EEEEEEEEEEEE";
            string retval = Save_Draft_Data(ModifyBY);
            int RetvalNum;
            bool Is_ResultNumric = int.TryParse(retval, out RetvalNum);
            if (Is_ResultNumric == true)
            {
                Response.Redirect("../cadmin/ContentManagementDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]));
            }
        }

        protected void btnupdatedraft_click(object sender, EventArgs e)
        {
            string ModifyBY = Convert.ToString(Session["UserID"]);
            string retval = Save_Draft_Data(ModifyBY);
            int RetvalNum;
            bool Is_ResultNumric = int.TryParse(retval, out RetvalNum);
            if (Is_ResultNumric == true)
            {
                Response.Redirect("../cadmin/ContentManagementDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]));
            }
        }

        private string Save_Draft_Data(string LastModifiedBy)
        {
            string RetValcontent = "";
            try
            {
                PageName = LitpageTitle.Text.Trim();
                string DropDownSelectedVal = "";
                List<Cls_ContentManagement> lstCMS = new List<Cls_ContentManagement>();
                Cls_ContentManagement objCMS = new Cls_ContentManagement();
                objCMS.pageid = Convert.ToInt32(Request.QueryString["PID"]);
                if (Request.QueryString["CMSID"] != null)
                {
                    objCMS.cmsid = Convert.ToInt32(Request.QueryString["CMSID"]);
                }
                else
                {
                    objCMS.cmsid = 0;
                }
                objCMS.pagename = LitpageTitle.Text.Trim();
                if (Session["UserID"] != null)
                {
                    objCMS.createdby = Guid.Parse(Convert.ToString(Session["UserID"]));
                    objCMS.lastmodifiedby = Guid.Parse(LastModifiedBy);
                }
                lstCMS.Add(objCMS);
                for (Int32 i = 0; i < RptrContentMgmt.Items.Count; i++)
                {
                    objCMS = new Cls_ContentManagement();
                    if (RptrContentMgmt.Items != null)
                    {
                        Label lblFieldType = (Label)RptrContentMgmt.Items[i].FindControl("lblFieldType");
                        Label lblDorder = (Label)RptrContentMgmt.Items[i].FindControl("lblDorder");
                        Label lblRequired = (Label)RptrContentMgmt.Items[i].FindControl("lblRequired");
                        Label lblDefaultText = (Label)RptrContentMgmt.Items[i].FindControl("lblDefaultText");

                        // Text Editor Single Line Text, Single Line Text,Full Name,First Name,Last Name, Email Address,Company,Phone Number,Address 1,Address 2,City,State,Zip,Custom 1,Custom 2,Custom 3
                        HtmlInputText txtContainer = (HtmlInputText)RptrContentMgmt.Items[i].FindControl("txtContainer");
                        HtmlGenericControl spnReqTexBox = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                        Literal LitLabelText = (Literal)RptrContentMgmt.Items[i].FindControl("LitLabelText");

                        // Text Editor
                        HtmlTextArea elm1 = (HtmlTextArea)RptrContentMgmt.Items[i].FindControl("elm1");

                        HtmlGenericControl spnReqtextarea = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                        Literal LitTextEditor = (Literal)RptrContentMgmt.Items[i].FindControl("LitTextEditor");

                        // MultiLine , Paragraph Text
                        HtmlTextArea txtMultiLine = (HtmlTextArea)RptrContentMgmt.Items[i].FindControl("txtMultiLine");
                        Literal LitMultiLine = (Literal)RptrContentMgmt.Items[i].FindControl("LitMultiLine");

                        // CheckBox
                        CheckBoxList chkList = (CheckBoxList)RptrContentMgmt.Items[i].FindControl("chkList");
                        Literal lblSingleChoice = (Literal)RptrContentMgmt.Items[i].FindControl("lblSingleChoice");

                        // Multiple Choice
                        RadioButtonList rbtnList = (RadioButtonList)RptrContentMgmt.Items[i].FindControl("rbtnList");
                        Literal LiRadioButton = (Literal)RptrContentMgmt.Items[i].FindControl("LiRadioButton");

                        // List Box
                        ListBox LstSelection = (ListBox)RptrContentMgmt.Items[i].FindControl("LstSelection");

                        // Dropdown
                        DropDownList ddlSelection = (DropDownList)RptrContentMgmt.Items[i].FindControl("ddlSelection");
                        Literal LitDropdown = (Literal)RptrContentMgmt.Items[i].FindControl("LitDropdown");

                        // Upload Image
                        FileUpload CMSFileUploader = (FileUpload)RptrContentMgmt.Items[i].FindControl("CMSFileUploader");
                        Label lblCMSFileUploader = (Label)RptrContentMgmt.Items[i].FindControl("lblCMSFileUploader");
                        Literal LitUploadLabel = (Literal)RptrContentMgmt.Items[i].FindControl("LitUploadLabel");
                        if (lblFieldType != null && lblDorder != null && lblRequired != null && lblDefaultText != null)
                        {
                            switch (lblFieldType.Text.Trim())
                            {
                                case "Single Line Text":
                                case "Full Name":
                                case "First Name":
                                case "Last Name":
                                case "Email Address":
                                case "Company":
                                case "Phone Number":
                                case "Address 1":
                                case "Address 2":
                                case "City":
                                case "State":
                                case "Zip":
                                case "Custom 1":
                                case "Custom 2":
                                case "Custom 3":
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    objCMS.fieldtype = lblFieldType.Text.Trim();

                                    if (txtContainer != null && spnReqTexBox != null && LitLabelText != null)
                                    {
                                        bool CkhExisting = false;
                                        if (i == 0 && (LitLabelText.Text.ToUpper().Contains("NAME")))
                                        {
                                            if (Request.QueryString["PID"] != null && Request.QueryString["CMSID"] != null)
                                            {

                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                            else
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                        else
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                            objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                            lstCMS.Add(objCMS);
                                        }
                                    }
                                    break;

                                case "TextEditor":
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    if (elm1 != null && spnReqtextarea != null && LitTextEditor != null)
                                    {
                                        if (string.IsNullOrEmpty(elm1.Value.Trim()) || elm1.Value.Trim() == lblDefaultText.Text.Trim())
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitTextEditor.Text.Trim().Replace("'", "''");
                                            objCMS.fieldvalue = "";
                                            lstCMS.Add(objCMS);
                                        }
                                        else
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitTextEditor.Text.Trim().Replace("'", "''");
                                            objCMS.fieldvalue = elm1.Value.Trim().Replace("'", "''");
                                            lstCMS.Add(objCMS);
                                        }
                                    }
                                    break;

                                case "Multi Line Text":
                                case "Paragraph Text":
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    if (txtMultiLine != null && spnReqtextarea != null && LitMultiLine != null)
                                    {
                                        if (string.IsNullOrEmpty(txtMultiLine.Value.Trim()) || txtMultiLine.Value.Trim() == lblDefaultText.Text.Trim())
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitMultiLine.Text.Trim().Replace("'", "''");
                                            objCMS.fieldvalue = "";
                                            lstCMS.Add(objCMS);
                                        }
                                        else
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitMultiLine.Text.Trim().Replace("'", "''");
                                            objCMS.fieldvalue = txtMultiLine.Value.Trim().Replace("'", "''");
                                            lstCMS.Add(objCMS);
                                        }
                                    }
                                    break;

                                case "Checkbox":
                                    objCMS.fieldtype = "Checkbox";
                                    string CheckBoxval = "";
                                    string CheckText = "";
                                    if (chkList != null && lblSingleChoice != null)
                                    {
                                        if (lblSingleChoice.Text == "Set as Default Page")
                                        {
                                            for (Int32 j = 0; j < chkList.Items.Count; j++)
                                            {
                                                if (chkList.Items[j].Selected == true)
                                                {
                                                    Int32 retval = ServicesFactory.DocCMSServices.Update_Default_Check(Convert.ToInt32(Request.QueryString["PID"]));
                                                    CheckBoxval = "True";
                                                }
                                                else
                                                {
                                                    CheckBoxval = "False";
                                                }

                                                if (CheckBoxval.StartsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(1);
                                                }
                                                if (CheckBoxval.EndsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                                }
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = lblSingleChoice.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = CheckBoxval;
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                        else if (lblSingleChoice.Text.ToUpper().Trim() == "COUNTRY NAME")
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = lblSingleChoice.Text.Trim().Replace("'", "''");
                                            for (Int32 j = 0; j < chkList.Items.Count; j++)
                                            {
                                                if (chkList.Items[j].Selected == true)
                                                {
                                                    CheckBoxval += "," + chkList.Items[j].Text;
                                                }
                                            }
                                            if (CheckBoxval.StartsWith(","))
                                            {
                                                CheckBoxval = CheckBoxval.Substring(1);
                                            }
                                            if (CheckBoxval.EndsWith(","))
                                            {
                                                CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                            }
                                            objCMS.fieldvalue = CheckBoxval;
                                            lstCMS.Add(objCMS);
                                        }
                                        else
                                        {
                                            if (chkList.Items.Count > 1)
                                            {

                                                for (Int32 j = 0; j < chkList.Items.Count; j++)
                                                {
                                                    if (chkList.Items[j].Selected == true)
                                                    {
                                                        CheckBoxval = "True";
                                                        CheckText = chkList.Items[j].Text;
                                                    }
                                                    else
                                                    {
                                                        CheckBoxval = "False";
                                                        CheckText = chkList.Items[j].Text;
                                                    }

                                                    if (CheckBoxval.StartsWith(","))
                                                    {
                                                        CheckBoxval = CheckBoxval.Substring(1);
                                                    }
                                                    if (CheckBoxval.EndsWith(","))
                                                    {
                                                        CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                                    }
                                                    objCMS = new Cls_ContentManagement();
                                                    objCMS.fieldtype = "Checkbox";
                                                    objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                    objCMS.fieldtext = CheckText;
                                                    objCMS.fieldvalue = CheckBoxval;
                                                    lstCMS.Add(objCMS);
                                                }
                                            }
                                            else
                                            {

                                                if (chkList.Items[0].Selected == true)
                                                {
                                                    CheckBoxval = "True";
                                                }
                                                else
                                                {
                                                    CheckBoxval = "False";
                                                }

                                                if (CheckBoxval.StartsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(1);
                                                }
                                                if (CheckBoxval.EndsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                                }
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = lblSingleChoice.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = CheckBoxval;
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    break;

                                case "MultipleChoice":
                                    objCMS.fieldtype = "MultipleChoice";
                                    string RadioVal = "";
                                    if (rbtnList != null && LiRadioButton != null)
                                    {
                                        if (LiRadioButton.Text == "Is Active")
                                        {
                                            for (Int32 j = 0; j < rbtnList.Items.Count; j++)
                                            {
                                                if (rbtnList.Items[j].Selected == true)
                                                {
                                                    RadioVal = "True";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (Int32 k = 0; k < rbtnList.Items.Count; k++)
                                            {
                                                if (rbtnList.Items[k].Selected == true)
                                                {
                                                    RadioVal = "," + rbtnList.Items[k].Value;
                                                }
                                            }
                                        }
                                    }
                                    if (RadioVal.StartsWith(","))
                                    {
                                        RadioVal = RadioVal.Substring(1);
                                    }
                                    if (RadioVal.EndsWith(","))
                                    {
                                        RadioVal = RadioVal.Substring(0, RadioVal.Length - 1);
                                    }
                                    objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                    objCMS.fieldtext = LiRadioButton.Text.Trim().Replace("'", "''");
                                    objCMS.fieldvalue = RadioVal.Trim().Replace("'", "''");
                                    lstCMS.Add(objCMS);
                                    break;
                                case "ListBox":
                                    objCMS.fieldtype = "ListBox";
                                    string ListBoxVal = "";
                                    if (lblDorder != null)
                                    {
                                        ListBoxVal = hdnListBoxVal.Value;
                                    }
                                    objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                    objCMS.fieldtext = Convert.ToString(LstSelection.SelectedItem);
                                    objCMS.fieldvalue = LstSelection.SelectedValue;
                                    lstCMS.Add(objCMS);
                                    break;

                                case "Dropdown":
                                    objCMS.fieldtype = "Dropdown";
                                    string DropDownVal = "";
                                    if (lblDorder != null)
                                    {
                                        DropDownVal = hdnDropDownListVal.Value;
                                    }
                                    if (ddlSelection != null && DropDownVal != null && LitDropdown != null)
                                    {
                                        objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                        objCMS.fieldtext = LitDropdown.Text.Trim();
                                        objCMS.fieldvalue = ddlSelection.SelectedValue;
                                        DropDownSelectedVal = ddlSelection.SelectedValue;
                                    }
                                    lstCMS.Add(objCMS);
                                    break;
                                case "Upload Image":

                                    string fimagename = "";
                                    string FolderName = "";
                                    string serverpath = "";
                                    DirectoryInfo directoryInfo = null;
                                    bool isDirCreated = true;
                                    FolderName = LitpageTitle.Text + "_Draft";
                                    if (FolderName.Contains(" "))
                                    {
                                        FolderName = FolderName.Replace(" ", "");
                                    }
                                    if (CMSFileUploader != null && lblCMSFileUploader != null)
                                    {
                                        if (CMSFileUploader.PostedFile != null && !string.IsNullOrEmpty(CMSFileUploader.PostedFile.FileName) && CMSFileUploader.HasFile && lblCMSFileUploader != null && LitUploadLabel != null)
                                        {
                                            fimagename = CMSFileUploader.PostedFile.FileName;
                                            if (fimagename.Contains(" "))
                                            {
                                                fimagename = fimagename.Replace(" ", "");
                                            }
                                            serverpath = Server.MapPath("../UploadedFiles/ContentImages/" + FolderName + "/");
                                            directoryInfo = new DirectoryInfo(serverpath);
                                            isDirCreated = directoryInfo.Exists;
                                            if (!isDirCreated)
                                            {
                                                directoryInfo.Create();
                                            }
                                            CMSFileUploader.PostedFile.SaveAs(serverpath + @"\\" + fimagename);
                                            objCMS.fieldtype = lblFieldType.Text.Trim();
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitUploadLabel.Text;
                                            objCMS.fieldvalue = fimagename;
                                            lstCMS.Add(objCMS);
                                        }
                                        else
                                        {
                                            objCMS.fieldtype = lblFieldType.Text.Trim();
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitUploadLabel.Text;
                                            objCMS.fieldvalue = lblCMSFileUploader.Text;
                                            if (!string.IsNullOrEmpty(lblCMSFileUploader.Text))
                                            {
                                                serverpath = Server.MapPath("../UploadedFiles/ContentImages/" + FolderName + "/");
                                                directoryInfo = new DirectoryInfo(serverpath);
                                                isDirCreated = directoryInfo.Exists;
                                                if (!isDirCreated)
                                                {
                                                    directoryInfo.Create();
                                                }
                                                if (!File.Exists(serverpath + lblCMSFileUploader.Text))
                                                {
                                                    File.Copy(serverpath.Replace("_Draft", "") + lblCMSFileUploader.Text, serverpath + lblCMSFileUploader.Text);
                                                }
                                            }
                                            lstCMS.Add(objCMS);
                                        }
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                }
                if (lstCMS != null && lstCMS.Count > 0)
                {
                    if (LastModifiedBy == null || LastModifiedBy == "")
                    {
                        LastModifiedBy = Convert.ToString(Session["UserID"]);
                    }
                    RetValcontent = ServicesFactory.DocCMSServices.Insert_CMS_Data_Draft(lstCMS, LastModifiedBy);
                }
                return RetValcontent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnremovedraft_click(object sender, EventArgs e)
        {
            try
            {
                string retval = ServicesFactory.DocCMSServices.Remove_CMS_Data_Draft(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToInt32(Request.QueryString["CMSID"]));
                if (string.IsNullOrEmpty(retval))
                {
                    if (Convert.ToInt32(Request.QueryString["CMSID"]) >= 99999999)
                    {
                        Response.Redirect("../cadmin/ContentManagementDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]));
                    }
                    else
                    {
                        Response.Redirect("../cadmin/ContentManagement.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]) + "&CMSID=" + Convert.ToString(Request.QueryString["CMSID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (LitpageTitle.Text.ToUpper().Contains("CATALOGUE"))
            {
                Response.Redirect("~/cadmin/CatalogueDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]) + "");
            }
            else
            {
                Response.Redirect("~/cadmin/ContentManagementDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]) + "");
            }
        }

        // FileUploader Click Button Event
        protected void Upload_Click(object sender, EventArgs e)
        {
            FileUpload FileUploader = (FileUpload)RptrContentMgmt.FindControl("FileUploader");
            HtmlImage ImgUpload = (HtmlImage)RptrContentMgmt.FindControl("ImgUpload");
            ImgUpload.Src = "../UploadedFiles/" + FileUploader.FileName;
        }

        protected void btnpublishdraft_click(object sender, EventArgs e)
        {
            try
            {
                PageName = LitpageTitle.Text.Trim();
                bool isPageEntryValidated = true;
                string DropDownSelectedVal = "";
                List<Cls_ContentManagement> lstCMS = new List<Cls_ContentManagement>();
                Cls_ContentManagement objCMS = new Cls_ContentManagement();
                objCMS.pageid = Convert.ToInt32(Request.QueryString["PID"]);
                objCMS.pagename = LitpageTitle.Text.Trim();
                if (Session["UserID"] != null)
                {
                    objCMS.createdby = Guid.Parse(Convert.ToString(Session["UserID"]));
                    objCMS.lastmodifiedby = Guid.Parse(Convert.ToString(Session["UserID"]));
                }
                lstCMS.Add(objCMS);
                for (Int32 i = 0; i < RptrContentMgmt.Items.Count; i++)
                {
                    objCMS = new Cls_ContentManagement();
                    if (RptrContentMgmt.Items != null)
                    {
                        Label lblFieldType = (Label)RptrContentMgmt.Items[i].FindControl("lblFieldType");
                        Label lblDorder = (Label)RptrContentMgmt.Items[i].FindControl("lblDorder");
                        Label lblRequired = (Label)RptrContentMgmt.Items[i].FindControl("lblRequired");
                        Label lblDefaultText = (Label)RptrContentMgmt.Items[i].FindControl("lblDefaultText");
                        bool ValFlag = true;
                        string ValMsg = "Please complete the entries:<br />";
                        Int32 ValCounter = 1;
                        if (lblFieldType != null && lblDorder != null && lblRequired != null && lblDefaultText != null)
                        {
                            switch (lblFieldType.Text.Trim())
                            {
                                case "Single Line Text":
                                case "Full Name":
                                case "First Name":
                                case "Last Name":
                                case "Email Address":
                                case "Company":
                                case "Phone Number":
                                case "Address 1":
                                case "Address 2":
                                case "City":
                                case "State":
                                case "Zip":
                                case "Custom 1":
                                case "Custom 2":
                                case "Custom 3":
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    if (lblRequired.Text == "TRUE")
                                    {
                                        HtmlInputText txtContainer = (HtmlInputText)RptrContentMgmt.Items[i].FindControl("txtContainer");
                                        HtmlGenericControl spnReqTexBox = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                                        Literal LitLabelText = (Literal)RptrContentMgmt.Items[i].FindControl("LitLabelText");
                                        if (txtContainer != null && spnReqTexBox != null && LitLabelText != null)
                                        {
                                            if (string.IsNullOrEmpty(txtContainer.Value.Trim()) || txtContainer.Value.Trim() == lblDefaultText.Text.Trim())
                                            {
                                                spnReqTexBox.Visible = true;
                                                isPageEntryValidated = false;
                                            }
                                            else
                                            {
                                                bool CkhExisting = false;

                                                if (i == 0 && (LitLabelText.Text.ToUpper().Contains("NAME")))
                                                {
                                                    if (Request.QueryString["PID"] != null && Request.QueryString["CMSID"] != null)
                                                    {
                                                        CkhExisting = ServicesFactory.DocCMSServices.Check_Existing_Menu_For_Update_Bycmsid(txtContainer.Value.Trim().ToUpper(), Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(Request.QueryString["PID"]));
                                                        if (CkhExisting == true)
                                                        {
                                                            ClientScript.RegisterStartupScript(this.GetType(), "msgbox", "<script>alert('" + LitLabelText.Text.ToLower() + "  which you have provided already exist');</script>");
                                                            txtContainer.Focus();
                                                            return;
                                                        }
                                                        else
                                                        {
                                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                            objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                            objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                                            lstCMS.Add(objCMS);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        CkhExisting = ServicesFactory.DocCMSServices.Check_Existing_Menu_For_Save(txtContainer.Value.Trim().ToUpper(), Convert.ToInt32(Request.QueryString["PID"]));
                                                        if (CkhExisting == true)
                                                        {
                                                            ClientScript.RegisterStartupScript(this.GetType(), "msgbox", "<script>alert('" + LitLabelText.Text.ToLower() + " which you have provided already exist');</script>");
                                                            txtContainer.Focus();
                                                            return;
                                                        }
                                                        else
                                                        {
                                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                            objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                            objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                                            lstCMS.Add(objCMS);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                    objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                    objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                                    lstCMS.Add(objCMS);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        HtmlInputText txtContainer = (HtmlInputText)RptrContentMgmt.Items[i].FindControl("txtContainer");
                                        HtmlGenericControl spnReqTexBox = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                                        Literal LitLabelText = (Literal)RptrContentMgmt.Items[i].FindControl("LitLabelText");
                                        if (txtContainer != null && spnReqTexBox != null && LitLabelText != null)
                                        {
                                            if (string.IsNullOrEmpty(txtContainer.Value.Trim()) || txtContainer.Value.Trim() == lblDefaultText.Text.Trim())
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = "";
                                                lstCMS.Add(objCMS);
                                            }
                                            else
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitLabelText.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = txtContainer.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    break;

                                case "TextEditor":
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    objCMS.fieldtype = lblFieldType.Text.Trim();

                                    if (lblRequired.Text == "TRUE")
                                    {
                                        HtmlTextArea elm1 = (HtmlTextArea)RptrContentMgmt.Items[i].FindControl("elm1");
                                        HtmlGenericControl spnReqtextarea = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                                        Literal LitTextEditor = (Literal)RptrContentMgmt.Items[i].FindControl("LitTextEditor");
                                        if (elm1 != null && spnReqtextarea != null && LitTextEditor != null)
                                        {
                                            if (string.IsNullOrEmpty(elm1.Value.Trim()) || elm1.Value.Trim() == lblDefaultText.Text.Trim())
                                            {
                                                spnReqtextarea.Visible = true;
                                                isPageEntryValidated = false;
                                            }
                                            else
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitTextEditor.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = elm1.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    else
                                    {

                                        HtmlTextArea elm1 = (HtmlTextArea)RptrContentMgmt.Items[i].FindControl("elm1");
                                        HtmlGenericControl spnReqtextarea = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                                        Literal LitTextEditor = (Literal)RptrContentMgmt.Items[i].FindControl("LitTextEditor");
                                        if (elm1 != null && spnReqtextarea != null && LitTextEditor != null)
                                        {
                                            if (string.IsNullOrEmpty(elm1.Value.Trim()) || elm1.Value.Trim() == lblDefaultText.Text.Trim())
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitTextEditor.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = "";
                                                lstCMS.Add(objCMS);
                                            }
                                            else
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitTextEditor.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = elm1.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    break;

                                case "Multi Line Text":
                                case "Paragraph Text":
                                    objCMS.fieldtype = lblFieldType.Text.Trim();
                                    if (lblRequired.Text == "TRUE")
                                    {
                                        HtmlTextArea txtMultiLine = (HtmlTextArea)RptrContentMgmt.Items[i].FindControl("txtMultiLine");
                                        HtmlGenericControl spnReqtextarea = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                                        Literal LitMultiLine = (Literal)RptrContentMgmt.Items[i].FindControl("LitMultiLine");
                                        if (txtMultiLine != null && spnReqtextarea != null && LitMultiLine != null)
                                        {
                                            if (string.IsNullOrEmpty(txtMultiLine.Value.Trim()) || txtMultiLine.Value.Trim() == lblDefaultText.Text.Trim())
                                            {
                                                spnReqtextarea.Visible = true;
                                                isPageEntryValidated = false;
                                            }
                                            else
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitMultiLine.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = txtMultiLine.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        HtmlTextArea txtMultiLine = (HtmlTextArea)RptrContentMgmt.Items[i].FindControl("txtMultiLine");
                                        HtmlGenericControl spnReqtextarea = (HtmlGenericControl)RptrContentMgmt.Items[i].FindControl("spnReqTexBox");
                                        Literal LitMultiLine = (Literal)RptrContentMgmt.Items[i].FindControl("LitMultiLine");
                                        if (txtMultiLine != null && spnReqtextarea != null && LitMultiLine != null)
                                        {
                                            if (string.IsNullOrEmpty(txtMultiLine.Value.Trim()) || txtMultiLine.Value.Trim() == lblDefaultText.Text.Trim())
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitMultiLine.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = "";
                                                lstCMS.Add(objCMS);
                                            }
                                            else
                                            {
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = LitMultiLine.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = txtMultiLine.Value.Trim().Replace("'", "''");
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    break;

                                case "Checkbox":
                                    objCMS.fieldtype = "Checkbox";
                                    CheckBoxList chkList = (CheckBoxList)RptrContentMgmt.Items[i].FindControl("chkList");
                                    Literal lblSingleChoice = (Literal)RptrContentMgmt.Items[i].FindControl("lblSingleChoice");
                                    string CheckBoxval = "";
                                    string CheckText = "";
                                    if (chkList != null && lblSingleChoice != null)
                                    {
                                        if (lblSingleChoice.Text == "Set as Default Page")
                                        {
                                            for (Int32 j = 0; j < chkList.Items.Count; j++)
                                            {
                                                if (chkList.Items[j].Selected == true)
                                                {

                                                    Int32 retval = ServicesFactory.DocCMSServices.Update_Default_Check(Convert.ToInt32(Request.QueryString["PID"]));
                                                    CheckBoxval = "True";
                                                }
                                                else
                                                {
                                                    CheckBoxval = "False";
                                                }

                                                if (CheckBoxval.StartsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(1);
                                                }
                                                if (CheckBoxval.EndsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                                }
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = lblSingleChoice.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = CheckBoxval;
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                        else if (lblSingleChoice.Text.ToUpper().Trim() == "COUNTRY NAME")
                                        {
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = lblSingleChoice.Text.Trim().Replace("'", "''");
                                            for (Int32 j = 0; j < chkList.Items.Count; j++)
                                            {
                                                if (chkList.Items[j].Selected == true)
                                                {
                                                    CheckBoxval += "," + chkList.Items[j].Text;
                                                }
                                            }
                                            if (CheckBoxval.StartsWith(","))
                                            {
                                                CheckBoxval = CheckBoxval.Substring(1);
                                            }
                                            if (CheckBoxval.EndsWith(","))
                                            {
                                                CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                            }
                                            objCMS.fieldvalue = CheckBoxval;
                                            lstCMS.Add(objCMS);
                                        }
                                        else
                                        {
                                            if (chkList.Items.Count > 1)
                                            {

                                                for (Int32 j = 0; j < chkList.Items.Count; j++)
                                                {
                                                    if (chkList.Items[j].Selected == true)
                                                    {
                                                        CheckBoxval = "True";
                                                        CheckText = chkList.Items[j].Text;
                                                    }
                                                    else
                                                    {
                                                        CheckBoxval = "False";
                                                        CheckText = chkList.Items[j].Text;
                                                    }
                                                    if (CheckBoxval.StartsWith(","))
                                                    {
                                                        CheckBoxval = CheckBoxval.Substring(1);
                                                    }
                                                    if (CheckBoxval.EndsWith(","))
                                                    {
                                                        CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                                    }
                                                    objCMS = new Cls_ContentManagement();
                                                    objCMS.fieldtype = "Checkbox";
                                                    objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                    objCMS.fieldtext = CheckText;
                                                    objCMS.fieldvalue = CheckBoxval;
                                                    lstCMS.Add(objCMS);
                                                }
                                            }
                                            else
                                            {

                                                if (chkList.Items[0].Selected == true)
                                                {
                                                    CheckBoxval = "True";
                                                }
                                                else
                                                {
                                                    CheckBoxval = "False";
                                                }
                                                if (CheckBoxval.StartsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(1);
                                                }
                                                if (CheckBoxval.EndsWith(","))
                                                {
                                                    CheckBoxval = CheckBoxval.Substring(0, CheckBoxval.Length - 1);
                                                }
                                                objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                                objCMS.fieldtext = lblSingleChoice.Text.Trim().Replace("'", "''");
                                                objCMS.fieldvalue = CheckBoxval;
                                                lstCMS.Add(objCMS);
                                            }
                                        }
                                    }
                                    break;
                                case "MultipleChoice":
                                    objCMS.fieldtype = "MultipleChoice";

                                    RadioButtonList rbtnList = (RadioButtonList)RptrContentMgmt.Items[i].FindControl("rbtnList");
                                    Literal LiRadioButton = (Literal)RptrContentMgmt.Items[i].FindControl("LiRadioButton");
                                    string RadioVal = "";
                                    if (rbtnList != null && LiRadioButton != null)
                                    {
                                        if (LiRadioButton.Text == "Is Active")
                                        {
                                            for (Int32 j = 0; j < rbtnList.Items.Count; j++)
                                            {
                                                if (rbtnList.Items[j].Selected == true)
                                                {
                                                    RadioVal = "True";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (Int32 k = 0; k < rbtnList.Items.Count; k++)
                                            {
                                                if (rbtnList.Items[k].Selected == true)
                                                {
                                                    RadioVal = "," + rbtnList.Items[k].Value;
                                                }
                                            }
                                        }
                                    }
                                    if (RadioVal.StartsWith(","))
                                    {
                                        RadioVal = RadioVal.Substring(1);
                                    }
                                    if (RadioVal.EndsWith(","))
                                    {
                                        RadioVal = RadioVal.Substring(0, RadioVal.Length - 1);
                                    }
                                    objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                    objCMS.fieldtext = LiRadioButton.Text.Trim().Replace("'", "''");
                                    objCMS.fieldvalue = RadioVal.Trim().Replace("'", "''");
                                    lstCMS.Add(objCMS);
                                    break;


                                case "ListBox":
                                    objCMS.fieldtype = "ListBox";
                                    ListBox LstSelection = (ListBox)RptrContentMgmt.Items[i].FindControl("LstSelection");
                                    string ListBoxVal = "";
                                    if (lblDorder != null)
                                    {
                                        ListBoxVal = hdnListBoxVal.Value;
                                    }

                                    if (LstSelection != null && ListBoxVal != null)
                                    {
                                        objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                        objCMS.fieldtext = Convert.ToString(LstSelection.SelectedItem);
                                        objCMS.fieldvalue = LstSelection.SelectedValue;
                                    }
                                    else
                                    {
                                        ValFlag = false;
                                        ValMsg = ValMsg + Convert.ToString(ValCounter) + "). Please select atleast one value from the dropdown list.<br/>";
                                        ValCounter++;
                                    }
                                    lstCMS.Add(objCMS);
                                    break;


                                case "Dropdown":
                                    objCMS.fieldtype = "Dropdown";
                                    DropDownList ddlSelection = (DropDownList)RptrContentMgmt.Items[i].FindControl("ddlSelection");
                                    Literal LitDropdown = (Literal)RptrContentMgmt.Items[i].FindControl("LitDropdown");
                                    string DropDownVal = "";
                                    if (lblDorder != null)
                                    {
                                        DropDownVal = hdnDropDownListVal.Value;
                                    }
                                    if (ddlSelection != null && DropDownVal != null && LitDropdown != null)
                                    {
                                        objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                        objCMS.fieldtext = LitDropdown.Text.Trim();
                                        objCMS.fieldvalue = ddlSelection.SelectedValue;
                                        DropDownSelectedVal = ddlSelection.SelectedValue;
                                    }
                                    else
                                    {
                                        ValFlag = false;
                                        ValMsg = ValMsg + Convert.ToString(ValCounter) + "). Please select atleast one value from the dropdown list.<br/>";
                                        ValCounter++;
                                    }
                                    lstCMS.Add(objCMS);
                                    break;
                                case "Upload Image":
                                    string Draftserverpath = "";
                                    string serverpath = "";
                                    string FolderName = LitpageTitle.Text;
                                    FolderName = FolderName.Replace(" ", "");
                                    Label lblCMSFileUploader = (Label)RptrContentMgmt.Items[i].FindControl("lblCMSFileUploader");
                                    if (lblCMSFileUploader.Text != "" && lblCMSFileUploader.Text != null)
                                    {

                                        Literal LitUploadLabel = (Literal)RptrContentMgmt.Items[i].FindControl("LitUploadLabel");
                                        if (lblCMSFileUploader != null && LitUploadLabel != null)
                                        {
                                            if (string.IsNullOrEmpty(lblCMSFileUploader.Text) && PageName.ToUpper().Contains("CATALOGUE"))
                                            {
                                                ClientScript.RegisterStartupScript(this.GetType(), "msgbox", "<script>alert('Please Upload Image for this part')</script>");
                                                return;
                                            }
                                            objCMS.fieldtype = lblFieldType.Text.Trim();
                                            objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                            objCMS.fieldtext = LitUploadLabel.Text;
                                            objCMS.fieldvalue = lblCMSFileUploader.Text;
                                            serverpath = Server.MapPath("../UploadedFiles/ContentImages/" + FolderName + "/");
                                            Draftserverpath = Server.MapPath("../UploadedFiles/ContentImages/" + FolderName + "_Draft" + "/");
                                            DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                                            bool isDirCreated = directoryInfo.Exists;
                                            if (!isDirCreated)
                                            {
                                                directoryInfo.Create();
                                            }
                                            if (File.Exists(serverpath + lblCMSFileUploader.Text))
                                            {
                                                File.Delete(serverpath + lblCMSFileUploader.Text);
                                            }
                                            File.Copy(Draftserverpath + lblCMSFileUploader.Text, serverpath + lblCMSFileUploader.Text);
                                            lstCMS.Add(objCMS);
                                        }
                                    }
                                    else
                                    {
                                        Literal LitUploadLabel = (Literal)RptrContentMgmt.Items[i].FindControl("LitUploadLabel");
                                        objCMS.fieldtype = lblFieldType.Text.Trim();
                                        objCMS.dorder = Convert.ToInt32(lblDorder.Text.Trim());
                                        objCMS.fieldtext = LitUploadLabel.Text;
                                        objCMS.fieldvalue = lblCMSFileUploader.Text;
                                        lstCMS.Add(objCMS);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                }

                if (isPageEntryValidated == true)
                {
                    if (lstCMS != null && lstCMS.Count > 0)
                    {
                        string RetVal = "";
                        if (Request.QueryString["CMSID"] != null)
                        {
                            if (Convert.ToInt32(Request.QueryString["CMSID"]) >= 99999999)
                            {
                                if (Convert.ToInt32(Request.QueryString["PID"]) == 88)
                                {

                                    bool CkhExisting = ServicesFactory.DocCMSServices.Check_Existing_ProductcategoryDetail_For_Save(DropDownSelectedVal.ToUpper(), Convert.ToInt32(Request.QueryString["PID"]));
                                    if (CkhExisting == true)
                                    {
                                        ClientScript.RegisterStartupScript(this.GetType(), "msgbox", "<script>alert('" + DropDownSelectedVal.ToLower() + " which you have provided already exist');</script>");
                                        return;
                                    }
                                    else
                                    {
                                        RetVal = ServicesFactory.DocCMSServices.Insert_CMS_Data(lstCMS, Convert.ToString(Session["UserID"]));
                                    }
                                }
                                else
                                {
                                    RetVal = ServicesFactory.DocCMSServices.Insert_CMS_Data(lstCMS, Convert.ToString(Session["UserID"]));
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(Request.QueryString["PID"]) == 88)
                                {
                                    bool CkhExisting = ServicesFactory.DocCMSServices.Check_Existing_ProductcategoryDetail_Update_Bycmsid(DropDownSelectedVal.ToUpper(), Convert.ToInt32(Request.QueryString["CMSID"]), Convert.ToInt32(Request.QueryString["PID"]));
                                    if (CkhExisting == true)
                                    {
                                        ClientScript.RegisterStartupScript(this.GetType(), "msgbox", "<script>alert('" + DropDownSelectedVal.ToLower() + "  which you have provided already exist');</script>");
                                        return;
                                    }
                                    else
                                    {
                                        RetVal = ServicesFactory.DocCMSServices.Update_CMS_Data(lstCMS, Convert.ToString(Session["UserID"]), Convert.ToInt32(Request.QueryString["CMSID"]));
                                    }
                                }
                                else
                                {
                                    RetVal = ServicesFactory.DocCMSServices.Update_CMS_Data(lstCMS, Convert.ToString(Session["UserID"]), Convert.ToInt32(Request.QueryString["CMSID"]));
                                }
                            }
                        }
                        if (RetVal == "")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "msgbox", "<script>ConfirmSave();</script>");
                            ServicesFactory.DocCMSServices.Remove_CMS_Data_Draft(Convert.ToInt32(Request.QueryString["PID"]), Convert.ToInt32(Request.QueryString["CMSID"]));
                        }
                    }
                    if (PageName.ToUpper().Contains("CATALOGUE"))
                    {  
                        Response.Redirect("~/cadmin/CatalogueDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]) + "");                     
                    }
                    else
                    {
                        Response.Redirect("~/cadmin/ContentManagementDashboard.aspx?PID=" + Convert.ToString(Request.QueryString["PID"]) + "");
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "DocFocus", "<script>alert('Please Complete the entries');</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    // Class Ends here
}

