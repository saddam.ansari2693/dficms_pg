﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.Factory;

namespace DocCMSMain.cadmin
{
    public partial class ApplicationUsersDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_ApplicationUsers();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
            }
        }

        // Bind Module inside RptrModules Repeater
        protected void Bind_ApplicationUsers()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_ApplicationUsers();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrModules.DataSource = dt;
                    RptrModules.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrModules_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
               
                switch (e.CommandName)
                {
                    case "EditApplicationUser":
                        Response.Redirect("~/cadmin/ApplicationUsers.aspx?AID=" + e.CommandArgument.ToString());
                        break;

                    case "DeleteApplicationUser":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_ApplicationUsers(Convert.ToInt32(e.CommandArgument));
                         Bind_ApplicationUsers();
                         Response.Redirect("~/cadmin/ApplicationUsersDashboard.aspx");
                        break;
                   
                    default:
                        break;
                }
            }
        }

        protected void RptrModules_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        protected void btnAddModuleBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/ApplicationUsers.aspx");
        }
    }
}