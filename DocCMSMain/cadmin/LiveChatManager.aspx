﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="LiveChatManager.aspx.cs" Inherits="DocCMSMain.cadmin.LiveChatManager" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="CadminFooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autogrow-textarea.js" type="text/javascript"></script>
    <script src="../js/ui.spinner.min.js" type="text/javascript"></script>
    <script src="../js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../js/forms.js" type="text/javascript"></script>
    <link href="../Autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Autocomplete/jquery.min.js" type="text/javascript"></script>
    <script src="../Autocomplete/jquery-ui.min.js" type="text/javascript"></script>
     <script type="text/javascript">
         function onlyNumbers(e) {
             var key;
             if (window.event) {
                 key = window.event.keyCode;     //IE
             }
             else {
                 key = e.which;      //firefox              
             }
             if (key == 0 || key == 8) {
                 return true;
             }

             if (key > 31 && (key < 48 || key > 57))
                 return false;
             //             
             return true;
         }
        

    </script>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
      

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
                            <ul class="breadcrumbs">
        <li><a href="/cadmin/Dashboard.aspx"><i class="iconfa-home"></i></a><span class="separator">
        </span></li>
        <li><a href="#">Administration</a> <span class="separator"></span></li>
        <li>Manage Session Manager</li>
    </ul>
                                                        <div class="pageheader">
        <form action="" method="post" class="searchbar">
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Live Chat Manager</h5>
            <h1>
                Manage Live Chat Manager</h1>
        </div>
    </div>
        <!--pageheader-->
        <div class="maincontent">
            <div class="maincontentinner" style="padding:20px 20px 185px;">
                    <div class="widget">
                <h4 class="widgettitle">
                    Manage Live Chat Manager</h4>
                <div class="widgetcontent">
                    <div class="inputwrapper login-alert" id="msgDiv" runat="server">
                        <div class="alert alert-error" style="height: 25px;" align="center">
                            <label id="labmsg" name="labmsg" runat="server" style="margin-top: 1px; color: #DA5251;">
                            </label>
                            <asp:HiddenField ID="hdnTimeoutId" runat="server" />
                        </div>
                    </div>
                    <div class="stdform">
                        <div class="par control-group">
                            <label class="control-label" for="ChkActiveStatus" style=" margin-top:-9px;">
                               Active Chat Panel</label>
                            <div class="controls">
                                <asp:CheckBox ID="ChkActiveStatus" runat="server" name="activestatus" AutoPostBack="true" Checked="true" class="input-large"  
                                    oncheckedchanged="ChkActiveStatus_CheckedChanged"/>
                            </div>
                        </div>
                    </div>
                 </div>
                <!--widgetcontent-->
            </div>
                <!--widget-->
       <uc1:CadminFooter ID="CadminFooter1" runat="server" />
</asp:Content>

