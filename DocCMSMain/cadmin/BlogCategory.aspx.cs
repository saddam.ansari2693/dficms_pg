﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class BlogCategory : System.Web.UI.Page
    {
        public Int32 BlogCategoryID { get; set; }
        dynamic UserId;
        public string Userid { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Session["linkname"] = "licontent";
                Session["Ulname"] = "ulcontent";
                
                if (Request.QueryString["CategoryID"] != null)
                {
                    BlogCategoryID = Convert.ToInt32(Request.QueryString["CategoryID"]);
                    Set_Data(BlogCategoryID);
                    btnEdits.Visible = true;
                    btnsubmit.Visible = false;
                    Bind_DisplayOrder();
                }
                else
                {
                    Int32 DisplayOrder = ServicesFactory.DocCMSServices.Get_Max_category_Type_dorder() + 1;
                    txtDisplayOrder.Value = Convert.ToString(DisplayOrder);
                    Bind_DisplayOrder();
                    btnEdits.Visible = false;
                    btnsubmit.Visible = true;
                }
            }
        }



        protected void btnsubmit_click(Object sender, EventArgs e)
        {
            try
            {
                bool CategoryExist = false;
                CategoryExist = ServicesFactory.DocCMSServices.ChkDuplicate_Blogcategory(txtCategoryName.Value.Trim());
                if (CategoryExist == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Category Allready Exegist');", true);
                }
                else
                {
                    Cls_Blog_category objsection = new Cls_Blog_category();
                    objsection.blogcategoryname= txtCategoryName.Value.Trim();
                    objsection.description = txtDescription.Value.Trim();
                    objsection.navigationurl= txtNavigationUrl.Value.Trim();
                    objsection.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                    objsection.createdby = Convert.ToString(Session["UserID"]);
                    if (ChkActiveStatus.Checked)
                        objsection.isactive = true;
                    else
                        objsection.isactive = false;
                    ServicesFactory.DocCMSServices.Insert_Blog_category(objsection);
                    ServicesFactory.DocCMSServices.Adjust_category_dorder();
                    Response.Redirect("~/cadmin/BlogCategoryDashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnreset_click(Object sender, EventArgs e)
        {
            reset_form();
        }
        protected void reset_form()
        {
            
        }
        protected void btnCancel_click(Object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/BlogCategoryDashboard.aspx");
        }
        protected void btnEdit_click(Object sender, EventArgs e)
        {

            try
            {
                if (Request.QueryString["CategoryID"] != null)
                {

                    bool SocialMediaHostExist = false;
                    SocialMediaHostExist = ServicesFactory.DocCMSServices.ChkDuplicate_BlogcategoryUpdate(txtCategoryName.Value.Trim(), Convert.ToInt32(Request.QueryString["CategoryID"]));
                    if (SocialMediaHostExist == true)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Category Name Allready Exegist');", true);
                    }
                    else
                    {
                        Cls_Blog_category objsection = new Cls_Blog_category();
                        objsection.blogcategoryname = txtCategoryName.Value;
                        objsection.navigationurl = txtNavigationUrl.Value.Trim();
                        objsection.description = txtDescription.Value.Trim();
                        objsection.dorder = Convert.ToInt32(txtDisplayOrder.Value);
                        objsection.lastmodifiedby = Convert.ToString(Session["UserID"]);
                        objsection.blogcategoryid = Convert.ToInt32(Request.QueryString["CategoryID"]);
                        if (ChkActiveStatus.Checked)
                            objsection.isactive = true;
                        else
                            objsection.isactive = false;
                        ServicesFactory.DocCMSServices.Update_category_By_id(objsection);
                        ServicesFactory.DocCMSServices.Adjust_category_dorder();
                        Response.Redirect("~/cadmin/BlogCategoryDashboard.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        protected void Set_Data(Int32 BlogCategoryID)
        {
            try
            {
                if ((Request.QueryString["CategoryID"] != null))
                {
                    Cls_Blog_category obj = new Cls_Blog_category();
                    obj = ServicesFactory.DocCMSServices.Fetch_category_By_id(BlogCategoryID);
                    if (obj != null)
                    {
                        txtCategoryName.Value = obj.blogcategoryname;
                        txtDescription.Value = obj.description;
                        txtDisplayOrder.Value = obj.description;
                        txtNavigationUrl.Value = obj.navigationurl;
                        txtDisplayOrder.Value = Convert.ToString(obj.dorder);
                        ChkActiveStatus.Checked = obj.isactive;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind Display order at Toggle inside RptrDisplayOrder repeater
        protected void Bind_DisplayOrder()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Blog_category();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrDisplayOrder.DataSource = dt;
                    RptrDisplayOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            EditDisplayOrders.Show();
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
            List<Cls_Blog_category> lstCategory= new List<Cls_Blog_category>();
            Int32[] DisplayOrder = new Int32[RptrDisplayOrder.Items.Count];
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                Label lblCategoryID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblCategoryID");
                Label lblCategoryName = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblCategoryName");
                if (Session["UserID"] != null && lblCategoryName != null && lblCategoryID != null && !string.IsNullOrEmpty(txtDorder.Value))
                {
                    DisplayOrder[ItemCount] = Convert.ToInt32(txtDorder.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Display Order for " + lblCategoryName.Text + " is not provided...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }
            //Sorting the Array into Acending Order
            Array.Sort(DisplayOrder);
            //Now checking if any term is missing in acending the display order
            for (int j = 0; j < DisplayOrder.Length - 1; j++)
            {
                if (DisplayOrder[j] < DisplayOrder[j + 1])
                {
                    if (DisplayOrder[j] == DisplayOrder[j + 1] - 1)
                    {
                        //do nothing
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                        Bind_DisplayOrder();
                        return;
                    }
                }
                else if (DisplayOrder[j] == DisplayOrder[j + 1])
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('The provided display orders are not in ascending order...');</script>");
                    Bind_DisplayOrder();
                    return;
                }
            }

            // Update the display Order
            for (Int32 ItemCount = 0; ItemCount < RptrDisplayOrder.Items.Count; ItemCount++)
            {
                Cls_Blog_category objCategory = new Cls_Blog_category();
                Label lblCategoryID = (Label)RptrDisplayOrder.Items[ItemCount].FindControl("lblCategoryID");
                HtmlInputText txtDorder = (HtmlInputText)RptrDisplayOrder.Items[ItemCount].FindControl("txtDorder");
                if (Session["UserID"] != null && lblCategoryID != null)
                {
                    objCategory.blogcategoryid = Convert.ToInt32(lblCategoryID.Text.Trim());
                    objCategory.dorder = Convert.ToInt32(txtDorder.Value);
                    objCategory.lastmodifiedby = Convert.ToString(Session["UserID"]);
                    lstCategory.Add(objCategory);
                }
            }
            if (lstCategory != null && lstCategory.Count > 0)
            {
                Int32 retVal = ServicesFactory.DocCMSServices.Update_category_dorder(lstCategory);
                if (retVal > 0)
                {

                    if (Request.QueryString["CategoryID"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + Request.QueryString["CategoryID"] + ");</script>");
                        Set_Data(Convert.ToInt32(Request.QueryString["CategoryID"]));
                        Bind_DisplayOrder();
                        btnsubmit.Visible = false;
                        btnEdits.Visible = true;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg(" + "" + ");</script>");
                        Int32 DisplayOrder1 = ServicesFactory.DocCMSServices.Get_Max_category_Type_dorder() + 1;
                        Bind_DisplayOrder();
                        txtDisplayOrder.Value = Convert.ToString(DisplayOrder1);
                        btnsubmit.Visible = true;
                        btnEdits.Visible = false;
                    }
                }
            }
        }
    }//==== class ends here 
}