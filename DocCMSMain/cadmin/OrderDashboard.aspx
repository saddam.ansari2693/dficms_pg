﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="OrderDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.OrderDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <link href="css/PopUpStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <style type="text/css">
      .newwidth
      {
          width:94%;
      }
    </style>
    <script type="text/javascript">
             jQuery(document).ready(function () {
                 var showValue = '<%= Session["showtablesize"] %>';
                 if (showValue == null || showValue == "") {
                     showValue = 10;
                 }

                 jQuery('#dyntable').dataTable({
                     "sPaginationType": "full_numbers",
                     "iDisplayLength": parseInt(showValue, 10),
                     "aaSortingFixed": [[0, 'desc'],
                     "fnDrawCallback": function (oSettings) {
                         jQuery.uniform.update();
                     }
                 });
             });
      </script>
      <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
        jQuery(document).ready(function () {
            jQuery('select[name="dyntable_length"]').on('change', function () {
                var ShowTableSize = jQuery(this).val();
                setSession(ShowTableSize);
             });
        });
        // Set Session for showing data in data table
        function setSession(ShowTableSize) {
            var args = {
                showtablesize: ShowTableSize
            }
            // service of session
            jQuery.ajax({
                type: "POST",
                url: "ContentManagementDashboard.aspx/SetSession",
                data: JSON.stringify(args),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                },
                error: function () {
               }
            });
       }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!--maincontent-->
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage Order</a> <span class="separator"></span></li>
        <li>Manage Order</li>
    </ul>
    <!--pageheader-->
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Order</h5>
            <h1>
              Manage Order</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              Manage Order
            </h4>
            <asp:Panel ID="PanelJob" runat="server">
            <table id="dyntable"  class="table table-bordered responsive" >
                <colgroup>
                 <col class="con0" style="align: center; width: 5%;" />
                   <col class="con0" style="align: center; width: 10%;" />
                    <col class="con1" style="width: 10%;" />
                     <col class="con1" style="width: 10%;" />
                      <col class="con1" style="width: 10%;" />
                       <col class="con1" style="width: 10%;" />
                         <col class="con1" style="width: 10%;" />
                           <col class="con1" style="width: 10%;" />  
                             <col class="con0" style="text-align: center; width: 25%;" />  
                 </colgroup>
                  <thead>
                    <tr>
                        <th class="head1">
                         Sl No.
                        </th>
                        <th class="head1">
                         Order No.
                        </th>
                         <th class="head1">
                         Company Name
                          </th>
                        <th class="head1">
                         Purchaser Name
                          </th>
                          <th class="head1">
                           Phone Number
                          </th>
                          <th class="head1">
                           Email ID
                          </th>
                          <th class="head1">
                           Order Status
                           </th>
                           <th class="head1">
                           Order Date
                           </th>
                           <th class="head1" style="text-align: center;">
                            Functions
                           </th>
                    </tr>
                </thead>
                  <tbody>
                    <asp:Repeater runat="server" ID="RptrOrder" OnItemCommand="RptrOrder_ItemCommand"
                        OnItemDataBound="RptrOrder_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                             <td>
                           <asp:Label runat="server" Text='<%# Eval("PurchaseOrderID")%>' ID="PurchaseOrderID"></asp:Label>
                            </td>
                                <td>
                                    <asp:Label ID="lblPurchaseOrderID" runat="server" ClientIDMode="Static" Text='<%# Eval("PurchaseOrderID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:LinkButton runat="server" Text='<%# Eval("OrderNo")%>'  CommandName="ViewAllDetail" ID="lnkOrderNo" ></asp:LinkButton>
                                </td>
                                  <td>
                                 <asp:Label runat="server" Text='<%# Eval("CompanyName")%>' ID="lblCompanyName"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("PurchaserName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("Phone")%>' ID="lblPhone"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("Email")%>' ID="lblOrderType"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("OrderStatus")%>' ID="lblOrderStatus"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("OrderDate")%>' ID="lblOrderDate"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:LinkButton runat="server"  CommandName="Export" ID="LnkPrintOrder" CssClass="btn"><i class="icon icon-download"></i> Export PDF</asp:LinkButton>
                                    <asp:LinkButton runat="server"  CommandName="ViewOrder" ID="lnkViewOrder" CssClass="btn  " ><i class="icon icon-eye-open"></i> View Detail</asp:LinkButton>
                                    <asp:LinkButton runat="server"  CommandName="EditDetail" ID="LnkEditDetail" CssClass="btn"><i class="icon icon-download"></i> Edit Detail</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                             <td>
                           <asp:Label runat="server" Text='<%# Eval("PurchaseOrderID")%>' ID="PurchaseOrderID"></asp:Label>
                            </td>
                              <td>
                                    <asp:Label ID="lblPurchaseOrderID" runat="server" ClientIDMode="Static" Text='<%# Eval("PurchaseOrderID") %>'
                                        Visible="false"></asp:Label>
                                    <asp:LinkButton runat="server" Text='<%# Eval("OrderNo")%>'  CommandName="ViewAllDetail" ID="lnkOrderNo" ></asp:LinkButton>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("CompanyName")%>' ID="lblCompanyName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("PurchaserName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("Phone")%>' ID="lblPhone"></asp:Label>
                                </td>
                                 <td>
                                  <asp:Label runat="server" Text='<%# Eval("Email")%>' ID="lblOrderType"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("OrderStatus")%>' ID="lblOrderStatus"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("OrderDate")%>' ID="lblOrderDate"></asp:Label>
                                </td>
                                <td class="center">
                                    <asp:LinkButton runat="server"  CommandName="Export" ID="LnkPrintOrder" CssClass="btn"><i class="icon icon-download"></i> Export PDF</asp:LinkButton>
                                    <asp:LinkButton runat="server"  CommandName="ViewOrder" ID="LinkViewOrder" CssClass="btn  " > <i class="icon icon-eye-open"></i> View Detail</asp:LinkButton>
                                    <asp:LinkButton runat="server"  CommandName="EditDetail" ID="LnkEditDetail" CssClass="btn"><i class="icon icon-download"></i> Edit Detail</asp:LinkButton>
                                </td>
                             </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                 Order Detail
                                     <a href="" id="btnCloseDisplayOrder" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table1" class="table table-bordered responsive" style="width: 100%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 30%;" />
                                    <col class="con0" style="align: right; width: 70%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Content
                                        </th>
                                       <th>
                                            Detail
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrCompleteDetail">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblContent" Text='<%# Eval("Order")%>' runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                     <asp:Label ID="Label1" Text='<%# Eval("OrderDetail")%>' runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                <td>
                                                    <asp:Label ID="lblContent" Text='<%# Eval("Order")%>' runat="server"></asp:Label>
                                               </td>
                                                <td>
                                                     <asp:Label ID="Label1" Text='<%# Eval("OrderDetail")%>' runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                             </table>
                           </div>
                         <div class="modal-footer">
                       </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
             <!--Detail Edit Functionality-->
             <div id="pnlEditDetail" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="H1" class="modal-title">
                                 Order Detail
                                     <a href="" id="btnCloseEditDetail" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table2" class="table table-bordered responsive" style="width: 100%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 30%;" />
                                    <col class="con0" style="align: right; width: 70%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Content
                                        </th>
                                       <th>
                                            Detail
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                            <tr class="gradeX">
                                                <td>
                                                    Company Name:
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtCompanyName" Text='<%# Eval("CompanyName")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                     <asp:HiddenField ID="hdnGrandTotal" Value='<%# Eval("GrandTotal")%>' runat="server" />
                                                     <asp:HiddenField ID="hdnPurchaseOrderID" Value='<%# Eval("PurchaseOrderID")%>' runat="server" />
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Purchaser Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPurchaserName" Text='<%# Eval("PurchaserName")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>
                                                   Billing Address1:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBillingAddress1" Text='<%# Eval("BillingAddress1")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>
                                                   Billing Address2:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBillingAddress2" Text='<%# Eval("BillingAddress2")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>
                                                   Billing City:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBillingCity" Text='<%# Eval("BillingCity")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>
                                                   Billing Provience:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBillingProvience" Text='<%# Eval("BillingProvience")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                                <tr class="gradeX">
                                                <td>
                                                   Billing PostalCode:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBillingPostalCode" Text='<%# Eval("BillingPostalCode")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                              </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Phone:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhone" Text='<%# Eval("Phone")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Fax:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFax" Text='<%# Eval("Fax")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Email:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmail" Text='<%# Eval("Email")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   PaymentMethod:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpPayment" runat="server" ClientIDMode="Static" style="width:97%;">
                                                     </asp:DropDownList>
                                                </td>
                                            </tr>
                                             <tr class="gradeX">
                                                <td>
                                                   Order Status:
                                                </td>
                                                <td>
                                                 <asp:DropDownList ID="drpOrderStatus" runat="server" ClientIDMode="Static" style="width:97%;">
                                                     </asp:DropDownList>
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Other Detail:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOtherDetail" Text='<%# Eval("OtherDetail")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   PO #:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPONumber" Text='<%# Eval("PONumber")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>
                                                   Shipping Address1:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtShippingAddress1" Text='<%# Eval("ShippingAddress1")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Shipping Address2:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtShippingAddress2" Text='<%# Eval("ShippingAddress2")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                             </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Shipping City:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtShippingCity" Text='<%# Eval("ShippingCity")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                             </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Shipping Provience:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtShippingProvience" Text='<%# Eval("ShippingProvience")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                             </tr>
                                              <tr class="gradeX">
                                                <td>
                                                   Shipping PostalCode:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtShippingPostalCode" Text='<%# Eval("ShippingPostalCode")%>' runat="server" class="input-large newwidth"></asp:TextBox> 
                                                </td>
                                            </tr>
                                          </tr>
                                  </tbody>
                              </table><br />
                             <asp:Button ID="btnUpdateExisting" OnClick="btnUpdateExisting_onclick" Text="Update" runat="server"  class="btn btn-default" />
                           <asp:Button ID="btnAddasNew" OnClick="btnAddasNew_onclick" Text="Add as New" runat="server"  class="btn btn-primary" />
                         </div>
                        <div class="modal-footer">
                      </div>
                    </div>
                </div>
            </div>
             <!--Detail Edit Functionality-->
             <asp:Button ID="MpeFakeTargetEdit" runat="server" CausesValidation="False" Style="display: none;" />
             <asp:ModalPopupExtender ID="EditDetail" runat="server" PopupControlID="pnlEditDetail"
                TargetControlID="MpeFakeTargetEdit" CancelControlID="btnCloseEditDetail" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
            </asp:Panel>
            <br />
            <br />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
</asp:Content>
