﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DocCMSMain.cadmin
{
    public partial class MainMenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "lihyperlink";
            Session["Ulname"] = "ulhyperlink";

        }
        protected void ShowFormView(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/cadmin/EditNavigationLinks.aspx");
        }

        protected void UpdatePage(object sender, EventArgs e)
        {
            RepeaterContent.DataBind();
        }
        protected void RepeaterContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "EditLink")
            {
                Response.Redirect("~/cadmin/EditNavigationLinks.aspx?id=" + e.CommandArgument.ToString());

            }
            if (e.CommandName == "Delete")
            {
                if (Set_Roles())
                {
                }
                else
                {
                    //== Do Nothing
                }
            }
            if (e.CommandName == "OrderUp")
            {
                
            }
            if (e.CommandName == "OrderDown")
            {
            }
        }

        protected void RepeaterContent_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }

        private bool Set_Roles()
        {
           
            return true;
        }
    }
}