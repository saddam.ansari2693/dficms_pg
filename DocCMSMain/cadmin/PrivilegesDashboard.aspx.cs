﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using DocCMS.Core.Factory;

namespace DocCMSMain.cadmin
{
    public partial class PrivilegesDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_Privileges();
                SetControls();
            }
        }


        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                {
                    btnAddPrivilegeTop.Visible = true;
                    btnAddPrivilegeBottom.Visible = true;
                }
                else
                {
                    btnAddPrivilegeTop.Visible = false;
                    btnAddPrivilegeBottom.Visible = false;
                }
                foreach (RepeaterItem Item in RptrPrivileges.Items)
                {

                    LinkButton lnkPrivilegeName = (LinkButton)Item.FindControl("lnkPrivilegeName");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton btnOrderUP = (ImageButton)Item.FindControl("btnOrderUP");
                    ImageButton btnOrderDown = (ImageButton)Item.FindControl("btnOrderDown");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    if (lblTitle != null && lnkPrivilegeName != null && btnOrderUP != null && btnOrderDown != null && lnkEdit != null && lnkDelete != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkPrivilegeName.Visible = true;
                            lblTitle.Visible = false;
                            btnOrderDown.Visible = true;
                            btnOrderUP.Visible = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkPrivilegeName.Visible = false;
                            lblTitle.Visible = true;
                            btnOrderDown.Visible = false;
                            btnOrderUP.Visible = false;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // Bind privileges Function
        protected void Bind_Privileges()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_privileges();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrPrivileges.DataSource = dt;
                    RptrPrivileges.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrPrivileges_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "EditPrivilege":
                        Response.Redirect("~/cadmin/Privileges.aspx?PID=" + e.CommandArgument.ToString());
                        break;

                    case "DeletePrivilege":
                        Int32 retVal = ServicesFactory.DocCMSServices.Delete_privileges(Convert.ToInt32(e.CommandArgument));
                        Bind_Privileges();
                        break;
                    case "OrderUp":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_privileges(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "UP");
                        }
                        Bind_Privileges();
                        break;
                    case "OrderDown":
                        if (lblDorder != null)
                        {
                            ServicesFactory.DocCMSServices.Reorder_privileges(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(lblDorder.Text), "DOWN");
                        }
                        Bind_Privileges();
                        break;
                    default:
                        break;

                }
            }
        }

        protected void RptrPrivileges_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
            }
        }
        protected void btnAddPrivilegeBottom_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/Privileges.aspx");
        }
    }
}