﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/CMSMaster.Master" AutoEventWireup="true" CodeBehind="OrderDetailDashboard.aspx.cs" Inherits="DocCMSMain.cadmin.OrderDetailDashboard" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/responsive-tables.css" />
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/responsive-tables.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var pagesize = jQuery('#hdpagesize').val();
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[2, 'asc'], [0, 'asc']],
                "fnDrawCallback": function (oSettings) {
                    jQuery.uniform.update();
                }
            });
            jQuery('#dyntable2').dataTable({
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
        });
    </script>
    <script type="text/javascript">
        function UploadComplete(sender, args) {
            $get("ContentPlaceHolder1_FormView1_PhotoPathHiddenField").value = args.get_fileName();
            document.getElementById("divUpload").style.display = "none";
        }
        function UploadStarted() {
            document.getElementById("divUpload").style.display = "block";
        }
    </script>
  <%--function to add new item to data base--%>
  <script type="text/javascript">
    function AddNewProduct(ImgCtrl, CMSID) {
        var categoryname = jQuery(ImgCtrl).closest("tr").find("#hdnProductCategoryName").val();
        var ProductName = jQuery(ImgCtrl).closest("tr").find("#lblPartNo").html();
        var ProductNumber = jQuery(ImgCtrl).closest("tr").find("#lblProductNumber").val();
        var InternalDescription = jQuery(ImgCtrl).closest("tr").find("#hdnInternalDescription").val();
        var description = jQuery(ImgCtrl).closest("tr").find("#lblDescription").html();
        var Price = jQuery(ImgCtrl).closest("tr").find("#lblProductPrice").html();
        var Cost = jQuery(ImgCtrl).closest("tr").find("#hdnCost").html();
        if (Cost == "" || Cost == null) {
            Cost = 0;
        }
        var quantity = jQuery(ImgCtrl).closest("tr").find("#txtEnterOrderQuantity").val();
        var amount = Price * quantity;
        var purchaseorderid = jQuery("#hdnPurchaseOrderID").val();
        var Cls_OrderDetail =
        {
        ProductName: ProductName,
        ProductNumber: ProductNumber,
        InternalDescription: InternalDescription,
        CategoryName: categoryname,
        Quantity: quantity,
        Description: description,
        Price: Price,
        Cost: Cost,
        RunningPrice: amount,
        PurchaseOrderID: purchaseorderid
    };
        var objOrderDetail = JSON.stringify(Cls_OrderDetail);
        jQuery.ajax({
            url: "../WebService/DocCMSApi.svc/Insert_New_Product_fromCMS2",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Cls_OrderDetail),
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });

        function ajaxSucceeded(data) {
            if (data != null && data > 0) {
                alert("Item Added Sucessfully");
                location.reload(true);
            }
            else {
                alert("This item is already added");
            }
        }
        function ajaxFailed() {
           alert("There is an error during operation.");
        }
    }

    function onlyNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
<script type="text/javascript">
    function ViewDetails(OrderDetailID) {
        jQuery.ajax({
            url: "../WebService/DocCMSApi.svc/Fetch_PartDetail_ByOrderDetalID_From_OrderDetailTable",
            contentType: 'application/json; charset=utf-8',
            data: { "OrderDetailID": OrderDetailID.toString() },
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });

        function ajaxSucceeded(data) {
            if (data != null) {
                var result = jQuery.parseJSON(data);
                if (result.length > 0) {
                    jQuery("#txtProductName").val(result[0].ProductName);
                    jQuery("#txtInternalDescription").val(result[0].InternalDescription);
                    jQuery("#txtUnitPrice").val(result[0].Price);
                    jQuery("#txtQuantity").val(result[0].Quantity);
                    jQuery("#txtAmount").val(result[0].RunningPrice);
                    jQuery("#hdnOrderDetailID").val(result[0].OrderDetailID);
                    jQuery("#txtQuantityShipped").val(result[0].QuantityShipped);
                    jQuery("#txtQuantitybackOrder").val(result[0].QuantityBackOrder);
                }
            }
            else {
            }
        }
        function ajaxFailed() {
            alert("There is an error during operation.");
        }
    }

    function EnterOrderQuatity() {
        if (jQuery("#ChkOrderQuantity").is(':checked')) {
            jQuery("#txtQuantity").attr('readonly', false);
        }
        else {
            jQuery("#txtQuantity").attr('readonly', true);
         }
    }

    //function on change of Order Quantity
    function ChangeQuantity(objStr) {
        var UnitPrice = jQuery("#txtUnitPrice").val();
        var TotalAmount = objStr * UnitPrice;
        jQuery("#txtAmount").val(TotalAmount.toFixed(2));
    }

    function QuanityShippedChange() {
            var UnitPrice = jQuery("#txtUnitPrice").val();
            var QuantityOrder = jQuery("#txtQuantity").val();
            var QuantityShipped = jQuery("#txtQuantityShipped").val();
            var QuantitybackOrder = jQuery("#txtQuantitybackOrder").val();
            var QuantityShippedAndBackOrder = parseInt(QuantityShipped) + parseInt(QuantitybackOrder);
            var TotalAmount = QuantityShippedAndBackOrder * UnitPrice;
            jQuery("#txtAmount").val(TotalAmount.toFixed(2));
    }

    function QuanityBackOrder() {
        var UnitPrice = jQuery("#txtUnitPrice").val();
        var QuantityOrder = jQuery("#txtQuantity").val();
        var QuantityShipped = jQuery("#txtQuantityShipped").val();
        var QuantitybackOrder = jQuery("#txtQuantitybackOrder").val();
        var QuantityShippedAndBackOrder = parseInt(QuantityShipped) + parseInt(QuantitybackOrder);
        var TotalAmount = QuantityShippedAndBackOrder * UnitPrice;
        jQuery("#txtAmount").val(TotalAmount.toFixed(2));
    }

    function ValidateQuantity() {
    }

    function NoQuantity() {
        alert('Please provide Quantity Shipped and Quantity back order');
        return false;
    }
</script>
 <script language="javascript" type="text/javascript">
     function SuccessMsg() {
         alert("Record is deleted successfully!!!");
         return false;
     }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:HiddenField ID="hdnPurchaseOrderID" runat="server" ClientIDMode="Static" />
    <ul class="breadcrumbs">
        <li><a href="#"><i class="iconfa-home"></i></a><span class="separator"></span></li>
        <li><a href="#">Manage Detail Order</a> <span class="separator"></span></li>
        <li>Manage Detail Order</li>
    </ul>
    <div class="pageheader">
        <form action="" method="post" class="searchbar">        
        </form>
        <div class="pageicon">
            <span class="iconfa-pencil"></span>
        </div>
        <div class="pagetitle">
            <h5>
                Manage Detail Order</h5>
            <h1>
              Manage Detail Order</h1>
        </div>
    </div>
    <!--pageheader-->
    <div class="maincontent">
        <div class="maincontentinner">
            <h4 class="widgettitle">
              Manage Detail Order
               <asp:Button ID="btnShowCustomerDetail" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Show Customer Detail" 
                    OnClick="btnShowCustomerDetail_Click" />
               <asp:Button ID="btnSendQuote" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Send Mail" 
                    Style="margin-top:-35px; margin-left:1400px; float:left;" 
                    OnClick="btnSendQuote_Click" />                      
                    
              <asp:Button ID="btnDownloadQuote" runat="server" CssClass="btn btn-primary" 
                    Text="Download PDF" 
                   style="font-size: 13px; width: 149px; margin-left:935px; margin-top:2px"
                    OnClick="btnDownloadQuote_Click"  />
              <asp:Button ID="btnAddNewProduct" runat="server" CssClass="btn btn-primary" 
                    Width="150" Text="Add New Product" 
                    Style="margin-top:-35px; margin-left:1061px; float:left;" 
                    OnClick="btnAddNewProduct_Click" />
            </h4>
              <asp:Panel ID="PanelJob" runat="server">
            <table id="dyntable" class="table table-bordered responsive" >
                <colgroup>
                    <col class="con0" style="align: center; width: 20%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 15%;" />
                    <col class="con1" style="width: 10%;" />
                    <col class="con1" style="width: 10%;" />
                    <col class="con1" style="width: 10%;" />
                    <col class="con1" style="width: 10%;" />
                    <col class="con1" style="width: 25%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">
                      Category
                        </th>
                         <th class="head1">
                    Product Number
                          </th>
                        <th class="head1">
                    Product Name
                          </th>
                              <th class="head1">
                       Quantity Order
                          </th>
                             <th class="head1">
                       Quantity Shipped
                          </th>
                           <th class="head1">
                       Quantity Back Order
                          </th>
                      <th class="head1">
                      Unit Price
                          </th>
                       <th class="head1">
                       Amount
                          </th>
                          <th class="head1">
                       Edit Order
                          </th>
                 </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="RptrOrder" OnItemCommand="RptrOrder_ItemCommand"
                        OnItemDataBound="RptrOrder_databound">
                        <ItemTemplate>
                            <tr class="gradeX">
                               <td>
                                 <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductNumber")%>' ID="lblProductNumber"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductName")%>' ID="Label1"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("Quantity")%>' ID="lblOrderType"></asp:Label>
                                </td>
                                  <td>
                                 <asp:Label runat="server" Text='<%# Eval("QuantityShipped")%>' ID="lblQuantityShipped"></asp:Label>
                                </td>
                                  <td>
                                 <asp:Label runat="server" Text='<%# Eval("QuantityBackOrder")%>' ID="lblQuantityBackOrder"></asp:Label>
                                </td>
                               <td>
                                 $<asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("RunningPrice")%>' ID="lblRunningPrice"></asp:Label>
                                </td>
                                <td>
                                <input type="image"  id="lblShowTimings" title="Edit Module" data-toggle="modal" data-target="#myModal" src="../images/table_icon_1.gif" onclick='return ViewDetails(<%# Eval("OrderDetailID") %>)' />
                                </td>
                             </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="gradeA">
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("CategoryName")%>' ID="lblPurchaserName"></asp:Label>
                                </td>
                                 <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductNumber")%>' ID="lblProductNumber"></asp:Label>
                                </td>
                                <td>
                                 <asp:Label runat="server" Text='<%# Eval("ProductName")%>' ID="Label1"></asp:Label>
                                </td>
                                   <td>
                                 <asp:Label runat="server" Text='<%# Eval("Quantity")%>' ID="lblOrderType"></asp:Label>
                                </td>
                                  <td>
                                 <asp:Label runat="server" Text='<%# Eval("QuantityShipped")%>' ID="lblQuantityShipped"></asp:Label>
                                </td>
                                  <td>
                                 <asp:Label runat="server" Text='<%# Eval("QuantityBackOrder")%>' ID="lblQuantityBackOrder"></asp:Label>
                                </td>
                               <td>
                                 $<asp:Label runat="server" Text='<%# Eval("Price")%>' ID="lblPrice"></asp:Label>
                                </td>
                                <td>
                                 $<asp:Label runat="server" Text='<%# Eval("RunningPrice")%>' ID="lblRunningPrice"></asp:Label>
                                </td>
                                 <td>
                                <input type="image"  id="lblShowTimings" title="Edit Module" data-toggle="modal" data-target="#myModal" src="../images/table_icon_1.gif" onclick='return ViewDetails(<%# Eval("OrderDetailID") %>)' />
                                </td>
                              </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <%--Customer Detail  strat here --%>  
            <div id="pnlDisplayOrders" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="myModalLabel" class="modal-title">
                                 Order Detail
                                     <a href="" id="btnCloseDisplayOrder" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table1" class="table table-bordered responsive" style="width: 100%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 30%;" />
                                    <col class="con0" style="align: right; width: 70%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Content
                                        </th>
                                       <th>
                                            Detail
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrCompleteDetail">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                    <asp:Label ID="lblContent" Text='<%# Eval("Order")%>' runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                     <asp:Label ID="Label1" Text='<%# Eval("OrderDetail")%>' runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                                <td>
                                                    <asp:Label ID="lblContent" Text='<%# Eval("Order")%>' runat="server"></asp:Label>
                                               </td>
                                                    <td>
                                                     <asp:Label ID="Label1" Text='<%# Eval("OrderDetail")%>' runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                         </div>
                        <div class="modal-footer">
                       </div>
                    </div>
                </div>
            </div>
             <asp:Button ID="MpeFakeTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="EditDisplayOrders" runat="server" PopupControlID="pnlDisplayOrders"
                TargetControlID="MpeFakeTarget" CancelControlID="btnCloseDisplayOrder" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
            <%--Customer Detail  ends here --%>  
              <%--Add new Product strat here --%>  
              <div id="pnlAddNewProduct" class="modal fade in" style="display: none;width: 50%;left:650 !important;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="btnClosepnl" class="modal-title">
                                 Product List
                                     <a href="" id="A1" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                            <table id="Table2" class="table table-bordered responsive" style="width: 100%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 25%;" />
                                    <col class="con0" style="align: right; width: 15%;" />
                                    <col class="con1" style="align: left; width: 15%;" />
                                    <col class="con0" style="align: right; width: 15%;" />
                                    <col class="con0" style="align: right; width: 5%;" />
                                    <col class="con0" style="align: right; width: 15%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th>
                                            Product Image
                                        </th>
                                       <th>
                                            Product Number
                                        </th>
                                         <th>
                                            Product Name
                                        </th>
                                        <th>
                                            Description
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <th>
                                           Enter Quantity
                                        </th>
                                        <th>
                                          Add
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                  <asp:Repeater runat="server" ID="RptrParts" OnItemDataBound="RptrParts_ItemDataBound" >
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td>
                                                   <asp:HiddenField ID="hdnProductCategoryName" runat="server" Value='<%#Eval("CategoryName")%>' ClientIDMode="Static"/>                                                
                                                   <asp:Label ID="lblImageName" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                                                   <img id="myImg"   clientidmode="Static" runat="server"  style="width:120px; Height:120px;" src='<%# "../UploadedFiles/ContentImages/Catalogue/" + Eval("ImageName") %>'  title='<%# Eval("Description") %>' alt='<%# Eval("Description") %>' width="300" height="200">
                                                </td>
                                                 <td>
                                                <asp:Label ID="lblProductNumbera" runat="server" Text='<%# Eval("ProductNumber") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' CssClass="DorderLabel" ClientIDMode="Static" Visible="false"></asp:Label>
                                                    <asp:HiddenField ID="lblProductNumber"  runat="server" Value='<%# Eval("ProductNumber") %>' ClientIDMode="Static" ></asp:HiddenField>
                                                     <asp:HiddenField ID="hdnInternalDescription"  runat="server" Value='<%# Eval("InternalDescription") %>' ClientIDMode="Static"></asp:HiddenField>
                                                    <asp:Label ID="lblPartNo" runat="server" Text='<%# Eval("ProductName") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                                <td>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                              <td>
                                                  <asp:HiddenField ID="hdnCost"  runat="server" Value='<%# Eval("Cost") %>' ClientIDMode="Static" ></asp:HiddenField>
                                                  <asp:Label ID="lblProductPrice" runat="server" Text='<%# Eval("Price") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtEnterOrderQuantity" onkeypress="return onlyNumbers(event)" ClientIDMode="Static"  runat="server"></asp:TextBox>
                                              </td>
                                              <td>
                                               <img id="imgAdd" alt="Add To Cart" src="../DFI/images/AddToCart1.png" title="Click here to add this item" style="margin: 0; height: 25px; width: 25px;
                                                            padding: 3px; cursor: pointer;"  onclick="AddNewProduct(this,'<%#Eval("CMSID")%>')" />
                                              </td>
                                            </tr>
                                        </ItemTemplate>
                                   </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                       </div>
                    </div>
                </div>
            </div>
             <asp:Button ID="MpeFakeProductList" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="AddProductList" runat="server" PopupControlID="pnlAddNewProduct"
                TargetControlID="MpeFakeProductList" CancelControlID="btnClosepnl" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
          <%--Add new Product ends here --%>  
          <%--Order description starts here --%>  
            <div id="pnlOrderDescription" class="modal fade in" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866C6; color: #FFFFFF;">
                            <h4 id="H1" class="modal-title">
                                 Order Description
                                     <a href="" id="A2" onclick="return false;" style="float:right;"><img src="../DFI/images/delete_active.png" /></a><span
                                style="display: none;"></span>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: 640px;">
                          <div class="par control-group">
                            <label class="control-label" for="txtInternalDescription">
                              Add Your Description Here</label>
                            <div class="controls">
                                <textarea id="txtOrderDescription" name="txtOrderDescription"  rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="min-height: 100px;width: 521px;height: 302px;margin: 0px 0px 10px;"
                                    runat="server" class="input-large" ></textarea>
                                    <br />
                                <asp:Button ID="btnAddOrderDescription" runat="server" CssClass="btn btn-primary" Width="200"
                                  Text="Add Description" OnClick="btnAddOrderDescription_Click"   />
                                <asp:Button ID="btnUpdateOrderDescription" Visible="false" runat="server" CssClass="btn btn-Default" Width="200"
                                  Text="Update Description" OnClick="btnUpdateOrderDescription_Click"   />
                            </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                       </div>
                    </div>
                </div>
            </div>
             <asp:Button ID="MpeOrderTarget" runat="server" CausesValidation="False" Style="display: none;" />
            <asp:ModalPopupExtender ID="ModalOrderDescription" runat="server" PopupControlID="pnlOrderDescription"
                TargetControlID="MpeOrderTarget" CancelControlID="A2" BackgroundCssClass="Background">
            </asp:ModalPopupExtender>
             <%--Order description ends here --%>  
            </asp:Panel>
            <div  style="width:200px; float:right; text-align:center; background-color:#0F5B96; color:#fff;font-size:17px; padding:5px;">
              <span ID="divTotal" runat="server"></span>
              </div>
            <br />
            <br />
            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Width="200"
                Text="Back To Report Dashboard" OnClick="btnback_Click" />
            <asp:Button ID="btnAddDescription" runat="server" CssClass="btn btn-primary" Width="200"
                Text="Add Description"  OnClick="btnAddDescription_Click"   />
            <br />
            <br />
            <uc1:Footer ID="FooterControl" runat="server" />
        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->
  <!-- Modal edit start-->
  <div class="modal fade" id="myModal" role="dialog" style="width:40%">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item Detail</h4>
        </div>
        <div class="modal-body">
                      <div class="stdformQuote">
                        <div class="par control-group">
                            <label class="control-label" for="txtModuleName">
                                Product Name</label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtProductName" id="txtProductName" class="input-large"
                                    clientidmode="Static"  readonly />
                                <input type="hidden" runat="server" name="hdnOrderDetailID" id="hdnOrderDetailID"
                                    clientidmode="Static"  />
                            </div>
                        </div>
                          
                        <div class="par control-group">
                            <label class="control-label" for="txtInternalDescription">
                               Internal Description</label>
                            <div class="controls">
                                <textarea id="txtInternalDescription" name="txtInternalDescription" readonly rows="8" clientidmode="Static"
                                    validationgroup="grpsubmit" style="height: 100px; min-height: 100px; max-width: 900px;"
                                    runat="server" class="input-large" ></textarea>
                            </div>
                        </div>
                        <div class="par control-group">
                            <label class="control-label" for="txtUnitPrice">
                                Unit Price </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtUnitPrice" readonly id="txtUnitPrice" onkeypress="return onlyNumbers(event)"
                                    class="input-large" placeholder="Enter Your Price"  clientidmode="Static" />$
                            </div>
                        </div>
                      <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                                Quantity Order </label>
                            <div class="controls">
                                <input type="text" runat="server" clientidmode="Static" name="txtQuantity"  readonly id="txtQuantity" 
                                    class="input-large"  />
                            </div>
                        </div>
                          <div class="par control-group">
                            <label class="control-label" for="txtQuantityShipped">
                               Quantity Shipped </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtQuantityShipped"  onblur="QuanityShippedChange()" clientidmode="Static"  id="txtQuantityShipped" onkeypress="return onlyNumbers(event)"
                                    class="input-large"   />
                            </div>
                        </div>
                         <div class="par control-group">
                            <label class="control-label" for="txtQuantitybackOrder">
                               Quantity Back Order </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtQuantitybackOrder" clientidmode="Static" onblur="QuanityBackOrder()"  id="txtQuantitybackOrder" onkeypress="return onlyNumbers(event)"
                                    class="input-large"   />
                            </div>
                        </div>  
                       <div class="par control-group">
                            <label class="control-label" for="txtDisplayOrder">
                               Total Amount </label>
                            <div class="controls">
                                <input type="text" runat="server" name="txtAmount" clientidmode="Static" readonly id="txtAmount" onkeypress="return onlyNumbers(event)"
                                    class="input-large"   />$
                            </div>
                        </div>
                      <p class="stdformbutton">
                         <asp:Button ID="btnUpdateOrder" runat="server" class="btn btn-primary" 
                                 Text="Update" Width="150px" ValidationGroup="grpsubmit" OnClientClick="return ValidateQuantity()" OnClick="btnUpdateOrder_Click" />
                        </p>
                    </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btncancel" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
   <!-- Modal edit ends-->
</asp:Content>