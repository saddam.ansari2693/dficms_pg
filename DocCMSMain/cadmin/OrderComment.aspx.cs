﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using DocCMS.Core.DataTypes;

namespace DocCMSMain.cadmin
{
    public partial class OrderComment : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["PurchaseOrderID"] != null)
                {
                    Bind_QuoteComments();
                }
            }
        }


        protected void Bind_QuoteComments()
        {
            DataTable dtCompany = null;
            DataTable dt = null;
            try
            {
                dtCompany = ServicesFactory.DocCMSServices.Fetch_AllQuote_comment();
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    dt = ServicesFactory.DocCMSServices.Fetch_AllQuote_comment();
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrOrderDescription.DataSource = dt;
                    RptrOrderDescription.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/CreateQuote.aspx?PurchaseOrderID=" + Convert.ToString(Request.QueryString["PurchaseOrderID"]));
        }

        protected void btnAddDescriptionToOrder_Click(object sender, EventArgs e)
        {
            Int32 Count = 0;
            ServicesFactory.DocCMSServices.Delete_Quotescomment_From_quotecommentTable(Convert.ToInt32(Request.QueryString["PurchaseOrderID"]));
            foreach (RepeaterItem ri in RptrOrderDescription.Items)
            {
                CheckBox ChkActive = (CheckBox)ri.FindControl("ChkActive");
                Label lblQuoteHeading = (Label)ri.FindControl("lblQuoteHeading");
                Label lblQuoteComment = (Label)ri.FindControl("lblQuoteComment");
                TextBox txtOrder = (TextBox)ri.FindControl("txtOrder");
                if (ChkActive.Checked)
                {
                    Cls_Quotescomment obj = new Cls_Quotescomment();
                    obj.purchaseorderid = Convert.ToInt32(Request.QueryString["PurchaseOrderID"]);
                    obj.quotename = lblQuoteHeading.Text;
                    obj.quotecomment = lblQuoteComment.Text;
                    obj.dorder = Convert.ToInt32(txtOrder.Text);
                    Int32 RetVal = ServicesFactory.DocCMSServices.Insert_Quotescomment_to_quotecommentTable(obj);
                    if (RetVal > 0)
                        Count = 1;
                    else
                        return;
                }
                if (Count > 0)
                {
                }
            }
        }
    }
}