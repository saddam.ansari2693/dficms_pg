﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.cadmin
{
    public partial class SliderItemDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                if (Request.QueryString["SliderID"] != null && Request.QueryString["SliderID"] != "")
                {
                    hndSliderID.Value = Convert.ToString(Request.QueryString["SliderID"]);
                    Bind_SliderItems(Convert.ToInt16(Request.QueryString["SliderID"]));
                    if (Session["showtablesize"] != null)
                        ShowTableSize = Convert.ToString(Session["showtablesize"]);
                }
            }
        }

        // Bind all Plan Details
        protected void Bind_SliderItems(int SliderID)
        {
            try
            {
                RptrPlans.DataSource = "";
                RptrPlans.DataBind();
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_SliderItem_Master_image(SliderID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrPlans.DataSource = dt;
                    RptrPlans.DataBind();
                }
                else
                {
                    RptrPlans.DataSource = "";
                    RptrPlans.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void RptrPlans_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "AddSlide":
                        Response.Redirect("SliderItemDetailsDashboard.aspx?SliderItemID=" + e.CommandArgument.ToString() + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
                        break;
                    case "EditSlider":
                        Response.Redirect("SliderItemsPage.aspx?SliderItemID=" + e.CommandArgument.ToString() + "&SliderID=" + Convert.ToString(Request.QueryString["SliderID"]));
                        break;
                    case "DeleteSlider":
                        Int32 retval = ServicesFactory.DocCMSServices.Delete_SliderItemMaster_Data_By_slideritemid(Convert.ToInt32(e.CommandArgument));
                        int SliderId = Convert.ToInt16(hndSliderID.Value);
                        Bind_SliderItems(SliderId);
                        break;

                    default:
                        break;
                }
            }
        }

        protected void RptrPlans_databound(object sender, RepeaterItemEventArgs e)
        {
        }
        protected void btnAddSliderImage_Click(object sender, EventArgs e)
        {
            Response.Redirect("SliderItemsPage.aspx?SliderID=" + hndSliderID.Value);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("SliderManagementDashboard.aspx");
        }
    }
}