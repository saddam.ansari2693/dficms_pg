﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.cadmin
{
    public partial class SliderManagementDashboard : System.Web.UI.Page
    {
        dynamic UserId;
        string ShowTableSize;
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["linkname"] = "liadministration";
            Session["Ulname"] = "uladministration";
            UserId = (Session["UserID"]);
            if (!IsPostBack)
            {
                Bind_SliderDetails();
                if (Session["showtablesize"] != null)
                    ShowTableSize = Convert.ToString(Session["showtablesize"]);
                SetControls();
            }
        }


        public void SetControls()
        {
            DataTable dt = ServicesFactory.DocCMSServices.Get_Roles_Privileges_Based_on_SubModuleId(Convert.ToInt32(Session["SubModuleID"]), Convert.ToInt32(Session["RoleID"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsAdd"]))
                    btnAddSlider.Visible = true;
                else
                    btnAddSlider.Visible = false;
                foreach (RepeaterItem Item in RptrSlider.Items)
                {

                    LinkButton lnkSliderID = (LinkButton)Item.FindControl("lnkSliderID");
                    Label lblTitle = (Label)Item.FindControl("lblTitle");
                    ImageButton lnkEdit = (ImageButton)Item.FindControl("lnkEdit");
                    ImageButton lnkDelete = (ImageButton)Item.FindControl("lnkDelete");
                    ImageButton lnkNavigate = (ImageButton)Item.FindControl("lnkNavigate");
                    if (lblTitle != null && lnkSliderID != null && lnkEdit != null && lnkDelete != null && lnkNavigate != null)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsEdit"]))
                        {
                            lnkEdit.Enabled = true;
                            lnkSliderID.Visible = true;
                            lblTitle.Visible = false;
                            lnkNavigate.Enabled = true;
                        }
                        else
                        {
                            lnkEdit.Enabled = false;
                            lnkSliderID.Visible = false;
                            lblTitle.Visible = true;
                            lnkNavigate.Enabled = false;
                        }
                        if (Convert.ToBoolean(dt.Rows[0]["IsDelete"]))
                            lnkDelete.Enabled = true;
                        else
                            lnkDelete.Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/cadmin/Error404.aspx");
            }
        }
        // Bind all Plan Details
        protected void Bind_SliderDetails()
        {
            try
            {
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Slider_Master_Data();
                if (dt != null && dt.Rows.Count > 0)
                {
                    RptrSlider.DataSource = dt;
                    RptrSlider.DataBind();
                }
                else
                {
                    RptrSlider.DataSource = "";
                    RptrSlider.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void RptrSlider_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                Label lblDorder = (Label)e.Item.FindControl("lblDorder");
                switch (e.CommandName)
                {
                    case "ShowSlides":
                        Response.Redirect("SliderItemDashboard.aspx?SliderID=" + e.CommandArgument.ToString());
                        break;

                    case "AddSlide":
                        Response.Redirect("SliderItemDashboard.aspx?SliderID=" + e.CommandArgument.ToString());
                        break;
                    case "EditSlider":
                        Response.Redirect("SliderManagementPage.aspx?SliderID=" + e.CommandArgument.ToString());
                        break;
                    case "DeleteSlider":
                        Int32 retval = ServicesFactory.DocCMSServices.Delete_Slider_Master_Data_By_sliderid(Convert.ToInt32(e.CommandArgument));
                        Bind_SliderDetails();
                        Response.Redirect("SliderManagementDashboard.aspx");
                        break;

                    default:
                        break;
                }
            }
        }

        protected void RptrSlider_databound(object sender, RepeaterItemEventArgs e)
        {
        }
        protected void btnAddSlider_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/cadmin/SliderManagementPage.aspx");
        }
    }
}