﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.DFI
{
    public partial class Enterprise : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Products";
            if (!IsPostBack)
            {
                if (Request.QueryString["CMSID"] != null)
                {
                    Bind_Enterprise(Convert.ToInt32(Request.QueryString["CMSID"]));
                }
                else
                {
                }
            }
        }

        // Bind function for Enterprise System
        protected void Bind_Enterprise(Int32 CMSID)
        {
            try
            {
                //=========BINDING PRODUCT CONTENT
                StringBuilder strContent = new StringBuilder();
                DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(CMSID, "Products");
                if (dtDetail != null && dtDetail.Rows.Count > 0)
                {
                    if (Convert.ToString(dtDetail.Rows[5]["Products"]).ToUpper() == "TRUE")
                    {
                        string ProductName = Convert.ToString(dtDetail.Rows[0]["Products"]);
                        string ImageURL = "../UploadedFiles/ContentImages/Products/" + Convert.ToString(dtDetail.Rows[1]["Products"]);
                        string ProductDescription = Convert.ToString(dtDetail.Rows[2]["Products"]);
                        strContent.Append("<h1>" + ProductName + "<hr class='fancy-divider'></h1>");
                        strContent.Append("<div class='row' style='font-size:16px;'>");
                        strContent.Append("<div class='col-md-7'>" + ProductDescription + "");
                        strContent.Append("<div class='row'><div class='col-md-9'><br><a href='images/PDF/Enterprise/aug11-serviceIT.pdf' class='link'><img src='images/products/serviceit-icon.png' />");
                        strContent.Append("<strong style='margin-left:30px;'>Service IT</strong></a></div>");
                        strContent.Append("<div class='col-md-3'></div>");
                        strContent.Append("<div class='col-md-9'><a href='images/PDF/Enterprise/aug11-viewIT.pdf' class='link'><img src='images/products/viewit-icon.png' /><strong style='margin-left:30px;'>View IT</strong></a></div>");
                        strContent.Append("<div class='col-md-3'></div>");
                        strContent.Append("<div class='col-md-9'><a href='images/PDF/Enterprise/aug11-signIT.pdf' class='link'><img src='images/products/signit-icon.png' /><strong style='margin-left:30px;'>Sign IT</strong></a></div>");
                        strContent.Append("<div class='col-md-3'></div></div></div>");
                        strContent.Append(" <div class='col-md-5'><img src='" + ImageURL + "' alt='" + ProductName + "' class='img-responsive'>");
                        strContent.Append("<h3 style='text-align:center;'>" + ProductName + "</h3></div>  </div>");
                    }
                    else
                    {
                        Response.Redirect("Error404.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Error404.aspx");
                }
                DivProductContent.InnerHtml = strContent.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}