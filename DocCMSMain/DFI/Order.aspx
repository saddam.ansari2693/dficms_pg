﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="DocCMSMain.DFI.Order" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="css/PaymentCart.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/jscript">
function ShowShippingAddress() {
    if ($("#chkShippingAddress").is(':checked'))
        $("#divShippingAddress").hide();
    else
        $("#divShippingAddress").show();
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <span class="shadow-top"></span>
				<!-- shell -->
               <div class="container">
						<!-- testimonial -->
   					<section class="blog">
							<!-- content -->
                   			<div class="content" id="DivCart" runat="server" clientidmode="Static">
<div class="container">
  <div id="Checkout" class="inline">
      <h1> <span id="CustomerOrder" runat="server">Customer Order Information</span><asp:LinkButton ID="lnkBack" runat="server"
              style="float:right;" onclick="lnkBack_Click" CssClass="btn btn-danger"><i class="fa fa-shopping-cart"></i>  <span id="spnTopButton" runat="server">Continue Shopping</span></asp:LinkButton></h1>
      <form  autocomplete="off">
       <div class="card-row clearfix">
             <div style="float:left;"><label for="radioOrderType" runat="server" id="lblNote" >All Order are to be picked by the Customer</label></div> 
              <div class="input-container" style="float:left; width:50%;">
             </div>  
          </div>
          <br />
           <div class="expiry-date-group form-group">
              <label for="txtCompanyName" runat ="server" id="lblCompanyName">Company Name</label>
              <input id="txtCompanyName" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255"  />
          </div>
           <div class="security-code-group form-group">
              <label for="txtPurchaserName" runat ="server" id="lblPurchaserName">Purchaser's Name</label>
              <input id="txtPurchaserName" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" required />
          </div>
                 <div class="expiry-date-group form-group">
              <label for="txtPurchaseOrderNo" runat ="server" id="lblPurchaseOrderNo">Order No.</label>
              <div class="input-container" >
                     <input id="txtPurchaseOrderNo" runat="server" readonly clientidmode="Static" class="form-control" type="text" maxlength="255" onkeypress='validate(event)'  required />
              </div>
          </div>
          <hr />
                 <div class="security-code-group form-group">
              <label for="txtBillingAddress1" runat="server" id="lblAddress1">Billing Address 1</label>
              <input id="txtBillingAddress1" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" />
          </div>
           <div class="expiry-date-group form-group">
                   <label for="txtBillingAddress2" runat="server" id="lblAddress2" >Billing Address 2</label>
              <div class="input-container" >
                     <input id="txtBillingAddress2" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255"  />
              </div>
          </div>
           <div class="security-code-group form-group">
          <label for="txtBillingCityTown" runat="server" id="lblCityTown" >Billing City/Town</label>
              <input id="txtBillingCityTown" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" required />
             
          </div>
                 <div class="expiry-date-group form-group">
              <label for="txtProvience"  runat="server" id="lblProvince">Billing Province</label>
              <div class="input-container" >
                     <input id="txtBillingProvience" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" required />
              </div>
          </div>
          <div class="security-code-group form-group">
              <label for="txtPostalCode" runat="server" id="lblPostalCode">Billing Postal Code</label>
          <div class="input-container">
                  <input id="txtBillingPostalCode" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="10" onkeypress="javascript:return ValidatePostalCode(event)" />
              </div>
          </div>
           <div class="expiry-date-group form-group">
              <label for="txtFax" runat="server" id="lblFax">Fax</label>
              <div class="input-container" >
              <input id="txtFax" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" />
              </div>
          </div>
          <div class="security-code-group form-group">
              <label for="txtPhone" runat="server" id="lblPhone">Phone</label>
              <input id="txtPhone" runat="server" clientidmode="Static" class="form-control" type="text" onkeypress="javascript:return isNumber(event)" pattern=".{10,}" maxlength="10"  required title="Enter your 10 digits phone number" />
          </div>
           <div class="expiry-date-group form-group" >
              <label for="drpPayment" runat="server" id="lblPaymentmethod">Method of Payment</label>
              <div class="input-container" >
                  <asp:DropDownList ID="drpPayment" runat="server" ClientIDMode="Static" style="width:97%;">
                  </asp:DropDownList>
              </div>
          </div>
             <div class="security-code-group form-group" style="position:relative;">
              <label for="txtEmail" runat="server" id="lblEmail" >E-mail</label>
              <input id="txtEmail" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" required/>
             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter valid email Id"
                                        ControlToValidate="txtEmail" style="color: Red; font-size: 12px; position:absolute;" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
          </div>
           <div class=" security-code-group form-group">
          <label for="txtOtherDetail" runat ="server" id="lblOtherDetail">Other Details</label>
           <input id="txtotherDetail" runat="server" clientidmode="Static" class="form-control" type="text" />
             </div>
             <div class="expiry-date-group form-group" >
             <label for="txtPurchasingInformation" runat ="server" id="lblPurchasingInformation">PO #</label>
              <input id="txtPurchasingInformation" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255"  />
              </div>
             <div class="security-code-group form-group" >
            <label for="radiobackOrder" runat="server" id="Label1" >Shipping Address is Same</label>
            <div class="input-container" >
            <input type="checkbox" runat="server" name="activestatus" onchange="ShowShippingAddress()" id="chkShippingAddress" clientidmode="Static" class="input-large"
                                    style="opacity: 1;padding-right: 465px;" checked />
              </div>
              </div>
          <div class="security-code-group form-group" style="float:right !important;padding: 0 33px !important;">
          <label for="radiobackOrder" runat="server" id="lblAllowBack" >Allow Back Order</label>
               <div class="input-container" >
            <input type="checkbox" runat="server" name="activestatus"  id="ChkActiveStatus" clientidmode="Static" class="input-large"
                                    style="opacity: 1" checked />
              </div>
              </div>
              <div id="divShippingAddress" style="display:none">
                <div class="expiry-date-group form-group">
              <label for="txtShippingAddress1" runat="server" id="Label2">Shipping Address 1</label>
              <input id="txtShippingAddress1" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255" />
             </div>
             <div class="security-code-group form-group">
              <label for="txtShippingCity" runat="server" id="Label3" >Shipping City/Town</label>
              <input id="txtShippingCity" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255"  />
          </div>
           <div class="expiry-date-group form-group">
              <label for="txtShippingAddress2" runat="server" id="Label4" >Shipping Address 2</label>
              <div class="input-container" >
                     <input id="txtShippingAddress2" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255"  />
              </div>
          </div>
                 <div class="security-code-group form-group">
              <label for="txtShippingProvince"  runat="server" id="Label5">Shipping Province</label>
              <div class="input-container" >
                     <input id="txtShippingProvince" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="255"  />
              </div>
          </div>
                <div class="expiry-date-group form-group">
              <label for="txtShippingPostalCode" runat="server" id="Label6">Shipping Postal Code</label>
         <div class="input-container">
                  <input id="txtShippingPostalCode" runat="server" clientidmode="Static" class="form-control" type="text" maxlength="10" onkeypress="javascript:return ValidatePostalCode(event)" />
              </div>
          </div>
             </div>
        <div style="width:100%;">
               <asp:Button ID="PayButton" runat="server" Text="Place an Order"  ClientIDMode="Static"
               class="btn  btn-success submit-button " 
               style="cursor:pointer;width:20%; margin-left:333px;margin-bottom:50px; margin-top:30px;" onclick="PayButton_Click"></asp:Button>
               <span style="font-size:20px"><b>OR</b></span>
               <asp:Button ID="QuoteButton" runat="server" Text="Request Quote"  ClientIDMode="Static"
               class="btn  btn-info submit-button " 
               style="cursor:pointer;width:20%;height:41px; margin-left:5px;margin-bottom:50px; margin-top:30px;" onclick="QuoteButton_Click"></asp:Button>
          </div>
        <div>
        <span><b>&nbsp;&nbsp;Note:</b>&nbsp;We will send you the quote ones you request quote</span>
        </div><br />
      </form>
  </div>
      <asp:HiddenField ID="hdnPDFFileName" ClientIDMode="Static" runat="server" />    
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $(".top-nav").css("margin-top", "65px ! important")
    });
    function SuccessMsg() {
        var PDFUrl = $('#hdnPDFFileName').val();
        window.open(PDFUrl.toString(), 'Download');
        alert("Your Order  is placed successfully");
        var url = "../dfi/Store.aspx";
        $(location).attr('href', url);
    }

    function SuccessMsg2() {
        var PDFUrl = $('#hdnPDFFileName').val();
        window.open(PDFUrl.toString(), 'Download');
        alert("Your Quote  is placed successfully");
        var url = "../dfi/Store.aspx";
        $(location).attr('href', url);
   }
    function isNumber(evt) {
        var key;
        if (window.event) {
            key = window.event.keyCode;     //IE
        }
        else {
            key = evt.which;      //firefox              
        }
        if ((key >= 48 && key <= 57)) {
            return true;
        }
        else if (key == 0 || key == 8 || key == 118 || key == 45)//|| key == 9 || key == 27 || key == 32 || key == 35 || key == 46 || key == 59 || key == 64 || key == 38 || key == 39 || key == 40 || key == 41 || key == 46 || key == 91 || key == 93 || key == 123 || key == 125 || key == 127)) {
        {
            return true;
        }
        else {
            return false;
        }
    }

    function ValidatePostalCode(evt) {
        var key;
        if (window.event) {
            key = window.event.keyCode;     //IE
        }
        else {
            key = evt.which;      //firefox              
        }
        if ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 67 && key <= 122)) {
            return true;
        }
        else if (key == 0 || key == 8 || key == 118 || key == 45)//|| key == 9 || key == 27 || key == 32 || key == 35 || key == 46 || key == 59 || key == 64 || key == 38 || key == 39 || key == 40 || key == 41 || key == 46 || key == 91 || key == 93 || key == 123 || key == 125 || key == 127)) {
        {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script src="js/Pages/Paymentcart.js" type="text/javascript"></script>
 </div>
</section>
</div>
<style type="text/css">
  #header
{
display:none !important;
 }
.rbl input[type="radio"]
{
   margin-left: 10px;
   margin-right: 1px;
}

.HdrLogo
{
    height:125px;
}

input[type="radio"] {
    display: inline-block;
}

input[type="radio"] + label {
    color: #333;
    font-size: 16px;
}

select
{
    margin: 0;
}
 </style>
 <div class="cl">&nbsp;</div>
</asp:Content>

