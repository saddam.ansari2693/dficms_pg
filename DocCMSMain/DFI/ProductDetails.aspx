﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="DocCMSMain.DFI.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ChangeImage(ctrl) {
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Fetch_Color_By_ColorId",
                data: { "colorid": $(ctrl).val() },
                dataType: "json",
                async: false,
                success: function (data) {
                    var htmlUser = "";
                    if (data != null && data != "") {
                        jQuery("#imgProduct").attr("src", "../UploadedFiles/ColorImages/" + data);
                    }
                    else {
                    }
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        }

        function AddtoCart() {
            var ColorCheck = "";
            var SizeCheck = "";
            if (jQuery("#ddlColor").val() == "0") {
                ColorCheck = "";
            }
            else {
                ColorCheck = jQuery("#ddlColor option:selected").text();
            }
            if (jQuery("#ddlSize").val() == "0") {
                SizeCheck = "";
            }
            else {
                SizeCheck = jQuery("#ddlSize").val();
            }
            var categoryname = jQuery("#hdnCategory").val();
            var ProductName = jQuery("#hdnProductName").val();
            var ProductNumber = jQuery("#hdnProductNumber").val();
            var InternalDescription = jQuery("#hdnIntenalDescription").val();
            var description = jQuery("#lblDecription").html();
            var Price = jQuery("#hdnPrice").val();
            var Cost = jQuery("#hdnCost").val();
            var Color = ColorCheck;
            var Size = SizeCheck;
            var Qty = "1";
            var Cls_ShopingCart =
       {
        sessionid: $("#hdnSession1").val(),
        productname: ProductName,
        productnumber: ProductNumber,
        internaldescription:InternalDescription,
        category: categoryname,
        qty: Qty,
        description: description,
        cost: Cost,
        price: Price,
        runningprice: Price,
        size: Size,
        color: Color
      };
            var objshoppingcart = JSON.stringify(Cls_ShopingCart);
            jQuery.ajax({
                url: "../WebService/DocCMSApi.svc/Insert_Shopping_Temp_Cart_Detail",
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(Cls_ShopingCart),
                dataType: "json",
                success: ajaxSucceeded,
                error: ajaxFailed
            });
            function ajaxSucceeded(data) {
                if (data != null && data > 0) {
                window.location.href = "../DFI/Store.html";
                }
                else {
                    window.location.href = "../DFI/Store.html";
               }
            }
            function ajaxFailed() {
               alert("There is an error during operation.");
            }
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1 runat="server" id="lblProductName" clientidmode="Static"></h1>
                </div>
                <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="Home.html">Home</a></li>
                        <li class="active"><a href="Store.aspx">Store</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/#title-->
    <section id="Product" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="blog">
                    <div class="blog-item">
                        <img class="img-responsive img-blog" src="images/blog/blog2.jpg" runat="server" id="imgProduct" clientidmode="Static" alt="" style="width:30%; float:left;" />
                        <div class="blog-content" style="width:70%; float:right;">
                            <h3>Product Description</h3>  <h3 id="lblPrice" runat="server"></h3>
                             <asp:HiddenField EnableViewState="true" ID="hdnSession" runat="server" ClientIDMode="Static"
                            Value="" />
                              <asp:HiddenField EnableViewState="true" ID="hdnSession1" runat="server" ClientIDMode="Static"
                            Value="" />
                             <asp:HiddenField ID="hdnProductName" runat="server" ClientIDMode="Static"></asp:HiddenField>
                             <asp:HiddenField ID="hdnProductNumber" runat="server" ClientIDMode="Static"></asp:HiddenField>
                             <asp:HiddenField ID="hdnIntenalDescription" runat="server" ClientIDMode="Static"></asp:HiddenField>
                             <asp:HiddenField ID="hdnCategory" runat="server" ClientIDMode="Static"></asp:HiddenField> 
                             <asp:HiddenField ID="hdnPrice" runat="server"  ClientIDMode="Static"></asp:HiddenField>
                             <asp:HiddenField ID="hdnCost"  runat="server"  ClientIDMode="Static"></asp:HiddenField>
                             <div runat="server" id="lblDecription" clientidmode="Static">   
                             </div>
                            <hr>
                            <div id="divSize" runat="server" clientidmode="Static" style="display:none;">
                            <h3>Size</h3>                            
                            <p class="lead">
                            <asp:DropDownList ID="ddlSize" runat="server" ClientIDMode="Static">
                            </asp:DropDownList>
                            </p>
                            <hr>
                            </div>
                            <div id="divColor" runat="server" clientidmode="Static" style="display:none;">
                            <h3>Color</h3>                            
                            <p class="lead">
                            <asp:DropDownList ID="ddlColor" runat="server" ClientIDMode="Static" onchange="ChangeImage(this)">
                            </asp:DropDownList>
                            </p>
                            <hr>
                            </div>
                            <p><input type="button" id="btnAddTocart" class="btn btn-primary" value="Add To Cart" onclick="AddtoCart();" /></p>
                              <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo" id="btnViewDetail" runat="server">View Detail</button>
                           <div id="demo" class="collapse" runat="server" clientidmode="Static" style="font-size: 16px;margin: 11px 5px 10px 4px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
