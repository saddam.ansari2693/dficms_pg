﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="ProjectManagement.aspx.cs" Inherits="DocCMSMain.DFI.ProjectManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="divPublished"  runat="server" clientidmode="Static">
        <!-- Banner Section Starts Here -->
        <section class="about-banner" id="SectionBanner" runat="server"  >
            <img  runat="server" id="ImgBanner" alt="DOCFOCUS Product Banner" class="img-responsive" style="margin:0 auto" clientidmode="Static" >
        </section>
        <section class="dms-dfi">
            <div class="container">
            <h1 class="dms-heading" id="pageHeading" runat="server"></h1>
              <div class="container" id="DivProjectManagement" runat="server" >
                </div>
                <div  id="DivProjectCategory" runat="server">
                </div>
            </div>
        </section>
</div>
<div id="divunpublished" runat="server" visible="false" align="center">
    <div class="center">
      <h2 id="H1">The Page is under construction</h2>
    </div>
    <div class="gap"></div>
   <img src="images/underConstruction.jpg" />
   <div class="gap"></div>
   <div class="center">
     <h2 id="H2">Comming Soon...</h2>
   </div>
</div>
</asp:Content>
