﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.Implementation;
using System.Drawing.Imaging;
using System.Data;
using DocCMS.Core;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;

namespace DocCMSMain.DFI
{
    public partial class ContactUsImageGenerator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
  
            try
            {
                if (Request.QueryString["id"] == "5")
                {
                    Bind_Address();
                }
                else
                {
                    Bind_PhoneNo();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void Bind_PhoneNo()
        {
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(29);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {

                //creating a image object
                System.Drawing.Image bitmap = (System.Drawing.Image)Bitmap.FromFile(Server.MapPath("ContactUsbackground.jpg")); // set image 
                //draw the image object using a Graphics object
                Graphics graphicsImage = Graphics.FromImage(bitmap);

                //Set the alignment based on the coordinates   
                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;

                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Far;

                //Set the font color/format/size etc..  
                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#000000");//customise color adding
                StringBuilder strComInfo = new StringBuilder();

                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {

                        string PhoneHeading = "Phone";
                        string PhoneDescription = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                        graphicsImage.DrawString(PhoneHeading.ToString(), new Font("calibri", 17,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(70, 75),
                        stringformat); Response.ContentType = "image/jpeg";
                        graphicsImage.DrawString(PhoneDescription.ToString(), new Font("calibri", 17,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(465, 75),
                        stringformat); Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                     }
                }
            }
        }

        protected void Bind_Address()
        {
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(29);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {

                //creating a image object
                System.Drawing.Image bitmap = (System.Drawing.Image)Bitmap.FromFile(Server.MapPath("ContactUsBackGround22.jpg")); // set image 
                //draw the image object using a Graphics object
                Graphics graphicsImage = Graphics.FromImage(bitmap);

                //Set the alignment based on the coordinates   
                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;

                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Near;
                
                //Set the font color/format/size etc..  
                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#000");//customise color adding
                StringBuilder strComInfo = new StringBuilder();
                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {
                        string AddressHeading = "Contact Us:";
                        string Address = Convert.ToString(dtDetail.Rows[7]["PageContent"]);
                        Address = Address.Replace("<br />", "\n");
                        string newAddress = Regex.Replace(Address, @"<[^>]+>|&nbsp;", "").Trim();
                        graphicsImage.DrawString(AddressHeading.ToString(), new Font("calibri", 25,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(160,60),
                        stringformat); Response.ContentType = "image/jpeg";

                        graphicsImage.DrawString(newAddress.ToString().Trim(), new Font("calibri", 25,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(430,10),
                        stringformat2); Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                    }

                }
            }
        }
    }
}