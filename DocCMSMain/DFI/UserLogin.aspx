﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="UserLogin.aspx.cs" Inherits="DocCMSMain.DFI.UserLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
   <!-- Contact Us Section Starts Here -->
        <section class="contact-section">
            <div class="container">
                <h1>DFI User Login Page <hr class="fancy-divider"></h1>
                <div class="row">
                   <div class="col-md-3" runat="server" id="Div1"></div>
                    <div class="col-md-6" runat="server" id="DivContactForm">
                        <div class="con-form">
                            <strong>Enter User Cardinals to Access Download Page</strong>
                            <br />
                            <asp:Label ID="lblMsg" runat="server" Text="" style="color:Red" Visible="false">User Name or Password is Invalid.</asp:Label>
                            <span class="input input--minoru">
                            <label>User ID</label>
                                <input class="input__field input__field--minoru" type="text" id="txtUserId" placeholder="User Name" runat="server" required />                             
                            </span>
                            <span class="input input--minoru">
                              <label>Password</label>
                                <input runat="server" class="input__field input__field--minoru" type="password" id="txtPassword" placeholder="Password" required />
                            </span>        
                            <span class="input input--minoru">
                                 <asp:Panel ID="PnlCapthaImage" runat="server" GroupingText="Image Verification">
                                 <asp:Literal ID="FailureText" runat="server" Text="Invalid Captcha Image, please re-enter." Visible="false"></asp:Literal>
                               </asp:Panel>
                            </span>                    
                            <asp:Button CssClass="progress-button"  data-style="fill" data-horizontal clientidmode="Static" ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click"  /></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Us Section Ends Here -->
</asp:Content>
