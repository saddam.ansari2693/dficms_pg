﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class FAQ : System.Web.UI.Page
    {
        string CurrSubPage = "";
        string CurrPage = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CurrPage"] != null && Session["CurrSubPage"] != null)
            {
                CurrPage = Convert.ToString(Session["CurrPage"]);
                CurrSubPage = Convert.ToString(Session["CurrSubPage"]);
                liRightHeading.InnerHtml = liRightHeading.InnerHtml + " / " + Convert.ToString(Session["CurrSubPage"]);
            }
            if (!IsPostBack)
            {
                Bind_Product_FAQ(44);
            }
        }

        // Bind FAQ function 
        protected void Bind_Product_FAQ(Int32 PageID)
        {
            try
            {
                DataTable dtFAQ = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtFAQ != null && dtFAQ.Rows.Count > 0)
                {
                    StringBuilder strFaqs = new StringBuilder();
                    Int32 NumberCount = 0;
                    for (Int32 i = 0; i < dtFAQ.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtFAQ.Rows[i]["CMSID"]), "FieldValue");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Question = Convert.ToString(dtDetail.Rows[0]["FieldValue"]);
                            string Answer = Convert.ToString(dtDetail.Rows[1]["FieldValue"]);
                            string DisplayOrder = Convert.ToString(dtDetail.Rows[3]["FieldValue"]);
                            string Category = Convert.ToString(dtDetail.Rows[2]["FieldValue"]).ToUpper();
                            if (Category == CurrSubPage.ToUpper())
                            {
                                error.Visible = false;                                
                                NumberCount = NumberCount + 1;
                                if (NumberCount < 10)
                                {
                                    strFaqs.Append(" <li><span class='number'>0" + NumberCount + "</span>");
                                }
                                else
                                {
                                    strFaqs.Append(" <li><span class='number'>" + NumberCount + "</span>");
                                }
                                strFaqs.Append("<div> <h4>" + Question + "</h4>");
                                strFaqs.Append("<p>" + Answer + "</p></div></li>");
                            }
                            else
                            {
                            }
                        }
                    }
                    UlFaq.InnerHtml = strFaqs.ToString();
                    if (!string.IsNullOrEmpty(UlFaq.InnerHtml))
                    {
                        UlFaq.Visible = true;
                        error.Visible = false;
                    }
                    else
                    {
                        error.Visible = true;
                        UlFaq.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}