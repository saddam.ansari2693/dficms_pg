﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class ProductDescription : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "productdescription";

            if (!IsPostBack)
            {
                string Product = Request.QueryString[0].ToString().Split('/')[1];
                DataTable dtCMSID = ServicesFactory.DocCMSServices.Get_cmsid_by_Projectcategoryname(88, Product);
                if (dtCMSID != null && dtCMSID.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dtCMSID.Rows.Count; i++)
                    {
                        Int32 CMSID = Convert.ToInt32(dtCMSID.Rows[i]["CMSID"]);
                        Bind_CompanyPage(75, CMSID);
                    }
                }
            }
        }
        // Bind Products Function
        protected void Bind_CompanyPage(Int32 PageID, Int32 CMSID)
        {

            try
            {
                StringBuilder strSocialMedia = new StringBuilder();
                DataTable dtSocialMediaContent = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(CMSID);
                if (dtSocialMediaContent != null && dtSocialMediaContent.Rows.Count > 0)
                {
                    ImgBanner.Src = "../UploadedFiles/ContentImages/ProjectCategoryDetail/" + Convert.ToString(dtSocialMediaContent.Rows[2]["PageContent"]) + "";
                    headinginfo.InnerHtml = Convert.ToString(dtSocialMediaContent.Rows[1]["PageContent"]);
                    CompanyDescription.InnerHtml = Convert.ToString(dtSocialMediaContent.Rows[3]["PageContent"]);
                }
                else
                {
                    divunpublished.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}