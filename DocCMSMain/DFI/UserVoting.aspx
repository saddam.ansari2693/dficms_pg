﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="UserVoting.aspx.cs" Inherits="DocCMSMain.DFI.UserVoting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/Popupstylesheet.css" rel="stylesheet" type="text/css" />
    <link href="css/UserVotingStyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>
    <script type="text/javascript">
         window.onload = function DisplayIP() {
               document.getElementById("hdnClientIPAddress").value = myip;
         };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
<a class='product-apply' href='#userPoll'" onclick="btn">Apply <i class='fa fa-check-circle'></i></a>
<asp:Button ID="btnVoting" runat="server" Text="Button"  style="display:none;" 
        onclick="btnVoting_Click" ClientIDMode="Static" />
<asp:Button ID="btnclick" runat="server" Text="Button"  style="display:none;" 
        onclick="btnclick_Click" ClientIDMode="Static" />
        <div Id="DivMain" runat="server">
    <div id="userPoll"> 
	   				<div class="white">
		<div class="blue-sec" id="DivSurvey" runat="server">
		</div>
		<p class="last">DocFocus</p>        
		</div>
		 <a href="UserVoting.aspx" class="cancel" >&times;</a> 
		</div>
		 <div id="cover" > </div>
</div>
<script type="text/javascript">
   function Voting(SurveyID, SurveyQuestionID, AnswerType) {
        var selectedVal = "";
        var checkedvalue = "";
        var arrAns = "";
        var Ans="";
        var ResponseText="";
        var ResponseType=""
        var  AdditionalText="";
        var checkboxValues = [];
        var IPAddress = $("#hdnClientIPAddress").val().toString();
        var Ques = SurveyQuestionID;
        // for Radio 
        if (AnswerType == "1") {
            var selected = $('input[name=radio]:checked');
            if (selected.length > 0) {
                selectedVal = selected.attr("id");
                arrAns = selectedVal.toString().split('_');
                    Ans=arrAns[1].toString()+",";
                    ResponseText = $('#txtradio_'+arrAns[1].toString()+'').text();
                  ResponseType="Option";
            }
        }

        //---------------------- for checkbox--------//
        else if (AnswerType == "2") {
              $('input[name=checkbox]:checked').map(function () {
                    checkboxValues.push($(this).val());
                 });
                   for(var i=0;i<checkboxValues.length;i++)
                    {
                      Ans=Ans+checkboxValues[i].toString()+",";
                       ResponseText =ResponseText+ $('#txtcheck_'+checkboxValues[i].toString()+'').text()+"~";
                        ResponseType="Check";
                    }
             }

        //----------For Textbox-------------------//
        else {
          ResponseText = $('textarea#message_'+SurveyQuestionID).val();
          AdditionalText="";
          Ans=",";
          ResponseType="Text";
        }

        //-------- Save Response-------------//
        if(Ans.length>0)
        {
        jQuery.ajax({
            contentType: "application/json; charset=utf-8",
            url: "/WebService/DocCMSApi.svc/Insert_Survey_Vote",
            data: { "SurveyID": SurveyID.toString(),
                "IPAddress": IPAddress.toString(),
                "SurveyQuestionID": Ques.toString(),
                "Ans": Ans.toString(),
                "ResponseText": ResponseText.toString(),
                "AdditionalText": AdditionalText.toString(),
                "ResponseType": ResponseType.toString(),
            },
            dataType: "json",
            success: function (data) {
               jQuery("#btnVoting").click();
            },
            error: function (result) {
                alert("Error during operatrion");
            }
         });
        }
        else
        {
           alert("Please Select Atleast one Option");
        }
    }
</script>
</asp:Content>
