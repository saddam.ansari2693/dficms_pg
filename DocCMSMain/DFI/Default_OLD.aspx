﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterOld.Master" AutoEventWireup="true"
    CodeBehind="Default_OLD.aspx.cs" Inherits="DocCMSMain.DFI.Default_OLD" %>

<%@ Register Src="~/Controls/HomePageSlider.ascx" TagName="HomePageSlider" TagPrefix="Uc1" %>
<%@ Register Src="~/Controls/SocialMedia.ascx" TagName="SocialMedia" TagPrefix="UC2" %>
<%@ Register Src="~/Controls/KeyElements.ascx" TagName="KeyElements" TagPrefix="UC3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <Uc1:HomePageSlider ID="slider1" runat="server"></Uc1:HomePageSlider>
    <!-- About Us Section Starts Here -->
    <div runat="server" id="DivAboutUs">
    </div>
    <!-- About Us section Ends Here -->
    <!-- Elements Of DMS Section Starts Here -->
    <UC3:KeyElements ID="KeyElement1" runat="server" Visible="false"></UC3:KeyElements>
     <UC2:SocialMedia ID="SocialMedia1" runat="server"></UC2:SocialMedia>
     <section id="pricing">
        <div class="container">
            <div class="center">
                <h2>See our Pricings</h2>
                <p class="lead">choose the best for you.</p>
            </div> 
            <div class="gap"></div>
            <div id="pricing-table" class="row">
                <div class="col-md-3 col-xs-6">
                    <ul class="plan plan1">
                        <li class="plan-name">
                            <h3>Basic</h3>
                        </li>
                        <li class="plan-price">
                            <div>
                                <span class="price"><sup>$</sup>29</span>
                                <small>month</small>
                            </div>
                        </li>
                        <li>
                            <strong>5GB</strong> Storage
                        </li>
                        <li>
                            <strong>1GB</strong> RAM
                        </li>
                        <li>
                            <strong>400GB</strong> Bandwidth
                        </li>
                        <li>
                            <strong>10</strong> Email Address
                        </li>
                        <li>
                            <strong>Forum</strong> Support
                        </li>
                        <li class="plan-action">
                            <a href="#" class="btn btn-primary btn-md">Signup</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <ul class="plan plan2 featured">
                        <li class="plan-name">
                            <h3>Standard</h3>
                        </li>
                        <li class="plan-price">
                            <div>
                                <span class="price"><sup>$</sup>39</span>
                                <small>month</small>
                            </div>
                        </li>
                        <li>
                            <strong>5GB</strong> Storage
                        </li>
                        <li>
                            <strong>1GB</strong> RAM
                        </li>
                        <li>
                            <strong>400GB</strong> Bandwidth
                        </li>
                        <li>
                            <strong>10</strong> Email Address
                        </li>
                        <li>
                            <strong>Forum</strong> Support
                        </li>
                        <li class="plan-action">
                            <a href="#" class="btn btn-primary btn-md">Signup</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <ul class="plan plan3">
                        <li class="plan-name">
                            <h3>Advanced</h3>
                        </li>
                        <li class="plan-price">
                            <div>
                                <span class="price"><sup>$</sup>49</span>
                                <small>month</small>
                            </div>
                        </li>
                        <li>
                            <strong>50GB</strong> Storage
                        </li>
                        <li>
                            <strong>8GB</strong> RAM
                        </li>
                        <li>
                            <strong>1024GB</strong> Bandwidth
                        </li>
                        <li>
                            <strong>Unlimited</strong> Email Address
                        </li>
                        <li>
                            <strong>Forum</strong> Support
                        </li>
                        <li class="plan-action">
                            <a href="#" class="btn btn-primary btn-md">Signup</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3 col-xs-6">
                    <ul class="plan plan4">
                        <li class="plan-name">
                            <h3>Mighty</h3>
                        </li>
                        <li class="plan-price">
                            <div>
                                <span class="price"><sup>$</sup>99</span>
                                <small>month</small>
                            </div>
                        </li>
                        <li>
                            <strong>50GB</strong> Storage
                        </li>
                        <li>
                            <strong>8GB</strong> RAM
                        </li>
                        <li>
                            <strong>1024GB</strong> Bandwidth
                        </li>
                        <li>
                            <strong>Unlimited</strong> Email Address
                        </li>
                        <li>
                            <strong>Forum</strong> Support
                        </li>
                        <li class="plan-action">
                            <a href="#" class="btn btn-primary btn-md">Signup</a>
                        </li>
                    </ul>
                </div>
            </div> 
        </div>
    </section><!--/#pricing-->
</asp:Content>
