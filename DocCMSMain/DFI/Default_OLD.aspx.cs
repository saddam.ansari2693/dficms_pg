﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class Default_OLD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_AboutUs(15);
        }
        protected void Bind_AboutUs(Int32 PageID)
        {
            try
            {
                DataTable dtAboutUs = ServicesFactory.DocCMSServices.Fetch_Menu_Dashboard_Bypagename(PageID);
                if (dtAboutUs != null && dtAboutUs.Rows.Count > 0)
                {
                    StringBuilder strAboutUs = new StringBuilder();
                    for (Int32 i = 0; i < dtAboutUs.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtAboutUs.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string Detail = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string LinkName = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                            strAboutUs.Append(" <section class='about-section'>");
                            strAboutUs.Append("<h1>"+Heading+"<hr class='fancy-divider'></h1>");
                            strAboutUs.Append("<div class='container'>"+Detail+" </div>   </section>");
                        }
                    }
                    DivAboutUs.InnerHtml = strAboutUs.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void Bind_Benefits(Int32 PageID)
        {
            try
            {
                DataTable dtBenefits = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtBenefits != null && dtBenefits.Rows.Count > 0)
                {
                    StringBuilder strBenefits = new StringBuilder();
                    strBenefits.Append("  <div id='owl-demo' class='owl-carousel owl-theme'>");
                    for (Int32 i = 0; i < dtBenefits.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtBenefits.Rows[i]["CMSID"]), "PageContent");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string Detail = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string ImageURL = "../UploadedFiles/ContentImages/DMSBenefits/" + Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            if (Detail.Length > 80)
                            {
                                Detail = Detail.Substring(0, 80);
                            }
                            if (i != 0)
                            {
                                strBenefits.Append(" <div class='item benefit'>");
                                strBenefits.Append("<img src='" + ImageURL + "' alt='" + Heading + "' class='benefit-icon img-responsive'>");
                                strBenefits.Append("<h3 style='margin-top:25px;'>" + Heading + "</h3>");
                                strBenefits.Append(" <p>" + Detail + "...</p><a class='read_more' href='Benefits.aspx'>Read More</a></div>");
                            }
                            else
                            {
                            }
                        }
                    }
                    strBenefits.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}