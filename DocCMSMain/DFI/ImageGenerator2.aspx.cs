﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.Implementation;
using System.Drawing.Imaging;
using System.Data;
using DocCMS.Core;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;

namespace DocCMSMain.DFI
{
    public partial class ImageGenerator2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindOfficeAddress(Convert.ToInt32(Request.QueryString["id"]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void BindOfficeAddress(Int32 CMSID)
        {
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(73);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {

                //creating a image object
                System.Drawing.Image bitmap = (System.Drawing.Image)Bitmap.FromFile(Server.MapPath("ContactUsBackGround22-Copy.jpg")); // set image 
                //draw the image object using a Graphics object
                Graphics graphicsImage = Graphics.FromImage(bitmap);
                //Set the alignment based on the coordinates   
                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;
                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Near;
                //Set the font color/format/size etc..  
                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#000");//customise color adding
                StringBuilder strComInfo = new StringBuilder();
                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(CMSID);
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {
                        string Address = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                        Address = Address.Replace("<br />", "\n");
                        string newAddress = Regex.Replace(Address, @"<[^>]+>|&nbsp;", "").Trim();
                        graphicsImage.DrawString(newAddress.ToString().Trim(), new Font("calibri", 25,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(50, 5),
                        stringformat2); Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                    }
                }
            }
        }

        protected void Bind_HeadOffice()
        {
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(73);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {

                //creating a image object
                System.Drawing.Image bitmap = (System.Drawing.Image)Bitmap.FromFile(Server.MapPath("ContactUsBackGround22-Copy.jpg")); // set image 
                //draw the image object using a Graphics object
                Graphics graphicsImage = Graphics.FromImage(bitmap);
                //Set the alignment based on the coordinates   
                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;
                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Near;
                //Set the font color/format/size etc..  
                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#000");//customise color adding
                StringBuilder strComInfo = new StringBuilder();
                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {
                        string Address = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                        Address = Address.Replace("<br />", "\n");
                        string newAddress = Regex.Replace(Address, @"<[^>]+>|&nbsp;", "").Trim();
                        graphicsImage.DrawString(newAddress.ToString().Trim(), new Font("calibri", 25,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(50, 5),
                        stringformat2); Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                    }
                }
            }
        }

        protected void Bind_SaleBranch()
        {
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(73);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {

                //creating a image object
                System.Drawing.Image bitmap = (System.Drawing.Image)Bitmap.FromFile(Server.MapPath("ContactUsBackGround22-Copy.jpg")); // set image 
                //draw the image object using a Graphics object
                Graphics graphicsImage = Graphics.FromImage(bitmap);

                //Set the alignment based on the coordinates   
                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;

                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Near;

                //Set the font color/format/size etc..  
                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#000");//customise color adding
                StringBuilder strComInfo = new StringBuilder();
                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {
                        string Address = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                        Address = Address.Replace("<br />", "\n");
                        string newAddress = Regex.Replace(Address, @"<[^>]+>|&nbsp;", "").Trim();
                        graphicsImage.DrawString(newAddress.ToString().Trim(), new Font("calibri", 25,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(50, 5),
                        stringformat2); Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                    }

                }
            }
        }

        protected void Bind_IndiaBranch()
        {
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(73);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {

                //creating a image object
                System.Drawing.Image bitmap = (System.Drawing.Image)Bitmap.FromFile(Server.MapPath("ContactUsBackGround22-Copy.jpg")); // set image 
                //draw the image object using a Graphics object
                Graphics graphicsImage = Graphics.FromImage(bitmap);
                //Set the alignment based on the coordinates   
                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;
                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Near;
                //Set the font color/format/size etc..  
                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#000");//customise color adding
                StringBuilder strComInfo = new StringBuilder();
                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {
                        string Address = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                        Address = Address.Replace("<br />", "\n");
                        string newAddress = Regex.Replace(Address, @"<[^>]+>|&nbsp;", "").Trim();
                        graphicsImage.DrawString(newAddress.ToString().Trim(), new Font("calibri", 25,
                        FontStyle.Regular), new SolidBrush(StringColor), new Point(50, 5),
                        stringformat2); Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                    }
                }
            }
        }
    }
}