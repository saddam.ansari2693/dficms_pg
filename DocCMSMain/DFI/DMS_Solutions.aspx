﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="DMS_Solutions.aspx.cs" Inherits="DocCMSMain.DFI.DMS_Solutions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <!-- Banner Section Starts Here -->
        <section class="about-banner" id="SectionBanner" runat="server" visible="false" >
            <img  runat="server" src="images/banners/dms.jpg" id="ImgBanner" alt="DOCFOCUS Product Banner" class="img-responsive" style="margin:0 auto" clientidmode="Static" >
        </section>
          <section class="dms-dfi">
            <div class="container">
                <div class="container" id="DivDMS" runat="server" visible="false">
                     </div>
                <div class="container" id="SectionKeyElement" runat="server" visible="false">
                   <h2 id="headingkey" runat="server"> <hr class="fancy-divider"></h2>
                   <div  id="DivKeyElements" runat="server">
                </div>
                </div>
            </div>
        </section>
</asp:Content>
