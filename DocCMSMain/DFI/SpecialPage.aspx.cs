﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.DFI
{
    public partial class SpecialPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ID"] != null)
            {

                DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(Request.QueryString["ID"]));
                string PageName = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                DataTable dtCMSID = ServicesFactory.DocCMSServices.Get_cmsid_by_Projectcategoryname(90, PageName);
                if (dtCMSID != null && dtCMSID.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dtCMSID.Rows.Count; i++)
                    {
                        Int32 CMSID = Convert.ToInt32(dtCMSID.Rows[i]["CMSID"]);
                        Bind_SpecialPage(90, CMSID);
                    }
                }
            }
        }

        protected void Bind_SpecialPage(Int32 PageID, Int32 CMSID)
        {
            try
            {
                StringBuilder strSocialMedia = new StringBuilder();
                DataTable dt = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(CMSID);
                if (dt != null && dt.Rows.Count > 0)
                {
                  SectionSpecialPage.InnerHtml = Convert.ToString(dt.Rows[2]["PageContent"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
