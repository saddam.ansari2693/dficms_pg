﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.DFI
{
    public partial class Sitemap : System.Web.UI.Page
    {
        protected static string CMSID = "";
        protected static string MainMenu = "";
        protected static string MainSubMenu = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Menus(1);
            }
        }

        //Binding main Menu inside rptrMainMenu Repeater Control
        protected void Bind_Menus(Int32 PageID)
        {
            try
            {
                DataTable dtMainMenu = ServicesFactory.DocCMSServices.Fetch_Main_Menu_Dashboard_Bypagename(PageID);
                if (dtMainMenu != null && dtMainMenu.Rows.Count > 0)
                {
                    rptrSitemapMainMenu.DataSource = dtMainMenu;
                    rptrSitemapMainMenu.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptrSitemapMainMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                //Start Get Page name
                string path = HttpContext.Current.Request.Url.AbsolutePath;
                string[] PageNameArray = path.Split('/');
                string PageName = PageNameArray.Last();
                //End Get Page name
                Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                HtmlGenericControl liMenuName = (HtmlGenericControl)e.Item.FindControl("liMenuName");
                HtmlGenericControl LiMainSubMenu = (HtmlGenericControl)e.Item.FindControl("LiMainSubMenu");

                LinkButton lnkNavigation = (LinkButton)e.Item.FindControl("lnkNavigation");
                Repeater rptrSitemapSubMenu = (Repeater)e.Item.FindControl("rptrSitemapSubMenu");
                Literal LitMenuName = (Literal)e.Item.FindControl("LitMenuName");
                if (rptrSitemapSubMenu != null && lblCMSID != null && liMenuName != null && lnkNavigation != null && LiMainSubMenu != null && LitMenuName != null)
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        liMenuName.Attributes.Add("class", "active");
                    }
                    DataTable dtMenu = ServicesFactory.DocCMSServices.Fetch_Menu_Detail_By_cmsid(Convert.ToInt32(lblCMSID.Text));
                    if (dtMenu != null && dtMenu.Rows.Count > 0)
                    {
                        lnkNavigation.CommandArgument = Convert.ToString(dtMenu.Rows[1]["MenuName"]).Trim();
                        lnkNavigation.Text = Convert.ToString(dtMenu.Rows[0]["MenuName"]).Trim();
                        LitMenuName.Text = Convert.ToString(dtMenu.Rows[0]["MenuName"]).Trim();
                        if (Session["MemberID"] != null)
                        {
                            if (Convert.ToString(dtMenu.Rows[0]["MenuName"]).Trim() == "Member Login")
                            {
                                liMenuName.Attributes.Add("class", "");
                                liMenuName.Visible = false;
                            }
                        }
                        if (Session["CurrSubPage"] != null && !string.IsNullOrEmpty(MainMenu) && MainMenu == LitMenuName.Text)
                        {
                            liMenuName.Attributes.Add("class", "");
                            LiMainSubMenu.Attributes.Add("class", "active");
                            Session["CurrPage"] = null;
                        }
                        else if (Session["CurrPage"] != null)
                        {
                            if (Convert.ToString(dtMenu.Rows[0]["MenuName"]).Trim() == Convert.ToString(Session["CurrPage"]).Trim())
                            {
                                liMenuName.Attributes.Add("class", "active");
                            }
                            else
                            {
                                liMenuName.Attributes.Add("class", "");
                            }
                        }
                        else
                        {
                            if (Convert.ToString(dtMenu.Rows[0]["MenuName"]).Trim() == "Home")
                            {
                                liMenuName.Attributes.Add("class", "active");
                            }
                            else
                            {
                                liMenuName.Attributes.Add("class", "");
                            }
                        }
                    }
                    else
                    {
                        liMenuName.Visible = false;
                    }
                    Int32 SubMenuCount = ServicesFactory.DocCMSServices.Fetch_Submenucount_By_Menuname(lnkNavigation.Text);
                    if (SubMenuCount > 0)
                    {
                        liMenuName.Visible = false;
                        LiMainSubMenu.Visible = true;

                        DataTable dtSubMenu = ServicesFactory.DocCMSServices.Fetch_SubMenuactiveStatus_By_Menuname(lnkNavigation.Text);
                        if (dtSubMenu != null && dtSubMenu.Rows.Count > 0)
                        {
                            dtSubMenu.Columns.Add("MainMenuName", typeof(string));
                            for (Int32 i = 0; i < dtSubMenu.Rows.Count; i++)
                            {
                                dtSubMenu.Rows[i]["MainMenuName"] = lnkNavigation.Text;
                            }
                            dtSubMenu.AcceptChanges();
                            rptrSitemapSubMenu.DataSource = dtSubMenu;
                            rptrSitemapSubMenu.DataBind();
                        }
                    }
                    else
                    {
                        // Do nothing
                    }
                }
                //Selected menu Remove when communityinformationandregistrationday page show
                if (PageName.ToLower() == "CommunityInformationAndRegistrationDay.aspx".ToLower())
                {
                    liMenuName.Attributes.Add("class", "");
                }
                if (PageName.ToLower() == "sitemap.aspx".ToLower())
                {
                    liMenuName.Attributes.Add("class", "");
                }
            }
        }

        protected void rptrSitemapMainMenu_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                HtmlGenericControl LiMainSubMenu = (HtmlGenericControl)e.Item.FindControl("LiMainSubMenu");
                LinkButton lnkNavigation = (LinkButton)e.Item.FindControl("lnkNavigation");
                if (e.CommandName == "NavigateToURL")
                {

                    Session["CurrSubPage"] = null;
                    string navURL = "";
                    if (lnkNavigation != null)
                    {
                        Session["CurrPage"] = lnkNavigation.Text;
                    }
                    if (e.CommandArgument.ToString().ToLower().Contains("default") || e.CommandArgument.ToString().ToLower().Contains("default.aspx"))
                    {
                        navURL = e.CommandArgument.ToString().ToLower().Replace("default", "home");
                    }
                    else
                    {
                        navURL = e.CommandArgument.ToString();
                    }
                    //Start Call to MenuName
                    Session["Navigation"] = Convert.ToString(lnkNavigation.Text);
                    //Start Call to MenuName
                    if (navURL.ToString().Trim() == "#" || navURL.ToString().Trim().IndexOf("#") > -1)
                    {
                        Response.Redirect(Convert.ToString(navURL.ToString().Replace(".aspx", ".html")));
                    }
                    else
                    {
                        Response.Redirect("~" + Convert.ToString(navURL.ToString().Replace(".aspx", ".html")));
                    }
                }
            }
        }

        protected void rptrSitemapSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblSubMenuName = (Label)e.Item.FindControl("lblSubMenuName");
                Label lblSubmenuNavURL = (Label)e.Item.FindControl("lblSubmenuNavURL");
                LinkButton lnkSubmenu = (LinkButton)e.Item.FindControl("lnkSubmenu");
                Label lblMainMenuName = (Label)e.Item.FindControl("lblMainMenuName");
                Literal LitSubMenuLevel1 = (Literal)e.Item.FindControl("LitSubMenuLevel1");
                HtmlGenericControl divSubMenuLevel1 = (HtmlGenericControl)e.Item.FindControl("divSubMenuLevel1");
                Repeater rptrSubMenuLevel2 = (Repeater)e.Item.FindControl("rptrSubMenuLevel2");
                if (lblSubMenuName != null && lblSubmenuNavURL != null && lnkSubmenu != null && lblMainMenuName != null)
                {
                    lnkSubmenu.Text = lblSubMenuName.Text;
                    LitSubMenuLevel1.Text = lblSubMenuName.Text;
                    lnkSubmenu.CommandArgument = lblSubmenuNavURL.Text;
                    if (Session["MemberID"] != null)
                    {
                        if (Convert.ToString(lnkSubmenu.Text) == "Create FAQ")
                            lnkSubmenu.Visible = true;
                    }
                    else
                    {
                        if (Convert.ToString(lnkSubmenu.Text) == "Create FAQ")
                            lnkSubmenu.Visible = false;
                    }
                }
                Int32 SubMenuCount = ServicesFactory.DocCMSServices.Fetch_SubMenuLevel2Count_By_Menuname(lblSubMenuName.Text);
                if (SubMenuCount > 0)
                {
                    lnkSubmenu.Visible = false;
                    DataTable dtSubMenu = ServicesFactory.DocCMSServices.Fetch_SubMenuLevel2activeStatus_By_Menuname(lblSubMenuName.Text);
                    if (dtSubMenu != null && dtSubMenu.Rows.Count > 0)
                    {
                        rptrSubMenuLevel2.DataSource = dtSubMenu;
                        rptrSubMenuLevel2.DataBind();
                    }
                }
                else
                {
                    divSubMenuLevel1.Visible = false;
                }
            }
        }

        protected void rptrSitemapSubMenu_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                if (e.CommandName == "SubNavigateToURL")
                {
                    LinkButton lnkSubmenu = (LinkButton)e.Item.FindControl("lnkSubmenu");
                    Label lblCMSID = (Label)e.Item.FindControl("lblCMSID");
                    Label lblSubMenuName = (Label)e.Item.FindControl("lblSubMenuName");
                    if (lnkSubmenu != null && lblCMSID != null)
                    {
                        Session["CurrSubPage"] = lnkSubmenu.Text;
                        DataTable dtMenuName = ServicesFactory.DocCMSServices.Fetch_Menuname_By_submodulename(Convert.ToInt32(lblCMSID.Text));
                        if (dtMenuName != null && dtMenuName.Rows.Count > 0)
                        {
                            MainMenu = Convert.ToString(dtMenuName.Rows[0][0]);
                            Session["CurrPage"] = MainMenu;
                        }
                    }
                    if (e.CommandArgument.ToString().Trim() == "#" || e.CommandArgument.ToString().Trim().IndexOf("#") > -1)
                    {
                        Response.Redirect(Convert.ToString(e.CommandArgument.ToString().Replace(".aspx", ".html")));
                    }
                    else
                    {
                        Response.Redirect(Convert.ToString("~" + e.CommandArgument.ToString().Replace(".aspx", ".html")));
                    }
                }
            }
        }


        //Start Sub Menu Level 2//

        protected void rptrSubMenuLevel2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblSubMenuName = (Label)e.Item.FindControl("lblSubMenuLevel2Name");
                Label lblSubmenuNavURL = (Label)e.Item.FindControl("lblSubmenuNavURLLevel2");
                LinkButton lnkSubmenuLevel2 = (LinkButton)e.Item.FindControl("lnkSubmenuLevel2");
                if (lblSubMenuName != null && lblSubmenuNavURL != null && lnkSubmenuLevel2 != null)
                {
                    lnkSubmenuLevel2.Text = lblSubMenuName.Text;
                    lnkSubmenuLevel2.CommandArgument = lblSubmenuNavURL.Text;
                    if (Session["MemberID"] != null)
                    {
                        if (Convert.ToString(lnkSubmenuLevel2.Text) == "Create FAQ")
                            lnkSubmenuLevel2.Visible = true;
                    }
                    else
                    {
                        if (Convert.ToString(lnkSubmenuLevel2.Text) == "Create FAQ")
                            lnkSubmenuLevel2.Visible = false;
                    }
                }
            }
        }

        protected void rptrSubMenuLevel2_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                if (e.CommandName == "SubLevel2NavigateToURL")
                {
                    LinkButton lnkSubmenuLevel2 = (LinkButton)e.Item.FindControl("lnkSubmenuLevel2");
                    Label lblSubLevel2CMSId = (Label)e.Item.FindControl("lblSubLevel2CMSId");
                    if (lblSubLevel2CMSId != null && lblSubLevel2CMSId != null)
                    {
                        Session["CurrSubPage"] = lblSubLevel2CMSId.Text;
                        DataTable dtMenuName = ServicesFactory.DocCMSServices.Fetch_Menuname_By_submodulename(Convert.ToInt32(lblSubLevel2CMSId.Text));
                        if (dtMenuName != null && dtMenuName.Rows.Count > 0)
                        {
                            MainMenu = Convert.ToString(dtMenuName.Rows[0][0]);
                            Session["CurrPage"] = MainMenu;
                        }
                    }
                    if (e.CommandArgument.ToString().Trim() == "#" || e.CommandArgument.ToString().Trim().IndexOf("#") > -1)
                    {
                        Response.Redirect(Convert.ToString(e.CommandArgument.ToString().Replace(".aspx", ".html")));
                    }
                    else
                    {
                        Response.Redirect(Convert.ToString("~" + e.CommandArgument.ToString().Replace(".aspx", ".html")));
                    }
                }
            }
        }
    }
}