﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class Benefits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["CurrSubPage"] = "Benefits";
            if(!IsPostBack)
            {
                Bind_Content(57);
            }
        }
        protected void Bind_Content(Int32 PageID)
        {
            try
            {
                DataTable dtBenefits = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage(PageID);
                if (dtBenefits != null && dtBenefits.Rows.Count > 0)
                {
                    StringBuilder strBenefits = new StringBuilder();
                    for (Int32 i = 0; i < dtBenefits.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtBenefits.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string Detail = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string ImageURL = "../UploadedFiles/ContentImages/BenefitsContent/" + Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            ImgBanner.Src = ImageURL;
                            strBenefits.Append("<h1 class='feature-heading'>" + Heading + "<hr class='fancy-divider'></h1>");
                            strBenefits.Append("<p>" + Detail + "</p>");
                        }
                    }
                    DivContent.InnerHtml = strBenefits.ToString();
                    if (!string.IsNullOrEmpty(DivContent.InnerHtml))
                    {
                        Bind_Benefits(16);
                    }
                }
                else
                {
                    Response.Redirect("Error404.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Bind_Benefits(Int32 PageID)
        {
            try
            {
                DataTable dtBenefits = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtBenefits != null && dtBenefits.Rows.Count > 0)
                {
                    StringBuilder strBenefits = new StringBuilder();
                    for (Int32 i = 0; i < dtBenefits.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtBenefits.Rows[i]["CMSID"]), "PageContent");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string Detail = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            strBenefits.Append(" <h3>" + Heading + " </h3>");
                            strBenefits.Append("<p>" + Detail + "</p>");
                        }
                    }
                   DivBenefits.InnerHtml = strBenefits.ToString();
                 }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}