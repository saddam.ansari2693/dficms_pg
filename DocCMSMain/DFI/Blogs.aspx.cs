﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Text;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Drawing;

namespace DocCMSMain.DFI
{
    public partial class Blogs : System.Web.UI.Page
    {
        public static string AccessType = "";
         readonly PagedDataSource _pgsource = new PagedDataSource();
        int _firstIndex, _lastIndex;
        private int _pageSize = 2;
        private int CurrentPage
        {
            get
            {
                if (ViewState["CurrentPage"] == null)
                {
                    return 0;
                }
                return ((int)ViewState["CurrentPage"]);
            }
            set
            {
                ViewState["CurrentPage"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //Retriving Search Result from Session Table
            DataTable SearchData = (DataTable)Session["SearchData"];
           
            if (!IsPostBack)
            {
                if ((DataTable)Session["SearchData"] != null)
                {
                    string Pagename = Convert.ToString(Session["CurrPage"]);
                    if (Session["UserID"] != null)
                        AccessType = "Internal";
                    else
                        AccessType = "Public";
                    if (SearchData != null && SearchData.Rows.Count > 0)
                    {
                        rptBlogs.DataSource = SearchData;
                        rptBlogs.DataBind();
                        error.Visible = false;
                        DivKey.Visible = false;
                        Session["SearchData"] = null;
                    }
                    else
                    {
                        Session["SearchData"] = null;
                        error.Visible = true;
                        DivKey.Visible = false;
                        rptBlogs.Visible = false;
                    }
                }
                else
                {
                    Session["SearchData"] = null;
                    DataTable dt = null;
                    Bind_Blogs(dt);
                    BindDataIntoRepeater();
                }
            }
        }
        private void Bind_Blogs(DataTable dt)
        {
            if (Request.QueryString["CategoryID"] != null)
            {
                dt = ServicesFactory.DocCMSServices.Fetch_Blog_Details_FrontEnd_BY_Blog_categoryid(Convert.ToInt32(Request.QueryString["CategoryID"]),AccessType);
                if (dt != null && dt.Rows.Count > 0)
                {
                    rptBlogs.DataSource = dt;
                    rptBlogs.DataBind();
                    error.Visible = false;
                    DivKey.Visible = true;
                }
                else
                {
                    error.Visible = true;
                    DivKey.Visible = false;
                }
            }
            else
            {

                dt = ServicesFactory.DocCMSServices.Fetch_Blog_Details_FrontEnd(AccessType);
                if (dt != null && dt.Rows.Count > 0)
                {
                    rptBlogs.DataSource = dt;
                    rptBlogs.DataBind();
                }
            }
        }
        protected void OnItemDataBound_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton lnkNavigationURL = (LinkButton)e.Item.FindControl("lnkNavigationURL");
            Label lblName = (Label)e.Item.FindControl("lblName");
            if (lblName != null)
            {
                if (e.CommandName == "Read")
                    Response.Redirect("blog-" + lblName.Text.Trim() + ".html");
            }
        }
        protected void rptBlogs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                HtmlImage ImageMain = (HtmlImage)e.Item.FindControl("blogImage");
                Label lblImage = (Label)e.Item.FindControl("lblImage");
                Label lblDesc = (Label)e.Item.FindControl("lblDescription");
                HiddenField hdnBlogType = (HiddenField)e.Item.FindControl("hdnBlogType");
                HiddenField hdnBlogID = (HiddenField)e.Item.FindControl("hdnBlogID");
                Label lblCommentCount = (Label)e.Item.FindControl("lblCommentCount");
                HtmlGenericControl divVideoPlayer = (HtmlGenericControl)e.Item.FindControl("divVideoPlayer");
                if (hdnBlogID != null && lblCommentCount != null)
                {
                    lblCommentCount.Text = Convert.ToString(ServicesFactory.DocCMSServices.Count_comments(Convert.ToInt32(hdnBlogID.Value)));
                }
                int Total = lblDesc.Text.Length;
                if (Total > 80)
                {
                    lblDesc.Text = lblDesc.Text + "..";
                }
                if (ImageMain != null && lblImage != null)
                {

                    ImageMain.Src = "../UploadedFiles/UploadedFiles/Blogs/" + lblImage.Text;
                }
                if (hdnBlogType.Value != null && hdnBlogType.Value != "")
                {
                    if (hdnBlogType.Value == "1")
                    {
                        ImageMain.Visible = true;
                        divVideoPlayer.Visible = false;
                    }
                    else
                    {
                        ImageMain.Visible = false;
                        divVideoPlayer.Visible = true;
                    }
                }
                else
                {
                    ImageMain.Visible = true;
                    divVideoPlayer.Visible = false;
                }
            }
        }

        // Bind PagedDataSource into Repeater
        private void BindDataIntoRepeater()
        {
            DataTable dt = null;
            if (Request.QueryString["CategoryID"] != null)
            {
                dt = ServicesFactory.DocCMSServices.Fetch_Blog_Details_FrontEnd_BY_Blog_categoryid(Convert.ToInt32(Request.QueryString["CategoryID"]), AccessType);
            }
            else
            {
                dt = ServicesFactory.DocCMSServices.Fetch_Blog_Details_FrontEnd(AccessType);
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                _pgsource.DataSource = dt.DefaultView;
                _pgsource.AllowPaging = true;
                // Number of items to be displayed in the Repeater
                _pgsource.PageSize = _pageSize;
                _pgsource.CurrentPageIndex = CurrentPage;
                // Keep the Total pages in View State
                ViewState["TotalPages"] = _pgsource.PageCount;
                // Example: "Page 1 of 10"
                lblpage.Text = "Page " + (CurrentPage + 1) + " of " + _pgsource.PageCount;
                // Enable First, Last, Previous, Next buttons
                lbPrevious.Enabled = !_pgsource.IsFirstPage;
                lbNext.Enabled = !_pgsource.IsLastPage;
                lbFirst.Enabled = !_pgsource.IsFirstPage;
                lbLast.Enabled = !_pgsource.IsLastPage;

                // Bind data into repeater
                rptBlogs.DataSource = _pgsource;
                rptBlogs.DataBind();

                // Call the function to do paging
                HandlePaging();
                error.Visible = false;
                DivKey.Visible = true;
            }
            else
            {
                error.Visible = true;
                DivKey.Visible = false;
            }
        }

        private void HandlePaging()
        {
            var dt = new DataTable();
            dt.Columns.Add("PageIndex"); //Start from 0
            dt.Columns.Add("PageText"); //Start from 1
            _firstIndex = CurrentPage - 5;
            if (CurrentPage > 5)
                _lastIndex = CurrentPage + 5;
            else
                _lastIndex = 10;

            // Check last page is greater than total page then reduced it 
            // to total no. of page is last index
            if (_lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
            {
                _lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
                _firstIndex = _lastIndex - 10;
            }

            if (_firstIndex < 0)
                _firstIndex = 0;

            // Now creating page number based on above first and last page index
            for (var i = _firstIndex; i < _lastIndex; i++)
            {
                var dr = dt.NewRow();
                dr[0] = i;
                dr[1] = i + 1;
                dt.Rows.Add(dr);
            }
            rptPaging.DataSource = dt;
            rptPaging.DataBind();
        }

        protected void lbFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 0;
            BindDataIntoRepeater();
        }
        protected void lbLast_Click(object sender, EventArgs e)
        {
            CurrentPage = (Convert.ToInt32(ViewState["TotalPages"]) - 1);
            BindDataIntoRepeater();
        }
        protected void lbPrevious_Click(object sender, EventArgs e)
        {
            CurrentPage -= 1;
            BindDataIntoRepeater();
        }
        protected void lbNext_Click(object sender, EventArgs e)
        {
            CurrentPage += 1;
            BindDataIntoRepeater();
        }
        protected void rptPaging_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (!e.CommandName.Equals("newPage")) return;
            CurrentPage = Convert.ToInt32(e.CommandArgument.ToString());
            BindDataIntoRepeater();
        }
        protected void rptPaging_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            var lnkPage = (LinkButton)e.Item.FindControl("lbPaging");
            if (lnkPage.CommandArgument != CurrentPage.ToString()) return;
            lnkPage.Enabled = false;
            lnkPage.ForeColor = Color.FromName("#00FF00");
        }
    }
}