﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;

namespace DocCMSMain.DFI
{
    public partial class TestEmail : System.Web.UI.Page
    {
        public NetworkCredential smtpCredentials { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSMTPServer.Text = "smtp.gmail.com";
                txtSubject.Text = "DOCFOCUS Test Email";
                txtportNumber.Text = "587";
                chkSSL.Checked = true;
                txtFromUser.Text = "shaperslogix@gmail.com";
                txtPassword.Text = "logix123";
            }
        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            Send_Email();
        }
        private void Send_Email()
        {
            try
            {

                var fromAddress = new MailAddress(txtFromUser.Text.Trim(), txtFromUser.Text.Trim());
                var toAddress = new MailAddress(txtToEmail.Text.Trim(), txtToEmail.Text.Trim());
                string fromPassword = txtPassword.Text.Trim();
                string subject = txtSubject.Text.Trim();
                string body = txtBody.Text.Trim();
                var smtp = new SmtpClient
                {
                    Host = txtSMTPServer.Text.Trim(),
                    Port = Convert.ToInt32(txtportNumber.Text.Trim()),
                    EnableSsl = chkSSL.Checked,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                lblConfirmation.Text = "Email Sent Successfully";
            }
            catch (Exception ex)
            {
                lblConfirmation.Text = ex.Message.ToString();
            }
            //===========End here =================
        }
    }// Class Ends here
}