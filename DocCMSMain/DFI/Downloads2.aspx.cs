﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class Downloads2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AppUserID"] != null )
            {
                if (!IsPostBack)
                {
                    if (Session["AppUserID"] != null)
                    {
                       
                          
                           Bind_Downloads(30);
                                  
                               
                        
                    }
                    else
                    {
                        Response.Redirect("UserLogin.html");
                    }
                }
            }
            else
            {
                Response.Redirect("Userlogin.html");
            }
        }
        // To bind the document for the DFI user
        protected void Bind_Downloads(Int32 PageID)
        {
            try
            {
                DataTable dtDownloads = ServicesFactory.DocCMSServices.Fetch_Main_Menu_Dashboard_Bypagename(PageID);
                if (dtDownloads != null && dtDownloads.Rows.Count > 0)
                {
                    StringBuilder strDownloads = new StringBuilder();
                    for (Int32 i = 0; i < dtDownloads.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtDownloads.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string FileName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string NavigationURL = "../UploadedFiles/ContentImages/Downloads/" + Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string IsActive = Convert.ToString(dtDetail.Rows[3]["PageContent"]).ToUpper();
                            if (IsActive == "TRUE")
                            {
                                strDownloads.Append(" <li> <a href='" + NavigationURL + "' class='link clearfix' target='_blank'><span class='link-text'>" + FileName + "</span> ");
                                strDownloads.Append(" <span class='link-img'>  <img src='images/icons/Download.png' alt='Download' class='img-responsive'> </span>");
                                strDownloads.Append("</a> </li>");
                            }
                        }
                    }
                    SectionDownload.InnerHtml = strDownloads.ToString();
                    if (!string.IsNullOrEmpty(SectionDownload.InnerHtml))
                    {

                    }
                    else
                    {
                        Response.Redirect("Error404.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }// Class Ends Here
}