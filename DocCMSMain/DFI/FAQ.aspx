﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="DocCMSMain.DFI.FAQ" %>
<%@ Register Src="~/Controls/SurveyPopup.ascx" TagName="SurveyPopup" TagPrefix="UC4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/Pages/SurveyPopup.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>FAQs</h1>
                </div>
                <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="Home.html">Home</a></li>
                        <li class="active" id="liRightHeading" runat="server" clientidmode="Static">FAQ</li>                         
                    </ul>
                </div>
            </div>
        </div>
    </section><!--/#title-->   
   <div class="container" style="min-height:400px;" runat="server">
         <section id="error" class="container" visible="false" runat="server" style=" font-size:16px;">
        <h1>No Result Found</h1>
        <p>The Page you are looking for doesn't exist...</p>
        <a class="btn btn-success" href="Home.html">GO BACK TO THE HOME PAGE</a>
    </section>
        <section id="faqs" class="container" runat="server">
        <ul class="faq unstyled" id="UlFaq" runat="server" style="font-size:16px;">
        </ul>
    </section><!--#faqs-->
    </div>
    <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
    <UC4:SurveyPopup id="surveyHome" runat="server"></UC4:SurveyPopup>
    <script type="text/javascript">
        var IPAddress = "";
        $(document).ready(function () {
            DisplayIP();
            $("#cover").show();
            $("#userPoll").show();
            Bind_Survey();
        });
        $(document).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $("#userPoll").css("top", ($(document).scrollTop() + 135).toString() + "px");
            }
        });
        function DisplayIP() {
            document.getElementById("hdnClientIPAddress").value = myip;
            IPAddress = myip;
        };
    </script>
    <script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>
</asp:Content>
