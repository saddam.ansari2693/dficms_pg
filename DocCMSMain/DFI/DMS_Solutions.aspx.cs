﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class DMS_Solutions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e) 
        {
            Session["CurrSubPage"] = "DMS Solutions";
            if (!IsPostBack)
            {
                Bind_DMSSolution(12);
            }
        }

        // Bind Products Function
        protected void Bind_DMSSolution(Int32 PageID)
        {
            try
            {
                DataTable dtProducts = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage(PageID);
                if (dtProducts != null && dtProducts.Rows.Count > 0)
                {
                    StringBuilder strContent = new StringBuilder();
                    for (Int32 i = 0; i < dtProducts.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtProducts.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            foreach (DataRow rw in dtDetail.Rows)
                            {
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "SELECT BANNER IMAGE")
                                    ImgBanner.Src = "../UploadedFiles/ContentImages/DMSSolutions/" + Convert.ToString(rw["PageContent"]);

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PAGE NAME")
                                    strContent.Append("<h1 class='dms-heading'>" + Convert.ToString(rw["PageContent"]) + " <hr class='fancy-divider'></h1>");
                                
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PAGE DESCRIPTION")
                                    strContent.Append(Convert.ToString(rw["PageContent"]));

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "DMS SOLUTION SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    DivDMS.Visible = true;

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "BANNER IMAGE" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    SectionBanner.Visible = true;

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "KEY ELEMENT TITLE")
                                   headingkey.InnerText=Convert.ToString(rw["PageContent"]);

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "KEY ELEMENTS DESCRIPTION")
                                      DivKeyElements.InnerHtml = Convert.ToString(rw["PageContent"]);

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "KEY ELEMENTS SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                   SectionKeyElement.Visible=true;
                            }
                        }
                    }
                    DivDMS.InnerHtml = strContent.ToString();
                }
                else
                {
                    Response.Redirect("Error404.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}