﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="ProductHistory.aspx.cs" Inherits="DocCMSMain.DFI.ProductHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <!-- Banner Section Starts Here -->
        <section class="about-banner">
            <img src="images/banners/dms_new.jpg" alt="About DOCFOCUS Banner" class="img-responsive">
        </section>
        <section class="about-dfi">
            <div class="container" id="DivProductContent" runat="server">
            </div>
        </section>
        <!-- product Docfocus Section Ends Here -->
</asp:Content>
