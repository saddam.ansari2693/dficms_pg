﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="SDM.aspx.cs" Inherits="DocCMSMain.DFI.SDM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
hr {
       margin-top: 4px !important;
       margin-bottom: 5px !important;
}
.fontdesign
{
    cursor: pointer;
    /* font-size: 24px; */
    font-size: 17px;
    line-height: 50px;
}
.img-responsive
{
    width: 50px;
    float: right;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <section class="dload-section" style="display:none">
            <div class="container">
                <h1 class="dload-heading">Downloads <hr class="fancy-divider"></h1>
                <div class="container" id="DivDownloads" runat="server">
                <ul class='dloads' id="SectionDownload" runat="server">
                </ul>
                </div>
            </div>
        </section>
        <br />
        <br />
        <br />
        <div class="container">
         <div class="col-lg-6">
          <ul style="list-style-type: none;">
           <li id="LiMainSubMenu">                     
                      <h3>ISDM Information :</h3>
                      <h4>Part 1</h4>
                       <ul style="list-style-type: none;">
                       <li id="df">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="LnkNavigationSubMenu" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="ss">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="LnkNavigationSubMenu" target="_blank" style="cursor:pointer; " class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                        <h4>Part 2</h4>
                       <ul style="list-style-type: none;">
                       <li id="Li16">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A11" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li17">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A12" target="_blank" style="cursor:pointer;" class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                    </li>
                    <li id="Li12">    
                     <h3>msds: </h3>
                       <ul style="list-style-type: none;">
                       <li id="Li1">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A1" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li2">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A2" target="_blank" style="cursor:pointer;" class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                    </li>
                    <li id="Li13">    
                     <h3>msds: </h3>
                      <ul style="list-style-type: none;">
                       <li id="Li3">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A3" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li4">
                          <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A4" target="_blank" style="cursor:pointer;" class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                    </li>
                    </ul>

         </div>
          <div class="col-lg-6">
         <ul style="list-style-type: none;">
       <li id="Li5">                     
                      <h3>ISDM Information :</h3>
                      <h4>Part 1</h4>
                       <ul style="list-style-type: none;">
                       <li id="Li6">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A5" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li7">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A6" target="_blank" style="cursor:pointer; " class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                        <h4>Part 2</h4>
                       <ul style="list-style-type: none;">
                       <li id="Li8">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A7" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li9">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A8" target="_blank" style="cursor:pointer;" class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                    </li>
                    <li id="Li10">    
                     <h3>msds: </h3>
                       <ul style="list-style-type: none;">
                       <li id="Li11">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A9" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li14">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A10" target="_blank" style="cursor:pointer;" class="fontdesign">NAPA</a>
                        <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                       </li><hr />
                      </ul>
                    </li>
                    <li id="Li15">    
                     <h3>msds: </h3>
                      <ul style="list-style-type: none;">
                       <li id="Li18">
                         <a href="http://www.ca.airliquide.com/en/gases-for-our-customers/msds.html" id="A13" target="_blank" style="cursor:pointer;" class="fontdesign">Air Liquide</a>
                          <img src="images/icons/Download.png" alt="Download" class="img-responsive">
                         </li><hr />
                        <li id="Li19">
                       <a href="https://www.uapinc.com/msds/MSDSSearch.asp" id="A14" target="_blank" style="cursor:pointer;" class="fontdesign">NAPA</a>
                        <a href="#"><img src="images/icons/Download.png" alt="Download" class="img-responsive"></a>
                       </li><hr />
                      </ul>
                    </li>
                    </ul>
             </div>
        </div>
</asp:Content>
