﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Text;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using RMA.Core.DataTypes;
using System.Drawing.Drawing2D;
namespace DocCMSMain.DFI
{
    public partial class BlogDetails : System.Web.UI.Page
    {
        public int BlogID { get; set; }
        public static string AccessType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["BlogID"] != null)
                {
                    hndBlogId.Value = Request.QueryString["BlogID"];
                    if (Session["MemberID"] != null)
                    {
                        hdnCurUserID.Value = Convert.ToString(Session["MemberID"]);
                        AccessType = "Internal";
                        ConvertedVideoShow(Convert.ToString(Session["MemberID"]));
                    }
                    else
                    {
                        hdnCurUserID.Value = "0";
                        AccessType = "Public";
                    }
                    Bind_Blogs();
                    //Hit Counter
                    Int32 TruncateRetval = 0;
                    TruncateRetval = ServicesFactory.DocCMSServices.Truncate_TempVideo_ConvertedVideo();
                }
            }
        }

        public void ConvertedVideoShow(string MemberID)
        {
            DataTable dt = ServicesFactory.DocCMSServices.Fetch_All_Converted_VideosList(MemberID);
            if (dt != null && dt.Rows.Count > 0)
            {
                DivVideo.Visible = true;
                FinalVideoPlayer.Attributes.Add("src", dt.Rows[0]["VideoName"].ToString());
                FinalVideoPlayer.Attributes.Add("poster", dt.Rows[0]["CoverImage"].ToString());
            }
            else
            {
                DivVideo.Visible = false;
                FinalVideoPlayer.Attributes.Add("src", "");
                FinalVideoPlayer.Attributes.Add("poster", "");
            }
        }


        //Count Hits Function

        //Bind blog
        private void Bind_Blogs()
        {
            if (Request.QueryString["BlogID"] != null)
            {
                BlogID = Convert.ToInt32(Request.QueryString["BlogID"]);
                DataTable dt = new DataTable();
                dt = ServicesFactory.DocCMSServices.Fetch_Blog_Details_FrontEnd_BYid(BlogID, AccessType);
                if (dt != null && dt.Rows.Count > 0)
                {
                    rptBlogs.DataSource = dt;
                    rptBlogs.DataBind();
                    if (!string.IsNullOrEmpty(dt.Rows[0]["AllowPosting"].ToString()))
                    {
                        if (Convert.ToString(dt.Rows[0]["AllowPosting"]) == "True")
                        {
                            CommentSection.Visible = true;
                        }
                        else
                        {
                            CommentSection.Visible = false;
                        }
                    }
                    else
                    {
                        CommentSection.Visible = false;
                    }
                }
            }
        }
        protected void rptBlogs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                HtmlImage ImageMain = (HtmlImage)e.Item.FindControl("blogImage");
                Label lblImage = (Label)e.Item.FindControl("lblImage");
                HiddenField hdnBlogType = (HiddenField)e.Item.FindControl("hdnBlogType");
                Label lblCommentCount = (Label)e.Item.FindControl("lblCommentCount");
                HtmlGenericControl divVideoPlayer = (HtmlGenericControl)e.Item.FindControl("divVideoPlayer");
                if (hndBlogId != null && lblCommentCount != null)
                {
                    lblCommentCount.Text = Convert.ToString(ServicesFactory.DocCMSServices.Count_comments(Convert.ToInt32(hndBlogId.Value)));
                }
                if (ImageMain != null && lblImage != null)
                {
                    ImageMain.Src = "../UploadedFiles/UploadedFiles/Blogs/" + lblImage.Text;
                }
                if (hdnBlogType.Value != null && hdnBlogType.Value != "")
                {
                    if (hdnBlogType.Value == "1")
                    {
                        ImageMain.Visible = true;
                        divVideoPlayer.Visible = false;
                    }
                    else
                    {
                        ImageMain.Visible = false;
                        divVideoPlayer.Visible = true;
                    }
                }
                else
                {
                    ImageMain.Visible = true;
                    divVideoPlayer.Visible = false;
                }
            }
        }

        protected void btnDownloadOriginal_Click(object sender, EventArgs e)
        {
            string OrigFilePath = hdnOrigImage.Value.Replace("/ResizedImage/", "/Original/").Replace("/Customed/", "/Original/").Replace("/Croped/", "/Original/");
            string[] downFileNameArray = hdnOrigImage.Value.Replace("/ResizedImage/", "/Original/").Replace("/Customed/", "/Original/").Replace("/Croped/", "/Original/").Split('/');
            string downFileName = downFileNameArray[downFileNameArray.Length - 1];
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition","attachment; filename=" + downFileName + ";");
            response.TransmitFile(Server.MapPath(OrigFilePath));
            response.Flush();
            response.End();
            ClientScript.RegisterStartupScript(this.GetType(), "", "<script>CloseZoomModel();</script>");
        }

        protected void btnUpdloadVideo_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 Retval = 0;
                Int32 TruncateRetval = 0;
                DataTable DtCovertedVideo = null;
                string CoverImage = hdnCoverImage.Value;
                string VideoName = "";
                string ImageName = "";
                string OldCoverImagePath = "";
                string OldVideoPath = "";
                string NewCoverImagePath = "";
                string NewCoverImagePathWithImage = "";
                string NewVideoPathWithImage = "";
                string NewVideoPath = "";
                DtCovertedVideo = ServicesFactory.DocCMSServices.GetConvertedVideo();
                if (DtCovertedVideo != null && DtCovertedVideo.Rows.Count > 0)
                {
                    OldCoverImagePath = Convert.ToString(DtCovertedVideo.Rows[0]["CoverImage"]);
                    OldVideoPath = Convert.ToString(DtCovertedVideo.Rows[0]["VideoName"]);
                }
                CoverImage = CoverImage.Split(new string[] { "base64," }, StringSplitOptions.None)[1];
                byte[] data = Convert.FromBase64String(CoverImage);
                Stream strm = new MemoryStream(data);
                GenerateThumbnails(0.5, strm, HttpContext.Current.Server.MapPath(OldCoverImagePath));
                if (DtCovertedVideo != null && DtCovertedVideo.Rows.Count > 0)
                {
                    NewCoverImagePathWithImage = Convert.ToString(DtCovertedVideo.Rows[0]["CoverImage"]);
                    NewVideoPathWithImage = Convert.ToString(DtCovertedVideo.Rows[0]["VideoName"]);
                    ImageName = NewCoverImagePathWithImage.Split('/').Last();
                    VideoName = NewVideoPathWithImage.Split('/').Last();
                    NewCoverImagePath = NewCoverImagePathWithImage.Replace(ImageName, "");
                    NewVideoPath = NewVideoPathWithImage.Replace(VideoName, "");
                }
                NewCoverImagePath = NewCoverImagePath.Replace("/TempVideo/", "/UploadedFiles/");
                NewVideoPath = NewVideoPath.Replace("/TempVideo/", "/UploadedFiles/");
                NewCoverImagePathWithImage = NewCoverImagePathWithImage.Replace("/TempVideo/", "/UploadedFiles/");
                NewVideoPathWithImage = NewVideoPathWithImage.Replace("/TempVideo/", "/UploadedFiles/");
                if (!File.Exists(HttpContext.Current.Server.MapPath(NewCoverImagePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(NewCoverImagePath));
                }
                if (!File.Exists(HttpContext.Current.Server.MapPath(NewVideoPath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(NewVideoPath));
                }
                if (File.Exists(HttpContext.Current.Server.MapPath(NewCoverImagePath)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath(NewCoverImagePath));
                }
                if (File.Exists(HttpContext.Current.Server.MapPath(NewVideoPath)))
                {
                    File.Delete(HttpContext.Current.Server.MapPath(NewVideoPath));
                }
                File.Copy(HttpContext.Current.Server.MapPath(OldCoverImagePath), HttpContext.Current.Server.MapPath(NewCoverImagePathWithImage));
                File.Copy(HttpContext.Current.Server.MapPath(OldVideoPath), HttpContext.Current.Server.MapPath(NewVideoPathWithImage));
                Cls_comment objComment = new Cls_comment();
                objComment.memberid = Convert.ToString(hdnCurUserID.Value);
                objComment.blogid = Convert.ToInt32(hndBlogId.Value);
                objComment.videocoverimageurl = NewCoverImagePathWithImage;
                objComment.videourl = NewVideoPathWithImage;
                objComment.accesstype = ddlPostingAccessVideo.SelectedValue;
                Retval = ServicesFactory.DocCMSServices.Insert_comments_Video(objComment);
                TruncateRetval = ServicesFactory.DocCMSServices.Truncate_TempVideo_ConvertedVideo();
                ConvertedVideoShow(Convert.ToString(hdnCurUserID.Value));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private static void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            try
            {
                using (var image = System.Drawing.Image.FromStream(sourcePath))
                {
                    var newWidth = (int)(640);
                    var newHeight = (int)(480);
                    var thumbnailImg = new Bitmap(newWidth, newHeight);
                    var thumbGraph = Graphics.FromImage(thumbnailImg);
                    thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                    thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                    thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                    thumbGraph.DrawImage(image, imageRectangle);
                    thumbnailImg.Save(targetPath, image.RawFormat);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSelectCoverImage_Click(object sender, EventArgs e)
        {
            try
            {
                Int64 Retval = 0;
                string CoverImage = hdnCoverImage.Value;
                string CoverImagePath = hdnVideoCoverImagePath.Value;
                string VideoPath = hdnVideoVideoPath.Value;
                string FileName = hndVideoFileName.Value;
                CoverImage = CoverImage.Split(new string[] { "base64," }, StringSplitOptions.None)[1];
                byte[] data = Convert.FromBase64String(CoverImage);
                Stream strm = new MemoryStream(data);
                GenerateThumbnails(0.5, strm, HttpContext.Current.Server.MapPath(CoverImagePath));
                Cls_TempcommentVideoConverted objTempCommentVideoConverted = new Cls_TempcommentVideoConverted();
                objTempCommentVideoConverted.sessionid = Convert.ToString(Session.SessionID);
                objTempCommentVideoConverted.memberid = Convert.ToString(hdnCurUserID.Value);
                objTempCommentVideoConverted.videoname = VideoPath;
                objTempCommentVideoConverted.coverimage = CoverImagePath;
                Retval = ServicesFactory.DocCMSServices.Insert_Temp_comment_Video_Converted(objTempCommentVideoConverted);
                ConvertedVideoShow(Convert.ToString(hdnCurUserID.Value));
                hdnCoverImage.Value = (Convert.ToString(hdnCoverImage.Value));
                Session["MemberID"] = hdnCurUserID.Value;
                if (Retval == null && Retval == 0)
                {
                    return;
                }
                else
                {
                    //nothing
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnDownloadVideo_Click(object sender, EventArgs e)
        {
            string[] downFileNameArray = hndVideoFileNameForDownload.Value.Split('/');
            string downFileName = downFileNameArray[downFileNameArray.Length - 1];
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition","attachment; filename=" + downFileName + ";");
            response.TransmitFile(Server.MapPath(hndVideoFileNameForDownload.Value.ToString()));
            response.Flush();
            response.End();
        }

        protected void imgDownloadImage_Click(object sender, ImageClickEventArgs e)
        {
            string OrigFilePath = hdnOrigImage.Value.Replace("/ResizedImage/", "/Original/").Replace("/Customed/", "/Original/").Replace("/Croped/", "/Original/");
            string[] downFileNameArray = hdnOrigImage.Value.Replace("/ResizedImage/", "/Original/").Replace("/Customed/", "/Original/").Replace("/Croped/", "/Original/").Split('/');
            string downFileName = downFileNameArray[downFileNameArray.Length - 1];
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition","attachment; filename=" + downFileName + ";");
            response.TransmitFile(Server.MapPath(OrigFilePath));
            response.Flush();
            response.End();
            ClientScript.RegisterStartupScript(this.GetType(), "", "<script>CloseZoomModel();</script>");
        }
    }
}