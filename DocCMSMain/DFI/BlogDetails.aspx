﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true"
    CodeBehind="BlogDetails.aspx.cs" Inherits="DocCMSMain.DFI.BlogDetails" %>

<%@ Register Src="~/Controls/BlogRightSideBar.ascx" TagName="BlogSideBar" TagPrefix="Uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jscolor.js" type="text/javascript"></script>
    <style type="text/css">
      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Blog</h1>
                   
                </div>
                <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="Home.html">Home</a></li>
                        <li class="active">Blog </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/#title-->
    <section id="blog" class="container">
        <div class="row">
        <asp:HiddenField ID="hndBlogId" runat="server" ClientIDMode="Static"></asp:HiddenField>
         <section id="error" class="container" visible="false" runat="server">
        <h1>No Result Found</h1>
        <p>The Page you are looking for doesn't exist...</p>
        <a class="btn btn-success" href="Blogs.html">GO BACK TO THE BLOG PAGE</a>
    </section><!--/#error-->
            <aside class="col-sm-4 col-sm-push-8">
                <uc1:BlogSideBar ID="BlogSideBar1" did="SideBar1" runat="Server"></uc1:BlogSideBar>
            </aside>        
            <div class="col-sm-8 col-sm-pull-4">
                <div class="blog">
                <asp:ScriptManager ID="MainScriptManager" runat="server" />
                    <asp:UpdatePanel ID="pnlHelloWorld" runat="server">
                        <ContentTemplate>
                <asp:Repeater ID="rptBlogs" runat="server" OnItemDataBound="rptBlogs_OnItemDataBound">
                <ItemTemplate>
                 <div class="blog-item">
                 <asp:HiddenField ID="hdnBlogType" runat="server" Value='<%# Eval("BlogType") %>'></asp:HiddenField>
                        <img id="blogImage" class="img-responsive img-blog" src=""  alt=""  runat="server"  />
                         <div id="divVideoPlayer" runat="server">
                        <video autobuffer controls=''>
                        <source type='video/mp4' src='../UploadedFiles/Blogs/<%#Eval("ImageName")%>'></source>
                        </video>
                        </div>
                        <asp:Label ID="lblImage" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                          <asp:LinkButton ID="lnkNavigationURL" runat="server" Visible="false" Text='<%# Eval("NavigationURL") %>'></asp:LinkButton>
                        <div class="blog-content">
                            <h3><asp:Label ID="lblName" runat="server" Text='<%# Eval("BlogName") %>'></asp:Label></h3>
                            <div class="entry-meta">
                                <span><i class="icon-user"></i> <asp:Label ID="lblBlogBy" runat="server" Text='<%# Eval("BlogPostedBy") %>'></asp:Label></span>
                                <span><i class="icon-folder-close"></i> <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("BlogCategoryName") %>'></asp:Label></span>
                                <span><i class="icon-calendar"></i>   <asp:Label ID="lblDate" runat="server" Text='<%# Eval("CreationDate") %>'></asp:Label></a></span>
                               <span><i class="icon-comment"></i> <a href="#"><asp:Label ID="lblCommentCount" runat="server" Text=""></asp:Label> Comments</a></span>
                            </div>
                            <p><asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label></p>
                        </div>
                    </div><!--/.blog-item-->
                </ItemTemplate>
                </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <!------------Crop Image Model popup------------------>
                    <span id="spnImageWidth"></span>
                    <div id="myModalNew" class="modal" style="height: auto !important;max-width:auto !important;z-index:99; " >
                            <div class="modal-content" style="height: auto !important;max-width:auto !important;" id="divResizeInner">
                                <span class="cross" onclick="CloseImageCropModel();">×</span>
                                 <div style="width:auto !important;">
                <img src="" id="EditImageOnModelPopup" style="width: auto !important; height: auto !important; " />
                <div style="width: 100%;">
                   
                       
                        <br />
                        <input type="hidden" id="hdnEditMode" value="Crop" />
                    <div>
                        Select Image Size<select id="ddlSize" style="width: 150px; height: 17px;" onchange="ChangeOrientation();">
                            <option value="None">None</option>
                            <option value="4x6">4x6</option>
                          
                        </select>
                        Select Orientation<select id="ddlOrientation" style="width: 150px; height: 17px;"  onchange="ChangeOrientation();">
                            <option value="None">None</option>
                            <option value="Portrait">Portrait</option>
                            <option value="Landscape">Landscape</option>
                        </select>
                    </div>
                </div>
                <input type="button" id="btnSaveChanges" class="btn btn-danger btn-lg" value="SaveChanges" onclick="SaveChanges();"
                    style="display: none; margin-top:10px; margin-bottom:10px;"  />
                   
                <div class="inline-labels">
                    <label>
                        Width Starting Point
                        <input type="text" size="4" id="x1" name="x1" /></label>
                    <label>
                        Height Starting Point
                        <input type="text" size="4" id="y1" name="y1" /></label>
                    <br />
                    <label>
                        Width Ending Point
                        <input type="text" size="4" id="x2" name="x2" /></label>
                    <label>
                        Height Ending Point
                        <input type="text" size="4" id="y2" name="y2" /></label>
                    <br />
                    <label>
                        Image Width
                        <input type="text" size="4" id="w" name="w" class="customwidth" onchange="Call()" /></label>
                    <label>
                        Image Height
                        <input type="text" size="4" id="h" name="h" /></label>
                </div>
                <asp:HiddenField ID="hdnRatio" runat="server" ClientIDMode="Static" />
            </div>
            <div id="preview-pane">
                <span>Image Preview</span>
                <div class="preview-container" style="height: 150px; width: 450px;">
                    <img src="" id="imgPreview" class="jcrop-preview" alt="Preview" style="height: 150px;
                        width: 450px;" />
                </div>
            </div>
                            </div>
                          <div id="caption" runat="server" clientidmode="Static"></div>
                        </div>
                        <!---------------------Image Section--------------------------->
                        <div id="divModelResize" class="modal" style="height: auto !important;max-width:auto !important;z-index:99; " >
                            <div class="modal-content" style="height: auto !important;max-width:auto !important;" >
                            <span class="ResizeHeader">Resize Your Image to get a best fit result</span>
                                <span class="cross" onclick="CloseImageResizeModel();">×</span>
                                 <div style="width:auto !important;">
                <img src="" id="ImgResizeImage" />
                <div style="width: 100%;">
               
                             <input type="checkbox" id="chkResizeImageCreate"/>
                        Resize Image Option

                         <input type="checkbox" id="chkCustomeImageCreate"/>
                        Create Custom Image
                       <div  id="divCustomResizeImageCreate">
                        <input type="checkbox" id="chkAspectRatioResize" checked="checked" />
                        Keep Aspect Ratio
                        Image Width :<input type="text" id="txtCustomWidth" placeholder="Width in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('w',event);"/>
                        Image Height :<input type="text" id="txtCustomHeight" placeholder="Height in Pixels" onblur="CheckResizeValues();" onkeyup="return ManageAspectRatio('h',event);"/>
                    
                    <div id="divResizeSection">
                        Select Image Size<select id="ddlSizeResize" style="width: 150px; height: 17px;" onchange="SetReSizeValues('S');">                            
                            <option value="4x6">4x6</option>
                        </select>
                        Select Orientation<select id="ddlOrientationResize" style="width: 150px; height: 17px;"  onchange="SetReSizeValues('O');">                            
                            <option value="Landscape">Landscape</option>
                            <option value="Portrait">Portrait</option>
                        </select>
                    </div>
                
                <input type="button" id="btnResizeImage" class="btn btn-danger btn-lg" value="Resize" onclick="ResizeImage();"
                    style="margin-top:10px; margin-bottom:10px;" />
                    <span id="spnFinalCrop" style="display:none;"><span style="color:Red;font-weight:400;">The previewed image exceeds the required dimensions.You need to crop the image to get the required dimensions.</span><br /><input type="checkbox" id="chkFinalCrop"/>Crop Image
                    <input type="button" class="btn btn-danger btn-lg" style="display:none;" id="btnCropWithResize" value="Crop Image" onclick="CropImageWithResize();" />
                    </span>
                     
                    <input type="button" id="Button1" class="btn btn-danger btn-lg" value="Preview" onclick="PreviewResizedImage();"
                    style="margin-top:10px; margin-bottom:10px;display:none;"  />
                <asp:HiddenField ID="hdnResizeAsepctRatio" runat="server" ClientIDMode="Static" Value="False"/>
                 <div id="divResizePreviewPane" style="margin-left:20px; margin-bottom :20px;">
                <span><b>Image Preview</b></span>
                <div class="preview-container" id="divResizePreview"  style="width:300px; height:200px; border:1px solid black;">
                    <img src="" id="ImgResizePreview" alt="Preview" />
                </div>
            </div>
            </div>
            </div>
           
             </div>
             <div id="divCustomImageCreate" style="display:none;">
             Select BackGround Image Color:<input type="text" id="txtColor" class="jscolor" onchange="ColorChange(this)" />
             <br />
               <input type="button" id="btncustomeImage" class="btn btn-danger btn-lg" value="Create Image" onclick="CustomeImage();"
                    style="margin-top:10px; margin-bottom:10px;"  />
           
             <img src="" id="ImgCustomeImagePreview" alt="Custome Image Preview" style="width:500px; height:200px;" />
                <div id="divCustomImagePreviewRunning" style="width:300px; height:200px; background-color:#FFFFFF;margin-left:5%;border:1px solid Black;margin-bottom:10px;">
                 <center><img src="" id="ImgCustomeImagePreviewRunning"  /></center>
                </div>
               </div>
             </div>
                          <div id="Div4" runat="server" clientidmode="Static"></div>
                        </div>
                        <!---------------------Image Section--------------------------->
                     
                        <!------------Zoom Image Modal Popup--------------------------->
                        <div id="ImageModel" class="modal" style="z-index:999;">

                          <!-- Modal content -->
                          <div class="modal-content" style="max-width:50%;">
                            <div class="modal-header">                            
                              <span id="spnClose" class="close" onclick="CloseZoomModel()">×</span>
                                     <h4 style="color:#fff; float:left;">Note: The image displayed here is the resized image due to the resolution.
                                If you want to have actual image click on the download button.</h4>
                          <asp:ImageButton ID="imgDownloadImage" ImageUrl="../images/Download.png" runat="server" onclick="imgDownloadImage_Click" style="width: 50px;float: right;" ClientIDMode="Static" ></asp:ImageButton>
                              <asp:HiddenField ID="hdnOrigImage" runat="server" ClientIDMode="Static" Value="" />
                              <div style="clear:both;"></div>
                            </div>
                            <div class="modal-body" style="text-align:center;">
                                 <img src="" id="imgZoomImage" style="width: auto !important; height: auto !important;" runat="server" clientidmode="Static" />
                                 
                            </div>
                            <div class="modal-footer">
  
                            </div>
                          </div>
                        </div>
                        <!------------Zoom Image Modal Popup--------------------------->


                          <!------------Video Modal Popup------------>
                          <div id="VideoModel" class="modal" style="height: auto !important;max-width:auto !important;z-index:99;" >
                            <div class="modal-content" style="height: auto !important;max-width:auto !important;" >
                            <span class="ConvertHeader">Convert your video format as for your requirment Click to browse.</span>
                                <span class="cross" onclick="CloseShowVideoSection();">×</span>
                                
                                 <div style="width:auto !important;margin-top:5%;">
                                  <div style="width:95%; display:inline;">
                                    <span class="btn btn-default btn-file" style="width:40%; height:20%;">
                                    <img src="../images/Browse.png" style="width:50%; height:100%;" /><asp:FileUpload ID="fileVideoComment" runat="server" ClientIDMode="Static"  onChange="return validateVideoFile(this.value);"/>
                                    </span>
                                    
                                    <div style="display:inline; margin-left:10%">
                                    Output Formats<asp:DropDownList ID="ddlOutputFormats" runat="server" ClientIDMode="Static" style="width:25%;">
                                                <asp:ListItem Value="mp4" Text="mp4"></asp:ListItem>
                                        </asp:DropDownList>
                                        </div>
                                        <span id="spnVideoName"></span>
            <input type="button" value="Upload & Convert" id="btnConvertVideoJquery" class="btn btn-danger btn-lg" onclick="ProcessVideo();" style="margin-left: 2%;margin-bottom: 2%; margin-top:3%" /><img src="../images/Progressbaar.gif" id="ImgProgressBar" style="display:none; width:60px; height:50px;" />
            </div>
             </div>
          
             <div id="divImageExtractSection" style="display:none;">
           
           
            <div id="divVideoImages" style="overflow-x: hidden; border: 1px solid; height:300px;">
                <div id="divVideoScreens" runat="server" clientidmode="Static" class="divVideoScreens">
                </div>
            
                </div>
                 <!----Selected Screen shot Image Preview---------->
             <div style="width:400px; height:200px; display:none;" id="DivImagePreview">
            <div>Selected Image Preview</div>
            <img src="" id="PreviewImage"  runat="server" clientidmode="Static"/>
        </div>
         <!----Selected Screen shot Image Preview---------->
           <asp:Button ID="btnSelectCoverImage" runat="server"  class="btn btn-danger btn-lg" ClientIDMode="Static"  style="height: 34px; margin-bottom:2%;margin-top:8%;height:45px;"
                                        Text="Save Cover Image" onclick="btnSelectCoverImage_Click"></asp:Button>

               </div>
             </div>
             <!-----------Hide Video Player for Getting Screenshot of video----------------------->
               <div id="videoCanvasContainer" style="display: none;">
                <video id="video1" width="320" height="240">
                     <canvas id="canvas1" filename="" width="640" height="360"></canvas>
                </video>
            </div>
            <!-----------Hide Video Player for Getting Screenshot of video----------------------->
           

   
        </div>
                         <!------------Video Modal Popup------------>
   
                     <!----------Start comment Section----------------->
                     <div class="blog" runat="server" id="CommentSection"  >
                        <div class="blog-item">
                        <div class="blog-content">
                     <div id="comments">
                                <div id="comments-list">
                                    <h3 id="TotelComment"></h3>
                                    <!--/.media-->
                                    <div id="BindComment">
                                    
                                    </div>
                                    <!--/.media-->
                                </div><!--/#comments-list-->  
                                
                                <div id="comment-form" >`
                                    <div style="width:25%;float:left;" ><h3 visible="false" runat="server">Give a response</h3></div>
                                          
                                    <div style="width:25%;float:left;" visible="false" runat="server" >
                                    <img src="../images/chat-alt-double-outline.png"  onclick="ShowCommentSection();" style="width:40px; height :40px;" alt="Post a Comment" />
                                      <img src="../images/video-clip-icone.png"  onclick="ShowVideoSection();" style="width:40px; height :40px;margin-top: 0px;margin-left: 10px;" alt="Post a video" />
                                         <img src="../images/Camera-Upload.png"  onclick="ShowImageSection();" style="width:40px; height :40px;margin-left: 10px;" alt="Post an Image" />
                                    </div>
                              
                                    <div id="DivComment" style="display:none;">
                                 
                                        <div class="form-group">
                                          <div class="col-sm-12">
                                              <span id="ErrorMessage" style="color:Red;display:none;">Please Login First </span>
                                            </div>
                                            <div class="col-sm-12" style="display:none;">
                                                <input type="text" class="form-control" placeholder="Name" id="txtName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea rows="8" cols="4" class="form-control" placeholder="Comment" style="width:95%;" id="txtComment"></textarea>
                                                
                                            </div>
                                        </div>
                                          Who can see your comments: <select id="ddlPostingAccessComment" style="width: 58%;">
                                    <option value="Public">All Users</option>
                                    <option value="Internal">Members Only</option>
                                    </select>
                                       <input type="button" class="btn btn-danger btn-lg" onclick="SubmitComment();" value="Post" />
                                    </div>
                                    <div id="DivVideo" runat="server" visible="false">
                                        <div class="form-group">
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                            <div id="divFinalVideo" runat="server" style="border:1px solid Green;width:400px;height:250px;" clientidmode="Static" >
                                            <video width="320" height="240" poster="" runat="server" clientidmode="Static"  id="FinalVideoPlayer" src="" controls>
                                            </video>
                                            </div>
                                        </div>
                                    </div>
                                       Who can see your comments:
                                    <asp:DropDownList ID="ddlPostingAccessVideo" runat="server" style="width: 39%;">
                                    <asp:ListItem Value="Public" Text="All Users"></asp:ListItem>
                                    <asp:ListItem Value="Internal" Text="Members Only"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btnUpdloadVideo" runat="server" class="btn btn-danger btn-lg" Text="Upload Video" Width="20%" onclick="btnUpdloadVideo_Click" ></asp:Button>
                                        
                                         </div>
                                    <div id="DivImage" style="display:none;">
                                        <div class="form-group">
                                          <div class="col-sm-12">
                                            </div>
                                           
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                             <div id="dZUpload" class="dropzone" style="border:1px solid Red;">
                                                <div class="dz-default dz-message">
                                                </div>
                                            </div>
                                            <div id="divFinalPreview" runat="server" style="border:1px solid Green;width:400px;height:250px;" clientidmode="Static" >
                                            <img src="" id="imgFinalPreview" runat="server" clientidmode="Static" style="width:400px;height:250px; " />
                                            </div>

                                            </div>
                                        </div>
                                    Who can see your comments: <select id="ddlPostingAccess" style="width: 57%;">
                                    <option value="Public">All Users</option>
                                    <option value="Internal">Members Only</option>
                                    </select>
                                        <input type="button" class="btn btn-danger btn-lg" onclick="UploadImage_New();" id="btnPostImage" disabled="true" value="Post" />
                                    </div>
                               <!--/#comment-form-->
                            </div>
                            </div>
                            </div>
                            </div>
                          
                            <asp:HiddenField ID="XAxis" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="YAxis" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="Width" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="Height" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="maxWidth" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="maxHeight" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="fileName" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="filePath" runat="server" ClientIDMode="Static"/>
                            <asp:HiddenField ID="hdnCropedImage" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hdnFinalImagePreview" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hdnCurUserID" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hdnImagePath" runat="server" ClientIDMode="Static" />   

                            <asp:HiddenField ID="hdnActualImageWidth" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hdnActualImageHeight" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hdnReqHeightPort" runat="server" ClientIDMode="Static" Value="1800" />   
                            <asp:HiddenField ID="hdnReqWidthPort" runat="server" ClientIDMode="Static" Value="1200" />   
                            <asp:HiddenField ID="hdnReqWidthLand" runat="server" ClientIDMode="Static" Value="1800" />   
                            <asp:HiddenField ID="hdnResizeRequired" runat="server" ClientIDMode="Static" />   
                             <asp:HiddenField ID="hdnCropImagePath" runat="server" ClientIDMode="Static"/>   
                            <asp:HiddenField ID="hdnCropImageName" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hdnCoverImage" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hdnVideoCoverImagePath" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hdnVideoVideoPath" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hndVideoFileName" runat="server" ClientIDMode="Static" /> 
                            <asp:HiddenField ID="hndVideoFileNames" runat="server" ClientIDMode="Static" />   
                            <asp:HiddenField ID="hndVideoFileNameForDownload" runat="server" ClientIDMode="Static" />   
                            <asp:Button ID="btnDownloadVideo" runat="server" Text="" onclick="btnDownloadVideo_Click" ClientIDMode="Static" style="display:none;"></asp:Button>
                            
                            
                           
                             
                                               
             <!----------Start comment Section----------------->
                </div>
               
            </div><!--/.col-md-8-->
        </div><!--/.row-->        
    </section>
    <img src="" id="imgGetHeightWidth" style="display: none;" />
    <script src="js/Pages/BlogDetails.js" type="text/javascript"></script>
    <link href="../js/Jcrop/Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../js/Jcrop/Jcrop.js" type="text/javascript"></script>
    <script src="../Dropzone/dropzone.js" type="text/javascript"></script>
    <link href="../Dropzone/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="../css/BlogDetails.css" rel="stylesheet" type="text/css" />
    <style type="text/css"></style>
       
    <!--/#blog-->
    </div>
</asp:Content>
