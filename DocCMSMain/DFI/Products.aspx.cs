﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class Products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Products";
            if (!IsPostBack)
            {
                Bind_Products(42);
            }
        }


        // Bind Products Function
        protected void Bind_Products(Int32 PageID)
        {
            try
            {
                DataTable dtProducts = null;
                DataTable dtDetail = null;
                if (Convert.ToString(Request.QueryString["preview"]) == "1")
                {
                    dtProducts = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview(PageID);
                }
                else if (Convert.ToString(Request.QueryString["preview"]) == "2")
                {
                    dtProducts = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview_FromDraft(PageID);
                }
                else
                {
                    dtProducts = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_for_Published(PageID);
                }
                if (dtProducts != null && dtProducts.Rows.Count > 0)
                {
                    StringBuilder strProducts = new StringBuilder();
                    for (Int32 i = 0; i < dtProducts.Rows.Count; i++)
                    {
                        if (Convert.ToString(Request.QueryString["preview"]) == "2")
                        {
                            dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid_fromDraft(Convert.ToInt32(dtProducts.Rows[i]["CMSID"]));
                        }
                        else
                        dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtProducts.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            foreach (DataRow rw in dtDetail.Rows)
                            {
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PAGE NAME")
                                    strProducts.Append(" <h1 class='dms-heading'>" + (Convert.ToString(rw["PageContent"])) + "<hr class='fancy-divider'></h1>");

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "SELECT BANNER")
                                    ImgBanner.Src = "../UploadedFiles/ContentImages/Products/" + Convert.ToString(rw["PageContent"]);

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PAGE DESCRIPTION")
                                   strProducts.Append(Convert.ToString(rw["PageContent"]));
                                
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PRODUCT OVERVIEW SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    DivProducts.Visible=true;

                                 if (Convert.ToString(rw["FieldText"]).ToUpper() == "BANNER IMAGE" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    SectionBanner.Visible=true;

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "ELITE SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                     SectionElite.Visible=true;
                                     Bind_Products_category(35);
                                }
                            }
                        }
                    }
                    DivProducts.InnerHtml = strProducts.ToString();
                }
                else
                {
                    divPublished.Visible = false;
                    divunpublished.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        // Bind Products Category Function
        protected void Bind_Products_category(Int32 PageID)
        {
            try
            {
                DataTable dtProducts = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtProducts != null && dtProducts.Rows.Count > 0)
                {
                    StringBuilder strProducts = new StringBuilder();
                    for (Int32 i = 0; i < dtProducts.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtProducts.Rows[i]["CMSID"]), "Products");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string ProductName = Convert.ToString(dtDetail.Rows[0]["Products"]);
                            Session.Add(Convert.ToString(dtProducts.Rows[i]["CMSID"]), "CMSID");
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["Products"]).Replace(".aspx", ".html") ;
                            string ImageURL = "../UploadedFiles/ContentImages/ProductCategory/" + Convert.ToString(dtDetail.Rows[1]["Products"]);
                            string ProductDescription = Convert.ToString(dtDetail.Rows[2]["Products"]);
                            strProducts.Append("<a href='.." + NavigationURL + "' style='color:#000;'><div class='col-md-4'>");
                            strProducts.Append("<div class='dfi-icon-box'><img src='"+ImageURL+"' alt='"+ProductName+"' class='img-responsive dms-dfi-icon' >");
                            strProducts.Append(" <h3>"+ProductName+" <hr class='fancy-divider'></h3>  ");
                            strProducts.Append(" <p>"+ProductDescription+"</p>");
                            strProducts.Append(" <p style='padding-left:28%'><a class='product-read-more' href='.." + NavigationURL + "' style='margin-right:30px;'>Read More <i class='fa fa-chevron-circle-right'></i></a></p>");
                            strProducts.Append("</div></div></a>");
                         }
                    }
                   DivProductsCategory.InnerHtml = strProducts.ToString();
                 }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}