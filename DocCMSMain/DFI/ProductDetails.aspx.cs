﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;

namespace DocCMSMain.DFI
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        protected static string PageName = "Store";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["CMSID"] != null)
                {
                    if (string.IsNullOrEmpty(hdnSession.Value))
                    hdnSession.Value = Session.SessionID;
                    hdnSession1.Value = Session.SessionID;
                    DataTable dtProductDetail = null;
                    dtProductDetail = ServicesFactory.DocCMSServices.Get_One_Product_Detail(Convert.ToInt32(Request.QueryString["CMSID"]));
                    if (dtProductDetail != null && dtProductDetail.Rows.Count > 0)
                    {
                        lblProductName.InnerText = Convert.ToString(dtProductDetail.Rows[0]["ProductName"]);
                        lblDecription.InnerText = Convert.ToString(dtProductDetail.Rows[0]["Description"]);
                        imgProduct.Src = "../UploadedFiles/ContentImages/Catalogue/" + Convert.ToString(dtProductDetail.Rows[0]["ImageName"]);
                        hdnCategory.Value = Convert.ToString(dtProductDetail.Rows[0]["CategoryName"]);
                        hdnProductName.Value = Convert.ToString(dtProductDetail.Rows[0]["ProductName"]);
                        hdnProductNumber.Value = Convert.ToString(dtProductDetail.Rows[0]["ProductNumber"]);
                        hdnIntenalDescription.Value = Convert.ToString(dtProductDetail.Rows[0]["InternalDescription"]);
                        hdnPrice.Value = Convert.ToString(dtProductDetail.Rows[0]["Price"]);
                        hdnCost.Value = Convert.ToString(dtProductDetail.Rows[0]["Cost"]);
                        if (Convert.ToString(dtProductDetail.Rows[0]["Cost"]) == "" || Convert.ToString(dtProductDetail.Rows[0]["Cost"]) == null)
                        {
                            hdnCost.Value = "0";
                        }
                        demo.InnerHtml = Convert.ToString(dtProductDetail.Rows[0]["DetailDescription"]);
                        if (demo.InnerHtml == "" || demo.InnerHtml == null)
                        {
                            btnViewDetail.Visible = false;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtProductDetail.Rows[0]["Price"])))
                        {
                            lblPrice.InnerText = "$ " + Convert.ToString(dtProductDetail.Rows[0]["Price"]);
                        }
                        else
                        {
                            lblPrice.InnerText = "$ 0.00";
                        }
                    }
                    Bind_Size_And_Color();
                    Session["CurrPage"] = PageName;
                }
            }
        }
        public void Bind_Size_And_Color()
        {
            //Bind ddlSize
            DataTable dtSize = new DataTable();
            dtSize = ServicesFactory.DocCMSServices.Fetch_size(Request.QueryString["CMSID"]);
            if (dtSize != null && dtSize.Rows.Count > 0)
            {
                ddlSize.DataSource = dtSize;
                ddlSize.DataTextField = "Size";
                ddlSize.DataValueField = "Size";
                ddlSize.DataBind();
                divSize.Style.Add("display", "");
            }
            else
            { 
                divSize.Style.Add("display", "none");
            }
            //Bind ddlColor
            DataTable dtColor = new DataTable();
            dtColor = ServicesFactory.DocCMSServices.Fetch_color_Details(Request.QueryString["CMSID"]);
            if (dtColor != null && dtColor.Rows.Count > 0)
            {
                ddlColor.DataSource = dtColor;
                ddlColor.DataTextField = "Color";
                ddlColor.DataValueField = "ColorId";
                ddlColor.DataBind();
                divColor.Style.Add("display", "");
            }
            else
            {
                divColor.Style.Add("display", "none");
            }
        }
    }
}