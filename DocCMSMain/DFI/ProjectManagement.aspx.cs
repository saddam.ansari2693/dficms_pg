﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class ProjectManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "ProjectManagement";
            if (!IsPostBack)
            {
                Bind_ProjectManagement(68);
            }
        }
        // Bind Project Management Function
        protected void Bind_ProjectManagement(Int32 PageID)
        {
            try
            {
                DataTable dtProjects= null;
                if (Request.QueryString["preview"] != null)
                {
                    dtProjects = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview(PageID);
                }
                else
                {
                    dtProjects = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_for_Published(PageID);
                }
                if (dtProjects != null && dtProjects.Rows.Count > 0)
                {
                    StringBuilder str = new StringBuilder();
                    for (Int32 i = 0; i < dtProjects.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtProjects.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            
                                string PageName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                                string BannerImage = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                                string PageDescription = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                                ImgBanner.Src = "../UploadedFiles/ContentImages/ProjectManagement/"+BannerImage+"";
                                DivProjectManagement.InnerHtml = PageDescription;
                                pageHeading.InnerHtml = PageName;
                                Bind_Projects_category(69);
                        }
                    }
                }
                else
                {
                    divPublished.Visible = false;
                    divunpublished.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind Products Category Function
        protected void Bind_Projects_category(Int32 PageID)
        {
            try
            {
                DataTable dtProject = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtProject != null && dtProject.Rows.Count > 0)
                {
                    StringBuilder strProjects = new StringBuilder();
                    for (Int32 i = 0; i < dtProject.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtProject.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string ProjectName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            Session.Add(Convert.ToString(dtProject.Rows[i]["CMSID"]), "CMSID");
                            string ImageURL = "../UploadedFiles/ContentImages/ProjectCategory/" + Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string ProjectDescription = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                            strProjects.Append("<div class='col-md-4' style='min-height:600px'>");
                            strProjects.Append("<div class='dfi-icon-box'><img src='" + ImageURL + "' alt='" + ProjectName + "' class='img-responsive dms-dfi-icon' >");
                            strProjects.Append(" <h3>" + ProjectName + " <hr class='fancy-divider'></h3>  ");
                            strProjects.Append(" <p>" + ProjectDescription + "</p>");
                            strProjects.Append(" <p style='padding-left:28%'><a class='product-read-more' href='.." + NavigationURL + "' style='margin-right:30px;'>Read More <i class='fa fa-chevron-circle-right'></i></a></p>");
                            strProjects.Append("</div></div>");
                         }
                    }
                    DivProjectCategory.InnerHtml = strProjects.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}