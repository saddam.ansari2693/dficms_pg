﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using System.IO;
using DocCMS.Core;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace DocCMSMain.DFI
{
    public partial class Careers : System.Web.UI.Page
    {
        protected static string JobTitle = "";
        protected static string ResumeFile = "";
        string str;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Contact";
            if (!IsPostBack)
            {
                string category = Request.QueryString["category"];
                string JobType = Request.QueryString["JobName"];
                Bind_Career(87);
                Bind_JobTitle(87, JobType);
                Bind_Skills(86, category);
            }
        }

       
        protected void btnShowDisplayOrders_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "DocCMS", "<script>return FormatTable();</script>");
        }

        protected void btnUpdateDisplayOrder_Click(object sender, EventArgs e)
        {
        }

        protected void btnPopUp_Click(object sender, EventArgs e)
        {
        }


        // Function for Career 
        protected void Bind_Career(Int32 PageID)
        {
            try
            {
                DataTable dtJob = ServicesFactory.DocCMSServices.Fetch_Job_cmsid_By_pageid(PageID);
                if (dtJob != null && dtJob.Rows.Count > 0)
                {
                    StringBuilder strJob = new StringBuilder();
                    for (Int32 i = 0; i < dtJob.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Job_By_cmsid(Convert.ToInt32(dtJob.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string JobName = Convert.ToString(dtDetail.Rows[0]["Job"]);
                            string Category = Convert.ToString(dtDetail.Rows[1]["Job"]);
                            string JobDescription = Convert.ToString(dtDetail.Rows[2]["Job"]);
                            string JobDeatilDescription = Convert.ToString(dtDetail.Rows[3]["Job"]);
                            string PdfURL = "../UploadedFiles/ContentImages/Job/" + Convert.ToString(dtDetail.Rows[4]["Job"]);
                            string DisplayOrder = Convert.ToString(dtDetail.Rows[5]["Job"]);
                            bool ShowPDF = Convert.ToBoolean(dtDetail.Rows[6]["Job"]);
                            bool ShowApplyButton = Convert.ToBoolean(dtDetail.Rows[7]["Job"]);
                            bool ShowReadMoreButton = Convert.ToBoolean(dtDetail.Rows[8]["Job"]);
                            strJob.Append("<h3>" + JobName + "</h3>");
                            strJob.Append(" <div  style='font-size:16px;'>" + JobDescription + "</div>");
                            if (JobDeatilDescription != null && JobDeatilDescription != "")
                            {
                                if (ShowReadMoreButton == true)
                                {
                                    strJob.Append("<a href='../dfi/CareerDetail.html?JobName=" + JobName + "&CMSID=" + Convert.ToInt32(dtJob.Rows[i]["CMSID"]) + "' id='AncReadMore' class='product-read-more'>Read More<i class='fa fa-chevron-circle-right'></i></a>");
                                }
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(dtDetail.Rows[4]["Job"])))
                            {
                                if (ShowPDF == true)
                                {
                                    strJob.Append("<a href='" + PdfURL + "'  class='product-view-pdf'>View Pdf <i class='fa fa-chevron-circle-right'></i></a>");
                                }
                            }
                            else
                            {
                               
                            }
                            if (ShowApplyButton == true)
                            {
                                strJob.Append(" <a class='product-apply abc' href='Career.htm?category=" + Category + "&JobName=" + JobName + "#jobapplication' id='job_" + DisplayOrder + "'  onclick='return setjobTitle(" + DisplayOrder + ");'>Apply <i class='fa fa-check-circle'></i></a>");
                            }
                            else
                            {
                            }
                            strJob.Append("<p>&nbsp;</p>");
                        }
                    }
                    SectionJob.InnerHtml = strJob.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_JobTitle(Int32 PageID, string category)
        {
            try
            {
                DataTable dtJob = ServicesFactory.DocCMSServices.Fetch_Job_cmsid_By_pageid(PageID);
                if (dtJob != null && dtJob.Rows.Count > 0)
                {
                    ddlJobTitle.DataSource = dtJob;
                    ddlJobTitle.DataTextField = "FieldValue";
                    ddlJobTitle.DataValueField = "FieldValue";
                    ddlJobTitle.DataBind();
                    ddlJobTitle.SelectedValue = category;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_Skills(Int32 PageID, string category)
        {
            try
            {
                DataTable dtJob = ServicesFactory.DocCMSServices.Fetch_skills_by_categoryname(PageID, category);
                if (dtJob != null && dtJob.Rows.Count > 0)
                {
                    chkSkills.DataSource = dtJob;
                    chkSkills.DataTextField = "FieldValue";
                    chkSkills.DataValueField = "FieldValue";
                    chkSkills.DataBind();
                }
                else
                {
                    SpanSkill.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void itemSelected(object sender, EventArgs e)
        {
           
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                Cls_JobApplications clsjob = new Cls_JobApplications();
                clsjob.jobtitle = ddlJobTitle.SelectedValue;
                clsjob.firstname = txtFirstName.Value.Trim();
                clsjob.lastname = txtLastName.Value.Trim();
                clsjob.phoneno = txtPhone.Value.Trim();
                clsjob.emailid = txtEmailId.Value.Trim();
                clsjob.resumeheading = txtResumeHeading.Value.Trim();
                clsjob.description = txtDescription.Value.Trim();
                for (int i = 0; i < chkSkills.Items.Count; i++)
                {
                    if (chkSkills.Items[i].Selected == true)
                    {
                        if (i == 0)
                            str += "" + chkSkills.Items[i].ToString();
                        else
                            str += "~" + chkSkills.Items[i].ToString();
                    }
                }
                clsjob.skills = str;
                if (FileUpload.HasFile)
                {
                    string serverpath = Server.MapPath("../UploadedFiles/JObApplication/");
                    DirectoryInfo directoryInfo = new DirectoryInfo(serverpath);
                    bool isDirCreated = directoryInfo.Exists;
                    if (!isDirCreated)
                    {
                        directoryInfo.Create();
                    }
                    FileUpload.PostedFile.SaveAs(serverpath + @"\\" + FileUpload.FileName);
                    clsjob.filename = FileUpload.FileName;
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error Message", "<script>alert('Please Provide Your Resume');</script>");
                    return;
                }
                Int32 retVal = ServicesFactory.DocCMSServices.Insert_Job_Application(clsjob);
                if (retVal > 0)
                {
                    bool MessageSuccess = SendMail(clsjob);
                    if (MessageSuccess == true)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>FireMessage(1);</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>FireMessage(2);</script>");
                    }
                }
                txtFirstName.Value = "";
                txtLastName.Value = "";
                txtPhone.Value = "";
                txtEmailId.Value = "";
                txtResumeHeading.Value = "";
                txtDescription.Value = "";
                ddlJobTitle.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Create Method for Send Mail
        public bool SendMail(Cls_JobApplications clsjob)
        {
            bool Retval = true;
            try
            {
                Cls_SMTP objSMTP = ServicesFactory.DocCMSServices.Fetch_SMTP_Detail_By_department("Human Resource");
                if (objSMTP != null)
                {
                    string Password = ServicesFactory.DocCMSServices.Decrypt(Convert.ToString(objSMTP.senderpassword));
                    string subject = clsjob.resumeheading;
                    //**************Mail To Admin starts from here********************
                    MailMessage mailToAdmin = new MailMessage();
                    mailToAdmin.IsBodyHtml = true;
                    mailToAdmin.To.Add(objSMTP.toemail);
                    mailToAdmin.From = new MailAddress(objSMTP.senderemailid.Trim());
                    if (!string.IsNullOrEmpty(objSMTP.bccemail))
                    {
                        MailAddress BccEmail = new MailAddress(objSMTP.bccemail);
                        mailToAdmin.Bcc.Add(BccEmail);
                    }
                    if (!string.IsNullOrEmpty(objSMTP.ccemail))
                    {
                        MailAddress CcEmail = new MailAddress(objSMTP.ccemail);
                        mailToAdmin.CC.Add(CcEmail);
                    }
                    mailToAdmin.Subject = subject;
                    StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/JobApplicationTemplate.htm"));
                    string readFile = reader.ReadToEnd();
                    string StrContent = "";
                    StrContent = readFile;
                    mailToAdmin.Body = readFile;
                    string URL = Request.Url.Scheme + "://" + Request.ServerVariables["HTTP_HOST"] + Request.ApplicationPath;
                    string Dlink = "href='" + URL + "/UploadedFiles/JObApplication/" + clsjob.filename + "'";
                    if (!string.IsNullOrEmpty(clsjob.filename))
                        mailToAdmin.Attachments.Add(new Attachment(FileUpload.PostedFile.InputStream, clsjob.filename));

                    if (mailToAdmin.Body != null)
                    {
                        mailToAdmin.Body = mailToAdmin.Body.ToString()
                        .Replace("<%JobName%>", Convert.ToString(clsjob.jobtitle))
                        .Replace("<%AppliedBy%>", Convert.ToString(clsjob.firstname + " " + clsjob.lastname))
                        .Replace("<%PhoneNo%>", Convert.ToString(clsjob.phoneno))
                        .Replace("<%EmailId%>", Convert.ToString(clsjob.emailid))
                        .Replace("<%Comment%>", Convert.ToString(clsjob.description))
                        .Replace("<%AppliedOn%>", Convert.ToString(DateTime.Now.ToString("MM/dd/yyyy")))
                        .Replace("<%Downloadlink%>", Convert.ToString(Dlink));
                    }
                    Retval = ServicesFactory.DocCMSServices.Send_Mail(mailToAdmin, objSMTP);
                    //*****Mail To admin ends here*****************
                    //***Mail to Job Seeker starts from here***
                    MailMessage mailToJobSeeker = new MailMessage();
                    mailToJobSeeker.IsBodyHtml = true;
                    mailToJobSeeker.To.Add(txtEmailId.Value.Trim());
                    mailToJobSeeker.From = new MailAddress(objSMTP.senderemailid);
                    mailToJobSeeker.Subject = subject;
                    reader = new StreamReader(Server.MapPath("~/EmailTemplate/JobSeekerReply.htm"));
                    readFile = reader.ReadToEnd();
                    StrContent = "";
                    StrContent = readFile;
                    mailToJobSeeker.Body = readFile;
                    string imageURl = "src='" + URL + "/images/Logos/DocLogo2.png" + "'";
                    if (mailToJobSeeker.Body != null)
                    {
                         mailToJobSeeker.Body = mailToJobSeeker.Body.ToString()
                        .Replace("<%LogoImage%>", Convert.ToString(imageURl))
                        .Replace("<%UserName%>", Convert.ToString(clsjob.firstname + " " + clsjob.lastname))
                        .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year))
                        .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()));
                    }
                    Retval = ServicesFactory.DocCMSServices.Send_Mail(mailToJobSeeker, objSMTP);
                    //***Mail to job seeker ends here
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Internal error occcured');</script>");
                }
            }
            catch (Exception ex)
            {
                Retval = false;
            }
            return Retval;
        }
    }// Class Ends Here
}
