﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Store.aspx.cs" Inherits="DocCMSMain.DFI.Store" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/Pages/CartSave.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function FilterMenu(event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13 || charCode == 1) {
                var lnkSearchMenu = document.getElementById("lnkSearchMenu");
                lnkSearchMenu.click();
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <style type="text/css">
        *,
        
        *:before,
        *:after
        {
            box-sizing: border-box;
        }
        .success
        {
            border: 1px solid;
            margin: 10px 0px;
            padding: 5px 43px 3px 22px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }
        
        .success
        {
            color: #4F8A10;
            background-color: #DFF2BF;
            font-weight: 600;
        }
        
        .top-row
        {
            background:#fff;
            box-shadow:0 0 3px 1px #aaa;
            padding:15px 0;
        }
       
         #header
          {
                       height:25px !important;
                       background: #fff;
          }
                    
                    .main
                    {
                        padding: 0 0 50px;
                    }
                    
                    .main .content
                    {
                        padding: 15px 5px;
                        
                    }
                    
                    .main span.shadow-top
                    {
                        display: none;
                    }
#div_Menu {
   background: #fff none repeat scroll 0 0;
    box-shadow: 0 0 3px 1px #aaa;
    margin-bottom: 206px;
    margin-left: -227px;
    margin-top: 55px;
    min-height: 600px;
    padding: 15px;
    width: 100%;
}

table {
  color: #333;
  font-family: sans-serif;
  font-size: .9em;
  font-weight: 300;
  text-align: left;
  line-height: 40px;
  border-spacing: 0;
  border: 2px solid #975997;
  width: 500px;
  margin: 50px auto;
}

thead tr:first-child {
  background: #949191;
  color: #fff;
  border: none;
}

th {font-weight: bold;}
th:first-child, td:first-child {padding: 0 15px 0 20px;}
thead tr:last-child th {border-bottom: 3px solid #ddd;}
tbody td {border-bottom: 1px solid #ddd;}

td:last-child {
  text-align: right;
  padding-right: 10px;
}

.button {
  color: #696969;
  padding-right: 5px;
  cursor: pointer;
}

.alterar:hover {
  color: #0a79df;
}

.excluir:hover {
  color: #dc2a2a;
}

.main .sidebar {
    display: block;
    float: left;
    margin-bottom: 80px;
    margin-right: -82px !important;
    width: 282px;
}
/* Main Grid styles*/
.catmain
{
 float: left; 
 width: 20%;
}
    .catitems
{ 

 width: 48%;
}
    .cartitems
{
 border: 1px solid rgb(0, 0, 255); 

 width: 31%;
}

.addtocart
{
    border:1px solid #000; height:30px; width:30px;border-radius:15px;
}
    
.addtocart:hover
{
    border:1px solid #000; height:30px; width:30px;border-radius:15px;background-color:#CFCDAB;
}
.quantity
{
    display:inline;
    width:100%;
}
.addqty
{
    background-color: rgb(0, 0, 0);
    color: rgb(255, 255, 255);
    cursor: pointer;
    font-size: 16px;
    line-height: 15px;
    margin-left: 2px;    
    padding-right: 3px;
    text-align: center;
    width: 22%;
}
.addqty:hover
{
      background-color: Green;
}
.remqty
{
    background-color: rgb(0, 0, 0);
    color: rgb(255, 255, 255);
    line-height: 15px;
    margin-left: 2px;
    padding-left: 3px;
    padding-right: 3px;
    text-align: center;
    font-size:16px;
    cursor:pointer;
    width: 22%;  
    
}    
.remqty:hover
{
    background-color: Red;
}
.imghover
{
    content:url("../images/IIF/delete_hover.png");
}
.qtyUnit
{
   width:45%;height:18px; text-align:center;
}
.itemdelbtn
        {
            background: rgb(128, 128, 128) none repeat scroll 0 0;
            border-radius: 6px;
            color: rgb(255, 255, 0);
            margin-left: 10px;
            padding: 2px;
            }
             .itemdelbtn:hover
            {
            background: rgb(128, 128, 128) none repeat scroll 0 0;    
            }
            
            .HdrLogo
            {
                height: 115px;
            }
            
            .order-table tbody tr:nth-child(1):hover,
            .order-table tbody tr:nth-child(2):hover
            {
                background: transparent;
                color: rgb(130,130,130);
            }
            
        @media screen and (max-width:1250px)
        {
            .catmain,
            .catitems,
            .cartitems
            {
                float: none;
                width: 100%;
                padding: 0 10px;
            }
            .product-table
            {
                width: 95%;
            }
            
            .product-table tbody th,
            .product-table tbody td
            {
                width: 25%;
            }
            
            .product-table tbody td
            {
                padding:5px;
            }
            .product-table tbody td a
            {
                display: block;
            }
            
            .product-table tbody td a img
            {
                width: 60px !important;
                height: 60px !important;
            }
            
            .main .content img
            {
                margin: 0;
            }
            
            th:first-child, 
            td:first-child
            {
                padding: 0 8px;
            }
            
            .search-box input
            {
                width: 90%;
            }
            
            .main h3
            {
                text-align: left;
            }
            
            .order-table
            {
                width:95%;
            }
            
            .cartitems
            {
                border: none;
                margin-top:25px;
            }
            
            .main
            {
                padding-top: 10px !important
            }
            
            .main .content
            {
                margin-top: 15px;
                padding-top:10px !important;
            }
         }
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.cross {
 
    color: #ff0000;
    font-size: 40px;
    font-weight: bold;
    position: absolute;
    top: -19px;
    right: 0;
    transition: all 0.3s ease 0s;
    z-index: 10000;
}

.cross:hover,
.cross:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 80%;
    }
} 
@media (max-width: 580px)
{ 
    .modal-content {
        width: 80%;
    }
}      

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updtPnl" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
<section id="portfolio" class="container">
     <div id="divunpublished" runat="server"  align="center" style="display:none">
    <div class="center">
                <h2 id="H1">The Page is under construction</h2>
            </div>
            <div class="gap"></div>
   <img src="images/underConstruction.jpg" />
   <div class="gap"></div>
   <div class="center">
                <h2 id="H2">Comming Soon...</h2>
            </div>
   </div>
                 <div class="main" style="display:block">
                <span class="shadow-top"></span>
                <!-- shell -->
                <div class="shell">
                    <!-- testimonial -->
                    <!-- content -->
                    <div class="content" style="background: #EFEFEF">
                        <!-- sidebar -->
                        <div class="row">
                           <div id="divSideBarCategories" class="catmain col-lg-3 col-md-12 col-xs-12 ">
                            <h1>
                                Catalogue</h1>
        <div class="input-group">
  <asp:TextBox ID="txtSearchMenu" runat="server" class="form-control" placeholder="Search Parts/Category"
 ClientIDMode="Static" onfocus="this.value = this.value;"
                                    onkeypress="return FilterMenu(event);"></asp:TextBox>
        <span class="input-group-btn" style="padding-left: 3px;">
            <div class="btn btn-danger Searchbtn " onclick="return FilterMenu(event);">
                <i class="icon-search"></i>
            </div>
        </span>
         <asp:LinkButton ID="lnkSearchMenu" runat="server" ClientIDMode="Static" Style="display: none;"
                                    OnClick="lnkSearchMenu_Click" OnClientClick="Fetch_Shopping_Cart()"></asp:LinkButton>
    </div>
                            <div id="divCategories" style="width: 95%;">
                                <ul id="ulMenuList" style="list-style: none; list-style-type: none;">
                                   <li id="limenu">
                                  <a onclick="Fetch_Shopping_Cart();" id="lnkShowallMenu" href="">Show All Products</a>
                                            </li>
                                    <asp:Repeater ID="Rptcategory" runat="server" OnItemCommand="Rptcategory_ItemCommand"
                                        >
                                        <ItemTemplate>
                                            <li id="limenu" runat="server" clientidmode="Static">
                                                <asp:Label ID="lblMenuID" runat="server" ClientIDMode="AutoID" Text='<%# Eval("CMSID") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:LinkButton ID="lnkMenu" runat="server" ClientIDMode="Static" Text='<%# Eval("CategoryName") %>'
                                                    CommandArgument='<%# Eval("CMSID") %>' CommandName="BindParts" OnClientClick="Fetch_Shopping_Cart()"></asp:LinkButton>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                        <div id="divParts" class="catitems col-lg-6 col-md-12 col-xs-12">
                         <section id="SectionError" class="container" visible="false" runat="server">
                            <h1>No Result Found</h1>
                                          <p>The Product / Category you are looking for doesn't exist...</p>
                                            </section>
                              <div id="DivMainTable" runat="server" clientidmode="Static" visible="false">        
                            <table id="dyntable" class="table table-bordered responsive product-table" cellpadding="0" cellspacing="0" >
                                <colgroup>
                                    <col class="con1" style="width: 20%;" />
                                    <col class="con0" style="width: 20%;" />
                                    <col class="con0" style="width: 20%;" />
                                      <col class="con0" style="width: 20%;" />
                                    <col class="con0" style="width: 5%;" />
                                </colgroup>
                                <thead style="background-color: #949191; color: #FFFFFF;">
                                    <tr>
                                        <th>
                                            Image
                                        </th>
                                        <th>
                                            Product Name
                                        </th>
                                        <th>
                                            Description
                                        </th>
                                          <th style="text-align: center;">
                                            Price
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrParts" OnItemDataBound="RptrParts_ItemDataBound" OnItemCommand="Rptcategory_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">
                                                <asp:HiddenField ID="hdnProductCategoryName" runat="server" Value='<%#Eval("CategoryName")%>' ClientIDMode="Static"/>                                                
                                                    <asp:Label ID="lblImageName" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                                                          <img id="myImg" onclick="change(this)"  clientidmode="Static" runat="server"  style="width:120px; Height:120px;" src='<%# "../UploadedFiles/ContentImages/Catalogue/" + Eval("ImageName") %>'  title='<%# Eval("Description") %>' alt='<%# Eval("Description") %>' width="300" height="200">
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' CssClass="DorderLabel" ClientIDMode="Static"
                                                        Visible="false"></asp:Label>
                                                         <asp:HiddenField ID="lblProductNumber"  runat="server" Value='<%# Eval("ProductNumber") %>' ClientIDMode="Static"
                                                        ></asp:HiddenField>
                                                          <asp:HiddenField ID="hdnInternalDescription"  runat="server" Value='<%# Eval("InternalDescription") %>' ClientIDMode="Static"
                                                        ></asp:HiddenField>
                                                    <asp:Label ID="lblPartNo" runat="server" Text='<%# Eval("ProductName") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                    <asp:LinkButton ID="lnkViewMore" runat="server" CommandName="ViewMore" CssClass="product-read-more" CommandArgument='<%#Eval("CMSID")%>' >View Details</asp:LinkButton>
                                                </td>
                                                   <td style="text-align: center;">
                                                     <asp:HiddenField ID="hdnCost"  runat="server" Value='<%# Eval("Cost") %>' ClientIDMode="Static"
                                                        ></asp:HiddenField>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("Price") %>' CssClass="DorderLabel" ClientIDMode="Static"></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <div class="addtocart">
                                                        <img id="imgAdd" alt="Add To Cart" src="images/AddToCart1.png" onclick="AddtoCart(this,'<%#Eval("CMSID")%>')"
                                                            title="Click here to add this on cart" style="margin: 0; height: 25px; width: 25px;
                                                            padding: 3px; cursor: pointer;"  />
                                                        </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            </div>    
                        </div>
                        <div id="divCart" class="cartitems col-lg-3 col-md-3 col-xs-12" style="border:none;">
                            <table id="tblCartItems" class="table table-responsive order-table order-table-2"
                                cellpadding="0" cellspacing="0" style="box-shadow: 0 0 1px 1px #ddd; border-radius: 3px;">
                                <tr>
                                    <td colspan="5">
                                        <h3 class="order-head" style="text-align:center">
                                         <span style="text-align: left;">Your Order</span>
                                        </h3>
                                        <h3>
                                        <asp:Button ID="btnsubmit" runat="server" Style="display: none;" class="btn btn-primary"
                                                OnClick="btnsubmit_onclick" ClientIDMode="Static" Text="End Order" />
                                        <input value="Cancel Order" runat="server" id="btnCancel" clientidmode="Static" type="button"
                                                class="btn btn-danger" title="Delete All Items from cart"
                                                onclick="DeleteCartItems();" style="display: none;" />
                                  </h3>
                                    </td>
                                </tr>
                                <tr class="tab-head tr-head-container">
                                    <td>
                                        Item Desc.
                                    </td>
                                    <td colspan="3" style="text-align:center;">
                                        Quantity
                                    </td>
                                    <td style="text-align:center;">
                                        Price
                                    </td>
                                </tr>
                            </table>
                           <div id="DivTotal" class="cartitems" style="width:100%; border:none;">
                            <table id="tblTotal" class="table table-responsive order-table order-table-2"
                                cellpadding="0" cellspacing="0" style="box-shadow: 0 0 1px 1px #ddd; border-radius: 3px; width:100%;" >
                                <tr >
                                    <td class="tab-head tr-head-container">
                                        <h3 class="order-head">
                                            <span style="text-align: left;">Total</span>
                                        </h3>
                                    </td>
                                    <td class="tab-head tr-head-container" id="tdTotalamount">
                                        <h3 class="order-head">
                                            <span style="text-align: left;" id="spnCartTotal">$0.00</span>
                                        </h3>
                                    </td>
                                </tr>
                            </table>
                            <p style="margin-bottom:0;"><b>The prices include taxes and shipping costs</b></p>
                        </div>
                        </div>
                        </div>
                        <asp:HiddenField EnableViewState="true" ID="hdnSession" runat="server" ClientIDMode="Static"
                            Value="" />
                        <asp:HiddenField EnableViewState="true" ID="hdnSession1" runat="server" ClientIDMode="Static"
                            Value="" />
                        <asp:HiddenField EnableViewState="true" ID="hdnCategoryName" runat="server" ClientIDMode="Static"
                            Value="" />
                    </div>
                </div>
                <!-- end of content -->
                <div class="cl">
                    &nbsp;</div>
            </div>
            <!-- end of shell -->
    </section><!--/#portfolio-->
     </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkSearchMenu" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- main -->
    <!-- end of content -->
    <div class="cl">
        &nbsp;</div>
  <!-- end of main -->
    <!-- The Modal -->
<div id="myModal" class="modal">
    <div class="modal-content">
        <img class="" id="img01">
        <span class="cross">×</span>
    </div>
  <div id="caption" runat="server" clientidmode="Static"></div>
</div>

<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById('caption');
   
 function change(id) {
        modal.style.display = "block";
        modalImg.src = id.src;
        modalImg.title = id.title;
        captionText.innerHTML = id.title;
   }
    // Get the <span> element that crosss the modal
    var span = document.getElementsByClassName("cross")[0];
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>
</asp:Content>

