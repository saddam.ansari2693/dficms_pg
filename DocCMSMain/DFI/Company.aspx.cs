﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using DocCMS.Core;

namespace DocCMSMain.DFI
{
    public partial class About_DFI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Company";
            if (!IsPostBack)
            {
                Bind_CompanyPage(11);
            }
        }
        // Bind Products Function
        protected void Bind_CompanyPage(Int32 PageID)
        {
            try
            {
                DataTable dtCompany = null;
                DataTable dtDetail = null;
                if (Convert.ToString(Request.QueryString["preview"]) == "1")
                {
                    dtCompany = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview(PageID);
                }
                else if (Convert.ToString(Request.QueryString["preview"]) == "2")
                {
                    dtCompany = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview_FromDraft(PageID);
                }
                else
                {
                    dtCompany = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_for_Published(PageID);
                }
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    StringBuilder strProducts = new StringBuilder();
                    for (Int32 i = 0; i < dtCompany.Rows.Count; i++)
                    {
                        if (Convert.ToString(Request.QueryString["preview"]) == "2")
                        {
                            dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid_fromDraft(Convert.ToInt32(dtCompany.Rows[i]["CMSID"]));
                        }
                        else
                            dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtCompany.Rows[i]["CMSID"]));
                       
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            foreach (DataRow rw in dtDetail.Rows)
                            {
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PAGE NAME")
                                    strProducts.Append(" <h1 class='dms-heading'>" + (Convert.ToString(rw["PageContent"])) + "<hr class='fancy-divider'></h1>");

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "SELECT BANNER IMAGE")
                                    ImgBanner.Src = "../UploadedFiles/ContentImages/CompanyPage/" + Convert.ToString(rw["PageContent"]);

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PAGE DESCRIPTION")
                                    strProducts.Append(Convert.ToString(rw["PageContent"]));

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "BANNER IMAGE" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    SectionBannerImg.Visible = true;

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "COMPANY DESCRIPTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                    SectionCompany.Visible = true;
                                    
                                }
                            }
                        }
                    }
                    CompanyDescription.InnerHtml = strProducts.ToString();
                }
                else
                {
                    divPublished.Visible = false;
                    divunpublished.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
         }
    }
}