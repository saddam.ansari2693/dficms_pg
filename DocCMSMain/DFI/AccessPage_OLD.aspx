﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="AccessPage_OLD.aspx.cs" Inherits="DocCMSMain.DFI.AccessPage_OLD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 clearfix" runat="server">      
                    </div>
                   <div class="col-md-6" runat="server" id="DivLogin">
                        <div class="con-form">
                            <h4>Enter User Cardinals to Access Download Page </h4>
                            <asp:Label ID="lblMsg" runat="server" Text="" style="color:Red" Visible="false">User Name or Password is Invalid.</asp:Label>
                            <span class="input input--minoru">
                            <label>User ID</label>
                                <input class="input__field input__field--minoru" type="text" id="txtUserId" placeholder="User Name" runat="server" required />                             
                            </span>
                            <span class="input input--minoru">
                              <label>Password</label>
                                <input runat="server" class="input__field input__field--minoru" type="password" id="txtPassword" placeholder="Password" required/>
                            </span>
                            <span class="input input--minoru">
                               <asp:Panel ID="PnlCapthaImage" runat="server" GroupingText="Image Verification">
                               <asp:Literal ID="FailureText" runat="server" Text="Invalid Captcha Image, please re-enter." Visible="false"></asp:Literal>
                               </asp:Panel>
                            </span>
                          <asp:Button ID="btnSubmit" CssClass="progress-button" runat="server" Text="Submit" data-style="fill" data-horizontal   onclick="btnSubmit_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
</asp:Content>
