﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Text;
using System.Data;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.DFI
{
    public partial class UserLogin : System.Web.UI.Page
    {
        public static Random random = new Random();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["AppUserID"] != null )
            {
                Response.Redirect("dfiuser-download.htm");
            }
            else
            {
                CreateCaptchaControls(PnlCapthaImage);
                if (!IsPostBack)
                {
                    Session["CaptchaImageText"] = GenerateRandomCode();
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            TextBox CodeNumberTextBox = (TextBox)PnlCapthaImage.FindControl("CodeNumberTextBox");
            if (HttpContext.Current.Server.HtmlDecode(CodeNumberTextBox.Text.Trim()) == HttpContext.Current.Session["CaptchaImageText"].ToString())
            {
                DataTable dt = new DataTable();
                dt = ServicesFactory.DocCMSServices.Validate_Application_User(txtUserId.Value, txtPassword.Value);
                if (dt != null && dt.Rows.Count > 0)
                {

                    Session["AppUserID"] = Convert.ToString(dt.Rows[0]["ApplicationUserID"]);
                    if (Request.QueryString["BlogId"] != null && Request.QueryString["BlogId"] != "")
                    {
                        string BlogName = ServicesFactory.DocCMSServices.Fetch_blogname_Byid(Convert.ToString(Request.QueryString["BlogId"]));
                        Response.Redirect("blog-" + BlogName + ".html");
                    }
                    if (Session["AppUserID"] != null)
                    {
                        Response.Redirect("dfiuser-download.htm");
                    }
                    else
                    {
                        Response.Redirect("dfiuser-download.htm", false);
                    }
                }
                else
                {
                    lblMsg.Visible = true;
                }
            }
            else
            {
                CodeNumberTextBox.Text = "";
                HttpContext.Current.Session["CaptchaImageText"] = GenerateRandomCode();
                FailureText.Visible = true;
            }
        }
        // To Generate Random  Code for captcha Image
        public static String GenerateRandomCode()
        {
            String s = "";
            for (int i = 0; i < 6; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }

        public void CreateCaptchaControls(Panel PnlImage)
        {
            HtmlTable CaptchaTable = new HtmlTable();
            CaptchaTable.CellSpacing = 5;
            HtmlTableRow CaptchaTableRow = new HtmlTableRow();
            HtmlTableCell CaptchaTableCell = new HtmlTableCell();
            HtmlImage JpegImageCaptcha = new HtmlImage();
            JpegImageCaptcha.Alt = "No Captcha";
            JpegImageCaptcha.Src = "CaptchaImageGenerator.aspx";
            CaptchaTableCell.Controls.Add(JpegImageCaptcha);
            CaptchaTableRow.Cells.Add(CaptchaTableCell);
            CaptchaTable.Rows.Add(CaptchaTableRow);
            CaptchaTableRow = new HtmlTableRow();
            CaptchaTableCell = new HtmlTableCell();
            CaptchaTableCell.InnerHtml = "<b>Enter the code shown above:</b>";
            CaptchaTableRow.Cells.Add(CaptchaTableCell);
            CaptchaTable.Rows.Add(CaptchaTableRow);
            CaptchaTableRow = new HtmlTableRow();
            CaptchaTableCell = new HtmlTableCell();
            TextBox CodeNumberTextBox = new TextBox();
            CodeNumberTextBox.Attributes.Add("class", "input__field input__field--minoru");
            CodeNumberTextBox.ID = "CodeNumberTextBox";
            RequiredFieldValidator ReqCap = new RequiredFieldValidator();
            ReqCap.ControlToValidate = CodeNumberTextBox.ID;
            ReqCap.ErrorMessage = "Please enter Captcha letters.";
            CaptchaTableCell.Controls.Add(CodeNumberTextBox);
            CaptchaTableRow.Cells.Add(CaptchaTableCell);
            CaptchaTable.Rows.Add(CaptchaTableRow);
            CaptchaTableRow = new HtmlTableRow();
            CaptchaTableCell = new HtmlTableCell();
            CaptchaTableCell.Controls.Add(ReqCap);
            CaptchaTableRow.Cells.Add(CaptchaTableCell);
            CaptchaTable.Rows.Add(CaptchaTableRow);
            PnlImage.Controls.Add(CaptchaTable);
        }
    }// Class Ends Here
}