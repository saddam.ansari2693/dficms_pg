﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using DocCMS.Core;
using System.Data;

namespace DocCMSMain.DFI
{
    public partial class ProductHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Products";
            if (!IsPostBack)
            {
                if (Request.QueryString["CMSID"] != null)
                {
                    Bind_ProductHistory(Convert.ToInt32(Request.QueryString["CMSID"]));
                }
                else
                {
                }
            }
        }
        // Bind Function for Workgroup System
        protected void Bind_ProductHistory(Int32 CMSID)
        {
            try
            {
                //=========BINDING PRODUCT CONTENT
                StringBuilder strContent = new StringBuilder();
                DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(CMSID, "Products");
                if (dtDetail != null && dtDetail.Rows.Count > 0)
                {
                    if (Convert.ToString(dtDetail.Rows[5]["Products"]).ToUpper() == "TRUE")
                    {
                        string ProductName = Convert.ToString(dtDetail.Rows[0]["Products"]);
                        string ImageURL = "../UploadedFiles/ContentImages/Products/" + Convert.ToString(dtDetail.Rows[1]["Products"]);
                        string ProductDescription = Convert.ToString(dtDetail.Rows[2]["Products"]);
                        strContent.Append("<h1>" + ProductName + "<hr class='fancy-divider'></h1>");
                        strContent.Append("<div class='row' style='font-size:16px;'>");
                        strContent.Append("<div class='col-md-7'>" + ProductDescription + " <br><br><a class='product-read-more' href='images/PDF/ProductHistory/oct08-product-history-transition.pdf'>Read More <i class='fa fa-chevron-circle-right'></i></a></div>");
                        strContent.Append(" <div class='col-md-5'><img src='" + ImageURL + "' alt='" + ProductName + "' class='img-responsive'>");
                        strContent.Append("<h3 style='text-align:center;'>" + ProductName + "</h3></div>  </div>");
                    }
                    else
                    {
                        Response.Redirect("Error404.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Error404.aspx");
                }
                DivProductContent.InnerHtml = strContent.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}