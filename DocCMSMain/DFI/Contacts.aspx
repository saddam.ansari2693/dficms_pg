﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Contacts.aspx.cs" Inherits="DocCMSMain.DFI.Contacts" %>
<%@ Register Src="~/Controls/SocialMedia.ascx" TagName="SocialMedia" TagPrefix="UC2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }
    jQuery(document).ready(function () {
    });
</script>
<style type="text/css">
    .contactmedia {
    display: inline-block;
    float: left;
    width: 73px;
    height: 70px;
    }

   .fa-twitter
   {
       color:#0084b4;
   }
   .fa-facebook
   {
       color:#3b5998;
   }
   .fa-google-plus
   {
       color:#d34836;
   }
   .fa-linkedin
   {
       color:#0077B5;
   }
</style>
<style>
.shadow-box {
    box-shadow: 0 0 5px rgba(0, 0, 0, .5);
}

.framed-box {
    border: 1px solid #e3e3e3;
    width: 100%;
}
.framed-box-wrap {
    background-color: #f9f9f9;
}
.framed-box-wrap .pricing-title {
    background-color: #f1f1f1;
    padding: 20px 0px 0px 0px;
    text-align: center;
    border-bottom: 1px solid #fff;
}
.country-name {
    text-align: left;
    margin-left: 15px;
    font-size: 25px;
}
.shadow-box .pricing-text-list {
    padding-bottom: 10px;
    padding: 0 6px 11px 6px;
}
style.css:777
.framed-box-wrap .pricing-text-list {
    padding: 15px 20px 40px 20px;
    text-align: center;
}

.white-bg > ul {
    padding-left: 10px;
    margin-bottom: 25px;
    box-shadow: 0 0 5px rgba(0, 0, 0, .5);
}

ul.list1 {
    padding: 17px;
    margin: 0px;
}

.punchline_text_box {
    float: left;
    width: 100%;
    padding: 25px 30px;
    margin: 0px;
    border: 1px solid #eee;
    border-left: 5px solid #13afeb;
    color: #999;
}
.framed-box-wrap {
    background-color: #f9f9f9;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<link href="css/Popupstylesheet.css" rel="stylesheet" type="text/css" />
 <!-- Banner Section Starts Here -->
        <section class="SectionContact">
            <img src="images/banners/contact_new.jpg"  alt="Contact Us Banner" class="img-responsive" id="ImgConractBanner" runat="server"  />
            <ul class="small-icons-img" id="ContactIcons" runat="server">
            </ul>
        </section>
        <form id="test" >
<input type="button" id="showpopup" name="showpopup" value="showpopup" style="display:none;" />
  </form>
       <section class="contact-section">
            <div class="container">
                <h1>Get In Touch <hr class="fancy-divider"></h1>
                <div class="row">
                 <div class="col-md-6 clearfix" id="DivContactUs" runat="server">
                    <img src="ContactUsImageGenerator.aspx" id="Img4" runat="server" style="width:100%;padding-left:10px" />
                             <dl class="con-info"  id="dlAddress" runat="server" style="font-family:Calibri;"></dl>
                            <div class="col-md-4">
                          <dl style="font-family:Calibri;font-size:25px;color:black;">Contact:</dl>
                          </div>
                          <div class="col-md-10" style="float:right"> 
                          <br />
                          <div class="framed-box clearfix shadow-box" style="position: relative;" runat="server" id="divHeadOffice">
                    </div>
                          <br />
                           <br />
                            <br />
                             <br />
                          <br />
                          </div><br />
                    </div>
                 <div class="col-md-6" runat="server" id="DivContactForm">
                        <div class="con-form">
                            <h3 class="contact-heading" >Contact Form <hr class="fancy-divider"></h3>
                            <span class="input input--minoru">
                                <input class="input__field input__field--minoru" type="text" clientidmode="Static" id="txtFirstName" placeholder="First Name" runat="server" required />
                            </span>
                            <span class="input input--minoru">
                                <input runat="server" class="input__field input__field--minoru" clientidmode="Static" type="text" id="txtLastName" placeholder="Last Name" required/>
                            </span>
                             <span class="input input--minoru">
                                <input runat="server" class="input__field input__field--minoru"  onkeypress="javascript:return isNumber(event)" pattern=".{10,}" maxlength="10" clientidmode="Static" type="text" id="txtContact" placeholder="Contact Number" required/>
                            </span>
                            <span class="input input--minoru">
                                <input runat="server"  class="input__field input__field--minoru" clientidmode="Static"  id="txtEmailId" placeholder="Email ID"  required />
                               <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Please enter valid email Id"
                                        ControlToValidate="txtEmailId" ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="grpsubmit" ClientIDMode="Static" Display="Dynamic"></asp:RegularExpressionValidator>
                            </span>
                            <span class="input input--minoru">
                                <input  runat="server" class="input__field input__field--minoru" clientidmode="Static" type="text" id="txtSubject" placeholder="Subject" required/>
                            </span>
                            <span class="con-tarea" rows="5">
                                <textarea runat="server" class="input__field input__field--minoru" clientidmode="Static" style="margin:0px;" rows="5" id="txtMessage" placeholder="Message" required></textarea>
                            </span>
                                 <span class="input input--minoru">
                                 <asp:Panel ID="PnlCapthaImage" runat="server" GroupingText="Image Verification">
                               </asp:Panel>
                               <asp:Panel ID="Panel1" runat="server">
                               <asp:TextBox ID="txtcaptcha" runat="server" CssClass="input__field input__field--minoru" ValidationGroup="grpsubmit"></asp:TextBox>
                               <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" Text="Please enter Text from image in the text box" style="font:16px;font-weight:bold;color:Red;" ValidationGroup="grpsubmit" ControlToValidate="txtcaptcha"></asp:RequiredFieldValidator>
                              <br /><asp:Label ID="FailureText" runat="server" Text="Invalid Captcha Image, please re-enter." ForeColor="Red" Visible="false"></asp:Label>
                               </asp:Panel>
                              </span>
                              <asp:Button CssClass="progress-button"  data-style="fill" data-horizontal 
                                ID="BtnSubmit" runat="server" Text="Submit" onclick="BtnSubmit_Click" ValidationGroup="grpsubmit" ></asp:Button>
                                <div id="btnCancel" class="progress-button grey-color" style="cursor:pointer;" clientidmode="Static" runat="server" onclick="return Cancel();" >Cancel</div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 89px;" runat="server" id="divOtherAddress">
                </div>
                <div class="row">
                 <dl class="con-info"  id="dlEvents" runat="server" style="font-family:Calibri;" ></dl>
                             <dl class="con-info" id="dlSocial" runat="server" style="font-family:Calibri;" visible="false">
                        </dl>
                        <div class="floating-box col-md-2">
                         <dt style="font-size:19px">Social Support :</dt>
                         </div>
             <div id="services" style="padding: 0 0 0 72px !important;" class="abcaa floating-box col-md-8">
             <div class="">
             <div class=""  id="DivSocialMedia" runat="server">
             </div>
        </div>
    </div>
                </div>
            </div>
        </section>
        <!-- Contact Us Section Ends Here -->
        <!------Event list starts here--->
            <div id="event" > 
	   		<div class="inner">
		 		<h2 style="margin:10px;">Event List</h2>
                   <table id="dyntable" class="table table-bordered responsive" style="width: 95%;">
                                <colgroup>
                                    <col class="con1" style="align: left; width: 40%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                    <col class="con0" style="align: right; width: 30%;" />
                                </colgroup>
                                <thead>
                                    <tr style="width: 100%;">
                                        <th style=" text-align:center;">
                                            Date
                                        </th>
                                         <th style=" text-align:center;">
                                            Area
                                        </th>
                                        <th style=" text-align:center;">
                                            Event&nbsp;Name
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    <asp:Repeater runat="server" ID="RptrEvent" OnItemDataBound="RptrEvent_ItemDataBound">
                                        <ItemTemplate>
                                            <tr class="gradeX">
                                                <td style=" text-align:center;">
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblEventDate" runat="server" Text='<%# Eval("EventDate") %>'></asp:Label>
                                                </td>
                                               <td style=" text-align:center;">
                                                 <asp:Label ID="lblCountryName" runat="server" Text='<%# Eval("CountryName") %>'  Visible="false"></asp:Label>
                                                 <asp:Literal id="litCountryFlag" runat="server"></asp:Literal>
                                                 
                                                 </td>
                                                <td style=" text-align:center;">
                                                    <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' CssClass="DorderLabel"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="gradeA">
                                               <td style=" text-align:center;">
                                                    <asp:Label ID="lblCMSID" runat="server" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblEventDate" runat="server" Text='<%# Eval("EventDate") %>'></asp:Label>
                                                </td>
                                                 <td style=" text-align:center;">
                                                 <asp:Label ID="lblCountryName" runat="server" Text='<%# Eval("CountryName") %>' Visible="false"></asp:Label>
                                                 <asp:Literal id="litCountryFlag" runat="server"></asp:Literal>
                                                 </td>
                                                <td style=" text-align:center;">
                                                    <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' CssClass="DorderLabel"></asp:Label>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table> 
			</div>
		 <a href="Contact.htm" class="cancel" >&times;</a> 
		</div>
		 <div id="cover" > </div>
    <!--/#terms-->
    <style type="text/css">
        .Background
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        
        .Popup
        {
            width: 40%;
            min-width: 350px;
        }
        .close
        {
            margin-top: -20px;
            margin-right: -20px;
            background: black;
            color: #fff;
        }
        .close:hover
        {
            background: #f9f7f6;
        }
    </style>
        <!------Event list end here--->
        <!-- Map Section Starts Here -->
       <section class="map">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2370.280044967141!2d-113.62457668401753!3d53.5527681669302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x53a021204fe21a89%3A0x2d4865146f6e2f12!2s17505+107+Ave+NW+%23103%2C+Edmonton%2C+AB+T5S+1E5%2C+Canada!5e0!3m2!1sen!2sin!4v1450678777099" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </section>
  
        <!-- Map Section Ends Here -->
 <style type="text/css">
 dt {
    font-weight:normal !important;
    color:#000;
}
a.specialeffects {
  color: #000 ;
 
} 
a.specialeffects:hover {
  color: red ;
 
} 

 </style>
 <script type="text/javascript">
     function Save() {
         alert("Your query has been sent successfully");
     }
     function openevent() {
         var showpopup = document.getElementById("showpopup");
         showpopup.click();
     }
     function FireMessage(Status) {
         if (Status == 1) {
             alert("Your query has been send successfully");
         }
         else {
             alert("Unable to send email, message: 'An error occurred while sending mail");
         }
         window.location = "../dfi/Contacts.htm";
     }
     $(function () {
         $("#showpopup").click(function () {
             alert('clicked!');
         });
     });

     function Cancel() {
         $('#txtFirstName').val("");
         $('#txtLastName').val("");
         $('#txtEmailId').val("");
         $('#txtSubject').val("");
         $('#txtMessage').val("");
         $('#txtMessage').val("");
         $('#RegularExpressionValidator7').hide();
     }
 </script>
</asp:Content>


