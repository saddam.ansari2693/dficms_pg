﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;
using DocCMS.Core.DataTypes;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Mail;
using DocCMSMain.Emails;
using System.Net;
using System.Web.Services.Description;
using System.IO;

namespace DocCMSMain.DFI
{
    public partial class Contacts : System.Web.UI.Page
    {
        public static Random random = new Random();

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Contact";
            if (!IsPostBack)
            {
                Bind_ContactIcon(70);
                Bind_Address(29);
                Bind_Events(51);
                Bind_SocialMedia(7);
                Bind_All_Event(51);
                Bind_HeadOffice_Address();
                Bind_Other_Address(73);
                CreateCaptchaControls(PnlCapthaImage);
                Session["CaptchaImageText"] = GenerateRandomCode();
            }
        }

        //Bind office head office 
        protected void Bind_Other_Address(Int32 PageID)
        {
            StringBuilder strAddress = new StringBuilder();
            DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage_Except_HeadOfficecmsid(PageID);
            if (dtContact != null && dtContact.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                {
                    DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                    if (dtDetail != null && dtDetail.Rows.Count > 0)
                    {
                        string OfficeName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                        string Flag = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                        string Address = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                        string AddressURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                        strAddress.Append(" <div class='col-md-4'>");
                        strAddress.Append(" <div class='thumbnail'>");
                        strAddress.Append(" <div class='framed-box clearfix shadow-box' >");
                        strAddress.Append(" <div class='framed-box-wrap'>");
                        strAddress.Append(" <div class='pricing-title>'");
                        strAddress.Append("<h4 class='nocaps country-name' style='color: #13afeb;'>");
                        strAddress.Append("  <strong style='padding-left:9px !important;'>" + OfficeName + "</strong>");
                        strAddress.Append("  <img src='../UploadedFiles/ContentImages/ContactAddress/" + Flag + "' alt='US Flag' style='padding: 4px 5px 8px 3px;'>");
                        strAddress.Append(" </h4>");
                        strAddress.Append(" </div>");
                        strAddress.Append("  <div class='pricing-text-list white-bg clearfix'>");
                        strAddress.Append("     <ul class='list1 punchline_text_box'>");
                        strAddress.Append(" <img src='ImageGenerator2.aspx?id=" + Convert.ToInt32(dtContact.Rows[i]["CMSID"]) + "' id='Img1' runat='server' style='width:100%;' />");
                        strAddress.Append(" </ul>");
                        strAddress.Append(" <div class='map_usa'>");
                        strAddress.Append("  <iframe src='" + AddressURL + "' width='100%' height='250' frameborder='0' style='border:0' allowfullscreen=''></iframe>");
                        strAddress.Append(" </div> ");
                        strAddress.Append(" </div>");
                        strAddress.Append(" </div> ");
                        strAddress.Append(" </div>");
                        strAddress.Append(" </div>");
                        strAddress.Append(" </div>");
                    }
                }
                divOtherAddress.InnerHtml = strAddress.ToString();
            }
        }
      
        protected void Bind_HeadOffice_Address()
        {
            StringBuilder strAddress = new StringBuilder();
            Int32 HeadOfficeID = ServicesFactory.DocCMSServices.Fetch_cmsid_For_Head_Office();
            DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(HeadOfficeID);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                string OfficeName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                string Flag = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                string Address = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                string AddressURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                strAddress.Append("  <div class='framed-box-wrap'>");
                strAddress.Append(" <div class='pricing-title'>");
                strAddress.Append("    <h4 class='nocaps country-name' style='color: #13afeb;'>");
                strAddress.Append("  <strong>"+OfficeName+"</strong>");
                strAddress.Append("  <img src='../UploadedFiles/ContentImages/ContactAddress/" + Flag + "' alt='US Flag'>");
                strAddress.Append(" </h4>");
                strAddress.Append(" </div>");
                strAddress.Append("  <div class='pricing-text-list white-bg clearfix'>");
                strAddress.Append("     <ul class='list1 punchline_text_box'>");
                strAddress.Append(" <img src='ImageGenerator2.aspx?id=" + HeadOfficeID + "' id='Img1' runat='server' style='width:100%;' />");
                strAddress.Append(" </ul>");
                strAddress.Append(" <div class='map_usa'>");
                strAddress.Append("  <iframe src='" + AddressURL + "' width='100%' height='250' frameborder='0' style='border:0' allowfullscreen=''></iframe>");
                strAddress.Append(" </div> ");
                strAddress.Append(" </div>");
                strAddress.Append(" </div>");
                divHeadOffice.InnerHtml = strAddress.ToString();
             }
        }

        // Bind ContactPage Icons
        protected void Bind_ContactIcon(Int32 PageID)
        {
            try
            {

                DataTable dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtContact != null && dtContact.Rows.Count > 0)
                {
                    StringBuilder strContact = new StringBuilder();

                    for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {

                            string IconName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string IconImage = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string DelayTime = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string Description = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[4]["PageContent"]);
                            if (i == 5 || i == 4)
                            {
                                strContact.Append("<li class='wow bounceInDown animated tooltipleft' data-wow-delay='" + DelayTime + "' style='visibility: visible; animation-delay: " + DelayTime + ";' >");
                                strContact.Append("<a href='" + NavigationURL + "' target='_blank'><img src='../UploadedFiles/ContentImages/ContactIcon/" + IconImage + "' />");
                                strContact.Append("<span class='tooltiptextleft'>" + Description + "</span></a></li>");
                            }
                            else
                            {
                                strContact.Append("<li class='wow bounceInDown animated tooltipleft' data-wow-delay='" + DelayTime + "' style='visibility: visible; animation-delay: " + DelayTime + ";' >");
                                strContact.Append("<a href='" + NavigationURL + "' target='_blank'><img src='../UploadedFiles/ContentImages/ContactIcon/" + IconImage + "' />");
                                if (Description == "")
                                {
                                    strContact.Append("</a></li>");
                                }
                                else
                                    strContact.Append("<span class='tooltiptextleft'>" + Description + "</span></a></li>");
                            }
                        }
                    }
                    ContactIcons.InnerHtml = strContact.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Generate Random  Code for captcha Image
        public static String GenerateRandomCode()
        {
            String s = "";
            for (int i = 0; i < 6; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }

        public void CreateCaptchaControls(Panel PnlImage)
        {
            HtmlTable CaptchaTable = new HtmlTable();
            CaptchaTable.CellSpacing = 5;
            HtmlTableRow CaptchaTableRow = new HtmlTableRow();
            HtmlTableCell CaptchaTableCell = new HtmlTableCell();
            HtmlImage JpegImageCaptcha = new HtmlImage();
            JpegImageCaptcha.Alt = "No Captcha";
            JpegImageCaptcha.Src = "CaptchaImageGenerator.aspx";
            CaptchaTableCell.Controls.Add(JpegImageCaptcha);
            CaptchaTableRow.Cells.Add(CaptchaTableCell);
            CaptchaTable.Rows.Add(CaptchaTableRow);
            CaptchaTableRow = new HtmlTableRow();
            CaptchaTableCell = new HtmlTableCell();
            CaptchaTableCell.InnerHtml = "<b>Enter the code shown above:</b>";
            CaptchaTableRow.Cells.Add(CaptchaTableCell);
            CaptchaTable.Rows.Add(CaptchaTableRow);
            CaptchaTableRow = new HtmlTableRow();
            CaptchaTableCell = new HtmlTableCell();
            PnlImage.Controls.Add(CaptchaTable);
        }

        // Bind Address Us Detail into dlContactDetail
        protected void Bind_Address(Int32 PageID)
        {
            try
            {
                DataTable dtContact = null;
                if (Request.QueryString["preview"] != null)
                {
                    dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview(PageID);
                }
                else
                {
                    dtContact = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage(PageID);
                }
                if (dtContact != null && dtContact.Rows.Count > 0)
                {
                    StringBuilder strContact = new StringBuilder();
                    for (Int32 i = 0; i < dtContact.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtContact.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string EmailHeading = Convert.ToString(dtDetail.Rows[1]["FieldText"]);
                            string EmailDescription = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string EmailURL = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string CareerHeading = Convert.ToString(dtDetail.Rows[3]["FieldText"]);
                            string CareerDescription = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[4]["PageContent"]);
                            string CorporateHeading = Convert.ToString(dtDetail.Rows[5]["FieldText"]);
                            string CorporateDescription = Convert.ToString(dtDetail.Rows[5]["PageContent"]);
                            string CorporateURL = Convert.ToString(dtDetail.Rows[6]["PageContent"]);
                            string Address = Convert.ToString(dtDetail.Rows[7]["PageContent"]);
                            strContact.Append("<dt>" + EmailHeading + " : </dt>");
                            strContact.Append(" <dd class='link1' style='color:#000;font-size:21px; margin-right:48px;''><a href='" + EmailURL + "' >" + EmailDescription + "</a></dd>");
                            strContact.Append("<dt>" + CareerHeading + " : </dt>");
                            strContact.Append(" <dd class='link1' style='color:#000;font-size:21px;  margin-right:50px;'><a href='" + NavigationURL + ".htm'>" + CareerDescription + "</a></dd>");
                            strContact.Append("<dt>" + CorporateHeading + " : </dt>");
                            strContact.Append(" <dd class='link1' style='color:#000;font-size:21px;  margin-right:120px;'><a href='" + CorporateURL + "'>" + CorporateDescription + "</a></dd>");
                        }
                    }
                    dlAddress.InnerHtml = strContact.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Bind Event into dlEvent Control
        protected void Bind_Events(Int32 PageID)
        {
            try
            {

                StringBuilder strEvents = new StringBuilder();
                strEvents.Append("<dt> Office status : </dt><dd style='float:left; margin-left:130px;'>");
                string status = "";
                string EventName = "";
                DataTable DtEvent = ServicesFactory.DocCMSServices.Fetch_For_Open_And_Close_Event_ByDate(PageID);
                if (DtEvent != null && DtEvent.Rows.Count > 0)
                {
                    EventName = Convert.ToString(DtEvent.Rows[0]["EventName"]);
                    status = Convert.ToString(DtEvent.Rows[0]["EventStatus"]);
                    strEvents.Append(" <dd  style='color:#000;font-size:21px; margin-left:276px; margin-top:-28px;'>" + status + "(office is closed due to  " + EventName + " <a id='a' class='eventbtn'   href='#event' data-toggle='tooltip' data-placement='right' title='View Office event' ><img id='imgevent' src='images/icons/bg_icon_calendar.png' runat='server' title='View Office event'></a></dd>");
                }
                else
                {
                    status = "Open";
                    strEvents.Append(" <dd  style='color:#000;font-size:21px; float:left; margin-left:20px;'>" + status + " <a class='view-more-events' id='a' class='eventbtn' href='#event' title='View Office event' > View Office Events</a></dd>");
                }
                strEvents.Append("</dd>");
                dlEvents.InnerHtml = strEvents.ToString();
                //Binding DivSocialMedia Ends here...
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Bind all event at pop up in repater
        protected void Bind_All_Event(Int32 PageID)
        {
            try
            {
                DataTable dtEvent = ServicesFactory.DocCMSServices.Fetch_All_Event_Bycmsid(PageID);
                if (dtEvent != null && dtEvent.Rows.Count > 0)
                {
                    RptrEvent.DataSource = dtEvent;
                    RptrEvent.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RptrEvent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null)
            {
                Label lblCountryName = (Label)e.Item.FindControl("lblCountryName");
                Literal litCountryFlag = (Literal)e.Item.FindControl("litCountryFlag");
                if (lblCountryName.Text != null && litCountryFlag != null)
                {
                    string CountryName = lblCountryName.Text;
                    string[] values = CountryName.Split(',');
                    for (int i = 0; i < values.Length; i++)
                    {
                        string FlagImage = ServicesFactory.DocCMSServices.Get_Flag_By_Country_name(values[i].Trim());
                        if (!string.IsNullOrEmpty(FlagImage))
                        {
                            litCountryFlag.Text += "<img src='..\\UploadedFiles\\ContentImages\\AreaMaster\\" + FlagImage + "' title='" + values[i].Trim() + "' style='width:30px;height:20px;padding-right:3px;'>";
                        }
                    }
                }
            }
        }


        //Bind Social Media

        protected void Bind_SocialMedia(Int32 PageID)
        {

            try
            {
                DataTable dtSocialMedia = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtSocialMedia != null && dtSocialMedia.Rows.Count > 0)
                {
                    StringBuilder strSocialMedia = new StringBuilder();
                    for (Int32 i = 0; i < dtSocialMedia.Rows.Count; i++)
                    {
                        DataTable dtSocialMediaContent = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtSocialMedia.Rows[i]["CMSID"]), "SocialMedia");
                        if (dtSocialMediaContent != null && dtSocialMediaContent.Rows.Count > 0)
                        {
                            string IconName = Convert.ToString(dtSocialMediaContent.Rows[0]["SocialMedia"]);
                            string NavigationURl = Convert.ToString(dtSocialMediaContent.Rows[1]["SocialMedia"]);
                            string Heading = Convert.ToString(dtSocialMediaContent.Rows[0]["SocialMedia"]);
                            string Description = Convert.ToString(dtSocialMediaContent.Rows[2]["SocialMedia"]);
                            //Binding DivSocialMedia Starts here...
                            if (dtSocialMedia.Rows.Count % 4 == 0)
                            {
                                strSocialMedia.Append("<div class=''>");
                            }
                            else
                            {
                                strSocialMedia.Append("<div class=''>");
                            }
                            strSocialMedia.Append("<div class='media contactmedia'>");
                            strSocialMedia.Append("<div class='pull-left'>");

                            if (IconName.ToUpper().Contains("TWITTER"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-twitter'></i></a>");
                            }
                            else if (IconName.ToUpper().Contains("FACEBOOK"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-facebook'></i></a>");
                            }
                            else if (IconName.ToUpper().Contains("GOOGLE PLUS"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-google-plus'></i></a>");
                            }
                            else if (IconName.ToUpper().Contains("LINKED IN"))
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa-linkedin'></i></a>");
                            }
                            else
                            {
                                strSocialMedia.Append("<a class='icon-md' style='color:#fff;' href='" + NavigationURl + "'><i class='fa fa'></i></a>");
                            }
                            strSocialMedia.Append("</div>");
                            strSocialMedia.Append("<a  style='color:#fff;' href='" + NavigationURl + "' class='link'><div class='media-body'>");
                            strSocialMedia.Append(" <h3 class='media-heading'>" + Heading + "</h3>");
                            strSocialMedia.Append("<p>" + Description + ".</p>");
                            strSocialMedia.Append("</div> </a></div></div><!--/.col-md-4-->");

                        }
                    }
                    DivSocialMedia.InnerHtml = strSocialMedia.ToString();
                    //Binding DivSocialMedia Ends here...
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtcaptcha.Text.Trim() == HttpContext.Current.Session["CaptchaImageText"].ToString())
                {
                    Cls_ContactUs clsContact = new Cls_ContactUs();
                    clsContact.firstname = txtFirstName.Value;
                    clsContact.lastname = txtLastName.Value;
                    clsContact.emailid = txtEmailId.Value;
                    clsContact.subject = txtSubject.Value;
                    clsContact.message = txtMessage.Value;
                    clsContact.phoneno = txtContact.Value;
                    Int32 retVal = ServicesFactory.DocCMSServices.Insert_Enquiry(clsContact);
                    if (retVal > 0)
                    {
                        bool MessageSuccess = SendMail();
                        if (MessageSuccess == true)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>FireMessage(1);</script>");
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>FireMessage(2);</script>");
                        }
                        txtFirstName.Value = "";
                        txtLastName.Value = "";
                        txtEmailId.Value = "";
                        txtSubject.Value = "";
                        txtMessage.Value = "";
                        txtContact.Value = "";
                        txtcaptcha.Text = "";
                        CreateCaptchaControls(PnlCapthaImage);
                        Session["CaptchaImageText"] = GenerateRandomCode();
                        FailureText.Visible = false;
                    }
                }
                else
                {
                    txtcaptcha.Text = "";
                    CreateCaptchaControls(PnlCapthaImage);
                    Session["CaptchaImageText"] = GenerateRandomCode();
                    FailureText.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Create Method for Send Mail
        public bool SendMail()
        {
            bool Retval = true;
            try
            {
                Cls_SMTP objSMTP = ServicesFactory.DocCMSServices.Fetch_SMTP_Detail_By_department("Enquiry");
                if (objSMTP != null)
                {
                    string Password = ServicesFactory.DocCMSServices.Decrypt(Convert.ToString(objSMTP.senderpassword));
                    string subject = txtSubject.Value;
                    //***Mail to Admin starts from here***
                    MailMessage mailToAdmin = new MailMessage();
                    mailToAdmin.IsBodyHtml = true;
                    mailToAdmin.To.Add(objSMTP.toemail);
                    mailToAdmin.From = new MailAddress(objSMTP.senderemailid);
                    mailToAdmin.Subject = subject;
                    if (!string.IsNullOrEmpty(objSMTP.bccemail))
                    {
                        MailAddress BccEmail = new MailAddress(objSMTP.bccemail);
                        mailToAdmin.Bcc.Add(BccEmail);
                    }
                    if (!string.IsNullOrEmpty(objSMTP.ccemail))
                    {
                        MailAddress CcEmail = new MailAddress(objSMTP.ccemail);
                        mailToAdmin.CC.Add(CcEmail);
                    }
                    StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/QueryTemplate.htm"));
                    string readFile = reader.ReadToEnd();
                    string StrContent = "";
                    StrContent = readFile;
                    mailToAdmin.Body = readFile;
                    if (mailToAdmin.Body != null)
                    {
                        mailToAdmin.Body = mailToAdmin.Body.ToString()
                        .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                        .Replace("<%BodyHeader%>", Convert.ToString(txtSubject.Value.Trim()))
                        .Replace("<%UserName%>", Convert.ToString(txtEmailId.Value.Trim()))
                        .Replace("<%ContactNo%>", Convert.ToString(txtContact.Value.Trim()))
                        .Replace("<%Name%>", Convert.ToString(txtFirstName.Value + " " + txtLastName.Value))
                        .Replace("<%BodyMessage%>", Convert.ToString(txtMessage.Value))
                        .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                        .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year));
                    }
                    Retval = ServicesFactory.DocCMSServices.Send_Mail(mailToAdmin, objSMTP);
                    //***************Mail to admin ends here ***////
                    //***Mail to Query user starts from here***
                    string URL = Request.Url.Scheme + "://" + Request.ServerVariables["HTTP_HOST"] + Request.ApplicationPath;
                    MailMessage mailToVisitor = new MailMessage();
                    mailToVisitor.IsBodyHtml = true;
                    mailToVisitor.To.Add(txtEmailId.Value.Trim());
                    mailToVisitor.From = new MailAddress(objSMTP.senderemailid);
                    mailToVisitor.Subject = subject;

                    reader = new StreamReader(Server.MapPath("~/EmailTemplate/QueryMailTemplate.htm"));
                    readFile = reader.ReadToEnd();
                    StrContent = "";
                    StrContent = readFile;
                    mailToVisitor.Body = readFile;
                    string imageURl = "src='" + URL + "/images/Logos/DocLogo2.png" + "'";
                    if (mailToVisitor.Body != null)
                    {
                         mailToVisitor.Body = mailToVisitor.Body.ToString()
                        .Replace("<%LogoImage%>", Convert.ToString(imageURl))
                        .Replace("<%UserName%>", Convert.ToString(txtFirstName.Value + " " + txtLastName.Value))
                        .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year))
                        .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()));
                    }
                    Retval = ServicesFactory.DocCMSServices.Send_Mail(mailToVisitor, objSMTP);
                    //***Mail to Query user  ends here
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Internal error occcured');</script>");
                }
            }
            catch (Exception ex)
            {
                Retval = false;
            }
            return Retval;
        }
    }
}




                   