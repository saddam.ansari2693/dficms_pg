﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;

namespace DocCMSMain.DFI
{
    public partial class CareerDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["JobName"] != null)
            {
                string JobName = Convert.ToString(Request.QueryString["JobName"]);
                string CMSID = Convert.ToString(Request.QueryString["CMSID"]);
                BindJobDetailDescription(Convert.ToInt32(CMSID));
            }
        }

        protected void BindJobDetailDescription(Int32 CMSID)
        {
            try
            {
                StringBuilder strJob = new StringBuilder();
                DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Job_By_cmsid(CMSID);
                if (dtDetail != null && dtDetail.Rows.Count > 0)
                {
                    string JobName = Convert.ToString(dtDetail.Rows[0]["Job"]);
                    string Category = Convert.ToString(dtDetail.Rows[1]["Job"]);
                    string JobDescription = Convert.ToString(dtDetail.Rows[2]["Job"]);
                    string JobDeatilDescription = Convert.ToString(dtDetail.Rows[3]["Job"]);
                    string PdfURL = "../UploadedFiles/ContentImages/Job/" + Convert.ToString(dtDetail.Rows[4]["Job"]);
                    string DisplayOrder = Convert.ToString(dtDetail.Rows[5]["Job"]);
                    strJob.Append("<h3>" + JobName + "</h3>");
                    strJob.Append(" <div  style='font-size:16px;'>" + JobDeatilDescription + "</div>");
                }
                DetailSectionJob.InnerHtml = strJob.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}