﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Downloads2.aspx.cs" Inherits="DocCMSMain.DFI.Downloads2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<section class="dload-section">
            <div class="container">
                <h1 class="dload-heading">DFI User Downloads <hr class="fancy-divider"></h1>
                <div class="container" id="Div1" runat="server">
                <ul class='dloads' id="SectionDownload" runat="server">
                </ul>
                </div>
            </div>
        </section>
<style type="text/css">
.link {
   color: #000;
  -webkit-transition: 300ms;
  -moz-transition: 300ms;
  -o-transition: 300ms;
  transition: 300ms;
}
</style>
</asp:Content>
