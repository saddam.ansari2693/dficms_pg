﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" enableEventValidation="true" CodeBehind="Careers.aspx.cs" Inherits="DocCMSMain.DFI.Careers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/Popupstylesheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Positions Available</h1>
                </div>
                <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="Default.htm">Home</a></li>
                        <li class="active">Careers</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/#title-->
    <section id="SectionJob" class="container" runat="server" >
    </section>
        <div id="jobapplication"  > 
	   		<div class="inner">
		 		<h2>Job Application</h2>
		 		<input type="text" name="fname" class="element text1" placeholder="First Name" id="txtFirstName" runat="server" clientidmode="Static" required>
		 		<input type="text" name="lname" placeholder="Last Name"  id="txtLastName" runat="server" clientidmode="Static"  required>
		 		<input type="text" name="mob" placeholder="Mobile number" onkeypress="return onlyNumbers(event)" maxlength="10"  id="txtPhone" runat="server" clientidmode="Static"  required>
		 		<input type="text" name="email" placeholder="Email Id"  id="txtEmailId" runat="server" clientidmode="Static"  required>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Please enter valid Email Address"
                                        ControlToValidate="txtEmailId" ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="grpsubmit" Display="Dynamic"></asp:RegularExpressionValidator>
	         <asp:UpdatePanel runat="server" ID="update2" UpdateMode="Conditional" >
            <ContentTemplate>                
            <asp:DropDownList ID="ddlJobTitle" runat="server" 
                            ClientIDMode="Static" Style="width: 85.5%;"  Enabled="False"  AutoPostBack="false" OnSelectedIndexChanged="itemSelected"  >
                        </asp:DropDownList>
                        </ContentTemplate>
                 </asp:UpdatePanel> 
              <input type="text" name="resume" placeholder="Resume Heading" id="txtResumeHeading" runat="server" clientidmode="Static" required>
				<span>Upload Resume <asp:FileUpload ID="FileUpload" runat="server" ClientIDMode="Static" CssClass=""
                            Style="margin-top: -25px; margin-left: 120px; width:56.5%;"/>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
    runat="server" ErrorMessage="Please Upload Your Resume" ValidationGroup="grpsubmit" ClientIDMode="Static"
    ControlToValidate="FileUpload"   Style="margin-top:-100px; margin-left: 150px; font-size:12px; color:red" ></asp:RequiredFieldValidator></span> 
    <span style="float:left;font-size:15px" id="SpanSkill" runat="server">Skills</span>
      <asp:CheckBoxList ID="chkSkills" runat="server" AutoPostBack="false"  Width="432px" style="margin: 15px 0 0 17px;"  RepeatColumns="3" RepeatDirection="Horizontal" >
    </asp:CheckBoxList>
    <textarea placeholder="About Yourself" id="txtDescription" runat="server" clientidmode="Static"  required></textarea>
				<h2><asp:Button ID="btnSubmit" data-style="fill" data-horizontal value="Submit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="grpsubmit" /></h2>	
			</div>
		 <a href="Career.htm" class="cancel" >&times;</a> 
		</div>
		 <div id="cover" > </div>
    <style type="text/css">
        .Background
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .Popup
        {
            width: 40%;
            min-width: 350px;
        }
        .close
        {
            margin-top: -20px;
            margin-right: -20px;
            background: black;
            color: #fff;
        }
        .close:hover
        {
            background: #f9f7f6;
        }
        table tbody tr td input
        {
             padding: 0px !Important;
             margin: 0px !Important;
             width: auto !Important;
        }
    </style>
    <script type="text/javascript">
        function PopUp() {
            $('#txtFirstName').val('');
            $('#txtLastName').val('');
            $('#txtEmailId').val('');
            $('#txtDescription').val('');
            $('#txtResumeHeading').val('');
            $('#RequiredFieldValidator1').val('');
            $('#RegularExpressionValidator7').hide();
            var myDDL = $('#ddlJobTitle');
            myDDL[0].selectedIndex = 0;
            $('#txtPhone').val('');
            var btnPopUp = document.getElementById("btnPopUp");
            btnPopUp.click();
        }
        function Cancel() {
            $('#txtFirstName').val('');
            $('#txtLastName').val('');
            $('#txtEmailId').val('');
            $('#txtDescription').val('');
            $('#txtResumeHeading').val('');
            $('#RequiredFieldValidator1').val('');
            $('#RegularExpressionValidator7').val('');
            var myDDL = $('#ddlJobTitle');
            myDDL[0].selectedIndex = 0;
            $('#txtPhone').val('');
        }
        jQuery('.Background').click(function () {
            var id = jQuery(this).attr('id').replace('_backgroundElement', '');
            $find(id).hide();
        });

        $(function () {
            var ddlJobTitle = $('select[id$=ddlJobTitle]');
            ddlJobTitle.removeAttr('onchange');
            ddlJobTitle.change(function (e) {
                if (this.value != 0) {
                    setTimeout('__doPostBack(\'ddlJobTitle\',\'\')', 0);
                }
            });
        });
    </script>
        <script type="text/javascript">
            function onlyNumbers(e) {
                var key;
                if (window.event) {
                    key = window.event.keyCode;     //IE
                }
                else {
                    key = e.which;      //firefox              
                }
                if (key == 0 || key == 8) {
                    return true;
                }
                if (key > 31 && (key < 48 || key > 57))
                    return false;
                return true;
            }

            function SuccessMsg() {
                alert("Job application has been sent successfully");
                url = "Career.htm";
                $(location).attr('href', url);
            }
            function FireMessage(Status) {
                if (Status == 1) {
                    alert("Job application has been sent successfully");
                }
                else {
                    alert("Unable to send email, message: 'An error occurred while sending mail");
                }
                window.location = "../dfi/careers.htm";
           }
            function nofile(DisplayOrder) {
                alert("Sorry, there is no file attached with this Job");
            }
            function setjobTitle(DisplayOrder) {
                var myDDL = $('#ddlJobTitle');
                if (DisplayOrder) {
                    myDDL[0].selectedIndex = DisplayOrder - 1;
                }
            }
    </script>
</asp:Content>


