﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Survey.aspx.cs" Inherits="DocCMSMain.DFI.Survey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Survey</title>
       <link href="css/Popupstylesheet.css" rel="stylesheet" type="text/css" />
    <link href="css/UserVotingStyleSheet.css" rel="stylesheet" type="text/css" />
     <script src="../DFI/js/jquery.js" type="text/javascript"></script>
        <script src="../DFI/js/bootstrap.min.js" type="text/javascript"></script>
 </head>
<body>
    <form id="form1" runat="server">
     <asp:HiddenField ID="hdnClientIPAddress" runat="server" ClientIDMode="Static" />
<a id="a" class='product-apply' href='#userPoll' >Apply <i class='fa fa-check-circle'></i></a>

<asp:Button ID="btnVoting" runat="server" Text="Button"  style="display:none;" 
        onclick="btnVoting_Click" ClientIDMode="Static" />
<asp:Button ID="btnclick" runat="server" Text="Button"  style="display:none;" 
        onclick="btnclick_Click" ClientIDMode="Static" />
        <div Id="DivMain" runat="server">
    <div id="userPoll"> 
	   	<div class="white">
		<div class="blue-sec" id="DivSurvey" runat="server">
		</div>
		<p class="last">DOCFOCUS</p>        
		</div>
		 <a href="Survey.aspx" class="cancel" >&times;</a> 
		</div>
		 <div id="cover" > </div>
</div>
<script type="text/javascript">
       $(document).ready(function() {
            alert(IPAddress);
       };
   function Voting(SurveyID, SurveyQuestionID, AnswerType) {
        var selectedVal = "";
        var checkedvalue = "";
        var arrAns = "";
        var Ans="";
        var ResponseText="";
        var ResponseType=""
        var  AdditionalText="";
        var checkboxValues = [];
        var Ques = SurveyQuestionID;
        // for Radio 
        if (AnswerType == "1") {
            var selected = $('input[name=radio]:checked');
            if (selected.length > 0) {
                selectedVal = selected.attr("id");
                arrAns = selectedVal.toString().split('_');
                    Ans=arrAns[1].toString()+",";
                    ResponseText = $('#txtradio_'+arrAns[1].toString()+'').text();
                    ResponseType="Option";
            }
        }

        //---------------------- for checkbox--------//
        else if (AnswerType == "2") {
              $('input[name=checkbox]:checked').map(function () {
                checkboxValues.push($(this).val());
                 });
                    for(var i=0;i<checkboxValues.length;i++)
                    {
                      Ans=Ans+checkboxValues[i].toString()+",";
                       ResponseText =ResponseText+ $('#txtcheck_'+checkboxValues[i].toString()+'').text()+"~";
                        ResponseType="Check";
                    }
             }

        //----------For Textbox-------------------//
        else {
        ResponseText = $('textarea#message_'+SurveyQuestionID).val();
        AdditionalText="";
        Ans=",";
        ResponseType="Text";
        }

        //-------- Save Response-------------//
        if(Ans.length>0)
        {
        jQuery.ajax({
            contentType: "application/json; charset=utf-8",
            url: "/WebService/DocCMSApi.svc/Insert_Survey_Vote",
            data: { "SurveyID": SurveyID.toString(),
                "IPAddress": IPAddress.toString(),
                "SurveyQuestionID": Ques.toString(),
                "Ans": Ans.toString(),
                "ResponseText": ResponseText.toString(),
                "AdditionalText": AdditionalText.toString(),
                "ResponseType": ResponseType.toString(),
            },
            dataType: "json",
            success: function (data) {
            jQuery("#btnVoting").click();
            },
            error: function (result) {
                alert("Error during operatrion");
            }
        });
        }
        else
        {
           alert("Please Select Atleast one Option");
        }
    }
</script>
</form>
</body>
</html>
