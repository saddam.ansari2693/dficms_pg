﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="DocCMSMain.DFI.Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="divPublished"  runat="server" clientidmode="Static">
        <!-- Banner Section Starts Here -->
        <section class="about-banner" id="SectionBanner" runat="server" visible="false" >
            <img  runat="server" src="images/banners/dms.jpg" id="ImgBanner" alt="DOCFOCUS Product Banner" style="margin:0 auto" class="img-responsive" clientidmode="Static" >
        </section>
        <!-- Banner Section Starts Here -->
        <!-- About Docfocus Section Starts Here -->
         <section class="dms-dfi">
            <div class="container">
                <div class="container" id="DivProducts" runat="server" visible="false">
                     </div>
                       <div class="container" id="SectionElite" runat="server" visible="false">
                   <h2>ELITE <hr class="fancy-divider"></h2>
                   <div  id="DivProductsCategory" runat="server">
                    </div>
                    </div>
            </div>
        </section>
         </div>
     <div id="divunpublished" runat="server" visible="false" align="center">
    <div class="center">
                <h2 id="H1">The Page is under construction</h2>
            </div>
            <div class="gap"></div>
   <img src="images/underConstruction.jpg" />
   <div class="gap"></div>
   <div class="center">
                <h2 id="H2">Comming Soon...</h2>
            </div>
   </div>
        <!-- About Docfocus Section Ends Here -->
</asp:Content>
