﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Blogs.aspx.cs" Inherits="DocCMSMain.DFI.Blogs" %>

<%@ Register Src="~/Controls/BlogRightSideBar.ascx" TagName="BlogSideBar" TagPrefix="Uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="title" class="emerald">
       <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Blog</h1>
                </div>
                <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="Home.html">Home</a></li>
                        <li class="active">Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/#title-->
    <section id="blog" class="container"  style="min-height:400px;" runat="server">
     <div class="row">
            <aside class="col-sm-4 col-sm-push-8">
              <uc1:BlogSideBar did="SideBar1" runat="Server"></uc1:BlogSideBar>
            </aside>  
             <div class="col-sm-8 col-sm-pull-4">
                <div class="blog">
                <asp:ScriptManager ID="MainScriptManager" runat="server" />
                    <asp:UpdatePanel ID="pnlHelloWorld" runat="server">
                        <ContentTemplate>
        <section id="error" class="container" visible="false" runat="server">
          <h1>No Result Found</h1>
          <p>The Page you are looking for doesn't exist...</p>
          <a class="btn btn-success" href="../dfi/Blogs.html">GO BACK TO THE BLOG PAGE</a>
        </section>
         <asp:Repeater ID="rptBlogs" runat="server" OnItemDataBound="rptBlogs_OnItemDataBound" OnItemCommand="OnItemDataBound_ItemCommand">
                <ItemTemplate>
                 <div class="blog-item">
                  <asp:HiddenField ID="hdnBlogType" runat="server" Value='<%# Eval("BlogType") %>'></asp:HiddenField>
                   <asp:HiddenField ID="hdnBlogID" runat="server" Value='<%# Eval("BlogID") %>'></asp:HiddenField>
                        <img id="blogImage" class="img-responsive img-blog" src=""  alt=""  runat="server"  />
                         <div id="divVideoPlayer" runat="server">
                        <video autobuffer controls=''>
                        <source type='video/mp4' src='../UploadedFiles/Blogs/<%#Eval("ImageName")%>'></source>
                        </video>
                        </div>
                        <asp:Label ID="lblImage" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                        <asp:LinkButton ID="lnkNavigationURL" runat="server" Visible="false" Text='<%# Eval("NavigationURL") %>'></asp:LinkButton>
                        <div class="blog-content">
                        <a href='blog-<%# Eval("BlogName") %>.html'><h3><asp:Label ID="lblName" runat="server" Text='<%# Eval("BlogName") %>'></asp:Label></h3></a>
                            <div class="entry-meta">
                                <span><i class="icon-user"></i>  <asp:Label ID="lblBlogBy" runat="server" Text='<%# Eval("BlogPostedBy") %>'></asp:Label></span>
                                <span><i class="icon-folder-close"></i> <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("BlogCategoryName") %>'></asp:Label></span>
                                <span><i class="icon-calendar"></i>  <asp:Label ID="lblDate" runat="server" Text='<%# Eval("CreationDate") %>'></asp:Label></a></span>
                                   <span><i class="icon-comment"></i> <a CommandArgument='<%# Eval("BlogID")%>' ><asp:Label ID="lblCommentCount" runat="server" Text=""></asp:Label> Comments</a></span>
                            </div>
                            <p><asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label></p>
                            <asp:LinkButton ID="lnkReadMore" runat="server" Text="Read More" class="product-read-more" CommandArgument='<%# Eval("BlogID")%>'
                                            CommandName="Read">Read More <i class='fa fa-chevron-circle-right'></i></asp:LinkButton>
                        </div>
                    </div><!--/.blog-item-->
                </ItemTemplate>
                </asp:Repeater>
                <div style="margin-top: 20px;" id="DivKey" runat="server">
                    <table style="width: 600px;">
                        <tr>
                            <td>
                                <asp:LinkButton ID="lbFirst" runat="server" OnClick="lbFirst_Click">First</asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbPrevious" runat="server" 	OnClick="lbPrevious_Click">Previous</asp:LinkButton>
                            </td>
                            <td>
                                <asp:DataList ID="rptPaging" runat="server"  OnItemCommand="rptPaging_ItemCommand" OnItemDataBound="rptPaging_ItemDataBound" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                     <asp:LinkButton ID="lbPaging" runat="server"
                                            CommandArgument='<%# Eval("PageIndex") %>' CommandName="newPage" Text='<%# Eval("PageText") %> ' Width="20px">
						            </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbNext" runat="server" OnClick="lbNext_Click">Next</asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbLast" runat="server" OnClick="lbLast_Click">Last</asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblpage" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
               </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div><!--/.col-md-8-->
        </div><!--/.row-->
    </section>
    <!--/#blog-->
</asp:Content>
