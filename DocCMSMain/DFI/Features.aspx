﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true"
    CodeBehind="Features.aspx.cs" Inherits="DocCMSMain.DFI.Features" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Features Section Starts Here -->
    <!-- Banner Section Starts Here -->
    <section class="dms-banner" id="SectionBanner" runat="server" visible="false">
         <img src="~/DFI/images/banners/dms-dfi.jpg"  alt="Contact Us Banner" class="img-responsive" style="margin:0 auto" id="ImgFeatureBanner" runat="server" />
    </section>
    <!-- Banner Section Starts Here -->
    <section class="feature-section" id="SectionFeatures" runat="server" visible="false">
            <div class="container" runat="server" id="DivFeatures" >
        </div>
        </section>
    <!-- Features Section Ends Here -->
</asp:Content>
