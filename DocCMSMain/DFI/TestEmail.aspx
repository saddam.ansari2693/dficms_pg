﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestEmail.aspx.cs" Inherits="DocCMSMain.DFI.TestEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="content">
        <div align="center">
            <div>
                <br />
                <br />
            </div>
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                SMTP&nbsp;Server:</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtSMTPServer" runat="server" ClientIDMode="Static" Width="600"
                        Height="30" Style="float: left; margin-left: 10px;"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="rftitle" ControlToValidate="txtSMTPServer"
                        ValidationGroup="grpsubmit" ForeColor="#DA5251">*</asp:RequiredFieldValidator>
                </span>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                Port&nbsp;No:</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtportNumber" runat="server" ClientIDMode="Static" Width="600"
                        Height="30" Style="float: left; margin-left: 10px;"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtportNumber"
                        ValidationGroup="grpsubmit" ForeColor="#DA5251">*</asp:RequiredFieldValidator>
                </span>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                Active&nbsp;SSL:</strong></span>
            <div id="divSSL" style="float: left; margin-left: 8px;">
                <asp:CheckBox ID="chkSSL" runat="server" />
            </div>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                From&nbsp;Email:</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtFromUser" runat="server" ClientIDMode="Static" Width="600" Height="30"
                        Style="float: left; margin-left: 10px;"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtFromUser"
                        ValidationGroup="grpsubmit" ForeColor="#DA5251">*</asp:RequiredFieldValidator>
                </span>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                From&nbsp;Password:</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtPassword" runat="server" ClientIDMode="Static" Width="600" Height="30"
                        TextMode="Password" Style="float: left; margin-left: 10px;"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtPassword"
                        ValidationGroup="grpsubmit" ForeColor="#DA5251">*</asp:RequiredFieldValidator>
                </span>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                To&nbsp;Email:</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtToEmail" runat="server" ClientIDMode="Static" Width="600" Height="30"
                        Style="float: left; margin-left: 10px;"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtToEmail"
                        ValidationGroup="grpsubmit" ForeColor="#DA5251">*</asp:RequiredFieldValidator>
                </span>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                Subject</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtSubject" runat="server" ClientIDMode="Static" Width="600" Height="30"
                        Style="float: left; margin-left: 10px;"></asp:TextBox></span>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtSubject"
                ValidationGroup="grpsubmit" ForeColor="#DA5251">*</asp:RequiredFieldValidator>
            <br />
            <br />
            <span style="width: 200px; float: left; font-size: 18px; text-align: right;"><strong>
                Body:</strong></span> <span style="width: 300px;">
                    <asp:TextBox ID="txtBody" runat="server" ClientIDMode="Static" TextMode="MultiLine"
                        Width="600" Height="140" Style="float: left; margin-left: 10px;"></asp:TextBox>                                        
                <br />
                </span>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />                
                <br />                
                    <asp:Button ID="btnSendMail" runat="server" Text="Send Email" CssClass="btn" Font-Size="24"
                        Style="float: left; margin-left: 250px;" ValidationGroup="grpsubmit" OnClick="btnSendMail_Click" />                    
                        <br />    
                        <br />    
                        <br />    
                    <asp:Label ID="lblConfirmation" runat="server" ClientIDMode="Static" Style="float: left; margin-left: 250px;"></asp:Label>
        </div>
    </div>
    </form>
</body>
</html>

