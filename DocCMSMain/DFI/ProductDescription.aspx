﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="DocCMSMain.DFI.ProductDescription" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
div.col-md-12 h3 a span 
{
    padding-top:85px;
    display:block;
}

div.col-md-12 h3 
{
    margin:0;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="divPublished"  runat="server" clientidmode="Static">
 <section class="about-dfi" id="SectionBannerImg" runat="server" > <img  id="ImgBanner" style="margin:0 auto" runat="server"  class="img-responsive" /> </section>
    <section class="about-dfi" id="SectionCompany" runat="server"> <div class="container"> <h1 id="headinginfo" runat="server"> <hr class='fancy-divider'></h1>
    <div class='about-dfi' id="CompanyDescription" runat="server"></div> </div> </section>
    </div>
     <div id="divunpublished" runat="server" visible="false" align="center">
    <div class="center">
                <h2 id="H1">The Page is under construction</h2>
            </div>
            <div class="gap"></div>
   <img src="images/underConstruction.jpg" />
   <div class="gap"></div>
   <div class="center">
                <h2 id="H2">Comming Soon...</h2>
            </div>
   </div>
        <!-- About Docfocus Section Ends Here -->
</asp:Content>
