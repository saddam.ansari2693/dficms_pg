﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true"
    CodeBehind="Benefits.aspx.cs" Inherits="DocCMSMain.DFI.Benefits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <!-- Banner Section Starts Here -->
        <section class="dms-banner">
            <img src="images/banners/contact.jpg" alt="Contact Us Banner" class="img-responsive" style="margin:0 auto" id="ImgBanner" runat="server">
        </section>
        <!-- Banner Section Starts Here -->
        <!-- Features Section Starts Here -->
        <section class="feature-section">
            <div class="container">
                <div runat="server" id="DivContent" clientidmode="Static"></div>
            <div runat="server" id="DivBenefits" clientidmode="Static"></div>
            </div>
        </section>
        <!-- Features Section Ends Here -->
        <!-- Benefit Section Ends Here -->
        </section>
    <!-- About Us section Ends Here -->
    <!-- Benefit Section Starts Here -->
</asp:Content>
