﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.Implementation;
using System.Drawing.Imaging;

namespace DocCMSMain.DFI
{
    public partial class CaptchaImageGenerator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Random RandTemp = new Random();
            CaptchaImage ci;
            if (HttpContext.Current.Session["CaptchaImageText"] != null)
            {
                ci = new CaptchaImage(Convert.ToString(HttpContext.Current.Session["CaptchaImageText"]), 200, 50, "Century Schoolbook");
            }
            else
            {
                string CaptchaNumber = Convert.ToString(RandTemp.Next());
                ci = new CaptchaImage(CaptchaNumber, 200, 50, "Century Schoolbook");
                HttpContext.Current.Session["CaptchaImageText"] = CaptchaNumber;
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "image/jpeg";
            ci.Image.Save(HttpContext.Current.Response.OutputStream, ImageFormat.Jpeg);
            ci.Dispose();
        }
    }// Class Ends Here
}