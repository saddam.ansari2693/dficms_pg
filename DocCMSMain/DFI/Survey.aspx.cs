﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core;
using System.Data;
using System.Text;
using System.Net;

namespace DocCMSMain.DFI
{
    public partial class Survey : System.Web.UI.Page
    {
        protected static Int32 QuesId = 0;
        string testip = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Survey();
                testip = hdnClientIPAddress.Value;
            }
        }


        protected void Bind_Survey()
        {
            try
            {
                Int32 SurveyID = ServicesFactory.DocCMSServices.Fetch_DefaultSet_Survey();
                if (SurveyID > 0)
                {

                    DataTable dtQues = ServicesFactory.DocCMSServices.Fetch_All_question(SurveyID);
                    StringBuilder strsurvey = new StringBuilder();
                    for (Int32 Quescount = 0; Quescount < dtQues.Rows.Count; Quescount++)
                    {
                          if (QuesId == Quescount && Convert.ToString(dtQues.Rows[Quescount]["IsActive"]).ToUpper() == "TRUE")
                        {
                            string Type = "";
                            string Question = Convert.ToString(dtQues.Rows[Quescount]["Question"]);
                            string SurveyQuestionID = Convert.ToString(dtQues.Rows[Quescount]["SurveyQuestionID"]);
                            string SurveyAnswerID = "";
                            strsurvey.Append("	<p class='PopupPara'>" + Question + "</p>");
                            DataTable dtAns = ServicesFactory.DocCMSServices.Fetch_All_answer_Byquestionid(Convert.ToInt32(dtQues.Rows[Quescount]["SurveyQuestionID"]));
                            if (dtAns != null && dtAns.Rows.Count > 0)
                            {
                                for (Int32 Anscount = 0; Anscount < dtAns.Rows.Count; Anscount++)
                                {

                                    string Answer = Convert.ToString(dtAns.Rows[Anscount]["Answer"]);
                                    string DisplayOrder = Convert.ToString(dtAns.Rows[Anscount]["DisplayOrder"]);
                                    SurveyAnswerID = Convert.ToString(dtAns.Rows[Anscount]["SurveyAnswerID"]);
                                    if (Convert.ToString(dtQues.Rows[Quescount]["AnswerType"]).ToUpper() == "OPTION")
                                    {
                                        Type = "1";
                                        strsurvey.Append("<div class='input-btn'>");
                                        strsurvey.Append("<input type='radio' id='radio_" + SurveyAnswerID + "' name='radio' />");
                                        strsurvey.Append("<label id='txtradio_" + SurveyAnswerID + "' for='radio_" + SurveyAnswerID + "'><span></span>" + Answer + "</label></div>");
                                    }
                                    else if (Convert.ToString(dtQues.Rows[Quescount]["AnswerType"]).ToUpper() == "CHECK")
                                    {
                                        Type = "2";
                                        strsurvey.Append("<div class='input-btn'>");
                                        strsurvey.Append("<input type='checkbox' id='checkbox_" + SurveyAnswerID + "' value='" + SurveyAnswerID + "' name='checkbox' />");
                                        strsurvey.Append("<label id='txtcheck_" + SurveyAnswerID + "' for='checkbox_" + SurveyAnswerID + "'><span></span>" + Answer + "</label></div>");
                                    }
                                }
                            }
                            else
                            {
                                Type = "3";
                                strsurvey.Append("<div class='input-btn'>");
                                strsurvey.Append(" <textarea rows='4' id='message_" + SurveyQuestionID + "'  name='textbox' ></textarea></div> ");
                            }
                            strsurvey.Append(" <p>See Results  <input type='button' runat='server' placeholder='Enter your Description' class='btn btn-default' value='Vote' onclick='return Voting(" + SurveyID + "," + SurveyQuestionID + "," + Type + ")' /></p>");
                        }
                    }
                    if (strsurvey.ToString() == "")
                    {
                        DivMain.Visible = false;
                        ClientScript.RegisterStartupScript(this.GetType(), "Success Message", "<script>alert('You have successfully completed your survey');</script>");
                        return;
                    }
                    else
                    {
                        DivMain.Visible = true;
                        DivSurvey.InnerHtml = strsurvey.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        protected string GetFQDN()
        {
           
            string fqdn = "";
            string UserHost_ComputerName4 = Dns.GetHostName();//Server Name
            string UserHost_ComputerName5 = Environment.MachineName;//Server Name  
            fqdn = UserHost_ComputerName4 + "~" + UserHost_ComputerName5;
            return fqdn;
        }

        protected void btnVoting_Click(object sender, EventArgs e)
        {
            QuesId = QuesId + 1;
            Response.Redirect("Survey.aspx#userPoll");
            Bind_Survey();
        }

        protected void btnclick_Click(object sender, EventArgs e)
        {
            Bind_Survey();
        }
    }
}