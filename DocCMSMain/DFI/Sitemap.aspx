﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Sitemap.aspx.cs" Inherits="DocCMSMain.DFI.Sitemap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container"> 
<h1 id="ContentPlaceHolder1_headinginfo"> <hr class="fancy-divider"></h1>
    <div class="row">
    <section class="about-dfi" id="ContentPlaceHolder1_CompanyDescription"> <h1 class="dms-heading">Site Map<hr class="fancy-divider"></h1>
    <ul class="SiteMap" id="ulMenu" runat="server" style="border:1px solid black; box-shadow: 10px 10px 10px 10px #888888;">
                <asp:Repeater ID="rptrSitemapMainMenu" runat="server" ClientIDMode="Static" 
                 onitemdatabound="rptrSitemapMainMenu_ItemDataBound" OnItemCommand="rptrSitemapMainMenu_ItemCommand">
                <ItemTemplate>
                 <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                    <li id="liMenuName" runat="server" clientidmode="Static" style ="display:block !important;">
                    <asp:LinkButton ID="lnkNavigation" runat="server" CommandName="NavigateToURL" ClientIDMode="Static"></asp:LinkButton>
                    </li>                  
                    <li class="" id="LiMainSubMenu" runat="server" clientidmode="Static" visible="false">                     
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Literal ID="LitMenuName" runat="server" ClientIDMode="Static"></asp:Literal></a>
                      <ul>
                    <asp:Repeater ID="rptrSitemapSubMenu" runat="server" ClientIDMode="Static"  onitemdatabound="rptrSitemapSubMenu_ItemDataBound" OnItemCommand="rptrSitemapSubMenu_ItemCommand" >
                    <ItemTemplate>                    
                            <asp:Label ID="lblCMSID" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblMainMenuName" runat="server" ClientIDMode="Static" Text='<%# Eval("MainMenuName") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblSubMenuName" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuName") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblSubmenuNavURL" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuNavigationURL") %>' Visible="false"></asp:Label>
                          <li>
                          <asp:LinkButton ID="lnkSubmenu" runat="server" CommandName="SubNavigateToURL" ClientIDMode="Static"></asp:LinkButton>  
                           <div id="divSubMenuLevel1" runat="server">
                          <a href='<%# Eval("SubMenuNavigationURL") %>'  ><asp:Literal ID="LitSubMenuLevel1" runat="server" ClientIDMode="Static"></asp:Literal><i class="icon-angle-down"></i> </a>
                          </div>
                           <ul>
                            <asp:Repeater ID="rptrSubMenuLevel2" runat="server" ClientIDMode="Static"  onitemdatabound="rptrSubMenuLevel2_ItemDataBound" OnItemCommand="rptrSubMenuLevel2_ItemCommand" >
                            <ItemTemplate>     
                           <asp:Label ID="lblSubMenuLevel2Name" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuName") %>' Visible="false"></asp:Label>
                                  <asp:Label ID="lblSubLevel2CMSId" runat="server" ClientIDMode="Static" Text='<%# Eval("CMSID") %>' Visible="false"></asp:Label>
                                  <asp:Label ID="lblSubmenuNavURLLevel2" runat="server" ClientIDMode="Static" Text='<%# Eval("SubMenuNavigationURL") %>' Visible="false"></asp:Label>
                                  <li style="float:left;width:100%;padding-left:25px;"> <asp:LinkButton ID="lnkSubmenuLevel2" runat="server" CommandName="SubLevel2NavigateToURL" ClientIDMode="Static"></asp:LinkButton> </li>
                                  </ItemTemplate>
                                  </asp:Repeater>
                                </ul>                                 
                          </li>
                    </ItemTemplate>
                    </asp:Repeater>                   
                    </ul>
                    </li>
                </ItemTemplate>   
                </asp:Repeater>  
                 <li>&nbsp;</li>
                </ul>
</section>
 </div>
<h1 class="dms-heading"></h1>
</div>
</asp:Content>
