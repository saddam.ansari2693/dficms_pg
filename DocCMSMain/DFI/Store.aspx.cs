﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocCMS.Core.DataTypes;
using System.Data;
using DocCMS.Core;
using System.Web.UI.HtmlControls;

namespace DocCMSMain.DFI
{
    public partial class Store : System.Web.UI.Page
    {
        protected static string PageName = "Store";
        public static List<Cls_OrderDetail> a = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(hdnSession.Value))
                    hdnSession.Value = Session.SessionID;
                hdnSession1.Value = Session.SessionID;
                Bind_Part_Category(62);
                if (string.IsNullOrEmpty(hdnCategoryName.Value))
                    Bind_Parts_Content(hdnCategoryName.Value.Trim());
                Session["CurrPage"] = PageName;
            }
        }

      
      
        private void Bind_Part_Category(Int32 PageID)
        {
            try
            {
                DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Catalouge_category_cmsid(PageID);
                if (dtDetail != null && dtDetail.Rows.Count > 0)
                {
                    DivMainTable.Visible = true;
                    SectionError.Visible = false;
                    Rptcategory.DataSource = dtDetail;
                    Rptcategory.DataBind();
                    Bind_Parts_Content(Convert.ToString(dtDetail.Rows[0]["CategoryName"]));
                }
                else
                {
                    SectionError.Visible = true;
                    DivMainTable.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Bind_Parts_Content(string CategoryName)
        {
            DataTable dtPartsBind = new DataTable();
            dtPartsBind.Columns.Add("CMSID");
            dtPartsBind.Columns.Add("ProductName");
            dtPartsBind.Columns.Add("ProductNumber");
            dtPartsBind.Columns.Add("Description");
            dtPartsBind.Columns.Add("InternalDescription");
            dtPartsBind.Columns.Add("ImageName");
            dtPartsBind.Columns.Add("CategoryName");
            dtPartsBind.Columns.Add("Cost");
            dtPartsBind.Columns.Add("Price");
            dtPartsBind.Columns.Add("Color");
            dtPartsBind.Columns.Add("Size");
            //Checking  if the page has Display order or not if not then it will not show display order column
            DataTable dtContent = null;
            if (Session["MemberID"] != null)
            {
                dtContent = ServicesFactory.DocCMSServices.Fetch_Catlouge_cmsid_By_category_name(CategoryName, 66, "All");
            }
            else
            {
                dtContent = ServicesFactory.DocCMSServices.Fetch_Catlouge_cmsid_By_category_name(CategoryName, 66, "Public");
            }

            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtContent.Rows.Count; i++)
                {
                    DataTable dtPartInfo = ServicesFactory.DocCMSServices.Fetch_Catalouge_By_SubType_id(Convert.ToInt32(dtContent.Rows[i]["CMSID"]));
                    if (dtPartInfo != null && dtPartInfo.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtPartInfo.Rows)
                        {
                            if (Convert.ToString(rw["ShowPrice"]).ToUpper().Equals("TRUE"))
                            {

                            }
                            else
                            rw["Price"] = "0.00";
                            string Color = "";
                            string Size = "";
                            DataTable dtSizeColor = ServicesFactory.DocCMSServices.Fetch_Product_Default_size_color(Convert.ToString(rw["CMSID"]));
                            if (dtSizeColor != null && dtSizeColor.Rows.Count > 0)
                            {
                                Color = Convert.ToString(dtSizeColor.Rows[0]["Color"]);
                                Size = Convert.ToString(dtSizeColor.Rows[0]["Size"]);
                            }
                            else
                            {
                                Color = "";
                                Size = "";
                            }
                            dtPartsBind.Rows.Add(Convert.ToString(rw["CMSID"]), Convert.ToString(rw["ProductName"]), Convert.ToString(rw["ProductNumber"]), Convert.ToString(rw["Description"]), Convert.ToString(rw["InternalDescription"]), Convert.ToString(rw["ImageName"]), Convert.ToString(rw["CategoryName"]), Convert.ToString(rw["Cost"]), Convert.ToString(rw["Price"]), Color, Size);
                        }
                    }
                }
                if (dtPartsBind != null && dtPartsBind.Rows.Count > 0)
                {
                    RptrParts.DataSource = dtPartsBind;
                    RptrParts.DataBind();
                    DivMainTable.Visible = true;
                    SectionError.Visible = false;
                }
                else
                {
                    RptrParts.DataSource = null;
                    RptrParts.DataBind();
                    DivMainTable.Visible = false;
                    SectionError.Visible = true;
                }
            }
            else
            {
                RptrParts.DataSource = null;
                RptrParts.DataBind();
                DivMainTable.Visible = false;
                SectionError.Visible = true;
            }
        }

        protected void Rptcategory_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                LinkButton lnkMenu = (LinkButton)e.Item.FindControl("lnkMenu");
                if (lnkMenu != null && e.CommandName == "BindParts")
                {
                    if (lnkMenu != null)
                    {
                        hdnCategoryName.Value = lnkMenu.Text.Trim();
                        Bind_Parts_Content(lnkMenu.Text.Trim());
                    }
                }
                if (e.CommandName == "ViewMore")
                {
                    Response.Redirect("../DFI/ProductDetails.aspx?CMSID=" + e.CommandArgument);
                }
            }
        }

        protected void Rptcategory_databound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && e.Item.ItemIndex == 0)
            {

            }
        }

        protected void Bind_Part_By_Search_Text(string searchTerm)
        {

            DataTable dtPartsBind = new DataTable();
            dtPartsBind.Columns.Add("CMSID");
            dtPartsBind.Columns.Add("ProductName");
            dtPartsBind.Columns.Add("ProductNumber");
            dtPartsBind.Columns.Add("Description");
            dtPartsBind.Columns.Add("InternalDescription");
            dtPartsBind.Columns.Add("ImageName");
            dtPartsBind.Columns.Add("CategoryName");
            dtPartsBind.Columns.Add("Price");
            dtPartsBind.Columns.Add("Color");
            dtPartsBind.Columns.Add("Size");
            //Checking  if the page has Display order or not if not then it will not show display order column
            DataTable dtContent = ServicesFactory.DocCMSServices.Fetch_Catalouge_By_Search_Text(searchTerm, 66);
            if (dtContent != null && dtContent.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dtContent.Rows.Count; i++)
                {
                    DataTable dtPartInfo = ServicesFactory.DocCMSServices.Fetch_Catalouge_By_SubType_id(Convert.ToInt32(dtContent.Rows[i]["CMSID"]));
                    if (dtPartInfo != null && dtPartInfo.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtPartInfo.Rows)
                        {
                            if (Convert.ToString(rw["ShowPrice"]).ToUpper().Equals("TRUE"))
                            {

                            }
                            else
                            {
                                rw["Price"] = "0.00";
                            }
                            dtPartsBind.Rows.Add(Convert.ToString(rw["CMSID"]), Convert.ToString(rw["ProductName"]), Convert.ToString(rw["ProductNumber"]), Convert.ToString(rw["Description"]), Convert.ToString(rw["InternalDescription"]), Convert.ToString(rw["ImageName"]), Convert.ToString(rw["CategoryName"]), Convert.ToString(rw["Price"]));
                        }
                    }
                }
                if (dtPartsBind != null && dtPartsBind.Rows.Count > 0)
                {
                    RptrParts.DataSource = dtPartsBind;
                    RptrParts.DataBind();
                    DivMainTable.Visible = true;
                    DivMainTable.Visible = true;
                    SectionError.Visible = false;
                    RptrParts.DataSource = dtPartsBind;
                    RptrParts.DataBind();
                }
                else
                {
                    DivMainTable.Visible = false;
                    SectionError.Visible = true;
                }
            }
            else
            {
                DivMainTable.Visible = false;
                SectionError.Visible = true;
            }
        }

        protected void lnkSearchMenu_Click(Object sender, EventArgs e)
        {
            string searchTerm = txtSearchMenu.Text;
            bool IsCategoryActive = false;
            IsCategoryActive = ServicesFactory.DocCMSServices.Check_For_category_active(searchTerm, 66);
            if (IsCategoryActive == true)
            {
                Bind_Part_By_Search_Text(searchTerm.Trim());
            }
            else
            {
                DivMainTable.Visible = false;
                SectionError.Visible = true;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "fetchcart", "javascript:Fetch_Shopping_Cart();", true);
        }

        protected void RptrParts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            if (lblPrice != null && !string.IsNullOrEmpty(lblPrice.Text))
            {
                if (lblPrice.Text != "0")
                    lblPrice.Text = "$" + lblPrice.Text;
                else
                    lblPrice.Text = "$0";
            }
            Label lblImageName = (Label)e.Item.FindControl("lblImageName");
            HtmlImage myImg = (HtmlImage)e.Item.FindControl("myImg");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            LinkButton lnkViewMore = (LinkButton)e.Item.FindControl("lnkViewMore");
            if (lblImageName != null && myImg != null)
            {
                if (!string.IsNullOrEmpty(lblImageName.Text))
                {
                }
                else
                    myImg.Src = "~/UploadedFiles/No_image_available.png";
            }
            string Description = "";
            if (lblDescription.Text.Length > 30)
            {
                Description = lblDescription.Text.ToString();
                lblDescription.Text = Description.Substring(0, 30) + "...";
            }
            else
            {
            }
        }

        protected void btnsubmit_onclick(object sender, EventArgs e)
        {
            DataTable dt = ServicesFactory.DocCMSServices.Fetch_Shopping_Temp_Cart_Detail(hdnSession.Value);
            if (dt != null && dt.Rows.Count > 0)
                Response.Redirect("Order.aspx?ShoppingCartId=" + hdnSession.Value.Trim());
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:alert('Please select atleast one item in a cart to proceed');", true);
            }
        }
    }// CLass Ends here
}