﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="GraphicsDesign.aspx.cs" Inherits="DocCMSMain.DFI.GraphicsDesign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div id="divPublished"  runat="server" clientidmode="Static">
 <section class="about-dfi" id="SectionBannerImg" runat="server" visible="false"> <img src="../UploadedFiles/UploadedFiles/ContentImages/CompanyPage/about-banner.png" id="ImgBanner" style="margin:0 auto" runat="server"  class="img-responsive" /> </section>
    <section class="about-dfi" id="SectionGraphics" runat="server" visible="false"> <div class="container"> <h1 id="headinginfo" runat="server"> <hr class='fancy-divider'></h1>
    <section class='about-dfi' id="GraphicsDescription" runat="server"></section> </div> </section>
    </div>
     <div id="divunpublished" runat="server" visible="false" align="center">
    <div class="center">
                <h2 id="H1">The Page is under construction</h2>
            </div>
            <div class="gap"></div>
   <img src="images/underConstruction.jpg" />
   <div class="gap"></div>
   <div class="center">
                <h2 id="H2">Comming Soon...</h2>
            </div>
   </div>
</asp:Content>
