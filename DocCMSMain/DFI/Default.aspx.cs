﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocCMS.Core;
using System.Text;


namespace DocCMSMain.DFI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "Home";
            if (!IsPostBack)
            {
                Bind_Home_Content(56);
                Bind_Projects_category(69);
            }
        }

        public void SliderHideandShow(Int32 SliderId)
        {
            string SliderPosition = ServicesFactory.DocCMSServices.Get_Single_Value_String("SliderPosition", "SliderMaster", "SliderID", Convert.ToString(SliderId), "Number");
            if (SliderPosition != "" && SliderPosition != null)
            {
                hdnSliderPosition.Value = SliderPosition;
                if (SliderPosition.ToUpper() == "RIGHT")
                {
                    RightSliderSlider.Visible = true;
                }
                else if (SliderPosition.ToUpper() == "LEFT")
                {
                    LeftSlider.Visible = true;
                }
                else
                {
                    FullWidthSlider.Visible = true;
                }
            }
        }
        // Binding Product Content whose status is active
        protected void Bind_Home_Content(Int32 PageID)
        {
            try
            {
                DataTable dtHome = null;
                if (Request.QueryString["preview"] != null)
                {
                    dtHome = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_Preview(PageID);
                }
                else
                {
                    dtHome = ServicesFactory.DocCMSServices.Fetch_cmsid_For_active_ContentPage_for_Published(PageID);
                }

               if (dtHome != null && dtHome.Rows.Count > 0)
                {
                    StringBuilder strAboutUs = new StringBuilder();
                    for (Int32 i = 0; i < dtHome.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtHome.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            foreach (DataRow rw in dtDetail.Rows)
                            {
                              
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "NEWS FEED SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                   News_Position(41);
                                }
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "SLIDER SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    SliderHideandShow(1);

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "SOCIAL MEDIA SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    SocialMedia.Visible = true;

                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "CLIENT TESTIMONIALS SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                    testimonial.Visible = true;
                                    Bind_Client_Says(36);
                                }
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PRODUCT SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                    SectionProduct.Visible = true;
                                    Bind_Products(35);
                                }
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PARTNER SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                    SectionPartner.Visible = true;
                                    Bind_Partners(38);
                                }
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "PLAN SECTION" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                {
                                    SectionPlan.Visible = true;
                                    Bind_Plans();
                                }
                                if (Convert.ToString(rw["FieldText"]).ToUpper() == "SURVEY POPUP" && Convert.ToString(rw["PageContent"]).ToUpper() == "TRUE")
                                    surveyHome.Visible = true;
                             }
                        }
                    }
                }
                else
                {
                    divPublished.Visible = false;
                    divunpublished.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Bind About Us Function
        protected void Bind_AboutUs(Int32 PageID)
        {
            try
            {
                DataTable dtAboutUs = ServicesFactory.DocCMSServices.Fetch_Menu_Dashboard_Bypagename(PageID);
                if (dtAboutUs != null && dtAboutUs.Rows.Count > 0)
                {
                    StringBuilder strAboutUs = new StringBuilder();
                    for (Int32 i = 0; i < dtAboutUs.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtAboutUs.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string Heading = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            string Detail = Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string LinkName = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                            strAboutUs.Append(" <section class='about-section'>");
                            strAboutUs.Append("<h1>" + Heading + "<hr class='fancy-divider'></h1>");
                            strAboutUs.Append("<div class='container'>" + Detail + " </div>   </section>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Bind Products Function
        protected void Bind_Products(Int32 PageID)
        {
            try
            {
                DataTable dtProducts = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtProducts != null && dtProducts.Rows.Count > 0)
                {
                    StringBuilder strProducts = new StringBuilder();
                    for (Int32 i = 0; i < dtProducts.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtProducts.Rows[i]["CMSID"]), "Products");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string ProductName = Convert.ToString(dtDetail.Rows[0]["Products"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["Products"]).Replace(".aspx", ".html");
                            string ImageURL = "../UploadedFiles/ContentImages/ProductCategory/" + Convert.ToString(dtDetail.Rows[1]["Products"]);
                            string ProductDescription = Convert.ToString(dtDetail.Rows[2]["Products"]);
                            if (ProductDescription.Length > 80)
                            {
                              ProductDescription = ProductDescription.Substring(0, 80);
                            }
                            strProducts.Append("<div class='col-md-4 product-details'>");
                            strProducts.Append("<a href='.." + NavigationURL + "'><img src='" + ImageURL + "' alt='" + ProductName + " Icon'></a>");
                            strProducts.Append("<h3><a href='.." + NavigationURL + "' class='link'>" + ProductName + "</a></h3>  ");
                            strProducts.Append("<p> " + ProductDescription + " </p>");
                            strProducts.Append("<p><a class='product-read-more' href='.." + NavigationURL + "' style='margin-right:30px;'>Read More <i class='fa fa-chevron-circle-right'></i></a></p></div>");
                        }
                    }
                   DivProducts.InnerHtml = strProducts.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Bind Client Says Function
        protected void Bind_Client_Says(Int32 PageID)
        {
            try
            {
                DataTable dtClients = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtClients != null && dtClients.Rows.Count > 0)
                {
                    StringBuilder strIndicator = new StringBuilder();
                    StringBuilder strClient = new StringBuilder();
                    for (Int32 i = 0; i < dtClients.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtClients.Rows[i]["CMSID"]), "Client");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string ClientName = Convert.ToString(dtDetail.Rows[0]["Client"]);
                            string Comment = Convert.ToString(dtDetail.Rows[2]["Client"]);
                            string ImageURL = "../UploadedFiles/ContentImages/Clients/" + Convert.ToString(dtDetail.Rows[1]["Client"]);
                            if (i == 0)
                            {
                                strIndicator.Append("<li data-target='#quote-carousel' data-slide-to='" + i + "' class='active'>");
                            }
                            else
                            {
                                strIndicator.Append("<li data-target='#quote-carousel' data-slide-to='" + i + "'>");
                            }
                            strIndicator.Append("<img class='img-responsive ' src='" + ImageURL + "' alt=''> </li>");
                            if (i == 0)
                            {
                                strClient.Append("<div class='item active'>");
                            }
                            else
                            {
                                strClient.Append("<div class='item'>");
                            }
                            strClient.Append("  <blockquote>  <div class='row'>  <div class='col-sm-8 col-sm-offset-2'>");
                            strClient.Append(" <p>" + Comment + "</p> <small>" + ClientName + "</small>");
                            strClient.Append("</div> </div> </blockquote> </div>");
                        }
                    }
                    LstIndicator.InnerHtml = strIndicator.ToString();
                    DivClient.InnerHtml = strClient.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Bind Partners Function
        protected void Bind_Partners(Int32 PageID)
        {
            try
            {
                DataTable dtPartners = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid(PageID);
                if (dtPartners != null && dtPartners.Rows.Count > 0)
                {
                    StringBuilder strPartners = new StringBuilder();
                    strPartners.Append("<div class='center'><h2>Our <strong>Partners</strong></h2></div><div class='gap'></div>");
                    strPartners.Append(" <div id='owl-demo' class='owl-carousel owl-theme' runat='server' style='margin-left:112px'>");
                    for (Int32 i = 0; i < dtPartners.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Content_Data_By_cmsid(Convert.ToInt32(dtPartners.Rows[i]["CMSID"]), "FieldValue");
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string PartnersName = Convert.ToString(dtDetail.Rows[0]["FieldValue"]);
                            string ImageURL = "../UploadedFiles/ContentImages/Partners/" + Convert.ToString(dtDetail.Rows[1]["FieldValue"]);
                            strPartners.Append("  <div class='item'>");
                            strPartners.Append("<img src='" + ImageURL + "' alt='" + PartnersName + "'/> </div>");
                        }
                    }
                    strPartners.Append("</div>");
                    DivPartners.InnerHtml = strPartners.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //bind project category
        protected void Bind_Projects_category(Int32 PageID)
        {
            try
            {
                DataTable dtProject = ServicesFactory.DocCMSServices.Fetch_cmsid_Bypageid_isactive(PageID);
                if (dtProject != null && dtProject.Rows.Count > 0)
                {
                    StringBuilder strProjects = new StringBuilder();
                    for (Int32 i = 0; i < dtProject.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtProject.Rows[i]["CMSID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string ProjectName = Convert.ToString(dtDetail.Rows[0]["PageContent"]);
                            Session.Add(Convert.ToString(dtProject.Rows[i]["CMSID"]), "CMSID");
                            string ImageURL = "../UploadedFiles/ContentImages/ProjectCategory/" + Convert.ToString(dtDetail.Rows[1]["PageContent"]);
                            string ProjectDescription = Convert.ToString(dtDetail.Rows[2]["PageContent"]);
                            string NavigationURL = Convert.ToString(dtDetail.Rows[3]["PageContent"]);
                            string ShowDescription = Convert.ToString(dtDetail.Rows[5]["PageContent"]);
                            strProjects.Append("<div class='col-md-4' style='min-height:600px'>");
                            if (Convert.ToString(dtDetail.Rows[1]["PageContent"]) == "" || Convert.ToString(dtDetail.Rows[1]["PageContent"]) == null)
                            {
                                strProjects.Append("<div class='dfi-icon-box'><img src='../UploadedFiles/No_image_available.png' alt='no image' class='img-responsive dms-dfi-icon' >");
                            }
                            else
                            {
                               strProjects.Append("<div class='dfi-icon-box'><img src='" + ImageURL + "' alt='no image' class='img-responsive dms-dfi-icon' >");
                            }
                            strProjects.Append(" <h3>" + ProjectName + " <hr class='fancy-divider'></h3>  ");
                            if (ShowDescription.ToUpper() == "TRUE")
                            {
                                strProjects.Append(" <p style='font-size:16px'>" + ProjectDescription + "</p>");
                            }
                            strProjects.Append(" <p style='padding-left:28%'><a class='product-read-more' href='.." + NavigationURL + "' style='margin-right:30px;'>Read More <i class='fa fa-chevron-circle-right'></i></a></p>");
                            strProjects.Append("</div></div>");
                         }
                    }
                   DivProjectCategory.InnerHtml = strProjects.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Bind About Us Function
        protected void Bind_Plans()
        {
            try
            {
                DataTable dtPlans = ServicesFactory.DocCMSServices.Fetch_Plan();
                if (dtPlans != null && dtPlans.Rows.Count > 0)
                {
                    StringBuilder strPlans = new StringBuilder();
                    for (Int32 i = 0; i < dtPlans.Rows.Count; i++)
                    {
                        DataTable dtDetail = ServicesFactory.DocCMSServices.Fetch_Plan_Detail_Data_By_planid(Convert.ToInt32(dtPlans.Rows[i]["PlanID"]));
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            string PlanName = Convert.ToString(dtPlans.Rows[i]["PlanName"]);
                            string Price = Convert.ToString(dtPlans.Rows[i]["Price"]);
                            strPlans.Append(" <div class='pricing-tables'>");
                            strPlans.Append("<div class='title'>" + PlanName + "</div>");
                            strPlans.Append(" <div class='price'>$" + Price + "<i>/ per month</i></div>");
                            strPlans.Append("<div class='cont-list'><ul>");
                            for (Int32 j = 0; j < dtDetail.Rows.Count; j++)
                            {
                                string ActiveStatus = Convert.ToString(dtDetail.Rows[j]["Active"]).ToUpper();
                                if (ActiveStatus == "TRUE")
                                {
                                    strPlans.Append(" <li>" + dtDetail.Rows[j]["FeatureName"] + "</li>");
                                }
                                else
                                {
                                }
                            }
                            strPlans.Append(" </ul> </div> <div class='ordernow'><a href='#' class='normalbut'>Order Now</a></div></div>");
                            DivPriceTable.InnerHtml = strPlans.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
       }

        // Fuction to show news feed visibility based on Postion value
        protected void News_Position(Int32 PageID)
        {
            try
            {
                DataTable dtNews = ServicesFactory.DocCMSServices.Fetch_News_Show_Position(PageID);
                if (dtNews != null && dtNews.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtNews.Rows)
                    {
                        if (Convert.ToString(rw["Position"]).Contains("Bottom"))
                        {
                            NewsFeedBottomNew.Visible = true;
                        }
                        else if (Convert.ToString(rw["Position"]).Contains("Top"))
                        {
                            NewsFeedTopNew.Visible = true;
                        }
                        else if (Convert.ToString(rw["Position"]).Contains("Both"))
                        {
                            NewsFeedBottomNew.Visible = true;
                            NewsFeedTopNew.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
