﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using DocCMS.Core;
using DocCMS.Core.DataTypes;
using System.Net.Mail;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DocCMSMain.DFI
{
    public partial class Order : System.Web.UI.Page
    {
        DataTable dtShoppingData = null;
        string ShoppingCartId = "";
        public bool IsValid = false;
        public static Int32 PurchaseOrderID;
        StringBuilder sb = new StringBuilder();
        public static string appRootDir = "";
        public static string PDFName = "";
        protected static string PageName = "Store";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrPage"] = "";
            if (Request.QueryString["ShoppingCartId"] != null)
                ShoppingCartId = Convert.ToString(Request.QueryString["ShoppingCartId"]);
            if (!IsPostBack)
            {
                Int32 GetMonth = DateTime.Now.Month;
                Bind_OrderNo();
                Bind_Controls();
                //Hit Counter
           }
            Session["CurrPage"] = PageName;
        }


        //Count Hits Function
        public void Bind_OrderNo()
        {
            try
            {
                string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string numbers = "1234567890";
                string characters = numbers;
                characters += alphabets + numbers;
                int length = 8;
                string otp = string.Empty;
                for (int i = 0; i < length; i++)
                {
                    string character = string.Empty;
                    do
                    {
                        int index = new Random().Next(0, characters.Length);
                        character = characters.ToCharArray()[index].ToString();
                    } 
                    while (otp.IndexOf(character) != -1);
                    otp += character;
                }
                txtPurchaseOrderNo.Value = "DFI-"+otp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("../dfi/Store.aspx");
        }

        protected void PayButton_Click(object sender, EventArgs e)
        {
            try
            {
                Cls_Order clsOrder = new Cls_Order();
                Cls_OrderDetail clsDetail = new Cls_OrderDetail();
                Cls_ShopingCart clscart = new Cls_ShopingCart();
                clsOrder.orderno = txtPurchaseOrderNo.Value;
                clsOrder.companyname = txtCompanyName.Value;
                clsOrder.purchasername = txtPurchaserName.Value.Trim();
                clsOrder.billingcity = txtBillingCityTown.Value.Trim();
                clsOrder.billingaddress1 = txtBillingAddress1.Value.Trim();
                clsOrder.billingaddress2 = txtBillingAddress2.Value.Trim();
                clsOrder.email = txtEmail.Value.Trim();
                clsOrder.fax = txtFax.Value.Trim();
                clsOrder.paymentmethod = drpPayment.SelectedValue;
                clsOrder.phone = txtPhone.Value.Trim();
                clsOrder.billingpostalcode = txtBillingPostalCode.Value.Trim();
                clsOrder.billingprovience = txtBillingProvience.Value.Trim();
                clsOrder.otherdetail = txtotherDetail.Value;
                clsOrder.ponumber = txtPurchasingInformation.Value;
                clsOrder.requesttype = "Order Request";
                clsOrder.orderstatus = "Order Request";
                if (chkShippingAddress.Checked)
                {
                    clsOrder.shippingcity = txtBillingCityTown.Value.Trim();
                    clsOrder.shippingaddress1 = txtBillingAddress1.Value.Trim();
                    clsOrder.shippingaddress2 = txtBillingAddress2.Value.Trim();
                    clsOrder.shippingpostalcode = txtBillingPostalCode.Value.Trim();
                    clsOrder.shippingprovience = txtBillingProvience.Value.Trim();
                }
                else
                {
                    clsOrder.shippingcity = txtShippingCity.Value.Trim();
                    clsOrder.shippingaddress1 = txtShippingAddress1.Value.Trim();
                    clsOrder.shippingaddress2 = txtShippingAddress2.Value.Trim();
                    clsOrder.shippingpostalcode = txtShippingPostalCode.Value.Trim();
                    clsOrder.shippingprovience = txtShippingProvince.Value.Trim();
                }
                if (ChkActiveStatus.Checked)
                    clsOrder.isbackorderallowed = true;
                else
                    clsOrder.isbackorderallowed = false;

                Int32 PurchaseOrderID = ServicesFactory.DocCMSServices.Insert_Cart_Details(clsOrder, ShoppingCartId);
                if (PurchaseOrderID > 0)
                {
                    Create_PDF_For_Order(PurchaseOrderID,clsOrder.requesttype);
                    IsValid = SendMail(clsOrder, PurchaseOrderID, clsOrder.requesttype);
                    if (IsValid)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg();</script>");
                    }
                    else
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('An unexpected error is occured while processing...')</script>");

                    drpPayment.SelectedIndex = 0;
                    txtPurchaseOrderNo.Value = "";
                    txtBillingAddress1.Value = "";
                    txtBillingAddress2.Value = "";
                    txtEmail.Value = "";
                    txtFax.Value = "";
                    txtPhone.Value = "";
                    txtBillingProvience.Value = "";
                    txtShippingCity.Value = "";
                    txtShippingAddress1.Value = "";
                    txtShippingAddress2.Value = "";
                    txtShippingPostalCode.Value = "";
                    txtShippingProvince.Value = "";
                    txtBillingPostalCode.Value = "";
                    txtBillingCityTown.Value = "";
                    txtPurchaserName.Value = "";
                    hdnPDFFileName.Value = @"..\UploadedFiles\Order\" + PDFName.ToString() + ".pdf";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool SendMail(Cls_Order clsOrder, Int32 PurchaseOrderID,string RequestType)
        {

            try
            {
                string subject = "";
                string File="";
                Cls_SMTP objSMTP = ServicesFactory.DocCMSServices.Fetch_SMTP_Detail_By_department("Sales");
                if (objSMTP != null)
                {
                    string[] ToMuliId = null;
                    if (RequestType == "Order Request")
                    {
                        subject = "Request for Order";
                    }
                    else
                    {
                        subject = "Request for Quote";
                    }
                    //**************Mail To Admin starts from here********************
                    MailMessage mailToAdmin = new MailMessage();
                    mailToAdmin.From = new MailAddress(objSMTP.senderemailid.Trim());
                    mailToAdmin.IsBodyHtml = true;
                    if (!string.IsNullOrEmpty(objSMTP.toemail))
                    {
                        ToMuliId = objSMTP.toemail.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ToEMailId in ToMuliId)
                        {
                            mailToAdmin.To.Add(new MailAddress(ToEMailId)); //adding multiple TO Email Id
                        }
                    }
                    if (!string.IsNullOrEmpty(objSMTP.bccemail))
                    {
                        ToMuliId = objSMTP.bccemail.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ToBcc in ToMuliId)
                        {
                            mailToAdmin.Bcc.Add(new MailAddress(ToBcc)); //adding multiple TO Email Id
                        }

                    }
                    if (!string.IsNullOrEmpty(objSMTP.ccemail))
                    {

                        ToMuliId = objSMTP.ccemail.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ToCC in ToMuliId)
                        {
                            mailToAdmin.CC.Add(new MailAddress(ToCC)); //adding multiple TO Email Id
                        }
                    }
                    string IsBackAllowed = "";
                    if (clsOrder.isbackorderallowed)
                        IsBackAllowed = "YES";
                    else
                        IsBackAllowed = "NO";


                    mailToAdmin.Subject = subject;
                    string HTMLPath = "";
                    if (RequestType == "Order Request")
                    {
                        HTMLPath = "~/EmailTemplate/OrderTemplate.htm";
                    }
                    else
                    {
                        HTMLPath = "~/EmailTemplate/QuoteTemplate.htm";
                    }
                    StreamReader reader = new StreamReader(Server.MapPath(HTMLPath));
                    string readFile = reader.ReadToEnd();
                    string StrContent = "";
                    StrContent = readFile;
                    mailToAdmin.Body = readFile;
                    if (RequestType == "Order Request")
                        File = "Order_" + PurchaseOrderID + ".pdf";
                    else
                        File = "Quote_" + PurchaseOrderID + ".pdf";
                    string URL = Request.Url.AbsoluteUri.Replace(Request.RawUrl, "");
                    string Dlink = "href='" + URL + "/UploadedFiles/Order/" + File + "'";
                    mailToAdmin.Attachments.Add(new Attachment(Server.MapPath("~/UploadedFiles/Order/" + File)));
                    if (mailToAdmin.Body != null)
                    {
                         mailToAdmin.Body = mailToAdmin.Body.ToString()
                        .Replace("<%Name%>", Convert.ToString("Admin"))
                        .Replace("<%CustomerName%>", Convert.ToString(clsOrder.purchasername))
                        .Replace("<%OrderNumber%>", Convert.ToString(clsOrder.orderno))
                        .Replace("<%OrderDate%>", Convert.ToString(DateTime.Now.ToString("MM/dd/yyyy")))
                        .Replace("<%PaymentType%>", Convert.ToString(clsOrder.paymentmethod))
                        .Replace("<%BackOrder%>", Convert.ToString(IsBackAllowed))
                        .Replace("<%CustomerEmailID%>", Convert.ToString(clsOrder.email))
                        .Replace("<%PhoneNumber%>", Convert.ToString(clsOrder.phone))
                        .Replace("<%Address%>", Convert.ToString(clsOrder.billingaddress1))
                        .Replace("<%City%>", Convert.ToString(clsOrder.billingcity))
                        .Replace("<%Province%>", Convert.ToString(clsOrder.billingprovience))
                        .Replace("<%Pincode%>", Convert.ToString(clsOrder.billingpostalcode))
                        .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                        .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year))
                        .Replace("<%Downloadlink%>", Convert.ToString(Dlink));
                    }
                    IsValid = ServicesFactory.DocCMSServices.Send_Mail(mailToAdmin, objSMTP);
                    //*****Mail To admin ends here*****************
                    //***Mail to Customer starts from here***
                    MailMessage mailToCustomer = new MailMessage();
                    mailToCustomer.IsBodyHtml = true;
                    mailToCustomer.To.Add(clsOrder.email);
                    mailToCustomer.From = new MailAddress(objSMTP.senderemailid);
                    mailToCustomer.Subject = subject;
                    if (RequestType == "Order Request")
                        reader = new StreamReader(Server.MapPath("~/EmailTemplate/OrderTemplate.htm"));
                    else
                        reader = new StreamReader(Server.MapPath("~/EmailTemplate/QuoteTemplate.htm"));
                    readFile = reader.ReadToEnd();
                    StrContent = "";
                    StrContent = readFile;
                    mailToCustomer.Body = readFile;
                    //Attaching File with Mail
                    mailToCustomer.Attachments.Add(new Attachment(Server.MapPath("~/UploadedFiles/Order/" + File)));
                    if (mailToCustomer.Body != null)
                    {
                        mailToCustomer.Body = mailToCustomer.Body.ToString()
                       .Replace("<%Name%>", Convert.ToString(clsOrder.purchasername))
                       .Replace("<%CustomerName%>", Convert.ToString("you"))
                       .Replace("<%OrderNumber%>", Convert.ToString(clsOrder.orderno))
                       .Replace("<%OrderDate%>", Convert.ToString(DateTime.Now.ToString("MM/dd/yyyy")))
                       .Replace("<%PaymentType%>", Convert.ToString(clsOrder.paymentmethod))
                       .Replace("<%BackOrder%>", Convert.ToString(IsBackAllowed))
                       .Replace("<%CustomerEmailID%>", Convert.ToString(clsOrder.email))
                       .Replace("<%PhoneNumber%>", Convert.ToString(clsOrder.phone))
                       .Replace("<%Address%>", Convert.ToString(clsOrder.billingaddress1))
                       .Replace("<%City%>", Convert.ToString(clsOrder.billingcity))
                       .Replace("<%Province%>", Convert.ToString(clsOrder.billingprovience))
                       .Replace("<%Pincode%>", Convert.ToString(clsOrder.billingpostalcode))
                       .Replace("<%DomainName%>", Convert.ToString(ServicesFactory.DocCMSServices.Get_Domain_name()))
                       .Replace("<%CurrentYear%>", Convert.ToString(DateTime.Now.Year))
                       .Replace("<%Downloadlink%>", Convert.ToString(Dlink));
                    }
                    IsValid = ServicesFactory.DocCMSServices.Send_Mail(mailToCustomer, objSMTP);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('Internal error occcured');</script>");
                }
            }
            catch (Exception ex)
            {
                IsValid = false;
                Response.Write(ex);
            }
            return IsValid;
        }

        protected void Bind_Controls()
        {

            DataTable dtMenus = ServicesFactory.DocCMSServices.Fetch_ItemValue_ForDropDown("Manage Payment Options");
            if (dtMenus != null && dtMenus.Rows.Count > 0)
            {
                drpPayment.DataSource = dtMenus;
                drpPayment.DataTextField = "FieldValue";
                drpPayment.DataValueField = "FieldValue";
                drpPayment.DataBind();
            }
        }
        public void Create_PDF_For_Order(Int32 PurchaseOrderID,string RequestType)
        {
            string OrderDirectoryPath = "";
            string PDFFilePath = "";
            try
            {
                if (RequestType == "Order Request")
                PDFName = "Order_" + PurchaseOrderID;
                else
                PDFName = "Quote_" + PurchaseOrderID;
                string InvoiceImagepath = Server.MapPath("~/images/Logos/DocLogo2-Copy.png");
                OrderDirectoryPath = "~/UploadedFiles/Order/";
                if (!Directory.Exists(Server.MapPath(OrderDirectoryPath)))
                {
                    Directory.CreateDirectory(Server.MapPath(OrderDirectoryPath));
                }
                appRootDir = Server.MapPath(OrderDirectoryPath);
                // Step 1: Creating System.IO.FileStream object
                using (FileStream fs = new FileStream(appRootDir + "/" + PDFName.ToString() + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                // Step 2: Creating iTextSharp.text.Document object
                using (Document doc = new Document(PageSize.A4))
                // Step 3: Creating iTextSharp.text.pdf.PdfWriter object
                // It helps to write the Document to the Specified FileStream
                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    // Step 4: Openning the Document
                    writer.CloseStream = false;
                    doc.Open();
                    //// select the font properties
                    BaseFont bfHeader = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    // TO SET INVOICE HEADING
                    PdfContentByte cbHeading = writer.DirectContent;
                    // select the font properties
                    BaseFont bfcbHeading = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbHeading.BeginText();
                    cbHeading.SetColorFill(new BaseColor(84, 141, 212));
                    cbHeading.SetFontAndSize(bfHeader, 28);
                    Cls_Order clsOrder = ServicesFactory.DocCMSServices.Fetch_Order_List_By_id(PurchaseOrderID);
                    if (clsOrder != null)
                    {
                        if (RequestType == "Order Request")
                        cbHeading.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ORDER REQUEST", 35, 770, 0);
                        else
                        cbHeading.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "QUOTE REQUEST", 35, 770, 0);
                        cbHeading.EndText();

                        cbHeading.SetColorFill(new BaseColor(84, 141, 212));
                        cbHeading.Rectangle(35, 760, 530, 2);
                        cbHeading.Fill();

                        //Adding Image to Right Header
                        iTextSharp.text.Image imagecmp = null;
                        using (FileStream fsimage = new FileStream(InvoiceImagepath, FileMode.Open))
                        {
                            imagecmp = iTextSharp.text.Image.GetInstance(fsimage);
                        }
                        imagecmp.SetAbsolutePosition(409, 765);
                        doc.Add(imagecmp);
                        // Adding Image to Right Header Ends
                        PdfContentByte cbCompany = writer.DirectContent;
                        // select the font properties

                        BaseFont bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 8);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Order By / Bill To:"), 40, 745, 0);
                        cbCompany.EndText();

                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.SetFontAndSize(bfcbCompany, 12);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.purchasername), 40, 730, 0);
                        cbCompany.EndText();

                        //Billing address
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        doc.Add(new Paragraph(5, "\u00a0"));
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.Rectangle(40, 677, 170, 40);
                        cbCompany.Fill();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Billing Address"), 40, 720, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.billingaddress1).Trim() + ", " + Convert.ToString(clsOrder.billingaddress2).Trim(), 40, 710, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.billingcity).Trim() + ", " + Convert.ToString(clsOrder.billingpostalcode).Trim(), 40, 700, 0);
                        cbCompany.EndText();

                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.email).Trim(), 40, 690, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Phone :" + Convert.ToString(clsOrder.phone).Trim(), 40, 680, 0);
                        cbCompany.EndText();

                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.Rectangle(425, 695, 140, 40);
                        cbCompany.Fill();
                        // Billing Address Ends

                        //Shipping address
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        doc.Add(new Paragraph(5, "\u00a0"));
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.Rectangle(190, 677, 200, 40);
                        cbCompany.Fill();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString("Shipping Address"), 240, 720, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.shippingaddress1).Trim() + ", " + Convert.ToString(clsOrder.shippingaddress2).Trim(), 240, 710, 0);
                        cbCompany.EndText();


                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.shippingcity).Trim() + ", " + Convert.ToString(clsOrder.shippingpostalcode).Trim(), 240, 700, 0);
                        cbCompany.EndText();


                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToString(clsOrder.email).Trim(), 240, 690, 0);
                        cbCompany.EndText();

                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(120, 126, 124));
                        cbCompany.SetFontAndSize(bfcbCompany, 10);
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Phone :" + Convert.ToString(clsOrder.phone).Trim(), 240, 680, 0);
                        cbCompany.EndText();

                        cbCompany.SetColorFill(new BaseColor(84, 141, 212));
                        cbCompany.Rectangle(425, 692, 140, 40);
                        cbCompany.Fill();

                        //Shipping address ends here
                        if (RequestType == "Order Request")
                        {
                            //Order Date
                            bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cbCompany.BeginText();
                            cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                            cbCompany.SetFontAndSize(bfcbCompany, 10);
                            cbCompany.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Order Date", 485, 718, 0);
                            cbCompany.EndText();
                        }
                        else
                        {
                            //Quote Date
                            bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cbCompany.BeginText();
                            cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                            cbCompany.SetFontAndSize(bfcbCompany, 10);
                            cbCompany.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Quote Date", 485, 718, 0);
                            cbCompany.EndText();
                        }
                        bfcbCompany = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbCompany.BeginText();
                        cbCompany.SetColorFill(new BaseColor(255, 255, 255));
                        cbCompany.SetFontAndSize(bfcbCompany, 14);
                        //30 November 2016
                        cbCompany.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Convert.ToDateTime(clsOrder.orderdate).ToString("MM/dd/yyyy"), 433, 703, 0);
                        cbCompany.EndText();

                            //Order No.
                            // TO Set the Light Gray Header with Company Name & Invoice No.
                            PdfContentByte cbDetailHeader = writer.DirectContent;
                            BaseFont bfDetailHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cbDetailHeader.SaveState();
                            cbDetailHeader.SetColorFill(new BaseColor(204, 204, 204));
                            cbDetailHeader.Rectangle(425, 735, 140, 18);
                            cbDetailHeader.Fill();
                            cbDetailHeader.RestoreState();
                            cbDetailHeader.BeginText();
                            cbDetailHeader.SetColorFill(BaseColor.WHITE);
                            cbDetailHeader.SetFontAndSize(bfHeader, 10);
                            if (RequestType == "Order Request")
                            {
                               cbDetailHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Order No: " + clsOrder.orderno, 560, 740, 0);
                               cbDetailHeader.EndText();
                            }
                            else
                            {
                                cbDetailHeader.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Quote No: " + clsOrder.orderno, 560, 740, 0);
                                cbDetailHeader.EndText();
                            }
                            if (Convert.ToString(clsOrder.ponumber) != "")
                            {
                                // TO Set the Light Gray Header with PO Number
                                PdfContentByte cbDetailHeader3 = writer.DirectContent;
                                BaseFont bfDetailHeader3 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                                cbDetailHeader.SaveState();
                                cbDetailHeader3.SetColorFill(new BaseColor(204, 204, 204));
                                cbDetailHeader3.Rectangle(425, 675, 140, 18);
                                cbDetailHeader3.Fill();
                                cbDetailHeader3.RestoreState();
                                cbDetailHeader3.BeginText();
                                cbDetailHeader3.SetColorFill(BaseColor.WHITE);
                                cbDetailHeader3.SetFontAndSize(bfHeader, 10);
                                cbDetailHeader3.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Purchase #: " + clsOrder.ponumber.Trim(), 525, 680, 0);
                                cbDetailHeader3.EndText();
                            }
                            //New Grey Box
                            PdfContentByte cbDetailHeader2 = writer.DirectContent;
                            BaseFont bfDetailHeader2 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cbDetailHeader.SaveState();
                            cbDetailHeader.SetColorFill(new BaseColor(204, 204, 204));
                            cbDetailHeader.Rectangle(35, 655, 530, 18);
                            cbDetailHeader2.Fill();
                            cbDetailHeader2.RestoreState();
                            cbDetailHeader2.BeginText();
                            cbDetailHeader2.SetColorFill(BaseColor.WHITE);
                            cbDetailHeader2.SetFontAndSize(bfHeader, 10);
                            cbDetailHeader2.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, " Payment method: " + clsOrder.paymentmethod, 420, 660, 0);
                            cbDetailHeader2.EndText();

                        // TO Set the Light Gray Header with Company Name & Invoice No.
                        PdfContentByte cbDescHeader = writer.DirectContent;
                        BaseFont bfDescHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbDescHeader.SaveState();
                        cbDescHeader.SetColorFill(new BaseColor(84, 141, 212));
                        cbDescHeader.Rectangle(35, 635, 530, 18);
                        cbDescHeader.Fill();
                        cbDescHeader.RestoreState();
                        cbDescHeader.BeginText();
                        cbDescHeader.SetColorFill(BaseColor.WHITE);
                        cbDetailHeader.SetFontAndSize(bfHeader, 10);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Description", 40, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Product Name", 190, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Quantity", 340, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Unit Price", 430, 640, 0);
                        cbDescHeader.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Amount ", 520, 640, 0);
                        cbDescHeader.EndText();
                        // To Add the Invoice Details
                        doc.Add(new Paragraph(175, "\u00a0"));
                        float TotalDues = 0.0f;
                        PdfPTable tblInvoiceDetails = null;
                        PdfPCell cell1 = null;
                        DataTable dt = ServicesFactory.DocCMSServices.Fetch_Order_Detail_List_By_purchaseorderid(PurchaseOrderID);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (Int32 i = 0; i < dt.Rows.Count; i++)
                            {
                                string Categoryname = Convert.ToString(dt.Rows[i]["CategoryName"]).Trim() + "\n";
                                string ProductName = Convert.ToString(dt.Rows[i]["ProductName"]).Trim();
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Color"]).Trim()))
                                {
                                    ProductName = ProductName + " - Color : " + Convert.ToString(dt.Rows[i]["Color"]).Trim();
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Size"]).Trim()))
                                {
                                    ProductName = ProductName + " - Size : " + Convert.ToString(dt.Rows[i]["Size"]).Trim();
                                }
                                string Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                                string Qty = Convert.ToString(dt.Rows[i]["Quantity"]).Trim();
                                string UnitPrice = "$" + Convert.ToString(dt.Rows[i]["Price"]).Trim();
                                string Amount = "$" + Convert.ToString(dt.Rows[i]["RunningPrice"]).Trim();
                                tblInvoiceDetails = new PdfPTable(new float[] { 2f, 30f, 25f, 25f, 20f, 20f });
                                tblInvoiceDetails.WidthPercentage = 100;
                                var MyFont = FontFactory.GetFont("Arial", 10, new BaseColor(105, 109, 108));

                                cell1 = new PdfPCell();
                                cell1.BackgroundColor = new BaseColor(255, 255, 255);
                                cell1.UseVariableBorders = true;
                                cell1.BorderColorLeft = BaseColor.WHITE;
                                cell1.BorderColorRight = BaseColor.WHITE;
                                cell1.BorderColorTop = BaseColor.WHITE;
                                cell1.BorderColorBottom = BaseColor.WHITE;
                                string blueDotPath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/icons/blue-dot.jpg");
                                iTextSharp.text.Image image = null;
                                using (FileStream fsimage = new FileStream(blueDotPath, FileMode.Open))
                                {
                                    image = iTextSharp.text.Image.GetInstance(fsimage);
                                }
                                image.ScaleAbsolute(5f, 5f);
                                image.SpacingBefore = 5f;
                                cell1.AddElement(image);
                                var DescHeadingFont = FontFactory.GetFont("Arial", 10, 1, new BaseColor(105, 109, 108));
                                var DescSubTitleFont = FontFactory.GetFont("Arial", 6, new BaseColor(105, 109, 108));
                                var titleChunk = new Chunk(Categoryname, DescHeadingFont);
                                var descriptionChunk = new Chunk(Description, DescSubTitleFont);
                                var phrase = new Phrase(titleChunk);
                                phrase.Add(descriptionChunk);

                                PdfPCell cell2 = new PdfPCell(phrase);
                                cell2.BackgroundColor = new BaseColor(255, 255, 255);
                                cell2.UseVariableBorders = true;
                                cell2.BorderColorLeft = BaseColor.WHITE;
                                cell2.BorderColorRight = BaseColor.WHITE;
                                cell2.BorderColorTop = BaseColor.WHITE;
                                cell2.BorderColorBottom = BaseColor.WHITE;
                                cell2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;


                                PdfPCell cell3 = new PdfPCell(new Phrase(ProductName, MyFont));
                                cell3.BackgroundColor = new BaseColor(255, 255, 255);
                                cell3.UseVariableBorders = true;
                                cell3.BorderColorLeft = BaseColor.WHITE;
                                cell3.BorderColorRight = BaseColor.WHITE;
                                cell3.BorderColorTop = BaseColor.WHITE;
                                cell3.BorderColorBottom = BaseColor.WHITE;
                                cell3.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                                PdfPCell cell4 = new PdfPCell(new Phrase(Qty, MyFont));
                                cell4.BackgroundColor = new BaseColor(255, 255, 255);
                                cell4.UseVariableBorders = true;
                                cell4.BorderColorLeft = BaseColor.WHITE;
                                cell4.BorderColorRight = BaseColor.WHITE;
                                cell4.BorderColorTop = BaseColor.WHITE;
                                cell4.BorderColorBottom = BaseColor.WHITE;
                                cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                                PdfPCell cell5 = new PdfPCell(new Phrase(UnitPrice, MyFont));
                                cell5.BackgroundColor = new BaseColor(255, 255, 255);
                                cell5.UseVariableBorders = true;
                                cell5.BorderColorLeft = BaseColor.WHITE;
                                cell5.BorderColorRight = BaseColor.WHITE;
                                cell5.BorderColorTop = BaseColor.WHITE;
                                cell5.BorderColorBottom = BaseColor.WHITE;
                                cell5.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                                PdfPCell cell6 = new PdfPCell(new Phrase(Amount, MyFont));
                                cell6.BackgroundColor = new BaseColor(255, 255, 255);
                                cell6.UseVariableBorders = true;
                                cell6.BorderColorLeft = BaseColor.WHITE;
                                cell6.BorderColorRight = BaseColor.WHITE;
                                cell6.BorderColorTop = BaseColor.WHITE;
                                cell6.BorderColorBottom = BaseColor.WHITE;
                                cell6.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                                tblInvoiceDetails.AddCell(cell1);
                                tblInvoiceDetails.AddCell(cell2);
                                tblInvoiceDetails.AddCell(cell3);
                                tblInvoiceDetails.AddCell(cell4);
                                tblInvoiceDetails.AddCell(cell5);
                                tblInvoiceDetails.AddCell(cell6);
                                doc.Add(tblInvoiceDetails);
                                doc.Add(new Paragraph(5, "\u00a0"));

                                // Add Horizontal Line under table Row
                                PdfPTable tblHorizontal = new PdfPTable(1);
                                tblHorizontal.HorizontalAlignment = Element.ALIGN_LEFT;
                                tblHorizontal.WidthPercentage = 100;
                                tblHorizontal.DefaultCell.FixedHeight = 1f;

                                var HorizontalFont = FontFactory.GetFont("Arial", 1, new BaseColor(221, 221, 221));
                                PdfPCell cellHorizontal = new PdfPCell(new Phrase("", HorizontalFont));
                                cellHorizontal.BackgroundColor = new BaseColor(221, 221, 221);
                                cellHorizontal.UseVariableBorders = true;
                                cellHorizontal.BorderColorLeft = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorTop = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorRight = new BaseColor(221, 221, 221);
                                cellHorizontal.BorderColorBottom = new BaseColor(221, 221, 221);
                                tblHorizontal.AddCell(cellHorizontal);
                                doc.Add(tblHorizontal);
                                doc.Add(new Paragraph(10, "\u00a0"));
                            }
                        }
                        //Adding Total
                        doc.Add(new Paragraph(5, "\u00a0"));
                        var Total = FontFactory.GetFont("Arial", 16, 1, new BaseColor(83, 142, 212));
                        var urlFont1 = FontFactory.GetFont("Arial", 16, 1, new BaseColor(83, 142, 212));
                        string GrandTotal = ServicesFactory.DocCMSServices.Fetch_Grand_Total_from_OrderDetail(Convert.ToInt32(PurchaseOrderID));

                        PdfPTable tblTotal = new PdfPTable(new float[] { 80f, 20f });
                        tblTotal.WidthPercentage = 100;

                        PdfPCell cell1Total = new PdfPCell(new Phrase("Total:", Total));
                        PdfPCell cell2Total = new PdfPCell(new Phrase("$" + Convert.ToString(GrandTotal), urlFont1));
                        cell1Total.PaddingBottom = 5f;
                        cell1Total.UseVariableBorders = true;
                        cell2Total.UseVariableBorders = true;

                        cell1Total.BorderColorBottom = BaseColor.WHITE;
                        cell1Total.BorderColorTop = BaseColor.WHITE;
                        cell1Total.BorderColorRight = BaseColor.WHITE;
                        cell1Total.BorderColorLeft = BaseColor.WHITE;

                        cell2Total.BorderColorBottom = BaseColor.WHITE;
                        cell2Total.BorderColorTop = BaseColor.WHITE;
                        cell2Total.BorderColorRight = BaseColor.WHITE;
                        cell2Total.BorderColorLeft = BaseColor.WHITE;

                        cell1Total.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2Total.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        tblTotal.AddCell(cell1Total);
                        tblTotal.AddCell(cell2Total);
                        doc.Add(tblTotal);

                        //Add Other Detal
                        doc.Add(new Paragraph(20, "\u00a0"));
                        doc.Add(new Paragraph(5, "\u00a0"));
                        PdfPTable tblComment2 = new PdfPTable(new float[] { 100f });
                        tblComment2.WidthPercentage = 100;
                        var fontComment2 = FontFactory.GetFont("Arial", 10, new BaseColor(138, 140, 158));
                        PdfPCell cellComment2 = new PdfPCell(new Phrase("Other Detail: " + clsOrder.otherdetail, fontComment2));
                        cellComment2.BackgroundColor = new BaseColor(255, 255, 255);
                        cellComment2.UseVariableBorders = true;
                        cellComment2.PaddingTop = 5f;
                        cellComment2.Colspan = 2;
                        cellComment2.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellComment2.BorderColorLeft = BaseColor.WHITE;
                        cellComment2.BorderColorRight = BaseColor.WHITE;
                        cellComment2.BorderColorTop = BaseColor.WHITE;
                        cellComment2.BorderColorBottom = BaseColor.WHITE;
                        tblComment2.AddCell(cellComment2);
                        doc.Add(tblComment2);

                        // Adding Footer Section
                        doc.Add(new Paragraph(50, "\u00a0"));
                        var ThankyouFont = FontFactory.GetFont("Arial", 16, 1, new BaseColor(83, 142, 212));
                        var urlFont = FontFactory.GetFont("Arial", 8, 1, new BaseColor(83, 142, 212));

                        PdfPTable tblThankYou = new PdfPTable(new float[] { 50f, 50f });
                        tblThankYou.WidthPercentage = 100;

                        PdfPCell cell1ThankYou = new PdfPCell(new Phrase("THANK YOU", ThankyouFont));
                        PdfPCell cell2ThankYou = new PdfPCell(new Phrase("www.docfocus.ca", urlFont));
                        cell1ThankYou.PaddingBottom = 5f;
                        cell1ThankYou.UseVariableBorders = true;
                        cell2ThankYou.UseVariableBorders = true;

                        cell1ThankYou.BorderColorBottom = BaseColor.WHITE;
                        cell1ThankYou.BorderColorTop = BaseColor.WHITE;
                        cell1ThankYou.BorderColorRight = BaseColor.WHITE;
                        cell1ThankYou.BorderColorLeft = BaseColor.WHITE;

                        cell2ThankYou.BorderColorBottom = BaseColor.WHITE;
                        cell2ThankYou.BorderColorTop = BaseColor.WHITE;
                        cell2ThankYou.BorderColorRight = BaseColor.WHITE;
                        cell2ThankYou.BorderColorLeft = BaseColor.WHITE;

                        cell1ThankYou.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cell2ThankYou.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2ThankYou.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        tblThankYou.AddCell(cell1ThankYou);
                        tblThankYou.AddCell(cell2ThankYou);
                        var urlHr = FontFactory.GetFont("Arial", 16, new BaseColor(83, 142, 212));
                        PdfPCell cellThankHR = new PdfPCell(new Phrase("", urlHr));
                        cellThankHR.BackgroundColor = new BaseColor(83, 142, 212);
                        cellThankHR.UseVariableBorders = true;
                        var fontInvoiceNoBottom = FontFactory.GetFont("Arial", 9, new BaseColor(138, 140, 158));
                        if (RequestType == "Order Request")
                        {
                            PdfPCell cellInvoiceNoBottom = new PdfPCell(new Phrase("Order No: " + clsOrder.orderno, fontInvoiceNoBottom));
                            cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                            cellInvoiceNoBottom.UseVariableBorders = true;
                            cellInvoiceNoBottom.PaddingTop = 2f;
                            cellInvoiceNoBottom.Colspan = 2;
                            cellInvoiceNoBottom.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                            cellInvoiceNoBottom.BorderColorLeft = BaseColor.WHITE;
                            cellInvoiceNoBottom.BorderColorRight = BaseColor.WHITE;
                            cellInvoiceNoBottom.BorderColorBottom = BaseColor.WHITE;
                            tblThankYou.AddCell(cellInvoiceNoBottom);
                        }
                        else
                        {
                            PdfPCell cellInvoiceNoBottom = new PdfPCell(new Phrase("Quote No: " + clsOrder.orderno, fontInvoiceNoBottom));
                            cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                            cellInvoiceNoBottom.UseVariableBorders = true;
                            cellInvoiceNoBottom.PaddingTop = 2f;
                            cellInvoiceNoBottom.Colspan = 2;
                            cellInvoiceNoBottom.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                            cellInvoiceNoBottom.BorderColorLeft = BaseColor.WHITE;
                            cellInvoiceNoBottom.BorderColorRight = BaseColor.WHITE;
                            cellInvoiceNoBottom.BorderColorBottom = BaseColor.WHITE;
                            tblThankYou.AddCell(cellInvoiceNoBottom);
                        }
                        var fontBottomMsg = FontFactory.GetFont("Arial", 6, new BaseColor(138, 140, 158));
                        PdfPCell cellBottomMsg = new PdfPCell(new Phrase("All payments can be made to DOCFOCUS and mailed 17505 107 Ave #103 Edmonton, AB T5S 1E5 Canada. If you have any questions, need another copy of the work, estimate, or invoice? Please contact me to address any concerns!", fontBottomMsg));
                        cellThankHR.BackgroundColor = new BaseColor(255, 255, 255);
                        cellBottomMsg.UseVariableBorders = true;
                        cellBottomMsg.PaddingTop = 5f;
                        cellBottomMsg.Colspan = 2;
                        cellBottomMsg.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cellBottomMsg.BorderColorLeft = BaseColor.WHITE;
                        cellBottomMsg.BorderColorRight = BaseColor.WHITE;
                        cellBottomMsg.BorderColorTop = BaseColor.WHITE;
                        cellBottomMsg.BorderColorBottom = BaseColor.WHITE;
                        tblThankYou.AddCell(cellBottomMsg);
                        doc.Add(tblThankYou);
                     }
                    // Step 6: Closing the Document
                    doc.Close();
                }
                  return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ProcessRequest(string fileRelativePath)
        {
            try
            {
                string contentType = "";
                //Get the physical path to the file.
                string FilePath = fileRelativePath;
                contentType = "application/pdf";
                //Set the appropriate ContentType.
                HttpContext.Current.Response.ContentType = contentType;
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(fileRelativePath)).Name);
                //Write the file directly to the HTTP content output stream.
                HttpContext.Current.Response.WriteFile(FilePath);
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                //To Do
                throw ex;
            }
        }

        //Bind Page Text
        public void Bind_Text(Int32 PageID)
        {
            try
            {
                DataTable dtGetPageID = ServicesFactory.DocCMSServices.Fetch_cmsid_For_ContentPage(PageID);
                if (dtGetPageID != null && dtGetPageID.Rows.Count > 0)
                {
                    DataTable DtOrderTextDetails = ServicesFactory.DocCMSServices.Fetch_Page_Contents_By_cmsid(Convert.ToInt32(dtGetPageID.Rows[0]["CMSID"]));
                    if (DtOrderTextDetails != null && DtOrderTextDetails.Rows.Count > 0)
                    {
                        CustomerOrder.InnerText = "Custom Order Information";
                        spnTopButton.InnerHtml = Convert.ToString(DtOrderTextDetails.Rows[1]["PageContent"]);
                        lblNote.InnerText = Convert.ToString(DtOrderTextDetails.Rows[2]["PageContent"]);
                        lblPurchaserName.InnerText = Convert.ToString(DtOrderTextDetails.Rows[4]["PageContent"]);
                        lblPurchaseOrderNo.InnerText = Convert.ToString(DtOrderTextDetails.Rows[5]["PageContent"]);
                        lblAddress1.InnerText = Convert.ToString(DtOrderTextDetails.Rows[6]["PageContent"]);
                        lblAddress2.InnerText = Convert.ToString(DtOrderTextDetails.Rows[7]["PageContent"]);
                        lblCityTown.InnerText = Convert.ToString(DtOrderTextDetails.Rows[8]["PageContent"]);
                        lblProvince.InnerText = Convert.ToString(DtOrderTextDetails.Rows[9]["PageContent"]);
                        lblPostalCode.InnerText = Convert.ToString(DtOrderTextDetails.Rows[10]["PageContent"]);
                        lblPhone.InnerHtml = Convert.ToString(DtOrderTextDetails.Rows[11]["PageContent"]);
                        lblFax.InnerText = Convert.ToString(DtOrderTextDetails.Rows[12]["PageContent"]);
                        lblEmail.InnerText = Convert.ToString(DtOrderTextDetails.Rows[13]["PageContent"]);
                        lblPaymentmethod.InnerHtml = Convert.ToString(DtOrderTextDetails.Rows[14]["PageContent"]);
                        lblAllowBack.InnerHtml = Convert.ToString(DtOrderTextDetails.Rows[15]["PageContent"]);
                        PayButton.Text = Convert.ToString(DtOrderTextDetails.Rows[16]["PageContent"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //function to request quote
        protected void QuoteButton_Click(object sender, EventArgs e)
        {
            try
            {
                Cls_Order clsOrder = new Cls_Order();
                Cls_OrderDetail clsDetail = new Cls_OrderDetail();
                Cls_ShopingCart clscart = new Cls_ShopingCart();
                clsOrder.orderno = txtPurchaseOrderNo.Value;
                clsOrder.companyname = txtCompanyName.Value;
                clsOrder.purchasername = txtPurchaserName.Value.Trim();
                clsOrder.billingcity = txtBillingCityTown.Value.Trim();
                clsOrder.billingaddress1 = txtBillingAddress1.Value.Trim();
                clsOrder.billingaddress2 = txtBillingAddress2.Value.Trim();
                clsOrder.email = txtEmail.Value.Trim();
                clsOrder.fax = txtFax.Value.Trim();
                clsOrder.paymentmethod = drpPayment.SelectedValue;
                clsOrder.phone = txtPhone.Value.Trim();
                clsOrder.billingpostalcode = txtBillingPostalCode.Value.Trim();
                clsOrder.billingprovience = txtBillingProvience.Value.Trim();
                clsOrder.otherdetail = txtotherDetail.Value;
                clsOrder.ponumber = txtPurchasingInformation.Value;
                clsOrder.requesttype = "Quote Request";
                clsOrder.orderstatus = "Quote Request";
                if (chkShippingAddress.Checked)
                {
                    clsOrder.shippingcity = txtBillingCityTown.Value.Trim();
                    clsOrder.shippingaddress1 = txtBillingAddress1.Value.Trim();
                    clsOrder.shippingaddress2 = txtBillingAddress2.Value.Trim();
                    clsOrder.shippingpostalcode = txtBillingPostalCode.Value.Trim();
                    clsOrder.shippingprovience = txtBillingProvience.Value.Trim();
                }
                else
                {
                    clsOrder.shippingcity = txtShippingCity.Value.Trim();
                    clsOrder.shippingaddress1 = txtShippingAddress1.Value.Trim();
                    clsOrder.shippingaddress2 = txtShippingAddress2.Value.Trim();
                    clsOrder.shippingpostalcode = txtShippingPostalCode.Value.Trim();
                    clsOrder.shippingprovience = txtShippingProvince.Value.Trim();
                }
                if (ChkActiveStatus.Checked)
                    clsOrder.isbackorderallowed = true;
                else
                    clsOrder.isbackorderallowed = false;

                Int32 PurchaseOrderID = ServicesFactory.DocCMSServices.Insert_Cart_Details(clsOrder, ShoppingCartId);
                if (PurchaseOrderID > 0)
                {

                    Create_PDF_For_Order(PurchaseOrderID,clsOrder.requesttype);
                    IsValid = SendMail(clsOrder, PurchaseOrderID,clsOrder.requesttype);
                    if (IsValid)
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>SuccessMsg2();</script>");
                    else
                        ClientScript.RegisterStartupScript(this.GetType(), "Sucess", "<script>alert('An unexpected error is occured while processing...')</script>");

                    drpPayment.SelectedIndex = 0;
                    txtPurchaseOrderNo.Value = "";
                    txtBillingAddress1.Value = "";
                    txtBillingAddress2.Value = "";
                    txtEmail.Value = "";
                    txtFax.Value = "";
                    txtPhone.Value = "";
                    txtBillingProvience.Value = "";
                    txtShippingCity.Value = "";
                    txtShippingAddress1.Value = "";
                    txtShippingAddress2.Value = "";
                    txtShippingPostalCode.Value = "";
                    txtShippingProvince.Value = "";
                    txtBillingPostalCode.Value = "";
                    txtBillingCityTown.Value = "";
                    txtPurchaserName.Value = "";
                    hdnPDFFileName.Value = @"..\UploadedFiles\Order\" + PDFName.ToString() + ".pdf";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}