﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/FEMaster.Master" AutoEventWireup="true" CodeBehind="Downloads.aspx.cs" Inherits="DocCMSMain.DFI.Downloads" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<section class="dload-section">
            <div class="container">
                <h1 class="dload-heading">Downloads <hr class="fancy-divider"></h1>
                <div class="container" id="DivDownloads" runat="server">
                <ul class='dloads' id="SectionDownload" runat="server">
                </ul>
                </div>
            </div>
        </section>
</asp:Content>
