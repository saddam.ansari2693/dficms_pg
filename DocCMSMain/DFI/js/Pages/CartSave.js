﻿// To Insert the cart Details
var urlInsertShoppingTempDataUrl = "../WebService/DocCMSApi.svc/Insert_Shopping_Temp_Cart_Detail";
var urlInsertCartDetailDataUrl = "../WebService/DocCMSApi.svc/Insert_Cart_Detail";
var urlUpdateShoppingCart = "../WebService/DocCMSApi.svc/Update_Shopping_cart_By_Id";
var urlDeleteShoppingCart = "../WebService/DocCMSApi.svc/Delete_Shopping_cart";
var urlDeleteShoppingCartById = "../WebService/DocCMSApi.svc/Delete_Shopping_cart_By_Id";
var urlFetchShoppingTempDataUrl = "../WebService/DocCMSApi.svc/Fetch_Shopping_Temp_Cart_Detail";
var urlFetchShoppingTotal = "../WebService/DocCMSApi.svc/Fetch_Shopping_Total";

var shoppingCart = [];
var objCartDetailData;
var Index = 0;
var Quantity = 1;
var index = 0;
//this function manipulates DOM and displays content of our shopping cart
$(document).ready(function () {
    Fetch_Shopping_Cart();
    $(".fancybox-thumb").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });
});

function getKeyCodeFromEvent(e) {
    if (typeof e == "object" && typeof e.which == "number") {
        return e.which;
    }
    return window.event.keyCode;
}

function validate(e) {
    var keyCode = getKeyCodeFromEvent(e);
    var regularExpression = /^[0-9.]+$/;
    // If the key is backspace, tab, left, right or delete
    if ([8, 9, 37, 39, 46].indexOf(keyCode) !== -1 || e.ctrlKey || e.metaKey) {
        return true;
    }
    if (!regularExpression.test(String.fromCharCode(keyCode))) {
        e.preventDefault();
        e.returnValue = false;
        return false;
    }
}

function hover(element) {
    element.setAttribute('src', 'images/delete_active.png');
}
function unhover(element) {
    element.setAttribute('src', 'images/delete_normal.png');
}


function GetTotal() {
    var SessionID = $("#hdnSession1").val();
    $.ajax({
        url: urlFetchShoppingTotal,
        contentType: 'application/json; charset=utf-8',
        data: { "sessionid": SessionID.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null && data.toString() != "0") {
            $("#spnCartTotal").html("$" + data.toString());
        }
        else {
            $("#spnCartTotal").html("$0.00");
        }
    }
    function ajaxFailed() {
   }
}


function Fetch_Shopping_Cart() 
{
    var SessionID = $("#hdnSession1").val();
    $.ajax({
        url: urlFetchShoppingTempDataUrl,
        contentType: 'application/json; charset=utf-8',
        data: { "sessionid": SessionID.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

    function ajaxSucceeded(data) {
        $("#tblCartItems").find(".tr-cartitem").remove();
        var result = jQuery.parseJSON(data);
        if (result != null && result.length > 0) {
            var html = "";
            for (var i = 0; i < result.length; i++) {
                html += '<tr class="order-row tr-cartitem">';
                html += '<td>' + result[i].productname + ' - ' + result[i].category + '</td>';
                html += '<td style="text-align:right; width:25%">';
                html += '<div class="quantity">';
                html += '<label id="btnOrderMinus_' + result[i].id + '"  onclick="ManageOrder(this.id,\'Minus\',' + result[i].id + ',' + parseFloat(result[i].price).toFixed(2) + ');" class="remqty"> - </label>';
                html += '<input type="text" name="quantity" value="' + result[i].quantity + '" onblur="Update_Quanity_By_Id(' + result[i].id + ',this.value,' + parseFloat(result[i].price).toFixed(2) + ');" class="qtyUnit" id="txtQty_' + result[i].id + '">';
                html += '<label id="btnOrderPlus_' + result[i].id + '" onclick="ManageOrder(this.id,\'Plus\',' + result[i].id + ',' + parseFloat(result[i].price).toFixed(2) + ');" class="addqty"> + </label>';
                html += '</div>';
                html += '</td>';
                html += '<td colspan="3" style="padding-right:1px; width:20%;"><div id="divActualPrice" style="display:none;">' + parseFloat(result[i].price).toFixed(2) + '</div>$' + parseFloat(result[i].runningprice).toFixed(2) + '<img style="margin: 0px; cursor: pointer; width: 16px; height: 16px; vertical-align: text-bottom;" onclick="DeleteCartItem(' + result[i].id + ');" onmouseout="unhover(this);" onmouseover="hover(this);" src="images/delete_normal.png" alt="Add To Cart"></td>';
                html += '</tr>';
            }
            $("#btnCancel").show();
            $("#btnsubmit").show();
            $(html).insertAfter($("#tblCartItems").find(".tr-head-container"));
            $("#tblCartItems").find(".tr-items").show();
        }
        else {
            $("#btnCancel").hide();
            $("#btnsubmit").hide();
            $("#tblCartItems").find(".tr-items").hide();
        }
    }
    function ajaxFailed() {
    }
    GetTotal();
}

function AddtoCart(ImgCtrl, CMSID) {
    var categoryname = $(ImgCtrl).closest("tr").find("#hdnProductCategoryName").val();
    var ProductName = $(ImgCtrl).closest("tr").find("#lblPartNo").html();
    var ProductNumber = $(ImgCtrl).closest("tr").find("#lblProductNumber").val();
    var InternalDescription = $(ImgCtrl).closest("tr").find("#hdnInternalDescription").val();
    var description = $(ImgCtrl).closest("tr").find("#lblDescription").html();
    var Price = $(ImgCtrl).closest("tr").find("#lblPrice").html();
    var Cost = $(ImgCtrl).closest("tr").find("#hdnCost").val();
    var Size = $(ImgCtrl).closest("tr").find("#hdnSize").val();
    var Color = $(ImgCtrl).closest("tr").find("#hdnColor").val();
    if (Cost == "" || Cost == null) {
        Cost = 0;
    }
    Price = Price.substring(1);
    orderedProductsTblBody = document.getElementById("orderedProductsTblBody");
    index++;
    var Qty = "1";
    var Cls_ShopingCart =
    {
        sessionid: $("#hdnSession1").val(),
        productname: ProductName,
        productnumber: ProductNumber,
        internaldescription:InternalDescription,
        category: categoryname,
        qty: Qty,
        description: description,
        cost: Cost,
        price: Price,
        runningprice: Price,
        size: Size,
        color: Color
    };
    var objshoppingcart = JSON.stringify(Cls_ShopingCart);
    $.ajax({
        url: urlInsertShoppingTempDataUrl,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(Cls_ShopingCart),
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null && data > 0) {
            Fetch_Shopping_Cart();
        }
        else {
            alert("This item is already added into cart");
        }

    }
    function ajaxFailed() {
        alert("There is an error during operation.");
    }
}


function AddtoCart_OLD(PartNo, categoryname, description, Price) {
    orderedProductsTblBody = document.getElementById("orderedProductsTblBody");
    index++;
    var Qty = "1";
    var Cls_ShopingCart =
    {
        sessionid: $("#hdnSession1").val(),
        partno: PartNo,
        category: categoryname,
        qty: Qty,
        description: description,
        price: Price,
        runningprice: Price
    };
    var objshoppingcart = JSON.stringify(Cls_ShopingCart);
    $.ajax({
        url: urlInsertShoppingTempDataUrl,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(Cls_ShopingCart),
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null && data > 0) {
            Fetch_Shopping_Cart();
        }
        else {
            alert("This item is already added into cart");
        }
    }
    function ajaxFailed() {
        alert("There is an error during operation.");
    }
}

function ManageOrder(ctrl, Reqaction, id, Price) {
    var Suffix;
    var tax = 0;
    var SubTotalAmount = 0;
    var GrandTotal = 0;
    var CurQty = 0;
    var OrderRate = 0;
    var OrderSubTotal = 0;
    if (ctrl.indexOf("_") > -1) {
        Suffix = ctrl.split('_')[1].toString();
        CurQty = parseInt($("#txtQty_" + Suffix).val());
        if (Reqaction == "Plus") {
            CurQty++;
        }
        else {
            CurQty--;
        }
        if (CurQty > 1 || CurQty == 1) {
            Update_Quanity_By_Id(id, CurQty, Price);
        }
        else {
        }
    }
}


function Update_Quanity_By_Id(id, CurQty, Price) {
    if (CurQty > 0) {
        var RunningPrice = 0.00;
        RunningPrice = CurQty * Price;
        $.ajax({
            url: urlUpdateShoppingCart,
            contentType: 'application/json; charset=utf-8',
            data: { "id": id.toString(), "qty": CurQty.toString(), "runningprice": RunningPrice.toString() },
            dataType: "json",
            success: ajaxSucceeded,
            error: ajaxFailed
        });
    }
    else {
        alert("Quantity should be more then zero");
        $("#txtQty_" + id).val("1");
    }
    function ajaxSucceeded(data) {
        if (data != null) {
            Fetch_Shopping_Cart();
        }
        else {
        }
    }
    function ajaxFailed() {
    }
}



function DeleteCartItem(id) {
    $.ajax({
        url: urlDeleteShoppingCartById,
        contentType: 'application/json; charset=utf-8',
        data: { "id": id.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

    function ajaxSucceeded(data) {
        if (data != null) {
            $("#btnCancel").hide();
            $("#btnsubmit").hide();
            Fetch_Shopping_Cart();
        }
        else {
        }
    }
    function ajaxFailed() {
        alert("There is an error during operation.");
    }

}
function DeleteCartItems() {
    $.ajax({
        url: urlDeleteShoppingCart,
        contentType: 'application/json; charset=utf-8',
        data: { "shoppingcartid": $("#hdnSession1").val() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

    function ajaxSucceeded(data) {
        if (data != null) {
            Fetch_Shopping_Cart();
            $("#btnCancel").hide();
            $("#btnsubmit").hide();
        }
        else {
        }
    }
    function ajaxFailed() {
        alert("There is an error during operation.");
    }
}

function ViewDetail(Control, CMSID) {
    window.location.href = "../RMA/ProductDetails.aspx?CMSID=" + CMSID;
}