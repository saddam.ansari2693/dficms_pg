﻿
var urlDeleteImage = "../WebService/DocCMSApi.svc/DeleteImage";
var urlSaveCropImageBlog = "../WebService/DocCMSApi.svc/SaveCropImageForBlog";
var UrlUserInputImageSize = "../WebService/DocCMSApi.svc/UserInputImageSize";
var urlCreateCustomeImage = "../WebService/DocCMSApi.svc/Custome_Image_Create_forBlog";
var urlCropImageWithResize = "../WebService/DocCMSApi.svc/CropImageWithResize_ForBlog";
var urlDeleteVideo = "../WebService/DocCMSApi.svc/DeleteVideo";
var urlVideoList = "../WebService/DocCMSApi.svc/VideosList";
var urlTempFolderDataRemove = "../WebService/DocCMSApi.svc/RemoveTempData";

///============================////////////////
$(document).ready(function () {
    $("#chkResizeImageCreate").attr("checked", true);
    Get_Comments_By_BlogID();
    DeleteAllTempFiles();
    var jcrop_api;
    Dropzone.autoDiscover = false;
    //Simple  Image Upload by Dropzonejs 
    $("#dZUpload").dropzone({
        url: "../Handlers/BlogImageHandler.ashx",
        acceptedFiles: ".jpeg,.jpg,.png",
        async: false,
        addRemoveLinks: true,
        success: function (file, response) {
            var imgName = response;
            file.previewElement.classList.add("dz-success");
            console.log("Successfully uploaded :" + imgName);
            $(file.previewElement).find(".dz-details").on("click", function () {
                var ImageName = $(this).find(".dz-filename").text();
                ResizeImagePopup(ImageName)
                $("#dZUpload").hide();
            });
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        },
        removedfile: function (file, response) {
            DeleteSingleImage($(file.previewElement).find(".dz-filename").text());
            $(file.previewElement).remove();
        },
        queuecomplete: function (file, response) {
        }
    });



    //Simple Video Upload by Dropzonejs 
    $("#dZUploadVideo").dropzone({
        url: "../Handlers/BlogVideoHandler.ashx",
        acceptedFiles: ".mp4,.mkv,.avi,.ogg,.ogv,.wmv",
        async: false,
        addRemoveLinks: true,
        success: function (file, response) {
            var imgName = response;
            file.previewElement.classList.add("dz-success");
            console.log("Successfully uploaded :" + imgName);
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        },
        removedfile: function (file, response) {
            DeleteVideo($(file.previewElement).find(".dz-filename").text());
            $(file.previewElement).remove();
        },
        queuecomplete: function (file, response) {
            BindVideoIntoDropdown();
        }
    });


    //Delete Temp Folder data
    function DeleteAllTempFiles() {
        $.ajax({
            type: "Get",
            contentType: "application/json; charset=utf-8",
            url: urlTempFolderDataRemove,
            async: false,
            dataType: "json",
            success: function (data) {
                if (data != null && data != "") {
                }
            },
            error: function (result) {
            }
        });
    }

    //Checkbox Ration Clicked
    $("#chkAspectRatio").click(function () {//
        if ($(this).is(':checked')) {
            $("#hdnRatio").val("True");
        } else {
            $("#hdnRatio").val("False");
         } 
    });

    $("#chkFinalCrop").click(function () {//
        if ($(this).is(':checked')) {
            Set_Cropper(1);
            $("#btnCropWithResize").show();
        } else {
            Set_Cropper(0);
            $("#btnCropWithResize").hide();
        }
    });
    $("#chkAspectRatioResize").click(function () {//
        if ($(this).is(':checked')) {
            $("#hdnResizeAsepctRatio").val("True");
            setTimeout(function () { PreviewResizedImage(); }, 200);
        } else {
            $("#hdnResizeAsepctRatio").val("False");
            setTimeout(function () { PreviewResizedImage(); }, 200);
        }
    });


    $("#chkResizeImageCreate").click(function () {
        if ($(this).is(':checked')) {
            $("#chkCustomeImageCreate").prop("checked", false);
            $("#divCustomResizeImageCreate").show();
            $("#divCustomImageCreate").hide();
        } else {
            $("#chkCustomeImageCreate").prop("checked", true);
            $("#divCustomResizeImageCreate").hide();
            $("#divCustomImageCreate").show();
        }
    });

    $("#chkCustomeImageCreate").click(function () {//
        if ($(this).is(':checked')) {
            $("#chkResizeImageCreate").prop("checked", false);
            $("#divCustomImageCreate").show();
            $("#divCustomResizeImageCreate").hide();
            var MiddleImageWidth = parseInt($("#hdnActualImageWidth").val()) / 5;
            var MiddleImageHeight = parseInt($("#hdnActualImageHeight").val()) / 4;
            var MarginTopAndBottom = parseInt(200 - MiddleImageHeight) / 2;
            $("#ImgCustomeImagePreviewRunning").css("margin-top", parseInt(MarginTopAndBottom));
            $("#ImgCustomeImagePreviewRunning").css("margin-bottom", parseInt(MarginTopAndBottom));
            $("#ImgCustomeImagePreviewRunning").css("width", parseInt(MiddleImageWidth));
            $("#ImgCustomeImagePreviewRunning").css("height", parseInt(MiddleImageHeight));
            $("#ImgCustomeImagePreviewRunning").attr("src", "../TempImage/" + $("#fileName").val());
        } else {
            $("#chkResizeImageCreate").prop("checked", true);
            $("#divCustomImageCreate").hide();
            $("#divCustomResizeImageCreate").show();
        }
    });

    $("#rbtnCrop").click(function () {
        $("#hdnEditMode").val($("#rbtnCrop").val());
        $("#divCustomResize").hide();
        ChangeOrientation();
    });
    $("#rbtnResize").click(function () {
        $("#hdnEditMode").val($("#rbtnResize").val());
        $("#divCustomResize").show();
        ChangeOrientation();
    });
   
    $("#divVideoScreens").off("click").on("click", "img", function () {
        if ($(this).hasClass("activeimage")) {
            $(this).removeClass("activeimage");
        }
        else {
            $("#divVideoScreens").find("img").removeClass("activeimage");
            $(this).addClass("activeimage");
            var ImageUrl = $(this).attr("src");
            $("#DivImagePreview").show();
            $("#PreviewImage").attr("src", ImageUrl);
            $("#hdnCoverImage").val(ImageUrl);
        }
    });
});

//Ready Function Close

function Set_Cropper(setVal) {
    if (setVal == 1) {
        var reqsize = $("#ddlSizeResize").val().split('x');
        var orientation = $("#ddlOrientationResize").val();
        var cropWidth = 0;
        var cropHeight = 0;
        var CropSize = "";
        var cropHeight = 0;
        if (orientation == "Portrait") {
            cropWidth = (parseInt(reqsize[0]) * 300) / 6;
            cropHeight = (parseInt(reqsize[1]) * 300) / 6
            CropSize = reqsize[0] + ' x ' + reqsize[1];
        }
        else {
            cropWidth = (parseInt(reqsize[1]) * 300) / 6;
            cropHeight = (parseInt(reqsize[0]) * 300) / 6
            CropSize = reqsize[1] + ' x ' + reqsize[0];
        }
        $("#maxHeight").val(cropHeight.toString());
        $("#maxWidth").val(cropWidth.toString());
        var imagewidth = parseInt($("#txtCustomWidth").val());
        var imageHeight = parseInt($("#txtCustomHeight").val());
        $("#ImgResizePreview").Jcrop({
            onSelect: SelectCropArea,
            onChange: SelectCropArea,
            bgFade: true
        }, function () {
            jcrop_api = this;
            jcrop_api.setOptions({ allowSelect: true });
            jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
            jcrop_api.animateTo([10, 20, ((imagewidth - cropWidth) / 2) + cropWidth, ((imageHeight - cropHeight) / 2) + cropHeight]);
            // Use the API to get the real image size
            jcrop_api.focus();
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];
        });
    }
    else {
        jcrop_api.destroy();
        $("#ImgResizePreview").css("visibility", "visible");
    }
}

function CloseImageCropModel() {
    $("#EditImageOnModelPopup").attr("src", "");
    jcrop_api.destroy();
    modal.style.display = "none";
    $("#dZUpload").show();
    $("#divFinalPreview").hide();
    $("#divCropFinalImage").hide();
}

function CloseImageResizeModel() {
    $("#ImgResizeImage").attr("src", "");
    modalResize.style.display = "none";
    $("#dZUpload").show();
}
////////////////////////Start Image Crop Functions///////////////////////////
function DeleteSingleImage(ImageName) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: urlDeleteImage,
        async: false,
        data: "{'imagename':'" + ImageName + "'}",
        dataType: "json",
        success: function (data) {
            BindImageIntoDiv();
        },
        error: function (result) {
        }
    });
}


var modal = document.getElementById('myModalNew');
var modalResize = document.getElementById('divModelResize');
// Edit Click into Images
function EditImage(ImageName) {
    $("#ddlSize").val("None");
    $("#ddlOrientation").val("None");
    $("#EditImageOnModelPopup").attr("src", "");
    // Get the modal
    modal.style.display = "block";
    $("#btnSaveChanges").show();
    $("#drpSize").show();
    $("#drpPortrait").show();
    $("#imgPreview").show();
    $("#preview-pane").show();
    $("#EditImageOnModelPopup").attr("src", "../TempImage/" + ImageName);
    $("#imgPreview").attr("src", "../TempImage/" + ImageName);
    $("#ImgResizePreview").css("visibility", "hidden");
    $("#ImgResizePreview").css("diplay", "block");
    $("#fileName").val(ImageName);    
    setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
    return false;
}

function ResizeImagePopup(ImageName) {
    $("#ImgResizePreview").Jcrop({
        onSelect: SelectCropArea,
        onChange: SelectCropArea,
        bgFade: true

    }, function () {
        jcrop_api = this;
    });
    jcrop_api.destroy();
    $("#chkAspectRatioResize").prop("checked", false);
    $("#txtCustomWidth").val("1800");
    $("#txtCustomHeight").val("1200");
    $("#ImgResizePreview").css("width","300");
    $("#ImgResizePreview").css("height","200");
    $("#ImgResizeImage").attr("src", "../TempImage/" + ImageName);
    $("#ImgResizePreview").attr("src", "../TempImage/" + ImageName);
    $("#ImgResizePreview").attr("src", "");
    $("#chkResizeImageCreate").prop("checked", true);
    $("#chkCustomeImageCreate").prop("checked", false);
    modalResize.style.display = "block";
    $("#ImgResizePreview").css("visibility", "visible");
    $("#ImgResizePreview").css("display", "block");
    $("#divCustomResizeImageCreate").show();
    $("#divCustomImageCreate").hide();
    $("#fileName").val(ImageName);
    $("#ImgResizePreview").attr("src", "../TempImage/" + ImageName);
    setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
    return false;
}

// Crop Data Function
function CropFunctionSecond() {
    var reqsize = $("#ddlSize").val().split('x');
    var orientation = $("#ddlOrientation").val();
    var cropWidth = 0;
    var cropHeight = 0;
    var CropSize = "";
    var cropHeight = 0;
    if (orientation == "Portrait") {
        cropWidth = parseInt(reqsize[0]) * 72;
        cropHeight = parseInt(reqsize[1]) * 72;
        CropSize = reqsize[0] + ' x ' + reqsize[1];
    }
    else {
        cropWidth = parseInt(reqsize[1]) * 72;
        cropHeight = parseInt(reqsize[0]) * 72;
        CropSize = reqsize[1] + ' x ' + reqsize[0];
    }
    $("#maxHeight").val(cropHeight.toString());
    $("#maxWidth").val(cropWidth.toString());    
    var imagewidth = parseInt($("#EditImageOnModelPopup").width());
    var imageHeight = parseInt($("#EditImageOnModelPopup").height());
    $(".preview-container").css("height", cropHeight.toString() + "px");
    $(".preview-container").css("width", cropWidth.toString() + "px");
    $("#imgPreview").css("height", cropHeight.toString() + "px");
    $("#imgPreview").css("width", cropWidth.toString() + "px");
    $("#EditImageOnModelPopup").Jcrop({
        onSelect: SelectCropArea,
        onChange: SelectCropArea,
        bgFade: true
     
    }, function () {
        jcrop_api = this; 
        jcrop_api.setOptions({ allowSelect: true });
        jcrop_api.setOptions({ maxSize: [cropWidth, cropHeight] });
        jcrop_api.animateTo([((imagewidth - cropWidth) / 2), ((imageHeight - cropHeight) / 2), ((imagewidth - cropWidth) / 2) + cropWidth, ((imageHeight - cropHeight) / 2) + cropHeight]);
        // Use the API to get the real image size
        jcrop_api.focus();
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
    });
}

function CropSelImage(ctrl) {
}

function ChangeOrientation() {
    if ($("#hdnEditMode").val() == "Crop") {
        jcrop_api.destroy();
        CropFunctionSecond();
    }
    else {
        jcrop_api.destroy();
        $("#EditImageOnModelPopup").css("visibility", "visible");
    }
}


function CheckSize(ctrl) {
    if (ctrl.id.toString().indexOf("Custom") > -1) {
        $("#spnCustomSize").show();
        $("#spnSize").hide();
    }
    else {
        $("#spnCustomSize").hide();
        $("#spnSize").show();
    }
}


var boundx, boundy;
function SelectCropArea(c) {
    // Grab some information about the preview pane            
    var preview = $('#preview-pane');
    var pcnt = $('#preview-pane .preview-container');
    var pimg = $('#preview-pane .preview-container img');
    var xsize = pcnt.width(),
    ysize = pcnt.height();
    $('#XAxis').val(parseInt(c.x));
    $('#YAxis').val(parseInt(c.y));
    $('#Width').val(parseInt(c.w));
    $('#Height').val(parseInt(c.h));
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
    if (parseInt(c.w) > 0) {
        var rx = xsize / c.w;
        var ry = ysize / c.h;
        pimg.css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
    }
}
function SaveChanges() {
        ResizeImageUsingByCrop();
        modal.style.display = "none";
        jcrop_api.destroy();
}


function ResizeImageUsingByCrop() {
    $.ajax({
        type: "GET",
        url: urlSaveCropImageBlog,
        data: { 'filepath': $("#filePath").val(), 'fileName': $("#fileName").val(), 'xaxis': document.getElementById('XAxis').value, 'yaxis': document.getElementById('YAxis').value, 'width': document.getElementById('Width').value, 'height': document.getElementById('Height').value, 'blogid': $('#hndBlogId').val(), 'userid': $('#hdnCurUserID').val() },
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.filename == null) {
                alert("Please Crop Image");
            }
            else {
                $("#imgFinalPreview").attr("src", data.filepath + data.filename);
                $("#hdnImagePath").val(data.filepath);                
                $("#divFinalPreview").show();
                $("#divCropFinalImage").show();
                $("#btnPostImage").attr("disabled", false);
                $("#hdnCropImagePath").val(data.filepath);
                $("#hdnCropImageName").val(data.filename);
            }
        },
        error: function (result) {
            alert("No Match");
        }
    });
}

function ResizeImage() {
    $.ajax({
        type: "GET",
        url: UrlUserInputImageSize,
        data: { 'imagename': $("#fileName").val(), 'ratiostatus': document.getElementById('hdnResizeAsepctRatio').value, 'imageheight': document.getElementById('txtCustomHeight').value, 'imagewidth': document.getElementById('txtCustomWidth').value, 'blogid': $('#hndBlogId').val(), 'userid': $('#hdnCurUserID').val() },
        dataType: "json",
        success: function (data) {
            if (data != null && data != undefined && data != "") {
                $("#ImgResizePreview").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#hdnCropedImage").val(data);
                modalResize.style.display = "none";
                $("#imgFinalPreview").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#EditImageOnModelPopup").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#imgGetHeightWidth").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#imgPreview").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#divFinalPreview").show();
                $("#divCropFinalImage").show();
                $("#btnPostImage").attr("disabled", false);
                $("#dZUpload").hide();
                $("#fileName").val(data.filename);
                $("#filePath").val(data.filepath);
            }
            else {                
                //Start Save Image Preview//
                $("#divFinalPreview").hide();
                $("#divCropFinalImage").hide();
                $("#imgGetHeightWidth").attr("src", "");
                $("#dZUpload").show();

            }
        },
        error: function (result) {
            alert("No Match");
        }
    });
}
////////////////////////End Image Crop Functions////////////////////////////

function TEstingTEsting() {
    var modal = document.getElementById('ImageModel');
    modal.style.display = "block";
}   
/*------------ To Insert Comment ------------------*/
function SubmitComment() {
    $.ajax({
        url: "../WebService/DocCMSApi.svc/Insert_Comment_Details",
        contentType: "application/json; charset=utf-8",
        data: { "blogid": $("#hndBlogId").val(), "comment": $("#txtComment").val(), "accesstype": $("#ddlPostingAccess").val() },
        dataType: "json",
        async: false,
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    //Success
    function ajaxSucceeded(data) {
        var htmlUser = "";
        if (data != null && data != "") {
            if (data == "Success") {
                $("#ErrorMessage").hide();
                $("#DivComment").hide();
                Get_Comments_By_BlogID();
                $("#txtComment").val("");
            }
            else if (data == "Error") {
                window.location.href = "../dfl/UserLogin.html";
            }
            else {
                window.location.href = "../dfl/UserLogin.html?BlogId=" + data;
            }
        }
    }
    //Error
    function ajaxFailed() {
        alert("There is an error occurred during operation");
    }
}


/*------------ To Insert Comment ------------------*/
function Get_Comments_By_BlogID() {
    $.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Comment_Based_On_BlogId",
        contentType: "application/json; charset=utf-8",
        data: { "blogid": $("#hndBlogId").val() },
        dataType: "json",
        async: false,
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    //Success
    function ajaxSucceeded(data) {
        var htmlUser = "";
        if (data != null && data != "") {
            var result = jQuery.parseJSON(data);
            $("#TotelComment").html(result.length + " Comments");
            if (result != null && result.length > 0) {
                var html = "";
                for (var i = 0; i < result.length; i++) {
                    html += "<div class='media'>";
                    html += "<div class='pull-left'>";
                    html += "<img class='avatar img-circle' src='../images/UserImage.jpg' alt='' style='width:50px;height:50px;'>";
                    html += "</div>";
                    html += "<div class='media-body'>";
                    html += "<div class='well'>";
                    html += "<div class='media-heading'>";
                    html += "<strong>" + result[i].username + "</strong>&nbsp; <small>" + result[i].posteddate + "</small>";
                    html += "<a class='pull-right' href='#' style='display:none;'><i class='icon-repeat'></i> Reply</a>"; /// <reference path="" />
                    html += "</div>";
                    if (result[i].comment != "") {
                        html += "<p>" + result[i].comment + "</p>";
                    }
                    else if (result[i].imageurl != "") {
                        html += "<img src='../images/Download.png' onclick=ImageDownload('" + result[i].imagethumbnailurl + result[i].imageurl + "','" + result[i].memberid + "') style='float:right;margin-top:-30px;margin-bottom:5px;width: 42px;'/>";
                        html += "<p><img src='" + result[i].imagethumbnailurl + result[i].imageurl + "' style='width:88%; height:100%;cursor:pointer;' title='Click to Full View' onclick=CommentImageShowOnPopup('" + result[i].imagethumbnailurl + result[i].imageurl + "','" + result[i].memberid + "') /></p>";
                    }
                    else if (result[i].videourl != "") {
                        html += "<img src='../images/Download.png' onclick=VideoDownload('" + result[i].videourl + "') style='float:right;margin-top:-30px;margin-bottom:5px;width: 42px;'/>";
                        html += "<video poster='" + result[i].videocoverimageurl + "' id='BlogVideoPlayer' src='" + result[i].videourl + "' controls></video>";
                    }
                    html += "</div>";
                    html += "</div>";
                    html += "</div>";
                }
                $("#BindComment").html(html);
            }
        }
    }
    //Error
    function ajaxFailed() {
        alert("There is an error occurred during operation");
    }
}

function ShowResponseSection() {
    $("#DivOptions").fadeIn(3000);
}
function model(value) {
    alert(value);
}

function VideoDownload(VideoUrl) {
    $("#hndVideoFileNameForDownload").val(VideoUrl);
    $("#btnDownloadVideo").click();
}


//Comment Image Zoom Model Using on Commnet Image
function CommentImageShowOnPopup(ImageName, MemberId) {    
    var modal = document.getElementById('ImageModel');
    modal.style.display = "block";
    if (ImageName.indexOf("ResizeImage") > -1) {
        ImageName = ImageName.replace("/Thumbnail/", "/ResizedImage/");
    } else if (ImageName.indexOf("CustomImage") > -1) {
        ImageName = ImageName.replace("/Thumbnail/", "/Customed/");
    } else if (ImageName.indexOf("Crop") > -1) {
        ImageName = ImageName.replace("/Thumbnail/", "/Croped/");
    }
    $("#imgZoomImage").attr("src", ImageName);
    $("#hdnOrigImage").val(ImageName);
}
//Image Download
function ImageDownload(ImageName, MemberId) {
if (ImageName.indexOf("ResizeImage") > -1) {
        ImageName = ImageName.replace("/Thumbnail/", "/ResizedImage/");
    } else if (ImageName.indexOf("CustomImage") > -1) {
        ImageName = ImageName.replace("/Thumbnail/", "/Customed/");
    } else if (ImageName.indexOf("Crop") > -1) {
        ImageName = ImageName.replace("/Thumbnail/", "/Croped/");
    }
    $("#hdnOrigImage").val(ImageName);
    $("#imgDownloadImage").click();
}

function CloseZoomModel() {
    var modal = document.getElementById('ImageModel');
    modal.style.display = "none";
}

function ShowCommentSection() {
    $("#DivComment").show();
    $("#DivImage").hide();
    $("#DivVideo").hide();
}

function ShowImageSection() {
    $.ajax({
        url: "../WebService/DocCMSApi.svc/MemberIdCheck",
        contentType: "application/json; charset=utf-8",
        data: { "memberid": $("#hdnCurUserID").val(), "blogid": $("#hndBlogId").val() },
        dataType: "json",
        async: false,
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    //Success
    function ajaxSucceeded(data) {
        var htmlUser = "";
        if (data != null && data != "") {
            if (data.retval == "Success") {
                $("#DivImage").show();
                $("#dZUpload").show();
                $("#DivComment").hide();
                $("#DivVideo").hide();
                $("#divFinalPreview").hide();
                $("#divCropFinalImage").hide();
            }
            else {
                window.location.href = "../dfl/UserLogin.html?BlogId=" + data.blogId;              
            }
        }
    }
    //Error
    function ajaxFailed() {
        alert("There is an error occurred during operation");
    }
}



function ImageFileUploadCall() {
    $("#FleUploadImage").click();
}
function VideoFileUploadCall() {
    $("#FleUploadVideo").click();
}
function ShowPreviewImage(input) {
    if (input.files && input.files[0]) {
        var ImageDir = new FileReader();
        ImageDir.onload = function (e) {
            $('#ImageUpload').attr('src', e.target.result);
            var Image = input.files[0].name;
            $("#ImgName").html(Image);
            $("#ImageUpload").show();
        }
        ImageDir.readAsDataURL(input.files[0]);
    }
}

function ShowPreviewVideo(input) {
    if (input.files && input.files[0]) {
        var ImageDir = new FileReader();
        ImageDir.onload = function (e) {
            var Video = input.files[0].name;
            $("#VideoName").html(Video);
            $("#ImgVideo").show();
        }
        ImageDir.readAsDataURL(input.files[0]);
    }
}



function UploadImage() {
    $.ajax({
        url: "../WebService/DocCMSApi.svc/SaveBlogImage",
        contentType: "application/json; charset=utf-8",
        data: {  "blogid": $("#hndBlogId").val(), "memberid": $("#hdnCurUserID").val(), "imageurl": $("#hdnImagePath").val(), "accesstype": $("#ddlPostingAccess").val() },
        dataType: "json",
        async: false,
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    //Success
    function ajaxSucceeded(data) {
        var htmlUser = "";
        if (data != null && data != "") {
            if (data == "Success") {
                $("#ErrorMessage").hide();
                Get_Comments_By_BlogID();
                $("#txtComment").val("");
                $("#DivVideo").hide();
                $("#DivImage").hide();
                $("#DivComment").hide();
                $("#dZUpload").show();
                $("#divFinalPreview").hide();
                $('div.dz-success').remove();
            }
            else {
                window.location.href = "../dfl/UserLogin.html?BlogId=" + data;
            }
        }
    }
    //Error
    function ajaxFailed() {
        alert("There is an error occurred during operation");
    }
}

function UploadImage_New() {
    $.ajax({
        url: "../WebService/DocCMSApi.svc/SaveBlogImage_New",
        contentType: "application/json; charset=utf-8",
        data: { "resizeimagepath": $("#filePath").val(), "resizeimagename": $("#fileName").val(), "blogid": $("#hndBlogId").val(), "memberid": $("#hdnCurUserID").val(), "imageurl": $("#hdnImagePath").val(), "accesstype": $("#ddlPostingAccess").val() },
        dataType: "json",
        async: false,
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    //Success
    function ajaxSucceeded(data) {
        var htmlUser = "";
        if (data != null && data != "") {
            if (data == "Success") {
                $("#ErrorMessage").hide();
                Get_Comments_By_BlogID();
                $("#txtComment").val("");
                $("#DivVideo").hide();
                $("#DivImage").hide();
                $("#DivComment").hide();
                $("#dZUpload").show();
                $("#divFinalPreview").hide();
                $('div.dz-success').remove();
                location.reload();
            }
            else {
                window.location.href = "../dfl/UserLogin.html?BlogId=" + data;
            }
        }
    }
    //Error
    function ajaxFailed() {
        alert("There is an error occurred during operation");
    }
}


function Get_Actual_Uploaded_Image_Size() {
    var myImg = document.querySelector("#ImgResizeImage");
    var realWidth = myImg.naturalWidth;
    var realHeight = myImg.naturalHeight;
    $("#hdnActualImageWidth").val(realWidth);
    $("#hdnActualImageHeight").val(realHeight);        
}

function SetReSizeValues(paramval) {
    jcrop_api.destroy();
    $("#ImgResizePreview").css("visibility", "visible");
    var AutoSize = "";
    var reqsize = "";
    var orientation = "";
    var ResizeWidth = 0;
    var ResizeHeight = 0;
    if (paramval == "S") {
        AutoSize = $("#ddlSizeResize").val();
        reqsize = $("#ddlSizeResize").val().split('x');
        orientation = $("#ddlOrientationResize").val();
        if (orientation == "Portrait") {
            ResizeWidth = parseInt(reqsize[0]) * 300;
            ResizeHeight = parseInt(reqsize[1]) * 300;
        }
        else {
            ResizeWidth = parseInt(reqsize[1]) * 300;
            ResizeHeight = parseInt(reqsize[0]) * 300;
        }
    }
    else {
        if (orientation == "Portrait") {
            ResizeWidth = $("#txtCustomWidth").val();
            ResizeHeight = $("#txtCustomHeight").val();
        }
        else {
            ResizeWidth = $("#txtCustomHeight").val();
            ResizeHeight = $("#txtCustomWidth").val();
        }
    }
    if (orientation == "Portrait") {
        var previewHeight = $("#divResizePreview").css("height");
        var previewwidth = $("#divResizePreview").css("width");
        $("#divResizePreview").css("height", previewwidth);
        $("#divResizePreview").css("width", previewHeight);
        $("#txtCustomHeight").val(ResizeHeight);
        $("#txtCustomWidth").val(ResizeWidth);
    }
    else {
        var previewHeight = $("#divResizePreview").css("height");
        var previewwidth = $("#divResizePreview").css("width");
        $("#divResizePreview").css("height", previewwidth);
        $("#divResizePreview").css("width", previewHeight);
        $("#txtCustomHeight").val(ResizeHeight);
        $("#txtCustomWidth").val(ResizeWidth);
    }
    setTimeout(function () { PreviewResizedImage(); }, 200);
    setTimeout(function () {
        if ($("#chkFinalCrop").prop("checked") == true) {
            Set_Cropper(1);
        }
    }, 400);
}

function CheckResizeValues() {
    setTimeout(function () { PreviewResizedImage(); }, 200);
}

function PreviewResizedImage() {
    var actWidth = $("#hdnActualImageWidth").val();
    var actHeight = $("#hdnActualImageHeight").val();
    var resizeWidth = $("#txtCustomWidth").val();
    var resizeHeight = $("#txtCustomHeight").val();
      if ($("#hdnResizeAsepctRatio").val() == "True") {
      var ratioX = parseInt(resizeWidth/6) / parseInt(actWidth/6);
      var ratioY = parseInt(resizeHeight/6) / parseInt(actHeight/6);
      var ratio = Math.max(ratioX, ratioY);
      var newWidth = parseInt(parseInt(resizeWidth/6) * ratio);
      var newHeight = parseInt(parseInt(resizeHeight/6) * ratio);
        newWidth = parseInt(resizeWidth / 6);
        newHeight = parseInt(resizeHeight / 6);
        $("#ImgResizePreview").attr("src", $("#ImgResizeImage").attr("src"));
        $("#ImgResizePreview").css("width", newWidth.toString() + "px");
        $("#ImgResizePreview").css("height", newHeight.toString() + "px");
        $("#ImgResizePreview").css("max-width", "none");
    }
    else {
        var newWidth = parseInt(resizeWidth / 6);
        var newHeight = parseInt(resizeHeight / 6);
        $("#ImgResizePreview").attr("src", $("#ImgResizeImage").attr("src"));
        $("#ImgResizePreview").css("width", newWidth.toString() + "px");
        $("#ImgResizePreview").css("height", newHeight.toString() + "px");
        $("#ImgResizePreview").css("max-width", "none");
    }
    CheckImageFits(newHeight, newWidth);
}

function CheckImageFits(newHeight, newWidth) {
    var RequiredWidth = parseInt($("#divResizePreview").css("width"));
    var Requiredheight = parseInt($("#divResizePreview").css("height"));
    if ((RequiredWidth < newWidth) || (Requiredheight < newHeight)) {
        $("#lblUnfitMsg").show();
        $("#spnFinalCrop").show();
        $("#btnCropWithResize").show();
        $("#btnResizeImage").hide();
    }
    else {
        $("#lblUnfitMsg").hide();
        $("#spnFinalCrop").hide();
        $("#btnResizeImage").show();
        $("#btnCropWithResize").hide();
    }
    return false;
}

function ManageAspectRatio(ratioparam, e) {
    var key;
    if (window.event) {
        key = window.event.keyCode;     //IE
    }
    else {
        key = e.which;      //firefox              
    }
    if ((key >= 48 && key <= 57) || (key >= 95 && key <= 106) || (key == 0 || key == 8 || key == 13 || key == 27 || key == 32 || key == 127)) {
        if ($("#chkAspectRatioResize").is(':checked')) {
            if (ratioparam == "w") {
                var width = $("#txtCustomWidth").val();
                if (parseInt($("#txtCustomWidth").val()) > 3200) {
                    $("#txtCustomWidth").css("color", "Red");
                    $("#txtCustomHeight").val("0");
                }
                else {
                    var height = parseFloat($("#txtCustomWidth").val()) / 1.333;
                    $("#txtCustomHeight").val(parseInt(height));
                }
            }
            else {
                var height = $("#txtCustomHeight").val();
                if (parseInt($("#txtCustomWidth").val()) > 3200) {
                    $("#txtCustomHeight").css("color", "Red");
                    $("#txtCustomWidth").val("0");
                }
                else {
                    var width = parseFloat($("#txtCustomHeight").val()) * 1.333;                    
                    $("#txtCustomWidth").val(parseInt(width));
                }
            }
        }
        else {
        }
    }
    else {
        return false;
    }
    setTimeout(function () { PreviewResizedImage(); }, 200);
}

// Custome Image Create
function CustomeImage() {
    var ImageNameArray = $("#ImgResizeImage").attr("src").split('/');
    var ImageName=ImageNameArray[ImageNameArray.length - 1];
    var Color = "#" + $('#txtColor').val();
    var hex = parseInt(Color.substring(1), 16);
    var r = (hex & 0xff0000) >> 16;
    var g = (hex & 0x00ff00) >> 8;
    var b = hex & 0x0000ff;
    $.ajax({
        type: "GET",
        url: urlCreateCustomeImage,
        data: { 'imagename': ImageName, 'R': r, 'G': g, 'B': b, 'blogid': $('#hndBlogId').val(), 'userid': $('#hdnCurUserID').val() },
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.filename != null && data.filename != undefined) {
                $("#ImgCustomeImagePreview").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#ImgCustomeImagePreview").hide();
                $("#imgFinalPreview").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#EditImageOnModelPopup").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#imgGetHeightWidth").attr("src", data.filepath.replace("~", "..") + data.filename);
                $("#divFinalPreview").show();
                $("#dZUpload").hide();
                $("#divCropFinalImage").show();
                modalResize.style.display = "none";
                $("#fileName").val(data.filename);
                $("#filePath").val(data.filepath);
                $("#btnPostImage").prop("disabled", false);
            }
            else {
                $("#ImgCustomeImagePreview").attr("src", "");
                $("#ImgCustomeImagePreview").hide();
                $("#imgFinalPreview").attr("src", "");
                $("#EditImageOnModelPopup").attr("src", "");
                $("#divFinalPreview").hide();
                $("#divCropFinalImage").hide();
                $("#dZUpload").show();
                modalResize.style.display = "block";
                $("#imgGetHeightWidth").attr("src", "");
                $("#btnPostImage").prop("disabled", true);
            }
        },
        error: function (result) {
            alert("No Match");
        }
    });
 }


    // function change event of color picker
    function ColorChange(Textbox) {
        var colorName = $("#" + Textbox.id).val();        
        $("#ImgCustomeImagePreviewRunning").attr("src", "../TempImage/" + $("#fileName").val());
        $("#divCustomImagePreviewRunning").css("background-color", "#" + colorName);
    }


     //Crop Image After Resize Image
    function CropImagePreview() {
        $("#EditImageOnModelPopup").Jcrop({
            onSelect: SelectCropArea,
            onChange: SelectCropArea,
            bgFade: true

        }, function () {
            jcrop_api = this;
        });
        $("#ddlSize").val("None");
        $("#ddlOrientation").val("None");        
        modal.style.display = "block";        
        $("#divResizeInner").css("max-width", $("#imgGetHeightWidth").width() + "px");        
        $("#btnSaveChanges").show();
        $("#drpSize").show();
        $("#drpPortrait").show();
        $("#imgPreview").show();
        $("#preview-pane").show();
        setTimeout(function () { Get_Actual_Uploaded_Image_Size(); }, 500);
    }


    //Crop Image With Resize Image
    function CropImageWithResize() {
        $.ajax({
            type: "Get",
            url: urlCropImageWithResize,
            data: { 'imagename': $("#fileName").val(), 'imageheight': $("#txtCustomHeight").val(), 'imagewidth': $("#txtCustomWidth").val(), 'xaxis': document.getElementById('XAxis').value, 'yaxis': document.getElementById('YAxis').value, 'cropimagewidth': document.getElementById('Width').value, 'cropimageheight': document.getElementById('Height').value, 'blogid': $('#hndBlogId').val(), 'userid': $('#hdnCurUserID').val() },
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.filename == null) {
                    alert("Please Crop Image");
                }
                else {
                    $("#imgFinalPreview").attr("src", data.filepath.replace("~", "..") + data.filename);
                    $("#hdnImagePath").val(data.filepath);
                    modal.style.display = "none";
                    $("#divModelResize").hide();                   
                    $("#btnPostImage").prop("disabled", false);
                    $("#filePath").val(data.filepath);
                    $("#fileName").val(data.filename)
                    $("#divFinalPreview").show();
                    $("#divCropFinalImage").show();
                    $("#hdnCropImagePath").val(data.filepath);
                    $("#hdnCropImageName").val(data.filename);
                }
            },
            error: function (result) {
                alert("No Match");
            }
        });
    }


    //======================================================================Video Section=============================================================================//

    // Video Convert
    function validateVideoFile(file) {
        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["mp4","mkv","avi","ogg","ogv","wmv"];
        if (arrayExtensions.lastIndexOf(ext) == -1) {
            alert("Wrong extension type.");
            $("#btnConvertVideoJquery").hide();
        }
        else {
            $("#btnConvertVideoJquery").show();
        }

        var filename = file.split('\\').pop();
        if (filename != null && filename != "") {
            $("#spnVideoName").html(filename);
        }
        else {
            $("#spnVideoName").html(filename);
        }
    }
    function ProcessVideo() {
        UploadVideo();
    }
    function UploadVideo() {
        $("#ImgProgressBar").show();
        var fileUpload = jQuery("#fileVideoComment").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        jQuery.ajax({
            url: "../Handlers/BlogVideoHandler.ashx",
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            success: function (result) {
                if (result != null && result != "") {
                    $("#hndVideoFileName").val(result);
                    ConvertVideo(result);
                }
            },
            error: function (err) {
            }
        });
    }
    function ConvertVideo(VideoName) {
        $.ajax({
            type: "Get",
            url: "../WebService/DocCMSApi.svc/Convert_Video",
            data: { 'videoname': VideoName, 'formattype': $("#ddlOutputFormats").val(), 'blogid': $('#hndBlogId').val(), 'userid': $('#hdnCurUserID').val() },
            dataType: "json",
            success: function (data) {
                if (data != null && data != "") {
                    if (data.returnvalue == "Success") {
                        $("#ImgProgressBar").hide();
                        $("#divImageExtractSection").show();
                        $("#hdnVideoCoverImagePath").val(data.coverimagepath);
                        $("#hdnVideoVideoPath").val(data.videopath);
                        $("#hndVideoFileName").val(data.videoname);
                        $("#divVideoScreens").html("");
                        var newID = 1;
                        var name = data.videoname + ".mp4";
                        var video = document.getElementById('video1');
                        video.src = data.videopath;
                        video.play();
                        takeScreenshot(newID, name);
                    }
                }
            },
            error: function (result) {
                alert(result);
            }
        });
    }

    function GetScreenShotofVideo() {
        $("#VideoModel").hide();
        $("#divFinalPreview").hide();
        $("#DivVideo").show();
        $("#dZUpload").hide();
        $("#FinalVideoPlayer").show();
        $("#divImageExtractSection").hide();
        $("#FinalVideoPlayer").attr("poster", $("#hdnVideoCoverImagePath").val() + $("#hndVideoFileName").val() + ".jpg");
        $("#FinalVideoPlayer").attr("src", $("#hdnVideoVideoPath").val() + $("#hndVideoFileName").val() + ".mp4");
    }

    var modalVideo = "";
    function ShowVideoSection() {
        $.ajax({
            url: "../WebService/DocCMSApi.svc/MemberIdCheck",
            contentType: "application/json; charset=utf-8",
            data: { "memberid": $("#hdnCurUserID").val(), "blogid": $("#hndBlogId").val() },
            dataType: "json",
            async: false,
            success: ajaxSucceeded,
            error: ajaxFailed
        });
        //Success
        function ajaxSucceeded(data) {
            var htmlUser = "";
            if (data != null && data != "") {
                if (data.retval == "Success") {
                    $("#DivImage").hide();
                    $("#DivComment").hide();
                    modalVideo = document.getElementById('VideoModel');
                    modalVideo.style.display = "block";
                    BindVideoIntoDropdown();
                }
                else {
                    window.location.href = "../dfi/UserLogin.html?BlogId=" + data.blogId;
                }
            }
        }
        //Error
        function ajaxFailed() {
            alert("There is an error occurred during operation");
        }
    }
    function CloseShowVideoSection() {
        modalVideo = document.getElementById('VideoModel');
        modalVideo.style.display = "none";
    }

    function DeleteVideo(VideoName) {
        $.ajax({
            type: "Get",
            contentType: "application/json; charset=utf-8",
            url: urlDeleteVideo,
            async: false,
            data: { videoname: VideoName, memberid: $("#hdnCurUserID").val() },
            dataType: "json",
            success: function (data) {
                if (data != null && data != "") {
                    BindVideoIntoDropdown();
                }
            },
            error: function (result) {
                BindVideoIntoDropdown();
            }
        });
    }
       
    function BindVideoIntoDropdown() {
        $.ajax({
            url: urlVideoList,
            contentType: 'application/json; charset=utf-8',
            data: { memberid: $("#hdnCurUserID").val() },
            async: false,
            dataType: "json",
            success: function (data) {
                if (data != null && data != "") {
                    var result = jQuery.parseJSON(data);
                    if (result.length > 0) {
                        $("#ddlVideos").empty();
                        for (var i = 0; i < result.length; i++) {
                            $("#ddlVideos").append($("<option></option>").val(result[i].videoname).html(result[i].videoname));
                        }
                    }
                }
            },
            error: function (result) {
            }
        });
    }

 


    //on video success take the screenshot and show at the box
    var values = 1;
    var videoHeight = 480, videoWidth = 640;
    var drawTimer = [];
    VideoDelay = 3;
    var value = 0;
    function takeScreenshot(newID, name) {
         screenshotStart(newID, name);
    }
    function screenshotStart(newID, name) {
        var video = document.getElementById('video1');
        video.muted = true;
        var SnapScreen = "";
        video.addEventListener("playing", function () { startScreenshot(newID, name); });
        video.addEventListener("pause", function () { stopScreenshot(newID, name); });
        video.addEventListener("ended", function () { stopScreenshot(newID, name); });
    }
    function stopScreenshot(newID, name) {
        if (drawTimer[newID]) {
            console.log("stoped");
            clearInterval(drawTimer[newID]);
            drawTimer[newID] = null;
        }
    }

    function startScreenshot(newID, name) {
        var video = document.getElementById('video1');
        console.log("started");
        drawTimer[newID] = setInterval(function () {
            value += 1;
            if (value > 20) {
                video.pause();
                return false;
            }
            grabScreenshot(newID, name);
        }, VideoDelay);
    }

    function grabScreenshot(newID, name) {
        var VideoContainer = document.getElementById("divVideoScreens");
        var video = document.getElementById('video1');
        var canvas = document.getElementById("canvas1");
        var ctx = canvas.getContext("2d");
        ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
        var img = new Image();
        img.src = canvas.toDataURL("image/png");
        img.width = 110;
        $("#divVideoScreens").append(img);
    }


  