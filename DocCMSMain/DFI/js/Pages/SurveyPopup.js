﻿var SurveyID="";
var TotalQuestions;
var CurActiveQuestion=1;
var SurveyQuesionID="";
var SurveyQuesionList="";
var CheckExist="";
var SIPAddress="";
var Show_Survey_Screen_Results_Status = "";
function Bind_Survey(){
SetSIPAddress();
$.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Default_Survey",
        contentType: 'application/json; charset=utf-8',
        data: { "ipaddress": IPAddress.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            SurveyID=data.toString();
            // Check Status weather to show result or not by survey id
            Check_Survey_Screen_Results_Status(SurveyID);
            Check_Exist_IPAddress();
        }
    }
    function ajaxFailed() {
    }
}
function SetSIPAddress()
{
 function getUserIP(onNewIP) { 
       //  onNewIp - your listener function for new IPs
       //compatibility for firefox and chrome
        var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        var pc = new myPeerConnection({
            iceServers: []
        }),
    noop = function () { },
    localIPs = {},
    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
    key;

   function iterateIP(ip) {
         if (!localIPs[ip]) onNewIP(ip);
         localIPs[ip] = true;
   }

        //create a bogus data channel
        pc.createDataChannel("");
        // create offer and set local description
        pc.createOffer(function (sdp) {
            sdp.sdp.split('\n').forEach(function (line) {
                if (line.indexOf('candidate') < 0) return;
                line.match(ipRegex).forEach(iterateIP);
            });
           pc.setLocalDescription(sdp, noop, noop);
        }, noop);

        //listen for candidate events
        pc.onicecandidate = function (ice) {
            if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
            ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
        };
    }

    // Usage
    getUserIP(function (ip) {
        SIPAddress=ip.toString();
    });
  
}

function Check_Exist_IP_ByID(){
SetSIPAddress();
 $.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Default_Survey",
        contentType: 'application/json; charset=utf-8',
       data: { "ipaddress": IPAddress.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            SurveyID=data.toString();
            Check_Exist_IPAddress_popup();
         }
    }
    function ajaxFailed() {
    }
}


function Check_Exist_IPAddress_popup(){
SetSIPAddress();
 $.ajax({
        url: "../WebService/DocCMSApi.svc/Check_Exist_IPAddress",
        contentType: 'application/json; charset=utf-8',
       data: { "ipaddress": IPAddress.toString(), "surveyid":SurveyID.toString(),"sipaddress":SIPAddress.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            CheckExist=data.toString();
            if(CheckExist!="True")
            { 
                $("#divSurveyNotification").show();
            }
            else
            {
                $("#DivMainPopup").hide();
                $("#divSurveyNotification").hide();
            }
        }
    }
    function ajaxFailed() {
    }
}

function Check_Exist_IPAddress(){
SetSIPAddress();
 $.ajax({
        url: "../WebService/DocCMSApi.svc/Check_Exist_IPAddress",
        contentType: 'application/json; charset=utf-8',
        data: { "ipaddress": IPAddress.toString(), "surveyid":SurveyID.toString(),"sipaddress":SIPAddress.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            CheckExist=data.toString();
            if(CheckExist!="True")
            {             
               Bind_Survey_Question();
               $("#divSurveyNotification").show();
            }
            else
            {
               $("#DivMainPopup").hide();
             }
         }
    }
    function ajaxFailed() {
    }
}
function Bind_Survey_Question(){
var SurveyQuestionID="";
var Question="";
var AnswerType="";
var DisplayOrder="";
var IsActive="";
 $.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Survey_Question",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyid": SurveyID.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(data) {
        if (data != null) {
            var result = jQuery.parseJSON(data);
            var List="";
            if(result.length>0)
            {
            TotalQuestions=result.length;
            for(var i=0;i<result.length;i++)
            {
            SurveyQuesionList=SurveyQuesionList+","+result[i].surveyquestionid.toString()+"~"+result[i].answertype.toString();
            if(i==0)
            {
                List=List+"<div id='divQuestion_"+result[i].surveyquestionid.toString()+"' >";
                List=List+"<input type='hidden' id='hdnNextQuest_"+result[i].surveyquestionid.toString()+"' value='"+result[i+1].surveyquestionid.toString()+"'/>";
                List=List+"<p class='popupques' >";
                List=List+result[i].question.toString();
                List=List+"</p>";
                List=List+"<div id='divAns_"+result[i].surveyquestionid.toString()+"' class='input-btn'>";
                List=List+"</div>";
                List=List+"</div>";
            }
            else
            {
            List=List+"<div id='divQuestion_"+result[i].surveyquestionid.toString()+"' style='display:none;' >";
            if(i<result.length-1)
            {
                List=List+"<input type='hidden' id='hdnNextQuest_"+result[i].surveyquestionid.toString()+"' value='"+result[i+1].surveyquestionid.toString()+"'/>";
            }
            else
            {
                List=List+"<input type='hidden' id='hdnNextQuest_"+result[i].surveyquestionid.toString()+"' value='0'/>";
            }
                List=List+"<p class='popupques' >";
                List=List+result[i].question.toString();
                List=List+"</p>";
                List=List+"<div id='divAns_"+result[i].surveyquestionid.toString()+"' >";
                List=List+"</div>";
                List=List+"</div>";
            }
            }
            $("#DivSurvey").html(List.toString());
            setTimeout(function(){$("#DivMainPopup").show(500)}, 500);
            }
            else{
            $("#DivSurvey").html("<p>No survey data found..</p>");
            }

     Bind_Survey_Answers(SurveyQuesionList);
    }
 }
    function ajaxFailed() {
    }
}

function Bind_Survey_Answers(SurveyQuesionList)
{
if(SurveyQuesionList.startsWith(",")==true)
{
   SurveyQuesionList=SurveyQuesionList.toString().substring(1);
}
if(SurveyQuesionList.endsWith(",")==true)
{
   SurveyQuesionList=SurveyQuesionList.toString().substring(0,SurveyQuesionList.length-1);
}
if(SurveyQuesionList!="")
{
    var QuestArray=SurveyQuesionList.split(',');
    for(var j=0;j<QuestArray.length;j++)
    {
       Bind_Answer_ByQuestionID(QuestArray[j].toString());
    }
 }
}

function Bind_Answer_ByQuestionID(SurveyQuesionID){
var VotingDetails="";
var QuestIDArray=SurveyQuesionID.toString().split('~');
var List="";
        switch(QuestIDArray[1].toString())
           {
           case "Text":
           List=List+"<div class='input-btn'>";
           List=List+"<textarea rows='4' id='message_" + QuestIDArray[0] + "'  name='textbox' ></textarea></div>";
           $("#divAns_"+QuestIDArray[0].toString()).html(List.toString());
           VotingDetails= SurveyID.toString()+"_"+QuestIDArray[0].toString()+"_"+QuestIDArray[1].toString();
           $("#divAns_"+QuestIDArray[0].toString()).append("<p><button type='button' runat='server' placeholder='Enter your Description' class='btn btn-success' value='Vote' onclick=Voting('"+VotingDetails+"'); > Vote <i class='fa fa-thumbs-up'></i></button></p>");
           break;
           default:
           break;
           }
               
    $.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Survey_Answer",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyquesionid": QuestIDArray[0].toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
    function ajaxSucceeded(datavalue) {
        if (datavalue != null) {
            var result = jQuery.parseJSON(datavalue);
            if(result.length>0)
            {
            var List="";
            for(var i=0;i<result.length;i++)
            {
           switch(QuestIDArray[1].toString())
           {
           case "Option":
           List=List+"<div class='input-btn'>";
           List=List+"<input type='radio' id='radio_" + result[i].surveyanswerid + "' name='radio' />";
          List=List+"<label id='txtradio_" + result[i].surveyanswerid + "' for='radio_" + result[i].surveyanswerid + "'><span></span>" + result[i].answer + "</label></div>";
          break;

          case "Check":           
          List=List+"<div class='input-btn'>";
          List=List+"<input type='checkbox' id='checkbox_" + result[i].surveyanswerid + "' value='" + result[i].surveyanswerid + "' name='checkbox' />";
          List=List+"<label id='txtcheck_" + result[i].surveyanswerid + "' for='checkbox_" + result[i].surveyanswerid + "'><span></span>" + result[i].answer + "</label></div>";
           break;
           default:
           List=List+"<div class='input-btn'>";
           List=List+"<textarea rows='4' id='message_" + QuestIDArray[0] + "'  name='textbox' ></textarea></div>";
           break;
           }
            }
           
           $("#divAns_"+QuestIDArray[0].toString()).html(List.toString());
           VotingDetails= VotingDetails+SurveyID.toString()+"_"+QuestIDArray[0].toString()+"_"+QuestIDArray[1].toString();
           $("#divAns_"+QuestIDArray[0].toString()).append("<p>  <button type='button' runat='server' placeholder='Enter your Description' class='btn btn-default' value='Vote' onclick=Voting('"+VotingDetails+"'); > Vote <i class='fa fa-thumbs-up'></i></button></p>");
            }
            else{
            }
        }
    }
    function ajaxFailed() {
   }
}

function Voting(VotingDetails) {
var VotingArray=VotingDetails.toString().split('_');
SurveyID=VotingArray[0].toString();
SurveyQuestionID=VotingArray[1].toString();
AnswerType=VotingArray[2].toString();
        var selectedVal = "";
        var checkedvalue = "";
        var arrAns = "";
        var Ans="";
        var ResponseText="";
        var ResponseType=""
        var  AdditionalText="";
        var checkboxValues = [];
        var Ques = SurveyQuestionID;
        // for Radio 
        if (AnswerType == "Option") {
            var selected = $('input[name=radio]:checked');
            if (selected.length > 0) {
                selectedVal = selected.attr("id");
                arrAns = selectedVal.toString().split('_');
                   Ans=arrAns[1].toString()+",";
                    ResponseText = $('#txtradio_'+arrAns[1].toString()+'').text();
                  ResponseType="Option";
            }
        }

        //---------------------- for checkbox--------//
        else if (AnswerType == "Check") {
              $('input[name=checkbox]:checked').map(function () {
                checkboxValues.push($(this).val());
                 });
                    for(var i=0;i<checkboxValues.length;i++)
                    {
                      Ans=Ans+checkboxValues[i].toString()+",";
                      ResponseText =ResponseText+ $('#txtcheck_'+checkboxValues[i].toString()+'').text()+"~";
                      ResponseType="Check";
                    }
             }

        //----------For Textbox-------------------//
        else {
        ResponseText = $('textarea#message_'+SurveyQuestionID).val();
        AdditionalText="";
        Ans=",";
        ResponseType="Text";
        }

        //-------- Save Response-------------//
        if(Ans.length>0)
        {
        jQuery.ajax({
            contentType: "application/json; charset=utf-8",
            url: "../WebService/DocCMSApi.svc/Insert_Survey_Vote",
            data: { "surveyid": SurveyID.toString(),
                "ipaddress": IPAddress.toString(),
                "sipaddress": SIPAddress.toString(),
                "surveyquestionid": Ques.toString(),
                "ans": Ans.toString(),
                "responsetext": ResponseText.toString(),
                "additionaltext": AdditionalText.toString(),
                "responsetype": ResponseType.toString(),
            },
            dataType: "json",
            success: function (data) {
         Ans='';
var nextQuest=$("#hdnNextQuest_"+SurveyQuestionID.toString()).val();
if(nextQuest!="0")
{
 if(Show_Survey_Screen_Results_Status=='1')
     {
        // Function to call Show question result status by question ID
        $.ajax({
        url: "../WebService/DocCMSApi.svc/CheckShowReportStatus_ByQuestionID",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyquestionid": SurveyQuestionID },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

    
     function ajaxSucceeded(datavalue) {
       var QuestionShowStatus=datavalue;
       if(QuestionShowStatus=='1')
       {
           Bind_Result_ByQuestionID(SurveyQuestionID,nextQuest);
       }
       else
       {
        $("#divQuestion_"+SurveyQuestionID.toString()).hide();
        $("#divQuestion_"+nextQuest.toString()).show();
       }
                 
                    }
   
     function ajaxFailed() {
                 //do noting
                 }
     }
     else
     {
        $("#divQuestion_"+SurveyQuestionID.toString()).hide();
        $("#divQuestion_"+nextQuest.toString()).show();
      }
}

else
{
 if(Show_Survey_Screen_Results_Status=='1')
 {
     // Function to call Show question result status by question ID
       $.ajax({
        url: "../WebService/DocCMSApi.svc/CheckShowReportStatus_ByQuestionID",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyquestionid": SurveyQuestionID },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });
       function ajaxSucceeded(datavalue) {
       var QuestionShowStatus=datavalue;
       if(QuestionShowStatus=='1')
       {
           Bind_Result_ByQuestionID(SurveyQuestionID,nextQuest);
       }
       else
       {
          $("#DivMainPopup").hide();
          $("#divSurveyNotification").hide();
          alert("Thanks for participating... We will get back to you soon with better services.");
       }
  }
  function ajaxFailed() {
       //do noting
    }
 }
 else
 {
   $("#DivMainPopup").hide();
   $("#divSurveyNotification").hide();
   alert("Thanks for participating... We will get back to you soon with better services.");
 }
}
    },
            error: function (result) {
                alert("Error during operation");
            }
        });
        }
        else
        {
        alert("Please Select Atleast one Option");
        }
    }

function CloseSurvey()
  {
       $("#userPoll").hide();
       $("#cover").hide();
  }
   
   function CloseSurveyResult()
  {
       $("#ResultPoll").hide();
       $("#cover2").hide();
  }

   // For Report 

   function Bind_Result_ByQuestionID(SurveyQuestionID,nextQuest)
   {
     var List="";
     $.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Survey_Question_ByID",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyquestionid": SurveyQuestionID.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

     function ajaxSucceeded(datavalue) {
                var result = datavalue;
                List=List+"<div id='divResultQuestion_"+SurveyQuestionID+"' >";
                List=List+"<input type='hidden' id='hdnNextResultQuest_"+SurveyQuestionID+"' value='"+nextQuest+"'/>";
                List=List+"<p class='popupques' >";
                List=List+result;
                List=List+"</p>";
                List=List+"<div id='divResultAns_"+SurveyQuestionID+"' class='input-btn'>";
                List=List+"</div>";
                List=List+"</div>";
                $("#DivResult").html(List.toString());
                Bind_Survey_AnswerDetail_By_QuestionID(SurveyQuestionID,nextQuest);
                $("#DivMainPopup").hide();
                $("#DivResultPopup").show();
                $("#ResultPoll").show();
     }

      function ajaxFailed(datavalue) {
     }
   }

  function Bind_Survey_AnswerDetail_By_QuestionID(SurveyQuestionID,nextQuest)
  {
   var List="";
     $.ajax({
        url: "../WebService/DocCMSApi.svc/Fetch_Current_Survey_QuestionDetail",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyquestionid": SurveyQuestionID.toString() },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

     function ajaxSucceeded(datavalue) {
       var result = jQuery.parseJSON(datavalue);
          if(result.length>0)
            {
            for(var i=0;i<result.length;i++)
            {
            List=List+"<div class='input-btn'>";
            List=List+"<label style='color:white;' id='txtradio_" + result[i].surveyanswerid + "' for='radio_" + result[i].surveyanswerid + "'><span></span>" + result[i].answer + "</label>";
            List=List+"<div style='width:100%;height:20px'>";
            List=List+"<div class='progress'>";
            List=List+"<div class='progress-bar progress-bar-striped bg-success progress-bar-animated' role='progressbar' aria-valuenow='"+result[i].total+"' style='width: "+result[i].total+"%' aria-valuemin='0' ariavaluemax='100'>";
            List=List+"</div>";
            List=List+"</div>";
            List=List+"<div class='PercentValue'>";
            List=List+"<span style='color:white'>"+result[i].total+"%</span>";
            List=List+"</div>";
            List=List+"</div>";
            List=List+"</div>";
            $("#divResultAns_"+SurveyQuestionID).html(List.toString());
            }
           if(nextQuest!=0)
           {
            VotingDetails= SurveyQuestionID+"_"+nextQuest;
            $("#divResultAns_"+SurveyQuestionID).append("<p>  <button type='button' runat='server' placeholder='Enter your Description' class='btn btn-default' value='Next..' onclick=NextQuestion('"+VotingDetails+"'); > Next..</button></p>");
          }
          else{
          $("#DivMainPopup").hide();
          $("#divResultAns_"+SurveyQuestionID).append("<p>  <button type='button' runat='server' placeholder='Enter your Description' class='btn btn-default' value='Next..' onclick=CloseSurveyResult(); >Close </button></p>");
          $("#divSurveyNotification").hide();
         }
            }
        }

      function ajaxFailed(datavalue) {
     }
  }

  function NextQuestion(SurveyQuestionID,nextQuest)
  {
      var VotingArray=VotingDetails.toString().split('_');
      SurveyQuestionID=VotingArray[0].toString();
      nextQuest=VotingArray[1].toString();
      $("#DivMainPopup").show();
      $("#DivResultPopup").hide();
      $("#divQuestion_"+SurveyQuestionID.toString()).hide();
      $("#divQuestion_"+nextQuest.toString()).show();
  }
  // Check Status weather to show result or not by survey id
  function Check_Survey_Screen_Results_Status()
  {

 $.ajax({
        url: "../WebService/DocCMSApi.svc/CheckShowReportStatus_BySurveyID",
        contentType: 'application/json; charset=utf-8',
        data: { "surveyid": SurveyID },
        dataType: "json",
        success: ajaxSucceeded,
        error: ajaxFailed
    });

 function ajaxSucceeded(datavalue) {
       Show_Survey_Screen_Results_Status=datavalue;
 }
   
function ajaxFailed() {
    }
}