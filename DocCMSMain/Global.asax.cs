﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.Data;
using System.Text.RegularExpressions;
using System.Data;
using DocCMS.Core;
using System.Web.Optimization;
namespace DocCMSMain
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RegisterRoutes(RouteTable.Routes);
        }
       

        private void RegisterRoutes(RouteCollection routeCollection)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string fullOrigionalpath = Request.Url.ToString();
            if (!string.IsNullOrEmpty(fullOrigionalpath) && !fullOrigionalpath.Contains(".js") && !fullOrigionalpath.Contains(".css") && !fullOrigionalpath.Contains("/cadmin/") && !fullOrigionalpath.Contains("/Modules/"))
            {
                string Url_Case = "";
                if (fullOrigionalpath.ToLower().Contains("dfi/") && (fullOrigionalpath.ToLower().EndsWith("dfi/") || fullOrigionalpath.ToLower().EndsWith("dfi/")))
                {
                    Url_Case = "Home";
                }
                else if (fullOrigionalpath.ToLower().Contains("dfi/error404") || fullOrigionalpath.ToLower().Contains("dfi/error404.aspx") || fullOrigionalpath.ToLower().Contains("dfi/error404.html"))
                {
                    Url_Case = "PNF";                    
                }
                else
                {
                    Url_Case = Set_URl_Case(fullOrigionalpath.ToLower());
                }
                if (!string.IsNullOrEmpty(Url_Case))
                {
                    switch (Url_Case)
                    {
                        // Backend URL Rewritting Starts here 
                        case "PNF":
                            Context.RewritePath("~/dfi/error404.aspx");
                            break;
                        case "Index":
                        case "Home":
                            Context.RewritePath("~/dfi/default.aspx");
                            break;
                        case "Products":
                            Context.RewritePath("~/dfi/Products.aspx");
                            break;
                        case "productdescription":
                            Context.RewritePath("~/dfi/productdescription.aspx");
                            break;
                        case "test":
                            Context.RewritePath("~/dfi/test.aspx");
                            break;
                        case "About-DMS":
                            Context.RewritePath("~/dfi/DMS_Solutions.aspx");
                            break;
                        case "More-Information":
                            Context.RewritePath("~/dfi/More_Information.aspx");
                            break;
                        case "Features":
                            Context.RewritePath("~/dfi/Features.aspx");
                            break;
                        case "Benefits":
                            Context.RewritePath("~/dfi/Benefits.aspx");
                            break;
                        case "Downloads":
                            Context.RewritePath("~/dfi/Downloads.aspx");
                            break;
                        case "SDM":
                            Context.RewritePath("~/dfi/SDM.aspx");
                            break;
                        case "UserLogin":
                            Context.RewritePath("~/dfi/UserLogin.aspx");
                            break;
                        case "SpecialPage":
                            Context.RewritePath("~/dfi/SpecialPage.aspx");
                            break;
                        case "DFIUser-Download":
                            Context.RewritePath("~/dfi/Downloads2.aspx");
                            break;
                        case "BlogsDetail":
                            string[] BlogName = fullOrigionalpath.Split('-');
                            BlogName = BlogName[1].Split('.');
                            Int32 BlogID = ServicesFactory.DocCMSServices.Fetch_blogid_Byname(BlogName[0]);
                            Context.RewritePath("~/dfi/BlogDetails.aspx?BlogID=" + BlogID + "");
                            break;
                        case "BlogCategory":
                            string[] CategoryName = fullOrigionalpath.Split('-');
                            CategoryName = CategoryName[1].Split('.');
                            Int32 BlogcategoryID = ServicesFactory.DocCMSServices.Fetch_categoryid_Bycategoryname(CategoryName[0]);
                            Context.RewritePath("~/dfi/Blogs.aspx?CategoryID=" + BlogcategoryID + "");
                            break;
                        case "Blogs":
                            Context.RewritePath("~/dfi/Blogs.aspx");
                            break;
                        case "Company":
                            Context.RewritePath("~/dfi/Company.aspx");
                            break;
                        case "GraphicsDesign":
                            Context.RewritePath("~/dfi/GraphicsDesign.aspx");
                            break;
                        case "Marketing":
                            Context.RewritePath("~/dfi/Marketing.aspx");
                            break;
                        case "Contact":
                            Context.RewritePath("~/dfi/Contacts.aspx");
                            break;
                        case "ProductDescription":
                            Context.RewritePath("~/dfi/ProductDescription.aspx");
                            break;
                        case "Career":
                            Context.RewritePath("~/dfi/Careers.aspx");
                            break;
                        case "CareerDetail":
                            Context.RewritePath("~/dfi/CareerDetail.aspx");
                            break;
                        case "ContactUsImageGenerator":
                            Context.RewritePath("~/dfi/ContactUsImageGenerator.aspx");
                            break;
                        case "FAQ":
                            Context.RewritePath("~/dfi/faq.aspx");
                            break;
                        case "Survey":
                            Context.RewritePath("~/dfi/Survey.aspx");
                            break;

                        case "Store":
                            Context.RewritePath("~/dfi/Store.aspx");
                            break;

                        case "docs":
                            Context.RewritePath("Error404Page.aspx");
                            break;

                        case "WorkGroup":
                        case "Enterprise":
                        case "ProductHistory":
                            string[] ProductName = fullOrigionalpath.Split('.');
                            Int32 ProductID = 0;
                            if (Url_Case == "WorkGroup")
                            {
                                ProductID = ServicesFactory.DocCMSServices.Fetch_Productsid_By_productname("WorkGroup System");
                                Context.RewritePath("~/dfi/WorkgroupSystem.aspx?CMSID=" + ProductID + "");
                            }
                            else if (Url_Case == "Enterprise")
                            {
                                ProductID = ServicesFactory.DocCMSServices.Fetch_Productsid_By_productname("Enterprise System");
                                Context.RewritePath("~/dfi/Enterprise.aspx?CMSID=" + ProductID + "");
                            }
                            else
                            {
                                ProductID = ServicesFactory.DocCMSServices.Fetch_Productsid_By_productname("Product History");
                                Context.RewritePath("~/dfi/ProductHistory.aspx?CMSID=" + ProductID + "");
                            }
                            break;
                        case "Sitemap":
                            Context.RewritePath("~/dfi/Sitemap.aspx");
                            break;
                        // Backend URL Rewriting Ends here 
                    }
                }
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var serverError = Server.GetLastError() as HttpException;

            if (null != serverError)
            {
                int errorCode = serverError.GetHttpCode();

                if (404 == errorCode)
                {
                    Server.ClearError();
                    Response.Redirect("~/dfi/Error404.html");
                }
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        private string Set_URl_Case(string fullOrigionalpath)
        {
            string Url_Case = "";
            if (fullOrigionalpath.ToLower().Contains("/dfi/"))
            {

                //=== FOR THE CASE Direct Links & Template links Combined ==============//
                if (fullOrigionalpath.ToLower().Contains("dfi/default") || fullOrigionalpath.ToLower().Contains("dfi/home") || fullOrigionalpath.ToLower().Contains("dfi/default.aspx"))
                {
                    Url_Case = "Home";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/error404") || fullOrigionalpath.ToLower().Contains("dfi/error404.aspx") || fullOrigionalpath.ToLower().Contains("dfi/error404.html"))
                {
                    Url_Case = "PNF";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/products.aspx") || fullOrigionalpath.ToLower().Contains("dfi/products"))
                {
                    Url_Case = "Products";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/projectmanagement.aspx") || fullOrigionalpath.ToLower().Contains("dfi/projectmanagement"))
                {
                    Url_Case = "ProjectManagement";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/specialpage.aspx") || fullOrigionalpath.ToLower().Contains("dfi/specialpage"))
                {
                    Url_Case = "SpecialPage";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/dms_solutions.aspx") || fullOrigionalpath.ToLower().Contains("dfi/dms_solutions"))
                {
                    Url_Case = "About-DMS";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/productdescription.aspx") || fullOrigionalpath.ToLower().Contains("dfi/productdescription"))
                {
                    Url_Case = "productdescription";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/test.aspx") || fullOrigionalpath.ToLower().Contains("dfi/test"))
                {
                    Url_Case = "test";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/Benefits.aspx") || fullOrigionalpath.ToLower().Contains("dfi/benefits"))
                {
                    Url_Case = "Benefits";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/Features.aspx") || fullOrigionalpath.ToLower().Contains("dfi/features"))
                {
                    Url_Case = "Features";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/More_Information.aspx") || fullOrigionalpath.ToLower().Contains("dfi/more_information"))
                {
                    Url_Case = "More-Information";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/downloads2.aspx") || fullOrigionalpath.ToLower().Contains("dfi/dfiuser-download"))
                {
                    Url_Case = "DFIUser-Download";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/sdm.aspx") || fullOrigionalpath.ToLower().Contains("dfi/sdm"))
                {
                    Url_Case = "SDM";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/downloads.aspx") || fullOrigionalpath.ToLower().Contains("dfi/downloads"))
                {
                    Url_Case = "Downloads";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/userlogin.aspx") || fullOrigionalpath.ToLower().Contains("dfi/userlogin"))
                {
                    Url_Case = "UserLogin";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/blogdetails.aspx") || fullOrigionalpath.ToLower().Contains("dfi/blogdetails"))
                {
                    Url_Case = "BlogDetails";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/blog-"))
                {
                    Url_Case = "BlogsDetail";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/blogs-"))
                {
                    Url_Case = "BlogCategory";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/blogs.aspx") || fullOrigionalpath.ToLower().Contains("dfi/blogs") || fullOrigionalpath.ToLower().Contains("dfi/Blogs-Search"))
                {
                    Url_Case = "Blogs";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/company.aspx") || fullOrigionalpath.ToLower().Contains("dfi/company"))
                {
                    Url_Case = "Company";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/graphicsdesign.aspx") || fullOrigionalpath.ToLower().Contains("dfi/graphicsdesign"))
                {
                    Url_Case = "GraphicsDesign";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/marketing.aspx") || fullOrigionalpath.ToLower().Contains("dfi/marketing"))
                {
                    Url_Case = "Marketing";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/contactusimagegenerator.aspx") || fullOrigionalpath.ToLower().Contains("dfi/contactusimagegenerator"))
                {
                    Url_Case = "ContactUsImageGenerator";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/contacts.aspx") || fullOrigionalpath.ToLower().Contains("dfi/contact"))
                {
                    Url_Case = "Contact";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/careerdetail.aspx") || fullOrigionalpath.ToLower().Contains("dfi/careerdetail"))
                {
                    Url_Case = "CareerDetail";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/careers.aspx") || fullOrigionalpath.ToLower().Contains("dfi/career"))
                {
                    Url_Case = "Career";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/faq.aspx") || fullOrigionalpath.ToLower().Contains("dfi/faq"))
                {
                    Url_Case = "FAQ";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/workgroupsystem.aspx") || fullOrigionalpath.ToLower().Contains("dfi/workgroupsystem"))
                {
                    Url_Case = "WorkGroup";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/enterprise.aspx") || fullOrigionalpath.ToLower().Contains("dfi/enterprise"))
                {
                    Url_Case = "Enterprise";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/producthistory.aspx") || fullOrigionalpath.ToLower().Contains("dfi/producthistory"))
                {
                    Url_Case = "ProductHistory";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/survey.aspx") || fullOrigionalpath.ToLower().Contains("dfi/survey"))
                {
                    Url_Case = "Survey";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/sitemap.aspx") || fullOrigionalpath.ToLower().Contains("dfi/sitemap"))
                {
                    Url_Case = "Sitemap";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("dfi/store.aspx") || fullOrigionalpath.ToLower().Contains("dfi/store"))
                {
                    Url_Case = "Store";
                    return Url_Case;
                }
                if (fullOrigionalpath.ToLower().Contains("docs") || fullOrigionalpath.ToLower().Contains("docs"))
                {
                    Url_Case = "docs";
                    return Url_Case;
                }
            }
            return Url_Case;
        }
    }//== CLASS ENDS HERE ===//
}